<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Test
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all test related ajax request.
 */
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];

    switch ($action) {
    case 'memberTestStatus':
        $params['companyId'] = COMPANY_ID;
        $params['clubId'] = isset($_SESSION['currentClubId']) ? 
            $_SESSION['currentClubId'] : 0;
        $members = $this->ios->getTestInforForCoach($params);

        $response['status'] = 'error';

        if ($members['members']['memberId'] > 0) {
            $members['members'] = array(0 => $members['members']);
        }

         //Generate member test graph data
        if ($members['members'][0]['memberId'] > 0) {
            $response['status'] = 'success';
            foreach ($members['members'] as $member) {
                $member['heartRateValue'] = explode('_', $member['heartRateValue']);
                $hrate = '';
                foreach ($member['heartRateValue'] as $key => $heartrate) {
                    $hrate[] = array('y' => (int) $heartrate, 'x' => $key);
                }

                   //Get Load value begins
                   $currentTestTime = $member['currentTestTime'];

                $loadValues = json_decode($member['loadValue'], true);
                $loadValueArr=$timeArr=array();
                foreach ($loadValues as $key => $loadValue) {
                    $loadValueArr[] = $loadValue['load'];
                    $timeArr[] = $loadValue['time'];
                }

                /* get the less than (latest and last) nearest value. */
                $nearestValues = $this->test->getclosest($timeArr, $currentTestTime);
                if (end($nearestValues) != '') {
                    $keyOfValue = end($nearestValues);
                    $keyValue = array_search($keyOfValue, $timeArr);
                    $getLoadValue = $loadValueArr[$keyValue];
                } else {
                    $getLoadValue = 0;
                }
                $member['loadValue'] = $getLoadValue;

                    //Get Load value ends					

                    $member['currentHeartrate'] = end($member['heartRateValue']);
                $member['heartRateFormatted'] = $hrate;
                $member['currentTestTime'] = gmdate(
                    'H:i:s', $member['currentTestTime']
                );

                $result['cycle'.$member['machineId']] = $member;
            }
            $response['data'] = isset($result) ? $result:array();
        }
            echo json_encode($response);
        break;
    /*case 'getTestList':
        $result = $this->test->getTestList();
        echo json_encode($result);
        break;
    */
    }
}
