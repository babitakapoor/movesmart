<?php

/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all settings related ajax request.
 */
//require_once '../lib/ExcelReader.php';
//require_once '../lib/PHPExcel.php';

if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
	
	//printLog($_REQUEST);
	
	switch ($action) {
		
		case 'RegisterNewMachine':
			$params = $_REQUEST;
			$res = $this->machine->addMachine($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'GetMachinesList':
			$params = $_REQUEST;
			$res = $this->machine->getMachineList($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'SetMachinePairing':
			$params = $_REQUEST;
			$res = $this->machine->setMachinePairing($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'GetClientReadyForMachine':
			$params = $_REQUEST;
			$res = $this->machine->getClientReadyForMachine($params);
			if(isset($res['clientlist']) && !isset($res['clientlist'][0]))
			{
				$res['clientlist'] = array(0=>$res['clientlist']);
			}
			echo json_encode($res,JSON_NUMERIC_CHECK);
			break;
		case 'SendTrainingData':
			$params = $_REQUEST;
			$res = $this->machine->sendTrainingData($params);
			//echo $res;die;
			echo json_encode($res,JSON_NUMERIC_CHECK);
			break;
		case 'RegisterClientHRDevice':
			$params = $_REQUEST;
			$res = $this->machine->registerClientHRDevice($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'GetCurrentTrainingStatus':
			$params = $_REQUEST;
			$res = $this->machine->getCurrentTrainingStatus($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;	
		case 'ValidateCoach':
			$params = $_REQUEST;
			$res = $this->machine->validateCoach($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'SearchClientByFirstName':
			$params = $_REQUEST;
			$res = $this->machine->searchClientByFirstName($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'StartTraining':
			$params = $_REQUEST;
			$res = $this->machine->startTraining($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'UpdateTrainingCredits':
			$params = $_REQUEST;
			$res = $this->machine->updateTrainingCredits($params);
			echo json_encode($res,JSON_UNESCAPED_SLASHES);
			break;
		case 'UpdateTrainingStatus':
			$params = $_REQUEST;
			$res = $this->machine->updateTrainingStatus($params);
			echo json_encode($res,JSON_NUMERIC_CHECK);
			break;
	}
}
