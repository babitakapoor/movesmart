<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package UserProfile
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all userprofile related ajax request.
 */
try {
    global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
        /*
    case 'getUserProfilePage':
           $page = $this->common->getUserProfilePage();
           echo $page;
        break;
        /*MK Added Block - Update Profile*/
    case 'updateProfiledata':
        $prams['userId'] = $_REQUEST['id'];
        $prams['question'] = isset($_REQUEST['question']) ? 
            $_REQUEST['question'] : '';
        $prams['first_name'] = $_REQUEST['first_name'];
        $prams['last_name'] = $_REQUEST['last_name'];
        $prams['email'] = $_REQUEST['email'];
        $prams['phone'] = $_REQUEST['phone'];
        $prams['gender'] = $_REQUEST['gender'];
        if (isset($_REQUEST['language'])) {
            $prams['language'] = $_REQUEST['language'];
        }
        if (isset($_REQUEST['passwrd']) and ($_REQUEST['passwrd'] != '')) {
            $prams['passwrd'] = $_REQUEST['passwrd'];
        }
		print_r($prams);
		die;    
        $result = $this->coach->updateUserProfile($prams);
        echo json_encode($result);
        break;
        /* SK- External image path upload, image & video */
    case 'externalImageUrlData':
        error_reporting(0);
        $get_url = $_REQUEST['url'];
        $url = trim($get_url);
        if ($url) {
            $file = fopen($url, 'rb');
            $directory = '../images/uploads/movesmart/machines/';
            $valid_exts = array(
                'php', 'jpeg', 'gif', 'png', 'JPEG', 
                'bmp', 'mp4', 'wmv', 'avi', 'jpg'
            );
            $ext = end(explode('.', strtolower(basename($url))));
            if (in_array($ext, $valid_exts)) {
                $newfilename = date('Ymdhis.').$ext;
                $filename = $newfilename;
                $newfile = fopen($directory.$filename, 'wb');
                if ($newfile) {
                    while (!feof($file)) {
                        fwrite($newfile, fread($file, 1024 * 8), 1024 * 8);
                    }
                    $imagedetails = array(
                            'status' => 'success',
                            'status_code' => 200,
                            'status_message' => 'Successfully Image Uploaded',
                            'imagefile' => $newfilename,
                        );
                    echo json_encode($imagedetails);
                } else {
                    echo 'File does not exists';
                }
            } else {
                echo 'Invalid URL';
            }
        } else {
            echo 'Please enter the URL';
        }
        break;
    case 'default':
        break;
    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
