<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package UploadImages
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all uploadimage related ajax request.
 */
global $isCronVarApi;
$isCronVarApi = 1;
$uploadSuccess = array();
if (isset($_FILES['file']['name']) 
    && ($_POST['uploadFor'] == 'menuIcons' 
    || $_POST['uploadFor'] == 'profileCoachImage' 
    || $_POST['uploadFor'] == 'questionIcons' 
    ||  $_POST['uploadFor'] == 'machineIcons' 
    ||  $_POST['uploadFor'] == 'uploadImportFile')
) {
    $userId = $_POST['imageNameContains']; // get userid;
    $filename = $_FILES['file']['name'];
    $tmp_name = $_FILES['file']['tmp_name'];
    $filearray = explode('.', $filename);
    $extension = end($filearray);
    $isUploaded = false;
    if (0 < $_FILES['file']['error']) {
        echo 'Error:'.$_FILES['file']['error'];
    } else {
        $dir = SERVICE_MENU_ICON;
        if ($_POST['uploadFor'] == 'profileCoachImage') {
            $dir = IMAGE_PROFILE_PATH;
        }
        if ($_POST['uploadFor'] == 'questionIcons') {
            $dir = SERVICE_QUESTION_ICON;
        }
        if ($_POST['uploadFor'] == 'machineIcons') {
            $dir = SERVICE_MACHINE_ICON;
        }
        if ($_POST['uploadFor'] == 'uploadImportFile') {
            $dir = SERVICE_IMPORT_TRANSLATION_FILE;
        }
        $newfilename = date('Ymdhis.').$extension;
        $uploadfile = $dir.$newfilename;
        if (move_uploaded_file($tmp_name, $uploadfile)) {
            $isUploaded = true;
            $uploadSuccess['imagefile'] = $newfilename;
            $uploadSuccess['status'] = 'Image Uploaded successfully';
        }
    }
} else {
    $uploadSuccess['status'] = 'Please Check the uploaded file type';
}
if (!isset($_POST['noecho'])) {
    echo json_encode($uploadSuccess);
}
