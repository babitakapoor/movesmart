<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Coach
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all coach related ajax request..
 */
global $LANG;
try {
    global $isCronVarApi;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }

    switch ($method) {
    case 'updateCoach':
         parse_str($_REQUEST['dataPost'], $params);
		 $relation_coach  = implode(',',$params['client_app']);
		 $params['relation_coach'] = $relation_coach;
         $result = $this->coach->getCoachAddOrUpdate($params);
         //Save Coach Profile picture.. 
        if (isset($_FILES['file'])) {
            if (($_FILES['file']['size']) != 0) {
                if (isset($result['movesmart']['userId'])) {
                    $_POST['uploadFor'] = 'profileCoachImage';
                    $_POST['imageNameContains'] = $result['movesmart']['userId'];
                    $_POST['uploadpath'] = IMAGE_PROFILE_PATH;
                    $_POST['noecho'] = 1;
                    include_once 'uploadImage.php';
                    if (isset($uploadSuccess)) {
                        $paramnew = array();
                        $paramnew['userImage'] = $uploadSuccess['imagefile'];
                        $paramnew['userId'] = $result['movesmart']['userId'];
                        $this->members->getSaveProfileImage($paramnew);
                    }
                }
            }
        }
        //End - Save Coach Profile picture.
         $_SESSION['fl'] = array(1, $LANG['msgCoachDetailsUpdateSuccess']);
         echo json_encode($result);
        break;

    case 'deleteCoach':
         $param = $_REQUEST;
         $param['userId'] = $_REQUEST['userId'];
         $result = $this->coach->getCoachDelete($param);
         echo json_encode($result);
        break;

    case 'update-coach-profile-image':
        $param['userImage'] = $_REQUEST['user_image'];
        $param['userId'] = $_REQUEST['user_id'];
        $result = $this->members->getSaveProfileImage($param);
         echo json_encode($result);
        //To logout user
        break;

    case 'coachListGrid':    // for ajax on key press loading the coach List 			
        /* To List the Clubs in Dropdown */
        $params['company_id'] = COMPANY_ID;
        $params['is_deleted'] = 0;
        $params['authorizedClubId'] =  '';
        if (isset($_SESSION['club']['authorizedClubId'])) {
            $params['authorizedClubId'] =  $_SESSION['club']['authorizedClubId'];
        }
        $clubList = $this->club->getClubList($params);

        $param['userType'] = 'coach';

        /*$userType : Param Types should be any one of these 
        all/employee/coach/admin/backOffice/member*/
        $param['loggedUserType'] = $_SESSION['user']['usertype_id'];

        //To search param 	
        $param['clubId'] = (isset($_REQUEST['clubId']) ? 
            $_REQUEST['clubId'] : '');
        $param['searchType'] = (isset($_REQUEST['searchType']) ? 
            $_REQUEST['searchType'] : '');
        $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
            ($_REQUEST['searchType'] == 'gender' ? 
            (strstr($_REQUEST['searchValue'], 'f') ? 
            '1' : '0') : $_REQUEST['searchValue']) : '');

        /*To sort param , If the param label field is 
        empty / default value should define at else part.*/
        $param['labelField'] = (isset($_SESSION['pageName'][$_REQUEST['p']])) ? 
            $_SESSION['pageName'][$_REQUEST['p']] : 'user_id';
        $param['sortType'] = (isset($_SESSION['pageName'][$_REQUEST['p']]) 
            && $_SESSION['sortType'] == 2) ? 'desc' : 'asc';

        //Company id
        $param['companyId'] = COMPANY_ID;

        //Specific/Authorized clubs
        $param['authorizedClubId'] 
            = (isset($_SESSION['club']['authorizedClubId']) ? 
            $_SESSION['club']['authorizedClubId'] : '');

        //Pagination code starts
        $limitStart = 0;
        $limitEnd = PAGINATION_SHOW_PER_PAGE;

        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
            $limitEnd = $_REQUEST['offset'];
        }
        if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
            $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
        }

        $param['limitStart'] = $limitStart;
        $param['limitEnd'] = $limitEnd;

        // Result set	
        $arrayListCoach = $this->members->getUserList($param);

        $arrayList = $arrayListCoach['result'];

        //Total count to create pagination
        $totalCount = $arrayListCoach['totalCount'];

        //Get current link which is not consider ajax page, ajax page should be 0
        $this->paginator->getUrlLink($param = 0);

            //Pagination Class in body section
        if (isset($arrayList) && !empty($arrayList) 
            && isset($arrayList[0]['user_id'])
        ) {
            echo $this->paginator->displayItemsPagination(
                $arrayList,
                array('user_id',
                      'first_name',
                      'middle_name',
                      'last_name',
                      'gsm',
                      'club_name',
                      'email',
                      'status',
                      'specValues2' => '<a onclick="redirectEdit(this,2)" hrefValue="$user_id$" 
                                        class="btn-link btn-inline dotline-sep">
                                        <span class="icon icon-edit"></span></a>
                                            <a href="#" class="btn-link btn-inline ">
                                                <span class="icon icon-cls-sm"></span></a>',
                     ),
                array('startTag' => '<tr>', 'midTagOpen' => '<td>', 
                'midTagClose' => '</td>', 'endTag' => '</tr>')
            );
        } else {
            echo "<tr><td colspan='20'>".$LANG['noResult'].'</td></tr>';
        }
        if (isset($arrayList) && !empty($arrayList) 
            && isset($arrayList[0]['user_id'])
        ) {
            echo '<tr class="pagination-end-row">
                        <td colspan="11">
                            <div class="pagination-block">';
                            //Pagination navigation class
                            echo $this->paginator->displayPagination($totalCount);
            echo '</div>
                </td>
                </tr>';
        }
        break;

    case 'default':
        break;
    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
