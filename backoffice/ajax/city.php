<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package City
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To manage city via ajax.
 */

global $isCronVarApi;
$isCronVarApi = 1;
if (isset($_POST['cityzip'])) {
    $param['zipCode'] = $_POST['cityzip'];
    $cityLists = $this->members->getCityList($param);
    echo '<option value="">Select City</option>';
    foreach ($cityLists as $cityZipValue) {
        echo '<option value="'.$cityZipValue['city_id'].'">'.
        $cityZipValue['city_name'].'</option>';
    }

    if ($param['zipCode'] != '') {
        echo '___';
        foreach ($cityLists as $cityZipValue) {
            if ($cityZipValue['city_id'] != '') {
                echo '<li id="ui-id-'.$cityZipValue['city_id'].
                '" class="ui-menu-item" tabindex="-1" 
                onclick="autoCompleteLi(this,\'city\')">'.
                $cityZipValue['city_name'].'</li>';
            }
        }
    }
}
