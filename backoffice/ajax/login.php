<?php
error_reporting(0);
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all login related ajax request.
 */
try {
    global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['method'];
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }

    switch ($method) {
        //To authenticate user
    case 'authenticate':
           $param['username'] = $_REQUEST['username'];
           $param['password'] = $_REQUEST['password'];
           /* MK Added the User Type Filter*/
        if (isset($_REQUEST['usertype'])) {
            $param['usertype_id'] = $_REQUEST['usertype'];
        }

           $user = $this->login->authenticate($param);
        if ($user['status_code'] == 1) {
            $_SESSION['user'] = $user; //set user data in session
            //$_SESSION['theme'] = $_SESSION['user']['r_company_id'];
            $_SESSION['theme'] = $_SESSION['user']['r_company_id'];
            $_SESSION['primaryClubId'] = $user['r_club_id'];
            $_SESSION['primaryClubName'] = $user['club_name'];
            $_SESSION['primaryClubRole'] = $user['usertype'];
            $_SESSION['currentClubId'] = $user['r_club_id'];
            $_SESSION['currentClubName'] = $user['club_name'];
            $_SESSION['currentClub_ismedicalinfo'] = $user['isclubmedicalinfo'];
            $_SESSION['currentClubRole'] = $user['usertype'];
            $_SESSION['language'] = 'EN';
            //$clubExistIds[] = $_SESSION['primaryClubId'];
            $_SESSION['club']['authorizedClubId'] = $_SESSION['currentClubId'];

            //Check if user is employee and set user club sessions
            $empType = array(1, 2, 4, 9);  
            if (in_array($user['usertype_id'], $empType)) {
                // Get employee role club ids begins
                $paramsR['userId'] = $user['user_id'];
                $paramsR['companyId'] = $_SESSION['user']['r_company_id'];
                $clubRoles = $this->club->getEmployeeClubRoles($paramsR);
				
                if (is_array($clubRoles)) {
                    $clubExist  =   array();
                    foreach ($clubRoles as $roles) {
                        $clubExist[] = $roles;
                        $clubExistIds[] = $roles['r_club_id'];
                    }
                    if (isset($clubExistIds) && is_array($clubExistIds) 
                        && (count($clubExistIds) > 0)
                    ) {
                        $_SESSION['associatedClubIds'] = implode(',', $clubExistIds);
                        $_SESSION['associatedClubs'] = $clubExist;
                        $_SESSION['showClubChangePop'] = 1;
                        $_SESSION['club']['authorizedClubId'] 
                            = $_SESSION['currentClubId'];
                    }
                }
                // Get employee role club ids ends						
            }

            //Set session to access all clubs
            $userType = array(2, 4, 9);
            if (in_array($user['usertype_id'], $userType)) {
                unset($_SESSION['showClubChangePop']);
                unset($_SESSION['club']['authorizedClubId']);

                //To enable common access to specified user types
                $_SESSION['showCommonAccess'] = 1;
                //To get club ids
                $paramsC['company_id'] = $_SESSION['user']['r_company_id'];
                $paramsC['is_deleted'] = 0;
                $clubList = $this->club->getClubList($paramsC);
                if (is_array($clubList)) {
                    foreach ($clubList as $row) {
                        $clubIdsArr[] = $row['club_id'];
                    }
                }
                if (is_array($clubIdsArr)) {
                    $_SESSION['associatedClubIds'] = implode(',', $clubIdsArr);
                    $_SESSION['club']['authorizedClubId'] 
                        = $_SESSION['associatedClubIds'];
                }
            }
        }
           //To restrict access for member
           $_SESSION['displayRespUser'] = 0;
        if (isset($user['usertype_id'])) {
        }
        if ($user['usertype_id'] == '3') {
            $_SESSION['displayRespUser'] = 1;
        }

           $result['status_code'] = $user['status_code'];
           $result['usertype_id'] = $user['usertype_id'];
           $paramMenu['isVisibleUser'] = 1;
           $paramMenu['usertype_id'] = $user['usertype_id'];
           $menuListDynamic = $this->settings->getMenuPages($paramMenu);
           $menupage_root = array();
        foreach ($menuListDynamic as $menu) {
            $menupage_root[$menu['parent_id']][] = $menu;
        }
           $securityQuestions = $this->settings->getSecurityQuestions();
           $result['user'] = $user;
           $result['user_session'] = $_SESSION;
        if ($user['status_code'] == 1) {
            $result['menuListDynamic'] = $menupage_root;
            $result['securityQuestions'] = $securityQuestions['sQuestions'];
        }
		
        break;
    case 'changeEmployeeClub':
        $clubId = $_POST['clubId'];
        $result = 0;
        if (isset($_SESSION['associatedClubIds'])) {
            $clubIdsArr = explode(',', $_SESSION['associatedClubIds']);

            //Check for club id
            if (in_array($clubId, $clubIdsArr)) {
                //Search and change session club id and club name
                foreach ($_SESSION['associatedClubs'] as $club) {
                    if ($club['r_club_id'] == $clubId) {
                        $_SESSION['currentClubId'] = $club['r_club_id'];
                        $_SESSION['currentClubName'] = $club['club_name'];
                        $_SESSION['currentClubRole'] = $club['usertype'];
                        $_SESSION['user']['usertype_id'] 
                            = $club['usertype_id'];
                        $_SESSION['user']['usertype'] = $club['usertype'];
                        $result = 1;
                        break;
                    }
                }
            }
        }
        $_SESSION['club']['authorizedClubId'] = $_SESSION['currentClubId'];
        //echo $result;
        //exit;	
        break;
    case 'getSequirityQuestions':
        //Send Email to user
        $result = array();
        $securityQuestions = $this->settings->getSecurityQuestions();
        $result['securityQuestions'] = $securityQuestions['sQuestions'];
        break;
    case 'checkSecurityAnswers':
            $params['email'] = $_REQUEST['email'];
            $params['randStr'] = $this->common->randStr(35);
            $usr = $this->members->UpdateForgotPasswordDetails($params);
        if (count($usr) > 0 and $usr['status_code'] == 200) {
            $security_questions = $usr['user']['security_questions'];
            $paramChkAns['email'] = $usr['user']['email'];
            $paramChkAns['key'] = $params['randStr'];
            $paramChkAns['password'] = $this->common->randStr(5);
            $question = (isset($_REQUEST['question']) 
                and $_REQUEST['question'] != '') ? 
                json_decode($_REQUEST['question'], true) : array();
            $chkquestion = ($security_questions != '') ? 
                json_decode($security_questions, true) : array();
            $check = false;
            $result['status_code'] = 0;
            $result['message'] = 'Server Error.Please try again later.';
            if ((count($question) > 0) 
                and (count($chkquestion) > 0)
            ) {
                $check = true;
                foreach ($chkquestion as $ques => $ans) {
                    if ($question[$ques] != $ans) {
                        $check = false;
                    }
                }
            }
            if ($check) {
                $respns = $this->members->updateMemberPasswordWithKey($paramChkAns);
                // DO MAIL HERE $paramChkAns['password']
                $emailParams = array();
                $emailParams['from'] = SEND_EMAIL_FROM;
                $emailParams['fromName'] = SEND_EMAIL_FROM_NAME;
                $emailParams['to'] = $paramChkAns['email'];
                $emailParams['subject'] = 'Password reset user';
                $imgtoplogo = BASE_URL.'images/mailtoplogo.png';
                $imgbottomlogo = BASE_URL.'images/mailbottomlogo.png';
                $bodyhtml   =   file_get_contents("../templates/email/forgotpassword.html");
                $bodyhtml   =   str_replace('{{EMAILTOPLOGO}}',$imgtoplogo,$bodyhtml);
                $bodyhtml   =   str_replace('{{FIRSTNAME}}',$usr['user']['first_name'],$bodyhtml);
                $bodyhtml   =   str_replace('{{LASTNAME}}',$usr['user']['last_name'],$bodyhtml);
                $message= 'Thanks for your registration at MOVESMART.company,';
                $bodyhtml   =   str_replace('{{MESSAGE}}',$message,$bodyhtml);
                $bodyhtml   =   str_replace('{{EMAIL}}',$paramChkAns['email'],$bodyhtml);
                $bodyhtml   =   str_replace('{{PASSWORD}}',$paramChkAns['password'],$bodyhtml);
                $bodyhtml   =   str_replace('{{EMAILBOTTOMLOGO}}',$imgbottomlogo,$bodyhtml);
                /*
                $body = '<div style="padding: 0 10px" >'.
                            '<img src="'.$imgtoplogo.'" alt="logo" />'.
                        '</div><br><br>';
                $body .= 'Dear '.$usr['user']['first_name'].' '.
                          $usr['user']['last_name'].',<br><br><br>';
                $body .= 'Thanks for your registration at MOVESMART.company,';
                $body .= '<br><br>Login :'.$paramChkAns['email'].' <br>';
                $body .= 'Password :'.trim($paramChkAns['password']).' <br><br><br>';
                $body .= 'With kind Regards,<br><br>MOVESMART.company<br>';
                $body .= 'T.+32(0)468 1941 85<br>';
                $body .= 'M.<a href="mail:support@movesmart.company">';
                $body .= "support@movesmart.company</a><br><br>"; 
                $body .= "<div style='padding:0 10px'><img src='".$imgbottomlogo."'";
                $body .= " alt='eatfreshicon' /> </div><br/><br/>";
                /*ED Commented - As Required UI  
                *<img src='http://www.movesmart.company/test/backoffice
                /images/movesmartlogo.png' 
                alt='movesmartlogo' /> <img src='images/mindswitchicon.png' 
                alt='mindswitchicon'/>
                </div><br><br>";* /
                $body .= 'MOVESMART.company BV<br>';
                $body .= 'Kwelkade 1 - 4001 RK Tiel<br>';
                $body .= "M.<a href='http://www.movesmart.company/'>";
                $body .= "www.MOVESMART.COMPANY</a><br><br><br>";
                /* MK Added Email Template . * /
                $body = 'Hi '.$usr['user']['first_name'].' '.
                $usr['user']['last_name'].',<br />';
                $body .='your password is <p><b>"'.
                trim($paramChkAns['password']).'"</b></p>'; 
                $body .= "<br><br>Regards,<br> Movesmart Team";
                */
                $emailParams['message'] = $bodyhtml;
                $result = array();
                $result['status_code'] = 0;
                $result['message'] = 'Network Error.Please try again later.';
                if ($this->common->sendEmail($emailParams)) {
                    $result['status_code'] = 1;
                    $result['message'] 
                        = 'Password is sent to your E-mail. Please check.';
                } else {
                    if ((isset($respns['status_code'])) 
                        && ($respns['status_code'] == 200)
                    ) {
                        $result['status_code'] = 1;
                        $result['message'] 
                            = 'Password not sent. Please try again later';
                    } else {
                        $result['status_code'] = 0;
                        $result['message'] 
                            = 'Invalid Login. Please re-try with correct details.';
                    }
                }
            } else {
                $result['status_code'] = 0;
                $result['message'] 
                    = 'Invalid Login. Please re-try with correct information.';
            }
        } else {
            $result['status_code'] = 3;
            $result['message'] 
                = 'Email not found. Please check associated email is given.';
        }
        echo json_encode($result);
        exit;
        break;
    case 'logout':
        session_destroy();
        $path = '../index.php?p=login';
        ob_clean();
        header('Location: '.$path);
        break;
    }
    if(!isset($result)){
        $result =   array();
    }
    echo json_encode($result);
    exit;
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
