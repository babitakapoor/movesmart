<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package AddCity
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Page to Quick add city page.
 */
global $LANG;
if ((isset($_REQUEST['cityName']) && $_REQUEST['cityName'] != '') 
    && (isset($_REQUEST['zipCode']) && $_REQUEST['zipCode'] != '')
) {
    $param['city_name'] = $_REQUEST['cityName'];
    $param['zipcode'] = $_REQUEST['zipCode'];
    $result = $this->quickAdd->insertCity($param);
    echo $result['response']['status_message'].'#'.
        $result['response']['status'].'#'.(isset($result['response']['insert_id']) ? 
        $result['response']['insert_id'] : '');
} else {
    $Errormsg = $Errormsg1 = '';
    $Errormsg = ($_REQUEST['cityName'] == '') ?  
                    $LANG['errProvideCityName'] :  $LANG['errProvideZipCode'];
    $Errormsg1 = ($_REQUEST['cityName'] == '' && $_REQUEST['cityName'] == '') ?  
                    $LANG['errProvideCityZipCode'] : '';
    $msg = ($Errormsg == '') ? $Errormsg1 : $Errormsg;
    echo $msg.'#'.'#';
}
