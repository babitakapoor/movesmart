<?php
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Questionaries
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all questionaries related ajax request.
 */
try {
    global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];

    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);
    }
    switch ($method) {
    case 'loadClientQuestionaries':
        $params = $_REQUEST;
        $result = $this->questionaries->loadClientQuestionaries($params);
        if (isset($result['status_code']) and $result['status_code'] == 200) {
            $responsedata = $result['data'];
            echo json_encode(
                array(
                    'status' => 'success', 
                    'data' => $responsedata
                )
            );
        } else {
            echo json_encode(array('status' => 'failure'));
        }
        break;
    case 'updateAnswerForQuestion':
        $params = $_POST;
        $response = array(
            'status' => 'success', 
            'status_message' => 'Result got successfully'
        );
        $result = $this->questionaries->updateAnswerForQuestion($params);
        if ($result['status_code'] == 200) {
            $response = $result;
        }
        echo json_encode($response);
        break;
    case 'searchCoachAndClub':
        $params = $_REQUEST;
        $result = $this->questionaries->searchCoachAndClub($params);
        $response = array(
            'status' => 0, 
            'status_message' => 'Result got successfully'
        );
        if ($result['status_code'] == 1) {
            $response = $result['data'];
        }
        echo json_encode($response);
        break;
    case 'sendAppoinmentRequest':
        $params = $_REQUEST;
        $coach_result = array();
        $user_result = array();
        if (isset($params['ClubID']) and isset($params['CoachID'])) {
            $coach_result = $this->members->getUserDetailByID(
                array('UserID' => $params['CoachID'])
            );
            $user_result = $this->members->getUserDetailByID(
                array('UserID' => $params['userId'])
            );
        }
        $coach = array();
        $user = array();
        if (isset($coach_result['status_code']) 
            and ($coach_result['status_code'] == 200)
        ) {
            $coach = $coach_result['userDetail'];
        }
        if (isset($user_result['status_code']) 
            and ($user_result['status_code'] == 200)
        ) {
            $user = $user_result['userDetail'];
        }
        if (count($user) > 0 and count($coach) > 0) {
            $paramUser['userId'] = $user['user_id'];
            $paramUser['userClubId'] = $params['ClubID'];
            $updateUser = $this->members->updateUserDetail($paramUser);
            $user_email = $user['email'];
            $email_params['to'] = $coach['email'];
            $email_params['from'] = SEND_EMAIL_FROM;
            $email_params['fromName'] = SEND_EMAIL_FROM_NAME;
            $imgtoplogo = BASE_URL.'images/mailtoplogo.png';
            $imgbottomlogo = BASE_URL.'images/mailbottomlogo.png';            
            $email_params['subject'] 
                = 'Movesmart - Client Request for Test Appointment';
            $body = "<div style='padding: 0 10px' >";
            $body .= "<img src='".$imgtoplogo."' alt='logo' /></div><br><br>";

          /*  $body .=    '<p>Hi '.$coach['first_name'].',</p>
                            <p>'.$user['first_name'].' has sent request 
                                for an appointment for a test.</p>
                            <p>Member Name: '.$user['first_name'].'</a></p>
                            <p>Email: <a href="mailto:'.$user_email.'">
                                '.$user_email.'</a></p><br/><br/>';*/
		 $body .=    '<p>Hi '.$coach['first_name'].',</p>
                            <p>'.$user['first_name'].' heeft zich aangemeld voor een coachgesprek. Open de app om deze klant te bekijken.</p>
                            <p>Mvg,</p><br/>
							<p>MOVESMART</p>';
         //   $body .= "<div style='padding:0 10px' >";
           // $body .= "<img src='".$imgbottomlogo."' alt='eatfreshicon' /></div>";
          //  $body .= 'MOVESMART.company BV<br>';
          //  $body .= 'Kwelkade 1 - 4001 RK Tiel<br>';
          //  $body .= "M.<a href='http://www.movesmart.company/'>";
           // $body .= "www.MOVESMART.COMPANY</a><br><br><br>";
            $email_params['message'] = $body;
            $msgemailsuc = 'Appointment request E-mail sent successfully';
            $msgemailfail = 'Appoint E-Mail not sent. Please try again later';
            if ($this->common->sendEmail($email_params)) {
                $response['success'] = 1;
                $response['message'] = $msgemailsuc;
            } else {
                $response['success'] = 1;
                $response['message'] = $msgemailfail;
            }
            $this->members->addCoachMember(
                array(
                'CoachId' => $coach['user_id'],
                'UserId' => $user['user_id'],
                'GroupId' => $params['groupID'],
                    )
            );
            $updated = $this->members->mapClubAndMember(
                array(
                'UserId' => $user['user_id'],
                'ClubID' => $params['ClubID'],
                'UserTypeId' => $user['r_usertype_id'],
                'CompanyId' => $user['r_company_id'],
                'isPrimary' => 0,
                    )
            );
        } else {
            $response['status'] = 0;
            $response['message'] = 'Failed to retrieve user';
        }
        echo json_encode($response);
        break;
    case 'updateUserPhase':
        $params = array();
        $params['userId'] = $_REQUEST['userId'];
        $params['phaseId'] = $_REQUEST['phaseId'];
        $params['groupId'] = $_REQUEST['groupId'];
        //$params['steptypeid'] = $_REQUEST['steptypeid'];
        
		/*$acticity_step_id = isset(
            $_REQUEST['steptypeid']
        ) ? $_REQUEST['steptypeid'] : 0;
		*/
		
        if (isset($_REQUEST['dailyTimer']) && $_REQUEST['dailyTimer']!='') {
            $params['dailyTimer']   =   $_REQUEST['dailyTimer'];
        }
        if (isset($_REQUEST['completed'])) {
            $params['completed'] = 1;
        }
        $result = $this->questionaries->updateUserPhase($params);
        $updStepstype = $this->members->updatePersonalInfo(
            array(
                //'stepTypeId' => $acticity_step_id, 
                'userId' => $params['userId']
            )
        );
        echo json_encode($result);
        break;
    /*case 'updateAnswersFromPreviousDay':
        $params = $_REQUEST;
        $result = $this->questionaries->updateAnswersFromPreviousDay($params);
        if (isset($result['status_code']) and $result['status_code'] == 200) {
            $responsedata = $result['data'];
            echo json_encode(
                array(
                    'status' => 'success', 
                    'data' => $responsedata
                )
            );
        } else {
            echo json_encode(array('status' => 'failure'));
        }
        break;
    */
    case 'updateAchivePoints':
        $params = $_REQUEST;
        $result = $this->questionaries->updateAchivePoints($params);
        if (isset($result['status_code']) and $result['status_code'] == 200) {
            $responsedata = $result['data'];
            echo json_encode(
                array(
                    'status' => 'success', 
                    'data' => $responsedata
                )
            );
        } else {
            echo json_encode(array('status' => 'failure'));
        }
        break;
    case 'resetQuestionMonitor':
        $params = array();
        $params['userId'] = $_REQUEST['userId'];
        $params['phaseId'] = $_REQUEST['phaseId'];
        $params['groupId'] = $_REQUEST['groupId'];
        $params['activityques'] = $_REQUEST['activityques'];
        $result = $this->questionaries->resetQuestionMonitor($params);
        echo json_encode($result);
        break;
    case 'resetAvailableTest':
        $params = array();
        $params['userId'] = $_REQUEST['userId'];
        $result = $this->questionaries->resetAvailableTest($params);
        echo json_encode($result);
        break;
    case 'resetTraining':
        $params = array();
        $params['userId'] = $_REQUEST['userId'];
        $result = $this->questionaries->resetTraining($params);
        echo json_encode($result);
        break;
    case 'resetMonitorActivity':
        $params = $_REQUEST;
        $result = $this->questionaries->resetMonitorActivity($params);
        echo json_encode($result);
        exit;
        break;
    case 'resetUserData':
        $params = $_REQUEST;
        $result = $this->questionaries->resetUserDataForTestingPurpose($params);
        echo json_encode($result);
        exit;
        break;
    case 'removeResetUser':
        $params = $_REQUEST;
        $result = $this->questionaries->removeResetUser($params);
        echo json_encode($result);
        break;
	case 'removecomplete':
        $params = $_REQUEST;
        $result = $this->questionaries->removecomplete($params);
        echo json_encode($result);
        break;
    case 'resetPassword':
        $params = $_REQUEST;
        $result = $this->questionaries->resetPassword($params);
        echo json_encode($result);
        break;
    case 'SaveAllAnswersQuestions':
        $params = $_REQUEST;
        $result = $this->questionaries->saveAllAnswersQuestions($params);
        echo json_encode($result);
        break;
    case 'default':
        break;
    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
