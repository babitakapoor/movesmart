<?php
error_reporting(1);
/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Member
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all member related ajax request.
 */
 require_once '../service/config.php';
try {
    global $isCronVarApi,$LANG;
    $isCronVarApi = 1;
    $method = $_REQUEST['action'];
    
    //validate method name
    if (empty($method)) {
        throw new Exception($LANG['errMethodNotSpecified']);  
    }
    switch ($method) {
    case 'memberListGrid':
	
        /* To List the Clubs in Dropdown */
            $userdetails = null;
            $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : 0;
        if ($userid > 0) {
            $userparam = array();
            $userparam['user_id'] = $userid;
            $jsonPersonalInfo = $this->coach->getUserPersonalInfoDetails($userparam);
            $userdetails = $jsonPersonalInfo['movesmart']['personalInfoDetails'];
			
			
        }//End				
            $companyid = (isset($userdetails) 
                && isset($userdetails['r_company_id'])) ? 
                $userdetails['r_company_id'] : COMPANY_ID;
            $params['company_id'] = $companyid;
            $params['is_deleted'] = 0;
            $params['authorizedClubId'] 
                = (isset($_SESSION['club']['authorizedClubId']) ? 
                $_SESSION['club']['authorizedClubId'] : '');
            $clubList = $this->club->getClubList($params);
            $param['userType'] = 'member';

            /*$userType : Param Types should be any one of these 
            all/employee/coach/admin/backOffice/member*/
            $param['loggedUserType'] = isset($userdetails) ? 
                $userdetails['r_usertype_id'] : -1;

            //To search param 
	
            $param['clubId'] = (isset($_REQUEST['clubId']) ? 
                $_REQUEST['clubId'] : '');
            $param['searchType'] = (isset($_REQUEST['searchType']) ? 
                $_REQUEST['searchType'] : '');
			$param['searchTypes'] = (isset($_REQUEST['searchTypes']) ? 
                $_REQUEST['searchTypes'] : '');
            $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
                ($_REQUEST['searchType'] == 'gender' ? 
                    (strstr($_REQUEST['searchValue'], 'f') ? 
                    '1' : '0') : $_REQUEST['searchValue']) : '');
            /*To sort param , If the param label field is empty / 
            default value should define at else part.*/
            $param['labelField'] 
                = (isset($_SESSION['pageName'][$_REQUEST['p']])) ? 
                $_SESSION['pageName'][$_REQUEST['p']] : 'user_id';
            $param['sortType'] 
                = (isset($_SESSION['pageName'][$_REQUEST['p']]) 
                    && $_SESSION['sortType'] == 2) ? 'desc' : 'asc';

            //Company id
            $param['companyId'] = $companyid;

            //Specific/Authorized clubs
            $param['authorizedClubId'] 
                = (isset($_SESSION['club']['authorizedClubId']) ?
                $_SESSION['club']['authorizedClubId'] : '');

            //Pagination code starts
            $limitStart = 0;
            $limitEnd = PAGINATION_SHOW_PER_PAGE;
            $currentpage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
            if (isset($_REQUEST['offset'])
                && ($_REQUEST['offset'] > 0)
            ) {
                $limitEnd = $_REQUEST['offset'];
            }
            if ($currentpage > 0) {
                $limitStart = ($currentpage - 1) * $limitEnd;
            }

            $param['limitStart'] = $limitStart;
            $param['limitEnd'] = $limitEnd;
            $tabs = isset($_GET['tab']) ? $_GET['tab'] : 'members_list';
			
            switch ($tabs) {
        case 'members_list':
            $arrayListCoach = $this->members->getUserList($param);
						
			
            break;
        case 'bodycomposition_test_todo':
            $arrayListCoach 
                = $this->members->getMembersBodycompositionTestToDo($param);
            break;
        case 'flexibility_test_todo':
            $arrayListCoach 
                = $this->members->getflexibilitycompostiontodo($param);
            break;
        case 'cardio_to_print_or_send':
            $arrayListCoach 
                = $this->members->getMembersToManyPointsThisWeek($param);
            break;
        case 'ergo_test_todo' :
            $arrayListCoach 
                = $this->members->getMembersErgoTestToDo($param);
            break;
        default:
            $arrayListCoach = array();
            $arrayListCoach['result'] = null;
            $arrayListCoach['totalCount'] = 0;
            break;
            }
            // Result set
            $arrayList = $arrayListCoach['result'];

            //Total count to create pagination
            $totalCount = $arrayListCoach['totalCount'];
			
            /*Get current link which is not consider 
            ajax page, ajax page should be 0*/
            $this->paginator->getUrlLink($param = 0); 

            //Pagination Class in body section
            if (isset($arrayList) && !empty($arrayList) 
                && isset($arrayList[0]['user_id'])
            ) {
                foreach ($arrayList as $arrayLists) {
                    $params['testStatus'] = 'completed';
                    $params['userId'] = $arrayLists['user_id'];
                    $lastTestId 
                        = $this->members->getMemberTestDetail($params);
                    if (isset($lastTestId['user_test_id'])) {
                        $userTestId = $lastTestId['user_test_id'];
                        echo '<input type="hidden" id="testID_'.
                        $params['userId'].'" value="'.$userTestId.'"/>';
                    }
                }
				
                echo $this->paginator->displayItemsPagination(
                    $arrayList,
                    array('user_id',
                            'person_id',
                            'first_name',
                            'last_name',
							'coach_name',
							'phone',
                            'gender',
                            'club_name',
                            'emailusername',
                            'specValues2' => '<button class="selectbtn" 
                            onclick="checkLastTestProgress(\'$user_id$\',this,1)" 
                            hrefValue="$user_id$">Select</button>', ),
                    array('startTag' => '<tr>', 'midTagOpen' => '<td>', 
                    'midTagClose' => '</td>', 'endTag' => '</tr>')
                );
            } else { ?>
                <tr><td colspan='20'><?php echo $LANG['noResult']?></td></tr>
            <?php }
            if (isset($arrayList) && !empty($arrayList) 
                && isset($arrayList[0]['user_id'])
            ) {
                $pages = ceil($totalCount / PAGINATION_SHOW_PER_PAGE);
                if ($pages > 1) { ?>
                    <tr class="borderradius">
                        <td colspan="9">
                            <div class="pagenation">
                                <button class="pageindex" page="members" pageindex="1"
                                        action_tab="<?php echo $tabs?>"
                                        <?php echo (($currentpage == 1) ? 'disabled="disabled"' : '')?>><<
                                </button>
                                <button class="pageindex" page="members"
                                        pageindex="<?php echo ($currentpage - 1);?>"
                                        action_tab="<?php echo $tabs;?>"
                                        <?php echo (($currentpage == 1) ? 'disabled="disabled"' : '')?>><
                                </button>
                                <ul>
                                    <?php
                    for ($pageindex = 1;$pageindex <= $pages;++$pageindex) { ?>
                        <li <?php echo (($pageindex == $currentpage)  ? 'class="active"' : '')?> >
                            <a href="javascript:void(0)" class="pageindex" page="members"
                                  pageindex="<?php echo $pageindex?>"
                                  action_tab="<?php echo $tabs?>">
                                  <?php echo $pageindex?>
                        </li>
                    <?php } ?>
                                </ul>
                                    <button class="pageindex" page="members"
                                            pageindex="<?php echo ($currentpage + 1) ?>"
                                            action_tab="<?php echo $tabs;?>"
                                        <?php echo (($currentpage == $pages) ? 'disabled="disabled"' : '')?>>>
                                    </button>
                                    <button class="pageindex" page="members"
                                            pageindex="<?php echo $pages?>"
                                            action_tab="<?php echo $tabs?>"
                                        <?php echo (($currentpage == $pages) ? 'disabled="disabled"' : '')?>>>>
                                    </button>
                                </div>
                        </td>
                    </tr>
                <?php } else { ?>
                    <tr><td colspan="9"></td></tr>
                <?php }
            }
        break;
    case 'assignDeviceMember':
        $param = array('deviceMemberId' => 
                $_POST['deviceMemberId'],
                'userId' => $_POST['userId']);
        $result = $this->members->assignDevice($param);
        echo json_encode($result);
        exit;
        break;
    case 'unlinkDeviceMember':
        $param = array('deviceMemberId' => $_POST['deviceMemberId'],
        'userId' => $_POST['userId']);
        $result = $this->members->unlinkDevice($param);
        echo json_encode($result);
        exit;
        break;
    case 'getSaveProfileImage':
        $param = array('userImage' => $_GET['userimage'],
                        'userId' => $_GET['userid']);
        $result = $this->members->getSaveProfileImage($param);
        echo json_encode($result);
        exit;
        break;
    case 'getPersonNumberExist':
        $valArr['personNumber'] = 1;
		
        if ($_REQUEST['personId']) {
            $param['personId'] = $_REQUEST['personId'];
            $result = $this->members->getPersonNumberExist($param);
		
            if ($result['movesmart']['status'] == 'Error') {
                $valArr['personNumber'] = 0;
            }
            //echo json_encode($result);
        }

        $valArr['email'] = 1;
        if ($_REQUEST['email']) {
            if ($_REQUEST['hiddenuserId']) {
                $param['userId'] = $_REQUEST['hiddenuserId'];
            }
            $param['email'] = $_REQUEST['email'];
            $param['password'] = $_REQUEST['password'];
            $param['username'] = $_REQUEST['username'];

            $result = $this->members->checkMemberEmailExist($param);
            if ($result['movesmart']['status'] == 'Error') {
                $valArr['email'] = 0;
            }
        }
        echo json_encode($valArr);
        exit;
        break;

    case 'getStartLevels':
        $params['weight'] = $_POST['weight'];
        $getStartLevels = $this->members->getStartLevels($params);
        echo $getStartLevels;
        exit;
        break;

    case 'lmoGetEmailExist'; 
        /* This case used to get existing 
        email list at the user registration*/
        $params['emailId'] = $_POST['emailId'];
        $result = $this->members->getExistingEmailList($params);
        echo json_encode($result);
        exit;
        break;

    case 'personIdExist':
        $param['userId'] = $_POST['userId'];
        $personIdExist = $this->members->personIdExist($param);
        echo $personIdExist;
        exit;
        break;

    case 'primaryClubUpdate':
        $param['userId'] = $_POST['userId'];
        $param['clubId'] = $_POST['clubId'];
        $param['employeeCenterId'] = $_POST['employeeCenterId'];
        $primaryClubUpdate = $this->members->primaryClubUpdate($param);
        echo $primaryClubUpdate;
        exit;
        break;

    case 'getMemberTestDetail':
        $param['userId'] = $_REQUEST['userId'];
        $param['testStatus'] = $_REQUEST['testStatus'];
        $result = $this->members->getMemberTestDetail($param);
        echo $result['user_test_id'];
        exit;
        break;

    case 'changeLabelSession':
        $param['labelField'] = $_REQUEST['labelField'];
        $linkData = $_REQUEST['linkData'];
        echo $_SESSION['pageName'][$linkData] = (isset($param['labelField']) 
            && $param['labelField'] != '') ? $param['labelField'] : '';
        echo $_SESSION['sortType'] = (isset($_SESSION['sortType']) 
            && $_SESSION['sortType'] == 1) ? 2 : 1;
        exit;
        break;

    case 'checkLastTestProgress':
        $param['userId'] = $_REQUEST['userId'];
        echo $result = $this->members->checkLastTestProgress($param);
        exit;
        break;
    case 'cancelMemberCurrentTest':
        $paramUser['userId'] = $_REQUEST['userId'];
        $memberTest = $this->members->getMemberMaxTestId($paramUser);
        $memberTest = $memberTest['rows'];
        $userTestId = $memberTest['user_test_id'];
        $result = array('status' => 'error');
        if ($userTestId > 0 && $memberTest['status'] = 3) {
            $params['userTestId'] = $userTestId;
            $params['status'] = 30;
            $params['reason'] = 'Cancelled from web application';
            $params['flagflagInterruptReset'] = 1;
            $status = $this->test->changeTestStatusToInterrupt($params);
            if ($status['testUpdateSuccess'] == 1) {
                //Change User Staus to "Test to do"
                $paramTest['userId'] = $paramUser['userId'];
                $paramTest['r_status_id'] = 5;
                $updateUser = $this->members->updateUserDetail($paramTest);

                $_SESSION['fl'] = array(1, $LANG['testDataDeleted']);
                $result = array('status' => 'success');
            }
            echo json_encode($result);
        }
        exit;
        break;
    case 'sendCardioPdfToMember';

        $userId = $_REQUEST['userId'];
        $userTestId = $_REQUEST['userTestId'];
        $paramsBasicInfo['userId'] = $userId;
        $paramsBasicInfo['testStatus'] = 3;
        $paramsBasicInfo['userTestId'] = $userTestId;
        $member = $this->members->getMemberBasicAndTestDetail($paramsBasicInfo);
        $testDate = date('d-m-Y', strtotime($member['test_start_date']));

        //Mail sender and receiver details
        $params['from'] = SEND_EMAIL_FROM;
        $params['fromName'] = SEND_EMAIL_FROM_NAME;
        $email = $member['email'];
        $params['to'] = $email;

        //Mail attachment details
        $fileName = '../tmppdf/'.$_REQUEST['fileName'];
        $params['attachment'] = $fileName;

        //Generate mail html content from template
        $bodyHtml = file_get_contents('../templates/sendcardiopdf.php');
        $memberName = $member['first_name'].' '.$member['last_name'];
        $companyName = $member['company_name'];
        $searchArr = array('MEMBER_NAME', 
            'REGARDS_SENDER_NAME', 'REGARDS_COMPANY_NAME');
        $replaceArr = array($memberName, 'Admin', $companyName);
        $bodyHtml = str_replace($searchArr, $replaceArr, $bodyHtml);

        $params['subject'] = 'Cardio Test Report - '.
        $member['club_name'].' - Test Date : '.$testDate;
        $params['message'] = $bodyHtml;
        //Send an email
        $res = array('status' => 'error');
        if ($this->common->sendEmail($params)) {
            unlink($fileName);
            $res = array('status' => 'success');
        }
        echo json_encode($res);
        break;

    case 'testResultsListDetail':
        $userId = isset($_GET['id']) ? $_GET['id'] : '';
        $paramUser['userId'] = $userId;
        $paramUser['authorizedClubId'] = $_REQUEST['authorizedClubId'];
        //Get user basic detail
        $memberBasicDetail = $this->members->getMemberBasicDetail($paramUser);

        //Get test and clubId, If isset check with user and retrieve the value
        $paramUser['testAndClub'] = 1;
        $arrayListMember = $this->test->getTestResultMemberList($paramUser);
        $arrayList = $arrayListMember['result'][0];
        $param['userId'] = $userId;

        //Get test result data
        $param['getTestDetail'] = 1;
        $param['testId'] = (isset($_GET['testId'])) ? 
            $_GET['testId'] : $arrayList['last_test_id'];
        $memberTestResultArr = $this->test->getTestResultInfo($param);
        //var_dump($memberTestResultArr);
        $sportmedBasic 
            = json_decode($memberTestResultArr['sportmed_graph'], true);
        $sportmedBasic = $sportmedBasic['result'];
        $sportmedAdjusted 
            = json_decode($memberTestResultArr['adjust_sportmed_graph'], true);
        $sportmedValues = $sportmedAdjusted['result'];
        if (!count($memberTestResultArr)) {
            //exit();
        }
        $urlNavigation = 'index.php?p=testResultsListDetail';
        $param['testId'] = $arrayList['last_test_id'];
        $param['userId'] = $userId;

        //Get user basic and test detail
        $param['userId'] = $_GET['id'];
        $currentTestId = $param['testId'];

        //Get test member navigation begins
        $paramTest['testStatus'] = '3';
        $paramTest['authorizedClubId'] = isset($_REQUEST['authorizedClubId']) ?
            $_REQUEST['authorizedClubId'] : '';
        $testMemberNavigation = $this->test->getTestMemberIds($paramTest);
        $navArrMember = array();
        if (is_array($testMemberNavigation)) {
            foreach ($testMemberNavigation as $row) {
                $navArrMember[] = $row['r_user_id'];
            }
            $navigation = $this->common->getNavIds($navArrMember, $_GET['id']);
        }

        //Get test member navigation end
        //get sport specific data
        $paramS['userTestId'] = $arrayList['last_test_id'];
        $paramS['userId'] = $_GET['id'];
        $sportSpecificData = $this->test->getSportSpecificData($paramS);

        $sportSpecific 
            = json_decode(
                $sportSpecificData['rows']['sportspecific_data'],
                true         
            );
        $testResultPdfFlag = isset($sportSpecific['result']['programs']) ? 1 : 0;
        echo json_encode($memberTestResultArr);
        break;
    case 'sendForgotPasswordLink':
        $params = $_REQUEST;
        $params['randStr'] = $this->common->randStr(35);
        //Update email key details 
        $result = $this->members->UpdateForgotPasswordDetails($params);
        //Send Email to user

        $response = array('status' => 'error');
        if ($result['status'] == 'success') {
            $user = $result['user'];
            $email['from'] = SEND_EMAIL_FROM;
            $email['fromName'] = SEND_EMAIL_FROM_NAME;
            $email['to'] = $params['email'];
            $email['subject'] = 'Password reset user';
            $body = 'Hi '.$user['first_name'].' '.$user['last_name'].',<br />';
            $body .= 'your password is "'.$user['password'].'"';
            $body .= '<br><br>Regards,<br> Movesmart Team';
            $email['message'] = $body;
            if ($this->common->sendEmail($email)) {
                $response = array('status' => 'success');
            }
        }
        echo json_encode($response);
        //To Create Log File
        $paramLog['webServiceUrl'] = $_SERVER['REQUEST_URI'];
        $paramLog['response'] = $result;
        $paramLog['mod'] = 'log';
        $paramLog['method'] = 'logServices';
        $paramLog['reference'] = 'UpdateForgotPasswordDetails';
        $paramLog['r_company_id'] = $parameter['companyId'];
        $paramLog['r_club_id'] = $parameter['clubId'];
        $paramLog['type'] = 'IOS';// PS/IOS/CCA
        $this->common->webServiceXMLToString(
            WEBSERVICE_PATH.QN.http_build_query($paramLog)
        );
        break;
    case 'testResult':
        //$titleName = $LANG['testResult'];
        $getCurrtentTestStatusMsg   =   array();
        $memberTestResultArr   =   array();
        $getPersonalInfo   =   array();
        $memberBasicDetail =   array();
        if (isset($_REQUEST['id'])) {
            $userId = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $paramUser['userId'] = $userId;
            //Get user basic detail							
            $memberBasicDetail 
                = $this->members->getMemberBasicAndTestDetail(
                    $paramUser
                );
            $param['user_id'] = $userId;
            $getPersonalInfo = $this->members->getListEditMembers($param);
            $param['userId'] = $userId;

            //Get the Member Test Result Data
            $param['testStatusNotIn'] = '3,30,50';
            $memberTestResultArr = $this->test->getTestResultInfo($param);
            $response = array();
            $response['status'] = 0;
            if (!count($memberTestResultArr)) {
                $response['status'] = 0;
                echo json_encode($response);
                exit;
            }
            if (isset($_REQUEST['newTest'])) {
                $memberTestResultArr['test_date'] = date('d-m-Y');
            }
            $showTestCancelButton = array(0, 1, 2, 10, 20);
            if (in_array(
                $memberTestResultArr['current_test_status'], 
                $showTestCancelButton
            )) {
                $showTestCancelButton = 1;
            }
            $testStatus['current_test_status'] 
                = $memberTestResultArr['current_test_status'];
            $getCurrtentTestStatusMsg 
                = $this->test->getUserTestStatusMessage($testStatus);
            if ($getCurrtentTestStatusMsg == '') {
                $getCurrtentTestStatusMsg = 'Completed';
            }
            /*This is temporary fix suggested by 
            testing team, will be removes as per future discussion*/
            $makeStopAndResetStatus = array(1, 2, 10, 20);
            if (in_array(
                $memberTestResultArr['current_test_status'], $makeStopAndResetStatus
            )) {
                $makeStopAndReset = 1;
            }
            if ($memberTestResultArr['user_test_id'] == '') {
                $showTestCancelButton = 0;
            }
        }
            echo json_encode(
                array('status' => 1,
                'memberDetails' => $memberBasicDetail,
                'getPersonalInfo' => $getPersonalInfo,
                'memberTestResultArr' => $memberTestResultArr,
                'getCurrtentTestStatusMsg' => $getCurrtentTestStatusMsg, )
            );
            //echo json_encode($memberTestResultArr);
        break;
    case 'getUserImage':
        if (isset($_REQUEST['id'])) {
            /*Passing user Id to get Member Info from class.*/
            $userId = $_REQUEST['id'];
            $param['user_id'] = $userId;
            $arrayList = $this->members->getListEditMembers($param);
            $file = isset($arrayList['userimage']) ? $arrayList['userimage'] : '';
            if (!file_exists(IMAGE_PROFILE_PATH.$file)) {
                $file = '';
            }
            $path = $file;
           
            echo json_encode($path);
        }
        break;
    case 'registerClient':
	
      
        $userdata = array();
        $first_name = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '';
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $phno = isset($_REQUEST['phno']) ? $_REQUEST['phno'] : '';
        $gender = isset($_REQUEST['gender']) ? $_REQUEST['gender'] : '';
        $imgval = isset($_REQUEST['imgval']) ? $_REQUEST['imgval'] : '';
        $regdob = isset($_REQUEST['regdob']) ? $_REQUEST['regdob'] : '';
        $logintype = isset($_REQUEST['logintype']) ? $_REQUEST['logintype'] : 1;
        $loginreffieldid = isset($_REQUEST['loginreffieldid']) ? $_REQUEST['loginreffieldid'] : 1;  
        $requestPassword = isset($_REQUEST['requestPassword']) ? $_REQUEST['requestPassword'] : '';
        $language = isset($_REQUEST['language']) ? $_REQUEST['language'] : '3';    
        $param['first_name'] = $first_name;
     
        /**************************
        Defining the static id
        ***************************/
        //$loginreffieldid = 88899;
        $param['name'] = $name;
        $param['email'] = $email;
        $param['phno'] = $phno;
        $param['gender'] = $gender;
        $param['imgval'] = $imgval;
        $param['regdob'] = $regdob;
        $param['password'] = $requestPassword;//$this->common->randStr(8);
        $param['usertypeid'] = 3;
        $param['logintype'] = $logintype;
        $param['loginreffieldid'] = $loginreffieldid;
        $param['language'] = $language;
		$param['register_date'] = date('Y-m-d h:i:s');
        /*SN added 20160203*/
      //  $param['statusid'] = USER_STATUS_InACTIVE;//MK Default to be inactive
	  

	   if($param['logintype']==2){
		   
		   $param['statusid'] = 1;
	   }
	   else{
	   $param['statusid'] = 2;
	   }
        $param['companyId'] = isset($_REQUEST['companyId']) ? 
        $_REQUEST : REGISTER_CLIENT_COMPANY;
       
        $result = $this->members->signupClient($param);
		// use for flogin if email already exist
	     $response = array();
		 
         if(($param['logintype']==2) && (isset($result['movesmart']['fblogin'])) && ($result['movesmart']['fblogin']==1))
		 {   
		
		
			 $userParam['userId'] = $result['movesmart']['user_id']; 
	
			 $userdata = $this->members->getMemberBasicDetailbyfb($userParam);
			  
			 $response['data'] = $userdata['userDetails'];
             $response['status'] = $userdata['status_code'];
			 $result['movesmart']['status']='';
		 }
		
        //$response['success'] = 1;
        //$response['message'] = 'User registerated successfully';
        if (isset($result['movesmart']) 
            and (isset($result['movesmart']['insertid']))
        ) 
		{
            $response['signup_user'] = $result['movesmart']['insertid'];
        }
		
		
        if ($result['movesmart']['status'] == 'Error') {
			
            $response['success'] = 0;
			$response['message']=$result['movesmart']['status_message'];
        } else {
			if($param['logintype']!=2){
            if ($loginreffieldid == '') {
				
                $insert_id = $result['movesmart']['insertid'];
                $userParam['userId'] = $insert_id;
				
                $userdata = $this->members->getMemberBasicDetail($userParam);

                $paramsemail = array();
                $paramsemail['from'] = SEND_EMAIL_FROM;
                $paramsemail['fromName'] = SEND_EMAIL_FROM_NAME;
                $paramsemail['to'] = $param['email'];
                $paramsemail['subject'] = 'Movesmart - Activation Link';
				
                /*Direct Link Added for the Quick Fix need Changes 
                on -Next Hub- Works*/
                $imgtoplogo = BASE_URL.'images/mailtoplogo.png';
                $imgbottomlogo = BASE_URL.'images/mailbottomlogo.png';
                $msg    =   "Dankjewel voor je registratie bij bij FeelGoodClub";
                /**/
                $actlink    =   BASE_URL."ajax/?p=members&action=activationfrommail&activeref=".$insert_id;
                $bodyhtml   =   file_get_contents("../templates/email/useractivation.html");
                $bodyhtml   =   str_replace('{{EMAILTOPLOGO}}',$imgtoplogo,$bodyhtml);
                $bodyhtml   =   str_replace('{{FIRSTNAME}}',$param['first_name'],$bodyhtml);
                $bodyhtml   =   str_replace('{{LASTNAME}}',$param['name'],$bodyhtml);
                $bodyhtml   =   str_replace('{{MESSAGE}}',$msg,$bodyhtml);
                $bodyhtml   =   str_replace('{{EMAIL}}',@$userdata['email'],$bodyhtml);
                $bodyhtml   =   str_replace('{{PASSWORD}}',@$userdata['password'],$bodyhtml);
                $bodyhtml   =   str_replace('{{ACTIVATIONLINK}}',$actlink,$bodyhtml);
                $bodyhtml   =   str_replace('{{EMAILBOTTOMLOGO}}',$imgbottomlogo,$bodyhtml);
				
                /**/
                /*
                $body = "<div style='padding: 0 10px' >
                <img src='".$imgtoplogo."' alt='logo' /></div><br><br>";
                $body .= 'Dear '.$param['first_name'].' '.$param['name'].',<br><br>';
                $body .= '<br>Thanks for your registration at MOVESMART.company,';
                $body .= '<br><br>Login :'.trim($param['email']).' <br>';
                //$body .='Password :'.trim($param['password']).' <br><br><br>';
                $activationlink = '<a href="'.BASE_URL.'ajax/?p=members&action=';
                $activationlink .= 'activationfrommail&activeref='.$insert_id.'">';
                //Need To Encrypt with the Algorithm
                $activationlink .= 'MOVESMART</a>';
                $body .= 'Please click on the following link to acitivate ';
                $body .= $activationlink.'<br>With kind Regards,<br><br>';
                $body .= 'MOVESMART.company<br>T.+32(0)468 1941 85<br>';
                $body .= "M.<a href='mailto:support@movesmart.company'>";
                $body .= "support@movesmart.company</a><br><br><div >";
                $body .= "style='padding:0 10px'<img src='".$imgbottomlogo."'";
                $body .= " alt='eatfreshicon' /></div>MOVESMART.company BV<br>";
                $body .= 'Kwelkade 1 - 4001 RK Tiel<br>';
                $body .= "M.<a href='http://www.movesmart.company/'>";
                $body .= "www.MOVESMART.COMPANY</a><br><br><br>";
                /**/
                //$paramsemail['message'] = $body;
                $paramsemail['message'] = $bodyhtml;
				 $response=$this->common->sendEmail($paramsemail);
				 
                if ($this->common->sendEmail($paramsemail)) {
                    $response['success'] = 1;
                    $response['message'] = 'User registerated. Password sent to mail successfully';
					//$userdata = $this->members->getMemberBasicDetail($userParam);
					//$response['data'] = $userdata;
                }
            } else {
				
                $userdata = $this->members->getMemberByNameSecurityAnswer($param);
				  $response['data'] = $userdata['userDetails'];
                $response['status'] = $userdata['status_code'];
				if($param['logintype']!=2){
				 $paramsemail = array();
                $paramsemail['from'] = SEND_EMAIL_FROM;
                $paramsemail['fromName'] = SEND_EMAIL_FROM_NAME;
                $paramsemail['to'] = $param['email'];
                $paramsemail['subject'] = 'Movesmart - Activation Link';
				
                /*Direct Link Added for the Quick Fix need Changes 
                on -Next Hub- Works*/
                $imgtoplogo = BASE_URL.'images/mailtoplogo.png';
                $imgbottomlogo = BASE_URL.'images/mailbottomlogo.png';
                $msg    =   "Dankjewel voor je registratie bij FeelGoodClub";
                /**/
                $actlink    =   BASE_URL."ajax/?p=members&action=activationfrommail&activeref=".$userdata['userDetails']['user_id'];
                $bodyhtml   =   file_get_contents("../templates/email/useractivation.html");
                $bodyhtml   =   str_replace('{{EMAILTOPLOGO}}',$imgtoplogo,$bodyhtml);
                $bodyhtml   =   str_replace('{{FIRSTNAME}}',$param['first_name'],$bodyhtml);
                $bodyhtml   =   str_replace('{{LASTNAME}}',$param['name'],$bodyhtml);
                $bodyhtml   =   str_replace('{{MESSAGE}}',$msg,$bodyhtml);
                $bodyhtml   =   str_replace('{{EMAIL}}',@$userdata['userDetails']['email'],$bodyhtml);
                $bodyhtml   =   str_replace('{{PASSWORD}}',@$userdata['userDetails']['password'],$bodyhtml);
                $bodyhtml   =   str_replace('{{ACTIVATIONLINK}}',$actlink,$bodyhtml);
                $bodyhtml   =   str_replace('{{EMAILBOTTOMLOGO}}',$imgbottomlogo,$bodyhtml);
                /**/
               $paramsemail['message'] = $bodyhtml;
				 //$respons=$this->common->sendEmail($paramsemail);
				 
                if ($this->common->sendEmail($paramsemail)) {    
                    $respons['success'] = 1;
                    $respons['message'] = 'User registerated. please activate account ';
					//$userdata = $this->members->getMemberBasicDetail($userParam);
					//$response['data'] = $userdata;
                }
				}
			  
            }
		 }
        }
		
        echo json_encode($response);
        break;
    case 'submitBodyCompositionTest': 
   
        /* To Add Testing measuring body vomposition test data */
        $param = $_POST;
        $result = $this->testingAndMeasuring->insertBodyCompositionTestData($param);
        if ($result['status'] == 'success') {
            $result['status_code'] = 1;
            $_SESSION['fl'] = array(1, $LANG['msgDetailsupdateSuccess']);
        } else {
            $result['status_code'] = 0;
        }
        echo json_encode($result);
        break;
    case 'fetchTestMethodologies':
        $param = $_POST;
        $bodyCompMethds 
            = $this->admin->getActiveBcompMethods($param);
        echo json_encode($bodyCompMethds);
        break;
    case 'submitFlexiblityTest':
        $param = $_POST;
       
        $flexblityttstdata 
            = $this->testingAndMeasuring->insertFlexibilityTestData($param);
        echo json_encode($flexblityttstdata);
        break;
    case 'updateTestResult':
        $param = $_REQUEST;
        printTestLog('/test params second time/');
		printTestLog($param);
		/* changing the Date settings in save format */
        /*XX $this->common->siteSaveDateFormat($_REQUEST['testDate']); 
        Changed by sankar since it is already sending it in correct format*/
        //mail('babitakapoor.immanentsolutions@gmail.com','test',print_r($param,true));
        $testDate = $_REQUEST['testDate'];
        $param['newTestDate'] = $testDate;
        $param['testDate'] = $testDate;
        $param['testPrepared'] = $testDate;

        /* save the Test Result */
        $weight = $_REQUEST['weightOnTestDate'];
        $testLevel = $_REQUEST['testLevel'];

        $paramTest['userId'] = $_REQUEST['currentuserId'];
        $testDetail = $this->members->getMemberTestDetail($paramTest);
    
        //Change User Staus to "Test to do"
        if ($_REQUEST['newTest'] == '1') 
        {
            $paramTest['r_status_id'] = USER_STATUS_TEST_TODO;
            $updateUser = $this->members->updateUserDetail($paramTest);
        }

        $param['userTestId'] = (isset($testDetail) && isset($testDetail['user_test_id'])) ? $testDetail['user_test_id'] : '';
        $param['testLevel'] = $testLevel;
        $result = $this->test->saveTestResult($param);   
        
        //echo $result['userTestId'];
        if ($result['userTestId']) 
        {
			$userTestId = $result['userTestId'];
			$params['memberId'] = $_REQUEST['currentuserId'];
            $params['testLevel'] = $testLevel;
			$params['testOptions'] = $_REQUEST['testOptions'];
            $params['testDate'] = $testDate;
            $params['userTestId'] = $userTestId;
            $params['weight'] = $weight;
            $params['getLoadValues'] = true;
            $params['testName'] = $_REQUEST['testName'];
	
            if (isset($_REQUEST['testStatus'])) {
                $params['status'] = $_REQUEST['testStatus'];  
            }
            $memberDetails = $this->test->insertMemberForTest($params);
            /* To Save Test Parameters Values */
            $param['userTestId'] = $userTestId;  

            $result = $this->test->saveTestParameters($param);
            $_SESSION['fl'] = array(1, $LANG['msgDetailsupdateSuccess']);

            if ((!isset($params['loadValue'])) || ($params['loadValue'] == '')) {
                $_SESSION['fl'] = array(1, $LANG['errLoadValueEmpty']);
            }
        }
       
        echo json_encode($result);
        break;
    case 'fetchTestTypes':
        $params = $_REQUEST;
        $bodyCompMethds = $this->admin->getActiveBcompMethods($params);
        echo json_encode($bodyCompMethds);
        break;
    /*case 'getCurrentTestCount':
        $params = $_REQUEST;
        $testcount = $this->members->getCurrentTestCount($params);
        $getUserDataalevel = $this->members->getUserDataalevelbydesc($params);
        $getUserDataalevelasc = $this->members->getUserDataalevelbyasc($params);
        $getFatpercentLevelbydesc
            = $this->members->getFatpercentLevelbydesc($params);
        $getFatpercentLevelbyasc = $this->members->getFatpercentLevelbyasc($params);
        $response = array();
        if (($getUserDataalevel['movesmart']['status_code'] == 200)
            and ($testcount['movesmart']['status_code'] == 200)
            and ($getUserDataalevelasc['movesmart']['status_code'] == 200)
            and ($getFatpercentLevelbydesc['movesmart']['status_code'] == 200)
            and ($getFatpercentLevelbyasc['movesmart']['status_code'] == 200)
        ) {
            $Currentfitlevel = $getUserDataalevel['movesmart']['count']['fitlevel'];
            $Currentweight = $getUserDataalevel['movesmart']['count']['weight'];

            $lastfitlevel = $getUserDataalevelasc['movesmart']['count']['fitlevel'];
            $lastweight = $getUserDataalevelasc['movesmart']['count']['weight'];

            $fatpercentlevel_first
                = ($getFatpercentLevelbyasc['movesmart']['status_code'] == 200) ?
                $getFatpercentLevelbyasc['movesmart']['count'] : array();
            $fatpercentlevel_last
                = ($getFatpercentLevelbydesc['movesmart']['status_code'] == 200) ?
                $getFatpercentLevelbydesc['movesmart']['count'] : array();
            $response = array('status' => 1,
                'status_message' => 'Result got successfully',
                'testcount' => $testcount, 'Currentfitlevel' => $Currentfitlevel,
                'Currentweight' => $Currentweight, 'lastfitlevel' => $lastfitlevel,
                'lastweight' => $lastweight,
                'fatpercent_latest' => $fatpercentlevel_last,
                'fatpercent_init' => $fatpercentlevel_first);
        } else {
            $response = array('status' => 0, 'status_message' => 'No test found');
        }
        echo json_encode($response);
        break;
   */
    case 'saveStrengthTest':
        $param = $_REQUEST;
        $params['hiddenUserId'] = $param['userId']; //userid
        $params['coach_id'] = $param['coach_id'];  //coachid,logid
        $params['weight'] = $param['weight']; //coachid,logid
        $params['height'] = $param['height'];  //coachid,logid
        $params['r_strength_user_test_type'] = $param['testoptiontype'];
        /* static data**/
        $params['test_status'] = 1;
        $params['strengthProgramId'] = 0;
        $params['testValuesCalculation'] = 0;
        $params['testOption'] = 1;
        $insertStrengthTest = $this->strength->insertUpdateUserTest($params);
        echo json_encode($insertStrengthTest);
        break;
    case 'clientMappedDetails':
        $params = $_REQUEST;
        $mappedetails = $this->members->clientMappedDetails($params);
        echo json_encode($mappedetails);
        break;
        /* SK Added, To handel latest test details of body composition,flexiblity 
        created ON : 09-02-2016
        */
    case 'getLastTestResultdata':
        $params = $_REQUEST;
        $lastTestDetail = $this->members->getLastTestDetails($params);
        $testresult = array();
        if (isset($lastTestDetail['status_code']) 
            and ($lastTestDetail['status_code'] == 200)
        ) {
            $testresult 
                = isset($lastTestDetail['testrecord']) ? 
                $lastTestDetail['testrecord'] : array();
        }
        echo json_encode($testresult);
        exit;
        break;
    case 'getMembersTestResultdataForClubandDate':
        
	    $_REQUEST['isActive'] ='0,1';	
		$params = $_REQUEST;
		
        $companyid = isset($userdetails) ? 
            $userdetails['r_company_id'] : COMPANY_ID;
        $params['companyId'] = $companyid;
        $lastTestDetail 
            = $this->members->getMembersTestResultdataForClubandDate($params);
        $testresult = array();
        if (isset($lastTestDetail['status_code']) 
            and ($lastTestDetail['status_code'] == 200)
        ) {
            $testresult = isset($lastTestDetail['testrecord']) ? 
                $lastTestDetail['testrecord'] : array();
        }
		
        $testresult = isset($testresult[0]) ? $testresult : array($testresult);
	
		header('Content-Type: application/json');
        echo json_encode($testresult);
        exit;
        break;
    case 'getCardioTestDetailByID':
        $params = $_REQUEST;
        $lastTestDetail = $this->members->getCardioTestByTestID($params);
        $testresult = array();
        if (isset($lastTestDetail['status_code']) 
            and ($lastTestDetail['status_code'] == 200)
        ) {
            $testresult = isset($lastTestDetail['testrecord']) ? 
                $lastTestDetail['testrecord'] : array();
        }
		header('Content-Type: application/json');
        echo json_encode($testresult);
        exit;
    case 'CardiotestMembers':
        $params = $_REQUEST;
        $companyid = isset($userdetails) ? 
            $userdetails['r_company_id'] : COMPANY_ID;
        $params['companyId'] = $companyid;
        $limitStart = 0;
        $limitEnd = PAGINATION_SHOW_PER_PAGE;
        $currentpage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
            $limitEnd = $_REQUEST['offset'];
        }
        if ($currentpage > 0) {
            $limitStart = ($currentpage - 1) * $limitEnd;
        }
        /*$params['limitStart'] = $limitStart;
        $params['limitEnd'] = $limitEnd; */
        $params['labelField'] = (isset($_SESSION['pageName'][$_REQUEST['p']])) ?
            $_SESSION['pageName'][$_REQUEST['p']] : 'user_id';
        $params['sortType'] 
            = (isset($_SESSION['pageName'][$_REQUEST['p']]) 
            && $_SESSION['sortType'] == 2) ? 'desc' : 'asc';
        $params['loggedUserType'] = 1;
        //Need to send From Application Later
        $params['testDate'] = isset($_REQUEST['testDate']) ? 
            $_REQUEST['testDate'] : date('Y-m-d');

        $arrayListCoach = $this->members->cardioTodoMembers($params);
        $testresult = array();
        if (isset($arrayListCoach['result']) 
            and is_array($arrayListCoach['result']) 
            and (!empty($arrayListCoach['result'][0])) 
            and (count($arrayListCoach['result']) > 0)
        ) {
            $userids = '';
            foreach ($arrayListCoach['result'] as $member) {
                if (is_array($member)) {
                    $userids .= $member['user_id'].',';
                }
            }
            if ($userids != '') {
                $cardiotestparams = array('testDate' => $params['testDate']);
                $cardiotestparams['userId'] = rtrim($userids, ',');
                $cardiotestparams['status'] = $_REQUEST['status'];
                $userTestDetail 
                    = $this->members->CardiotestDetails($cardiotestparams);
                $testresultobjs = array();
                if (isset($userTestDetail['result']) 
                    and ($userTestDetail['result'] != '')
                ) {
                    //echo '2'.$userTestDetail['result'];
                    $testresultobjs = isset($userTestDetail['result'][0]) ? 
                    $userTestDetail['result'] : array($userTestDetail['result']);
                }
                //echo json_encode($testresultobjs);
                foreach ($arrayListCoach['result'] as $member) {
                    $member['cardiotest_info'] = array();
                    foreach ($testresultobjs as $testresultobj) {
                        if ($member['user_id'] == $testresultobj['userid']) {
                            $member['cardiotest_info'] = $testresultobj;
                        }
                    }
                    $testresult[] = $member;
                }
            }
        }
		header('Content-Type: application/json');
        echo json_encode($testresult);
        exit;
                    break;
    case 'getMemberTestTobeAnalysed':
        $params = $_REQUEST;
		
        $result = $this->members->getMemberTestTobeAnalysed($params);
        $response = array();
        if ($result['status_code'] == 200) {
            $response = ($result['recordcount'] == 1) ? 
                array($result['testrecord']) : $result['testrecord'];
        }
		//header('Content-Type: application/json');
        echo json_encode($response);
        exit;
                    break;
    case 'getclubnamedetails':
        $param['clubId'] = $_REQUEST['clubid'];
        $getClubDetail = $this->club->getClubDetailById($param);
        $clubDetails = $getClubDetail['rows'];
		header('Content-Type: application/json');
        echo json_encode($clubDetails);
        break;
                /*SK Added fot getting Machines list of details*/
    case 'ListMachinesData':
        $param = $_REQUEST;
        $type = isset($_POST['listType']) ? $_POST['listType'] : 'l';
        $userdetails = null;
        $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : 0;
        if ($userid > 0) {
            $userparam = array();
            $userparam['user_id'] = $userid;
            $jsonPersonalInfo 
                = $this->coach->getUserPersonalInfoDetails($userparam);
            $userdetails 
                = $jsonPersonalInfo['movesmart']['personalInfoDetails'];
        }//End
        $companyid = isset($userdetails) ? $userdetails['r_company_id'] : COMPANY_ID;
        $params['company_id'] = $companyid;
        $params['is_deleted'] = 0;
        $params['authorizedClubId'] 
            = (isset($_SESSION['club']['authorizedClubId']) ?
            $_SESSION['club']['authorizedClubId'] : '');
        $clubList = $this->club->getClubList($params);
        $param['userType'] = 'member';

        /*$userType : Param Types should be any one of these 
        all/employee/coach/admin/backOffice/member*/
        $param['loggedUserType'] = isset($userdetails) ? 
            $userdetails['r_usertype_id'] : -1;

        //To search param 	
        $param['clubId'] = (isset($_REQUEST['clubId']) ? 
            $_REQUEST['clubId'] : '');
        $param['searchType'] = (isset($_REQUEST['searchType']) ? 
            $_REQUEST['searchType'] : '');
        $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
            ($_REQUEST['searchType'] == 'gender' ? 
                (strstr($_REQUEST['searchValue'], 'f') ? 
                    '1' : '0') : $_REQUEST['searchValue']) : '');

        /*To sort param , If the param label field is empty / 
        default value should define at else part.*/
        $param['labelField'] 
            = (isset($_SESSION['pageName'][$_REQUEST['p']])) ? 
                $_SESSION['pageName'][$_REQUEST['p']] : 'user_id';
        $param['sortType'] 
            = (isset($_SESSION['pageName'][$_REQUEST['p']]) 
                && $_SESSION['sortType'] == 2) ? 'desc' : 'asc';

        //Company id
        $param['companyId'] = $companyid;

        //Specific/Authorized clubs
        $param['authorizedClubId'] 
            = (isset($_SESSION['club']['authorizedClubId']) ? 
            $_SESSION['club']['authorizedClubId'] : '');

        //Pagination code starts
        $limitStart = 0;
        $limitEnd = PAGINATION_SHOW_PER_PAGE;
        $currentpage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
            $limitEnd = $_REQUEST['offset'];
        }
        if ($currentpage > 0) {
            $limitStart = ($currentpage - 1) * $limitEnd;
        }
        $param['limitStart'] = $limitStart;
        $param['limitEnd'] = $limitEnd;
        $subtabs = isset($_GET['tab']) ? $_GET['tab'] : 'manage_machine_list';
        switch ($subtabs) {
        case 'manage_machine_list':
            $listmachinedata = $this->members->listMachinesData($param);
            break;
        case 'Machines':
            $param['groupID'] = 1;
            $listmachinedata = $this->members->listMachinesData($param);
            break;
        case  'Activities' :
            $param['groupID'] = 3;
            $listmachinedata = $this->members->listMachinesData($param);
            break;
        case 'Dumbells':
            $param['groupID'] = 4;
            $listmachinedata = $this->members->listMachinesData($param);
            break;
        case 'Exercises':
            $param['groupID'] = 2;
            $listmachinedata = $this->members->listMachinesData($param);
            break;
        default:
            $listmachinedata = array();
            $listmachinedata['result'] = null;
            $listmachinedata['totalCount'] = 0;
            break;
        }
        $statuslist 
            = ($listmachinedata['movesmart']['error_code'] == 200) ? 1 : 0;
        $HTMLList = '';
        $HTMLGrid = '<div class="exercises_listbx" ><ul>'; // Grid paginationchnages
        if ($statuslist) {
            $arrayList = array();
            $mechinelist = $listmachinedata['movesmart']['data'];
            $totalCount = $listmachinedata['movesmart']['count'];
            $rowscount = $listmachinedata['movesmart']['rows'];
            if ($rowscount == 1) {
                $arrayList = array($mechinelist);
            } else {
                $arrayList = $mechinelist;
            }
            $this->paginator->getUrlLink($param = 0);
            if (isset($arrayList) && !empty($arrayList)) {
                if ($type == 'l') {
                    $ImagePath = SERVICE_MACHINE_IMAGEURL_WEB;
                    //onclick="editmachinetestdetails($machineid$)";
                    $HTMLList .= $this->paginator->displayItemsPagination(
                        $arrayList,
                        array('specValues' => '<span class="checkouter">
                            <input type="checkbox" class="machinedetails" 
                            val="$machineid$" name="machinedetails_$machineid$"   
                            hrefValue="$machineid$"></span>',
                            'clubname',
                            'nameofmachine',
                            'brand_name',
                            'type',
                            'groupname',
                            'subtype',
                            'path' => '<img src="'.$ImagePath.''.'$image$'.
                            '" width=25px; height=25px;>',
                            'status',
                            'specValues2' => '<button class="selectbtn" 
                            onclick="machinetestdetails('.$machineid.')" 
                            hrefValue="$machineid$">Select</button>', ),
                        array('startTag' => '<tr>', 'midTagOpen' => '<td>', 
                        'midTagClose' => '</td>', 'endTag' => '</tr>')
                    );
                }
            } else {
                $HTMLList .= "<tr><td colspan='20'>No result found</td></tr>";
            }

            if (count($arrayList) > 0) {
                foreach ($arrayList as $row) {
                    $imageurl 
                        = ($row['image'] != '') ? 
                        SERVICE_MACHINE_IMAGEURL_WEB.$row['image'] : 
                        'images/no_image.png';
                    $HTMLGrid .= '<li><span class="checkouter ">
                    <input type="checkbox" class="machinedetails" 
                    val="'.$row['machineid'].'" name="machinedetails_'.
                    $row['machineid'].'"   hrefValue="'.$row['machineid'].
                    '" /></span><img alt="exer_images1" src="'.$imageurl.
                    '" id="idgridlistimages" attr="'.$row['machineid'].
                    '"><h4>'.$row['nameofmachine'].'</h4></li>';
                }
            } else {
                $HTMLGrid .= '<li>No Result Found</li>';
            }
            if (isset($arrayList) && !empty($arrayList)) {
                $pages = ceil($totalCount / PAGINATION_SHOW_PER_PAGE);
                if ($pages > 1) {
                    $HTMLList .= '<tr class="borderradius">
                                    <td colspan="9">
                                        <div class="pagenation">
                                           <button class="pageindex" page="members" 
                                           pageindex="1" action_tab="'.$subtabs.'" '.
                                            (($currentpage == 1) ? 
                                            'disabled="disabled"' : '').'><<</button>
                                            <button class="pageindex" 
                                           page="members" pageindex="'.
                                           ($currentpage - 1).
                                            '" action_tab="'.$subtabs.'" '.
                                            (($currentpage == 1) ? 
                                            'disabled="disabled"' : '').
                                            '><</button>
                                        <ul>';
                    for ($pageindex = 1;$pageindex <= $pages;
                        ++$pageindex) {
                        $HTMLList .= '<li '.
                                            (($pageindex == $currentpage)  ? 
                                            'class="active"' : '').
                                            '><a href="javascript:void(0)"  
                                            class="pageindex" page="members" 
                                            pageindex="'.$pageindex.'" 
                                            action_tab="'.$subtabs.'">'.
                                            $pageindex.'</li>';
                    }
                    $HTMLList .= '      </ul>
                                           <button class="pageindex" page="members" 
                                           pageindex="'.($currentpage + 1).'" 
                                           action_tab="'.$subtabs.'" '.
                                           (($currentpage == $pages) ? 
                                           'disabled="disabled"' : '').'>></button>
                                           <button class="pageindex" page="members" 
                                           pageindex="'.$pages.'" action_tab="'.
                                           $subtabs.'" '.(($currentpage == $pages) ? 
                                           'disabled="disabled"' : '').
                                           '>>></button>
                                        </div>
                                    </td>
                                </tr>';
                                    /* Images Grid */
                        $HTMLGrid .= '</ul></div>
                                    <tr class="borderradius">
                                        <td colspan="9">
                                            <div class="pagenation">
                                                <button class="pageindex" 
                                                    page="members" pageindex="1" 
                                                    action_tab="'.$subtabs.'" '.
                                                    (($currentpage == 1) ? 
                                                    'disabled="disabled"' : '').
                                                    '><<</button>
                                                <button class="pageindex" 
                                                    page="members" pageindex="'.
                                                    ($currentpage - 1).
                                                    '" action_tab="'.
                                                    $subtabs.'" '.
                                                    (($currentpage == 1) ? 
                                                    'disabled="disabled"' : '').
                                                    '><</button>
                                            <ul>';
                    for ($pageindex = 1;$pageindex <= $pages;
                        ++$pageindex) {
                        $HTMLGrid .= '<li '.(($pageindex == $currentpage)  ?
                            'class="active"' : '').'><a href="javascript:void(0)"  
                            class="pageindex" page="members" pageindex="'.
                            $pageindex.'" action_tab="'.$subtabs.'">'.
                            $pageindex.'</li>';
                    }
                    $HTMLGrid .= '</ul>
                                        <button class="pageindex" 
                                            page="members" pageindex="'.
                                            ($currentpage + 1).
                                            '" action_tab="'.
                                            $subtabs.'" '.
                                            (($currentpage == $pages) ?
                                            'disabled="disabled"' : '').
                                            '>></button>
                                        <button class="pageindex" page="members" 
                                            pageindex="'.$pages.'" action_tab="'.
                                            $subtabs.'" '.(($currentpage == $pages) ?
                                            'disabled="disabled"' : '').
                                            '>>></button>
                                      </div>
                                   </td>
                                </tr>';
                }
            }
        } else {
            $HTMLList .= "<tr><td colspan='20'>No result found</td></tr>";
            $HTMLGrid .= "<tr><td colspan='20'>No result found</td></tr>";
        }
        if ($type == 'g') {
            echo $HTMLGrid;
        } else {
            echo $HTMLList;
        }
        exit;
        break;
        /*Comments: To handle save new machine data.TASKID = '4307'
        * author SK Shanethatech *
        * Created Date  : FEB-05-2016
        */
    case 'machinesData':
        $param = $_REQUEST;
        $clubId = isset($param['clubId']) ? $param['clubId'] : 1;
        $machinelist = $this->members->machinesData($param);
        $listofmachine = array();
        $listofmachine = $machinelist['data'];
        $rowscount = $machinelist['total_records'];
        if ($rowscount == 1) {
            $listofmachine = array($listofmachine);
        }
        $html = '';
        $html .= '<option value="0">Select</option>';
        foreach ($listofmachine as $list) {
            $html .= '<option value="'.$list['machineid'].'">'.
            $list['nameofmachine'].'</option>';
        }
        echo $html;
        break;
    case 'getDeviceById':
        $param = $_REQUEST;
        $devicelist = $this->members->getDeviceById($param);
        $result = array();
        $result = $devicelist['data'];
        $result = array($result);
        echo json_encode($result);
        break;
    case 'getListDeviceData':
        $param = $_REQUEST;
        $devicelist = $this->members->getListDeviceData($param);
        $listofdevice = array();
        $listofdevice = $devicelist['data'];
        $rowscount = $devicelist['total_records'];
        if ($rowscount == 1) {
            $listofdevice = array($listofdevice);
        }
        $html = '';
        if ($devicelist['sucess_code'] == 1) {
            foreach ($listofdevice as $device) {
                $html .= '<tr>
                            <td>'.$device['device'].'</td>
                            <td>'.$device['machine_name'].'</td>
                            <td>
                                <button class="selectbtn" 
                                    onclick="editDevice(this,\''.$device['deviceid'].'\')">
                                    Select
                                </button>
                            </td>
                            </tr>';
            }
        } else {
            $html .= '<tr>No Result Found</tr>';
        }
        echo $html;
        break;
    case 'saveDeviceData':
        $param = $_REQUEST;
        $param['random'] = $this->common->randStr(10);
        $devicelist = $this->members->saveDeviceData($param);
        echo json_encode($devicelist);
        break;
    case 'getlistdetails':
        $params = $_REQUEST;
        $lastclubid = $this->members->getlastclubiddetails($params);
        $maxclubidid = $lastclubid['result'];
        $max_auto_club_id = '';
        if ($maxclubidid == '' || $maxclubidid == null) {
            $i = 1;
            $val = $i;
            $max_auto_club_id = str_pad($val, 5, '0', STR_PAD_LEFT);
        } else {
            $maxid = intval(substr($maxclubidid['maxclubid'], 2)) + 1;
            $max_auto_club_id = str_pad($maxid, 5, '0', STR_PAD_LEFT);
        }
        $listofbrands = $this->members->getbrandslist($params);
        $brands = $listofbrands['data'];
        $listoftypelist = $this->members->gettypelist($params);
        $type = $listoftypelist['data'];
        $listofgroup = $this->members->getgrouplist($params);
        $group = $listofgroup['data'];
        $subtypelist = $this->members->getsubtypelist($params);
        $subtype = $subtypelist['data'];
        echo json_encode(
            array(
                'brands' => $brands, 'type' => $type, 
                'group' => $group, 'subtype' => $subtype, 
                'maxclubid' => $max_auto_club_id
            )
        );
        break;
    case 'saveNewMachineDetails':
        $params = $_REQUEST;
        $savenewmachinedetails = '';
        $savenewmachinedetails = $this->members->newmachinedetails($params);
        $response   =   array();
        $response['success'] = 0;
        $response['message'] = 'Machine adding failed';
        if ($savenewmachinedetails['status_id'] == 200) {
            if ($savenewmachinedetails['update'] == 0) {
                $userid = isset($_REQUEST['createdby']) ? 
                    $_REQUEST['createdby'] : '';
                $userParam['userId'] = $userid;
                $userdata = $this->members->getMemberBasicDetail($userParam);
                $paramsemail = array();
                $paramsemail['from'] = SEND_EMAIL_FROM;
                $paramsemail['fromName'] = SEND_EMAIL_FROM_NAME;
                $paramsemail['to'] = $userdata['email'];
                $paramsemail['subject'] = 'New Machine Added Successfully';
                $imgtoplogo = BASE_URL.'images/mailtoplogo.png';
                $imgbottomlogo = BASE_URL.'images/mailbottomlogo.png';
                $bodyhtml   =   file_get_contents("../templates/email/addprogram.html");
                $pharr  =   array(
                    '{{EMAILTOPLOGO}}',
                    '{{FIRSTNAME}}',
                    '{{LASTNAME}}',
                    '{{EMAILBOTTOMLOGO}}'
                );
                $reparr=    array($imgtoplogo,$userdata['first_name'],$userdata['last_name'],$imgbottomlogo);
                $bodyhtml   =   str_replace($pharr,$reparr,$bodyhtml);
                /*
                $body = "<div style='padding: 0 10px' ><img src='".$imgtoplogo."' alt='logo' /></div><br><br>";
                $body .=  'Dear '.$userdata['first_name'].' '.
                $userdata['last_name'].',<br><br><br>';
                $body .=  'Thanks for your Adding New Program at MOVESMART.company,';
                $body .=   '<br><br>With kind Regards,<br><br>MOVESMART.company<br>';
                $body .=  'T.+32(0)468 1941 85<br>';
                $body .=  "M.<a href='mailto:support@movesmart.company'>";
                $body .=  "support@movesmart.company</a><br><br>";
                $body .= "<div style='padding:0 10px'><img src='".$imgbottomlogo."'";
                $body .= " alt='eatfreshicon' /></div>MOVESMART.company BV<br>";
                $body .= "Kwelkade 1 - 4001 RK Tiel<br>M.";
                $body .= "<a href='http://www.movesmart.company/'>";
                $body .= "www.MOVESMART.COMPANY</a><br><br><br>";
                */
                $paramsemail['message'] = $bodyhtml;
                if ($this->common->sendEmail($paramsemail)) {
                    $response['success'] = 1;
                    $response['message'] = 'Machine added successfully';
                } else {
                    $response['success'] = 0;
                    $response['message'] = 'Server error';
                }
                $response = $savenewmachinedetails;
            }
            $response = $savenewmachinedetails;
        }
        echo json_encode($response);
        exit;
        break;
    case 'saveMachineMapping':
        $params = $_REQUEST;
        $param['clubId'] = $params['clubId'];
        $param['loggedUserId'] = $params['loggedUserId'];
        $param['strengthMachineId'] = json_decode($params['machine']);
        $updatePlan = $this->admin->updateStrengthMachineDetails($param);
        break;
    case 'savePushMessage':
        $params = $_REQUEST;
        $param['clubid'] = $params['clubid'];
        $param['messagetext'] = $params['messagetext'];
        $param['loggedUserId'] = $params['loggedUserId'];
        $memberlist = $this->admin->getMemberByClub($param);
        $messsge = 'No users availble';
        if ($memberlist['total_records'] > 0) {
            $listofmember = isset($memberlist['memberlist']) ?
                $memberlist['memberlist'] : array();
            $param['text'] = $param['messagetext'];
            $param['currentid'] = $param['loggedUserId'];
            //$param['clubid'] = $param['clubid'];
            $param['msgtemplate'] = 3;
            $mid = array();
            foreach ($listofmember as $member) {
                $mid[] = $member['user_id'];
            }
            $param['mid'] = json_encode($mid);
            $memberlist = $this->members->InsertMessageEmail($param);
            $messsge = 'Send Message Sucessfully';
        }
        echo $messsge;
        break;
    case 'getNewMachinedetailsbyeditid':
        $params = $_REQUEST;
        $editMachiendetails 
            = $this->members->editNewMachinedetails($params);
        if ($editMachiendetails['status_code'] == 1) {
            $result = $editMachiendetails['result'];
        } else {
            $result = 'No Result found';
        }
        echo json_encode($result);
        break;
    case 'deleteStrengthMachienItem':
        $params = $_REQUEST;
        $deletebyeditID 
            = $this->members->deletemachinedetaislbyId($params);
        if ($deletebyeditID['status_code'] == 1) {
            $result = $deletebyeditID;
        } else {
            $result = 'No Result found please Try again later';
        }
        echo json_encode($result);
        break;
    case 'deleteNewMachine':
        $params = $_REQUEST;
        $deletemultiplemachinedetails 
            = $this->members->delNewMachineDetails($params);
        if ($deletemultiplemachinedetails['status_code'] == 1) {
            $result = $deletemultiplemachinedetails;
        } else {
            $result = $deletemultiplemachinedetails;
        }
        echo json_encode($result);
        break;
                    /*SN Added 20160125*/
    case 'getRegUserData':
        $regid = isset($_REQUEST['regid']) ? $_REQUEST['regid'] : '';
        $param['regid'] = $regid;
        $user = $this->members->getRegUserData($param);
        $result['status_code'] = $user['movesmart']['status_code'];
        $result['usertype_id'] = 3;
        $result['user'] = $user['movesmart']['user'];
        echo json_encode($result);
        break;
                /*SN added 20160129*/
    case 'bodyCompositionListGrid':
        $paramUser['userId'] 
            = isset($_REQUEST['tab']) ? $_REQUEST['tab'] : '';
        $memberBasicDetail 
            = $this->members->getMemberBasicAndTestDetail($paramUser);
        $paramD['userId'] = $paramUser['userId'];
        $paramD['testId'] = 7;
        $tablehtml = '';
        $tablehtml .= '<table class="bodycommplist_tbl" 
                        cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Test Date</th>
                                <th>Body Fat Level</th>
                                <th>Weight</th>
                                <th>Height</th>
                                <th>BMI</th>
                                <th>BellyGrowth</th>
                                <th>FatPercent</th>
                                <th>LeanKg</th>
                                <th>Sport</th>
                                <th>lifeStyle</th>
                                <th>Stress</th>
                            </tr>
                            </thead>
                        <tbody>';
        $getTestingMeasuringData 
            = $this->testingAndMeasuring->getTestingMeasuringData($paramD);
        $param['testId'] = $paramD['testId'];
        $getTestItems = $this->test->getTestItems($param);
        $getTestItems = $getTestItems['rows'];
        $fields =   array();
        foreach ($getTestItems as $row) {
            $fields[$row['test_item']] = $row['test_item_id'];
            $tablehtml .= '<input type="hidden"  
                name="items['.$row['test_item_id'].
                '][testItemId]" value="'.$row['test_item_id'].'" />';
        }
        $fieldSet = array(
            $fields['BODY_FAT_LEVEL'],
            $fields['WEIGHT'],
            $fields['HEIGHT'],
            $fields['BODY_MASS_INDEX'],
            $fields['BELLY_GRITH'],
            $fields['FAT_PERCENTAGE'],
            $fields['LEAN_KG'],
            $fields['SPORTS'],
            $fields['LIFE_STYLE'],
            $fields['STRESS'],
        );
        $values = isset($getTestingMeasuringData['rows']) ? 
            $getTestingMeasuringData['rows'] : array();
        $data = array();
        $fatKgGraph = array();
        $weightKgGraph = array();
        $dateGraph = array();
        foreach ($fieldSet as $field) {
            if (count($values) > 0) {
                foreach ($values as $row) {
                    if ($field == $row['r_test_item_id']) {
                        $data[$row['testing_measuring_id']][] = $row;
                    }
                }
            }
        }
        $tabletrhtml = '';
        if (count($data) > 0) {
            foreach ($data as $row) {
                $testDate 
                    = $this->common->siteDateFormat(
                        ($row[0]['test_date'] != '') ?
                        $row[0]['test_date'] : ''
                    );
                $tablehtml .= '<tr><td>'.$testDate.'</td>';
                array_push($dateGraph, "'".$testDate."'");
                array_push(
                    $fatKgGraph, 
                    $this->members->getItemValueFromTestItem('BODY_FAT_LEVEL', $row)
                );
                array_push(
                    $weightKgGraph, 
                    $this->members->getItemValueFromTestItem('WEIGHT', $row)
                );
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'BODY_FAT_LEVEL', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'WEIGHT', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'HEIGHT', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'BODY_MASS_INDEX', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'BELLY_GRITH', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'FAT_PERCENTAGE', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'LEAN_KG', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'SPORTS', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'LIFE_STYLE', $row
                ).'</td>';
                $tablehtml .= '<td>'.$this->members->getItemValueFromTestItem(
                    'STRESS', $row
                ).'</td>';
                $tablehtml .= '</tr>';
            }

            $tablehtml .= '<tr><td colspan="11">
                <span>Body Composition Test History</span>
                </td></tr>';
        } else {
            $tablehtml .= '<tr><td colspan="11">
                <span>No result found</span></td>
                </tr>';
        }
        $tablehtml .= '</tbody></table>';
        echo $tablehtml;
        break;
    case 'bodyCompositionGraph':
        $paramUser['userId'] 
            = isset($_REQUEST['tab']) ? 
            $_REQUEST['tab'] : '';
        $memberBasicDetail 
            = $this->members->getMemberBasicAndTestDetail($paramUser);
        $paramD['userId'] = $paramUser['userId'];
        $paramD['testId'] = 7;
        $getTestingMeasuringData 
            = $this->testingAndMeasuring->getTestingMeasuringData($paramD);
        $param['testId'] = $paramD['testId'];
        $getTestItems = $this->test->getTestItems($param);
        $getTestItems = $getTestItems['rows'];
        $fields =   array();
        foreach ($getTestItems as $row) {
            $fields[$row['test_item']] = $row['test_item_id'];
        }
        $fieldSet = array(
            $fields['BODY_FAT_LEVEL'],
            $fields['WEIGHT'],
            $fields['HEIGHT'],
            $fields['BODY_MASS_INDEX'],
            $fields['BELLY_GRITH'],
            $fields['FAT_PERCENTAGE'],
            $fields['LEAN_KG'],
            $fields['SPORTS'],
            $fields['LIFE_STYLE'],
            $fields['STRESS'],
        );
        $values = isset($getTestingMeasuringData['rows']) ? 
            $getTestingMeasuringData['rows'] : array();
        $data = array();
        $fatKgGraph = array();
        $weightKgGraph = array();
        $dateGraph = array();
        $arraySeries = array();
        foreach ($fieldSet as $field) {
            if (count($values) > 0) {
                foreach ($values as $row) {
                    if ($field == $row['r_test_item_id']) {
                        $data[$row['testing_measuring_id']][] = $row;
                    }
                }
            }
        }
        if (count($data) > 0) {
            foreach ($data as $row) {
                $testDate 
                    = $this->common->siteDateFormat(
                        ($row[0]['test_date'] != '') ? 
                        $row[0]['test_date'] : ''
                    );
                array_push($dateGraph, $testDate);
                array_push(
                    $fatKgGraph, 
                    intval(
                        $this->members->getItemValueFromTestItem(
                            'FAT_PERCENTAGE', $row
                        )
                    )
                );
                array_push(
                    $weightKgGraph, 
                    intval(
                        $this->members->getItemValueFromTestItem(
                            'WEIGHT', $row
                        )
                    )
                );
            }
        }
        $fatarrayseries = array(
        'name' => 'Fat',
        'data' => $fatKgGraph,
        'pointWidth' => '30',
        'stack' => 'Fat',
        'color' => '#88ed8c',
        'edgeWidth' => '0.5',
        'edgeColor' => '#1f1f1f',
        'groupPadding' => 0.56,
        );
        $weightarrayseries = array(
        'name' => 'Weight',
        'data' => $weightKgGraph,
        'stack' => 'Weight',
        'color' => '#f9a0a9',
        'edgeColor' => '#1f1f1f',
        );
        array_push($arraySeries, $fatarrayseries);
        array_push($arraySeries, $weightarrayseries);
        echo json_encode(
            array(
                'dateGraph' => $dateGraph, 
                'fatKgGraph' => $fatKgGraph, 
                'weightKgGraph' => $weightKgGraph,
                'arraySeries' => $arraySeries
            )
        );
        break;
                /*SN added 20160201*/
    case 'FlexibilityTestListGrid':
        $paramUser['userId'] = isset($_REQUEST['tab']) ? $_REQUEST['tab'] : '';
        $memberBasicDetail 
            = $this->members->getMemberBasicAndTestDetail($paramUser);
        $paramD['userId'] = $paramUser['userId'];
        $paramD['testId'] = 8;
        $tablehtml = '';
        $tablehtml .= '<table cellpadding="0" 
                            cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <th>Test date</th>
                                    <th>CALFS(L-R)</th>
                                    <th>ILLIPSOAS(L-R)</th>
                                    <th>HAMSTRING (L-R)</th>
                                    <th>BREAST (L-R)</th>
                                    <th>QUADRICEPS (L-R)</th>
                            </tr>';
        $getTestingMeasuringData 
            = $this->testingAndMeasuring->getTestingMeasuringData($paramD);
        $param['testId'] = $paramD['testId'];
        $getTestItems = $this->test->getTestItems($param);
        $getTestItems = ($getTestItems and isset($getTestItems['rows']) > 0) ? 
            $getTestItems['rows'] : array();
        $fields = array();
        if (count($getTestItems) > 0) {
            foreach ($getTestItems as $row) {
                $fields[$row['test_item']] = $row['test_item_id'];
                $tablehtml .= '<input type="hidden"  
                    name="items['.$row['test_item_id'].'][testItemId]" 
                    value="'.$row['test_item_id'].'" />';
            }
        }
        $fieldSet = array();
        if (count($fields) > 0) {
            $fieldSet = array($fields['CALFS_LEFT'],
                $fields['CALFS_RIGHT'],
                $fields['ILLIPSOAS_LEFT'],
                $fields['ILLIPSOAS_RIGHT'],
                $fields['HAMSTRING_LEFT'],
                $fields['HAMSTRING_RIGHT'],
                $fields['BREAST_LEFT'],
                $fields['BREAST_RIGHT'],
                $fields['QUADRICEPS_LEFT'],
                $fields['QUADRICEPS_RIGHT'],
            );
        }
        $countFieldSet = count($fieldSet);
        $values = isset($getTestingMeasuringData['rows']) ? 
            $getTestingMeasuringData['rows'] : array();
        $data = array();
        $fatKgGraph = array();
        $weightKgGraph = array();
        $dateGraph = array();
        foreach ($fieldSet as $field) {
            if (count($values) > 0) {
                foreach ($values as $row) {
                    if ($field == $row['r_test_item_id']) {
                        $data[$row['testing_measuring_id']][] = $row;
                    }
                }
            }
        }
        if (count($data) > 0) {
            foreach ($data as $row) {
                $testDate 
                    = $this->common->siteDateFormat(
                        ($row[0]['test_date'] != '') ? 
                        $row[0]['test_date'] : ''
                    );
                array_push(
                    $fatKgGraph, 
                    $this->members->getItemValueFromTestItem(
                        'FAT_PERCENTAGE_1', $row
                    )
                );
                array_push(
                    $weightKgGraph, 
                    $this->members->getItemValueFromTestItem(
                        'WEIGHT', $row
                    )
                );
                $tablehtml .= '<tr>
                                <td>'.$testDate.'</td>
                                <td>'.
                                $this->members->getItemValueFromTestItem(
                                    'CALFS_LEFT', $row
                                ).
                                '-'.$this->members->getItemValueFromTestItem(
                                    'CALFS_RIGHT', $row
                                ).
                                '</td>
                                <td>'.
                                $this->members->getItemValueFromTestItem(
                                    'ILLIPSOAS_LEFT', $row
                                ).
                                '-'.$this->members->getItemValueFromTestItem(
                                    'ILLIPSOAS_RIGHT', $row
                                ).
                                '</td>
                                <td>'.
                                $this->members->getItemValueFromTestItem(
                                    'HAMSTRING_LEFT', $row
                                ).
                                '-'.$this->members->getItemValueFromTestItem(
                                    'HAMSTRING_RIGHT', $row
                                ).
                                '</td>
                                <td>'.
                                $this->members->getItemValueFromTestItem(
                                    'BREAST_LEFT', $row
                                ).
                                '-'.$this->members->getItemValueFromTestItem(
                                    'BREAST_RIGHT', $row
                                ).
                                '</td>
                                <td>'.
                                $this->members->getItemValueFromTestItem(
                                    'QUADRICEPS_LEFT', $row
                                ).
                                '-'.$this->members->getItemValueFromTestItem(
                                    'QUADRICEPS_RIGHT', $row
                                ).
                                '</td>
                        </tr>';
            }
            $tablehtml .= '<tr><td colspan="7">
                <span>Flexibility History</span>
                </td></tr>';
        } else {
            $tablehtml .= '<tr><td colspan="7">
                <span>No results found</span></td>
                </tr>';
        }
        $tablehtml .= '</tbody></table>';
        echo $tablehtml;
        break;
    case 'CardioListGrid':
        $paramUser['userId'] 
            = isset($_REQUEST['tab']) ? $_REQUEST['tab'] : '';
        $memberBasicDetail 
            = $this->members->getMemberBasicAndTestDetail($paramUser);
        $paramD['userId'] 
            = $paramUser['userId'];
        $limitStart = 0;
        $limitEnd = PAGINATION_SHOW_PER_PAGE;
        $currentpage = isset($_REQUEST['page']) ? 
            $_REQUEST['page'] : 1;
        if (isset($_REQUEST['offset']) 
            && ($_REQUEST['offset'] > 0)
        ) {
            $limitEnd = $_REQUEST['offset'];
        }
        if ($currentpage > 0) {
            $limitStart = ($currentpage - 1) * $limitEnd;
        }
        $paramD['limitStart'] = $limitStart;
        $paramD['limitEnd'] = $limitEnd;
        ?>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Test date</th>
                    <th>Weight</th>
                    <th>Test Level</th>
                    <th>Age on Test</th>
                </tr>
            </thead>
            <tbody>
        <?php
        $getTestingMeasuringData 
            = $this->testingAndMeasuring->getTestingMeasuringCardioData($paramD);
        $data = array();
        $rows = isset(
            $getTestingMeasuringData['result']
        ) ? $getTestingMeasuringData['result'] : array();
        $totalCount 
            = isset($getTestingMeasuringData['totalCount']) ? 
                $getTestingMeasuringData['totalCount'] : 1;
        if (count($rows) > 0) {
            if ($rows) {
                foreach ($rows as $values) { ?>
                    <tr>
                        <td><?php echo $this->members->getFormatDate($values['test_start_date'])?></td>
                        <td><?php echo $values['weight']?></td>
                        <td><?php echo $this->members->getTestLevel($values['test_level'])?></td>
                        <td><?php echo $values['age_on_test']?></td>
                    </tr>
                    <?php
                }
            }
            $pages = ceil($totalCount / PAGINATION_SHOW_PER_PAGE);
            if ($pages > 1) { ?>
                <tr class="borderradius">
                    <td colspan="9">
                        <div class="pagenation">
                            <button class="listpageindex" page="cardio" pageindex="1"
                                <?php echo (($currentpage == 1) ? 'disabled="disabled"' : '')?>><<
                            </button>
                            <button class="listpageindex" page="cardio" 
                                    pageindex="<?php echo ($currentpage - 1);?>"
                                    <?php echo (($currentpage == 1) ? 'disabled="disabled"' : '')?>><
                            </button>
                            <ul>
               <?php for ($pageindex = 1;$pageindex <= $pages;++$pageindex) { ?>
                    <li <?php echo (($pageindex == $currentpage)  ? 'class="active"' : '');?>>
                        <a href="javascript:void(0)" id="listpageindex" class="listpageindex" page="cardio"
                           pageindex="<?php echo $pageindex;?>">
                        <?php echo $pageindex;?>
                    </li>
                <?php } ?>
                    </ul>
                    <button class="listpageindex" page="cardio" pageindex="<?php echo($currentpage + 1);?>"
                        <?php echo (($currentpage == $pages) ? 'disabled="disabled"' : '')?>>>
                    </button>
                    <button class="listpageindex"
                            page="cardio"
                            pageindex="<?php echo $pages?>"
                            <?php echo (($currentpage == $pages) ?'disabled="disabled"' : '')?>>>>
                    </button>
                    </div>
                </td>
                </tr>
            <?php } else { ?>
                <tr><td colspan="4"><span>Cardio Test History</span></td></tr>
            <?php }
        } else { ?>
            <tr><td colspan="4"><span>No results found</span></td></tr>
        <?php } ?>
        </tbody></table>
        <?php
        break;
    /*MK Added Complete Registeration Steps*/
    case 'SecurityQuesAns':
        $regid = $_REQUEST['regid'] ? 
            $_REQUEST['regid'] : '';
        $question = $_REQUEST['question'] ? 
            $_REQUEST['question'] : '';
        $param['regid'] = $regid;
        $param['question'] = $question;
        /*Update Security Questions Answers*/
        $result = $this->members->saveSecurityQues($param);
        $response = array();
        $response['success'] = 0;
        if ($result['movesmart']['status'] == 'success') {
            $response['success'] = 1;
        }
        echo json_encode($response);
        break;
    case 'getNationalityList':
        $param = array();
        $result = $this->members->getNationalityList($param);
        echo json_encode(array('nationalities' => $result));
        break;
    case 'getActivityList':
        $param = array();
        $result = $this->members->getActivityList($param);
        $resultdata = array('status' => 'failed', 
            'status_code' => 0, 'activities' => array());
        if ($result['status_code'] == 1) {
            $resultdata = array('status' => 'success', 
                'status_code' => 1, 
                'activities' => $result['activitesList']);
        }
        echo json_encode($resultdata);
        break;
    case 'getProfessionList':
        $param = array();
        $result = $this->members->getProfessionList($param);
        echo json_encode(array('professions' => $result));
        break;
    case 'getCityListByZipCode':
        $parms['zipCode'] = $_REQUEST['zipCode'];
        $cityLists = $this->members->getCityList($parms);
        echo json_encode(array('CityByZipcode' => $cityLists));
        break;
    case 'updatePersonalInfo':
        $param = $_REQUEST;
        $result = $this->members->updatePersonalInfo($param);
        $resdata['status'] = 'failed';
        $resdata['status_message'] = 'Try again later';
        if ($result['status_code'] == 200) {
            $resdata['status'] = 'success';
            $resdata['status_message'] = '';
        }
        echo json_encode($resdata);
        exit;
                    break;
    case 'updateActivityMember':
        $params['userId'] = isset($_POST['userId']) ? 
            $_REQUEST['userId'] : 0;
        $params['activity'] = isset($_POST['activity_freq']) ? 
            json_decode($_REQUEST['activity_freq'], true) : array();
        $result = $this->members->updateActivityMember($params);
        echo json_encode($result);
        break;
    case 'updateMedicalMuscleInjury':
        $params['userId'] = isset($_REQUEST['userId']) ? 
            $_REQUEST['userId'] : 0;
        $params['injuryDesc'] = isset($_REQUEST['injuryDesc']) ? 
            $_REQUEST['injuryDesc'] : '';
        $result 
            = $this->members->updateMedicalMuscleInjury($params);
        echo json_encode($result);
        exit;
                    break;
    case 'updateMemberMedicalInfo':
        $params = $_REQUEST;
        $result = $this->members->updateMemberMedicalInfo($params);
        echo json_encode($result);
        exit;
        break;
    /*MK Added - 03 Feb 2016 - Get the member's basic detail by ID*/
    case 'getMemberDetailByID':
        if (isset($_REQUEST['memberId'])) {
            /*Passing member as User Id to get Member Info from class.*/
            $memberId = $_REQUEST['memberId'];
            $param['user_id'] = $memberId;
            $result = $this->members->getListEditMembers($param);
            echo json_encode($result);
        }
        break;
        /* MK Added Heart rate Devices */
    case 'getHeartrateDevices':
        $params = array();
        $result = $this->members->getHeartrateDevices($params);
        $response = array();
        if (isset($result['status_code']) 
            and ($result['status_code'] == 1)
        ) {
            $response = $result['devices'];
        }
        echo json_encode($response);
        exit;
        break;
        /*SNK Get all mapped device info*/
    case 'getMappedDeviceInfo':
        $params = array();
        $result = $this->members->getMappedDeviceInfo($params);
        $response = array();
        if (isset($result['status_code']) 
            and ($result['status_code'] == 1)
        ) {
            $response = $result['devices'];
            if ($result['total_records'] == 1) {
                $response = array($result['devices']);
            }
        }
        echo json_encode($response);
        exit;
                    break;
    case 'updateHeartRateOnTest':
        $params = $_REQUEST;
        $datastring = isset($_POST['data']) ? 
            $_POST['data'] : '';
        $loadData = array();
        if ($datastring != '') {
            $loadData = json_decode($datastring, true);
        }
        if (isset($loadData['updateCoolDownStart']) 
            and ($loadData['updateCoolDownStart'] == 1)
        ) {
            $loadData['coolDownStart'] = date('Hi');
        }
        /*if (isset($loadData['coolDownStart'])) {
            $loadData['coolDownStart'] = $loadData['coolDownStart'];
        }*/
        if (isset($loadData['updateCoolDownStop']) 
            and ($loadData['updateCoolDownStop'] == 1)
        ) {
            $loadData['coolDownStop'] = date('Hi');
        }
        /*if (isset($loadData['coolDownStop'])) {
            $loadData['coolDownStop'] = $loadData['coolDownStop'];
        }*/
        $params['updatedata'] = $loadData;
        $result 
            = $this->members->updateHeartRateOnTest($params);
        $response = array();
        $response['status'] = 0;
        $response['status_message'] = 'Failed to update';
        if (isset($result['movesmart']['status_code']) 
            and ($result['movesmart']['status_code'] == 1)
        ) {
            $response['status'] = 1;
            $response['status_message'] = 'Updated successfully';
        }
        echo json_encode($response);
        exit;
                    break;
    case 'MailAnalysisReportPDF':
        $parms = $_REQUEST;
        $result = $this->members->MailAnalysisReportPDF($parms);
        $response = array();
        $status = 'failed';
        $status_message = 'Failed to generated and send mail.';
        if ($result['status_code'] == 200) {
            $testdata = $result['data'];
            $graphdatastr = $testdata['analysis_graphdata'];
            $graphdata = json_decode($graphdatastr, true);
            $clubname = $testdata['club_name'];
            $clientname 
                = ($testdata['first_name'].' '.$testdata['last_name']);
            $testdate 
                = date('d/m/Y', strtotime($testdata['test_start_date']));
            $hrgraph 
                = '<img src="'.$graphdata['hr'].'" style="width:100%"/>';
            $cathrgraph 
                = '<img src="'.$graphdata['cathr'].'" style="width:100%"/>';
            $coachadvice 
                = ($testdata['analysed_info'] == '') ? 
                    'N/A' : $testdata['analysed_info'];
            $paramsemail = array();
            $paramsemail['from'] = SEND_EMAIL_FROM;
            $paramsemail['fromName'] = SEND_EMAIL_FROM_NAME;
            $paramsemail['to'] = $testdata['email'];

            include_once '../lib/dompdf/dompdf_config.inc.php';
            
            $imgtoplogo = BASE_URL.'images/mailtoplogo.png';
            $imgbottomlogo = BASE_URL.'images/mailbottomlogo.png';

            $paramsemail['subject'] = 'Cardio Test Analysis Report - '.
                $clubname.' - Date : '.$testdate;
            $html = file_get_contents('../pdftemplates/report-cardioanalysed.html');
            $placeholder_array = array(
                '{{Client-Name}}', '{{Test-Date}}', 
                '{{Analysed-Date}}', '{{hr-Graph}}', '{{cathr-Graph}}', 
                '{{coach-note}}', '{{Club-Name}}'
            );
            $replace_array = array(
                $clientname, $testdate, $testdate, 
                $hrgraph, $cathrgraph, $coachadvice, 
                $clubname
            );
            $pdfcontent = str_replace(
                $placeholder_array, 
                $replace_array, 
                $html
            );
            $bodyhtml   =   file_get_contents("../templates/email/pdfanalysedreport.html");
            $pharr  =   array(
                '{{EMAILTOPLOGO}}',
                '{{CLIENTNAME}}',
                '{{EMAILBOTTOMLOGO}}'
            );
            $reparr =   array($imgtoplogo,$clientname,$imgbottomlogo);
            $bodyhtml   =   str_replace($pharr,$reparr,$bodyhtml);
            /*
            $body = "<div style='padding: 0 10px' ><img src='".$imgtoplogo."' alt='logo' /></div><br><br>";
            $body .= "Dear $clientname, <br/><br/>The Cardio analysed";
            $body .= ' Report is attached to print.<br/><br/>';
            $body .= "With kind Regards,<br> Movesmart Team alt='eatfreshicon'/>";
            $body .= "<div style='padding:0 10px'><img src='".$imgbottomlogo."'";
            $body .= '</div>MOVESMART.company BV<br>Kwelkade 1 - 4001 RK Tiel<br>';
            $body .= "M.<a href='http://www.movesmart.company/'>";
            $body .= "www.MOVESMART.COMPANY</a><br><br><br>";
            */

            $paramsemail['message'] = $bodyhtml;
            $pdfgen = new DOMPDF();
            $pdfgen->load_html(
                '<div style="width:100%">'.
                $pdfcontent.'</div>'
            );
            $pdfgen->render();
            $filename = '../tmppdf/report-analysed/'.
                str_replace(' ', '_', $clientname).time().'.pdf';
            file_put_contents($filename, $pdfgen->output());
            $paramsemail['attachment'] = $filename;
            $status = 'success';
            $status_message 
                = 'Generated Report and failed to send please try again.';
            if ($this->common->sendEmail($paramsemail)) {
                //unlink($filename);
                $status = 'success';
                $status_message = 'Generated Report and Sent to the client.';
            }
        }
        $response['status'] = $status;
        $response['status_message'] = $status_message;
        echo json_encode($response);
        exit;
        break;
    /* strength Machine Overview Details--- */
    case 'strengthOverviewprogram':
            $param = $_REQUEST;
            $param['companyId'] = COMPANY_ID;
            $pid = $_REQUEST['pid'];
            $strengthProgramArr 
                = $this->settings->strengthProgramList($param);
            $limitStart = 0;
            $limitEnd = PAGINATION_SHOW_PER_PAGE;
            $currentpage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
            $limitEnd = $_REQUEST['offset'];
        }
        if ($currentpage > 0) {
            $limitStart = ($currentpage - 1) * $limitEnd;
        }
        $param['limitStart'] = $limitStart;
        $param['limitEnd'] = $limitEnd;
        /* grid*/
        $status= ($strengthProgramArr['status_code'] == 200) ? 1 : 0;
        if ($status) {
            $strengthProgramList 
                = $strengthProgramArr['strengthProgramList'];
            $totalCount = $strengthProgramArr['totalCount'];
            $rowscount = $strengthProgramArr['total_records'];
            if ($rowscount == 1) {
                $arrayList = array($strengthProgramList);
            } else {
                $arrayList = $strengthProgramList;
            }
            if ($arrayList > 0) {
                foreach ($arrayList as $list) {
                    $data = $data1 = '';
                    if ($pid == $list['programid']) {
                        $data = 'checked="checked"';
                        $data1 = 'checkbtn';
                    }?>
                    <tr>
                        <td>
                            <span class="checkouter <?php echo $data1?>" id="idprogramlist">
                                <input type="checkbox" class="machinedetails programdetails"
                                    val="<?php echo $list['programid']?>"
                                    name="Programdetails_<?php echo $list['programid'];?>"
                                    title="Select Program"
                                    descript ="<?php echo $list['description']?>"
                                    hrefValue="<?php echo $list['programid'];?>" <?php echo $data;?>>
                        </td>
                        <td><?php echo $list['description'];?></td>
                        <td><?php echo $list['type']?></td>
                    </tr>
                <?php
                }
            } else { ?>
                <tr><td colspan='20'>No result found</td></tr>
            <?php
            }
        } else { ?>
            <tr><td colspan='20'>No result found</td></tr>
        <?php
        }
        break;
    case 'strengthProgramTraining':
        $params['strengthProgramId'] = $_REQUEST['tapvalue'];
        $strengthProgramTrainingHhtml 
            = $this->strength->getStrengthProgramTrainingHtml($params);
        echo $strengthProgramTrainingHhtml;
        break;
    case 'getMachineDataOverview':
        $params['is_deleted'] = 0;
        $strengthMachineList 
            = $this->admin->getStrengthMachineList($params);
        /* pagination*/
        $limitStart = 0;
        $limitEnd = PAGINATION_SHOW_PER_PAGE;
        $currentpage = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
            $limitEnd = $_REQUEST['offset'];
        }
        if ($currentpage > 0) {
            $limitStart = ($currentpage - 1) * $limitEnd;
        }
        $param['limitStart'] = $limitStart;
        $param['limitEnd'] = $limitEnd;
        /* pagination*/
        $status = ($strengthMachineList['status_code'] == 1)  ? 1 : 0;
        $pagination = '';
        $subtabs = 'getMachineDataOverview';
        if ($status) {
            $totalCount = $strengthMachineList['affectedrows'];
            $rowscount = $strengthMachineList['affectedrows'];
            $rows = $strengthMachineList['strengthMachine'];
            if ($totalCount == 1) {
                $arrayList = array($rows);
            } else {
                $arrayList = $rows;
            }
            $this->paginator->getUrlLink($param = 0);
            if (isset($arrayList) && !empty($arrayList)) {
                $pagination =   $this->paginator->displayItemsPagination(
                    $arrayList,
                    array('specValues' => '<span class="checkouter">
                            <input type="checkbox" class="programdetails" 
                            val="$mid$" name="Programdetails_$mid$"   
                            hrefValue="$mid$" id=""></span>',
                            'count',
                            'name',
                            '',
                            '',
                            'specValues2' => '<button class="selectbtn" 
                                onclick="machinetestdetails(\'$mid$\')"
                                hrefValue="$mid$">Select</button>',),
                    array('startTag' => '<tr>', 'midTagOpen' => '<td>', 
                            'midTagClose' => '</td>', 
                            'endTag' => '</tr>')
                );
            } else { ?>
                <tr><td colspan='20'>No result found</td></tr>
            <?php }
            if (isset($arrayList) && !empty($arrayList)) {
                $pages = ceil($totalCount / PAGINATION_SHOW_PER_PAGE);
                if ($pages > 1) { ?>
                    <tr class="borderradius">
                        <td colspan="9">
                            <div class="pagenation smallpagintion">
                                <button class="pageindex" page="members" pageindex="1"
                                        action_tab="<?php echo $subtabs;?>"
                                        <?php echo (($currentpage == 1) ? 'disabled="disabled"' : '')?>>
                                        <<
                                </button>
                                <button class="pageindex" page="members" 
                                        pageindex="<?php echo ($currentpage - 1)?>" 
                                        action_tab="<?php echo $subtabs?>"
                                    <?php echo (($currentpage == 1) ?  'disabled="disabled"' : '')?>><</button>
                                <ul>
                                    <?php
                    for ($pageindex = 1;$pageindex <= $pages;++$pageindex) { ?>
                        <li <?php echo (($pageindex == $currentpage)  ? 'class="active"' : '')?>>
                            <a href="javascript:void(0)"  class="pageindex" page="members"
                                pageindex="<?php echo $pageindex;?>" action_tab="<?php echo $subtabs;?>">
                                <?php echo $pageindex;?>
                        </li>
                    <?php } ?>
                    </ul>
                    <button class="pageindex" page="members" pageindex="<?php echo ($currentpage + 1)?>"
                            action_tab="<?php echo $subtabs;?>"
                            <?php echo (($currentpage == $pages) ? 'disabled="disabled"' : '')?>>>
                    </button>
                    <button class="pageindex" page="members" pageindex="<?php echo $pages?>"
                            action_tab="<?php echo $subtabs;?>"
                            <?php echo ( ($currentpage == $pages) ? 'disabled="disabled"' : '')?>>>>
                    </button>
                    </div>
                    </td>
                    </tr>
                <?php }
            } else { ?>
                <tr><td colspan='20'>No result found</td></tr>
            <?php }
        } else { ?>
            <tr><td colspan='20'>No result found</td></tr>
        <?php }
        echo $pagination;
        exit;
        break;
    case 'getMachienMappedetails': /* pending */
        $paramUser['userId'] = $_REQUEST['userID'];
        $paramUser['flag'] = 1;
        $getStrengthTestList 
            = $this->strength->getStrengthTestList($paramUser);
        $params['is_deleted'] = 0;
        $params = $_REQUEST;
        $strengthMachineList 
            = $this->admin->getStrengthMachineList($params);
        $status = ($getStrengthTestList['status_code'] == 200) ? 1 : 0;
        if ($status) {
            $count = count($getStrengthTestList['rows']);
            $result = $getStrengthTestList['rows'][0];
            $count = count($getStrengthTestList['rows']);
            $params['is_deleted'] = 0;
            $strengthMachineList 
                = $this->admin->getStrengthMachineList($params);
        } else {
            $result = 'No Test Performed for this user';
            $count = 0;
        }
        echo json_encode(
            array(
                'result' => $result, 
                'count' => $count, 'testid' => ''
            )
        );
        break;
        /*SN added Get client personal info*/
    case 'getClientDetailByID':
        if (isset($_REQUEST['memberId'])) {
            /*Passing member as User Id 
            to get Member Info from class.*/
            $memberId = $_REQUEST['memberId'];
            $param['user_id'] = $memberId;
            $result 
                = $this->members->getClientDetailByID($param);
            $activity 
                = $this->members->getClientActivityByID($param);
            $response = array();
            foreach ($result as $data) {
                $response[$data['user_id']] = $data;
                /*$response['activity'][] = array(
                   "activityid" =>$data['r_activity_id'],
                   "frequency_type" =>$data['frequency_type'],
                   "average_time" =>$data['average_time'],
                   "activity_name" =>$data['activity_name']
                );*/
            }
            $response['activity'] = $activity;
            echo json_encode($response);
        }
        break;
    case 'getWeekProgramTraining':
        $params = $_REQUEST;
        $getWeekTrainProgram 
            = $this->members->getweekTrainProgram($params);
        $status = ($getWeekTrainProgram['status_code'] == 1) ? 1 : 0;
        if ($status) {
            $trainList = $getWeekTrainProgram['devices'];
            $totalCount = count($getWeekTrainProgram['devices']);
            $i = 0;
            $arrayList = '';
            if ($totalCount == 1) {
                $arrayList = array($trainList);
            } else {
                $arrayList = $trainList;
            }
            foreach ($arrayList as $key => $value) {
                ?>
                <tr>
                    <td href="<?php echo $value['id'];?>" class="iseditval">
                        <span oldvalue="<?php echo $value['tweek']?>">
                            <?php echo $value['tweek']?>
                        </span>
                        <input type="hidden"
                               class="strength_val_edit_text"
                               value="<?php echo $_REQUEST['hrefid'] ?>" />
                    </td>
                    <td>
                        <span class="strg_val" oldvalue="<?php echo $value['series']?>">
                            <?php echo $value['series']?></span>
                            <input type="number" min ="0" onkeypress="return isAllowedColen(event);"
                                   title="Training Program:Series"
                                   id="strength_series" value="0" style=""
                                   class="strg_int" />
                    </td>
                    <td>
                        <span class="strg_val " oldvalue="<?php echo $value['reputation']?>">'.
                            <?php echo $value['reputation']?></span>
                            <input type="number"  min ="0"
                                   onkeypress="return isAllowedColen(event);"
                                   title="Training Program:Reputation"
                                   id="strength_reputation" value="0" style=""
                                   class="strg_int" />
                    </td>
                    <td>
                        <span class="strg_val" oldvalue="<?php echo $value['time']?>">
                            <?php echo $value['time']?></span>
                            <input type="number" min ="0" onkeypress="return isAllowedColen(event);"
                                   title="Training Program:Time"
                                   id="strength_time"
                                   value="0" style=""
                                   class="strg_int" />
                    </td>
                    <td>
                        <span class="strg_val"
                              oldvalue="<?php echo $value['strengthpercentage']?>">
                                <?php echo $value['strengthpercentage']?>
                        </span>
                        <input type="number"  min ="0" title="Training Program:Percentage"
                               onkeypress="return isAllowedColen(event);"
                               id="strenthper" value="0" style="" class="strg_int"/>
                    </td>
                    <td>
                        <span class="strg_val" oldvalue="<?php echo $value['rest']?>">
                            <?php echo $value['rest']?></span>
                        <input type="number" min="0" title="Training Program:Rest"
                               onkeypress="return isAllowedColen(event);" id="" value="0" style=""
                               class="strg_int" />
                    </td>
                </tr>
            <?php
            }
        } else { ?>
            <tr><td colspan="20">No result found</td></tr>
        <?php }
        exit;
        //echo $HTMLList;
        break;
    case 'getStengthProgramType' :
        $strengthProgramType = json_decode(STRENGTH_PROGRAM_TYPE, true);
        $html = '';
        foreach ($strengthProgramType as $key => $type) {
            $html .=  '<option value="'.$key.'">'.$type.'</option>';
        }
        echo $html;
        exit;
            break;
    case 'createWeekAssignProgram':
        $param = $_REQUEST;
        $param['companyId'] = COMPANY_ID;
        $param['typeval'] = $_REQUEST['val'];
        $param['loggedUser'] = $_REQUEST['loggedUserID'];
        $param['strengthProgramId'] 
            = isset($_REQUEST['strengthProgramId']) ? 
                $_REQUEST['strengthProgramId'] : 0;
        $result = $this->settings->insertUpdateStrengthProgram($param);
        echo json_encode($result);
        break;
    case 'strengthProgramTrainingAddUpdate':
        $param = $_REQUEST;
        $training 
            = $this->settings->insertUpdateStrengthProgramTraining($param);
        echo json_encode($training);
        break;
            /* Set New Resstore point/ Set Program Point */
    case 'saveNewProgramStatus':
        $params = $_REQUEST;
        $params['height'] = 172; // static content;
        $params['weight'] = 55; // static content;
        $params['test_status'] = 1;
        $params['testDate'] = date('Y-m-d H:i:s');
        $insertStrengthTest 
            = $this->strength->insertUpdateUserTest($params);
        $strengthUserTestId 
            = $insertStrengthTest['strengthUserTestId'];
        $strengthMachineFormData 
            = $_REQUEST['strengthProgramId'];
        if ($strengthUserTestId > 0) {
            $testStartDate = date(
                'Y-m-d', 
                strtotime($params['testDate'])
            );
            /*Get weekly schedule start and 
            end dates as array based on training week*/
            $paramWk['trainingNoOfWeeks'] = TRAINING_NO_OF_WEEKS;
            $weeksData 
                = $this->strength->getTrainingWeekDates(
                    $testStartDate, 
                    $paramWk['trainingNoOfWeeks']
                );
            $trainingWeek['strength_user_test_id'] 
                = $strengthUserTestId;
            $trainingWeek['userId'] 
                = $_REQUEST['hiddenUserId'];
            $trainingWeek['week_data'] = $weeksData;
            $this->strength->insertUpdateStrengthTrainingWeek($trainingWeek);
            $result = $insertStrengthTest;
        } else {
            $result = 'Please Try again Later';
        }
        echo json_encode($result);
        break;
    case 'getMappedMachienbyProgramId':
        /* getting list of machines by user id  */
        $params['is_deleted'] = 0;
        $params['clubid'] = $_REQUEST['clubid'];
        $strengthMachineList 
            = $this->admin->getStrengthMachineList($params);
        $html = '';
        /* Getting list machines by  
            it's ProgramID and Club id */
        $params['userId'] = $_REQUEST['userId'];
        $params['pid'] = $_REQUEST['pid'];
        $strenthMachineListbyProgId 
            = $this->admin->getMachineListByProgramId($params);
        /*member basic detail*/
        $params['userId'] = $_REQUEST['userId'];
        $memberBasicDetail = $this->members->getMemberBasicDetail($params);
        $paramsP['userId'] = $_REQUEST['userId'];
        $getMemberFatPercentage = $this->members->getMemberFatPercentage($paramsP);
        $bmi = 0;
        $fatPercentageMade = 0; 
        /*If body composition test was made 
        then value is 1 else 0*/
        if ($getMemberFatPercentage['status'] == 'success') {
            $bmi = $getMemberFatPercentage['rows']['value'];
            $fatPercentageMade = 1;
        }
        if ($strengthMachineList['status_code'] == 1) {
            if ($strenthMachineListbyProgId['status_code'] == 1) {
                $clubStrengthMachineIds =   array();
                foreach ($strenthMachineListbyProgId['strengthMachine'] as $row) {
                    $clubStrengthMachineIds[] = $row['id'];
                }
                foreach ($strengthMachineList['strengthMachine'] as $key => $value) {
                    $exist = in_array($value['mid'], $clubStrengthMachineIds);
                    $data = ($exist == 1) ? 'checked' : '';
                    $button = ($exist == 1) ? 'checkbtn' : '';
                    $strength = 0;
                    //Calculate Strength  
                    //Get BMI value
                    $weight = 58; // declare statically, by Saikrishna;
                    $height = 172;  // declare statically, by Saikrishna; 
                    $gender = (strtolower(
                        trim($memberBasicDetail['gender'])
                    ) == 'male') ? 1 : 0;
                    if ($fatPercentageMade == 0) {
                        $paramsBMI = array(
                            'height' => $height, 'weight' => $weight, 
                            'gender' => $gender
                        );
                        $bmi = $this->strength->calculateBMI($paramsBMI);
                    }
                    //Get BMI value	
                    $paramsLean = array('bmi' => $bmi, 'weight' => $weight);
                    $lean = $this->strength->calculateLean($paramsLean);
                    $coeft = 1;
                    //($gender == 1)?$value['coef_man']:$value['coef_woman'];
                    $paramsStrength = array('lean' => $lean, 'coeft' => $coeft);
                    $strength 
                        = $this->strength->calculateStrength($paramsStrength);
                    //Calculate Strength
                    //$strength = $strength;
                    if ($_REQUEST['flag'] == 1) { ?>
                        <tr>
                            <td>
                                <span class="checkouter '<?php echo $button?>" >
                                    <input type="checkbox" title="Check Selected Program"
                                           class="programmachinedetails
                                            programmachinedetails_'<?php echo $value['mid'] ?>"
                                           val="<?php echo $value['mid']?>"
                                           hrefValue="<?php echo $value['mid']?>" <?php echo $data;?>></span></td>
                                        <td><?php echo $value['name']?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="selectbtn OnEditweekdetails_'<?php echo $value['mid']?>"
                                                    name="<?php echo $value['name'];?>"
                                                    circuittype="<?php echo $_REQUEST['pid']?>"
                                                    id="OnEditweekdetails"
                                                    hrefValue="<?php echo $_REQUEST['pid']?>">Edit
                                            </button>
                                            <!--onclick="OnEditweekdetails('<?php //echo $_REQUEST['pid']?>')"-->
                                        </td>
                                    </tr>
                    <?php 
                        } else { ?>
                        <tr>
                            <td>
                                <span class="checkouter <?php echo $button;?>" >
                                    <input type="checkbox" title="Program Machine Select"
                                           class="programmachinedetails
                                                programmachinedetails_'<?php echo $value['mid']?>"
                                           val="<?php echo $value['mid']?>"
                                           hrefValue="<?php echo $value['mid']?>"  <?php echo $data; ?> >
                                </span>
                            </td>
                            <td><?php echo $value['name'];?></td>
                        </tr>
                        <?php
                    }
                }
            } else { ?>
                <tr><td colspan="20">No result found</td></tr>
            <?php }
        } else { ?>
            <tr><td colspan="20">No result found</td></tr>';
        <?php  }
        ?>
        <?php
        //echo $html;
        exit;
        break;
    case 'getweekdatelist':
        /* getting weel date for regarding 
        test based on number of test be done*/
        $params = $_REQUEST;
        $getdate = $this->strength->gettestdatevalue($params);
        echo json_encode($getdate['rows']);
        break;
            /*MK Added - Test Summary*/
    case 'getTestSummaryByMember':
        $params = $_REQUEST;
        $result = $this->members->getTestSummaryByMember($params);
        $response = array();
        $response['status'] = 0;
        if ($result['status_code'] == 200) {
            $response['status'] = 1;
            $response['summary'] = $result['summary'];
        }
        echo json_encode($response);
        exit;
                break;
    case 'updateMappedMemberDevice':
        $params = $_REQUEST;
        $result = $this->members->updateMappedMemberDevice($params);
        $response = array('status' => 0, 
            'status_message' => 'Failed to update the code');
        if ($result['status_code'] == 200) {
            $response = array('status' => 1, 
                'status_message' => 'Updated successfully');
        }
        echo json_encode($response);
        exit;
                break;

            /*SNK For Cardio test and analysis*/
    case 'getfitlevelforagesexspeed':
        $params = $_REQUEST;
        $result 
            = $this->members->getfitlevelforagesexspeed($params);
        echo json_encode($result);
        exit;
                break;
    case 'get12weekstraninigplanfor_age_sex_fitlevel':
        $params = $_REQUEST;
        $result 
            = $this->members->get12weekstraninigplanfor_age_sex_fitlevel($params);
        echo json_encode($result);
        exit;
                break;
    case 'getpointsper_activity_for_testid_iantw':
        $params = $_REQUEST;
        $result 
            = $this->members->getpointsper_activity_for_testid_iantw($params);
        echo json_encode($result);
        exit;
                break;
    case 'postpointachieveddata':
        $params = $_REQUEST;
        $result 
            = $this->members->postpointachieveddata($params);
        echo json_encode($result);
        exit;
                break;
    case 'getMessageDetails':
        $param = array();
        $messagelist = array();
        $result = array();
        $limit = isset($_REQUEST['limit']) ? 
                    $_REQUEST['limit'] : PAGINATION_SHOW_PER_PAGE;
        $limit_page = isset($_REQUEST['page']) ? 
                        $_REQUEST['page'] : 1;
        $params['limit_start'] = ($limit_page - 1) * $limit;
        $params['limit_records'] = $limit;
        if ($limit_page == -1) {
            $params['limit_records'] = -1;
        }
        $managemessage 
            = $this->message->getMessageDetails($params);
        $messagelist = $managemessage['message'];
        if ($managemessage['total_records'] == 1) {
            $messagelist = array($messagelist);
        }
        $result['messagelist_test'] = $messagelist;
        $result['messagelist'] = $messagelist;
        $result['recordsfound'] = $managemessage['recordsfound'];
        $result['total_records'] = $managemessage['total_records'];
        echo json_encode($result);
        break;
    case 'InsertMessageEmail';
        $params = $_REQUEST;
        $result = $this->members->InsertMessageEmail($params);
        $msgtemplate = (isset($params['msgtemplate'])) ? 
            $params['msgtemplate'] : 0;
        $response = array('status' => 'error', 
                    'status_code' => 0, 
                    'message' => 'Failed to send mail/messages');
        if ($result['status'] == 'success') {
            if ($msgtemplate == 1) {
                $param['userId'] = $params['mid'];
                $userdata = $this->members->getMemberBasicDetail($param);
                $response['status'] = 'success';
                $response['status_code'] = 1;
                $response['message'] = 'Scheduled successfully';
                if (isset($userdata['email'])) {
                    $email['from'] = SEND_EMAIL_FROM;
                    $email['fromName'] = SEND_EMAIL_FROM_NAME;
                    $email['to'] = $userdata['email'];
                    $email['subject'] = '';
                    $email['message'] = $params['text'];
                    if ($this->common->sendEmail($email)) {
                        $response['message'] 
                            = 'Scheduled & Sent E-mail successfully.';
                    }
                }
            }
        }
        echo json_encode($response);
        exit;
                break;
    case 'saveContactFormDetail':
        $params = $_REQUEST;
        //$result   =   $this->members->saveContactMail($params);
        $name = isset($_POST['name']) ? $_POST['name'] : null;
        $emailid = isset($_POST['emailid']) ? $_POST['emailid'] : null;
        $phoneno = isset($_POST['phoneno']) ? $_POST['phoneno'] : null;
        $message = isset($_POST['message']) ? $_POST['message'] : null;
        $subject = isset($_POST['subject']) ? $_POST['subject'] : null;
        /*$mail	=	new PHPMailer();
        $mail->IsSendmail(); 
        $mail->From = $emailid;
        $mail->FromName = $name;
        $mail->CharSet = "UTF-8";
        $mail->AddAddress("shanethatech.com");
        $mail->Subject 	= $subject;
        $mail->Body = $phoneno;
        $mail->AltBody = $message;
        $mail->isHTML(true);
        //print_r($mail);
        if($mail->Send()){
            echo "Message has been sent";
        }
        else{
             echo "Message could not be sent. <p>";
             echo "Mailer Error: " . $mail->ErrorInfo;
             exit;
        }*/
        $email_params['from'] = $emailid;
        $email_params['to'] = SEND_EMAIL_FROM;
        $email_params['fromName'] = $emailid;
        $email_params['subject'] = $subject;
        $body = '<p>Hi '.SEND_EMAIL_FROM.',</p>
                    <p>Member Name: '.$name.'</p>
                    <p>Phone No:'.$phoneno.'</p>
                    <p>Message:'.$message.'<br/><br/>';
        $email_params['message'] = $body;
        if ($this->common->sendEmail($email_params)) {
            $response['success'] = 1;
            $response['status_code'] = 1;
            $response['message'] = 'E-mail sent successfully';
        } else {
            $response['success'] = 1;
            $response['status_code'] = 0;
            $response['message'] = 'E-Mail not sent. Please try again later';
        }
        echo json_encode($response);
        exit;
                break;
            /*PK Added Reports Page*/
            case 'getReportsActivity':
                $params = $_REQUEST;
                //$params['gid'] = $_REQUEST['gid'];
                // $paramUser['userId'] = $_REQUEST['userId'];
                $params['userId'] = $_REQUEST['userId'];
                $result = $this->members->getMemberBasicDetail($params);
                $activity	=	$this->members->getReportsActivity($params);
                $response = array();
                    // foreach($result as $data){
                        // $response[$data['user_id']] = $data;
                    // }
                    $response['userdata'] = $result;
                    $response['activityresult'] = $activity;
                    echo json_encode($response);
                exit;
                break;
            case 'getSearchValue';
                $param = $_REQUEST;
                $param['userType']='member';
                $param['companyId']=2;
                $param['loggedUserType'] = (
                    isset($userdetails)
                ) ? $userdetails["r_usertype_id"]:-1;
                $param['labelField']=(
                    isset($_SESSION['pageName'][$_REQUEST['p']])
                ) ? $_SESSION['pageName'][$_REQUEST['p']] : "user_id";
                $param['sortType']=(
                    isset($_SESSION['pageName'][$_REQUEST['p']])
                    && $_SESSION['sortType']==2
                ) ? "desc" : "asc";
                $param['clubId']=(
                    isset($_REQUEST['clubId'])
                ) ? $_REQUEST['clubId']:"";
                $param['searchType'] = (
                    isset($_REQUEST['searchType'])
                ) ? $_REQUEST['searchType'] : "";
                //print_r($param['searchType']);
                $param['searchValue']='';
                if(isset($_REQUEST['searchValue'])) {
                    if($_REQUEST['searchType']=="gender"){
                        $param['searchValue']=strstr($_REQUEST['searchValue'],'f') ? "1":"0";
                    } else {
                        $param['searchValue']=$_REQUEST['searchValue'];
                    }
                }
                $resultlist =   array();
                $resultlist =   array();
                if($param['searchValue']!='') { 
                    $limitStart = 0;
                    $limitEnd = PAGINATION_SHOW_PER_PAGE;
                    $currentpage
                        =isset($_REQUEST['page']) ? $_REQUEST['page']:1;
                    if(isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                        $limitEnd = $_REQUEST['offset'];
                    }
                    if($currentpage > 0) {
                        $limitStart = ($currentpage - 1) * $limitEnd;
                    }
                    $param['limitStart'] = $limitStart; 
                    $param['limitEnd'] = $limitEnd; 	
                    $ureslist = $this->members->getUserList($param);
					
                    $result = array();                    
                    foreach($ureslist['result'] as $row) {
                        if(
                            isset($row[$param['searchType']])
                        ) {
                            $resultlist[$row['user_id']]
                                =$row[$param['searchType']];
                        }
                    }
                }
				
                echo json_encode($resultlist);
        exit;
                break;
            /*Direct Activation Link Page*/
    case 'activationfrommail' :
        $prams = array();
        $prams['userId'] = $_REQUEST['activeref'];
        $prams['r_status_id'] = USER_STATUS_ACTIVE;
        $result = $this->members->updateUserDetail($prams);
        $userdata = $this->members->getMemberBasicDetail($prams);
        //print_r($userdata);
        /*Redirect to the Application*/
        if ($result['status_code'] == 200 && isset($userdata['email'])) {
          //  echo 'Your account is activated!';
		  echo 'Je account is geactiveerd!';
            /* echo '<script>
                    window.location.href="movesmartclientapp://?username='.
                    base64_encode($userdata['email']).'&pwd='.
                    base64_encode($userdata['password']).'"</script>'; */
            exit;
        }
        break;
    case 'default' :
        break;
    }
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
