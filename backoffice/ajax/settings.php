<?php

/**
 * PHP version 5.
 
 * @category Ajax
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To handle all settings related ajax request.
 */
require_once '../lib/ExcelReader.php';
require_once '../lib/PHPExcel.php';

if (isset($_POST['action'])) {
    $action = $_POST['action'];
	//echo "<pre>";print_r($_POST);die;
    switch ($action) {

    case 'AddEditMenu':
        $param = $_REQUEST;
        $page = $this->settings->InsertUpdateMenus($param);
        echo json_encode($page);
        break;
    case 'deleteMenu':
        global $LANG;
        $params = $_REQUEST;
        $result = $this->settings->deleteMenuPage($params);
        if ($result['status'] == 'success') {
            $_SESSION['fl'] = array(1, $LANG['menuDeleteSuccess']);
        }
        if ($result['status'] == 'parent_exist') {
            $_SESSION['fl'] = array(0, $result['status_message']);
        }
        echo json_encode($result);
        exit;
           break;
    case 'AddEditGroup':
        $param['groupId'] = (isset($_POST['groupId'])) ? $_POST['groupId'] : '';
        $param['groupName'] = (isset($_POST['groupName'])) ? 
            $_POST['groupName'] : '';
        $param['selectid'] = (isset($_POST['selectid'])) ? $_POST['selectid'] : 0;
        $dataresult = $this->settings->InsertUpdateGroup($param);
        if ($dataresult['status_code'] == 1) {
            $params['userid'] = $_SESSION['user']['user_id'];
            $managegroup = $this->settings->fetchManagegroup($params);
            if ($managegroup['status_code'] == 1) {
                $dataresult = $managegroup['getGroupDetail'];
                $dataresult['status'] = 200;
            } else {
                $dataresult = 'No recordsd found';
            }
        }
        echo json_encode($dataresult);
        break;
    case 'deletegroup':
        $param = $_REQUEST;
        //$param['groupId'] = isset($_POST['deleteid']) ?$_POST['deleteid']:'';
        $dataresult = $this->settings->deleteGroup($param);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getGroup();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'AddEditBrand':
        $param['brandId'] = (isset($_POST['brandId'])) ? $_POST['brandId'] : '';
        $param['brandName'] = (isset($_POST['brandName'])) ? 
            $_POST['brandName'] : '';
        $param['brandDescription'] = (isset($_POST['brandDescription'])) ?
            $_POST['brandDescription'] : '';
        $param['selectid'] = (isset($_POST['selectid'])) ? $_POST['selectid'] : 0;
        $dataresult = $this->settings->InsertUpdateBrand($param);
        if ($dataresult['status_code'] == 1) {
            $params['userid'] = $_SESSION['user']['user_id'];
            $managebrand = $this->settings->fetchManagebrand($params);
            if ($managebrand['status_code'] == 1) {
                $dataresult = $managebrand['getBrandDetail'];
                $dataresult['status'] = 200;
            } else {
                $dataresult = 'No recordsd found';
            }
        }
        echo json_encode($dataresult);
        break;
    case 'deletebrand':
        $param = $_REQUEST;
        $dataresult = $this->settings->deleteBrand($param);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getBrand();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
		 case 'deleteProgram':
        $param = $_REQUEST;
        $dataresult = $this->settings->deleteProgram($param);
        /* if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getBrand();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        } */
        echo json_encode($dataresult);
        break;
    case 'AddEditQuestion':
        $param['questionnaireId'] = (isset($_POST['questionnaireId'])) ? 
            $_POST['questionnaireId'] : '';
        $param['questionnaireText'] = (isset($_POST['questionnaireText'])) ? 
            $_POST['questionnaireText'] : '';
        $param['group_name'] = (isset($_POST['group_name'])) ? 
            $_POST['group_name'] : '';
        $param['phase_name'] = (isset($_POST['phase_name'])) ? 
            $_POST['phase_name'] : '';
        $param['questopic'] = (isset($_POST['questopic'])) ? 
            $_POST['questopic'] : '';
        $param['questopictxt'] = (isset($_POST['questopictxt'])) ? 
            $_POST['questopictxt'] : '';
        $param['questxt'] = (isset($_POST['questxt'])) ? $_POST['questxt'] : array();
        $param['quesicon'] = (isset($_POST['quesicon'])) ? 
            $_POST['quesicon'] : array();
        $param['quesoptnumitems'] = (isset($_POST['quesoptnumitems'])) ? 
            $_POST['quesoptnumitems'] : array();
        //$param['activityques'] = (isset($_POST['activityques'])) ? $_POST['activityques'] : '';
        $param['questype'] = (isset($_POST['questype'])) ? $_POST['questype'] : '';
        $param['quesId'] = (isset($_POST['quesId'])) ? $_POST['quesId'] : '';
        $param['formId'] = (isset($_POST['formId'])) ? $_POST['formId'] : '';
        $param['selectid'] = (isset($_POST['selectid'])) ? $_POST['selectid'] : 0;
        $param['isquestypechange'] = (isset($_POST['isquestypechange'])) ? 
            $_POST['isquestypechange'] : 0;
        $param['orderid'] = (isset($_POST['orderid'])) ? $_POST['orderid'] : '';
        $param['score'] = (isset($_POST['score'])) ? $_POST['score'] : 0;
		
		  
        //echo json_encode($param);echo "<pre>";print_r($param);die;
        $dataresult = $this->settings->InsertUpdateQuestion($param);
        echo json_encode($dataresult);
        break;
    case 'AddEditEducation':
        $param['eduinfotxtitems'] = (isset($_POST['eduinfotxtitems'])) ? 
            $_POST['eduinfotxtitems'] : '';
        $param['group_name'] = (isset($_POST['group_name'])) ? 
            $_POST['group_name'] : '';
        $dataresult = $this->settings->InsertUpdateEducation($param);
        echo json_encode($dataresult);
        break;
    case 'getEducationByGroupId':
        $param = $_REQUEST;
        $param['groupid'] = (isset($_POST['groupid'])) ? $_POST['groupid'] : '';
        $dataresult = $this->settings->getEducationByGroupId($param);
        echo json_encode($dataresult);
        break;
    case 'getEducation':
        $param['groupid'] = (isset($_POST['groupid'])) ? $_POST['groupid'] : '';
        $dataresult = $this->settings->getEducation($param);
        echo json_encode($dataresult);
        break;
    case 'AddEditPageContent':
        $param = $_REQUEST;
        $dataresult = $this->settings->InsertUpdatePageContent($param);
        echo json_encode($dataresult);
        break;
    case 'deletePageContentById':
        $param = $_REQUEST;
        $dataresult = $this->settings->deletePageContentById($param);
        echo json_encode($dataresult);
        break;
    case 'editQuestionary':
        $param['questionnaireId'] = (isset($_POST['questionnaireId'])) ? 
            $_POST['questionnaireId'] : 0;
        $dataresult = $this->settings->getQuestionnaireDetail($param);
        echo json_encode($dataresult['getQuestionnaireDetailByID']);
        break;
    case 'deletequestionnaire':
        $param = $_REQUEST;
        //$param['groupId'] = isset($_POST['deleteid']) ?$_POST['deleteid']:'';
        $dataresult = $this->settings->deletequestionnaire($param);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getQuestionaries();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'savePhaseRange':
        $param = $_REQUEST;
        $dataresult = $this->settings->insertPhaseRange($param);
        echo json_encode($dataresult);
        break;
    case 'savePhaseWeight':
        $param = $_REQUEST;
        $dataresult = $this->settings->insertPhaseWeight($param);
        echo json_encode($dataresult);
        break;
    case 'getQuestion':
        $param = $_REQUEST;
        $dataresult = $this->settings->getQuestionaries($param);
        $questions = isset($dataresult['getQuestionaries']) ? 
            $dataresult['getQuestionaries'] : array();
        if ($dataresult['total_records'] == 1) {
            $questions = array($questions);
        }
        $tablecnt = '';
        if (($questions != '') && (count($questions) > 0)) {
            foreach ($questions as $res) {
                $elemid = $res['formelemid'];
                $tablecnt .= '<tr>
                                <td>'.$res['form_name'].'</td>
                                <td>'.$res['question'].'</td>
                                <td>'.$res['question_info'].'</td>
                                <td>
                                    <a title="Edit" class="btn-link btn-inline dotline-sep 
                                    icon-edit-menu" 
                                    onclick="editQuestion(\'Add/Edit Question\',
                                    \'addquestion\',\''.trim($elemid).'\',
                                    \''.trim($res['r_quesgroup_id']).'\',
                                    \''.trim($res['r_quesphase_id']).'\')">
                                    <span class="icon icon-edit"></span>
                                </a>';
                if ($res['is_static'] == 0) {
                    $tablecnt .= '<a title="Delete" class="btn-link';
                    $tablecnt .= ' btn-inline icon-delete-menu"';
                    $tablecnt .= ' onclick="deleteQuestion('.trim($elemid).','.
                        trim($res['r_quesgroup_id']).',';
                    $tablecnt .= trim($res['r_quesphase_id']).')">
                        <span class="icon icon-cls-sm"></span></a>';
                }
                $tablecnt .= '</td></tr>';
            }
        } else {
            $tablecnt .= '<tr><td colspan="4">No results found</td></tr>';
        }
        echo $tablecnt;
        break;
    case 'saveQuesActivity':
        $param = $_REQUEST;
        $dataresult = $this->settings->insertQuesActivity($param);
        echo json_encode($dataresult);
        exit;
        break;
    case 'deletelanguage':
        $param = $_REQUEST;
        $dataresult = $this->settings->deletelanguage($param);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getLanguageType();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'deletetranslation':
        $param = $_REQUEST;
        $dataresult = $this->settings->deletetranslation($param);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getTranslationType();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'getBrand':
        $params = $_REQUEST;
        $page = $this->settings->getBrand($params);
        break;

    case 'deleteBrand':
        $params = $_REQUEST;
        $dataresult = $this->settings->deleteBrand($params);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getBrand();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'saveBrandType':
        $params = $_REQUEST;
        $page = $this->settings->insertBrand($params);
		echo json_encode($page);
        break;
    case 'getLanguageDetails':
        $params = $_REQUEST;
        $languagedetails = $this->settings->getLanguageDetails();
        echo "Reached " . 123;
        print_r($languagedetails);
        $lang = array();
        if (count($languagedetails) > 0) {
            foreach ($languagedetails['languageTypeDetails'] as $row) {
                $lang[$row['language_id']][$row['translationkey_id']] 
                    = utf8_decode($row['translation_text']);
            }
        }
        echo json_encode($lang);
        break;
    case 'saveGroup':
        $params = $_REQUEST;
        $page = $this->settings->insertGroup($params);
        break;
    case 'getGroup':
        $params = $_REQUEST;
        $page = $this->settings->getGroup($params);
        break;
    case 'gettypelist':
        $params = $_REQUEST;
        $page = $this->settings->gettypelist($params);
        break;
    case 'getsubtypelist':
        $params = $_REQUEST;
        $page = $this->settings->getsubtypelist($params);
        break;
    case 'deleteGroup':
        $params = $_REQUEST;
        $dataresult = $this->settings->deleteGroup($params);
        if ($dataresult['status_code'] == 1) {
            $resultset = $this->settings->getGroup();
            $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'getGroupTopics':
        $params = $_REQUEST;
        $dataresult = $this->settings->getGroupTopics($params);
        echo json_encode($dataresult);
        break;
    case 'getQuestionById':
        $params = $_REQUEST;
        $dataresult = $this->settings->getQuestionById($params);
        $result = array();
        $response = array();
        $quesopt = array();
        $quesoptadio = array();
        $i = 0;
        foreach ($dataresult['getQuestionById'] as $data) {
            $result['form_id'] = $data['form_id'];
            $result['type'] = $data['type'];
            $result['topicid'] = $data['r_topic_id'];
            $result['quesid'] = $data['id'];
            $result['ui_order'] = $data['ui_order'];
            $result['is_static'] = $data['is_static'];
            $result['score'] = $data['score'];
            $result['icon'] = $data['icon'];

            $data['question'] = utf8_decode($data['question']);

            $response[] = $data;
            $i = $i + 1;
        }
        $result['queslang'] = $response;
        $questionoptions = $this->settings->getQuestionOptionById($params);
        $quesopt = $questionoptions['getQuestionOptionById'];
        if ($questionoptions['total_records'] == 1) {
            $quesopt = array($quesopt);
        }
        if ($quesopt == '') {
            $quesopt = array();
        }
        $result['quesopt'] = $quesopt;
        foreach ($quesopt as $opt) {
            if (isset($opt['formelemoptid'])) {
                $quesoptadio[$opt['formelemoptid']][] = $opt;
            }
        }
        $result['quesoptadio'] = $quesoptadio;
        echo json_encode($result);
        break;
    case 'getPhaseWeightage':
        $params = $_REQUEST;
        $dataresult = $this->settings->getPhaseWeightage($params);
        echo json_encode($dataresult);
        break;
    case 'getPhaseRangeById':
        $params = $_REQUEST;
        $dataresult = $this->settings->getPhaseRangeById($params);
        $result = array();
        foreach ($dataresult as $data) {
            $result['minrange'] = $data['min_range'];
            $result['maxrange'] = $data['max_range'];
            $result['topic'] = $data['r_topic_id'];
            $result['data'][$data['text_type']][] = $data;
        }
        echo json_encode($result);
        break;
    case 'deletePhaseRangeById':
        $params = $_REQUEST;
        $dataresult = $this->settings->deletePhaseRangeById($params);
        break;
    case 'getQuesPhaseGroupType':
        $params = $_REQUEST;
        $dataresult = $this->settings->getQuesPhaseGroupType($params);
        break;
    case 'getQuesPhaseGroup':
        $params = $_REQUEST;
        $dataresult = $this->settings->getQuesPhaseGroup();
        $result = array();
        foreach ($dataresult['getQuesPhaseGroup'] as $ques) {
            $result[$ques['r_phase_id']][] = $ques;
        }
        echo  json_encode($result);
        break;
    case 'getQuesPhaseGroupActivityPoints':
        $params = $_REQUEST;
        $dataresult = $this->settings->getQuesPhaseGroupActivityPoints($params);
        break;
    case 'getFormIdForPhaseAndGroupAndTopic':
        $params = $_REQUEST;
        $dataresult = $this->settings->getFormIdForPhaseAndGroupAndTopic($params);
        break;
    case 'getLanguageActiveType':
        $params = $_REQUEST;
        $dataresult = $this->settings->getLanguageActiveType($params);
        echo json_encode($dataresult);
        break;
    case 'deleteQuestionById':
        $param = $_REQUEST;
        $dataresult = $this->settings->deleteQuestionById($param);
        if ($dataresult['status_code'] == 1) {
            // $resultset = $this->settings->getLanguageType();
            // $resultset['status'] = 1;
        } else {
            $resultset = 'No result found';
        }
        if(!isset($resultset)){
            $resultset  =   'No result found';
        }
        echo json_encode($resultset);
        break;
    case 'getQuesPhaseRange':
        $params = $_REQUEST;
        $lang = 1;
        $dataresult = $this->settings->getQuesPhaseRange($lang);
        $phasesrangelist = isset($dataresult['phaserangeinfo']) ? 
            $dataresult['phaserangeinfo'] : array();
        if ($dataresult['total_records'] == 1) {
            $phasesrangelist = array($phasesrangelist);
        }
        $tablecnt = '';
        foreach ($phasesrangelist as $res) {
            if ($params['phaseid'] == $res['r_phase_id'] 
                && ($params['groupid'] == $res['r_group_id'])
            ) {
                $elemid = $res['id'];
                $tablecnt .= '<tr>
                                <td>'.$res['min_range'].'</td>
                                <td>'.$res['max_range'].'</td>
                                <td>'.$res['txt_success'].'</td>
                                <td>'.$res['txt_aw'].'</td>
                                <td>'.$res['txt_goal'].'</td>
                                <td>'.$res['txt_adv'].'</td>
                                <td>
                                    <a title="Edit" class="btn-link btn-inline dotline-sep 
                                        icon-edit-menu" onclick="editPhageRange(
                                        \'Add/Edit Phase Range\',
                                        \'testrange\',
                                        \''.trim($elemid).'\',
                                        \''.$res['r_phase_id'].'\',
                                        \''.$res['r_group_id'].'\');">
                                        <span class="icon icon-edit"></span></a>
                                    <a title="Delete" 
                                        class="btn-link btn-inline icon-delete-menu" 
                                        onclick="deletePhaseRange(\''.trim($elemid).'\',
                                            \''.$res['r_phase_id'].'\',
                                            \''.$res['r_group_id'].'\')">
                                        <span class="icon icon-cls-sm"></span>
                                    </a>
                                </td>
                            </tr>';
            }
        }
        echo $tablecnt;
        break;
    case 'getPageContentById':
        $params = $_REQUEST;

        $dataresult = $this->settings->getPageContentById($params);
        $pagecontent = isset($dataresult['getPageContentById']) ? 
            $dataresult['getPageContentById'] : array();
        if ($dataresult['total_records'] == 1) {
            $pagecontent = array($pagecontent);
        }
        $result = array();
        foreach ($pagecontent as $page) {  
            $result[$page['page_id']][] = $page;
            $result['pageid'] = $page['t_pageid'];
        }
		//print_r($result);
        echo json_encode($result);
        break;
    case 'getPageContent':
        $pagecont = $this->settings->getPageContentDetails();
        $pagelistrows = isset($pagecont['pagecontentdetails']) ? 
            $pagecont['pagecontentdetails'] : array();
        $tablecnt = '';
        if ($pagecont['total_records'] == 1) {
            $pagelistrows = array($pagelistrows);
        }
        $sno = 1;
        if (count($pagelistrows) > 0) {
            foreach ($pagelistrows as $res) {
                $elemid = (isset($res['page_id'])) ? trim($res['page_id']):-1;
                $tablecnt .= '<tr>
                                    <td>'.$sno.'</td>
                                    <td>'.$res['page_name'].'</td>
                                    <td>'.$res['title'].'</td>
                                    <td>'.$res['content'].'</td>
                                    <td>'.$res['createdon'].'</td>
                                    <td>'.$res['modifiedon'].'</td>
                                    <td class="actionwidth txt-center">
                                    <a title="Edit" 
                                        class="btn-link btn-inline 
                                            dotline-sep icon-edit-menu" 
                                        onclick="editPageContent
                                        (\'Edit Phase Range\',
                                        \'pagecontent\',\''.$elemid.'\');" >
                                        <span class="icon icon-edit"></span>
                                    </a>
                                    <a title="Delete" 
                                        class="btn-link btn-inline icon-delete-menu" 
                                        onclick="deletePageContent(\''.$elemid.'\')">
                                            <span class="icon icon-cls-sm"></span>
                                    </a>
                                </td>
                            </tr>';
                ++$sno;
            }
        } else {
            $tablecnt .= '<tr><td colspan="7">No Results Found</td></tr>';
        }
        echo $tablecnt;
        break;
    case 'saveDisease':
        $params = $_REQUEST;
        $dataresult = $this->settings->updateDisease($params);
        break;
    case 'copyQuestion':
        $params = $_REQUEST;
        $questionlist = $this->settings->getQuestionaries($params);
        //$questions = array();
        $questions = isset($questionlist['getQuestionaries']) ? 
            $questionlist['getQuestionaries'] : array();
        if ($questionlist['total_records'] == 1) {
            $questions = array($questions);
        }
        $param = array();
        if (($questions != '') && (count($questions) > 0)) {
            foreach ($questions as $ques) {
                $questionsdetail = array();
                $param['form_name'] = $ques['form_name'];
                $param['questopic'] = $ques['r_topic_id'];
                $param['group_name'] = $params['curgroup'];
                $param['phase_name'] = $params['curphaseid'];
                $param['activityques'] = $ques['activitylink'];
                $param['questype'] = $ques['type'];
                $param['quesicon'] = $ques['icon'];
                $param['id'] = $ques['formelement_id'];
                $questionoption = $this->settings->getQuestionById($param);
                $questionoptbyid = array();
                $questionoptbyid = isset($questionoption['getQuestionById']) ? 
                    $questionoption['getQuestionById'] : array();
                if ($questionoption['total_records'] == 1) {
                    $questionoptbyid = array($questionoptbyid);
                }
                $questionsdetail = $questionoptbyid;
                $questxt = array();
                foreach ($questionsdetail as $quesdet) {
                    $questxt[] = array(
                    'queslanguageid' => $quesdet['language_id'],
                    'value' => $quesdet['question'],
                    'quesinfo' => $quesdet['question_info'],
                    );
                }
                $quesopt = array();
                $quesoptnumitems = array();
                $optnumvalarr = array();
                $optradiovalarr = array();
                $formelemoptid = array();
                $optionval = '';
                $param['questxt'] = json_encode($questxt);
                $questionoptions = $this->settings->getQuestionOptionById($param);
                $quesopt = $questionoptions['getQuestionOptionById'];
                if ($questionoptions['total_records'] == 1) {
                    $quesopt = array($quesopt);
                }
                if (count($quesopt) > 0) {
                    foreach ($quesopt as $opt) {
                        if ($param['questype'] == 1) {
                            $optionval = $opt['formelement_option'];
                            $optnumvalarr[] = array(
                            'min' => $opt['min_value'],
                            'max' => $opt['max_value'],
                            'range' => $opt['range_points'],
                            );
                        } else {
                            $formelemoptid[$opt['formelemoptid']][] = $opt;
                        }
                    }
                }
                if ($param['questype'] == 1) {
                    $quesoptnumitems[] = array(
                        'value' => $optionval,
                        'optnumvalarr' => $optnumvalarr,
                    );
                } else {
                    foreach ($formelemoptid as $radioopt) {
                        $quesoptlang = array();
                        $optradioval = '';
                        foreach ($radioopt as $optradio) {
                            $optradioval = $optradio['range_points'];
                            $quesoptlang[] = array(
                             'optradiolanguageid' => $optradio['language_id'],
                                'value' => $optradio['options'],
                            );
                        }
                        $optradiovalarr[] = array(
                            'quesoptlang' => $quesoptlang,
                            'radiopoint' => $optradioval,
                        );
                    }
                }
                $param['quesoptnumitems'] = ($param['questype'] == 1) ? 
                    json_encode($quesoptnumitems) : json_encode($optradiovalarr);
                $dataresult = $this->settings->InsertUpdateQuestion($param);
            }
            if(!isset($dataresult)){
                $dataresult=array();
            }
            echo json_encode($dataresult);
        }
        break;
    case 'getQuestionaries':
        $param = $_REQUEST;
        $dataresult = $this->settings->getQuestionaries($param);
        $questions = isset($dataresult['getQuestionaries']) ? 
            $dataresult['getQuestionaries'] : array();
        if ($dataresult['total_records'] == 1) {
            $questions = array($questions);
        }
        $htmlcnt = '';
        $htmlcnt = '<option value="0">Select</option>';
        foreach ($questions as $ques) {
            $htmlcnt .= '<option value="'.$ques['formelemid'].'">'.
                $ques['question'].'</option>';
        }
        echo $htmlcnt;
        break;
    case 'editQuestionaryHint':
        $param['queshintId'] = (isset($_POST['queshintId'])) ? 
            $_POST['queshintId'] : 0;
        $dataresult = $this->settings->getQuestionHintsById($param);
        $result = $dataresult['getQuestionHintsById'];
        $response = array();
        foreach ($result as $hint) {
            $response[$hint['langid']][] = $hint;
        }
        echo json_encode($response);
        break;
    case 'deleteQuestionHintById':
        $param = $_REQUEST;
        $dataresult = $this->settings->deleteQuestionHintById($param);
        $resultset = 0;
        if ($dataresult['status_code'] == 1) {
            $resultset = 1;
        }
        echo $resultset;
        break;
    case 'getQuestionHint':
        $questionhintlist = $this->settings->getQuestionHints();
        $questionhintlisttrows = isset($questionhintlist['getQuestionHints']) ? 
            $questionhintlist['getQuestionHints'] : array();
        if ($questionhintlist['total_records'] == 1) {
            $questionhintlisttrows = array($questionhintlisttrows);
        }
        $htmlcnt = '';
        $i = 1;
        foreach ($questionhintlisttrows  as $hint) {
            $htmlcnt .= '<tr>';
            $htmlcnt .= '<td>'.$i.'</td>';
            $htmlcnt .= '<td>'.$hint['phase_name'].'</td>';
            $htmlcnt .= '<td>'.$hint['group_name'].'</td>';
            $htmlcnt .= '<td>'.$hint['t_lang_hint'].'</td>';
            $htmlcnt .= '<td>'.$hint['no_days'].'</td>';
            $htmlcnt .= '<td>'.$hint['created_on'].'</td>';
            $htmlcnt .= '<td class="txt-center" >';
            $questhintid = $hint['t_ques_hint_id'];
            $htmlcnt .= '<a title="Edit" class="btn-link btn-inline dotline-sep';
            $htmlcnt 
                .= ' icon-edit-menu" onclick="editQuesHint(\'Edit Questionaries';
            $htmlcnt .= ' Hint\',\'questionaryHints\','.$questhintid.');">';
            $htmlcnt .= '<span class="icon icon-edit"></span></a>';
            $htmlcnt .= '<a title="Delete" class="btn-link btn-inline ';
            $htmlcnt .= ' icon-delete-menu" onclick="deleteQuestionHint(';
            $htmlcnt .= $questhintid.');"><span class="icon icon-cls-sm">';
            $htmlcnt .= '</span></a></td>';
            $i = $i + 1;
        }
        echo $htmlcnt;
        break;
    case 'machineList':
        $param = $_REQUEST;
        $dataresult = $this->settings->machineList($param);
        echo json_encode($dataresult);
        break;
    case 'getNewMachinedetailsbyeditid':
        $param = $_REQUEST;
        $dataresult = $this->settings->getNewMachinedetailsbyeditid($param);
        echo json_encode($dataresult);
        break;
    case 'exportTranslationList':
        global $excelExporter;
        $translationlist = $this->settings->getTranslationkeyType();
        $translationlistrows 
            = isset($translationlist['translationkeyTypeDetails']) ? 
            $translationlist['translationkeyTypeDetails'] : array();
        if ($translationlist['total_records'] == 1) {
            $translationlistrows = array($translationlistrows);
        }
        unset($translationlist);
        $header = array(
            'ID', 'Translation Key', 'Translation Key Desc', 
            'Translation text in English', 'Translation text in Russian', 
            'Translation text in Dutch'
        );
        $excelData = array($header);
        foreach ($translationlistrows as $list) {
            $data = array();
            $translationarr = array();
            $param['translationkey_id'] = $list['translationkey_id'];
            $translist = $this->settings->getTranslations($param);
            $listtrans = isset($translist['translationlist']) ? 
                $translist['translationlist'] : array();
            if ($translist['total_records'] == 1) {
                $listtrans = array($listtrans);
            }
            foreach ($listtrans as $trans) {
                if (!isset($translationarr[$trans['r_translation_key']])) {
                    $translationarr[$trans['r_translation_key']] = array();
                }
                $transkey = $trans['r_translation_key'];
                $translangid = $trans['r_language_id'];
                $translationarr[$transkey][$translangid] 
                    = $trans['translation_text'];
            }
            $rowData = array($list['translationkey_id'],
                    $list['translation_key'],
                    $list['transaction_key_desc'],
                    isset($translationarr[$list['translationkey_id']][1]) ? 
                        $translationarr[$list['translationkey_id']][1] : '',
                    isset($translationarr[$list['translationkey_id']][2]) ? 
                        $translationarr[$list['translationkey_id']][2] : '',
                    isset($translationarr[$list['translationkey_id']][3]) ? 
                        $translationarr[$list['translationkey_id']][3] : '',
            );
            $excelData[] = $rowData;
        }
        $this->settings->setExcelAttributes(
            count($excelData), count($header), 'Translations'
        );
        $excelExporter->getActiveSheet()->fromArray($excelData, null, 'A1');
        $writer = PHPExcel_IOFactory::createWriter($excelExporter, 'Excel5');
        $filename = 'export_translation_'.time().'.xls';
        $writer->save(SERVICE_EXPORT_TRANSLATION_FILE.$filename);
        echo $filename;
        exit;
        break;
		
    case 'importTranslationList':
        $params = $_REQUEST;
        $filename = $params['importfile'];
        global $excelData;
        $excelData->read(SERVICE_IMPORT_TRANSLATION_FILE.$filename);
        $translations = array();
        for ($i = 2;$i <= ($excelData->sheets[0]['numRows']);++$i) {
            $ID = trim($excelData->val($i, 'A', 0));
            $translationkey = trim($excelData->val($i, 'B', 0));
            $translationkeydes = trim($excelData->val($i, 'C', 0));
            $translationtext_eng 
                = mysql_escape_string(trim($excelData->val($i, 'D', 0)));
            $translationtext_dut 
                = mysql_escape_string(trim($excelData->val($i, 'E', 0)));
            $translationtext_rus
                = mysql_escape_string(trim($excelData->val($i, 'F', 0)));
            $param['id'] = $ID;
            array_push(
                $translations, array(
                'translationKey' => $translationkey,
                'translationLanguageID' => LANG_ENG,
                'translationKeytext' => $translationtext_eng,
                )
            );
            array_push(
                $translations, array(
                'translationKey' => $translationkey,
                'translationLanguageID' => LANG_DUT,
                'translationKeytext' => $translationtext_dut,
                )
            );
            array_push(
                $translations, array(
                'translationKey' => $translationkey,
                'translationLanguageID' => LANG_RUS,
                'translationKeytext' => $translationtext_rus,
                )
            );
        }
        $params = array();
        $params['translations'] = $translations;
        $this->settings->UpdateImportedTranslations($params);

        break;
    }
}
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];

    switch ($action) {
    case 'getLanguageDetails':
        $params = $_REQUEST;
        $languagedetails = $this->settings->getLanguageDetails();
        $lang = array();
       // printLog($languagedetails);
        if (count($languagedetails) > 0) {
			//if(!empty($languagedetails['languageTypeDetails'])){
            foreach ($languagedetails['languageTypeDetails'] as $row) {
				
                $lang[$row['language_id']][$row['translationkey_id']] 
                    = utf8_decode($row['translation_text']);
          //  }
			}
        }
        echo json_encode($lang);
        break;
    case 'getdevicesettings':
        $params = $_REQUEST;
        $devicemachinedetail = $this->settings->getdevicesettings($params);
        $result = array();
        if (count($devicemachinedetail) > 0) {
            $result = $devicemachinedetail['devicemachinedetail'];
        }
         echo json_encode($result);
        break;
    case 'getLanguagePageContent':
        $pagecont = $this->settings->getLanguagePageContentDetails();
        $pagelistrows = isset($pagecont['getLanguagePageContentDetails']) ? 
            $pagecont['getLanguagePageContentDetails'] : array();
        $tablecnt = '';
        if ($pagecont['total_records'] == 1) {
            $pagelistrows = array($pagelistrows);
        }
        $result = array();
        foreach ($pagelistrows as $page) {
            $result[$page['languageid']][$page['page_id']][] = $page;
        }
            echo json_encode($result);
        break;
    case 'getPageThemeContent':
        $pagethemecont = '<style>header {background: rgb(255, 0, 0) !important;}';
        $pagethemecont .= 'footer {background: rgb(0, 0, 0) !important;}</style>';
        echo $pagethemecont;
        break;
    case 'getLanguagelist':
		
        $languagelist = $this->settings->getLanguageList();
        $langlist = array();
        if (count($languagelist) > 0) {
            foreach ($languagelist['languageTypeDetails'] as $row) {
                $langlist[] = $row;
            }
        }
		echo json_encode($langlist);
        break;
    case 'AddEditQuestionHints';
        $param = $_REQUEST;
        $dataresult = $this->settings->InsertUpdateQuestionHints($param);
        echo json_encode($dataresult);
        break;
    case 'getUserFitlevel':
        $param = $_REQUEST;
        $fitlevel_points = $this->settings->getUserFitlevel($param);
        $fitlevelanpoints = isset($fitlevel_points['pointfitlevel']) ? 
            $fitlevel_points['pointfitlevel'] : array();
        $response = array();
        $archievepoints = '';
        if ($fitlevel_points['total_records'] > 0) {
            foreach ($fitlevelanpoints as $pts) {
                $response['testid'] = $pts['testid'];
                $response['fitlevel'] = $pts['fitlevel'];
                $response['weight'] = $pts['weight'];
                $response['age_on_test'] = $pts['age_on_test'];
                $response['iant_hr'] = $pts['iant_hr'];
                $response['iant_p'] = $pts['iant_p'];

                $tainingdata = array();
                $tainingdata['f_cardiotrainingid'] = $pts['f_cardiotrainingid'];
                $tainingdata['f_trainingtype'] = $pts['f_trainingtype'];
                $tainingdata['f_trainingstdate'] = $pts['f_trainingstdate'];
                $tainingdata['f_trainingeddate'] = $pts['f_trainingeddate'];
                $tainingdata['f_cardiotrainingplanid'] 
                    = $pts['f_cardiotrainingplanid'];
                $tainingdata['f_weeknr'] = $pts['f_weeknr'];
                $tainingdata['f_points'] = $pts['f_points'];
                $tainingdata['f_min'] = $pts['f_min'];
                $tainingdata['f_max'] = $pts['f_max'];
                $tainingdata['f_min_a'] = $pts['f_min_a'];
                $tainingdata['f_max_a'] = $pts['f_max_a'];
                $response['trainingdata'][] = $tainingdata;

                $archievepoints += $pts['f_points'];
            }
        }
        $response['archievepoints'] = $archievepoints;
        echo json_encode($response);
        break;
        /*Calendar Settings:*/
    case 'getCalenderDetails':
        $params = $_REQUEST;
        $result = $this->settings->getCalenderDetails($params);
        echo json_encode($result);
        exit;
        break;
    case 'SaveCalenderNote';  
        $params = $_REQUEST;
        $result = $this->settings->SaveCalenderNote($params);
        echo json_encode($result);
        exit;
        break;
    case 'saveGoal':
        $params = $_REQUEST;
        $dataresult = $this->settings->updateGoal($params);
        break;
	case 'saveStrengthProgram':
	
        $params = $_REQUEST;
        $dataresult = $this->settings->saveStrengthProgram($params);
		echo json_encode($dataresult);
		
        break;
	case 'updatetimeslot':
	
        $params = $_REQUEST;
        $dataresult = $this->settings->updatetimeslot($params);
		echo json_encode($dataresult);
        break;
	case 'saveblockpin':
	
        $params = $_REQUEST;
        $dataresult = $this->settings->saveblockpin($params);
        break;
    }
}
