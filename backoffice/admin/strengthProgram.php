<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package StrengthProgram
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display strength list.
 */
    /* To List the strength Program in Dropdown */
    //To search param
    global $LANG;
    $param['searchType'] = (isset($_REQUEST['searchType']) ? 
        $_REQUEST['searchType'] : '');
    $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
        ($_REQUEST['searchType'] == 'gender' ? 
            (strstr($_REQUEST['searchValue'], 'f') ? 
                '1' : '0') : $_REQUEST['searchValue']) : '');

    /*To sort param , If the param label field is empty / 
        default value should define at else part.*/
    $param['labelField'] = (isset($_SESSION['pageName'][$_REQUEST['p']])) ?
        $_SESSION['pageName'][$_REQUEST['p']] : 'description';
    $param['sortType'] = (isset($_SESSION['pageName'][$_REQUEST['p']]) 
            && $_SESSION['sortType'] == 2) ? 'desc' : 'asc';

    //Company id
    $param['companyId'] = COMPANY_ID;
	
    //Pagination code starts
    $limitStart = 0;
    $limitEnd = PAGINATION_SHOW_PER_PAGE;

if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
    $limitEnd = $_REQUEST['offset'];
}
if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
    $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
}

    $param['limitStart'] = $limitStart;
    $param['limitEnd'] = $limitEnd;
    //Pagination code ends

    $param['companyId'] = COMPANY_ID;

    $arrayListMember = $this->settings->strengthProgramList($param);
	//print_r($arrayListMember['strengthProgramList']);
	$list = $this->settings->programlist($param);
	
 //$arrayList=array();
    $arrayList = $arrayListMember['strengthProgramList'];

    //Total count to create pagination
    $totalCount = isset($arrayListMember['totalCount']) ? 
        $arrayListMember['totalCount'] : 0;

    /* Search Labels */
    $customSearchArray = array(
        'description' => $LANG['description'],
        'type' => $LANG['type'],
    );
?>
<div class="content-wrapper" id="manage-members">
<div class="con-title-sec pos-fixed mt40">
  <h1><span class="icon icon-strength-test"></span>
        <?php echo $LANG['strengthProgram']; ?></h1>
    <div class="user-features">
        <ul>       
            <li>   
                <a href="../index.php?p=settings"
                   title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
        </ul>
    </div>
</div>
    <div class="con-sec pt100">
        <div class="row-sec" align="center"><br/>
            <div class="col9 successSetMessgae success-msg"
                align="center" style="display:none;">
                <div class="col9 fadeMsg"></div>
            </div>
        </div>
        <div class="row-sec ">
            <form name="membersearch" id="searchFilterForm"
                    action="" method="get">
                <!--div class="col6 widthcol3">
                    <label class="fl"><?php// echo $LANG['select']; ?> :</label>
                    <div>
                        <input type="hidden" name="p" value="strengthProgram">
                        <input type="hidden" name="theme" value="2">
                        <input type="hidden" name="labelField" id="labelField"
                            value="<?php// echo $param['labelField']; ?>">
                            <input type="hidden" name="sortType" id="sortType"
                            value="<?php// echo $param['sortType']; ?>">
                        </div>
                            <div class="select-custom">
                            <label for="searchType" style="display: none;"></label>
                                <select id="searchType" name="searchType">
                                    <option value="">-
                                    <?php //echo $LANG['choose']; ?>-</option>
                                    <?php
        /*foreach ($customSearchArray as $row => $value) {
            $sel = (isset($param['searchType']) && $param['searchType'] == $row) ?
                "selected='selected'" : '';
            echo "<option value='".$row."'>".$value.'</option>';
        }
                                */?>
                                </select>
                            </div>
                </div-->  
                <div class="col6 widthcol7">
                    <?php
                        if ($_SESSION['page_edit'] == 1) { ?>
                            <a href="javascript:void(0)" class="btn black-btn fr"
                            onclick="showQuickAddPop('Add Strength Program', 
                            'addstrengthpgm');">
                            <?php echo $LANG['addStrengthProgram'];
    ?></a>
        <?php 
} ?>  
                        <!--a href="index.php?p=strengthProgram">
                            <input type="button" value="<?php //echo $LANG['clear'];?>"
                                class="btn black-btn fr" id="clear_search" />
                        </a>
                        <input type="submit" value="<?php// echo $LANG['search'];?>"
                            id="searchFilterSubmit" class="btn black-btn fr" />
                        <label for="searchValue" style="display: none;"></label>
                        <input type="text" name="searchValue" id="searchValue"
                            class="fr wid40" value="<?php //echo
                            //isset($_REQUEST['searchValue']) ?
                            //$_REQUEST['searchValue'] : ''; ?>"/-->
                    </div>

                </form>
            </div><br/>
            <div class="question_box newstrenght_box" >
                <h2 class="pg_htr_h2" >Strenght Program List</h2>
                <div class="question_box_inr" >
                    <div class="qtn_inr">
                        <h2>Free</h2>
                        <div class="qtn_inr_bx divnone">
                            <div class="grid-block">
							
                                <table id="free" width="100%" border="0" cellspacing="0"
                                    cellpadding="0" id="">
									<thead>
                                    <tr>
                                        <th rowspan="2" >Number</th>
                                        <th rowspan="2" >Description</th>
                                        <th rowspan="2" >Type</th>
                                        <th colspan="5" >Standard parameter</th>
                                        <th rowspan="2">action</th>
                                    </tr>
                                    <tr>
                                        <th>Series</th>
                                        <th>Reps</th>
                                        <th>Time(sec)</th>
                                        <th>Strenght(%)</th>
                                        <th>Pause(sec)</th>
                                    </tr>
									</thead>
									<?php foreach($list['program'] as $program){
										
										if($program['type']==1){
										?>
										<tbody>
                                    <tr>
                                        <td><?php echo $program['strength_program_id']; ?></td>
                                        <td><?php echo $program['title_english']; ?></td>
                                        <td>Free</td>
                                        <td><?php echo $program['series']; ?></td>
                                        <td><?php echo $program['reps']; ?></td>
                                        <td><?php echo $program['time_sec']; ?></td>
                                        <td><?php echo $program['strength']; ?></td>
                                        <td><?php echo $program['pause']; ?></td>
                                        <td class="grid-width txt-center" >
			
				<?php if ($_SESSION['page_edit'] == 1) { ?>
                                            <a title="Edit" class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="showQuickAddPop('Add Strength Program', 
                            'addstrengthpgm','<?php echo $program['strength_program_id']; ?>')">
                                                <span class="icon icon-edit"></span>
                                            </a>
				<?php } 
				 if ($_SESSION['page_edit'] == 1) { ?>
                                            <a title="Delete" onclick="delete_program('<?php echo $program['strength_program_id']; ?>')"
                                                class="btn-link btn-inline
                                                align-center">
                                                <span class="icon icon-cls-sm"></span>
                                            </a>
				<?php } ?>
                                        </td>
                                    </tr>
									</tbody>
                                    <?php } } ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="question_box_inr" >
                    <div class="qtn_inr">
                        <h2>Circuit</h2>
                        <div class="qtn_inr_bx divnone">
                            <div class="grid-block">
                                <table id="circuit" width="100%" border="0" cellspacing="0"
                                    cellpadding="0" id="">
									<thead>
                                    <tr>
                                        <th rowspan="2" >Number</th>
                                        <th rowspan="2" >Description</th>
                                        <th rowspan="2" >Type</th>
                                        <th colspan="5" >Standard parameter</th>
                                        <th rowspan="2">action</th>
                                    </tr>
                                    <tr>
                                        <th>Series</th>
                                        <th>Reps</th>
                                        <th>Time(sec)</th>
                                        <th>Strenght(%)</th>
                                        <th>Pause(sec)</th>
                                    </tr> 
									</thead>
									<?php foreach($list['program'] as $program){
										
										if($program['type']==3){
										?>
										<tbody>
                                    <tr>
                                        <td><?php echo $program['strength_program_id']; ?></td>
                                        <td><?php echo $program['title_english']; ?></td>
                                        <td>Circuit</td>
                                        <td><?php echo $program['series']; ?></td>
                                        <td><?php echo $program['reps']; ?></td>
                                        <td><?php echo $program['time_sec']; ?></td>
                                        <td><?php echo $program['strength']; ?></td>
                                        <td><?php echo $program['pause']; ?></td>
                                        <td class="grid-width txt-center" >
                                          <?php if ($_SESSION['page_edit'] == 1) { ?>
                                            <a title="Edit" class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="showQuickAddPop('Add Strength Program', 
                            'addstrengthpgm','<?php echo $program['strength_program_id']; ?>')">
                                                <span class="icon icon-edit"></span>
                                            </a>
				<?php } ?>
                                            <a title="Delete" onclick="delete_program('<?php echo $program['strength_program_id']; ?>')"
                                                class="btn-link btn-inline
                                                align-center">
                                                <span class="icon icon-cls-sm"></span>
                                            </a>
                                        </td>
                                    </tr>
									</tbody>
                                    <?php } } ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="question_box_inr" >
                    <div class="qtn_inr">
                        <h2>Mix</h2>
                        <div class="qtn_inr_bx divnone">
                            <div class="grid-block">
                                <table id="mix" width="100%" border="0" cellspacing="0"
                                    cellpadding="0" id="">
									<thead>
                                   <tr>
                                        <th rowspan="2" >Number</th>
                                        <th rowspan="2" >Description</th>
                                        <th rowspan="2" >Type</th>
                                        <th colspan="5" >Standard parameter</th>
                                        <th rowspan="2">action</th>
                                    </tr>
                                    <tr>
                                        <th>Series</th>
                                        <th>Reps</th>
                                        <th>Time(sec)</th>
                                        <th>Strenght(%)</th>
                                        <th>Pause(sec)</th>
                                    </tr> 
									</thead>
									<?php foreach($list['program'] as $program){
										
										if($program['type']==2){
										?>
										<tbody>
                                     <tr>
                                        <td><?php echo $program['strength_program_id']; ?></td>
                                        <td><?php echo $program['title_english']; ?></td>
                                        <td>Mix</td>
                                        <td><?php echo $program['series']; ?></td>
                                        <td><?php echo $program['reps']; ?></td>
                                        <td><?php echo $program['time_sec']; ?></td>
                                        <td><?php echo $program['strength']; ?></td>
                                        <td><?php echo $program['pause']; ?></td>
                                        <td class="grid-width txt-center" >
                                                 <?php if ($_SESSION['page_edit'] == 1) { ?>
                                            <a title="Edit" class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="showQuickAddPop('Add Strength Program', 
                            'addstrengthpgm','<?php echo $program['strength_program_id']; ?>')">
                                                <span class="icon icon-edit"></span>
                                            </a>
				<?php } ?>
                                            <a title="Delete" onclick="delete_program('<?php echo $program['strength_program_id']; ?>')" 
                                                class="btn-link btn-inline
                                                align-center">
                                                <span class="icon icon-cls-sm"></span>
                                            </a>
                                        </td>
                                    </tr>
									</tbody>
                                    <?php } } ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <table>
        <?php 
       /*  if (isset($arrayList) && !empty($arrayList)) {
            foreach ($arrayList as $pgm) {
                $editLink = '';
                $deleteLink = '';
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                    <a title="{$LANG['titleEdit']}" 
                    class="btn-link btn-inline dotline-sep 
                    icon-edit-menu" 
                    onclick="editStrengthProgram({$pgm['programid']});" 
                    href="index.php?p=strengthProgramEdit&id={$pgm['programid']}">
                    <span class="icon icon-edit"></span></a>
EDIT_LINK;
                }

                if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                            <a title="{$LANG['titleDelete']}" 
                            class="btn-link btn-inline icon-delete-menu" 
                            onclick="deleteQuestionHint({$pgm['programid']});">
                            <span class="icon icon-cls-sm"></span></a>
EDIT_LINK;
                } */
                ?>
                <tr><td><?php //echo $pgm['description'];
                ?></td><td><?php //echo $pgm['type']?></td></tr>
        <?php
          //  }
       // } else {
          //  echo '<tr><td colspan="20">'.$LANG['noResult'].'</td></tr>';
       // }
                                ?>
    </table>
</div>
<div class="pagination-block">
    <?php
      /*   if (isset($arrayList) && !empty($arrayList) 
            && isset($arrayList[0]['strength_program_id'])
        ) {
            $this->paginator->getUrlLink($param = 1);
            echo $this->paginator->displayPagination($totalCount);
        } */
    ?>
</div>
<script>
$(document).ready(function(){
   // $('#free').DataTable();
	$('#mix').DataTable();
	//$('#circuit').DataTable();
});
function delete_program(program_id){
var confrm = confirm("Are you sure want to delete?");

if(confrm){
	
	 var dataString = "programId="+program_id+"&action=deleteProgram";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
				dataType: "json",
                success: function(data) {
				//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay('Program deleted successfully', 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
}
}
</script>

