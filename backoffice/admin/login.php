<?php


/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description All login Process
 */
//Check user already logged. 
if (!empty($_SESSION['user'])) {
    $path = 'index.php?p=dashboard';

    //If coach or admin should redirect to managemembers page
    if (isset($_SESSION['usertype_id']) && $_SESSION['usertype_id'] != 3) {
        $path = 'index.php?p=settings';
    }

   // ob_clean();
    header('Location: '.$path);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Movesmart Login</title> 
<link rel="shortcut icon" href="<?php echo IMG_PATH.DS; ?>fav.ico" />
<!--<link
    href="<?php /*echo FOLDER_PATH.CSS_PATH.
    'common/master.css?version='.MY_VERSION; */?>"
    rel="stylesheet" type="text/css"/>-->
<link 
    href="<?php echo FOLDER_PATH.CSS_PATH.
    'common/custom.css?version='.MY_VERSION; ?>" 
    rel="stylesheet" type="text/css"/>
<link 
    href="<?php echo FOLDER_PATH.CSS_PATH.
    'common/login.css?version='.MY_VERSION; ?>" 
    rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
      <!--<link rel="stylesheet" type="text/css" href="../../css/movesmart/ie8.css" />-- >
    <![endif]-->
</head>
<body>
<div id="customPopoverlay" style="min-height: 100%; width: 100%;"></div>
    <div id="customPopbox">
        <div class="customPopcontent">
            <div id="customPopmessage"></div>
        </div>
    </div>
    <!-- Page ajax loader
    <div id="pageLoader"></div>
    <div id="pageLoaderLightBox"></div> -->

    <div class="loginpage">
        <div class="log-content">
            <div class="loginlogo">
                <a href="#" >
                    <img src="../images/logo.png" alt="logo" />
                </a>
            </div>
            <div class="form-field">
			<h2>Development</h2>
                <h2>Login</h2>
                <form name="authentication" id="authentication" method="post">
                    <input type="text"  id="username" name="firstname" placeholder="User Name" title="User Name"/>
                    <input type="password" id="password"  name="password" placeholder="Password" title=""Password"/>
                    <div class="login-error" style="display:none"></div>
                    <br>
                    <input type="submit" value="Login" id="login">
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript" 
    src="<?php echo FOLDER_PATH.JS_PATH.
    '/library/jquery-1.9.1.js?version='.MY_VERSION; ?>"></script> 
<script type="text/javascript" 
    src="<?php echo FOLDER_PATH.JS_PATH.
    '/common.js?version='.MY_VERSION; ?>"></script> 
<script type="text/javascript" 
    src="<?php echo FOLDER_PATH.JS_PATH.
    'library/jqueryUi.js?version='.MY_VERSION; ?>"></script>
<script type="text/javascript" 
    src="<?php echo FOLDER_PATH, JS_PATH.
    'library/ckeditor/ckeditor.js?version='.MY_VERSION; ?>">
</script>
<script type="text/javascript" 
    src="<?php echo FOLDER_PATH,JS_PATH.'autoComplete.js?version='.MY_VERSION; ?>">
</script>

<!-- MultiLanguage Settings -->
<script type="text/javascript" 
    src="<?php echo FOLDER_PATH.'lang/en.js?version='.MY_VERSION; ?>">
</script>

</body>
</html>
