<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Coachclubroles
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get all the permission from role permission
 */
global $LANG;
$roles['page_view'] = 1;

if (USER_RIGHTS_CHECK_FOR_APPLICATION == 1) {
    $paramPg['fileName'] = 'userClubRoles';
    $perm = $this->login->getUserPagePermissions($paramPg);

    //Setting sessions for page access
    $roles['page_view'] = 0;

    if (is_array($perm) && isset($perm['view_f'])) {
        $roles['page_view'] = $perm['view_f'];
        $_SESSION['page_view'] = $perm['view_f'];
        $_SESSION['page_add'] = $perm['add_f'];
        $_SESSION['page_edit'] = $perm['edit_f'];
        $_SESSION['page_delete'] = $perm['delete_f'];
    }
}
//Get all the permission from role permission

if (isset($roles['page_view']) && $roles['page_view'] == 1) {
    ?>
<!-- Manage Roles -->
    <div class="row-sec coach-search-sec search-list-form">
    <?php 
    if ($_SESSION['page_add'] == 1 
        && ($_SESSION['user']['usertype_id'] == 4)
    ) { ?>
        <form name="coachsearch" 
        id="coachsearch" 
        action="" 
        method="post" 
        onSubmit="
            return validationForm(
                ['dropdown&clubIdRole&<?php echo $LANG['alertClub'];?>', 
                'dropdown&userTypeId&<?php echo $LANG['alertRole'];?>'],
                'coachsearch','2')">
        <div class="col6">
            <label><?php echo $LANG['club'];?> :</label>
            <div class="select-custom">
                <label for="clubIdRole" style="display: none;" ></label>
                <select id="clubIdRole" name="clubIdRole" >
                    <option value="">
                        --<?php echo $LANG['select'];?>--
                    </option>
                    <?php
                    $clubExistIds   =   array();
                    if(isset($members_center_id)) {
                        $clubExistIds[] = $members_center_id;
                    }
                    if(isset($clubList) and count($clubList)>0) {
                        foreach ($clubList as $row) {
                            $isFlag = 0;
                            if (is_array($clubExistIds)) {
                                $isFlag = in_array($row['club_id'], $clubExistIds);
                            }
                            if (!$isFlag) {
                                $sel = '';
                                if (isset($param['clubId'])
                                    && $row['club_id'] == $param['clubId']
                                ) {
                                    $sel = "selected='selected'";
                                }
                                echo "<option value='" . $row['club_id'] . "' 
                                    $selected>" .
                                    $row['club_name'] .
                                    '</option>';
                            }
                        }
                    } ?>
                </select>
            </div>
            <label><?php echo $LANG['role'];?> :</label>
            <div class="select-custom">
                <label for="userTypeId" style="display: none;"></label>
                <select id="userTypeId" name="userTypeId">
                    <option value="">--<?php echo $LANG['select'];?>--</option>
                    <?php
                    /*foreach($userType as $value){
                            echo "<option 
                                value='" . $value['usertype_id'] . "' 
                                $sel>".
                                ucfirst(strtolower($value['usertype'])).
                            "</option>";
                        }*/
                        ?>
                        <option value="1">Coach</option>
                        <option value="2">Admin</option>
                        <!--<option value="4">Back Office</option>-->
                </select>
            </div>
            <label>
                <input 
                    type="submit" 
                    name="assignRole" 
                    value="<?php echo $LANG['assign'];?>" 
                    class="btn black-btn" />
            </label>
            <!-- Custom search variable select option ends here -->
        </div>
    </form>
<?php 
    }
?>
    </div>
    <div class="clear">&nbsp;</div>
        <div class="grid-block" id="coachListGriddiv">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr class="grid-title">
                        <td>
                            <?php echo $LANG['club'];?>
                        </td>
                        <td>
                            <?php echo $LANG['role'];?>
                        </td>
                        <td>
                            <?php echo $LANG['isPrimary'];?>
                        </td>
                        <!-- ED 20160501 -->
                        <td class="txt-center" >
                            <?php echo $LANG['action'];?>
                        </td>
                    </tr>
                </thead>
                <?php
                if (isset($clubRoles) and count($clubRoles)) {
                    $roleHtml = '';
                    foreach ($clubRoles as $roles) {
                        $isPrimary=($roles['is_primary'] == 1) ? ' checked':'';
                        $isPrimaryValue = ($roles['is_primary'] == 1) ? '1' : '0';
                        $name = ucfirst(strtolower($roles['usertype']));
                        $clubName = $roles['club_name'];
                        $deleteLink = '';
                        if ($roles['is_primary'] == 0 
                            && $_SESSION['page_delete'] == 1 
                            && $_SESSION['user']['usertype_id'] == 4
                        ) {
                            $deleteLink = <<<DEL_LINK
                                <a deleteId="{$roles['employee_centre_id']}" 
                                href="javascript:void(0);" 
                                class="emp_role_club_delete btn-link btn-inline ">
                                <span class="icon icon-cls-sm"></span>
                            </a>
DEL_LINK;
                        }?>
                        <tr id="employee_club_role_tr_<?php echo $roles['employee_centre_id']?>">
                        <td><?php echo $clubName;?></td>
                        <td><?php echo $name;?></td>
                        <td>
                            <div class="in-cell cus-check get-check">
                                <input type="radio" class="oxy" name="primaryClub"
                                       id="primaryClub<?php echo $roles['employee_centre_id']?>"
                                       value="<?php echo $roles['employee_centre_id'];?>"
                                        <?php echo $isPrimary?>
                                       onclick=primaryClubUpdate('<?php echo $_REQUEST['id'];?>',
                                        '<?php echo $roles['r_club_id']?>',
                                        '<?php echo $roles['employee_centre_id']?>',
                                        '<?php echo $isPrimaryValue?>')>
                                <label for="primaryClub<?php echo $roles['employee_centre_id'];?>">&nbsp;</label>
                            </div>
                        </td>
                        <td><?php echo $deleteLink;?></td>
                        </tr>
                        <?php
                        /*$roleHtml .= <<<ROLES
                        <tr 
                        id="employee_club_role_tr_{$roles['employee_centre_id']}">
                        <td>{$clubName}</td>
                        <td>{$name}</td>
                        <td>
                            <div class="in-cell cus-check get-check">
                                <input 
                                    type="radio" 
                                    class="oxy" 
                                    name="primaryClub" 
                                    id="primaryClub{$roles['employee_centre_id']}" 
                                    value="{$roles['employee_centre_id']}" 
                                    {$isPrimary} 
                                    onclick=primaryClubUpdate({$_REQUEST['id']},
                                    {$roles['r_club_id']},
                                    {$roles['employee_centre_id']},
                                    {$isPrimaryValue})>
                                <label 
                                for="primaryClub{$roles['employee_centre_id']}">
                                </label>
                            </div>
                        </td>
                        <td>{$deleteLink}</td>
                        </tr>
ROLES;*/
                    }
                    //echo $roleHtml;
                } ?>
            </table>
        </div><?php 
} ?>