<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Index
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Main file to access the pages
 */

require '../include/define.php';
require_once BASE_DIR.'classes/class.controller.php';
$controller = new controller();
