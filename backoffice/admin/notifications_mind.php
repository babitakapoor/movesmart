<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Translation
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */
 /* To List the strength Program in Dropdown */
    global $LANG;
    $param['searchType'] = (isset($_REQUEST['searchType']) ? 
        $_REQUEST['searchType'] : '');
    $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
        ($_REQUEST['searchType'] == 'gender' ? 
            (strstr($_REQUEST['searchValue'], 'f') ? 
                '1' : '0') : $_REQUEST['searchValue']) : '');
$tabType = 3;
$records = 	$this->notifications->getNotifications($tabType);
$content = 	$this->notifications->getContent($tabType);
$specialcontent = 	$this->notifications->getSpecialContent($tabType);

$contentData = $content['data'];
if(isset($specialcontent['data']))
{
	$specailContentData = $specialcontent['data'];
}
///echo "<pre>";print_r($records);die;
?>
<!--To List the translation language -->
<style>
ul.tabs-main::after {
    clear: both;
    content: " ";
    display: block;
}
ul.tabs-main {
    border-bottom: 4px solid #0095ac !important;
}
ul.tabs-main {
    border-bottom: 4px solid #e40043;
}
ul.tabs-main li.current, ul.tabs-main li.current:hover {
    background: #0095ac none repeat scroll 0 0;
    border: 1px solid #0095ac;
}

ul.tabs-main li {
    background-color: #dfdfdf;
    border: 1px solid #dfdfdf;
    color: #4d515f;
    cursor: pointer;
    float: left;
    font-size: 14px;
    margin: 0 2px 0 0;
    min-width: 84px;
    text-align: center;
}
ul, li {
    list-style: outside none none;
    margin: 0;
}
ul.tabs-main li.current a {
    color: #ffffff !important;
}
ul.tabs-main li a {
    color: #363636;
    display: block;
    padding: 6px 10px;
}
.tabs-container-main {
    border-color: #0095ac;
}
.tabs-container-main {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #98a205 #cccccc #cccccc;
    border-image: none;
    border-style: solid;
    border-width: 0 1px 1px;
    padding: 18px;
}
</style>
<div class="content-wrapper" id="manage-members">
    <div class="con-sec pt100">
        
		<!-- Flash message begins -->
		<div>
			<?php
            if (isset($_SESSION['flMsg'])) 
			{
                if (isset($_SESSION['flMsg']['flashMessageError'])) {
                    echo '<div class="pageFlashMsg error">'.
                        $_SESSION['flMsg']['flashMessageError'].'</div>';
                } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                    echo '<div class="pageFlashMsg success">'.
                        $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
                }
                unset($_SESSION['flMsg']);
            } 
			?>
			<div>&nbsp;</div>
		</div>
		<div>
			<ul class="tabs-main">
				<li>
					<a href="../admin/index.php?p=notifications&notification_tab=movesmart">Move</a>
				</li>
				<li  class="current">
					<a href="javascript:void(0)">Mind</a>
				</li>
				<li>
					<a href="../admin/index.php?p=notifications&notification_tab=eat">Eat</a>
				</li>
			</ul>
            <div class="clear"></div>
			<div class="tabOuterDiv">
				<div class="tabs-container-main">
					<div class="tabscontentmain">
						<!--grid-->
						<ul class="tabs">
							<?php
								$check = 0;
								foreach($records['data']['tabs'] as $tabsDiv)
								{
							?>
									<li class="<?php echo ($check == 0)?'current':'' ?>">
										<a href="#tab-<?php echo $tabsDiv['slug'] ?>"><?php echo $tabsDiv['name'] ?></a>
									</li>
							<?php
									$check =1;
								}
							?>
						</ul>
						
						<div class="tabs-container">
							<?php
							foreach($records['data']['tabs'] as $tabsDiv)
							{
								$data = $records['data']['data'];
							?>
							<div id="tab-<?php echo $tabsDiv['slug'] ?>" class="tabscontent testResultListGrid">
								<div class="row-sec member-search-sec">
									<div class="">
										<?php 
											$urlMain = '?p=addnotification&notification_tab='.$tabsDiv['slug'].'&type='.$tabType.'&type_id='.$tabsDiv['id'];
											if ($_SESSION['page_add'] == 1) { 
										?>
											<a href="<?php echo $urlMain ?>" class="btn black-btn fr">
												Add Day
											</a>
										<?php 
											} 
										?>
									</div>
								</div>
								<div class="grid-block">
									<input type="hidden" class="paramNone" value="?p=strengthProgramEdit">
									<input type="hidden" id="strength_program_id" value="">
									<div class="pagination-block">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" id="memberListGridTab">
											<thead>
												<tr class="grid-title toggle-label" linkdata="<?php echo $_REQUEST['p'];?>">
													<td>Sr No</td>
													<!--<td>Notification message</td>-->
													<td>Notification day</td>
													<td>Action</td>
												</tr>
											</thead>
											<tbody class="trnslang">
											<?php
											$canEdit = false;
											$canDelete = false;
											if ($_SESSION['page_edit'] == 1) 
											{
												$canEdit = true;
											}
											if ($_SESSION['page_delete'] == 1) {
												$canDelete = true;
											}
											if(isset($data[$tabsDiv['slug']]) && !empty($data[$tabsDiv['slug']]))
											{
												$sno= 1;
												
												//$count = count($tabsDiv[$tabsDiv['slug']]);
												if(isset($data[$tabsDiv['slug']]) && !isset($data[$tabsDiv['slug']][0]))
												{
													$data[$tabsDiv['slug']] = array(0=>$data[$tabsDiv['slug']]);
												}
												foreach($data[$tabsDiv['slug']] as $notdata)
												{
													$deleteUrl = '?p=deletenotification&id='.$notdata['id'].'&notification_tab=mind';
													//echo "<pre>";print_r($notdata);die;
											?>
													<tr>
														<td><?php echo $sno?></td>
														<!--<td><?php echo $notdata['message']?></td>-->
														<td><?php echo $notdata['not_day'] ?></td>
														<td class="txt-center" >
															<?php 
															if($canEdit == true)
															{
															?>
																<a title="Edit" href ="<?php echo $urlMain ?>&id=<?php echo $notdata['id'] ?>" class="btn-link btn-inline dotline-sep align-center">
																	<span class="icon icon-edit"></span>
																</a>
															<?php
															}
															if($canDelete == true)
															{
															?>
																<a title="Delete" href ="<?php echo $deleteUrl ?>" class="btn-link btn-inline dotline-sep align-center">
																	<span class="icon icon-cls-sm"></span>
																</a>
															<?php
															}
															?>
														</td>
													</tr>
											<?php
													++$sno;
												}
											}
											else
											{
											?>
												<tr><td colspan="5">No Results Found</td></tr>
											<?php
											}
											?>
											</tbody>
										</table>
									</div>
								</div>
								<?php
								//echo $tabsDiv['slug'];die;
								if($tabsDiv['slug'] != 'behaviour_tip')
								{
								?>
									<br/>
									<br/>
									<div class="row-sec member-search-sec" style="text-align:center;font-weight:bold">
										Content
										<div class="">
											<?php 
												$url = '?p=addnotification&notification_tab='.$tabsDiv['slug'].'_content&type='.$tabType.'&type_id='.$tabsDiv['id'];
												if ($_SESSION['page_add'] == 1) { 
											?>
												<a href="<?php echo $url ?>" class="btn black-btn fr">
													Add Content
												</a>
											<?php 
												} 
											?>
										</div>
									</div>
									<div class="grid-block">
										<div class="pagination-block">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" id="memberListGridTab">
												<thead>
													<tr class="grid-title toggle-label" linkdata="<?php echo $_REQUEST['p'];?>">
														<td>Sr No</td>
														<!--<td>Notification message</td>-->
														<td>Topic Name</td>
														<td>Action</td>
													</tr>
												</thead>
												<tbody class="trnslang">
												<?php
												if(isset($contentData[$tabsDiv['slug']]) && !empty($contentData[$tabsDiv['slug']]))
												{
													$sno= 1;
													if(isset($contentData[$tabsDiv['slug']]) && !isset($contentData[$tabsDiv['slug']][0]))
													{
														$contentData[$tabsDiv['slug']] = array(0=>$contentData[$tabsDiv['slug']]);
													}
													foreach($contentData[$tabsDiv['slug']] as $cntData)
													{
														$deleteContentUrl = '?p=deletenotificationcontent&id='.$cntData['id'].'&notification_tab=mind';
														//echo "<pre>";print_r($notdata);die;
												?>
														<tr>
															<td><?php echo $sno?></td>
															<td><?php echo $cntData['topic_name'] ?></td>
															<td class="txt-center" >
																<?php 
																if($canEdit == true)
																{
																?>
																	<a title="Edit" href ="<?php echo $url ?>&id=<?php echo $cntData['id'] ?>" class="btn-link btn-inline dotline-sep align-center">
																		<span class="icon icon-edit"></span>
																	</a>
																<?php
																}
																if($canDelete == true)
																{
																?>
																	<a title="Delete" href ="<?php echo $deleteContentUrl ?>" class="btn-link btn-inline dotline-sep align-center">
																		<span class="icon icon-cls-sm"></span>
																	</a>
																<?php
																}
																?>
															</td>
														</tr>
												<?php
														++$sno;
													}
												}
												else
												{
												?>
													<tr><td colspan="5">No Results Found</td></tr>
												<?php
												}
												?>
												</tbody>
											</table>
										</div>
									</div>
									<?php
									if($tabsDiv['slug'] == 'top')
									{
									?>
										<br/>
										<br/>
										<div class="row-sec member-search-sec" style="text-align:center;font-weight:bold">
											Special Top
											<div class="">
												<?php 
													$urlContent = '?p=addnotification&notification_tab='.$tabsDiv['slug'].'_special_content&type='.$tabType.'&type_id='.$tabsDiv['id'];
													if ($_SESSION['page_add'] == 1) { 
												?>
													<a href="<?php echo $urlContent ?>" class="btn black-btn fr">
														Add Special Top
													</a>
												<?php 
													} 
												?>
											</div>
										</div>
										<div class="grid-block">
											<div class="pagination-block">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" id="memberListGridTab">
													<thead>
														<tr class="grid-title toggle-label" linkdata="<?php echo $_REQUEST['p'];?>">
															<td>Sr No</td>
															<!--<td>Notification message</td>-->
															<td>Special Top</td>
															<td>Action</td>
														</tr>
													</thead>
													<tbody class="trnslang">
													<?php
													if(isset($specailContentData[$tabsDiv['slug']]) && !empty($specailContentData[$tabsDiv['slug']]))
													{
														$sno= 1;
														if(isset($specailContentData[$tabsDiv['slug']]) && !isset($specailContentData[$tabsDiv['slug']][0]))
														{
															$specailContentData[$tabsDiv['slug']] = array(0=>$specailContentData[$tabsDiv['slug']]);
														}
														foreach($specailContentData[$tabsDiv['slug']] as $cntData)
														{
															$deleteContentUrl = '?p=deletenotificationcontent&id='.$cntData['id'].'&notification_tab=mind';
															//echo "<pre>";print_r($notdata);die;
													?>
															<tr>
																<td><?php echo $sno?></td>
																<td><?php echo $cntData['top_day_name'] ?></td>
																<td class="txt-center" >
																	<?php 
																	if($canEdit == true)
																	{
																	?>
																		<a title="Edit" href ="<?php echo $urlContent ?>&id=<?php echo $cntData['id'] ?>" class="btn-link btn-inline dotline-sep align-center">
																			<span class="icon icon-edit"></span>
																		</a>
																	<?php
																	}
																	if($canDelete == true)
																	{
																	?>
																		<a title="Delete" href ="<?php echo $deleteContentUrl  ?>" class="btn-link btn-inline dotline-sep align-center">
																			<span class="icon icon-cls-sm"></span>
																		</a>
																	<?php
																	}
																	?>
																</td>
															</tr>
													<?php
															++$sno;
														}
													}
													else
													{
													?>
														<tr><td colspan="5">No Results Found</td></tr>
													<?php
													}
													?>
													</tbody>
												</table>
											</div>
										</div>
									<?php
									}
									?>
								<?php
								}
								?>
							</div>
							<?php
							}
							?>
						</div>
					</div>
				</div>	
            </div>
        </div>
	</div>
</div>