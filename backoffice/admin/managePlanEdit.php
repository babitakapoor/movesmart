<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Manageplanedit
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Page to Add/Edit plan details.
 */

global $LANG;
//Get club Details

//Add user roles begins
$plan_ID = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$planDetails = array();
if ($plan_ID > 0) {
    $param['planId'] = $plan_ID;
    $getClubDetail = $this->admin->getPlanDetailById($param);
    $planDetails = $getClubDetail['rows'];
}

//To Save the Associated Club details.

if (isset($_POST['centerListRt']) || isset($_POST['centerListLf'])) {
    $paramPlanClub['clubId'] = implode(',', $_POST['centerListRt']);
    $paramPlanClub['unClubId'] = implode(',', $_POST['centerListLf']);
    $paramPlanClub['planId'] = $_POST['planId'];
    $paramPlanClub['companyId'] = COMPANY_ID;
    $paramPlanClub['loggedUserId'] = 1;
    $updatePlan = $this->admin->updatePlanClubDetails($paramPlanClub);
    if ($updatePlan['status'] == 'success') {
        $_SESSION['fl'] = array(1, $LANG['DetailsUpdateSuccess']);
    } else {
        $_SESSION['fl'] = array(0, $LANG['oopsError']);
    }
}
?>
<form method="post" id="planDetailsForm">
<div class="content-wrapper">  
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon icon-perinfo"></span>
            <?php echo $LANG['planManagement']; ?></h1>	   
        <div class="user-features">
            <ul>
                <li><a title="<?php echo $LANG['titleSave']; ?>" class="btn-link">
                        <span class="icon icon-save" id="btnSavePlan"></span></a>
                </li>
                <li>
                    <a href="index.php?p=managePlan"
                        title="<?php echo $LANG['titleBack']; ?>">
                        <span class="icon icon-back"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="con-sec pt100">
            <input type="hidden" id="companyId" name="companyId" 
                value="<?php echo COMPANY_ID; ?>"/>
            <input type="hidden" id="planId" name="planId"
                value="<?php echo isset($planDetails['plan_id']) ?
                $planDetails['plan_id'] : ''; ?>"/>
            <div class="accordion-holder mb20">
                <div class="acc-row">
                    <div class="row-sec" align="center"><br/>

                        <div class="col9 successSetMessgae success-msg"
                            align="center" style="display:none;">
                        <div style="background-color:#fff;color:#000;">
                        <?php echo $LANG['requiredResult']; ?></div>
                        </div>
        <?php 
        if (isset($_SESSION['flMsg'])) {
            if (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                ?>
                <div class="pageFlashMsg success">
                    <div class="cussuccess-icon">
                        <img src="<?php echo IMG_PATH.DS.THEME_NAME.DS.'tick_success.png';?>">
                    </div>
                    <div>
                        <?php echo $_SESSION['flMsg']['flashMessageSuccess']?>
                    </div>
                </div>
                <?php
            }
            if (isset($_SESSION['flMsg']['flashMessageError'])) {
                echo '<div class="pageFlashMsg error"><div>'.
                $_SESSION['flMsg']['flashMessageError'].
                '</div></div>';
            }
            unset($_SESSION['flMsg']);
        }
                            ?>
                    </div>
            <div class="acc-content"> 
                <div class="row-sec">
                    <div class="col8">
                        <div class="row-sec">
                            <div class="col3">
                                <label class="fl">
                                    <?php echo $LANG['planName']; ?> :
                                    <span class="required">*</span></label>
                                <input type="text" id="plan_name" 
                                    name="plan_name" class="form-control" 
                                    placeholder="<?php echo $LANG['planName']; ?>" 
                                    value="<?php 
                                        echo isset($planDetails['plan_name']) ? 
                                        $planDetails['plan_name'] : ''; ?>"/>
                            </div>
                            <div class="col3">
                                <label class="fl"><?php echo $LANG['planType']; ?> :
                                    </label>
                                <div class="select-custom">
                                    <label for="type" style="display: none;"></label>
                                    <select id="type" name="type" 
                                        class="planManagementType">
                                        <option value="0">
                                            <?php echo $LANG['planTypeDefault']; ?>
                                                </option>
                                        <option value="1" <?php echo 
                                            (isset($planDetails['type']) 
                                            && $planDetails['type'] == 1) ? 
                                            'selected' : '';?>><?php 
                                            echo $LANG['planTypeOneoff']; 
                                            ?></option>
                                        <option value="2" <?php echo 
                                            (isset($planDetails['type']) 
                                                && $planDetails['type'] == 2) ?
                                                'selected' : '';?>>
                                                <?php echo $LANG['planTypeVzw']; 
                                                ?></option>
                                        <option value="3" <?php 
                                            echo (isset($planDetails['type']) && 
                                            $planDetails['type'] == 3) ? 'selected'
                                            : '';?>><?php 
                                            echo $LANG['planTypeSession']; ?>
                                            </option>
                                    </select>
                                </div>										
                            </div>
                            <div class="col3">	
                                <label class="fl"><?php echo $LANG['isActive']; ?> :
                                </label>
                                <div class="input-group">
                                    <div class="in-cell cus-check get-check">
                                        <input type="radio" class="oxy" value="1" 
                                            name="is_active" id="active_yes" 
                                            <?php echo 
                                                (isset($planDetails['is_active']) 
                                                && $planDetails['is_active'] == 1) ?
                                                'checked' : '';?> />
                                        <label for="active_yes">
                                        <?php echo $LANG['activeYes']; ?></label>
                                    </div>
                                    <div class="in-cell cus-check get-check">
                                        <input type="radio" class="oxy" 
                                            value="0" name="is_active" 
                                            id="active_no" <?php 
                                            echo (isset($planDetails['is_active']) 
                                            && $planDetails['is_active'] == 0) 
                                            ? 'checked' : '';?> />
                                        <label for="active_no"><?php echo 
                                            $LANG['activeNo']; ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-sec">
                            <input type="hidden" id="external_plan_id" 
                                name="external_plan_id" value="<?php 
                                    echo isset($planDetails['external_plan_id']) ? 
                                    $planDetails['external_plan_id'] : ''; ?>" />
                            <div class="col3">
                                <label class="fl"><?php 
                                    echo $LANG['validFrom']; ?> :</label>
                                <input type="text" class="form-control 
                                    icon-datepiker" placeholder="<?php 
                                        echo 
                                        $LANG['validFrom']; ?>" id="validity_from" 
                                        name="validity_from" value="<?php 
                                        echo isset($planDetails['validity_from']) ?
                                        $planDetails['validity_from'] : ''; ?>"
                                        />
                            </div>
    <?php
                                $egClass = (isset($planDetails['type']) && 
                                $planDetails['type'] == 1) ? 'none' : 'show';
                            ?>
                            <div class="planEngagePeriod"  
                                style="display:<?php echo $egClass; ?>">
                            <div class="col3">
                                <label class="fl" for="minimum_engagement_period" ><?php echo 
                                    $LANG['engagementPeriod']; ?> :</label>
                                <div class="select-custom">
                                    <select id="minimum_engagement_period" 
                                        name="minimum_engagement_period">
    <?php 
    $period = $planDetails['minimum_engagement_period'];
    for ($i = 1; $i <= 11; ++$i) {
        $sel = ($period == $i) ? 'selected="selected"' : '';
        echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
    }
                                    ?>
                                    </select>
                                </div>		
                                <div class="select-custom">
                                    <label for="engagement_period_type" style="display: none;"></label>
                                    <select id="engagement_period_type" 
                                        name="engagement_period_type">
                                        <option value="1" 
                                <?php echo 
                                    (isset($planDetails['engagement_period_type']) 
                                    && $planDetails['engagement_period_type'] == 1)
                                    ? 'selected' : '';?>>Month</option>
                                    <option value="2" <?php echo 
                                    (isset($planDetails['engagement_period_type']) 
                                    && $planDetails['engagement_period_type'] == 2) 
                                    ? 'selected' : '';?>>Year</option>
                                    </select>
                                </div>
                            </div>	
                            </div>	
                        </div>	
                        <div class="row-sec">
                            <div class="col3">	
                                <label class="fl"><?php echo 
                                    $LANG['isDefault']; ?> :</label>
                                <div class="input-group">
                                    <div class="in-cell cus-check get-check">
                                        <input type="radio" class="oxy" 
                                            value="1" name="is_default" 
                                            id="default_yes" <?php echo 
                                            (isset($planDetails['is_default']) 
                                                && $planDetails['is_default'] == 1) 
                                                ? 'checked' : '';?> />
                                        <label for="default_yes"><?php 
                                            echo $LANG['activeYes']; 
                                            ?></label>
                                    </div>
                                    <div class="in-cell cus-check get-check">
                                        <input type="radio" class="oxy" value="0" 
                                            name="is_default" id="default_no" 
                                    <?php echo (isset($planDetails['is_default']) 
                                    &&
                                    $planDetails['is_default'] == 0) ?
                                    'checked' : '';?> />
                                        <label for="default_no"><?php 
                                        echo $LANG['activeNo']; ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- Session section begins -->
                <div class="acc-row form-control-groub">
                    <h2><?php echo $LANG['planSessions']; ?><span class="icon 
                        icon-down"></span></h2>
                    <div class="acc-content planSessionAll" style="display:<?php 
                        echo ((isset($planDetails['type'])) && 
                            ($planDetails['type'] != 3)) ? 
                            'block' : 'none' ?>">
                        <div class="row-sec">
                            <div class="col3">
                                <label class="fl"><?php
                                    echo $LANG['totalSession']; ?> :</label>
                                <input type="text" class="form-control"
                                    placeholder
                                    ="<?php echo $LANG['totalSession']; ?>" 
                                    id="total_session" name="total_session" 
                                    value="<?php 
                                    echo isset($planDetails['total_session']) ?
                                    $planDetails['total_session'] : ''; ?>" />
                            </div>
                            <div class="col3">
                                <label class="fl">
                                    <?php
                                    echo $LANG['outdoorSession']; ?> :</label>
                                <input type="text" class="form-control"
                                       placeholder="<?php echo $LANG['outdoorSession']; ?>"
                                id="outdoor_session" name="outdoor_session" 
                                value="<?php 
                                echo isset($planDetails['outdoor_session']) ?
                                $planDetails['outdoor_session'] : ''; ?>" />
                            </div>
                            <div class="col3">
                                <label class="fl"><?php echo
                                    $LANG['indoorSession']; ?> :</label>
                                <input type="text"
                                       class="form-control"
                                       placeholder="<?php
                                        echo $LANG['indoorSession'];
                                       ?>"
                                       id="indoor_session"
                                       name="indoor_session"
                                       value="<?php echo
                                       isset($planDetails['indoor_session']) ?
                                           $planDetails['indoor_session'] : '';
                                       ?>" />
                            </div>
                            <div class="col3">
                                <label class="fl"><?php
                                    echo $LANG['freeSession']; ?> :</label>
                                <input type="text"
                                       class="form-control"
                                       placeholder="<?php
                                        echo $LANG['freeSession'];
                                       ?>"
                                       id="free_session"
                                       name="free_session"
                                       value="<?php
                                        echo isset($planDetails['free_session']) ?
                                            $planDetails['free_session'] : ''; ?>"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="acc-content planSessionMax"
                        style="display:<?php echo ((isset($planDetails['type'])) 
                        && ($planDetails['type'] == 3)) ? 'block' : 'none' ?>">
                        <div class="row-sec">
                            <div class="col3">
                                <label class="fl">
                                    <?php echo $LANG['maxSessions']; ?> :</label>
                                <input type="text"
                                       class="form-control"
                                       placeholder="<?php echo $LANG['maxSessions']; ?>"
                                       id="max_sessions"
                                       name="max_sessions"
                                       value="<?php
                                        echo isset($planDetails['max_sessions']) ?
                                            $planDetails['max_sessions'] : '';
                                       ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Session section ends -->
                <!-- Payment section begins -->
                <div class="acc-row form-control-groub">
                    <h2><?php echo $LANG['planPayment']; ?>
                        <span class="icon icon-down"></span></h2>
                    <div class="acc-content">
                        <div class="row-sec">
                            <div class="col3">
                                <label class="fl">
                                <?php echo $LANG['paymentMethod']; ?>
                                :<span class="required">*</span></label>
                                <div class="input-group">
                                    <div class="in-cell cus-check get-check">
                                        <input type="checkbox" class="oxy" value="1"
                                        name="is_payment_dom"
                                        id="fclub" <?php echo
                                        (isset($planDetails['is_payment_dom']) &&
                                        $planDetails['is_payment_dom'] == 1) ?
                                        'checked' : '';?> />
                                        <label for="fclub"><?php echo
                                        $LANG['paymentDom']; ?></label>
                                    </div>
                                    <div class="in-cell cus-check get-check">
                                        <input type="checkbox" class="oxy"
                                            value="1" name="is_payment_cash"
                                            id="is_payment_cash" <?php echo
                                            (isset($planDetails['is_payment_cash'])
                                            &&
                                            $planDetails['is_payment_cash'] == 1) ?
                                            'checked' : '';?> />
                                        <label for="is_payment_cash"><?php echo
                                        $LANG['paymentCash']; ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col3">
                                <label class="fl"><?php echo $LANG['price']; ?>
                                :</label>
                                <input type="text" class="form-control"
                                    placeholder="<?php echo $LANG['price']; ?>"
                                    id="price" name="price" value="<?php echo
                                    isset($planDetails['price']) ?
                                    $planDetails['price'] : ''; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Payment section ends -->

                    <input type="hidden" class="form-control" value="<?php
                        echo $_SESSION['user']['user_id']; ?>"
                        name="loggedUserId"  />
            </div>
            </form>

            <!-- Page tabs start -->
    <?php if ($plan_ID > 0) { ?>
            <div class="acc-row form-control-groub coach-role-left">
                <div class="con-sec">
                    <div class="tabOuterDiv">
                        <ul class="tabs">
                            <li class="current">
                            <a href="#tab-1">Associated Clubs</a></li>
                        </ul>
                        <div class="clear"></div>
                        <div class="tabs-container">
                            <div id="tab-1" class="tabscontent">
                                <?php
                                    /* To List the Clubs in Dropdown */
                                    $params['company_id'] = COMPANY_ID;
        $params['is_deleted'] = 0;
        $clubList = $this->club->getClubList($params);

                                    /* To List the Plan clubs */
                                    $params['planId'] = $plan_ID;
        $planClubList = $this->admin->getPlanClubList($params);
        $planClubList = isset($planClubList['clubPlanlist']) ?
        $planClubList['clubPlanlist'] : array();
        include_once 'associateClubs.php';
    ?>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Page tabs end -->
        </div>
    <?php 
} ?>
