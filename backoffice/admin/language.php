<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Language
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list
 */
global $LANG;
 /* To List the strength Program in Dropdown */
    $param['searchType'] = (
        isset($_REQUEST['searchType']) 
    ) ? $_REQUEST['searchType'] : '';
    $param['searchValue'] ='';
    if (isset($_REQUEST['searchValue'])) {
        if ($_REQUEST['searchType'] == 'gender') {
            $param['searchValue'] =   strstr($_REQUEST['searchValue'], 'f') ?
                '1' : '0'; 
        } else {
            $param['searchValue'] =   $_REQUEST['searchValue'];
        }
    }
    /*To sort param , If the param label field is empty / default value 
        should define at else part.*/
    $param['labelField'] = (
        isset($_SESSION['pageName'][$_REQUEST['p']])
    ) ? $_SESSION['pageName'][$_REQUEST['p']] : 'description';
    $param['sortType'] = (
        isset($_SESSION['pageName'][$_REQUEST['p']]) 
        && $_SESSION['sortType'] == 2
    ) ? 'desc' : 'asc';

    //Company id
    $param['companyId'] = COMPANY_ID;

    //Pagination code starts
    $limitStart = 0;
    $limitEnd = PAGINATION_SHOW_PER_PAGE;

    if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
        $limitEnd = $_REQUEST['offset'];
    }
    if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
        $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
    }

    $param['limitStart'] = $limitStart;
    $param['limitEnd'] = $limitEnd;
    //Pagination code ends
    
    $languageList = $this->settings->getLanguageType();
    $languageListRows = (
        isset($languageList['languageTypeDetails']) 
    )? $languageList['languageTypeDetails'] : array();
    if ($languageList['total_records'] == 1) {
        $languageListRows = array($languageListRows);
    }
    ?>
<!--To List the language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1>
        <span class="icon icon-set"></span>
        <?php echo $LANG['language']; ?>
      </h1>
      <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="con-sec pt100">
    <div class="row-sec member-search-sec">
        <form name="managelang" id="managelang" action="" method="post">
            <div class="col6">
                <label>&nbsp;</label>
            </div>
            <div class="col6">
    <?php if ($_SESSION['page_add'] == 1) { ?>
        <a href="javascript:void(0);"
            onclick="editLanguage('Add Language','language');"
            class="btn black-btn fr">
            <?php echo $LANG['addLanguage'];?>
        </a>
    <?php
    } ?>
            </div>
    </form>
    </div>

    <!-- Flash message begins -->
    <div>
    <?php
    if (isset($_SESSION['flMsg'])) {
        if (isset($_SESSION['flMsg']['flashMessageError'])) {
            echo '<div class="pageFlashMsg error">'.
                $_SESSION['flMsg']['flashMessageError'].'</div>';
        } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
            echo '<div class="pageFlashMsg success">'.
                $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
        }
        unset($_SESSION['flMsg']);
    }

    ?>
    <div>&nbsp;</div>
    </div>
    <!-- Flash message ends -->
    <div class="tabOuterDiv">
        <ul class="tabs">
            <li class="current">
                <a href="#tab-1"><?php echo $LANG['language']; ?></a>
            </li>
        </ul>
        <div class="clear"></div>
            <div class="tabs-container">
                <div id="tab-1" class="tabscontent testResultListGrid">
                    <div class="clear"></div>
                    <p class="mb15"><span class="count-block"></span> </p>
                    <!--grid-->
                    <div class="grid-block">
                        <input type="hidden" 
                            class="paramNone" value="?p=strengthProgramEdit"> 
                        <input type="hidden" id="strength_program_id" value="">
                        <!--If navigate back to the page. 
                        Move the all query string to another page -->
                        <div class="pagination-block">
                        <table 
                            width="100%" 
                            border="0" 
                            cellspacing="0" 
                            cellpadding="0" 
                            id="memberListGridTab">
                            <!--<div class="pagination-block">-->
                                <thead>
                                    <tr class="grid-title toggle-label" 
                                        linkdata="<?php echo $_REQUEST['p']; ?>">
                                        <td>							  
                                            <?php echo $LANG['sno'];?>
                                        </td>
                                        <td>
                                            <?php echo $LANG['language'];?>
                                        </td>
                                        <td>
                                            <?php echo $LANG['created_on'];?>
                                        </td>
                                        <td>
                                            <?php echo $LANG['modified_on'];?>
                                        </td>
                                        <td> 
                                            <?php echo $LANG['status'];?>
                                        </td>
                                        <td class="actionwidth ">
                                            <?php echo $LANG['action'];?>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody class="langappnt">
        <?php 
        $sno = 1;
        if (count($languageListRows) > 0) {
            foreach ($languageListRows as $language) {
                $editLink = '';
                $deleteLink = '';
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                        <a title="{$LANG['titleEdit']}" 
                            class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="editLanguage('Edit language',
                            'language',
                            {$language['language_id']});">
                            <span class="icon icon-edit"></span>
                        </a>
EDIT_LINK;
                }

                if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                    <a 
                        title="{$LANG['titleDelete']}" 
                        class="btn-link btn-inline icon-delete-menu" 
                        onclick="deletelanguage('Add/Edit language',
                            'deletelanguage',
                            {$language['language_id']});">
                        <span class="icon icon-cls-sm"></span>
                    </a>
EDIT_LINK;
                }
                ?>
                <tr>
                    <td><?php echo $sno?></td>
                    <td><?php echo $language['title']?></td>
                    <td><?php echo $language['created_on']?></td>
                    <td><?php echo $language['modified_on']?></td>
                    <td>
                        <?php echo ($language['active'] == 1) ? 
                            'Active' : 'Inactive';?>
                    </td>
                    <!--Ed 20160501 -->
                    <td class="txt-center" ><?php echo $editLink.$deleteLink?></td>
                </tr>
            <?php 
                $sno++;
            }
        } else { ?>
            <tr><td colspan="5">No Results Found</td></tr>
        <?php 
        } 
        ?>
                                   </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
    </div>
</div>