<?php

/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Page used to show settings.
 */
global $LANG;
?>

<script>
function myFunction() {
    alert("Page Coming Soon...");
}
</script>

<div class="content-wrapper" id="coach-list">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-set"></span>Settings</h1>
      <div class="user-features">
        <ul>
          <!--li><a href="#" class="btn-link">
          <span class="icon icon-close"></span></a></li-->
        </ul>
      </div>
    </div>
    <div class="con-sec pt100">
   <!--  <div class="select-custom clear-left-menu"><select>
   </select></div> --> <!--EDISON 20160120 -->
        <div class="row-sec" align="center">
            <div class="col9 successSetMessgae success-msg"
                 align="center" style="display:none;">
                <div class="col9 fadeMsg"></div>
            </div>
        </div>
        <div class="row-sec coach-search-sec search-list-form">

        </div>
        <div>
            <ul class="set-permission">
                <li <?php echo  ($_SESSION['user']['usertype_id'] == ROLE_ADMIN) ?
                    "style='display:inline-block'" : 
                    "style='display:none'"; ?>>
                    <a href="../admin/index.php?p=manageMenu"><span 
                        class="set-icon sp-icon-str"></span>
                <?php echo $LANG['manageMenu']; ?></a></li>
                <li><a href="../admin/index.php?p=manageGroup"><span
                    class="set-icon sp-icon-group"></span>
                <?php echo $LANG['manageGroup']; ?></a></li>
                <li><a href="../admin/index.php?p=manageClub"><span
                    class="set-icon sp-icon-club"></span>
                <?php echo $LANG['manageClub']; ?></a></li>
                <li><a href="../admin/index.php?p=coachList"><span
                    class="set-icon sp-icon-coachman"></span>
                <?php echo $LANG['coachManagement']; ?></a></li>
                <li><a href="../admin/index.php?p=managePermissions"><span
                    class="set-icon sp-icon-per"></span>
                <?php echo $LANG['setPermissions']; ?></a></li>
                <li><a href="../admin/index.php?p=question"><span
                    class="set-icon sp-icon-str"></span>
                <?php echo $LANG['manageQuestionaries']; ?></a></li>
                <li><a href="../admin/index.php?p=manageBrand"><span
                    class="set-icon sp-icon-brand"></span>
                <?php echo $LANG['manageBrand']; ?></a></li>
                <li ><a href="../admin/index.php?p=strengthProgram"><span
                    class="set-icon sp-icon-strpro"></span>
                <?php echo $LANG['strengthProgram']; ?></a></li>
                <li onclick="myFunction()"><a href="javascript:void(0)" ><span
                    class="set-icon sp-icon-plan"></span>
                <?php echo $LANG['managePlan']; ?></a></li>
                <li><a href="../admin/index.php?p=language"><span
                    class="set-icon sp-icon-language"></span>
                <?php echo $LANG['language']; ?></a></li>
                <li><a href="../admin/index.php?p=translation"><span
                    class="set-icon sp-icon-translation"></span>
                <?php echo $LANG['translation']; ?></a></li>
                <li><a href="../admin/index.php?p=pagecontent"><span
                    class="set-icon pagecontent_icon"></span>
                <?php echo $LANG['Pagecontent']; ?></a></li>
                <li><a href="../admin/index.php?p=questionaryHints"><span
                    class="set-icon quest_icon"></span>
                <?php echo $LANG['questionaryHints']; ?></a></li>
                <li><a href="../admin/index.php?p=resetusers"><span
                    class="set-icon reset_icon"></span>
                <?php echo $LANG['resetUsers']; ?></a></li>
                <li><a href="../admin/index.php?p=managemachine"><span
                    class="set-icon manages_icon"></span>
                <?php echo $LANG['managemMchines']; ?></a></li>
                <li><a href="../admin/index.php?p=managemessage"><span
                    class="set-icon message_icon"></span>
                <?php echo $LANG['managemessage']; ?></a></li>
                <li><a href="../admin/index.php?p=education"><span
                    class="set-icon education_icon"></span>
                <?php echo $LANG['manageeducation']; ?></a></li>
                <li><a href="../admin/index.php?p=report"><span
                    class="set-icon pagecontent_icon"></span>
                Report Text</a></li> 
                <li><a href="../admin/index.php?p=supportlink"><span
                    class="set-icon pagecontent_icon"></span>
               Support link</a></li>
				<li><a href="../admin/index.php?p=notifications&notification_tab=movesmart"><span
                    class="set-icon message_icon"></span>
                <?php echo $LANG['manageNotification']; ?></a></li>
				<li><a href="../admin/index.php?p=push_notifications"><span
                    class="set-icon message_icon"></span>
                <?php echo $LANG['managepushNotification']; ?></a></li>
                 <li><a href="../admin/index.php?p=voucher_list"><span
                    class="set-icon pagecontent_icon"></span>
               Voucher List</a></li>
			    <li><a href="../admin/index.php?p=machine_group"><span
                    class="set-icon manages_icon"></span>
               Machine Groups</a></li>
			   <li><a href="../admin/index.php?p=machine_subtype"><span
                    class="set-icon sp-icon-str"></span>
               Machine Subtype </a></li>
             </ul>
        </div>
    </div>
  </div>