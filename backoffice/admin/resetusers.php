<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Resetusers
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get Cycle Associated with members.
 */
 
/**
* Returns reset type string.
*
* @param int $userid user id
*
* @return string type
*/
global $LANG;
function getResetTypeString($userid)
{
    $string = '';
    switch ($userid) {
    case RESET_USER_test:
        $string = 'Reset to avaliable for a test';
        break;
    case RESET_USER_checkfase:
        $string = 'Reset to clear Check Fase';
        break;
    case RESET_USER_MVS_fase1:
        $string = 'Reset to clear MOVESMART. Fase - I answers';
        break;
    case RESET_USER_EF_fase1:
        $string = 'Reset to EATFERSH! Fase - I answers';
        break;
    case RESET_USER_MS_fase1:
        $string = 'Reset to MINDSWITCH? Fase - I answers';
        break;
    case RESET_USER_Training:
        $string = 'Reset to make avilable for Training';
        break;
    case RESET_USER_Analysed:
        $string = 'Reset to make available for Analysis';
        break;
    }

    return $string;
}
$resetuser = $this->settings->getResetUsers();
$userlistrows = isset($resetuser['getresetusers']) ? 
    $resetuser['getresetusers'] : array();
if ($resetuser['total_records'] == 1) {
    $userlistrows = array($userlistrows);
}
?>
<div class="content-wrapper" id="coach-list">
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon icon-coachmng"></span>
            <?php echo $LANG['resetUsers']; ?></h1>
        <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back" ></span>
                </a>
            </li>
          </ul>
        </div>
    </div>
    <div class="con-sec pt100">
        <div class="row-sec" align="center">
                      <div class="col9 successSetMessgae success-msg" 
                      align="center" style="display:none;">
                <div class="col9 fadeMsg"></div>
            </div>
    <?php 
    
    if (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
        ?>
            <div class="pageFlashMsg success">
                <div class="cussuccess-icon">
                    <img src="<?php echo IMG_PATH.DS.THEME_NAME.DS.'tick_success.png';?>">
                </div>
                <div>
                    <?php echo $_SESSION['flMsg']['flashMessageSuccess']?>
                </div>
            </div>
        <?php
        unset($_SESSION['flMsg']);
    }
    if(isset($_SESSION['delet_message']) && $_SESSION['delet_message'] == 1)
    {
	?>
		 <div class="pageFlashMsg success">
                <div class="cussuccess-icon">
                    <img src="<?php echo IMG_PATH.DS.THEME_NAME.DS.'tick_success.png';?>">
                </div>
                <div>User deleted successfully</div>
            </div>
	<?php
		unset($_SESSION['delet_message']);
	}
    ?>
        </div>
          <div class="row-sec coach-search-sec search-list-form">
                <div class="">
                    <input type="text" name="p" value="" id="resetusername"
                        placeholder="email" class="fl wid40 reset_usrname">
                    <input type="hidden" class="form-control loggedUserId"
                        value="<?php echo $_SESSION['user']['user_id']; ?>"
                            name="loggedUserId"  />
                    <button class="btn black-btn fl"
                        onclick="removeResetUser()">Remove</button>
                    <button class="btn black-btn fl"
                        onclick="resetPassword()">Reset Password</button>
                    <button class="btn black-btn fl"
                        onclick="resetActive()">Active</button>
						<!--<button class="btn black-btn fl"
                        onclick="removecomplete()">Complete remove</button>-->
                </div>
        </div>
        <div class="tabOuterDiv">
            <ul class="tabs">
                <li class="current"><a href="#tab-1">
                    <?php echo $LANG['resetUserslist']; ?></a>
                </li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
                <div id="tab-1" class="tabscontent">
                    <div class="clear"></div>
                    <div class="grid-block" id="coachListGriddiv">
                    <input type="hidden" class="paramNone"
                        value="?p=coachManagementEdit">
                        <!--If navigate back to the page. Move the all
                        query string to another page -->
                        <table width="100%" border="0" cellspacing="0"
                            cellpadding="0" id="coachListGridTab">
                            <thead>
                                <tr>
                                    <td>S.No</td>
                                    <td>First Name</td>
                                    <td>Last Name</td>
									 <td>Email</td>
                                    <td>Status</td>
                                    <td class="actionwidth">Action</td>
                                </tr>
                            </thead>
                            <tbody class="pgcnt">
                                <?php
                                $sno = 1;
        if (count($userlistrows) > 0) 
        {
            foreach ($userlistrows as $row) 
            {
				$resetButton = '<button class="btn black-btn" onclick="ResetUserData('.$row['user_id'].')">Reset</button>';
        ?>
           <tr>
				<td><?php echo $sno?></td>
				<td><?php echo $row['first_name'];?></td>
				<td><?php echo $row['last_name']?></td>
				<td><?php echo $row['email']; ?></td>
				<td>
					<?php if($row['r_status_id']!=2) { echo 'Active'; } else { echo 'Inactive'; } ?>
				</td>
				<td class="txt-center" style="width:140px">
					<?php 
						echo $resetButton;
						$deleteUser = SERVICE_BASEURL."admin/index.php?p=resetusers&action=deleteUser&id=".$row['user_id'];
					?>
					<a href ='<?php echo $deleteUser ?>' class="btn black-btn" onclick="return confirm('Are you sure, You want to delete this user?')" title="Delete User" style="padding:1px 12px">
						<span class="icon icon-cls-sm"></span>
					</a>
				</td>
			</tr>
            <?php ++$sno;
            }
        } else {
            ?>
                                    <tr><td colspan="8">No Results Found</td></tr>
        <?php
        }    ?>
                                </tbody>
                        </table>
                    </div>
                    <div class="pagination-block">
        <?php
        if (isset($arrayList) && !empty($arrayList)
            && isset($arrayList[0]['user_id'])
            && isset($totalCount)
        ) {
            echo $this->paginator->displayPagination($totalCount);
        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('#coachListGridTab').DataTable({
		"order": [[ 4, "desc" ]]
	});
});
</script>
