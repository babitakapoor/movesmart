<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package BcompMethods
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description actions for body composition methods.
 */
global $LANG;
?>
<div class="search-list-form">
<form name="associatedClubs" id="" action="" method="post">
   <div class="row-sec">
       <div class="strength-machine-left strength-machine-left-bottom">
    <!--div class="strength-machine-left-bottom"><label>
        <span class="left_CheckAll">Select All</span>
        | <span class="left_unCheckAll">Deselect All</span>
       </label> </div-->
    <?php
    $savedBcompMethods = array();
    if(isset($activeBcompMethods) and count($activeBcompMethods)>0) {
        foreach ($activeBcompMethods as $key => $actvalue) {
            $savedBcompMethods[] = $actvalue['r_bcomp_id'];
        }
    } ?>
    <label for="centerBcompMethods" style="display: none;" ></label>
    <select id="centerBcompMethods"
        name="centerBcompMethods[]"
        multiple="multiple" style="height:150px;min-width:220px;" >
    <?php
    if(isset($bcompMethods) and count($bcompMethods)>0) {
        foreach ($bcompMethods as $compMethod) {
            if (!in_array($compMethod['bcomp_id'], $savedBcompMethods)) {
                echo "<option value='" . $compMethod['bcomp_id'] . "'>" .
                    $compMethod['method_name'] . '</option>';
            }
        }
    } ?>
        </select>
    </div>
    <div class="strength-machine-center">
            <span class="icon-graph icon-rightshift moveall_bcomp"
                data-mode="left-all-right"></span>
            <a class="move_rights_bcomp interchange_bcomp" data-mode="left-right">
                <img id="preview_user_image"
                    src="<?php echo IMG_PATH.DS.'right.png';?>" />
            </a>
            <br>
            <a class="move_lefts_bcomp interchange_bcomp"
                data-mode="right-left">
                <img id="preview_user_image"
                src="<?php echo IMG_PATH.DS.'left.png';?>" />
            </a>
        <span class="icon-graph icon-leftshift moveall_bcomp"
        data-mode="right-all-left"> </span>
    </div>
    <div class="strength-machine-right">
            <!--div><label>
                <span class="right_CheckAll">Select All</span>
                | <span class="right_unCheckAll">Deselect All</span>
            </label>
            </div-->
            <label for="savedBcompMethods" style="display: none;" ></label>
            <select id="savedBcompMethods"
                name="savedBcompMethods[]"
                multiple="multiple"
                style="height:150px;min-width:220px;" >
    <?php
    if (!empty($activeBcompMethods[0]) && count($activeBcompMethods) > 0) {
        foreach ($activeBcompMethods as $actMethod) {
            echo "<option value='".$actMethod['r_bcomp_id']."'>".
                $actMethod['method_name'].'</option>';
        }
    }
            ?>
            </select>
    </div>
    </div>
    <div class="strength-machine-top">
    <input type="button" id="save_bodycomp"
        class="btn black-btn"
        value="<?php echo $LANG['btnSave']; ?>" />
    <input type="hidden" name="clubId" value="<?php echo $_REQUEST['id'];?>">
    </div>
 </form>
</div>
<div class="clear">&nbsp;</div>