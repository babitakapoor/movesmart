<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Managemachine
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */

global $LANG;
    /* To List the strength Program in Dropdown */

    //To search param 
		//$this->load->model('settings');
  
   ?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-set"></span>
        Manage Machine Groups</h1>
          <div class="user-features">
                  <ul>
                    <li>
                        <a href="index.php?p=settings"
                            title="<?php echo $LANG['backToSettings']; ?>">
                            <span class="icon icon-back"></span>
                        </a>
                    </li>
                  </ul>
           </div>
                </div>
                <div class="con-sec pt100">
                    <div class="row-sec" align="center"><br/>
                        <div class="col9 successSetMessgae success-msg"
                            align="center" style="display:none;">
                            <div class="col9 fadeMsg"></div>
                        </div>
                    </div>
                  <div class="row-sec member-search-sec">
                    <form name="membersearch" id="searchFilterForm"
                        action="" method="get">
                      <div class="col6 widthcol3">
                        <label class="fl"><?php echo $LANG['select']; ?> :</label>
                        <div>
                          <input type="hidden" name="p" value="strengthProgram">
                          <input type="hidden" name="theme" value="2">
                          <input type="hidden" name="labelField" id="labelField"
                            value="<?php echo $param['labelField']; ?>">
                          <input type="hidden" name="sortType" id="sortType"
                            value="<?php echo $param['sortType']; ?>">
                        </div>
                        <!--<div class="select-custom">
                          <select id="searchType" name="searchType">
                            <option value="">-<?php
                            echo $LANG['choose']; ?>-</option>
        <?php
        foreach ($customSearchArray as $row => $value) {
            $sel = (isset($param['searchType']) &&
            $param['searchType'] == $row) ? "selected='selected'" : '';
            echo "<option value='".$row."'>".$value.'</option>';
        }
                            ?>
                          </select>
                        </div>-->

                      </div>
                      <div class="col6 widthcol7">
    
                      <a href="index.php?p=insert_group_mchn" class="btn black-btn fr">
                        Add Machine Group</a>
       
                      </div>

                    </form>
                  </div><br/>
                  <div class="tabOuterDiv">
                    
                    <div class="clear"></div>
                    <div class="tabs-container">
                    <div id="tab-1" class="tabscontent testResultListGrid">
                         <div class="clear"></div>
                        
                         <!--grid-->
                        <div class="grid-block">
                         <input type="hidden" class="paramNone"
                            value="?p=strengthProgramEdit">
                         <input type="hidden" id="strength_program_id"
                            value="">
                         <!--If navigate back to the page. Move the all
                         query string to another page -->
                          <table width="100%" border="0" cellspacing="0"
                            cellpadding="0" id="memberListGridTab">
                            <thead>
                            <tr class="grid-title toggle-label">
                              <td><?php echo 'group_id'?></td>
                              <td><?php echo 'group_name'?></td>
                             
                              <!-- ED 20160501 -->
                              <td class="grid-width txt-center">
                            <?php echo $LANG['action'];?></td>
                            </tr>
                            </thead>
				<?php
				  $machine = $this->settings->getMachineGroup();
				 
	if(isset($machine['groups']) && !empty($machine['groups'])){
		if(!isset($machine['groups'][0]))
				 {
					 $machine['groups'][] = $machine['groups'];
					 unset($machine['groups']['group_id']);
					 unset($machine['groups']['group_name']);
				 }
				foreach($machine['groups'] as $mchn){
					
				?>					
            <tr>
				<td><?php echo $mchn['group_id'] ?></td>
                <td><?php echo $mchn['group_name'] ?></td>
				<td><a href="index.php?p=edit_machine_group&id=<?php echo $mchn['group_id']; ?>"><span class="icon icon-edit"></span></a>
				<!--href="index.php?p=edit_machine_group&id=<?php echo $mchn['group_id']; ?>&action=delete"-->
				<a onclick="delete_group('<?php echo $mchn['group_id']; ?>')" ><span class="icon icon-cls-sm"></span></a></td>
            </tr>
			<?php
			
				}
	} else { ?>
	<tr>
	<td colspan="2">No Record</td>
	</tr>
	<?php } ?>
					

                          </table>
                        </div>
                        
                    </div>

                  </div>
                </div>
              </div>
</div>
<script>
function delete_group(id){
	var conform = confirm("Are you sure want to delete?");
	if(conform){
		window.location="index.php?p=edit_machine_group&id="+id+"&action=delete";
	}
	
	
}
</script>