<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Coachlist
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Get Cycle Associated with members
 */
global $LANG;
    /* To List the Clubs in Dropdown */
    $params['company_id'] = COMPANY_ID;
    $params['is_deleted'] = 0;
    $params['authorizedClubId']='';
if (isset($_SESSION['club']['authorizedClubId'])) {
    $params['authorizedClubId']=$_SESSION['club']['authorizedClubId'];
}
    $clubList = $this->club->getClubList($params);

    $param['userType'] = 'coach';

    /* $userType : Param Types should be any one of these 
        all/employee/coach/admin/backOffice/member*/
    $param['loggedUserType'] = $_SESSION['user']['usertype_id'];

    //To search param 	
    $param['clubId']
        = (isset($_REQUEST['clubId']) ? $_REQUEST['clubId'] : '');
    $param['searchType']
        =(isset($_REQUEST['searchType']) ? $_REQUEST['searchType'] : '');
    $param['searchValue']='';
if (isset($_REQUEST['searchValue'])) {
    if ($_REQUEST['searchType'] == 'gender') {
        $param['searchValue']
            =(strstr($_REQUEST['searchValue'], 'f') ? '1' : '0');
    } else {
        $param['searchValue']=$_REQUEST['searchValue'];
    }
}
    /*To sort param , 
        If the param label field is empty / default value 
        should define at else part.*/
    $reqp=$_REQUEST['p'];
    $param['labelField'] = 'user_id';
if (isset($_SESSION['pageName'][$reqp])) {
    $param['labelField'] = $_SESSION['pageName'][$reqp];
}
    $param['sortType']  ='asc';
if (isset($_SESSION['pageName'][$reqp]) && $_SESSION['sortType'] == 2) {
    $param['sortType']='desc' ;
}

    //Company id
    $param['companyId'] = COMPANY_ID;

    //Specific/Authorized clubs
     $param['authorizedClubId'] = '';
if (isset($_SESSION['club']['authorizedClubId'])) {
    $param['authorizedClubId'] =  $_SESSION['club']['authorizedClubId'];
}
    //Pagination code starts
    $limitStart = 0;
    $limitEnd = PAGINATION_SHOW_PER_PAGE;

if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
    $limitEnd = $_REQUEST['offset'];
}
if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
    $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
}

    $param['limitStart'] = $limitStart;
    $param['limitEnd'] = $limitEnd;

    // Result set	
    $arrayListCoach = $this->members->getUserList($param);

    $arrayList = $arrayListCoach['result'];

    //Total count to create pagination
    $totalCount = $arrayListCoach['totalCount'];

    /* Search Labels */
    $customSearchArray = array(
        'user_id' => 'Employee Code',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'gsm' => 'Gsm',
        'status' => 'Status',
    );

?>
<div class="content-wrapper" id="coach-list">
    <div class="con-title-sec pos-fixed mt40">
        <h1>
            <span class="icon icon-coachmng"></span>
            <?php echo $LANG['coachManagement']; ?>
        </h1>
        <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back" ></span>
                </a>
            </li>
          </ul><!--ul style="margin-top: 0px;">
                        <li>
                            <input
                                type="button"
                                id="exportActiveMembers"
                                value="<?php echo $LANG['exportForNewsLetter'];?>"
                                class="btn black-btn fr" />
                        </li>
                    </ul>
                    <form>
                        <input type="hidden" id="userType" value="coach">
                      <label for="export-formate">
                        <?php echo $LANG['exportType'];?>
                     </label>
                      <div class="select-custom">
                        <select id="exportType">
                           <option value="XLS">XLS</option>
                        </select>
                      </div>
                    </form-->
        </div>
    </div>
    <div class="con-sec pt100">
        <div class="row-sec" align="center">
          <div class="col9 successSetMessgae success-msg"
            align="center" 
            style="display:none;">
            <div class="col9 fadeMsg"></div>
        </div>
    <?php 
    if (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
        ?>
        <div class="pageFlashMsg success">
            <div class="cussuccess-icon">
                <img src="<?php echo IMG_PATH.DS.THEME_NAME.DS.'tick_success.png';?>">
            </div>
            <div>
                <?php echo $_SESSION['flMsg']['flashMessageSuccess'] ?>
            </div>
        </div>
        <?php
        unset($_SESSION['flMsg']);
    }
    ?>
        </div>
        <div class="row-sec coach-search-sec search-list-form">
            <form name="coachsearch" id="searchFilterForm" action="" method="get">
                <div class="col6">
                    <input type="hidden" name="p" value="coachList">
                    <label><?php echo $LANG['select']; ?> :</label>
                    <div>
                          <input type="hidden" name="p" value="coachList">
                          <input type="hidden" name="theme" value="2">
                          <input
                            type="hidden" 
                            name="labelField" 
                            id="labelField" 
                            value="<?php echo $param['labelField']; ?>">
                      <input type="hidden" name="sortType" id="sortType" value="<?php echo $param['sortType']; ?>">
                      <!--<select id="clubId" name="clubId" style="display:none;">
                        <option
                                value='<?php 
                                echo $_SESSION['club']['authorizedClubId']; ?>'>
                            </option>
                          </select>-->
                    </div>
                    <!-- Club list begins -->
<?php 
if (isset($_SESSION['showCommonAccess'])) {
?>
                    <div class="select-custom">
                      <label for="clubId" style="display: none;"></label>
                      <select  id="clubId" name="clubId">
                        <option value="">-
                            <?php echo $LANG['all'];?>-
                        </option>
        <?php
        foreach ($clubList as $row) {
            $selected = '';
            if (isset($param['clubId']) && ($param['clubId'] == $row['club_id'])) {
                $selected =  "selected='selected'";
            }
            echo "<option value='".$row['club_id']."' $selected>".
                $row['club_name'].'</option>';
        }
    ?>
                      </select>
                    </div>
<?php  
} else { 
?>
            <input 
                type="hidden" 
                id="clubId" 
                value="<?php echo $_SESSION['club']['authorizedClubId'];?>" >
<?php 
} 
?>
                    <!-- Club list ends -->
                    <div class="select-custom">
                        <label for="searchType" style="display: none;" ></label>
                        <select id="searchType" name="searchType">
                            <option value="">
                                -<?php echo $LANG['choose']; ?>-
                            </option>
                            <?php
                            foreach ($customSearchArray as $row => $value) {
                                $sel =  '';
                                if (isset($param['searchType'])
                                    && $param['searchType'] == $row
                                ) {
                                    $sel =  "selected='selected'";
                                }
                                echo "<option value='".$row."' $sel>".
                                $value.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Custom search variable select option ends here -->
                </div>
                <div class="col6">
<?php 
if ($_SESSION['page_add'] == 1) { ?>
    <a href="index.php?p=coachManagementEdit" 
        class="btn black-btn fr">
        <?php echo $LANG['addCoach'];?>
    </a>
<?php 
} 
?>
                    <a href="index.php?p=coachList">
                        <input
                            type="button"
                            value="<?php echo $LANG['clear']; ?>"
                            class="btn black-btn fr"
                            id="clear_search" class="fr"  />
                    </a>
                    <input type="submit"
                        value="<?php echo $LANG['search']; ?>"
                        id="searchFilterSubmit"
                        class="btn black-btn fr" />
                    <label for="searchValue" style="display: none;" ></label>
                    <input type="text"
                    name="searchValue"
                    id="searchValue"
                    class="fr"
                    value="<?php echo isset($_REQUEST['searchValue']) ?
                        $_REQUEST['searchValue'] : ''; ?>" />
                </div>
            </form>
        </div>
        <div class="tabOuterDiv">
            <ul class="tabs">
                <li class="current">
                    <a href="#tab-1"><?php echo $LANG['coachList']; ?></a>
                </li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
                <div id="tab-1" class="tabscontent">
                    <div class="clear"></div>
                    <p class="mb15">
                        <?php echo $LANG['totalUserCount']; ?> :
                        <span class="count-block"><?php echo $totalCount; ?></span>
                    </p>
                    <div class="grid-block" id="coachListGriddiv">
                    <!--If navigate back to the page.
                        Move the all query string to another page -->
                    <input type="hidden" class="paramNone"
                        value="?p=coachManagementEdit">
                    <table width="100%"
                        border="0"
                        cellspacing="0"
                        cellpadding="0"
                        id="coachListGridTab">
                        <thead>
                            <tr class="grid-title toggle-label"
                            linkData="<?php echo $reqp; ?>">
                                <td>
                                <?php
                                $clsdesc=$clsasc='';
                                if ($param['labelField']=='user_id') {
                                    if ($param['sortType']=='desc') {
                                        $clsdesc='active';
                                    } else {
                                        $clsasc='active';
                                    }
                                }?>
                            <?php echo $LANG['employeeCode']; ?>
                            <span class="spinner">
                            <a href="javascript:;"
                            onClick="sortingField('user_id', '1',
                                '<?php echo $reqp; ?>')"
                            class="spi-arr-up <?php echo $clsasc; ?>">
                            </a>
                            <a href="javascript:;"
                            onClick="sortingField('user_id', '2',
                            '<?php echo $reqp; ?>')"
                            class="spi-arr-down <?php echo $clsdesc; ?>">
                            </a>
                            </span>
                            </td>
                                <td>
                                <?php
                                $clsdesc=$clsasc='';
                                if ($param['labelField']=='first_name') {
                                    if ($param['sortType']=='desc') {
                                        $clsdesc='active';
                                    } else {
                                        $clsasc='active';
                                    }
                                }?>
                                <?php echo $LANG['firstName']; ?>
                                <span class="spinner">
                                <a href="javascript:;"
                                onClick="sortingField('first_name', '1','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-up <?php echo $clsasc;?>">
                                </a>
                                <a href="javascript:;"
                                onClick="sortingField('first_name', '2','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-down <?php echo $clsdesc;?>">
                                </a>
                                </span>
                                </td>
                                <td>
                                <?php
                                $clsdesc=$clsasc='';
                                if ($param['labelField']=='middle_name') {
                                    if ($param['sortType']=='desc') {
                                        $clsdesc='active';
                                    } else {
                                        $clsasc='active';
                                    }
                                }?>
                                <?php echo $LANG['interjection']; ?>
                                <span class="spinner">
                                <a href="javascript:;"
                                onClick="sortingField('middle_name', '1','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-up <?php echo $clsasc; ?>">
                                </a>
                                <a href="javascript:;"
                                onClick="sortingField('middle_name', '2','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-down <?php echo $clsdesc;?>">
                                </a>
                                </span>
                                </td>
                                <td>
                                <?php
                                $clsdesc=$clsasc='';
                                if ($param['labelField']=='last_name') {
                                    if ($param['sortType']=='desc') {
                                        $clsdesc='active';
                                    } else {
                                        $clsasc='active';
                                    }
                                }?>
                                <?php echo $LANG['lastName']; ?>
                                <span class="spinner">
                                <a href="javascript:;"
                                onClick="sortingField('last_name', '1','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-up <?php echo $clsasc;?>">
                                </a>
                                <a href="javascript:;"
                                onClick="sortingField('last_name', '2','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-down <?php echo $clsdesc;?>">
                                </a>
                                </span>
                                </td>
                                <td>
                                <?php
                                $clsdesc=$clsasc='';
                                if ($param['labelField']=='gsm') {
                                    if ($param['sortType']=='desc') {
                                        $clsdesc='active';
                                    } else {
                                        $clsasc='active';
                                    }
                                }?>
                                <?php echo $LANG['GSM']; ?>
                                <span class="spinner">
                                <a href="javascript:;"
                                onClick="sortingField('gsm', '1','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-up <?php echo $clsasc;?>">
                                </a>
                                <a
                                href="javascript:;"
                                onClick="sortingField('gsm', '2','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-down <?php echo $clsdesc;?>">
                                </a>
                                </span>
                                </td>
                                 <td>
                                <?php
                                $actclsdesc=$actclsasc='';
                                if ($param['labelField']=='club_name') {
                                    if ($param['sortType']=='desc') {
                                        $actclsdesc='active';
                                    } else {
                                        $actclsasc='active';
                                    }
                                }?>
                                <?php echo $LANG['fitClassCentrum']; ?>
                                <span class="spinner">
                                <a href="javascript:;"
                                onClick="sortingField('club_name', '1','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-up <?php echo $actclsasc; ?>">
                                </a>
                                <a href="javascript:;"
                                onClick="sortingField('club_name', '2','<?php
                                echo $reqp; ?>')"
                                class="spi-arr-down <?php echo $actclsdesc; ?>">
                                </a>
                                </span>
                                </td>
                                <td>
                                <?php
                                $actclsdesc=$actclsasc='';
                                if ($param['labelField']=='email') {
                                    if ($param['sortType']=='desc') {
                                        $actclsdesc='active';
                                    } else {
                                        $actclsasc='active';
                                    }
                                }
                                ?>
                                <?php echo $LANG['emailID']; ?>
                    <span class="spinner">
                        <a href="javascript:;"
                        onClick="sortingField('email', '1','<?php echo $reqp; ?>')"
                        class="spi-arr-up <?php echo $actclsasc; ?>"></a>
                        <a href="javascript:;"
                        onClick="sortingField('email', '2','<?php echo $reqp; ?>')"
                        class="spi-arr-down <?php echo $actclsdesc;?>">
                       </a>
                    </span>
                                </td>
                                <td>
                                    <?php echo $LANG['status']; ?>
                                    <?php
                                    $actclsdesc=$actclsasc='';
                                    if ($param['labelField']=='status') {
                                        if ($param['sortType']=='desc') {
                                            $actclsdesc='active';
                                        } else {
                                            $actclsasc='active';
                                        }
                                    }?>
                                    <span class="spinner">
                                    <a href="javascript:;"
                                    onClick="sortingField('status', '1','<?php
                                    echo $reqp; ?>')"
                                    class="spi-arr-up <?php echo $actclsasc; ?>"></a>
                                    <a
                                    href="javascript:;"
                                    onClick="sortingField('status', '2','<?php
                                    echo $reqp;?>')"
                                    class="spi-arr-down <?php echo $actclsdesc;?>">
                                    </a>
                                    </span>
                                </td>
                                <!-- ED 20160501 -->
                                <td class="txt-center">
                                    <?php echo $LANG['action']; ?>
                                </td>
                            </tr>
                        </thead>
                        <?php
                        // Pagination Class in body section
                        if (isset($arrayList)
                            && !empty($arrayList)
                            && isset($arrayList[0]['user_id'])
                        ) {
                            $deleteLink = '';
                            $editLink = '';
                            if ($_SESSION['page_edit'] == 1) {
                                $editLink = '<a 
                                    title="'.$LANG['titleEdit'].'" 
                                    onclick="redirectEdit(this,2)" 
                                    uid="$uid$" 
                                    hrefValue="$user_id$" 
                                    class="btn-link btn-inline dotline-sep">
                                    <span class="icon icon-edit"></span>
                                </a>';
                            }
                            if ($_SESSION['page_delete'] == 1) {
                                $deleteLink = '<a 
                                    title="'.$LANG['titleDelete'].'" 
                                    onclick="deleteRow($user_id$)" 
                                    href="#" 
                                    class="btn-link btn-inline" >
                                    <span class="icon icon-cls-sm"></span>
                                </a>';
                            }
                            //Get current link that is not consider ajax page
                            $this->paginator->getUrlLink($param = 1);
                            echo $this->paginator->displayItemsPagination(
                                $arrayList, array(
                                    'user_id',
                                    'first_name',
                                    'middle_name',
                                    'last_name',
                                    'gsm',
                                    'clubs',
                                    'email',
                                    'status',
                                    'specValues2' => '<input type="hidden" 
                                        class="testId" 
                                        value="$user_id$">'.
                                        $editLink.$deleteLink
                                ),
                                array('startTag' => '<tr>',
                                   'midTagOpen' => '<td>',
                                   'midTagClose' => '</td>',
                                   'endTag' => '</tr>'
                                )
                            );
                        } else {
                            echo '<tr><td colspan="20">'.
                                $LANG['noResult'].
                            '</td></tr>';
                        }?>
                    </table>
                    </div>
                    <div class="pagination-block">
        <?php
        if (isset($arrayList)
            && !empty($arrayList)
            && isset($arrayList[0]['user_id'])
        ) {
            echo $this->paginator->displayPagination($totalCount);
        } ?>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>