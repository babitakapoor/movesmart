<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Managemachine
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */

global $LANG;
    /* To List the strength Program in Dropdown */

    //To search param 
    $param['searchType'] = (isset($_REQUEST['searchType']) ? 
        $_REQUEST['searchType'] : '');
    $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
        ($_REQUEST['searchType'] == 'gender' ? 
            (strstr($_REQUEST['searchValue'], 'f') ?
            '1' : '0') : $_REQUEST['searchValue']) : '');

    /*To sort param , If the param label field is empty /
    default value should define at else part.*/
    $param['labelField'] = (isset($_SESSION['pageName'][$_REQUEST['p']])) ?
        $_SESSION['pageName'][$_REQUEST['p']] : 'description';
    $param['sortType'] = (isset($_SESSION['pageName'][$_REQUEST['p']]) &&
        $_SESSION['sortType'] == 2) ? 'desc' : 'asc';

    //Company id
    $param['companyId'] = COMPANY_ID;

    //Pagination code starts
    $limitStart = 0;
    $limitEnd = PAGINATION_SHOW_PER_PAGE;

if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
    $limitEnd = $_REQUEST['offset'];
}
if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
    $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
}

    $param['limitStart'] = $limitStart;
    $param['limitEnd'] = $limitEnd;
    //Pagination code ends

    $param['companyId'] = COMPANY_ID;

    $arrayListMember = $this->settings->strengthProgramList($param);

    $arrayList = (!isset($arrayListMember['strengthProgramList'][0])) ? 
        array($arrayListMember['strengthProgramList']) : 
        $arrayListMember['strengthProgramList'];

    //Total count to create pagination
    $totalCount = isset($arrayListMember['totalCount']) ? 
        $arrayListMember['totalCount'] : 0;

    /* Search Labels */
    $customSearchArray = array(
        'description' => $LANG['description'],
        'type' => $LANG['type'],
    );
    $machinelist = $this->settings->machineList($param);
	
    $listmachine = isset($machinelist['movesmart']['data']) ? 
        $machinelist['movesmart']['data'] : array();
    if ($machinelist['movesmart']['rows'] == 1) {
        $listmachine = array($listmachine);
    }
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-set"></span>
        <?php echo $LANG['managemMchines']; ?></h1>
          <div class="user-features">
                  <ul>
                    <li>
                        <a href="../index.php?p=settings"
                            title="<?php echo $LANG['backToSettings']; ?>">
                            <span class="icon icon-back"></span>
                        </a>
                    </li>
                  </ul>
           </div>
                </div>
                <div class="con-sec pt100">
                    <div class="row-sec" align="center"><br/>
                        <div class="col9 successSetMessgae success-msg"
                            align="center" style="display:none;">
                            <div class="col9 fadeMsg"></div>
                        </div>
                    </div>
                  <div class="row-sec member-search-sec">
                    <form name="membersearch" id="searchFilterForm"
                        action="" method="get">
                      <div class="col6 widthcol3">
                        <label class="fl"><?php echo $LANG['select']; ?> :</label>
                        <div>
                          <input type="hidden" name="p" value="strengthProgram">
                          <input type="hidden" name="theme" value="2">
                          <input type="hidden" name="labelField" id="labelField"
                            value="<?php echo $param['labelField']; ?>">
                          <input type="hidden" name="sortType" id="sortType"
                            value="<?php echo $param['sortType']; ?>">
                        </div>
                        <!--<div class="select-custom">
                          <select id="searchType" name="searchType">
                            <option value="">-<?php
                            echo $LANG['choose']; ?>-</option>
        <?php
        foreach ($customSearchArray as $row => $value) {
            $sel = (isset($param['searchType']) &&
            $param['searchType'] == $row) ? "selected='selected'" : '';
            echo "<option value='".$row."'>".$value.'</option>';
        }
                            ?>
                          </select>
                        </div>-->

                      </div>
                      <div class="col6 widthcol7">
                        <?php if ($_SESSION['page_edit'] == 1) {
    ?>
                      <a href="index.php?p=addmachine" class="btn black-btn fr">
                        <?php echo $LANG['addmachine'];
    ?></a>
        <?php
    } ?>
                      </div>

                    </form>
                  </div><br/>
                  <div class="tabOuterDiv">
                    <ul class="tabs">
                        <li class="current"><a href="#tab-1"><?php
                        echo $LANG['machineList']; ?></a></li>
                    </ul>
                    <div class="clear"></div>
                    <div class="tabs-container">
                    <div id="tab-1" class="tabscontent testResultListGrid">
                         <div class="clear"></div>
                         <p class="mb15"><?php echo $LANG['totalUserCount']; ?>:
                            <span class="count-block"><?php echo $totalCount; ?>
                            </span> </p>
                         <!--grid-->
                        <div class="grid-block">
                         <input type="hidden" class="paramNone"
                            value="?p=strengthProgramEdit">
                         <input type="hidden" id="strength_program_id"
                            value="">
                         <!--If navigate back to the page. Move the all
                         query string to another page -->
                          <table width="100%" border="0" cellspacing="0"
                            cellpadding="0" id="memberListGridTab">
                            <thead>
                            <tr class="grid-title toggle-label">
                              <!--<td><?php echo 'Club Name'?></td>-->
							  <td><?php echo 'ID'?></td>
                              <td><?php echo 'Name'?></td>
                              <td><?php echo 'Brand'?></td>
                              <td><?php echo 'Type'?></td>
                              <td><?php echo 'Group'?></td>
                              <td><?php echo 'Sub Type'?></td>
                              <td><?php echo 'Images'?></td>
                              <td><?php echo 'Status'?></td>
                              <!-- ED 20160501 -->
                              <td class="grid-width txt-center">
                            <?php echo $LANG['action'];?></td>
                            </tr>
                            </thead>
        <?php
        if (isset($listmachine) && !empty($listmachine)) {
            foreach ($listmachine as $macine) {
                $editLink = '';
                $deleteLink = '';
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                    <a title="{$LANG['titleEdit']}" class="btn-link 
                        btn-inline dotline-sep icon-edit-menu"  
                        href="index.php?p=addmachine&id={$macine['machineid']}">
                        <span class="icon icon-edit"></span></a>
EDIT_LINK;
                }
				 if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                    <a title="{$LANG['titleDelete']}" class="btn-link btn-inline icon-delete-menu"  onclick="delete_machine({$macine['machineid']})">
                     <span class="icon icon-cls-sm"></span>
                        </a>
EDIT_LINK;
                }
            ?>

            <tr><!--<td><?php echo $macine['clubname'];
                ?></td>-->
				<td><?php echo $macine['machineid'] ?></td>
				<td><?php echo $macine['nameofmachine']?></td>
                <td><?php echo $macine['brand_name']?></td><td>
                <?php echo $macine['type']?></td>
                <td><?php echo $macine['groupname']?></td><td>
                <?php echo $macine['subtype']?></td>
                <td><?php echo $macine['image']?></td><td>
                <?php echo $macine['status']?></td><td>
                <?php echo $editLink.$deleteLink?></td></tr>
        <?php
            }
        } else {
            echo '<tr><td colspan="20">'.$LANG['noResult'].'</td></tr>';
        }
                                ?>

                          </table>
                        </div>
                        <div class="pagination-block">
        <?php
        if (isset($arrayList) && !empty($arrayList)
            && isset($arrayList[0]['strength_program_id'])
        ) {
            $this->paginator->getUrlLink($param = 1);
            echo $this->paginator->displayPagination($totalCount);
        }
                        ?>
                        </div>
                    </div>

                  </div>
                </div>
              </div>
</div>
<script>

$(document).ready(function(){
    $('#memberListGridTab').DataTable({
		 "order": [[ 0, "desc" ]]
	});
		
});
function delete_machine(id){

		var confm = confirm("Are you sure want to delete?");
		if(confm==true){
		 window.location="index.php?p=addmachine&id="+id+"&action=delete";
		}
		
	}
</script>