<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package ManageClub
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display club list.
 */
global $LANG;
    /* To List the Clubs in Dropdown */
    //To search param
    $param['searchType'] = (
        isset($_REQUEST['searchType'])
    ) ? $_REQUEST['searchType'] : '';
    $param['searchValue'] = '';
    if(isset($_REQUEST['searchValue'])){
        if ($_REQUEST['searchType'] == 'gender') {
            $param['searchValue'] = strstr($_REQUEST['searchValue'], 'f') ?
            '1' : '0';
        } else { 
            $param['searchValue']   =   $_REQUEST['searchValue'];
        }
    }
    /*To sort param , If the param label field is empty / default value 
        should define at else part.*/
    $param['labelField'] = (
        isset($_SESSION['pageName'][$_REQUEST['p']])
    ) ? $_SESSION['pageName'][$_REQUEST['p']] : 'club_name';
    $param['sortType'] = (
        isset($_SESSION['pageName'][$_REQUEST['p']]) && $_SESSION['sortType'] == 2
    ) ? 'desc' : 'asc';

    //Company id
    $param['companyId'] = COMPANY_ID;

    //Specific/Authorized clubs
    /* 
        $param['authorizedClubId'] =(isset($_SESSION['club']['authorizedClubId'])?
        $_SESSION['club']['authorizedClubId']:"");
        */

    //Pagination code starts
    $limitStart = 0;
    $limitEnd = PAGINATION_SHOW_PER_PAGE;

    if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
        $limitEnd = $_REQUEST['offset'];
    }
    if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
        $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
    }

    $param['limitStart'] = $limitStart;
    $param['limitEnd'] = $limitEnd;
    //Pagination code ends

    $param['companyId'] = COMPANY_ID;

    $arrayListMember = $this->club->getClubListByCompany($param);
    $arrayList = (
        (!isset($arrayListMember['rows'][0]))
    ) ? array($arrayListMember['rows']) : $arrayListMember['rows'];

    //Total count to create pagination
    $totalCount = $arrayListMember['totalCount'];

    /* Search Labels */
    $customSearchArray = array(
        'club_id' => 'Club Id',
        'club_name' => 'Club Name',
        'email_id' => 'Email Id',
    );
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-doubl"></span><?php echo $LANG['clubList']; ?></h1>
          <div class="user-features">
            <ul>
                <li>
                    <a href="../index.php?p=settings"
                        title="<?php echo $LANG['backToSettings']; ?>">
                        <span class="icon icon-back"></span>
                    </a>
                </li>
            </ul>
                    <!--ul style="margin-top: 0px;">
                        <li><input type="button"
                            id="exportActiveMembers"
                            value="<?php echo $LANG['exportForNewsLetter'];?>"
                            class="btn black-btn fr" />
                        </li>
                    </ul>
                    <form>
                        <input type="hidden" id="userType" value="clubList">
                        <label for="export-formate">
                            <?php echo $LANG['exportType'];?>
                        </label>
                        <div class="select-custom">
                            <select id="exportType">
                                <option value="XLS">XLS</option>
                            </select>
                        </div>
                    </form-->
           </div>
                </div>
                <div class="con-sec pt100">
                    <div class="row-sec" align="center"><br/>
                        <div class="col9 successSetMessgae success-msg"
                            align="center" style="display:none;">
                            <div class="col9 fadeMsg"></div>
                        </div>
                    </div>
                  <div class="row-sec member-search-sec">
                    <form name="membersearch"
                        id="searchFilterForm" action="" method="get">
                      <div class="col6 widthcol3">
                        <label class="fl">
                            <?php echo $LANG['select']; ?> :
                        </label>
                        <div>
                          <input type="hidden" name="p" value="manageClub">
                          <input type="hidden" name="theme" value="2">
                          <input type="hidden"
                            name="labelField"
                            id="labelField"
                            value="<?php echo $param['labelField']; ?>">
                          <input type="hidden"
                            name="sortType" id="sortType"
                            value="<?php echo $param['sortType']; ?>">
                          </div>
                        <div class="select-custom">
                          <label for="searchType" style="display: none;"></label>
                          <select id="searchType" name="searchType">
                            <option value="">-<?php echo $LANG['choose']; ?>-</option>
        <?php
        foreach ($customSearchArray as $row => $value) {
            $sel = (
                isset($param['searchType']) && $param['searchType'] == $row
            ) ? "selected='selected'" : '';
            echo "<option value='".$row."' $sel>".$value.'</option>';
        } ?>
        </select>
    </div>
        </div>
                      <div class="col6 widthcol7">
                        <?php if ($_SESSION['page_edit'] == 1) { ?>
                      <a href="index.php?p=manageClubEdit"
                        class="btn black-btn fr">
                            <?php echo $LANG['addClub'];?>
                      </a>
<?php
}
?>
                      <a href="index.php?p=manageClub">
                            <input type="button"
                                value="<?php echo $LANG['clear'];?>"
                                class="btn black-btn fr"
                                id="clear_search" />
                        </a>
                        <input type="submit"
                            value="<?php echo $LANG['search'];?>"
                            id="searchFilterSubmit"
                            class="btn black-btn fr" />
                        <label for="searchValue" style="display: none;" ></label>
                        <input type="text"
                            name="searchValue"
                            id="searchValue"
                            class="fr wid40 search-club"
                            value="<?php echo isset($_REQUEST['searchValue']) ?
                                $_REQUEST['searchValue'] : ''; ?>"/>
                      </div>

                    </form>
                  </div>
                  <div class="tabOuterDiv">
                    <ul class="tabs">
                        <li class="current">
                            <a href="#tab-1">
                                <?php echo $LANG['clubList']; ?>
                            </a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                    <div class="tabs-container">
                    <div id="tab-1" class="tabscontent testResultListGrid">
                        <div class="clear"></div>
                        <p class="mb15">
                            <?php echo $LANG['totalUserCount']; ?>:
                            <span class="count-block">
                            <?php echo $totalCount; ?></span>
                        </p>
                         <!--grid-->
                        <div class="grid-block">
                         <input type="hidden"
                            class="paramNone"
                            value="?p=manageClubEdit">
                         <input type="hidden"
                            id="clubId" value="">
                         <!--If navigate back to the page.
                            Move the all query string to another page -->
                          <table
                            width="100%"
                            border="0"
                            cellspacing="0"
                            cellpadding="0"
                            id="memberListGridTab">
                            <thead>
                            <tr class="grid-title toggle-label"
                                linkdata="<?php echo $_REQUEST['p']; ?>">
                              <td class="grid-width">
                                <?php echo $LANG['clubId'];?>
                                  <span class="spinner">
                                      <a href="javascript:;"
                                        onClick="sortingField('club_id',
                                            '1',
                                            '<?php echo $_REQUEST['p']; ?>')"
                                            class="spi-arr-up"></a>
                                      <a href="javascript:;"
                                        onClick="sortingField('club_id',
                                        '2',
                                        '<?php echo $_REQUEST['p']; ?>')"
                                        class="spi-arr-down"></a>
                                  </span>
                              </td>
                              <td>
                                    <?php echo $LANG['clubName'];?>
                                  <span class="spinner">
                                      <a href="javascript:;"
                                        onClick="sortingField('club_name',
                                            '1',
                                            '<?php echo $_REQUEST['p']; ?>')"
                                            class="spi-arr-up"></a>
                                      <a href="javascript:;"
                                        onClick="sortingField('club_name',
                                        '2',
                                        '<?php echo $_REQUEST['p']; ?>')"
                                        class="spi-arr-down"></a>
                                  </span>
                              </td>
                              <td>
                                <?php echo $LANG['emailId'];?>
                                <span class="spinner">
                                    <a href="javascript:;"
                                        onClick="sortingField('email_id',
                                        '1',
                                        '<?php echo $_REQUEST['p']; ?>')"
                                        class="spi-arr-up"></a>
                                    <a href="javascript:;"
                                        onClick="sortingField('email_id',
                                            '2',
                                            '<?php echo $_REQUEST['p']; ?>')"
                                        class="spi-arr-down"></a>
                                </span>
                              </td>
                              <!-- ED 20160501 -->
                              <td class="grid-width txt-center">
                                <?php echo $LANG['action'];?></td>
                            </tr>
                        </thead>
        <?php 
        if (isset($arrayList) 
            && !empty($arrayList) 
            && isset($arrayList[0]['club_id'])
        ) {
            $deleteLink = '';
            $editLink = '';
            if ($_SESSION['page_edit'] == 1) {
                $editLink = '<a title="'.$LANG['titleEdit'].'" 
                    onclick="redirectEdit(this,2)" 
                    hrefValue="$club_id$" 
                    class="btn-link btn-inline dotline-sep align-center">
                    <span class="icon icon-edit"></span></a>';
            }
            if ($_SESSION['page_delete'] == 1) {
                $deleteLink = '<a  title="'.$LANG['titleDelete'].'" 
                    onclick="deleteClub($club_id$)" 
                    href="#" 
                    class="btn-link btn-inline  align-center">
                    <span class="icon icon-cls-sm"></span></a>';
            }
            echo $this->paginator->displayItemsPagination(
                $arrayList, array(
                    'club_id', 
                    'club_name', 
                    'email_id', 
                    'specValues2' => '<input 
                        type="hidden" 
                        class="testId" value="$club_id$">'.$editLink.$deleteLink
                ),
                array (
                    'startTag' => '<tr>', 
                    'midTagOpen' => '<td>', 
                    'midTagClose' => '</td>', 
                    'endTag' => '</tr>'
                )
            );
        } else {
            echo '<tr><td colspan="20">'.$LANG['noResult'].'</td></tr>';
        }
                                ?>

                          </table>
                        </div>
                        <div class="pagination-block">
        <?php
        if (isset($arrayList) 
            && !empty($arrayList) 
            && isset($arrayList[0]['club_id'])
        ) {
            $this->paginator->getUrlLink($param = 1);
            echo $this->paginator->displayPagination($totalCount);
        }
                        ?>
                        </div>
                    </div>

                  </div>
                </div>
              </div>
</div>