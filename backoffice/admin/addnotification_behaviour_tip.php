<?php 
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Translation
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display stregth list.
 */
 /* To List the strength Program in Dropdown */

$formType = 'save_behaviour_content';
$notday = '';
$answerData = array();
$id = '';
$title = 'Add';
switch($_REQUEST['type'])
{
	case(1):
		$tab = 'Move';
		$urltab = 'movesmart';
		break;
	case(2):
		$tab = 'Mind';
		$urltab = 'mind';
		break;
	case(3):	
		$tab = 'Eat';
		$urltab = 'eat';
		break;
}
if(isset($_REQUEST['id']))
{
	$disableCheck = true;
	$data = $this->notifications->getBehaviourData($_REQUEST['id']);
	//echo "<pre>";print_r($data);
	if(isset($data['data']['id']))
	{
		$title = 'Edit';
		$notday = $data['data']['day_id'];
		$formType = 'edit_behaviour_content';
		$answerData = $data['data']['content_data'];
		$id = $data['data']['id'];
		//echo "<pre>";print_r($answerData);die;
	}
}
$notificationSettings = 	$this->notifications->getNotificationsSettings($_REQUEST['type']);
$settingData = $this->notifications->getProfileData();
$profiles = $settingData['data']['profile'];
?>
<style>
.grid-block label{width:200px}
.grid-block textarea{resize:none}
</style>
<!--To List the translation language -->
<div class="content-wrapper" id="manage-members">
    <div class="con-sec pt100">
        
		<!-- Flash message begins -->
		<div>
			<?php
			if (isset($_SESSION['flMsg']) || isset($_REQUEST['mess'])) 
			{
                if (isset($_SESSION['flMsg']['flashMessageError'])) {
                    echo '<div class="pageFlashMsg error" style="height:40px !important">'.
                        $_SESSION['flMsg']['flashMessageError'].'</div>';
                } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                    echo '<div class="pageFlashMsg success" style="height:40px !important">'.
                        $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
                }
				elseif (isset($_REQUEST['mess'])) {
                    echo '<div class="pageFlashMsg error" style="height:40px !important">'.
                        $_REQUEST['mess'].'</div>';
                }
                unset($_SESSION['flMsg']);
            } 
			?>
			<div>&nbsp;</div>
		</div>
		<div class="tabOuterDiv">
			<ul class="tabs">
				<li class="current">
					<a href="#tab-1"><?php echo $title." ".$tab." ".ucwords(str_replace("_"," ",$_REQUEST['notification_tab'])) ?></a>
				</li>
			</ul>
           <div class="tabs-container">
				<div id="tab-1" class="tabscontent testResultListGrid">
					<div class="row-sec member-search-sec">
						<div class="">
							<a href="../admin/index.php?p=notifications&notification_tab=<?php echo $urltab ?>" class="btn black-btn fr">
									Back To Notification Listing
							</a>
						</div>
					</div>
					<!--grid-->
					<div class="grid-block" style="border:none">
						<form>
							<div style="padding:10px;float:left;width:100%">
								<input type="hidden" name="action_type" value="<?php echo $formType ?>">
								<input type="hidden" name="p" value="<?php echo $_REQUEST['p'] ?>">
								<input type="hidden" name="notification_pages_id" value="<?php echo $_REQUEST['type'] ?>">
								<input type="hidden" name="notification_tab" value="<?php echo $_REQUEST['notification_tab'] ?>">
								<input type="hidden" name="notification_types_id" value="<?php echo $_REQUEST['type_id'] ?>">
								<input type="hidden" name="id" value="<?php echo (isset($_REQUEST['id']))?$_REQUEST['id']:'' ?>">
								<?php
								if($notday != '')
								{
								?>
									<input type="hidden" name="notification_days_id" value="<?php echo $notday ?>">
								<?php
								}
								?>
							</div>
							<div style="padding:10px;float:left;width:100%">
								<label>Notification Day<span style="color:red">*</span></label>
								<select name="notification_days_id" required='required' <?php echo ($notday != '')?'disabled="disabled"':'' ?>>
									<option value="">Select Day</option>
									<?php
										foreach($notificationSettings['data']['day'] as $day)
										{
									?>
											<option value="<?php echo $day['id'] ?>" <?php echo ($notday==$day['id'])?'selected="selected"':'' ?>><?php echo $day['name'] ?></option>
									<?php
										}
									?>
								</select> 
							</div>
							<div style="padding:10px;float:left;width:100%;">
								<div style="padding:10px;float:left;width:100%;text-align:center">
									<b>Profiles</b>
								</div>
								<div class="row-sec member-search-sec">
									<?php 
									foreach($profiles as $pr)
									{
										if(strtolower($pr['name']) != 'all')
										{
									?>
										<div style="float:left;width:25%">
											<div style="width:100%;float:left;">
												<label style="text-align:center"><?php echo $pr['name'] ?></label>
											</div>
										</div>
									<?php
										}
									}
									?>
								</div>
								<?php 
								foreach($profiles as $pr)
								{
									if(strtolower($pr['name']) != 'all')
									{
								?>
										<div style="float:left;width:25%">
											<div style="width:100%;float:left">
												<?php
													$value = '';
													if(!empty($answerData))
													{
														foreach($answerData as $ans)
														{
															$prfid  = $ans['t_notification_user_profile_id'];
															$catid  = $ans['t_notification_score_categorie_id'];
															$answernumber  = $ans['message_number'];
															if($prfid == $pr['id'] && $catid == 0 && $answernumber == 1)
															{
																$value = $ans['content'];
															}
														}
													}
												?>
												<textarea name="content[<?php echo $pr['id'] ?>][0][1]" required='required'><?php echo $value ?></textarea>
											</div>
										</div>
								<?php
									}
								}
								?>
							</div>
							<div style="padding:10px;float:left;width:100%">
								<input class="btn black-btn fr" value="Save" type="submit">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
