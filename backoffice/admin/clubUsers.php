<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Clubuusers
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display test result members.
 */
?>
<!-- Assign member to club begins -->
<?php
    global $LANG;
    $employeeIdExist = array();
    if(isset($getEmployeeCenter) and count($getEmployeeCenter)>0) {
        foreach ($getEmployeeCenter as $row) {
            if (isset($row['user_id'])) {
                $employeeIdExist[] = $row['user_id'];
            }
        }
    }
    $params['companyId'] = COMPANY_ID;
    $employeeList = $this->club->getCompanyEmployees($params);
    $employees = $employeeList['rows']; 
?>
<div class="row-sec coach-search-sec search-list-form">
<form name="coachsearch" id="coachsearch" action="" method="post">
    <div class="col6">
        <label><?php echo $LANG['user']; ?> :</label>
        <div class="select-custom">
              <label for="employeeId" style="display: none;" ></label>
              <select id="employeeId" name="employeeId" >
                <option value="">---<?php echo $LANG['select']; ?>---</option>
                <?php
    foreach ($employees as $row) {
        if (!in_array($row['user_id'], $employeeIdExist)) {
            if ($row['r_usertype_id'] != ROLE_ADMIN) {
                if (isset($param['user_id'])
                    && $row['user_id']==$param['user_id']
                ) {
                    $sel =  "selected='selected'";
                } else {
                    $sel =  '';
                }
                echo "<option value='".$row['user_id']."' $sel>".
                    $row['first_name'].' '.$row['last_name'].'</option>';
            }
        }
    }
                ?>
              </select>
        </div>
        <label><?php echo $LANG['role']; ?> : </label>
        <div class="select-custom">
            <label for="userTypeId" style="display: none;" ></label>
            <select id="userTypeId" name="userTypeId">
                <option value="">---<?php echo $LANG['select']; ?>---</option>
                <option value="1">Coach</option>
                <!--<option value="2">Admin</option>-->
                <!--<option value="4">Back Office</option>-->
            </select>
        </div>
        <label>
            <input
                type="submit"
                name="assignRole"
                value="<?php echo $LANG['assignUser']; ?>"
                class="btn black-btn" />
        </label>
        <div class="clear" ></div>
        <!-- Custom search variable select option ends here -->
        <div class="inmaincontant">
            <div style="clear:both;"></div>
            <label><?php echo $LANG['isMainContact']; ?> :</label>
            <div class="in-cell cus-check get-check ">
                <input type="radio" id="isMainContact" name="isMainContact" value="1" class="oxy">
                <label for="isMainContact">Yes</label>
            </div>
            <div class="in-cell cus-check get-check">
                <input type="radio" id="isMainContactno" name="isMainContact" value="0" class="oxy">
                <label for="isMainContactno">No</label>
            </div>
            <div class="clear"></div>
            <!--
            <label>
                <input type="radio" name="isMainContact" value="1" />
            </label>
            <label>Yes</label>
            <label>
                <input
                    type="radio"
                    name="isMainContact"
                    value="0"
                    checked="checked" />
            </label>
            <label>No</label> -->
        </div>
    </div>
    </form>
    </div>
    <div>&nbsp;</div>
    <!-- Assign member to club ends -->

    <div class="grid-block">
    <table
        width="100%"
        border="0"
        cellspacing="0"
        cellpadding="0"
        id="manageClubEditTab">
    <thead>
    <tr class="grid-title">
      <td><?php echo $LANG['employeeName']; ?></td>
      <td><?php echo $LANG['email']; ?></td>
      <td><?php echo $LANG['role']; ?></td>
      <td><?php echo $LANG['mainContact']; ?></td>
      <!-- ED 20160501 -->
      <td class="txt-center" ><?php echo $LANG['action']; ?></td>
    </tr>
    </thead>
    <tbody>
    <?php
    if (!empty($getEmployeeCenter[0]) && count($getEmployeeCenter) > 0) {
        foreach ($getEmployeeCenter as $key => $value) {
            $isMainContact = ($value['is_main_contact'] == 1) ? 'Yes' : 'No';?>
            <tr>
                <td>
                    <?php echo $value['first_name'];?>
                </td>
                <td>
                    <?php echo $value['email'];?>
                </td>
                <td>
                    <?php echo $value['usertype'];?>
                </td>
                <td>
                    <?php echo $isMainContact;?>
                </td>
                <td>
                <a href="#"
                onclick="deleteRow('<?php echo $value['employee_centre_id'];?>');"
                class="btn-link btn-inline ">
                    <span class="icon icon-cls-sm"></span></a></td>
            </tr>
            <tr>
            </tr>
        <?php
        }
    } else { ?>
        <tr>
            <td colspan="20"><?php echo $LANG['noRecordsFound'];?></td>
        <td>
            <input
                type="button"
                id="save_bodycomp"
                class="btn black-btn"
                value="<?php echo $LANG['btnSave'];?>" />
        <input type="hidden" name="clubId"
            value="<?php echo $_REQUEST['id'];?>">
    </td>
        </tr>

    <?php
    } ?>
    </tbody>
    </table>
    </div>