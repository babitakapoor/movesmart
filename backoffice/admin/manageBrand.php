<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package ManageBrand
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To manage user type page access permissions.
 */
global $LANG;
$brandlist = $this->settings->getBrand();
$brandlistrows = isset($brandlist['getBrand']) ? $brandlist['getBrand'] : array();
if ($brandlist['total_records'] == 1) {
    $brandlistrows = array($brandlistrows);
}
?>
<div class="content-wrapper" id="manage-members">
    <div class="con-title-sec pos-fixed mt40">
      <h1><span class="icon icon-lmo"></span><?php echo $LANG['manageBrand']; ?></h1>
      <div class="user-features">
          <ul>
            <li>
                <a href="../admin/index.php?p=settings"
                    title="<?php echo $LANG['backToSettings']; ?>">
                    <span class="icon icon-back"></span>
                </a>
            </li>
          </ul>
      </div>
    </div>
    <div class="con-sec pt100">
    <!-- <div class="select-custom clear-left-menu"></div> --><!--EDISON 20160120 -->
    <div class="row-sec member-search-sec">
        <form name="managebrand" id="managebrandForm" action="" method="post">
            <div class="col6">
                <label>&nbsp;</label>
            </div>
            <div class="col6">
    <?php if ($_SESSION['page_add'] == 1) {
    ?>
            <a href="javascript:void(0);"
                onclick="showQuickAddPop('Add Brand','manageBrand');"
                class="btn black-btn fr">
                <?php echo $LANG['addBrand'];
    ?></a>
    <?php
    } ?>
            </div>
            <input type="hidden" class="form-control loggedUserId"
                value="<?php echo $_SESSION['user']['user_id']; ?>"
                name="loggedUserId"  />
        </form>
    </div>
        <!-- Flash message begins -->
        <div>
    <?php

    if (isset($_SESSION['flMsg'])) {
        if (isset($_SESSION['flMsg']['flashMessageError'])) {
            echo '<div class="pageFlashMsg error">'.
                $_SESSION['flMsg']['flashMessageError'].'</div>';
        } elseif (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
            echo '<div class="pageFlashMsg success">'.
                $_SESSION['flMsg']['flashMessageSuccess'].'</div>';
        }
        unset($_SESSION['flMsg']);
    }

    ?>
        <div>&nbsp;</div>
    </div>
    <!-- Flash message ends -->

      <div class="tabOuterDiv">
            <ul class="tabs">
                <li class="current"><a href="#tab-1"><?php echo $LANG['brands']; ?></a></li>
            </ul>
            <div class="clear"></div>
            <div class="tabs-container">
            <div id="tab-1" class="tabscontent">
                <div class="clear"></div>
             <!--grid-->
                <div class="grid-block">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr class="grid-title">
                                <td><?php echo $LANG['sno'];?></td>
                                <td><?php echo $LANG['brandName']; ?></td>
                                <td><?php echo $LANG['brandDescription']; ?></td>
                                <!-- ED 20160501 -->
                                <td class="actionwidth txt-center"><?php echo $LANG['action']; ?></td>
                            </tr>
                        </thead>
                        <tbody>
        <?php 
                        $sno = 1;
        if (is_array($brandlistrows) && count($brandlistrows) > 0) {
            $result = '';
            foreach ($brandlistrows as $res) {
                $editLink = '';
                $deleteLink = '';
                if ($_SESSION['page_edit'] == 1) {
                    $editLink = <<<EDIT_LINK
                        <a title="{$LANG['titleEdit']}" 
                            class="btn-link btn-inline dotline-sep icon-edit-menu" 
                            onclick="showQuickAddPop('Edit Brand',
                                'manageBrand',{$res['brand_id']});">
                            <span class="icon icon-edit"></span>
                        </a>
EDIT_LINK;
                }
                if ($_SESSION['page_delete'] == 1) {
                    $deleteLink = <<<EDIT_LINK
                        <a title="{$LANG['titleDelete']}" 
                        class="btn-link btn-inline icon-delete-menu" 
                            menuIcoId="{$res['brand_id']}" 
                            onclick="deleteBrand('Add/Edit Brand',
                                'deleteBrand',
                                {$res['brand_id']});">
                            <span class="icon icon-cls-sm"></span>
                        </a>
EDIT_LINK;
                }
                ?>
                                <tr>
                                <td><?php echo $sno?></td>
                                <td><?php echo $res['brand_name']?></td>
                                <td><?php echo $res['brand_description']?></td>
                                <td class="txt-center" >
                                    <?php echo $editLink.$deleteLink?>
                                </td>
                                </tr>
                                <?php ++$sno;
            }
        } else {
            ?>
                            <tr><td colspan="5">No Results Found</td></tr>
        <?php
        } ?>
                        </tbody>
                    </table>
                </div>
                </div>
        </div>
</div>