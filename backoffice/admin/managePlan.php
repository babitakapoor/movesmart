<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package Manageplan
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To display plan list.
 */

global $LANG;
/* To List the Clubs in Dropdown */

    //To search param
    $param['searchType'] = (isset($_REQUEST['searchType']) ? 
        $_REQUEST['searchType'] : '');
    $param['searchValue'] = (isset($_REQUEST['searchValue']) ? 
        ($_REQUEST['searchType'] == 'gender' ? 
            (strstr($_REQUEST['searchValue'], 'f') ? 
            '1' : '0') : $_REQUEST['searchValue']) : '');

    /*To sort param , If the param label field is empty / 
        default value should define at else part.*/
    $param['labelField'] = (isset($_SESSION['pageName'][$_REQUEST['p']])) ?
        $_SESSION['pageName'][$_REQUEST['p']] : 'plan_id';
    $param['sortType'] = (isset($_SESSION['pageName'][$_REQUEST['p']]) && 
        $_SESSION['sortType'] == 2) ? 'desc' : 'asc';

    //Company id
    $param['companyId'] = COMPANY_ID;

    //Specific/Authorized clubs
    /*$param['authorizedClubId'] =(isset($_SESSION['club']['authorizedClubId'])?
    $_SESSION['club']['authorizedClubId']:"");*/

    //Pagination code starts
    $limitStart = 0;
    $limitEnd = PAGINATION_SHOW_PER_PAGE;

if (isset($_REQUEST['offset']) && ($_REQUEST['offset'] > 0)) {
    $limitEnd = $_REQUEST['offset'];
}
if (isset($_REQUEST['page']) && $_REQUEST['page'] > 0) {
    $limitStart = ($_REQUEST['page'] - 1) * $limitEnd;
}

    $param['limitStart'] = $limitStart;
    $param['limitEnd'] = $limitEnd;
    //Pagination code ends

    $param['companyId'] = COMPANY_ID;

    $arrayListMember = $this->admin->getPlanListByCompany($param);
    $arrayList = (!isset($arrayListMember['rows'][0])) ? 
        array($arrayListMember['rows']) : $arrayListMember['rows'];

    //Total count to create pagination
    $totalCount = $arrayListMember['totalCount'];

    /* Search Labels */
    $customSearchArray = array(
        'plan_id' => 'Plan Id',
        'plan_name' => 'Plan Name',
        'is_active' => 'Active',
    );
?>
<div class="content-wrapper" id="manage-members">
<div class="con-title-sec pos-fixed mt40">
  <h1><span class="icon icon-set"></span><?php
    echo $LANG['planList']; ?></h1>
      <div class="user-features">
      <ul>
        <li>
            <a href="../admin/index.php?p=settings"
                title="<?php echo $LANG['backToSettings']; ?>">
                <span class="icon icon-back"></span>
            </a>
        </li>
      </ul>

       </div>
            </div>
            <div class="con-sec pt100">
                <div class="row-sec" align="center"><br/>
                    <div class="col9 successSetMessgae success-msg"
                        align="center" style="display:none;">
                        <div class="col9 fadeMsg"></div>
                    </div>
                </div>
              <div class="row-sec member-search-sec mb15">
                <form name="membersearch" id="searchFilterForm"
                    action="" method="get">
                  <div class="col6 widthcol3">
                    <label class="fl" for="searchType"><?php echo $LANG['select']; ?> :</label>
                    <div>
                      <input type="hidden" name="p" value="managePlan">
                      <input type="hidden" name="theme" value="2">
                      <input type="hidden" name="labelField" id="labelField"
                        value="<?php echo $param['labelField']; ?>">
                      <input type="hidden" name="sortType" id="sortType"
                        value="<?php echo $param['sortType']; ?>">

                      </div>
                    <div class="select-custom">
                      <select id="searchType" name="searchType">
                        <option value="">-<?php
                            echo $LANG['choose']; ?>-</option>
    <?php
    foreach ($customSearchArray as $row => $value) {
        $sel = (isset($param['searchType'])
            && $param['searchType'] == $row) ?
               "selected='selected'" : '';
        echo "<option value='".$row."' $sel>".$value.'</option>';
    }
                        ?>
                      </select>
                    </div>

                  </div>
                  <div class="col6 widthcol7">
                    <?php if ($_SESSION['page_edit'] == 1) {
    ?>
                  <a href="index.php?p=managePlanEdit"
                    class="btn black-btn fr"><?php echo $LANG['addPlan'];
    ?></a>
    <?php
    } ?>
                    <a href="index.php?p=managePlan">
                        <input type="button" value="<?php
                            echo $LANG['clear'];?>" class="btn black-btn fr"
                            id="clear_search" />
                    </a>
                    <input type="submit" value="<?php echo $LANG['search'];?>"
                        id="searchFilterSubmit" class="btn black-btn fr" />
                     <label for="searchValue" style="display: none;"></label>
                    <input type="text" name="searchValue" id="searchValue"
                        class="fr wid40"value="<?php echo
                            isset($_REQUEST['searchValue']) ?
                            $_REQUEST['searchValue'] : ''; ?>"/>
                  </div>

                </form>
              </div>
              <div class="tabOuterDiv">
                <ul class="tabs">
                    <li class="current"><a href="#tab-1"><?php echo
                        $LANG['plans']; ?></a></li>
                </ul>
                <div class="clear"></div>
                <div class="tabs-container">
                <div id="tab-1" class="tabscontent testResultListGrid">
                     <div class="clear"></div>
                     <p class="mb15"><?php echo $LANG['plansTotal']; ?>:
                        <span class="count-block"><?php echo $totalCount; ?>
                        </span> </p>
                     <!--grid-->
                    <div class="grid-block">
                     <input type="hidden" class="paramNone"
                     value="?p=managePlanEdit">
                     <!--If navigate back to the page. Move the all query
                     string to another page -->
                      <table width="100%" border="0" cellspacing="0"
                      cellpadding="0" id="memberListGridTab">
                        <thead>
                        <tr class="grid-title">
                          <td>

        <?php echo $LANG['planId'];?>
                              <span class="spinner">
                                  <a href="javascript:;"
                                    onClick="sortingField('plan_id',
                                    '1','<?php echo $_REQUEST['p']; ?>')"
                                    class="spi-arr-up"></a>
                                  <a href="javascript:;" onClick="
                                  sortingField('plan_id', '2',
                                  '<?php echo $_REQUEST['p']; ?>')"
                                  class="spi-arr-down"></a>
                              </span>
                          </td>
                          <td>
        <?php echo $LANG['planName'];?>
                            <span class="spinner">
                                <a href="javascript:;"
                                onClick="sortingField('plan_name', '1','<?php
                                echo $_REQUEST['p']; ?>')" class="spi-arr-up">
                                </a>
                                <a href="javascript:;"
                                onClick="sortingField('plan_name', '2',
                                '<?php echo $_REQUEST['p']; ?>')"
                                class="spi-arr-down"></a>
                            </span>
                          </td>
                          <td>
        <?php echo $LANG['isActive'];?>
                              <span class="spinner">
                                    <a href="javascript:;"
                                    onClick="sortingField('is_active', '1',
                                    '<?php echo $_REQUEST['p']; ?>')"
                                    class="spi-arr-up"></a>
                                      <a href="javascript:;"
                                      onClick="sortingField('is_active', '2',
                                      '<?php echo $_REQUEST['p']; ?>')"
                                      class="spi-arr-down"></a>
                                  </span>
                              </td>
                              <!-- ED 20160501 -->
                              <td class="txt-center"><?php
                                echo $LANG['action'];?></td>
                            </tr>
                            </thead>
        <?php 
        if ((isset($arrayList) && !empty($arrayList))
            && (isset($arrayList[0]['plan_id']))
        ) {
            $deleteLink = '';
            $editLink = '';
            if ($_SESSION['page_edit'] == 1) {
                $editLink = '<a title="'.$LANG['titleEdit'].'" 
                onclick="redirectEdit(this,2)" hrefValue="$plan_id$" 
                class="btn-link btn-inline dotline-sep">
                <span class="icon icon-edit"></span></a>';
            }
            if ($_SESSION['page_delete'] == 1) {
                $deleteLink = '<a title="'.$LANG['titleDelete'].'" 
                onclick="deleteRow($plan_id$)" href="#" class="btn-link 
                btn-inline "><span class="icon icon-cls-sm"></span></a>';
            }
            echo $this->paginator->displayItemsPagination(
                $arrayList, array(
                'plan_id', 'plan_name', 'is_active', 
                'specValues2' => ''.$editLink.$deleteLink, ), 
                array('startTag' => '<tr>', 'midTagOpen' => '<td>',
                'midTagClose' => '</td>', 'endTag' => '</tr>')
            );
        } else {
            echo '<tr><td colspan="20">'.$LANG['noResult'].'</td></tr>';
        }
                                ?>

                          </table>
                        </div>
                        <div class="pagination-block">
        <?php
        if ((isset($arrayList) && !empty($arrayList))
            && (isset($arrayList[0]['plan_id']))
        ) {
            $this->paginator->getUrlLink($param = 1);
            echo $this->paginator->displayPagination($totalCount);
        }
                        ?>
                        </div>
                    </div>

                  </div>
                </div>
              </div>
</div>