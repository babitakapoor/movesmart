<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package CoachManagementEdit
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Page displays the personal Information of the particular coach.
 */
global $LANG;
//Get Coach Details
$coach_ID = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
if ($coach_ID > 0) {
    $param['user_id'] = $coach_ID;
    $jsonPersonalInfo 
        = $this->coach->getUserPersonalInfoDetails($param);
    $coachDetails = $jsonPersonalInfo['movesmart']['personalInfoDetails'];
	
 //print_r($coachDetails);
}//End

/* Workstation Dropdown */
$params['company_id'] = COMPANY_ID;
$params['is_deleted'] = 0;
$params['authorizedClubId'] = (isset($_SESSION['club']['authorizedClubId']) ?
    $_SESSION['club']['authorizedClubId'] : '');
$clubList = $this->club->getClubList($params);
/* Workstation Dropdown End */

$cityId = isset($coachDetails['r_city_id']) ? $coachDetails['r_city_id'] : '';
$members_center_id = isset($coachDetails['r_club_id']) ? 
    $coachDetails['r_club_id'] : '';

$gender = isset($coachDetails['gender']) ? $coachDetails['gender'] : '';
$status = isset($coachDetails['r_status_id']) ? $coachDetails['r_status_id'] : 1;

//Profile Image  parameters
$param_pi['imageName'] = isset($coachDetails['userimage']) ? 
    $coachDetails['userimage'] : '';
$param_pi['userType'] = 'coach';
$param_pi['userId'] = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
$param_pi['imgSize'] = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
$param_pi['size'] = 'm'; /* 
    Added by M.JahabarYusuff to mention the size of image has to 
    show(s-small or m-medium) */
$utype=$_SESSION['user']['usertype_id'];
$resetPwdOption = empty($userId) ? "checked='checked'" : '';

$navigation = array();
$urlNavigation='';
if (isset($_GET['id'])) {
    /*     * Used to, If navigate back to the page. Move the 
        all query string to another page */
    $urlAll = '?'.$_SERVER['QUERY_STRING'];
    $url = preg_replace('/(?)p=[A-Za-z]+/i', 'p=coachList', $urlAll);

    /* Used to, Icons Navigation in next,previout,first,last records Id */
    $param['user_id'] = $coach_ID;
    $param['authorizedClubId'] = (isset($_SESSION['club']['authorizedClubId']) ?
        $_SESSION['club']['authorizedClubId'] : '');
    $navigation = $this->members->getEmployeeIdForNavigation($param);

    /* URL for Navigation */
    $urlNavigation = preg_replace('/(?)&id=[0-9]+/i', '', $urlAll);
}

//Add user roles begins
$userId = $coach_ID;
if (isset($_POST['assignRole'])) {
    if ($_POST['userTypeId'] > 0 && $_POST['clubIdRole'] > 0) {
        $paramRole['userTypeId'] = $_POST['userTypeId'];
        $paramRole['clubId'] = $_POST['clubIdRole'];
        $paramRole['companyId'] = $params['company_id'];
        $paramRole['userId'] = $userId;
        $insertEmployeeClubRole = $this->club->insertEmployeeClubRole($paramRole);
    }
}
//Add user roles ends
?>
<div class="content-wrapper" id="coachManagement">  
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon icon-perinfo"></span>
            <?php echo $LANG['coachManagement']; ?></h1>
        <div class="user-features">
            <ul>
    <?php 
    if (isset($coach_ID)) {
    ?>
                    <li class="dotline-sepl">
        <?php 
        if (isset($navigation['min_user_id']) 
            && ($navigation['min_user_id'] != $coach_ID)
        ) {
        ?>
                            <a title="<?php echo $LANG['titleFirst'];
    ?>" href="<?php echo $urlNavigation.'&id='.$navigation['min_user_id'];
    ?>" class="btn-link"><span class="icon icon-arrfirst"></span></a>
<?php 
        } else {
    ?>
                            <span title="<?php echo $LANG['titleFirst'];
    ?>" class="noCursor icon icon-arrfirst "></span>
                        <?php 
        }
    ?>
                    </li>
                    <li>
    <?php 
    if (isset($navigation['previd']) 
        && $navigation['previd'] != '' 
        && ($navigation['min_user_id'] != $coach_ID)
    ) {
    ?>
                            <a title="<?php echo $LANG['titlePrevious'];
    ?>" href="<?php echo $urlNavigation.'&id='.$navigation['previd'];
    ?>" class="btn-link"><span class="icon icon-arrpre"></span></a>
<?php 
    } else {
    ?>
                            <span title="<?php echo $LANG['titlePrevious'];
    ?>" class="noCursor icon icon-arrpre "></span>
                        <?php 
    }
    ?>
                    </li>
                    <li>
    <?php 
    if (isset($navigation['nextid']) && $navigation['nextid'] != '' 
        && ($navigation['max_user_id'] != $coach_ID)
    ) {
    ?>
                        
                        <a title="<?php echo $LANG['titleNext'];
    ?>" href="<?php echo $urlNavigation.'&id='.$navigation['nextid'];
    ?>" class="btn-link"><span class="icon icon-arrnext"></span></a>
<?php 
    } else {
    ?>
                            <span title="<?php echo $LANG['titleNext'];
    ?>" class="noCursor icon icon-arrnext"></span>
                        <?php 
    }
    ?>
                    </li>
                    <li class="dotline-sep">
    <?php 
    if (isset($navigation['max_user_id']) 
        && ($navigation['max_user_id'] != $coach_ID)
    ) {
    ?>
                            <a title="<?php echo $LANG['titleLast'];
    ?>" href="<?php echo $urlNavigation.'&id='.$navigation['max_user_id'];
    ?>" class="btn-link"><span class="icon icon-arrlast"></span></a>
<?php 
    } else {
    ?>
                            <span title="<?php echo $LANG['titleLast'];
    ?>" class="noCursor icon icon-arrlast"></span>
<?php 
    }
    ?>
                    </li>
                    <li class="icon-hide"><a href="#" 
                        title="<?php echo $LANG['underConstruction'];?>" 
                        class="btn-link"><span class="icon icon-search">
                        </span></a></li>
                    <li class="dotline-sep icon-hide"><a 
                        title="<?php echo $LANG['underConstruction'];?>" 
                        href="#" class="btn-link"><span 
        class="icon icon-useminus"></span></a></li>
                    <li><a href="#" 
                    title="<?php echo $LANG['underConstruction'];?>" 
    <?php 
    if (isset($_REQUEST['id'])) {
        echo "onclick='deleteRow($userId)'";
    }
    ?> class="btn-link"><span class="icon icon-close"></span></a></li>         
                    <li class="icon-hide"><a href="#" 
                        title="<?php echo $LANG['underConstruction'];?>" 
                        class="btn-link"><span class="icon icon-useredit">
                        </span></a></li>
                    <li class="icon-hide"><a href="#" 
                    title="<?php echo $LANG['underConstruction'];?>" 
                    class="btn-link"><span 
                        class="icon icon-useadd"></span></a></li>
    <?php 
    } 
    ?>     
                <li><a title="<?php echo $LANG['titleSave']; ?>" class="btn-link">
                        <span class="icon icon-save" 
                        id="btnSaveCoach"></span></a>               
                </li>
                <li>
                    <a href="index.php?p=coachList" 
                        title="<?php echo $LANG['titleBack']; ?>">
                        <span class="icon icon-back" id="btnSaveCoach"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="con-sec pt100">
        <form name="personalInfoForm" id="personalInfoForm" method="post" 
            enctype="multipart/form-data">
            <input type="hidden" id="company_id" name="company_id" 
                value="<?php echo COMPANY_ID; ?>"/>
            <input id="client_image" type="hidden" 
                value="<?php echo $userImage; ?>" name="client_image">
            <input id="client_image_old" type="hidden" 
                value="<?php echo $userImage; ?>" name="client_image_old">
            <div class="accordion-holder mb20">
                <div class="acc-row">
                    <div class="row-sec" align="center"><div class="col9 
                        successSetMessgae success-msg" align="center" 
                        style="display:none;">
                            <div style="background-color:#fff;color:#000;">
                            <?php echo $LANG['requiredResult']; ?></div>
                        </div>
                        <?php
                        if (isset($_SESSION['flMsg']['flashMessageSuccess'])) {
                            $ticksuccess    =   IMG_PATH.DS.THEME_NAME.DS."/tick_success.png";
                            ?>
                            <div class="pageFlashMsg success">'.
                            <div class="cussuccess-icon"><img src="<?php echo $ticksuccess;?>"/></div>
                                <div>
                                    <?php echo $_SESSION['flMsg']['flashMessageSuccess']?>
                                </div>
                            </div>
                        <?php
                            unset($_SESSION['flMsg']);
                        }
                        ?>
                    </div>
                    <h2><?php echo $LANG['generalPersonalInfo']; ?> <span 
                        class="icon icon-down"></span></h2>
                    <div class="acc-content">
                        <div class="row-sec">
                            <div class="col8">
                                <div class="row-sec">
                                    <div class="col3">
                                        <label class="fl" for="first_name" >
                                        <?php echo $LANG['firstName']; ?> :
                                        <span class="required">*</span></label>
                                        <input type="text" id="first_name" 
                                            name="first_name" class="form-control" 
                                            value="<?php echo 
                                                isset($coachDetails['first_name']) ?
                                                $coachDetails['first_name'] : ''; 
                                                ?>"/>
                                    </div>

                                    <div class="col3">
                                        <label class="fl" for="last_name" >
                                        <?php echo $LANG['lastName']; ?> :
                                        <span class="required">*</span></label>
                                        <input type="text" class="form-control" 
                                        id="last_name" name="last_name" value="<?php echo isset($coachDetails['last_name']) ? $coachDetails['last_name'] : ''; ?>" />
                                    </div>

                                    <div class="col3">
                                        <label class="fl" for="userId" ><?php echo
                                            $LANG['employeeCode']; ?> :</label>
                                        <input type="text" class="form-control 
                                        txt-bx-readonly" id="userId" name="userId"
                                        value="<?php echo 
                                        isset($coachDetails['user_id']) ?
                                        $coachDetails['user_id'] : ''; ?>"
                                        readonly="" />
                                    </div>  </div>
                                <div class="row-sec">
                                    <div class="col3">
                                        <label class="fl"><?php 
                                        echo $LANG['gender']; ?>  :</label>
                                        <div class="input-group">
                                            <div class="in-cell cus-check 
                                            get-check">
                                                <input type="radio" class="oxy" 
                                                value="0" name="gender" id="male" 
                                                <?php 
                                                if ($gender == 'male' 
                                                    || ($gender == '')
                                                ) {
                                                ?> checked="checked" <?php 
                                                }
                                                ?> >
                                                <label for="male"><?php 
                                                    echo $LANG['male']; ?></label>
                                            </div>
                                            <div class="in-cell cus-check 
                                                get-check">
                                                <input type="radio" class="oxy" 
                                                    value="1" name="gender" 
                                                    id="female" 
                                                    <?php 
                                                    if ($gender == 'female') {
                                                    ?> checked="checked" <?php 
                                                    } 
                                                    ?> >
                                                <label for="female">
                                                <?php echo $LANG['female']; ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col3">
                                        <label class="fl">
                                            <?php echo $LANG['status']; ?>  :
                                            </label>
                                        <div class="input-group">
                                            <div class="in-cell 
                                            cus-check get-check">
                                                <input type="radio" 
                                                class="oxy" value="1" 
                                                name="status_id" id="act" 
<?php 
if ($status == '1') {
    ?> checked="checked" <?php 
} 
?> >
                                                <label for="act"><?php 
                                                echo $LANG['active']; ?></label>
                                            </div>
                                            <div class="in-cell cus-check 
                                                get-check">
                                                <input type="radio" class="oxy" 
                                                value="2" name="status_id" id="act1"
<?php
if ($status == '2') {
?> checked="checked" 
<?php 
} 
?> >
                                                <label for="act1">
                                                <?php echo $LANG['inactive']; ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col3">
                                        <label class="fl" for="phone">
                                        <?php echo $LANG['phone']; ?> :
                                        </label>
                                        <input type="text" class="form-control"
                                        id="phone" name="phone" value="<?php echo 
                                        isset($coachDetails['phone']) ? 
                                        $coachDetails['phone'] : ''; ?>" />
                                    </div>  </div>
                                <div class="row-sec">
                                    <div class="col3">
                                        <label class="fl" for="mobile"><?php
                                        echo $LANG['GSM']; ?> :
                                        <span class="required">*</span>
                                        </label>
                                        <input type="text" class="form-control"
                                        id="mobile" name="mobile" value="<?php 
                                        echo isset($coachDetails['mobile']) ? 
                                        $coachDetails['mobile'] : ''; ?>" />
                                    </div>
                                    <div class="col3">
                                        <label class="fl" for="doorNo">
                                        <?php echo $LANG['no']; ?> / 
                                        <?php echo $LANG['bus']; ?> :
                                        <span class="required">*</span>
                                        </label>
                                        <input type="text" class="form-control" 
                                        name="doorNo" 
                                        id="doorNo" 
            value="<?php 
            echo isset($coachDetails) ?$coachDetails['doorno'] : ''; ?>" 
             style="width:56px;float:left;margin-right:10px;" />
                                        <label for="bus_no" style="display: none;"></label>
                                        <input type="text" class="form-control" 
                                        name="bus_no" id="bus_no" value="<?php echo 
                                        isset($coachDetails) ? $coachDetails['bus']
                                        : ''; ?>" style="width:56px;" />
                                    </div>

                                    <div class="col3">
                                        <label class="fl" for="address"><?php
                                        echo $LANG['street']; ?> :
                                        <span class="required">*</span></label>
                                        <input type="text" class="form-control" 
                                        id="address" name="address" value="<?php 
                                        echo isset($coachDetails['address']) ?
                                        $coachDetails['address'] : ''; ?>" />
                                    </div></div>
                <div class="row-sec">
                    <div class="col3">
                        <label class="fl"><?php 
                        echo $LANG['zipCode']; ?> :</label>                    
                        <!--input type="text" id="zipCode" name="zipCode" 
                        class="form-control" value="<?php 
                        echo isset($coachDetails['zipcode']) ? 
                        $coachDetails['zipcode'] : ''; ?>" -->
                        <div class="cus-combobox autoselectfld 
                            zipCode cus-add-fld">
                            <label style="display: none;" for="zipCode" ></label>
                            <select name="zipCode" id="zipCode" 
                                class="autoCompletebox">
                <?php
                $parms  =   isset($parms) ? $parms:array();
                $zipCodeLists = $this->members->getCityList($parms);
                echo '<option value="">Select Zipcode</option>';

                foreach ($zipCodeLists as $zipCodeList) {
                    $sel 
                        = ($zipCodeList['city_id'] == $coachDetails['r_city_id']) ?
                        "selected='selected'" : '';
                    echo '<option value="'.$zipCodeList['city_id'].'" '.$sel.'>'.
                    $zipCodeList['zipcode'].'</option>';
                }
                ?>
                            </select>
                            <div class="puls-box" 
                                onclick="showQuickAddPop('<?php 
                                echo $LANG['addCity']; ?>', 'addCity');">
                                <span class="icon quick_add_icon"></span></div>
                        </div>
                    </div>
                    <div class="col3">
                        <label class="fl"><?php echo $LANG['city']; ?> :</label>
                        <!--div class="cus-combobox autoselectfld">
                        <select name="city" id="city" class="autoCompletebox">
                        <?php
                        $parms = '';
                        $cityLists = $this->members->getCityList($parms);

                        echo '<option value="">Select City</option>';

                        foreach ($cityLists as $cityList) {
                            if ($cityList['city_id'] == $cityId) {
                                $sel = "selected='selected'";
                            } else {
                                $sel = '';
                            }
                            echo '<option value="'.$cityList['city_id'].'" '.
                            $sel.'>'.$cityList['city_name'].'</option>';
                        }
                        ?>
                          </select>
                        </div-->
                        <div class="cus-combobox autoselectfld cityName">
                <label style="display: none;" for="city" ></label>
                            <select name="city" id="city" class="autoCompletebox">
                                <?php
                                $cityLists = $this->members->getCityList($parms);
                                echo '<option value="">Select City</option>';

                                foreach ($cityLists as $cityList) {
                                    $sel = ($cityList['city_id'] == $cityId) ?
                                    "selected='selected'" : '';
                                    echo '<option value="'.$cityList['city_id'].
                                    '" '.$sel.'>'.$cityList['city_name'].
                                    '</option>';
                                }
                                ?>
                            </select>
                            <!-- Quick add city -->
                            <!-- Quick add city -->
                        </div>
                    </div>                                    

                    <div class="col3">
                        <label class="fl" for="workStart" ><?php echo $LANG['workStarton']; ?>  :
                        <span class="required">*</span></label>
                        <input type="text" class="form-control icon-datepiker" 
                        id="workStart" name="workStart" value="<?php echo 
                        isset($coachDetails['work_starton']) ? 
                        $this->common->siteDateFormat($coachDetails['work_starton'])
                        : ''; ?>" />     
                    </div>
                </div>
                        <div class="row-sec">
                            <div class="col3">
                            <label class="fl" for="workEnd" >
                            <?php echo $LANG['workEndon']; ?> : <span class="required">*</span></label>
                            <input type="text" class="form-control 
                            icon-datepiker" id="workEnd" name="workEnd" 
                        value="<?php echo isset($coachDetails['work_endon']) ?
                        $this->common->siteDateFormat($coachDetails['work_endon'])
                        : ''; ?>"/>
                            </div>
                              <div class="col3">
                                <label class="fl" for="person_id" ><?php echo $LANG['personId']; ?>:
                                </label>
                                <input type="text" class="form-control 
                                <?php echo isset($_REQUEST['id']) ? 
                                'txt-bx-readonly' : '';
                                ?>" id="person_id" name="person_id" value="<?php 
                                echo isset($coachDetails['person_id']) ? 
                                $coachDetails['person_id'] : ''; ?>" <?php 
                                echo isset($_REQUEST['id']) ? 
                                "readonly='readonly'" : 
                                ' onblur="personIdExist(this)"'; ?> />
                                <label class="fl" style="height: 1px;">&nbsp;
                                </label>
                                <p class="hint">
                                <?php echo $LANG['uniqueMessage']; ?></p>
                            </div>
                            <div class="col3">
                                <label class="fl" for="securityNo" >
                                    <?php echo $LANG['socialSecurity']; ?> :
                                </label>
                                <input 
                                    type="text" 
                                    class="form-control" id="securityNo" 
                                    name="securityNo" 
                                    value="<?php 
                                    echo ( isset($coachDetails['securityno']) ? 
                                        $coachDetails['securityno'] : ''
                                    ); ?>"/>
                            </div>
                        </div>
                        <div class="row-sec">
                            <div class="col3">
                                <label class="fl" for="membercenterid">
                                    <?php echo $LANG['primaryClub']; ?> :
                                    <span class="required">*</span></label>
                                <div class="select-custom <?php 
                                    echo (isset($members_center_id) 
                                    && $members_center_id != '') ?
                                    'txt-bx-readonly' : '' ?>">
                                    <label style="display: none;" for=""></label>
                                    <select <?php echo (isset($members_center_id) && $members_center_id != '') ?
                                        'disabled="disabled" ' : '' ?>
                                        onchange="sendDataToHidden(this.value, 'club_id')" id="membercenterid">
                        <?php
                        $paramsC['authorizedClubId'] = '';
                        $paramsC['company_id'] = COMPANY_ID;
                        $paramsC['is_deleted'] = 0;
                        $paramsC['authorizedClubId']
                            =(isset($_SESSION['club']['authorizedClubId']) ? 
                            $_SESSION['club']['authorizedClubId'] : '');
                        $clubList
                            =$this->club->getClubList($paramsC);
							
                        if ($utype==4) {
                            echo "<option value=''>
                                    ---Select Club---
                                </option>";
                        }
                        foreach ($clubList as $row) {
							
                            $isFlag = 0;
                            if (isset($clubExistIds) and is_array($clubExistIds)) {
                                $isFlag=in_array($row['club_id'], $clubExistIds);
							
                            }
                            if (!$isFlag) {
								
                                $selected ='';
                                if ($members_center_id==$row['club_id']
                                ) {
                                    $selected = "selected='selected'";
                                }
                                echo "<option 
                                    value=
                                    '".$row['club_id']."' 
                                    $selected>".
                                    $row['club_name'].'
                                </option>';
                            }
                        }
                        ?></select>
                        <?php
                        $primaryClubId = '';
                        if (!isset($_REQUEST['id'])) {
                            if ($members_center_id == '') {
                                $primaryClubId = $_SESSION['primaryClubId'];
                            }
                        }
                        ?>
                                    <input type="hidden" id="club_id" name="club_id"
                                    value="<?php echo ($members_center_id > 0) ? 
                                    $members_center_id : $primaryClubId; ?>" />
                                </div>     
                            </div>
                            <div class="col3">
                                <label class="fl">
                                    <?php echo $LANG['assignRole']; ?> :
                                </label>
                                <div class="select-custom">
                                    <label for="user_type_id" style="display: none;" ></label>
                                    <select id="user_type_id" name="user_type_id">
                                    <?php 
                                    $usertype_id 
                                        = isset($coachDetails['r_usertype_id']) ?
                                    $coachDetails['r_usertype_id'] : ''; ?>
                                    <option 
                                        value="1" 
                                        <?php 
                                        if ($usertype_id == 1) {
                                            ?> 
                                        selected="selected" <?php 
                                        } ?> >Coach</option>
                                    <option value="9"
                                    <?php 
                                    if ($usertype_id == 9) {
                                        ?> 
                                    selected="selected" <?php 
                                    } 
                                    ?>>
                                    Main coach</option>
									<!--
                                    <option value="4" 
                                    <?php 
                                    if ($usertype_id == 4) {
                                        ?> 
                                        selected="selected" <?php 
                                    } 
                                    ?>>
                                    Back Office</option>-->
                                    </select>
                                </div>
							</div>
							<!------------------------------------>
							<div class="col3">
                              <div class="input-group">
							  <?php 
							  if(isset($coachDetails['relation_coach']))
							  {
								$rel_coach = explode(',',$coachDetails['relation_coach']);
                                        foreach($rel_coach as $value)
										{
											if($value == 1)
											{
												$val = $value;
											}
											else if($value == 2)
											{
												$val1 = $value;
											}
										}
							  }
										?>
                                    <div>
                                        <input 
                                            type="checkbox" 
                                            class="oxy" 
                                            value="1" 
                                            name="client_app[]"
											<?php if(isset($val) && $val == 1){?>checked<?php }?>>
                                        <label for="client_app">
                                           Client app
                                        </label>
										<br/>
										 <input 
                                            type="checkbox" 
                                            class="oxy" 
                                            value="2" 
                                            name="client_app[]"
											checked>
											<?php //if(isset($val1) && $val1 == 2){?>
                                        <label for="couch_app">
                                          Coach app
                                        </label>
                                    </div>
										
                                </div>
                            </div>
							<!--------------------------------------->
						</div>
						
                        <div class="row-sec" style="display:none;">
                            <div class="col3">
                                <label class="fl">
                                    <?php echo $LANG['commercialUser']; ?>:
                                </label>
                                <div class="input-group">
                                    <div 
                                        class="in-cell cus-check get-check">
                                        <input 
                                            type="radio" 
                                            class="oxy" 
                                            value="Act" 
                                            name="act" 
                                            id="Act" 
                                            checked="checked">
                                        <label for="Act">
                                            <?php echo $LANG['active']; ?>
                                        </label>
                                    </div>
                                    <div class="in-cell cus-check get-check">
                                        <input type="radio" 
                                            class="oxy" value="Act2" 
                                            name="act" id="Act2">
                                        <label for="Act2">
                                            <?php echo $LANG['inactive']; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col3">
                                <label class="fl">
                                    <?php echo $LANG['backOffice']; ?>:
                                </label>
                                <div class="input-group">
                                    <div class="in-cell cus-check get-check">
                                        <input type="radio" class="oxy" value="yes" name="YesNo" id="yes">
                                        <label for="yes">
                                            <?php echo $LANG['yes']; ?>
                                        </label>
                                    </div>
                                    <div 
                                        class="in-cell cus-check get-check">
                                        <input 
                                            type="radio" 
                                            class="oxy" 
                                            value="No" 
                                            name="YesNo" 
                                            id="No" 
                                            checked="checked">
                                        <label for="No">
                                            <?php echo $LANG['no']; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col3">
                        <div class="user-photo-holder dotline-sepl">
                            <?php 
                            $display='display:block';
                            if ($param_pi['imageName'] == '') {
                                $display='display:none';
                            }?>
                            <div class="photo-cls" style="<?php echo $display?>">
                                <?php $delimg=IMG_PATH.DS.'delete-image.png'; ?>
                                <a id="delete-member-image" class="delete-image"
                                   data-role-deleteimg="user-up-photo.jpg"
                                   data-role-defpath="<?php echo IMG_PATH.DS;?>" style="" >
                                    <img id="preview_user_image" src="<?php echo $delimg;?>" alt="Delete Image" />
                                </a>
                            </div>
                            <?php ?>
                            <div class="user-up-photo">
                                <img id="imagePreview"
                                     src="<?php echo $this->common->getProfileImage($param_pi); ?>"
                                     width="146" height="128" alt="" />
                            </div>
                            <div class="row-sec">
                                <div class="fileUpload btn black-btn">
                                    <span>
                                        <?php echo $LANG['upload']; ?>
                                    </span>
                                    <input 
                                        type="file" 
                                        class="upload" 
                                        onchange="displayImage(this,'n',
                                            '<?php echo $userId; ?>', 
                                            'profileCoachImage')"
                                        accept="image/*" 
                                        id="uploadBtn" 
                                        name="profileImage" />
                                </div>
                            </div>
                            <span id="member_image_err" class="error_msg"></span>
                            <span 
                                style="display:none;" 
                                class="ajax_image_loader ajax-loader">
                                <?php echo $LANG['loading']; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="acc-row form-control-groub">
            <h2><?php echo $LANG['loginDetails']; ?>
                <span class="icon icon-down"></span>
            </h2>
            <div class="acc-content">
                <div class="row-sec">
                    <div class="col3">
                        <label class="fl" for="email">
                            <?php echo $LANG['emailID']; ?> :
                            <span class="required">*</span>
                        </label>
                        <?php $cemail='';
                        if (isset($coachDetails['email'])) {
                            $cemail  =   $coachDetails['email'];
                        }?>
                        <input 
                            type="text" 
                            class="form-control" 
                            id="email" 
                            name="email" 
                            value="<?php echo $cemail; ?>" />
                    </div>
                    <div class="col3">
                        <label class="fl" for="password" >
                            <?php echo $LANG['password']; ?> :
                            <span class="required">*</span>
                        </label>
                        <?php 
                        $pwd='';
                        if (isset($coachDetails['password'])) {
                            $pwd=$coachDetails['password'];
                        }?>
                        <input type="password" 
                            class="form-control" 
                            id="password" 
                            name="password" 
                            value="<?php echo $pwd; ?>" />
                    </div>
                    <div class="col3">
                        <label class="fl" for="cpassword">
                            <?php echo $LANG['confirmPassword']; ?> :
                        </label>
                        <input 
                            type="password" 
                            class="form-control" 
                            id="cpassword" 
                            name="cpassword" />
                    </div>
                </div>
                <div class="row-sec">                            
                <!--<div class="col3">
                        <label class="fl">
                            <?php echo $LANG['resetPassword'];?>  :
                        </label>
                        <div class="input-group">
                            <div class="in-cell cus-check get-check">
                                <input 
                                    type="radio" 
                                    class="oxy resetPwd" 
                                    value="1" 
                                    name="resetPwd" 
                                    id="yes1" 
                                    <?php echo $resetPwdOption; ?> >
                                <label for="yes1">
                                    <?php echo $LANG['yes']; ?>
                                </label>
                            </div>
                            <div class="in-cell cus-check get-check">
                                <input type="radio" 
                                    class="oxy resetPwd" 
                                    value="0" 
                                    name="resetPwd" 
                                    id="No1"
                                <?php echo!empty($userId)?
                                    "checked='checked'":'';?>>
                                <label for="No1">
                                    <?php echo $LANG['no']; ?>
                                </label>
                            </div>
                        </div>

                    </div> -->
                </div>
            </div>
        </div>     
        </form>
        <div class="acc-row form-control-groub coach-role-left">
            <div class="con-sec">
                <div class="tabOuterDiv">
                    <ul class="tabs">
                        <li class="current">
                            <a href="#tab-1"><?php echo $LANG['roleList']; ?></a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                    <div class="tabs-container">
                        <div id="tab-1" class="tabscontent">
                            <!-- Manage Roles -->
                            <?php
                            if (!empty($userId)) {
                                // Get employee role club ids begins
                                $emp    =   array();
                                $emp['userId']=$userId;
                                $emp['companyId'] = $params['company_id'];
                                $crole = $this->club->getEmployeeClubRoles($emp);
                                if (is_array($crole)) {
                                    foreach ($crole as $roles) {
                                        $clubExistIds[] = $roles['r_club_id'];
                                    }
                                }
                                // Get employee role club ids ends
                                include_once 'coachClubRoles.php';
                            }
                            ?>
                            <!-- Manage Roles -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <input 
        type="hidden" 
        class="form-control" 
        value="<?php echo $userId; ?>" 
        name="hiddenuserId" 
        id="hiddenuserId" />
    <input 
        type="hidden" 
        class="form-control" 
        value="<?php echo isset($_REQUEST['uid']) ? $_REQUEST['uid'] : 0; ?>" 
        name="uid" 
        id="uid" />
</div>
