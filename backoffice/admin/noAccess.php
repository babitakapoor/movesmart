<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package NoAccess
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Page no access page.
 */
?>
<div class="content-wrapper" id="coach-list">
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon icon-coachmng"></span>No Access</h1>
        <div class="user-features">
        </div>
    </div>
    <div class="con-sec pt100">
        <div class="row-sec" align="center">
            <br/>
        </div>
        <div class="tabOuterDiv">
            <h2>You don't have rights to access this page</h2>
        </div>
   </div>
</div>