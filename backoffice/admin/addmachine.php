<?php
/**
 * PHP version 5.
 
 * @category Admin
 
 * @package AdminAddMachines
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 
 
 
 * @link http://movesmart.company/admin/

 * @description Page to Add/Edit Strength Programs.
 */
global $LANG;
require_once '../lib/phpmailer/PHPMailerAutoload.php';
$machineid = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$programDetail = array();
if ($machineid > 0) {
    $param['editId'] = $machineid;
    $machinedetails = $this->settings->getNewMachinedetailsbyeditid($param);
    $machinedetail = $machinedetails['result'];
}
$brandlist = $this->settings->getBrand();
$brandlistrows = isset($brandlist['getBrand']) ? $brandlist['getBrand'] : array();
if ($brandlist['total_records'] == 1) {
    $brandlistrows = array($brandlistrows);
}
/*$grouplist = $this->settings->getGroup();
$grouplistrows = isset($grouplist['getGroup']) ? $grouplist['getGroup'] : array();
if ($grouplist['total_records'] == 1) {
    $grouplistrows = array($grouplistrows);
}*/
$grouplist = $this->settings->getMachineGroup();
$grouplistrows = isset($grouplist['groups']) ? $grouplist['groups'] : array();
if ($grouplist['total_records'] == 1) {
    $grouplistrows = array($grouplistrows);
}
$typelist = $this->settings->gettypelist();
$typelistrows = isset($typelist['data']) ? $typelist['data'] : array();
    // if ($typelist['total_records']==1) {
        // $typelistrows=array($typelistrows);
    // }
$subtypelist = $this->settings->getsubtypelist();
$subtypelistrows = isset($subtypelist['data']) ? $subtypelist['data'] : array();
    // if ($subtypelist['total_records']==1) {
        // $subtypelistrows=array($subtypelistrows);
    // }
$param['companyId'] = COMPANY_ID;
$arrayListMember = $this->club->getClubListByCompany($param);
if (!isset($arrayListMember['rows'][0])) {
    $clublist =  array($arrayListMember['rows']);
} else {
    $clublist =  $arrayListMember['rows'];
}
$params['clubid'] = 1;
$lastclubid = $this->members->getlastclubiddetails($params);
$maxclubidid = $lastclubid['result'];
$max_auto_club_id = '';
if ($maxclubidid == '' || $maxclubidid == null) {
    $i = 1;
    $val = $i;
    $max_auto_club_id = str_pad($val, 5, '0', STR_PAD_LEFT);
} else {
    $maxid = intval(substr($maxclubidid['maxclubid'], 2)) + 1;
    $max_auto_club_id = str_pad($maxid, 5, '0', STR_PAD_LEFT);
}
/*******for delete machine*********/
$id='';
if(isset($_GET['id'])){
$id = $_GET['id'];
}

if(isset($_GET['action']) && ($_GET['action']=='delete'))
{
	$str="update `t_strength_machine` set `is_deleted`= 1 where `strength_machine_id`=".$id;
	$delete = mysql_query($str);
	if($delete)
	{
	?>
	<script>
	
	alert("data is deleted");
	window.location="index.php?p=managemachine";
	</script>
	<?php 
	//header("location:index.php?p=report");
	}
}
/****************/



?>
<!-- <form method="post">
<div class="content-wrapper">  
    <div class="con-title-sec pos-fixed mt40">
        <h1><span class="icon icon-perinfo"></span>
        <?php echo $LANG['machine']; ?>
        </h1>
        <div class="user-features">
            <ul>
                <li><a title="<?php echo $LANG['titleSave']; ?>" 
                class="btn-link">
                <span class="icon icon-save" 
                    id="savemachine" 
                    onclick="saveMachine()"></span>
                </a>
                </li>
                <li>
                    <a href="index.php?p=managemachine"
                    title="<?php echo $LANG['titleBack']; ?>">
                        <span class="icon icon-back"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="con-sec pt100">
            <input type="hidden" id="companyId"
            name="companyId" value="<?php echo COMPANY_ID; ?>"/-->
            
            <input type="hidden" id="machineid"
            name="machineid"
            value="<?php
            echo
            isset($machinedetail['strength_machine_id'])?
            $machinedetail['strength_machine_id'] : 0; ?>"/>
            <!--input type="hidden"
            class="form-control
            loggedUserId"
            value="<?php echo $_SESSION['user']['user_id']; ?>"
            name="loggedUserId"  />
            <div class="accordion-holder mb20">
            <div class="acc-row">
            <div class="acc-row form-control-groub">
                <h2><?php echo $LANG['machinedetails']; ?>
                <span class="icon icon-down"></span></h2>
                    <div class="acc-content">
                        <div class="row-sec">
                            <div class="row-sec">
                                <div class="col4">
                                    <label class="fl">
                                    <?php echo $LANG['name']; ?>:
                                    <span class="required">*</span></label>
                                    <input type="text" id="machinename"
                                    name="description"
                                    class="form-control"
                                    value="
                                    <?php echo
                                    isset($machinedetail['machine_name'])?
                                    $machinedetail['machine_name'] :
                                    '';?>"/>

                                </div>
                                <div class="col4">
                                    <label class="fl">
                                    <span class="required">*</span></label>
                                    <input type="text" id="clubauotgnerateid"
                                        name="clubauotgnerateid"
                                        class="form-control"
                                        value=""/>
                                </div>
                                <div class="col4">
                                    <label class="fl">
                                    <?php echo $LANG['brand']; ?>:
                                    <span class="required">*</span></label>
                                    <div  class="select-custom">
                                        <select id="brandtype" name="type">
                                            <option value=''>-
                                            <?php echo $LANG['select']; ?> -
                                           </option>
            <?php
            foreach ($brandlistrows as $brand) {
                $sel =  '';
                if ($machinedetail['brand'] == $brand['brand_id']) {
                    $sel =  'selected=selected';
                }
                echo '<option value="'.$brand['brand_id'].'" '.$sel.'>'.
                    $brand['brand_name'].'</option>';
            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                   <div class="col4">
                                    <label class="fl">
                                        <?php echo $LANG['type']; ?>:
                                        <span class="required">*</span></label>
                                    <div  class="select-custom">
                                        <select id="machinetype" name="type">
                                            <option value=''>
                                            - <?php echo $LANG['select']; ?> -
                                           </option>
            <?php
            foreach ($typelistrows as $type) {
                $sel =  '';
                if ($machinedetail['type'] == $type['machine_type_id']) {
                    $sel =  'selected=selected';
                }
                echo '<option value="'.$type['machine_type_id'].'" '.$sel.'>'.
                    $type['machine_type'].'</option>';
            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col4">
                                    <label class="fl"><?php echo $LANG['group']; ?>
                                    :<span class="required">*</span></label>
                                    <div  class="select-custom">
                                        <select id="machinegroup" name="type">
                                            <option value=''>-
                                            <?php echo $LANG['select']; ?>
                                           -</option>
            <?php
            foreach ($grouplistrows as $group) {
                $sel =  '';
                if ($machinedetail['group'] == $group['group_id']) {
                    $sel =  'selected=selected';
                }
                echo '<option value="'.$group['group_id'].'" '.$sel.'>'.
                    $group['group_name'].'</option>';
            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col4">
                                    <label class="fl">
                                        <?php echo $LANG['subtype']; ?> :
                                       <span class="required">*</span>
                                        </label>
                                        <?php echo $LANG['subtype']; ?> :
                                        <span class="required">*</span></label>
                                    <div  class="select-custom">
                                        <select id="subtype" name="type">
                                            <option value=''>
                                           -<?php echo $LANG['select']; ?>-
                                           </option>
            <?php
            foreach ($subtypelistrows as $subtype) {
                $sel = '';
                if ($machinedetail['subtype'] == $subtype['subtype_id']) {
                    $sel = 'selected=selected';
                }
                echo '<option value="'.$subtype['subtype_id'].'" '.$sel.'>'.
                    $subtype['subtype_name'].'</option>';
            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col4">
                                    <label class="fl">
                                        <?php echo $LANG['club']; ?>:
                                        <span class="required">*</span>
                                       </label>
                                    <div  class="select-custom">
                                        <select id="club" name="type">
                                            <option value=''>-
                                            <?php echo $LANG['select']; ?> -
                                           </option>
            <?php
            foreach ($clublist as $club) {
                $sel =  '';
                if ($machinedetail['clubid'] == $club['club_id']) {
                    $sel =  'selected=selected';
                }
                echo '<option value="'.$club['club_id'].'" '.$sel.'>'.
                    $club['club_name'].'</option>';
            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col4">
                                    <label class="fl">
                                    <?php echo $LANG['description']; ?>
                                    :<span class="required">*</span></label>
                                    <input type="text" id="machinedescription"
                                    name="description" class="form-control"
                                    value="<?php
                                    echo isset($machinedetail['description']) ?
                                       $machinedetail['description'] : '';?>"/>
                                </div>
                                <div class="col4">
                                    <label class="fl">
                                    <?php echo $LANG['status']; ?> :
                                    <span class="required">*</span></label>
                                    <input type="radio"  name="status"
                                    class="form-control" value=""/>
                                    <label>Active</label>
                                    <input type="radio"  name="status"
                                    class="form-control" value=""/>
                                    <label>Out of Order</label>
                                    <input type="radio"  name="status"
                                    class="form-control"
                                    value=""/><label>Movesmart Certified</label>
                                </div>
                                <input type="hidden" value="" id="machineicon">
                                <div class="user-photo-holder dotline-sepl">
                                    <div class="photo-cls" style="display:block">
                                        <a id="delete-member-image"
                                        class="delete-image"
                                        data-role-deleteimg="user-up-photo.jpg"
                                        data-role-defpath="" style="">
                                    </div>
                                    <div class="user-up-photo">
                                        <img id="imagePreview"
                                        width="146" height="128"
                                        alt="Question icon" />
                                    </div>
                                <div class="row-sec mb15">
                                <div class="fileUpload btn black-btn">
                                <span><?php echo $LANG['browse']; ?></span>
                                <input type="file" class="upload"
                                onchange="displayImage(this,'n','','machineIcons')"
                                accept="image/*" id="uploadBtn"
                                name="menuIcons" id="menuIcons" />
                                </div>
                                    </div>
                                    <span style="display:none;"
                                        class="ajax_image_loader ajax-loader">
                                        <?php echo $LANG['loading']; ?></span>
                                </div>

                            </div>
                        </div>
                    </div>
                    </div>
            </div>
            <input type="hidden" class="form-control"
                value="<?php echo $_SESSION['user']['user_id']; ?>"
                name="loggedUserId"  />
            </form>
            </div>
    </div>
    </div> -->
    <div class="content-wrapper addmachine_page">

                    
    <div class="addmachine_head">
			<div class="addtabs_in_bx">	
            <label class="fl" for="machinename" >Name :*</label>
            <input type="text" id="machinename" value="<?php 
			if(isset($machinedetail['machine_name'])){
				echo $machinedetail['machine_name']; } else { echo ''; } ?>"/>
				</div>
				<div class="addtabs_in_bx">
				 <label class="fl" for="machinename" >Number :*</label>
            <input type="text" name="machine_no" id="machine_number" value="<?php 
			if(isset($machinedetail['uniqueid'])){ echo $machinedetail['uniqueid']; }
else{ echo $max_auto_club_id; } ?>" title="Select Club"/>
</div>
					<a href="index.php?p=managemachine"
                            title="<?php echo $LANG['backToSettings']; ?>">
                            <span class="icon icon-back backmachine"></span>
                        </a>
            <a class="btn black-btn fr"
                href="javascript:void(0)" onclick="saveMachine()"> Save </a>
				
                        
    </div>
    <div class="addmachine_tabs">
	<div class="addtabs_in_bx">
                 <label for="machinegroup" >Club :*</label><select id="club" name="type" title="Select Club">
                    <option value=''>- <?php echo $LANG['select']; ?> -</option>
        <?php
        foreach ($clublist as $club) {
            $sel =  '';
            if (($machinedetail['clubid'] == $club['club_id'])) {
                $sel =  'selected=selected';
            }
            echo '<option value="'.$club['club_id'].'" '.$sel.'>'
                .$club['club_name'].'</option>';
        }?>
                </select>
            </div>
        <div class="addtabs_in_bx">
        <label for="brandtype" >Brand :*</label>
            <select id="brandtype">
                <option value=''>- <?php echo $LANG['select']; ?> -</opt ion>
                <?php
    foreach ($brandlistrows as $brand) {
        if (isset($machinedetail['brand']) && $machinedetail['brand'] == $brand['brand_id']) {
            $sel =  'selected=selected';
        }
		else{
			$sel = '';
		}
        echo '<option value="'.$brand['brand_id'].'" '.$sel.'>'.
            $brand['brand_name'].'</option>';
		
		
    }
                ?>
            </select>
        </div>
        <div class="addtabs_in_bx">
            <label for="machinetype" >Type :*</label>
            <select id="machinetype">
                <option value=''>- <?php echo $LANG['select']; ?> -</option>
                <?php
    foreach ($typelistrows as $type) {
        if ($machinedetail['r_type'] == $type['machine_type_id']) {
            $sel =  'selected=selected';
        } else {
            $sel =  '';
        }
        echo '<option value="'.$type['machine_type_id'].'" '.$sel.'>'.
            $type['machine_type'].'</option>';
    }
                ?>
            </select>
        </div>
        <div class="addtabs_in_bx">
            <label for="machinegroup" >Group :*</label>
            <select id="machinegroup">
                <option value=''>- <?php echo $LANG['select']; ?> -</option>
                <?php
    foreach ($grouplistrows as $group) {
        if (isset($machinedetail['r_group']) && ($machinedetail['r_group'] == $group['group_id'])) {
            $sel =  'selected=selected';
        } else {	
            $sel =  '';
        }
        echo '<option value="'.$group['group_id'].'" '.$sel.'>'.
                $group['group_name'].'</option>';
    }?>
            </select>
        </div>
        <div class="addtabs_in_bx">
            <label>Subtype :*</label>
            <select id="subtype" title="Sub Type">
                <option value=''>- <?php echo $LANG['select']; ?> -</option>
                <?php
    foreach ($subtypelistrows as $subtype) {
        $sel =  '';
        if (($machinedetail['subtype'] == $subtype['subtype_id'])) {
            $sel =  'selected=selected';
        }
        echo '<option value="'.$subtype['subtype_id'].'" '.$sel.'>'
            .$subtype['subtype_name'].'</option>';
    }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" class="form-control loggedUserId"
        value="<?php echo $_SESSION['user']['user_id']; ?>" name="loggedUserId"  />
    <div class="addmachine_cnt">
        <div class="exercise_bx1">
            
            <div class="clear"></div>
            <div class="exercise_bx_get">
                <div class="exercise_bx_iner">
				
                    <img src="<?php if(isset($machinedetail) && $machinedetail['machineimage']){ echo '../images/uploads/movesmart/machines/'.$machinedetail['machineimage']; } else{ echo 'images/activices.png'; } ?>"  id="imagePreview" alt="machines" />
                    <h4 class="machinefile" style="cursor:hand; cursor:pointer;" id="get_file_button" >Get Image</h4>
                    <input style="cursor:hand; cursor:pointer;" type="file" class="upload machinefile click_image_btn"
                        onchange="displayImage(this,'n','','machineIcons')"
                        accept="image/*"
                        id="uploadBtn"
                        name="menuIcons" id="menuIcons" />
                </div>
                <input type="text" name="image_url" id = "image_url" value="<?php if(isset($machinedetail) && $machinedetail['image_url']){ echo $machinedetail['image_url']; } else { echo ''; }?>" style="display:none;" />
                <button class="urlbtn" id="get_image_url"  >Image url</button>
                <button class="clearbtn" >clear</button>
                <input type="hidden" value="<?php if(isset($machinedetail) && $machinedetail['machineimage']){ echo $machinedetail['machineimage']; }else { echo ""; } ?>" id="machineicon">
            </div>
            <div class="exercise_bx_get">
                <div class="exercise_bx_iner  im">
                    <!--img src="images/activices.png" alt="activices" id="video_image_preview"/-->
					<div class="vediourl">
					<?php if(isset($machinedetail) && $machinedetail['videourl']){ 
						$vurl = $machinedetail['videourl'];
							//$vediourl = str_replace('//vimeo.com','//player.vimeo.com/video',$vurl); 
							?>
							<a href="<?php echo $vurl; ?>" target="_blank"><?php echo $vurl; ?></a>
							<?php
								
								}
								 ?>
					</div>
				<!--p><a href="https://vimeo.com/57910221">GYM WORKOUT MOTIVATION VIDEO</a> from <a href="https://vimeo.com/angelisfilms">Angelis Films Ltd.</a> on <a href="https://vimeo.com">Vimeo</a>.</p-->
                    <h4 class="machinefile">Movie</h4>
                  <!--  <input type="file" class="upload machinefile"
                        onchange="displayImage(this,'n','','machineIcons')"
                        accept="image/*" id="uploadBtn"
                        name="menuIcons" id="menuIcons" /> -->
                       <!-- <form method="post" enctype="multipart/form-data" id="video_form_upload"> -->
                        <!--input type="file" class="upload machinefile"
                        id="upload_video"
                        accept="image/*" 
                        name="menuIcons"  /-->
                      <!--  </form> -->
                </div>
                <input type="text" name = "video_url" id = "video_url" style = "display:none; "/>
                <button class="urlbtn" id="get_video_url"   >Movie url </button>
                <button class="clearbtn" >clear</button>
    
            </div>
            <div class="exercise_bx_decrip">
                <div class="exercise_bx_iner im">
                <label for="machinedescription" style="display: none;" ></label>
                    <textarea id="machinedescription"> <?php if(!empty($machinedetail)){echo $machinedetail['description']; } ?> </textarea>
                    <h4>Description</h4>
                </div>
            </div>
            <div class="exercise_bx_check">
                <ul>
                    <li>
                        <!--<div class="in-cell cus-check get-check">-->
                            <span class="checkouter" id="active">
                <label style="display: none" for="activemachine" ></label>
                <input type="checkbox" value="<?php if((isset($machinedetail))){ echo $machinedetail['active']; } ?>"
                                 <?php if((isset($machinedetail)) &&  $machinedetail['active']==1) { ?> checked="checked" <?php } ?> class="statusmachine"
                                name="activemachine" id="activemachine"></span>
                            <label>Active</label>
                        <!--</div>-->
                    </li>
                    <li>
                        <span class="checkouter" id = "outofordermachine">
            <label style="display: none" for="outofordermachine" ></label>
            <input type="checkbox" value="<?php if((isset($machinedetail))){ echo $machinedetail['outoforder']; } ?>" <?php if((isset($machinedetail)) &&  $machinedetail['outoforder']==1) { ?> checked="checked" <?php } ?>
                            class="statusmachine" name="outofordermachine" id="outoforder"></span>
                        <label>Out of order</label>
                    </li>
                    <li>
                        <span class="checkouter" id="allowmixtrain1">
            <label for="allowmixtrain" style="display: none;"></label>
            <input type="checkbox" value="<?php if((isset($machinedetail))){ echo $machinedetail['allow_mix']; } ?>" <?php if((isset($machinedetail)) &&  $machinedetail['allow_mix']==1) { ?> checked="checked" <?php } ?>
                            class="" name="allowmixtrain"
                id="allowmixtrain"></span>
                        <label>Use in mix training allowed</label>
                    </li>
                    <li>
                        <span class="checkouter" id="allowcircuittrain1">
            <label for="allowcircuittrain" style="display: none; "></label>
            <input type="checkbox" value="<?php if((isset($machinedetail))){ echo $machinedetail['allow_circuit']; } ?>" <?php if((isset($machinedetail)) &&  $machinedetail['allow_circuit']==1) { ?> checked="checked" <?php } ?>
                   class="" id="allowcircuittrain" name="allowcircuittrain"></span>
                        <label>Use in circuit training allowed</label>
                    </li>
                    <li>
                    <span class="checkouter" id="allowfreetrain1">
             <label for="allowfreetrain" style="display: none; "></label>
             <input type="checkbox" value="<?php if((isset($machinedetail))){ echo $machinedetail['allow_free_training']; } ?>" <?php if((isset($machinedetail)) &&  $machinedetail['allow_free_training']==1) { ?> checked="checked" <?php } ?>
                        class="" name="allowfreetrain" id="allowfreetrain"></span>
                        <label>use in free training allowed</label>
                    </li>

                    <li>
                        <span class="checkouter" id="certified">
            <label for="certifymyfitplan" style="display: none; "></label>
            <input type="checkbox " value="<?php if((isset($machinedetail))){ echo $machinedetail['is_certified']; } ?>" <?php if((isset($machinedetail)) &&  $machinedetail['is_certified']==1) { ?> checked="checked" <?php } ?> 
                            class="check" id="certifymyfitplan" name="certifymyfitplan"></span>
                        <label>Movesmart certified: <br />(only allowed with machines
                            or equipment certified by Movesmart)</label>
                    </li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <?php 
	
/*	$str="SELECT * FROM t_block_weight WHERE strength_machine_id = ".$id;
	$weight_data = mysql_query($str);
	echo "<pre>";
	while($row = mysql_fetch_assoc($weight_data))
	{print_r($row);}
	echo "</pre>";
	*/
	
	
	
		$sql_sel = "SELECT * FROM t_bolck_weight WHERE strength_machine_id =".$machineid ;   
   $retval_sel = mysql_query($sql_sel); 

		$all_weights = array();
		$count=0;
   while($match_token_count = mysql_fetch_assoc($retval_sel))
   {
	   $all_weights[$count] =$match_token_count;
	   $count++;
   }
    //echo "<pre>";
	//print_r($all_weights[0]);
   //	echo "</pre>";

	?>
    <div class="conf_tab_box">
    <span><input type="text" placeholder="Conf.Factor: 0,98" name="confactor" value="<?php if(isset($machinedetail) && $machinedetail['f_confactor']) { echo $machinedetail['f_confactor']; } else { echo ''; } ?>" id="confactor"/> </span>

        <table id="block_pin_tab">
            <tr>
                <th>Block Weight</th>
                <th>Pinposition</th>
                <th></th>
            </tr>
            <tbody id = "weight_pin_point">
            <?php if(!empty($all_weights)) {
				foreach($all_weights as $value)
				{
				?>
           <tr  id="remove_<?php echo $value['block_weight_id']; ?>" >
                <td><?php echo $value['weight']; ?></td>
                <td><?php echo $value['position']; ?></td>
                <td><button class ="btn danger-btn " style="width:50px;" onclick="remove_weight(<?php echo $value['block_weight_id']; ?>)" >X</button></td>
            </tr>
			
            <?php } }
else{ ?>
	   <tr  id="remove_1" >
                <td><input type='text' name='weight[]' style='border: none;border-color: transparent;width:100px;'/></td>
                <td><input type ='text' name='pinposition[]'  style='border: none;border-color: transparent;width:100px;'/></td>
                <td><button class ="btn danger-btn " style="width:50px;" onclick="remove_weight(1)" >X</button></td>
            </tr>
	
<?php }			?>

            </tbody>
        </table>
        <div style="margin-top:20px">
        <button  class="btn black-btn fr" id="add_new_row">Add</button>
        <!--button  class="btn black-btn fr" id="update_data">Update</button-->
        </div>
         
    </div>

    <div class="clear"></div>
</div> 
<script>



$(window).load(function()
{
	/**active**/
			$("#active").click(function(){
				
				 var ele = $("#activemachine").find(':checkbox');
		//	 if ($('#activemachine').is(':checked')) {
				 $("#active").addClass("checkbtn");
				 ele.prop('checked', true);
				 $("#activemachine").val(1);
				
			// }
			});   
	/******for certified*****/
			$("#certified").click(function(){
				
				 var ele = $("#certifymyfitplan").find(':checkbox');
			// if ($('#certifymyfitplan').is(':checked')) {
				 $("#certified").addClass("checkbtn");
				 ele.prop('checked', true);
				 $("#certifymyfitplan").val(1);
				
			// }
			
			 
			});
			
	/**outoforder**/
			$("#outofordermachine").click(function(){
				
				 var ele = $("#outoforder").find(':checkbox');
			// if ($('#outoforder').is(':checked')) {
				
				 $("#outofordermachine").addClass("checkbtn");
				 ele.prop('checked', true);
				 $("#outoforder").val(1);
				
			// }
			 
			});
	/***alowwmix**/
			$("#allowmixtrain1").click(function(){
				
				 var ele = $("#allowmixtrain").find(':checkbox');
			// if ($('#allowmixtrain').is(':checked')) {
				
				 $("#allowmixtrain1").addClass("checkbtn");
				 ele.prop('checked', true);
				 $("#allowmixtrain").val(1);
				
		//	 }
			
			});
			
	/**allow free**/
			$("#allowfreetrain1").click(function(){
				
				 var ele = $("#allowfreetrain").find(':checkbox');
			// if ($('#allowfreetrain').is(':checked')) {
				
				 $("#allowfreetrain1").addClass("checkbtn");
				 ele.prop('checked', true);
				 $("#allowfreetrain").val(1);
				
		//	 }
			 
			});
	/**allow circuit**/
				$("#allowcircuittrain1").click(function(){
					
					 var ele = $("#allowcircuittrain").find(':checkbox');
				// if ($('#allowcircuittrain').is(':checked')) {
					
					 $("#allowcircuittrain1").addClass("checkbtn");
					 ele.prop('checked', true);
					 $("#allowcircuittrain").val(1);
					
				 //}
				
				});
	
	var active = $('#activemachine').val();
    var is_certified = $('#certifymyfitplan').val();
    var outoforder = $('#outoforder').val();
    var allow_mix = $('#allowmixtrain').val();
    var allow_circuit = $('#allowcircuittrain').val();
     var allow_free_training = $('#allowfreetrain').val();
if(active==1){
		
		
	$("#active").addClass("checkbtn");
	//$('#activemachine').checked(true);

	}
	
if(is_certified==1){
        
        
    $("#certified").addClass("checkbtn");
    
    }
if(outoforder==1){
        
        
    $("#outofordermachine").addClass("checkbtn");
    
    }
if(allow_mix==1){
        
        
    $("#allowmixtrain1").addClass("checkbtn");
    
    } 
    
if(allow_circuit==1){
        
        
    $("#allowcircuittrain1").addClass("checkbtn");
    
    }
if(allow_free_training==1){
        
        
    $("#allowfreetrain1").addClass("checkbtn");
    
    }
    
	
$("#get_file_button").click(function (){
	$(".click_image_btn").trigger("click");	});

$("#get_image_url").click(function()
{
	$("#image_url").toggle();
});


	$("#get_video_url").click(function()
	{
		$("#video_url").toggle();
	});
		
	
		$("#add_new_row").click(function(){
		
			var row = "<tr id='remove_"+localStorage.count_row+"'>";
			row = row +"<td><input type='text' name='weight[]' style='border: none;border-color: transparent;width:100px;'/></td>";
			row = row +"<td><input type ='text' name='pinposition[]'  style='border: none;border-color: transparent;width:100px;'/></td>";
			row = row +"<td><button class ='btn danget-btn ' style='width:50px' onclick='remove_weight("+localStorage.count_row+")' >X</button></td></tr>";
			localStorage.count_row++;
			$("#weight_pin_point").append(row);
			
			});    
			$("#update_data").click(function(){
			
				  var weights= new Array();
				  var pinposition= new Array();
					$('input[name^="weight"]').each(function() 
					{
						
					weights.push($(this).val());
					});
					$('input[name^="pinposition"]').each(function() 
					{
						
					pinposition.push($(this).val());
					});
					/* console.log(weights);
					console.log(pinposition); */
					// var url = 'action=saveblockpin&weights='+weights+'&pinposition='+pinposition;
					$.ajax({
						
    type: "POST",
    url: "../ajax/index.php?p=settings",
    data: {action:'saveblockpin', weights:weights, pinposition:pinposition},
    dataType: "POST",
    success: function(data) {
		alert(data);
        //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
        // do what ever you want with the server response
    },
    error: function() {
        alert('error handing here');
    }
});
				});
			
			
	
	});
	function remove_weight(id)
	{
		
		$("#remove_"+id).remove();
	}
    localStorage.preImageUrl = "";
	localStorage.count_row = 25874
    </script>

