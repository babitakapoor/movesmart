<?php

/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle settings related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class settings extends common
{

    /**
     * Returns an json obj of  get all user type list
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function __construct()
	{
		
		if(isset($_REQUEST['action']) && trim($_REQUEST['action']) == 'deleteUser')
		{
			$this->deleteUser($_REQUEST);
		}
		//echo "<pre>";print_r($_REQUEST);die;
	}
    public function getUserType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getUserType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['userTypeDetails'];
    }

    /**
     * Returns an json obj of  all menus and user type permission for pages
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getMenuPermissions()
    {
        
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getMenuPermissions';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));

        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['getMenuPermissions'];
    }
     /**
    * Returns an json obj of get all menus for pages
    * @param string $params service parameter
    *
    * @return array object object
    */  
    public function getMenuPages($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getMenuPages';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if ($result['total_records'] > 0 && !isset($result['getMenuPages'][0])) {
            $result['getMenuPages'] = array($result['getMenuPages']);
        }

        return $result['getMenuPages'];
    }

     /**
    * Returns an json obj of get all brand for pages
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getQuestionaries($params = '')
    {

        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuestionaries';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        /*if($result['total_records'] > 0 && !isset($result['getQuestionaries'][0])){
            $result['getQuestionaries'] = array($result['getQuestionaries']);
        }
        else{
            $result['getQuestionaries']=array();
        }*/
        return $result;
    }

    /**
     * Returns an json obj of get question by id
     * @param $param
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getQuestionnaireByID($param)
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getQuestionnaireByID';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get all menus set as parent
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getParentMenus()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getParentMenus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if ($result['total_records'] > 0 && !isset($result['getParentMenus'][0])) {
            $result['getParentMenus'] = array($result['getParentMenus']);
        }

        return $result['getParentMenus'];
    }
      /**
    * Returns an json obj of get all menus set as parent
    * @param string ''
    *
    * @return array object object
    */
    public function getMenuPageDetail($param)
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getMenuPageDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
    * Returns an json obj of get all menus detail
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getQuestionnaireDetail($param)
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getQuestionnaireDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

       /**
    * Returns an json obj of get all menus for pages
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertUserTypePermissions($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertUserTypePermissions';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
    * Returns an json obj of insert user type permissions
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getUserTypePagePermission($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getUserTypePagePermission';

            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if ($result['total_records'] > 0 && !isset($result['UserTypePermission'][0])) {
            $result['UserTypePermission'] = array($result['UserTypePermission']);
        }

        if (isset($result['UserTypePermission'])) {
            return $result['UserTypePermission'];
        }
        return null;
    }
     /**
    * Returns an json obj of add or update page menus
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function InsertUpdateMenus($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'InsertUpdateMenus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of add or update page Questionnaire
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function InsertUpdateQuestion($params)
    {
		
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'InsertUpdateQuestion';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of add or update education
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function InsertUpdateEducation($params)
    {

        try {
            $params['mod'] = 'settings';
            $params['method'] = 'InsertUpdateEducation';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of get all education list
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getEducation($params)
    {

        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getEducation';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     public function getReporttextByid($id)
    {

        $str="select * from t_level_description where id=".$id;
        $result = mysql_query($str);
        while($row=mysql_fetch_array($result))
        {
         return $row;
         }
    }
      /**
    * Returns an json obj of insert Questuin activity
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertQuesActivity($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertQuesActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of get form exist
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getFormExist($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getFormExist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of insert phase range
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertPhaseRange($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertPhaseRange';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
        /**
    * Returns an json obj of insert phase weightage
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertPhaseWeight($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertPhaseWeight';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of delete menu page
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteMenuPage($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteMenuPage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of delete Question
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteQuestionnaire($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteQuestionnaire';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
    * Returns an json obj of add/update strength program
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertUpdateStrengthProgram($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertUpdateStrengthProgram';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of get strength program list
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function strengthProgramList($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'strengthProgramList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of add/update  Strength Programs training
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertUpdateStrengthProgramTraining($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertUpdateStrengthProgramTraining';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of get Strength Programs details
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStrengthProgramDetailById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getStrengthProgramDetailById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

       /**
    * Returns an json obj of get Strength Programs training list
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStrengthTrainingList($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getStrengthTrainingList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if (isset($result['rows']) && $result['total_records'] == 1) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of delete Strength Programs Training row by ID
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteStrengthProgramTrainingById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteStrengthProgramTrainingById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of delete Strength Program 
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteStrengthProgramById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteStrengthProgramById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get security questions
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getSecurityQuestions()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getSecurityQuestions';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
    * Returns an json obj of fetch manage group
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function fetchManagegroup($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'fetchManagegroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
    * Returns an json obj of get menu List
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function fetchManagemenu($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'fetchManagemenu';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

        /**
        * Returns an json obj of get brand List
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function fetchManagebrand($params)
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'fetchManagebrand';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
        /**
        * Returns an json obj of get question list
        * @param string $params service parameter
        *
        * @return array object object
        */
       /* public function fetchManagequestionnaire($params)
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'fetchManageqquestionnaire';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
       */
        /**
        * Returns an json obj of get question list by group
        * @param string ''
        *
        * @return array object object
        */
    public function getQuesGroup()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuesGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get question list by phase
    * @param string ''
    *
    * @return array object object
    */
    public function getQuesPhase()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuesPhase';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get question phase range
    * @param string $lang default language id
    *
    * @return array object object
    */
    public function getQuesPhaseRange($lang)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuesPhaseRange';
            $params['defaultlang'] = $lang;
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get education by group id
    * @param string $groupid group id
    *
    * @return array object object
    */
    public function getEducationByGroupId($groupid)
    {

        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getEducationByGroupId';
            $params['groupid'] = $groupid;
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get question activity
    * @param string ''
    *
    * @return array object object
    */
    public function getQuesActivity()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuesActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
        /**
        * Returns an json obj of get language type
        * @param string ''
        *
        * @return array object object
        */
        public function getLanguageType()
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'getLanguageType';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
     /**
    * Returns an json obj of edit language type
    * @param string ''
    *
    * @return array object object
    */
    /*public function editLanguageType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getLanguageType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/
     /**
    * Returns an json obj of edit translation type
    * @param string ''
    *
    * @return array object object
    */
    /*public function editTrtanslationType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'gettranslationType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
     /**
    * Returns an json obj of get translation key type
    * @param string ''
    *
    * @return array object object
    */
    public function getTranslationkeyType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getTranslationkeyType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get translation  list
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTranslations($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getTranslations';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of update translation key
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function updateTranslationsKey($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'updateTranslationsKey';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /**
    * Returns an json obj of update  imported translations
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function UpdateImportedTranslations($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'UpdateImportedTranslations';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of update   translations
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function updateTranslations($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'updateTranslations';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/

    /**
    * Returns an json obj of set excel attributes
    * @param string $rowCount excel row count
    * @param string $colCount excel column count
    * @param string $title excel title
    *
    * @return array object object
    */
    public function setExcelAttributes($rowCount, $colCount, $title)
    {
        global $excelExporter;
        $activeSheet = $excelExporter->getActiveSheet();
        $activeSheet->setTitle($title);
        $CharHead   ='A';
        for ($row = 1;$row <= $rowCount;++$row) {
            for ($col = 1, $CharHead = 'A';$col <= $colCount;++$col) {
                $activeSheet->getColumnDimension($CharHead)->setAutoSize();
                $activeSheet->getStyle($CharHead.'1')->getFont()->setBold(true);
                $activeSheet->getStyle($CharHead.$row)->getAlignment()->applyFromArray(
                        array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    )->setWrapText(true);
                ++$CharHead;
            }
        }
        $activeSheet->setAutoFilter('A1:'.$CharHead.$rowCount);
    }
    /**
     * Returns an json obj of get ttranslation language type
     * @param string ''
     *
     * @return array object object
     */
    public function getTranslationLanguageType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getTranslationLanguageType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of delete language
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deletelanguage($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deletelanguage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of delete translation
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deletetranslation($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deletetranslation';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get language details
    * @param string ''
    *
    * @return array object object
    */
    public function getLanguageDetails()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getLanguageDetails';
           // printLog(WEBSERVICE_PATH);
            $path = $_SERVER['DOCUMENT_ROOT'].'/movesmart_staging/requestLogs/request.log';
            error_log(print_r(WEBSERVICE_PATH.QN.http_build_query($params),true),3,$path);
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get device settings
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getdevicesettings($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getdevicesettings';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get lanaguage list
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getLanguageList()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getLanguageType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
         /**
        * Returns an json obj of get brand list
        * @param string ''
        *
        * @return array object object
        */
        public function getBrand()
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'getBrand';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
     /**
    * Returns an json obj of get brand detail list
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getBrandDetail($param)
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getBrandDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of add/update brand
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertBrand($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertBrand';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of delete brand
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteBrand($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deletebrand';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	 public function deleteProgram($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteProgram';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get group
    * @param string ''
    *
    * @return array object object
    */
    public function getGroup()
    {
		try {
            $params['mod'] = 'settings';
            $params['method'] = 'getGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get type list
    * @param string ''
    *
    * @return array object object
    */
    public function gettypelist()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'gettypelist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get sub type list
    * @param string ''
    *
    * @return array object object
    */
    public function getsubtypelist()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getsubtypelist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get group detail
    * @param string $param service parameter
    *
    * @return array object object
    */

    public function getGroupDetail($param)
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getGroupDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of add/update group 
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertGroup($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of delete group 
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteGroup($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of get active language type list
    * @param string ''
    *
    * @return array object object
    */
    public function getLanguageActiveType()
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getLanguageActiveType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
        /**
        * Returns an json obj of get group topics
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getGroupTopics($params = '')
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'getGroupTopics';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            if ($result['total_records'] > 0 && !isset($result['getGroupTopics'][0])) {
                $result['getGroupTopics'] = array($result['getGroupTopics']);
            }

            return $result['getGroupTopics'];
        }
  /**
    * Returns an json obj of get question by id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getQuestionById($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuestionById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
        * Returns an json obj of get question option by id
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getQuestionOptionById($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuestionOptionById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
        * Returns an json obj of get phase weightage
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getPhaseWeightage($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getPhaseWeightage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if ($result['total_records'] > 0 && !isset($result['getPhaseWeightage'][0])) {
            $result['getPhaseWeightage'] = array($result['getPhaseWeightage']);
        } else {
            $result['getPhaseWeightage'] = array();
        }

        return $result['getPhaseWeightage'];
    }
     /**
        * Returns an json obj of get phase range by id
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getPhaseRangeById($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getPhaseRangeById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if ($result['total_records'] > 0 && !isset($result['getPhaseRangeById'][0])) {
            $result['getPhaseRangeById'] = array($result['getPhaseRangeById']);
        }

        return $result['getPhaseRangeById'];
    }
      /**
        * Returns an json obj of delete phase range by id
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function deletePhaseRangeById($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deletePhaseRangeById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
    }
      /**
        * Returns an json obj of get question phase group type
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getQuesPhaseGroupType($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuesPhaseGroupType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
            /*	if($result['total_records'] > 0 && !isset($result['getQuesPhaseGroupType'][0])){
            $result['getQuesPhaseGroupType'] = array($result['getQuesPhaseGroupType']);
            }
            */
            return $result;
    }
     /**
        * Returns an json obj of get question phase group 
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getQuesPhaseGroup($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuesPhaseGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
            /*	if($result['total_records'] > 0 && !isset($result['getQuesPhaseGroup'][0])){
            $result['getQuesPhaseGroup'] = array($result['getQuesPhaseGroup']);
            }
            */
            return $result;
    }
        /**
        * Returns an json obj of get question phase group  activity points
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getQuesPhaseGroupActivityPoints($params = '')
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'getQuesPhaseGroupActivityPoints';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            /*	if($result['total_records'] > 0 && !isset($result['getQuesPhaseGroupActivityPoints'][0])){
            $result['getQuesPhaseGroupActivityPoints'] = array($result['getQuesPhaseGroupActivityPoints']);
            }
            */
            return $result;
        }
        /**
        * Returns an json obj of get form id for phase and group topic
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getFormIdForPhaseAndGroupAndTopic($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getFormIdForPhaseAndGroupAndTopic';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
       /**
        * Returns an json obj of delete question by id
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function deleteQuestionById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteQuestionById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get page name
     * @return array object object
     * @internal param string $param service parameter
     *
     */
    public function getPageName()
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getPageName';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
        * Returns an json obj of add/update page content
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function InsertUpdatePageContent($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'InsertUpdatePageContent';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
   /**
    * Returns an json obj ofdelete page content by id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deletePageContentById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deletePageContentById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get page content details
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getPageContentDetails()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getPageContentDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get page content by id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPageContentById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getPageContentById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get language page content details
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getLanguagePageContentDetails()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getLanguagePageContentDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get disease
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getDisease()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getDisease';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of update disease
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateDisease($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'updateDisease';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get phase type
     * @return array object object
     * @internal param string $params service parameter
     *
     */
        public function getPhaseType()
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'getPhaseType';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }

    /**
     * Returns an json obj of getgroup list type
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getGroupListType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getGroupListType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get question by type
     * @return array object object
     * @internal param string $params service parameter
     *
     */

    public function getQuestionsType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuestionsType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of add/update question hints
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function InsertUpdateQuestionHints($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'InsertUpdateQuestionHints';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get question hints
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getQuestionHints($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getQuestionHints';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get question hints by id
     * @param $param
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getQuestionHintsById($param)
    {
        try {
            $param['mod'] = 'settings';
            $param['method'] = 'getQuestionHintsById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of delete question hints by id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deleteQuestionHintById($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteQuestionHintById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of get user fitlevel
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getUserFitlevel($params = '')
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getUserFitlevel';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get reset user
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getResetUsers()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getResetUsers';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    
    public function deleteUser($params)
    {
		try {
            $params['mod'] = 'settings';
            $params['method'] = 'deleteUser';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            $_SESSION['delet_message'] = 1;
			$url = 'index.php?p='.$params['p'];
			header('Location:'.$url);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
       /**
        * Returns an json obj of save calender note
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function SaveCalenderNote($params)
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'SaveCalenderNote';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
          /**
        * Returns an json obj of get calender details
        * @param string $params service parameter
        *
        * @return array object object
        */
    public function getCalenderDetails($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getCalenderDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get machine list
     * @return array object object
     * @internal param string $params service parameter
     *
     */
        public function machineList()
        {
            try {
                $params['mod'] = 'members';
                $params['method'] = 'listMachinesData';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
     /**
    * Returns an json obj of get  new machine details by id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getNewMachinedetailsbyeditid($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'editNewMachinedetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get  goal
     * @return array object object
     * @internal param string $params service parameter
     *
     */
        public function getGoal()
        {
            try {
                $params['mod'] = 'settings';
                $params['method'] = 'getGoal';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
    /**
    * Returns an json obj of update  goal
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateGoal($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'updateGoal';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	 public function getMachine_groupData($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getMachine_groupData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			//echo '<pre>'; print_r($result);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	/**********for getting machine group*****/
	 public function getMachineGroup()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getMachineGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
	/****************for saving strength program in the backoffice******/
	 public function saveStrengthProgram($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'saveStrengthProgram';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
	/************for program list*****/
	 public function programlist($params)
    {  
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'programlist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
	 public function getprogramDetailbyId($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getprogramDetailbyId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
	 public function gettimeslotbyId($params)
    {
		
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'gettimeslotbyId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
	 public function updatetimeslot($params)
    {
		
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'updatetimeslot';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
	public function saveblockpin($params)
    {
		
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'saveblockpin';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
    
        return $result;
    }
}
