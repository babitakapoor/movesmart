<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class userType extends common
{
    /**
    * Returns an json obj of users List.
    *
    * @param string $dataString service parameter
    *
    * @return json obj
    */    
    public function getUserType($dataString)
    {
        $this->result = parent::webServiceXMLToString(
            LOCAL_WEBSERVICE_PATH.'getUserType.php'.QN.$dataString
        );

        return json_decode($this->result, true);
    }
}
