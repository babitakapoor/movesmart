<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle message related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class message extends common
{
    /**
    * Returns an json obj of get all user messages
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getMessagesByUserId($params)
    {
        try {
            $params['mod'] = 'message';
            $params['method'] = 'getMessagesByUserId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['message'][0])) {
            $result['message'] = array($result['message']);
        }

        return $result;
    }
    */
     /**
    * Returns an json obj of get message replies by message id
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getMessageDetailById($params)
    {
        try {
            $params['mod'] = 'message';
            $params['method'] = 'getMessageDetailById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['message'][0])) {
            $result['message'] = array($result['message']);
        }

        return $result;
    }
    */
    /**
    * Returns an json obj of insert new message or reply message
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertNewMessage($params)
    {
        try {
            $params['mod'] = 'message';
            $params['method'] = 'insertNewMessage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of mark message status as deleted
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function deleteMessageById($params)
    {
        try {
            $params['mod'] = 'message';
            $params['method'] = 'deleteMessageById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/

     /**
    * Returns an json obj of  mark message as read
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function markMessageReadById($params)
    {
        try {
            $params['mod'] = 'message';
            $params['method'] = 'markMessageReadById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/
    /**
     * Returns an json obj of get message details
     * @param string $params service parameter
     *
     * @return array object object
     */
    public function getMessageDetails($params)
    {
        try {
            $params['mod'] = 'message';
            $params['method'] = 'getMessageDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
}; // End Class.
