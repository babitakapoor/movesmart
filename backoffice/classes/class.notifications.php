<?php

/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle message related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class notifications extends common
{
    /**
    * Returns an json obj of get all user messages
    * @param string $params service parameter
    *
    * @return array object object
    */
    
    /**
    * Returns an json obj of insert new message or reply message
    * @param string $params service parameter
    *
    * @return array object object
    */
	
	public function __construct()
	{
		//echo "<pre>";print_r($_REQUEST);die;
		if(isset($_REQUEST['action_type']) && ($_REQUEST['action_type'] == 'save_message' || $_REQUEST['action_type'] == 'edit_message'))
		{
			if($_REQUEST['action_type'] == 'save_message')
			{
				$this->save_message($_REQUEST);
			}
			if($_REQUEST['action_type'] == 'edit_message')
			{
				$this->edit_message($_REQUEST);
			}
			
		}
		if(isset($_REQUEST['action_type']) && ($_REQUEST['action_type'] == 'save_push_message' || $_REQUEST['action_type'] == 'edit_push_message'))
		{
			if($_REQUEST['action_type'] == 'save_push_message')
			{
				$this->save_push_message($_REQUEST);
			}
			if($_REQUEST['action_type'] == 'edit_push_message')
			{
				$this->edit_push_message($_REQUEST);
			}
			
		}
		if(isset($_REQUEST['action_type']) && ($_REQUEST['action_type'] == 'save_notification_content' || $_REQUEST['action_type'] == 'edit_notification_content'))
		{
			if($_REQUEST['action_type'] == 'save_notification_content')
			{
				$this->save_notification_content($_REQUEST);
			}
			if($_REQUEST['action_type'] == 'edit_notification_content')
			{
				$this->edit_notification_content($_REQUEST);
			}
			
		}
		if(isset($_REQUEST['action_type']) && ($_REQUEST['action_type'] == 'save_behaviour_content' || $_REQUEST['action_type'] == 'edit_behaviour_content'))
		{
			if($_REQUEST['action_type'] == 'save_behaviour_content')
			{
				$this->save_behaviour_content($_REQUEST);
			}
			if($_REQUEST['action_type'] == 'edit_behaviour_content')
			{
				$this->edit_behaviour_content($_REQUEST);
			}
			
		}
		if(isset($_REQUEST['p']) && ($_REQUEST['p'] == 'deletenotification'))
		{
			$this->delete_message($_REQUEST);
		}
		
		if(isset($_REQUEST['p']) && ($_REQUEST['p'] == 'deletenotificationcontent'))
		{
			$this->delete_content($_REQUEST);
		}
		
		if(isset($_REQUEST['p']) && ($_REQUEST['p'] == 'delete_pushnotification'))
		{
			$this->delete_pushnotification($_REQUEST);
		}
		
	}
	
    public function getNotifications($id = '')
    {
        try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getNotifications';
			$params['id'] = $id;
			//echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	public function getNotificationsSettings($slug = '')
    {
        try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getNotificationsSettings';
			$params['slug'] = $slug;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    
	public function getScoreCategories()
	{
		try {
			$params['mod'] = 'notifications';
            $params['method'] = 'getScoreCategories';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function save_message($params)
	{
		try {
			//echo "<pre>";print_r($params);die;
			$params['mod'] = 'notifications';
            $params['method'] = 'save_message';
			$type =  $params['notification_pages_id'];
			//$result = parent::curlPostDataToPage(WEBSERVICE_PATH.QN.http_build_query($params));
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			//echo "<pre>";print_r($result);die;
			if($result['status_code'] == '200')
			{
				switch($type)
				{
					case(1):
						$tab = 'movesmart';
						break;
					case(3):
						$tab = 'mind';
						break;
					case(2):	
						$tab = 'eat';
						break;
				}
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				$url = 'index.php?p=notifications&notification_tab='.$tab;
			}
			else
			{	
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				$type_id =  $params['notification_types_id'];
				$tab =  $params['notification_tab'];
				$url = 'index.php?p=addnotification&notification_tab='.$tab.'&type='.$type.'&type_id='.$type_id;
			}
			header('Location:'.$url);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getContent($id = '')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getContent';
			$params['id'] = $id;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getSpecialContent($id = '')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getSpecialContent';
			$params['id'] = $id;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getNotificationsData($id = '')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getNotificationsData';
			$params['id'] = $id;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function edit_message($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'edit_message';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$type =  $params['notification_pages_id'];
			if($result['status_code'] == '200')
			{
				switch($type)
				{
					case(1):
						$tab = 'movesmart';
						break;
					case(3):
						$tab = 'mind';
						break;
					case(2):	
						$tab = 'eat';
						break;
				}
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				$url = 'index.php?p=notifications&notification_tab='.$tab;
			}
			else
			{	
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				$type_id =  $params['notification_types_id'];
				$tab =  $params['notification_tab'];
				$id =  $params['id'];
				$url = 'index.php?p=addnotification&notification_tab='.$tab.'&type='.$type.'&type_id='.$type_id.'&id='.$id;
			}
			header('Location:'.$url);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function delete_message($params)
	{
		try {
			$params['mod'] = 'notifications';
            $params['method'] = 'delete_message';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
			$tab =  $params['notification_tab'];
			$url = 'index.php?p=notifications&notification_tab='.$tab;
			header('Location:'.$url);
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function delete_content($params)
	{
		try {
			$params['mod'] = 'notifications';
            $params['method'] = 'delete_content';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
			$tab =  $params['notification_tab'];
			$url = 'index.php?p=notifications&notification_tab='.$tab;
			header('Location:'.$url);
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getSelectBox($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getSelectBox';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
		return $result;
	}
	
	
	
	public function getAppNotification($params = array())
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getAppNotification';
			echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getTopicName($type ='')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getTopicName';
			$params['type'] = $type;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getInappNotification($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getInappNotification';
			//echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			printLog(WEBSERVICE_PATH.QN.http_build_query($params));
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			//echo "<pre>";print_r($result);die;
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	
	
	public function getProfileData()
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getProfileData';
			//echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function save_notification_content($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'save_notification_content';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$type =  $params['notification_pages_id'];
			//echo "<pre>";print_r($result);die;
			if($result['status_code'] == 200)
			{
				//header('Location: index.php?p=notifications');
				switch($type)
				{
					case(1):
						$tab = 'movesmart';
						break;
					case(3):
						$tab = 'mind';
						break;
					case(2):	
						$tab = 'eat';
						break;
				}
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				$url = 'index.php?p=notifications&notification_tab='.$tab;
			}
			else
			{
				$type_id =  $params['notification_types_id'];
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				$tab =  $params['notification_tab'];
				$url = 'index.php?p=addnotification&notification_tab='.$tab.'&type='.$type.'&type_id='.$type_id;
			}
			header('Location: '.$url);
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	
	public function edit_notification_content($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'edit_notification_content';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$type =  $params['notification_pages_id'];
			//echo "<pre>";print_r($result);die;
			if($result['status_code'] == 200)
			{
				//header('Location: index.php?p=notifications');
				switch($type)
				{
					case(1):
						$tab = 'movesmart';
						break;
					case(3):
						$tab = 'mind';
						break;
					case(2):	
						$tab = 'eat';
						break;
				}
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				$url = 'index.php?p=notifications&notification_tab='.$tab;
			}
			else
			{
				$type_id =  $params['notification_types_id'];
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				$tab =  $params['notification_tab'];
				$id  = $params['id'];
				$url = 'index.php?p=addnotification&notification_tab='.$tab.'&type='.$type.'&type_id='.$type_id.'&id='.$id;
			}
			header('Location: '.$url);
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	public function getContentData($id = '')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getContentData';
			$params['id'] = $id;
			//echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function save_behaviour_content($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'save_behaviour_content';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$type =  $params['notification_pages_id'];
			//echo "<pre>";print_r($result);die;
			if($result['status_code'] == 200)
			{
				//header('Location: index.php?p=notifications');
				switch($type)
				{
					case(1):
						$tab = 'movesmart';
						break;
					case(3):
						$tab = 'mind';
						break;
					case(2):	
						$tab = 'eat';
						break;
				}
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				$url = 'index.php?p=notifications&notification_tab='.$tab;
			}
			else
			{
				$type_id =  $params['notification_types_id'];
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				$tab =  $params['notification_tab'];
				$url = 'index.php?p=addnotification&notification_tab='.$tab.'&type='.$type.'&type_id='.$type_id;
			}
			header('Location: '.$url);
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getBehaviourData($id = '')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getBehaviourData';
			$params['id'] = $id;
			//echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function edit_behaviour_content($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'edit_behaviour_content';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$type =  $params['notification_pages_id'];
			//echo "<pre>";print_r($result);die;
			if($result['status_code'] == 200)
			{
				//header('Location: index.php?p=notifications');
				switch($type)
				{
					case(1):
						$tab = 'movesmart';
						break;
					case(3):
						$tab = 'mind';
						break;
					case(2):	
						$tab = 'eat';
						break;
				}
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				$url = 'index.php?p=notifications&notification_tab='.$tab;
			}
			else
			{
				$type_id =  $params['notification_types_id'];
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				$tab =  $params['notification_tab'];
				$id  = $params['id'];
				$url = 'index.php?p=addnotification&notification_tab='.$tab.'&type='.$type.'&type_id='.$type_id.'&id='.$id;
			}
			header('Location: '.$url);
			
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	
	////////////////////////Push Notification Code /////////////////////////////////////
	
	public function getPuhSelectBox($type)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getPushSelectBox';
			 $params['type'] = $type;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getPushType()
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getPushType';
			//echo WEBSERVICE_PATH.QN.http_build_query($params);die;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getPushNotificationsSettings($slug = '')
    {
        try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getPushNotificationsSettings';
			$params['slug'] = $slug;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	public function getPushNotifications()
    {
		
        try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getPushNotifications';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	public function getPushSelectBox($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getPushSelectBox';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function save_push_message($params)
	{
		try {
			$params['mod'] = 'notifications';
            $params['method'] = 'save_push_message';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			//header('Location: index.php?p='.$params['p'].'&mess='.$result['status_message']);
			if($result['status_code'] == '200')
			{
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				header('Location: index.php?p=push_notifications');
			}
			else
			{	
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				header('Location: index.php?p='.$params['p'].'&mess='.$result['status_message']);
			}
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function edit_push_message($params)
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'edit_push_message';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			
			if($result['status_code'] == '200')
			{
				$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
				header('Location: index.php?p=push_notifications');
			}
			else
			{	
				$_SESSION['flMsg']['flashMessageError'] = $result['status_message'];
				header('Location: index.php?p='.$params['p'].'&id='.$params['id'].'&mess='.$result['status_message']);
			}
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function getPushNotificationsData($id ='')
	{
		try {
            $params['mod'] = 'notifications';
            $params['method'] = 'getPushNotificationsData';
			$params['id'] = $id;
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function delete_pushnotification($params)
	{
		try {
			$params['mod'] = 'notifications';
            $params['method'] = 'delete_pushnotification';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
			$tab =  $params['notification_tab'];
			header('Location: index.php?p=push_notifications');
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
	public function sendPushNotification($params)
	{
		try {
			$params['mod'] = 'notifications';
            $params['method'] = 'sendPushNotification';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
			$_SESSION['flMsg']['flashMessageSuccess'] = $result['status_message'];
			$tab =  $params['notification_tab'];
			header('Location: index.php?p=push_notifications');
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
	}
	
}; // End Class.
