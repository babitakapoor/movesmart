<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Test
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle test related functions.
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class test extends common
{
     /**
    * Returns an json obj of  get Evoulation List
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getEvolutionList($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getEvolutionList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['evolutionList'];
    }

     /**
    * Returns an json obj of  get test values List
    * @param string $dataString service parameter
    *
    * @return array object object
    */
    public function getTestValues($dataString)
    {
        try {
            $dataString['mod'] = 'test';
            $dataString['method'] = 'getTestValues';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($dataString));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['testValuesList'];
    }

    /**
    * Returns an json obj of  insert temporary adjust sportmed data
    * @param string $dataString service parameter
    *
    * @return array object object
    */
    public function insertTmpAdjustSportmedData($dataString)
    {
        try {
            $dataString['mod'] = 'test';
            $dataString['method'] = 'insertTmpAdjustSportmedData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($dataString));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of  get temporary adjust sportmed data
    * @param string $dataString service parameter
    *
    * @return array object object
    */
    public function getTmpAdjustSportmedData($dataString)
    {
        try {
            $dataString['mod'] = 'test';
            $dataString['method'] = 'getTmpAdjustSportmedData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($dataString));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get test sport specific data
    * @param string $dataString service parameter
    *
    * @return array object object
    */
    public function getSportSpecificData($dataString)
    {
        try {
            $dataString['mod'] = 'test';
            $dataString['method'] = 'getSportSpecificData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($dataString));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
     /**
    * Returns an json obj of get test items by test id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestItems($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTestItems';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if (isset($result['rows']) && !isset($result['rows'][0]['test_item_id'])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get the test result details based on the test_result_id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestResultInfo($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTestResultInfo';

            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return isset($result['movesmart']['testResultInfoArr']) ? $result['movesmart']['testResultInfoArr'] : array();
    }

     /**
    * Returns an json obj of get the Fitness Level of Evolution..
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getEvolutionFitnessLevel($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getEvolutionFitnessLevel';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            if ($result['movesmart']['status'] == 'success'
                    && !isset($result['movesmart']['evolutionFitnessLevel'][0])
            ) {
                $result['movesmart']['evolutionFitnessLevel'] = array($result['movesmart']['evolutionFitnessLevel']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

       /**
    * Returns an json obj of getting the Bio Metrics of Evolution.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getEvolutionBioMetrics($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getEvolutionBioMetrics';

            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of cancel member current test.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function cancelMemberTest($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'cancelMemberTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

   
       /**
    * Returns an json obj of Get Medical Info Details.
    * @param string $dataString service parameter
    *
    * @return array object object
    */
    /*public function getMedicalTestInfo($dataString)
    {
        $this->result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$dataString);

        return json_decode($this->result, true);
    }*/

    
     /**
    * Returns an json obj of  get the User List with Club Id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getUserWithClubId($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getUserWithClubId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['userList'];
    }

      /**
    * Returns an json obj of  get the Email List with User Id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getEmailWithUserId($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getEmailWithUserId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['emailList'];
    }

    /* get the Email List with User Id*
    public function getHeartRateGraph($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getHeartRateGraph';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        //Getting heart rate value to draw graph
        if (!isset($result['movesmart']['heartGraphList'][0])) {
            $result['movesmart']['heartGraphList']
                =array($result['movesmart']['heartGraphList']);
        }
        $hrValueExplode = $result['movesmart']['heartGraphList'][0]['heart_rate_value'];
        $coolDownStart = (
            $result['movesmart']['heartGraphList'][0]['cool_down_start_time'] != 0
        ) ? $result['movesmart']['heartGraphList'][0]['cool_down_start_time'] : START_PERIOD;

        $heartRateValue = explode('_', $hrValueExplode);
        //$totalSecondsPerMin = 60; //Total Seconds per min
        $totalGraphXAxis = DEFAULT_MAX_TEST_TIME; // X axis value

        //Create an array get X and Y values
        $countgraphXAxis = 0;
        $drawGraphValues = array();
        $drwHRWthoutCoolDwn = array();
        $testData = array();
        for ($graphXAxis = 1;$graphXAxis <= ($totalGraphXAxis * 60);++$graphXAxis) {
            $keyValueOfHeartRate = $graphXAxis; //Seconds
            // Seconds * 60(total seconds per min) to get key value of Heart Rate Value array.
            if (isset($heartRateValue[$keyValueOfHeartRate])) {

                //Test Data
                if ($graphXAxis >= WARMUP_PERIOD && $graphXAxis <= $coolDownStart) {
                    $countgraphXAxis = $countgraphXAxis + 1;
                    $xAndY = '{"y":'.$heartRateValue[$keyValueOfHeartRate].',"x":'.$graphXAxis.'}';
                    array_push($testData, $xAndY);

                    //Last Down value
                    if ($graphXAxis == $coolDownStart) {
                        $xAndY = '{"y":0,"x":'.$graphXAxis.'}';
                        array_push($testData, $xAndY);
                    }
                }

                if ($graphXAxis <= $coolDownStart) {
                    $xAndYCool = '{"y":'.$heartRateValue[$keyValueOfHeartRate].',"x":'.$graphXAxis.'}';
                    array_push($drwHRWthoutCoolDwn, $xAndYCool);
                    $xAndYCool = '{"y":0,"x":'.$graphXAxis.'}';
                }

                $xAndY = '{"y":'.$heartRateValue[$keyValueOfHeartRate].',"x":'.$graphXAxis.'}';
                array_push($drawGraphValues, $xAndY);
                $xAndY = '{"y":0,"x":'.$graphXAxis.'}';
            }
        }
        //Down Plot Last value HeartRate
        array_push($drawGraphValues, $xAndY);
        array_push($drwHRWthoutCoolDwn, $xAndYCool);

        //Return graph format here.
        $result['movesmart']['heartGraphList']['heartRateValue'] = implode(',', $drawGraphValues);
        $result['movesmart']['heartGraphList']['heartRateValueWithOutCoolDown'] = implode(',', $drwHRWthoutCoolDwn);
        $result['movesmart']['heartGraphList']['testData'] = implode(',', $testData);

        //Handling error
        if (!isset($result['movesmart']['heartGraphList'][0])) {
            $result['movesmart']['heartGraphList'] = array($result['movesmart']['heartGraphList']);
        }

        return $result['movesmart']['heartGraphList'];
    }

    /**
    * Returns an json obj of   get the Test List
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestList()
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTestList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['testList'];
    }

     /**
    * Returns an json obj of   Save on Test Results
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function saveTestResult($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'saveTestResult';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of Save on Test Parameters Results 
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function saveTestParameters($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'saveTestParameters';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * settings::saveHeartRateData()        
     * Description : Update heartrate Data
     * Param       : $params array 
     * Fields         : $params['userTestId'] and etc..
     * return      : Array.
     ** /.
     public function saveHeartRateData($params)
     {
     try {
     $params['mod'] = 'test';
     $params['method'] = 'saveHeartRateData';
     $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
     } catch (Exception $e) {
     $result = 'Caught Exception:'.$e->getMessage();
     }
     
     return $result;
     }
     
     /**
     * test::saveUserTestData()        
     * Description : Update heartrate Data
     * Param       : $params array 
     * Fields         : $params['userTestId'] and $params['userId']
     * return      : Array.
     ** /
     public function saveUserTestData($params)
     {
     try {
     $params['mod'] = 'test';
     $params['method'] = 'saveUserTestData';
     $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
     } catch (Exception $e) {
     $result = 'Caught Exception:'.$e->getMessage();
     }
     
     return $result;
     }
     
    
     /**
    * Returns an json obj of update adjust sportmed data
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateAdjustSportmedData($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'updateAdjustSportmedData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get Details for training workload
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getDetailsForTrainingWorkLoad($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getDetailsForTrainingWorkLoad';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['trainingWorkLoad'];
    }

     /**
    * Returns an json obj of get Training WorkLoad Grid Details
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTrainingWorkLoad($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTrainingWorkLoad';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['trainingWorkLoad'];
    }
     /**
    * Returns an json obj of save Training WorkLoad 
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function saveTrainingWorkLoad($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'saveTrainingWorkLoad';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of get the test result details based on the test_result_id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getSportMedDataForGraph($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getSportMedDataForGraph';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of change the interrupt result in test table.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function changeTestStatusToInterrupt($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'changeTestStatusToInterrupt';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        /**If the datas are updated in test table get the new user test id and pull the value to user parameters*/

        if ($result['error_code'] == 0) {
            $userTestId = $result['userTestId'];
            if ($userTestId > 0) {
                try {
                    $paramUserParameter['userTestId'] = $userTestId;
                    $paramUserParameter['existingUserTestId'] = $params['userTestId'];
                    $paramUserParameter['mod'] = 'test';
                    $paramUserParameter['method'] = 'changeTestParameterToInterrupt';
                    $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($paramUserParameter));
                } catch (Exception $e) {
                    $result = 'Caught Exception:'.$e->getMessage();
                }
            }
        }

        return $result;
    }
      /**
    * Returns an json obj of get test members list..
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestResultMemberList($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTestResultMemberList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        //Covert array if only one row exist
        if (!isset($result['movesmart']['testResultList'][0])) {
            $result['movesmart']['testResultList'] = array($result['movesmart']['testResultList']);
        }

        return array(
            'result' => $result['movesmart']['testResultList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }

    /**
     *  Author      : Jeyaram.A
     *  Description : To get manage Points members list.
     * /.
     public function getManagePointsList($params)
     {
     try {
     $params['mod'] = 'test';
     $params['method'] = 'getManagePointsList';
     $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
     } catch (Exception $e) {
     $result = 'Caught Exception:'.$e->getMessage();
     }
     
     //Covert array if only one row exist
     if (!isset($result['movesmart']['testResultList'][0])) {
     $result['movesmart']['testResultList'] = array($result['movesmart']['testResultList']);
     }
     
     return array(
        'result' => $result['movesmart']['testResultList'],
        'totalCount' => $result['movesmart']['totalCount']
     );
     }

    /**
    * Returns an json obj of gget test detail navigation links.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestDetailNavIds($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTestDetailNavIds';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if (!isset($result['movesmart']['testDetailNavLinks'][0])) {
            $result['movesmart']['testDetailNavLinks'] = array($result['movesmart']['testDetailNavLinks']);
        }

        return $result['movesmart']['testDetailNavLinks'];
    }


    /**
    * Returns an json obj of get test member ids
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestMemberIds($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getTestMemberIds';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if (!isset($result['movesmart']['testDetailNavLinks'][0])) {
            $result['movesmart']['testDetailNavLinks'] = array($result['movesmart']['testDetailNavLinks']);
        }

        return $result['movesmart']['testDetailNavLinks'];
    }


     /**
    * Returns an json obj of  get activity Id based on Activity Name.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getActivity($params)
    {
        try {
            $params['mod'] = 'test';
            $params['method'] = 'getActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }
   /**
    * Returns an json obj of  Get training points Details of the Member.
    * @param string $params service parameter
    *
    * @return array object object
    */
     public function getTrainingPointsDetail($params)
     {
         try {
             $params['mod'] = 'test';
             $params['method'] = 'getTrainingPointsDetail';
             $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
         } catch (Exception $e) {
             $result = 'Caught Exception:'.$e->getMessage();
         }
         if (!isset($result['movesmart']['trainingInfoDetails'][0])) {
             $result['movesmart']['trainingInfoDetails'] = array($result['movesmart']['trainingInfoDetails']);
         }

         return $result['movesmart']['trainingInfoDetails'];
     }

         /**
        * Returns an json obj of  Get Training Points week Details of Member.
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getMemberTrainingPointsAchieve($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getMemberTrainingPointsAchieve';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart'];
        }

        /**
        * Returns an json obj of  Get Training Program Parameters.
        * @param string $params service parameter
        *
        * @return array object object
        */

        public function getMemberTrainingParameters($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getMemberTrainingParameters';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart']['trainingParm'];
        }

        /* Get Heart Rate Zone Activity *
        public function getHeartRateZoneActivity($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getHeartRateZoneActivity';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            if (!isset($result['movesmart']['trainingZone'][0])) {
                $result['movesmart']['trainingZone'] = array($result['movesmart']['trainingZone']);
            }

            return $result['movesmart']['trainingZone'];
        }

         /**
        * Returns an json obj of  Get Points Achieve Week.
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getPointsAchieveWeek($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getPointsAchieveWeek';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart'];
        }

        /* Get Training Transaction Points *
        public function insertPointAchived($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'insertPointAchived';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart'];
        }

        /* update achieved point * /
        public function updateAchievedPoint($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'updateAchievedPoint';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart'];
        }

        /**
        * Returns an json obj of  Get Achieved Point By Member In Week List.
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getAchievedPointByMember($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getAchievedPointByMember';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            if (!isset($result['movesmart']['achievedPointMemberByWeek'][0])) {
                $result['movesmart']['achievedPointMemberByWeek']
                    =array($result['movesmart']['achievedPointMemberByWeek']);
            }
            /*if(!isset($result['movesmart']['TotalPoints'][0])){
            $result['movesmart']['TotalPoints']=array($result['movesmart']['TotalPoints']);
            }*/
            return array(
                $result['movesmart']['achievedPointMemberByWeek'],
                $result['movesmart']['TotalPoints']
            );
        }

        /* Get Achieved Point By Member In Week List for grid* /
        public function getAchievedPointByMemberGriddata($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getAchievedPointByMemberGriddata';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            if (!isset($result['movesmart']['achievedPointMemberByWeek'][0])) {
                $result['movesmart']['achievedPointMemberByWeek']
                    =array($result['movesmart']['achievedPointMemberByWeek']);
            }
            /*if(!isset($result['movesmart']['TotalPoints'][0])){
            $result['movesmart']['TotalPoints']=array($result['movesmart']['TotalPoints']);
            }* /
            return array($result['movesmart']['achievedPointMemberByWeek'], $result['movesmart']['TotalPoints']);
        }

          /**
        * Returns an json obj of   Delete Points Transaction Details.
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function deletePointsTransction($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'deletePointsTransction';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart'];
        }

          /**
        * Returns an json obj of   getTrainingPointsTransaction Detail
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getTrainingPointsTransaction($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getTrainingPointsTransaction';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result['movesmart']['trainingPoints'];
        }

          /**
        * Returns an json obj of   getMemberTestLastIdSet Detail
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getMemberTestLastIdSet($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getMemberTestLastIdSet';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            if (!isset($result['movesmart']['testIdSet'][0])) {
                $result['movesmart']['testIdSet'] = array($result['movesmart']['testIdSet']);
            }

            return $result['movesmart']['testIdSet'];
        }

         /**
        * Returns an json obj of   get points for each training devices in activity
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getCollectedPointsTrainingDevicesInWeek($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getCollectedPointsTrainingDevicesInWeek';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            if (!isset($result['movesmart']['collectedDevices']['0'])) {
                $result['movesmart']['collectedDevices'] = array($result['movesmart']['collectedDevices']);
            }

            return $result['movesmart']['collectedDevices'];
        }

         /**
        * Returns an json obj of  All Test Dates For Member
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getAllTestDatesForMember($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getAllTestDatesForMember';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }
            if (!isset($result['movesmart']['testDates'][0])) {
                $result['movesmart']['testDates'] = array($result['movesmart']['testDates']);
            }

            return $result['movesmart']['testDates'];
        }

        /* Reanalyze the current test for member * /
        public function reanalyzeMemberTest($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'reanalyzeMemberTest';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }

        /**
        * Returns an json obj of  Get The User Test Status Message
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getUserTestStatusMessage($params)
        {
            $statusMsg = '';
            $userStatusType = $params['current_test_status'];
            switch ($userStatusType) {
            case '0':
                $statusMsg = 'Test Initiated';
                break;
            case '1':
                $statusMsg = 'Started';
                break;
            case '2':
                $statusMsg = 'Cool Down';
                break;
            case '3':
                $statusMsg = 'Completed';
                break;
            case '10':
                $statusMsg = 'Cycle is Assigned for the Member';
                break;
            case '20':
                $statusMsg = 'Hrm is Assigned for the Member';
                break;
            case '30':
                $statusMsg = 'Interrupted/Cancelled';
                break;
            case 'default':
                $statusMsg = 'Completed';
                break;
            }

            return $statusMsg;
        }

          /**
        * Returns an json obj of   Reanalyze the current test for member
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getCoachInfo($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'getCoachInfo';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }

          /**
        * Returns an json obj of   get the closest value in array with given value
        * @param array $arrayName list of array
        * @param string $number total count
        *
        * @return array object object
        */
        public function getclosest($arrayName, $number)
        {
            sort($arrayName);
            $allLessValues = array();
            if(isset($arrayName)) {
                foreach ($arrayName as $aliasArray) {
                    if ($aliasArray <= $number) {
                        $allLessValues[] = $aliasArray;
                    }
                }
            }
            return $allLessValues;
        }

          /**
        * Returns an json obj of   set test to put in waitinglist
        * @param string $dataString service parameter
        *
        * @return array object object
        */
        public function saveTestPutInWaitingList($dataString)
        {
            try {
                $dataString['mod'] = 'test';
                $dataString['method'] = 'saveTestPutInWaitingList';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($dataString));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }

    /**
     * Returns an json obj of  add/updtae Members Test Details for test
     * @param $params
     * @return array object object
     * @internal param string $dataString service parameter
     *
     */
        public function insertMemberForTest($params)
        {
            try {
                $params['mod'] = 'test';
                $params['method'] = 'insertMemberForTest';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
}
