<?php

/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Common
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle common functions.
 */
/**
 * Class to handle userType related functions.

 * @category Classes

 * @package UserType

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @version Release: 1.0

 * @link http://movesmart.company/admin/

 */
require_once '../'.DS.'ws'.DS.'data.php';
require_once '../lib/phpmailer/PHPMailerAutoload.php';

class common extends log
{
    /* Method : displayVersion. Display the version details. */

    public function displayVersions()
    {
        return MODULE_VERSION.'-'.MODULE_VERSION_DATE;
    }

    /* Added By Sankar to get the valid formated message */

    /**
     * get formatted message
     *
     * @param $message
     * @param string $status
     * @param string $errorcode
     * @return array object obj
     * @internal param array $params service parameter, Search arguments
     */
    public function getFormatedMessage($message, $status = 'success', $errorcode = '200')
    {
        $status = array(
                'status' => $status,
                'error_code' => $errorcode,
                'status_message' => $message,
            );

        return $status;
    }
    /**
     * call the web service through curl and convert the xml as string.
     */


    /**
    * web service to convert xml to string
    *
    * @param url
    *
    * @return array object obj
    */
    public function webServiceXMLToString($url)
    {
        ob_start();
        ob_get_clean();
        $API['url'] = $url;
        $dataModel = new Data();
        // Obtain XML
        curl_init();
        //$curlReturnTransfer = 1;
        //$curlTimeOut = 500;
        $curlUrl = $API['url'];

        $this->writeWebserviceLogs($curlUrl);

        /* Change method to post begins - Note this is temporary fix to skip get method for long URL */
        $tmpUrl = $curlUrl;
        $urlStartPos = strpos($tmpUrl, '?');
        //$curlUrl = substr($tmpUrl, 0, $urlStartPos);
        $postFields = substr($tmpUrl, $urlStartPos + 1);
        /* curl_setopt($curlHandler, CURLOPT_POST, 1);
          curl_setopt($curlHandler, CURLOPT_POSTFIELDS,$postFields);
          //Change method to post ends

          curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, $curlReturnTransfer);
          curl_setopt($curlHandler, CURLOPT_TIMEOUT, $curlTimeOut);
          curl_setopt($curlHandler, CURLOPT_URL, $curlUrl);
          ini_set('max_execution_time', 300); //300 seconds = 5 minutes
          $curlResponse = curl_exec($curlHandler);
          $curlError = curl_error($curlHandler);
          $curlInfo = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
          curl_close($curlHandler); */
        $curlResponse = $dataModel->processRequest($postFields);
        //$xmlResponse = new stdClass();
        $curlResponse = utf8_encode($curlResponse);
        $xmlResponse = @simplexml_load_string($curlResponse);
        $responseArray = $this->XML2Array($xmlResponse);

        return $responseArray;
    }

    /**
     * call the curl url to post data.
     */


    /**
     * to curl post data to page
     *
     * @param url
     *
     * @return array object obj
     */
    public function curlPostDataToPage($url)
    {
        ob_start();
        ob_get_clean();
        $API['url'] = $url;

        // Obtain XML
        $curlHandler = curl_init();
        $curlReturnTransfer = 1;
        $curlTimeOut = 500;
        $curlUrl = $API['url'];

        /* Change method to post begins - Note this is temporary fix to skip get method for long URL */
        $tmpUrl = $curlUrl;
        $urlStartPos = strpos($tmpUrl, '?');
        $curlUrl = substr($tmpUrl, 0, $urlStartPos);
        $postFields = substr($tmpUrl, $urlStartPos + 1);
        curl_setopt($curlHandler, CURLOPT_POST, 1);
        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $postFields);
        //Change method to post ends

        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, $curlReturnTransfer);
        curl_setopt($curlHandler, CURLOPT_TIMEOUT, $curlTimeOut);
        curl_setopt($curlHandler, CURLOPT_URL, $curlUrl);
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        $curlResponse = curl_exec($curlHandler);
        curl_error($curlHandler);
        curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
        curl_close($curlHandler);

        return $curlResponse;
    }

    /**
     * call the web service through curl and convert the xml as string.
     */


    /**
     * web service to convert jason to array
     *
     * @param $url
     * @param int $userId
     * @param int $usertestId
     * @return array object obj
     * @internal param array $params service parameter, Search arguments
     */
    public function webServiceJsonToArray($url, $userId = 0, $usertestId = 0)
    {
        ob_start();
        ob_get_clean();
        $API['url'] = $url;

        // Obtain XML
        $curlHandler = curl_init();
        $curlReturnTransfer = 1;
        $curlTimeOut = 500;
        $curlUrl = $API['url'];
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, $curlReturnTransfer);
        curl_setopt($curlHandler, CURLOPT_TIMEOUT, $curlTimeOut);
        curl_setopt($curlHandler, CURLOPT_URL, $curlUrl);
        $curlResponse = curl_exec($curlHandler);
        curl_error($curlHandler);
        curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
        curl_close($curlHandler);
        $this->writeWebserviceLogs($curlUrl);
        $responseArray = json_decode($curlResponse, true);

        //To Create Log File for point system
        $paramLog['webServiceUrl'] = $url;
        $paramLog['response'] = $curlResponse;
        $paramLog['userId'] = isset($userId) ? $userId : 0;
        $paramLog['usertestId'] = isset($usertestId) ? $usertestId : 0;
        $paramLog['mod'] = 'log';
        $paramLog['method'] = 'logServices';
        $paramLog['reference'] = 'logServices';
        $paramLog['type'] = 'PS'; // PS/IOS/CCA
        $this->webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($paramLog));

        return $responseArray;
    }

    /**
     * call the web service through curl and insert the records to DB.
     */


    /**
     * webservice to insert records
     *
     * @param $url
     * @param $fields
     * @return array object obj
     * @internal param $url , fields
     *
     */
    /*public function webServiceToInsert($url, $fields)
    {
        $fields_string = '';
        //$responseArray = array();
        //url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key.'='.$value.'&';
        }
        rtrim($fields_string, '&');

        //open connection
        curl_init();
        //set the url, number of POST vars, POST data

        curl_setopt($chobj, CURLOPT_URL, $url);
        curl_setopt($chobj, CURLOPT_POST, count($fields));
        curl_setopt($chobj, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($chobj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chobj, CURLOPT_ENCODING, 'UTF-8');

        $curlResponse = curl_exec($chobj);
        curl_error($chobj);
        curl_getinfo($chobj, CURLINFO_HTTP_CODE);
        curl_close($chobj); //close connection
        //$xmlResponse = new stdClass();

        $xmlResponse = simplexml_load_string($curlResponse);
        $responseArray = $this->XML2Array($xmlResponse);

        return $responseArray;
    }*/

    /**
     * Convert the Array to Json encoded data.
     * param string $queryStringArray
     * return json encode format $response.
     */

    /**
     * to convert array to jsaon
     *
     * @param $queryStringArray
     * @return array object obj
     * @internal param array $params service parameter, Search arguments
     *
     */
    public function convertArrayToJson($queryStringArray)
    {
        $response = json_encode($queryStringArray);

        return $response;
    }

    /**
     * Post serialised data with key value pair and call webservice
     * param string $url
     * param array $fields
     * return array $response.
     */


    /**
     * webservice to multi insert
     *
     * @param $url
     * @param $fields
     * @return array object obj
     * @internal param $url , fields
     *
     */
    /*
    public function webServiceToMultiInsert($url, $fields)
    {
        curl_init();
        curl_setopt($chobj, CURLOPT_URL, $url);
        curl_setopt($chobj, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chobj, CURLOPT_POST, 1);
        curl_setopt($chobj, CURLOPT_POSTFIELDS, $fields);

        $buffer = curl_exec($chobj);
        curl_close($chobj);

        //$xmlResponse = new stdClass();

        $xmlResponse = simplexml_load_string($buffer);
        $response = $this->XML2Array($xmlResponse);

        return $response;
    }
    */
    /**
     * This function convert XML into array
     * param <SimpleXMLElement> $xml
     * known bug: always adding a sub item 0=>null to the item which have more than 1;
     * return array.
     */

    /**
    * convert xml to array
    *
    * @param xml
    *
    * @return array object obj
    */
    public function XML2Array($xml)
    {
        if ($xml instanceof SimpleXMLElement) {
            $attributes = $xml->attributes();
            $children = $xml->children();
        } else {
            return false;
        }
        //get attributes as items
        if ($attributes) {
            foreach ($attributes as $name => $attribute) {
                $thisNode[$name] = (String) $attribute;
            }
        }
        //get children elements to array item
        if ($children) {
            $newarray = array();
            foreach ($children as $name => $child) {
                //have children. and atributes alway with the element have children;
                if ($child->children()) {
                    if (isset($newarray[$name])) {
                        ++$newarray[$name];
                    } else {
                        $newarray[$name] = 1;
                    }
                } else {
                    $thisNode[$name] = (string) $child;
                }
            }
            //to fix the version 0.1 always has a 0=>null to end the multi elements
            foreach ($newarray as $name => $value) {
                if ($value > 1) {
                    for ($i = 0; $i < $value; ++$i) {
						
                        $thisNode[$name][] = $this->XML2Array($children->{$name}[$i]);
						
                    }
					
                } else {
                    $thisNode[$name] = $this->XML2Array($children->$name);
                }
            }
			
        }

        if (isset($thisNode) && count($thisNode) > 0) {
            return $thisNode;
        } else {
            return false;
        }
    }


    /**
    * get session
    *
    * @param string
    *
    * @return string obj
    */
    public function GetSession($CName)
    {
        $value = '';
        if (isset($_COOKIE["$CName"])) {
            $value = $_COOKIE["$CName"];
            $value = $this->Decrypt($value);
        }

        return $value;
    }

    /**
    * decrypt
    *
    * @param string
    *
    * @return string obj
    */
    public function Decrypt($string)
    {
        $result = '';
        $string = base64_decode($string);
        $key = 'IP123';
        for ($i = 0; $i < strlen($string); ++$i) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }

        return $result;
    }

    // To escape hacking
    /**
     * decrypt
     *
     * @param string $data
     *
     * @return string obj
     */
    public function escapeStr($data='')
    {
        //return ($data!='' )? mysqli_real_escape_string($data):$data;
        return ($data!='' )? mysql_escape_string($data):$data;
    }

    /**
     * To write log process.
     *
     * param string $urlInformation
     * Author nelson Roux
     *
     * access public
     */
    /**
    * to write log process
    *
    * @param string $urlInformation
    *
    * @return string obj
    */
    public function writeWebserviceLogs($urlInformation)
    {
        /* Check the write log is true than only write the service log */
        if (WRITE_SERVICE_LOG == true) {
            $LogFileName = date('Ymd').'_webservice.txt';
            $user = (!empty($_SESSION['user'])) ? $_SESSION['user'] : null;

            if (!empty($user)) {
                $userVal = 'userType-->'.$user['usertype'].
                        '--UserName-->'.$user['username'].
                        '--Email-->'.$user['email'].
                        '--UserId-->'.$user['user_id'];
            } else {
                $userVal = 'Anonymous user';
            }

            $datas = date('H:i:s').'   '.$userVal."\n\n".urldecode($urlInformation)."\n\n";
            file_put_contents(WEBSERVICE_LOG_PATH.$LogFileName, $datas, FILE_APPEND);
        }
    }

    /**
     * convert the given dateformat to "d-m-Y"
     * return string.
     */
    /**
     * to format site data
     *
     * @param $dateString
     * @param int $time
     * @return datetime $date
     * @internal param $datestring
     * @internal param $time
     *
     */
    public function siteDateFormat($dateString, $time = 0)
    {
        $date = '';
        if ($dateString != 0) {
            $date = date('d-m-Y', strtotime($dateString));
            if ($time == 1) {
                $date = date('d-m-Y H:i:s', strtotime($dateString));
            }
        }

        return $date;
    }

    /**
     * convert the given dateformat to "Y-m-d" to save db.
     * return string.
     */

    /**
    * to save date format
    *
    * @param datestring
    *
    * @return string obj
    */
    public function siteSaveDateFormat($dateString)
    {
        $dateFormat = $dateString;
        if ($dateString) {
            $explodeSLSH = explode('/', $dateString);
            if (count($explodeSLSH) == 1) {
                $explodeSLSH = explode('-', $dateString);
            }
            if (count($explodeSLSH) > 1) {
                list($sdateD, $sdateM, $sdateY) = $explodeSLSH;//explode("/", $explodeSLSH);
                $dateFormat = $sdateY.'-'.$sdateM.'-'.$sdateD;
            }
            //list($sdateD, $sdateM, $sdateY) = explode("-", $dateString);
        }

        return $dateFormat;
    }

    /**
     * get profile image
     *
     * @param $param
     * @return string obj
     * @internal param array $params service parameter, Search arguments
     *
     */
    public function getProfileImage($param)
    {
        $imgPath = PROFILEIMAGE_PATH;
        /*$imgPath = $imgPath . (isset($param['userType']) ? $param['userType'] . DS : "") .
            (isset($param['userId']) ? $param['userId'] . DS : "") .
            (isset($param['size']) ? $param['size'] . DS : "") . $param['imageName'];
        */
        $imgPath = $imgPath.$param['imageName'];
        if (!file_exists($imgPath) || $param['imageName'] == '') {
            $imgPath = PROFILEIMAGE_PATH.'user-up-photo.jpg';
        }
        echo $imgPath;
    }

    /* Set site flash message Error/Success message */
    
     /**
    * show flash message
    *
    * @return string obj
    */

    public function showFlashMessage()
    {
        $type = '';
        $msg = '';
        $html = '';
        if (isset($_SESSION['fl'])) {
            $type = ($_SESSION['fl'][0] == 1) ? 'success-msg' : 'failure-msg';
            $msg = $_SESSION['fl'][1];

            unset($_SESSION['fl']);
        }

        if ($type != '' && $msg != '') {
            $html = <<<HTML
                    <script type="text/javascript" language="javascript">
                    flashMsgDisplay('{$msg}', '{$type}');
                    </script>
HTML;
        }

        return $html;
    }

    //This function generate navigation link based on the given array List 
    
    /**
    * get nav ids
    *
    * @param array
    *
    * @param integer
    *
    * @return array object obj
    */
    public function getNavIds($arrayIds, $currentId)
    {
        $navIds = array('min_user_id' => '', 'max_user_id' => '', 'nextid' => '', 'previd' => '');
        $navArr = $arrayIds;
        if (is_array($navArr)) {
            $navIds['max_user_id'] = max($navArr);
            $navIds['min_user_id'] = min($navArr);

            asort($navArr);
            foreach ($navArr as $arr) {
                if ($arr > $currentId) {
                    $navIds['nextid'] = $arr;
                    break;
                }
            }
            arsort($navArr);
            foreach ($navArr as $arr) {
                if ($arr < $currentId) {
                    $navIds['previd'] = $arr;
                    break;
                }
            }

            if ($currentId == $navIds['min_user_id']) {
                $navIds['min_user_id'] = '';
            }
            if ($currentId == $navIds['max_user_id']) {
                $navIds['max_user_id'] = '';
            }
        }
        $navigation = $navIds;

        return $navigation;
    }

    /* Function used to generate random string */
    
     /**
    * to generate random string
    *
    * @param integer $len
    *
    * @return string obj
    */

    public function randStr($len)
    {
        // The alphabet the random string consists of
        $abc = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        // The default length the random key should have
        $defaultLength = 3;

        // Ensure $len is a valid number
        // Should be less than or equal to strlen( $abc ) but at least $defaultLength
        $len = max(min(intval($len), strlen($abc)), $defaultLength);

        // Return snippet of random string as random string
        return substr(str_shuffle($abc), 0, $len);
    }

    //Common send email 
    
    /**
    * send email
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function sendEmail($params)
    {
			$mail = new PHPMailer();
		
			//Enable SMTP 
			 //$mail1->IsSMTP(); 
			  $mail->SMTPDebug = true;  // debugging: 1 = errors and messages, 2 = messages only
			  $mail->SMTPAuth = true;  // authentication enabled
			  $mail->Host = 'mail.movesmart.offshoresolutions.nl';
			  $mail->Port = 25;
			  $mail->Username = 'movesmartinfo@movesmart.offshoresolutions.nl';
			  $mail->Password = 'welcome@108'; 
		  
			//$mail->setFrom('movesmartinfo@movesmart.offshoresolutions.nl');
			 $mail->From = "movesmartinfo@movesmart.offshoresolutions.nl";
			$mail->FromName = "MOVESMART.Company";
			//$mail->setFrom($params['from'], isset($params['fromName']) ? $params['fromName'] : '');$params['to']
			$mail->addAddress($params['to']);
			//$mail->addAddress('movesmartinfo@movesmart.offshoresolutions.nl');
			//$mail->addAddress("babitakapoor.immanentsolutionsgmail.com");   
			$mail->Subject = $params['subject'];
			$mail->msgHTML($params['message']);
			$mail->addCC("prasenjeetd.immanentsolutions@gmail.com");
			//$bcc = "nikhil.immanentsolution@gmail.com, babitakapoor.immanentsolutions@gmail.com";
			$mail->addBCC("nikhil.immanentsolution@gmail.com");
        if (isset($params['attachment'])) {    

            //Multiple attachments
            if (is_array($params['attachment'])) {
                $params['attachment'] = implode(',', $params['attachment']);
            }
            $mail->addAttachment($params['attachment']);
			
			
        }

        //send the message, check for errors
        $res = 1;
        if (!$mail->send()) {
            $res = 'Mailer Error: '.$mail->ErrorInfo;
        }

        return $res;
    }
    /* Lines Update for fixing the bug - identify that MFP_JSON is started or not 13072015 92782 */
    
     /**
    * validate MFP_JSON
    *
    * @param json 
    *
    * @return string
    */
    public function validateMFP_JSON($MFP_JSON)
    {
        return ((isset($MFP_JSON['system_error'])) && ($MFP_JSON['system_error'] == 1)) ? true : false;
    }
    /* End 13072015 */
}

// End of class
;
