<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Import
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle import related functions.
 */
  /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class import extends common
{
    /*This function used to get all the users List.*/

    public $module = 'import';
    /**
    * Returns an json obj of get all the users List
    * @param string $params service parameter
    *
    * @return json obj
    */

    public function importUser($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'createUser';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

      /**
    * Returns an json obj of get member userid Based External Application user id
    * @param string $params service parameter
    *
    * @return json obj
    */

    public function getMemberuserIdBasedExternalApplicationUserId($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'getMemberuserIdBasedExternalApplicationUserId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }
      /**
    * Returns an json obj of update password
    * @param string $params service parameter
    *
    * @return json obj
    */

    public function updatePassword($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'updatePassword';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }
    /*
    public function importCompany($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'createCompany';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function listCompanies()
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'listCompanies';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function listUserStatus()
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'listUserStatus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    /* To get profession list *
    public function getProfession()
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'getProfession';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    /* To import profession list *
    public function importProfession($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importProfession';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function listUserType()
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'listUserType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function importClub($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importClub';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function importPersonalInfo($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importPersonalInfo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function importSlotType($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importSlotType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function importBooking($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importBooking';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    public function cancelBooking($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'cancelBooking';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

      /**
    * Returns an json obj of import medical against user data
    * @param string $params service parameter
    *
    * @return json obj
    */
    public function importMedicalAgainst($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importMedicalAgainst';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    /**
    * Returns an json obj of import medical joint Problem
    * @param string $params service parameter
    *
    * @return json obj
    */
    public function importMedicalJointProblem($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importMedicalJointProblem';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

     /**
    * Returns an json obj of import medical muscle injury user data
    * @param string $params service parameter
    *
    * @return json obj
    */
    public function importMedicalMuscleInjury($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importMedicalMuscleInjury';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }

    /**
    * Returns an json obj of import medical risk factor user data
    * @param string $params service parameter
    *
    * @return json obj
    */
    public function importMedicalRiskFactor($params)
    {
        try {
            $params['mod'] = $this->module;
            $params['method'] = 'importMedicalRiskFactor';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return json_encode($result, true);
    }
}
