<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Questionaries
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle questionaries related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class questionaries extends common
{
     /**
    * Returns an json obj of  get all questions
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function loadClientQuestionaries($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'loadClientQuestionaries';
          //  printLog(WEBSERVICE_PATH.QN.http_build_query($params));
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  update answer for question
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateAnswerForQuestion($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'updateAnswerForQuestion';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  get question by phase group
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getQuestionsByPhaseGroup($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'getQuestionsByPhaseGroup';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  updtae user phase
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateUserPhase($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'updateUserPhase';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  search coach and club
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function searchCoachAndClub($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'searchCoachAndClub';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  update answers from previous day question
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function updateAnswersFromPreviousDay($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'updateAnswersFromPreviousDay';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/
     /**
    * Returns an json obj of  update achieve points.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateAchivePoints($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'updateAchivePoints';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));

            return $result;
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  reset monitor activity
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function resetMonitorActivity($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'resetMonitorActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  reset question monitor
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function resetQuestionMonitor($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'resetQuestionMonitor';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  reset available test
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function resetAvailableTest($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'resetAvailableTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  reset training
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function resetTraining($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'resetTraining';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  reset  user data for testung purpose
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function resetUserDataForTestingPurpose($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'resetUserDataForTestingPurpose';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  remove reset user
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function removeResetUser($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'removeResetUser';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	public function removecomplete($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'removecomplete';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  reset password
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function resetPassword($params)
    {
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'resetPassword';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of  save all answer questions
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function saveAllAnswersQuestions($params)
    {
         
        try {
            $params['mod'] = 'questionaries';
            $params['method'] = 'saveAllAnswersQuestions';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
}
