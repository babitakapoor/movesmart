<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Image
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle image related functions.
 */
   /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class ImageDesign
{
    //Variables to store instances of this module and other core classes.
    public $fontSize, $fontFile, $img, $backgroundColor, $textColor;

    //The ImageDesign constructor.
    public function __construct()
    {
        //Core object creating
    }
    /**
    * Returns an json obj of create image based on the given width and height.
    * @param string $sourceFile source img filename
    * @param string $targetFile move folder name
    * @param string $maxwidth image max Width 
    * @param string $maxheight image  Max height
    *
    * @return json obj
    */  
    public function createImage($sourceFile, $targetFile, $maxwidth, $maxheight)
    {
        $maxwidth = $maxwidth;
        $maxheight = $maxheight;
        $info = pathinfo($sourceFile);
            //check image type 
        if (strtolower($info['extension']) == 'jpg' || strtolower($info['extension']) == 'jpeg') {
            list($orig_width, $orig_height, $orig_type, $orig_attr) = getimagesize("{$sourceFile}");
            if ($orig_type) {
            };
            if ($orig_attr) {
            };
            $img = imagecreatefromjpeg("{$sourceFile}");

            //calculate jpg image width and height
            if ($maxwidth >= $orig_width and $maxheight >= $orig_height) {
                $height = $orig_height;
                $width = $orig_width;
            } else {
                if ($maxwidth / $orig_width <= $maxheight / $orig_height) {
                    $width = $maxwidth;
                    $height = ($maxwidth / $orig_width) * $orig_height;
                } else {
                    $height = $maxheight;
                    $width = ($maxheight / $orig_height) * $orig_width;
                }
            }
            // create jpg thumb image
            $newx = $width;
            $newy = $height;
            $orgx = $orig_width;
            $orgy = $orig_height;
            $convimg = imagecreatetruecolor($newx, $newy);
            imagecopyresampled($convimg, $img, 0, 0, 0, 0, $newx, $newy, $orgx, $orgy);
            imagejpeg($convimg, "{$targetFile}", 100);
        }
        // store image details into xml file
        return array($newx, $newy);
    }
    /*
    public function createMediumPath($sourceFile, $targetFile, $thumb = true, $imagelogo = false)
    {
        $medium = false;
        if ($thumb) {
            $maxwidth = THUMB_WIDTH;
            $maxheight = THUMB_HEIGHT;
        } elseif ($imagelogo) {
            $maxwidth = LOGO_WIDTH;
            $maxheight = LOGO_WIDTH;
        } else {
            $maxwidth = 600;
            $maxheight = 600;
            $medium = true;
        }
        $info = pathinfo($sourceFile);

        //check image type 
        if (strtolower($info['extension']) == 'jpg' || strtolower($info['extension']) == 'jpeg') {
            list($orig_width, $orig_height, $orig_type, $orig_attr) = getimagesize("{$sourceFile}");
            if ($orig_type){};
            if ($orig_attr){};
            $img = imagecreatefromjpeg("{$sourceFile}");

            //calculate jpg image width and height
            if ($maxwidth >= $orig_width and $maxheight >= $orig_height) {
                $height = $orig_height;
                $width = $orig_width;
            } else {
                if ($maxwidth / $orig_width <= $maxheight / $orig_height) {
                    $width = $maxwidth;
                    $height = ($maxwidth / $orig_width) * $orig_height;
                } else {
                    $height = $maxheight;
                    $width = ($maxheight / $orig_height) * $orig_width;
                }
            }
            // create jpg thumb image
            $newx = $width;
            $newy = $height;
            $orgx = $orig_width;
            $orgy = $orig_height;
            $convimg = imagecreatetruecolor($newx, $newy);
            imagecopyresampled($convimg, $img, 0, 0, 0, 0, $newx, $newy, $orgx, $orgy);
            imagejpeg($convimg, "{$targetFile}", 100);
        }
        // store image details into xml file
        return array($newx, $newy);
    }
      /**
    * Returns an json obj of change the hexa color value to RGB.
    * @param string $color hexavalue parameter
    *
    * @return json obj
    */
    public function hex2RGB($color)
    {
        if ($color[0] == '#') {
            $color = substr($color, 1);
        }

        if (strlen($color) == 6) {
            list($red, $green, $blue) = array($color[0].$color[1],
                                     $color[2].$color[3],
                                     $color[4].$color[5], );
        } elseif (strlen($color) == 3) {
            list($red, $green, $blue) = array($color[0], $color[1], $color[2]);
        } else {
            return false;
        }

        $red = hexdec($red);
        $green = hexdec($green);
        $blue = hexdec($blue);

        return array($red, $green, $blue);
    }
     /**
    * Returns an json obj of change the  resize the image to given width and height.
    * @param string $sourceFile source img filename
    * @param string $targetFile move folder name
    * @param string $newWidth image resize width 
    * @param string $newHeight image  resize height
    *
    * @return json obj
    */
    public function resizeImage($sourceFile, $targetFile, $newWidth, $newHeight)
    {
        // Getting file extension
        $filetype = strtolower(substr($sourceFile, strlen($sourceFile) - 3));

        $img_size = getImageSize($sourceFile);
        $width = $img_size[0];
        $height = $img_size[1];

        $thumb = ImageCreateTrueColor($newWidth, $newHeight);

        switch ($filetype) {
        case 'jpg':
            $source = ImageCreateFromJPEG($sourceFile);
            ImageCopyResampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
            ImageJPEG($thumb, $targetFile, 95);
            break;

        case 'gif':
            $source = ImageCreateFromGIF($sourceFile);
            ImageCopyResampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
            ImageGIF($thumb, $targetFile);
            break;

        default:
            break;
        }
    }
     /**
    * Returns an json obj of change the  crop the picture.
    * @param string $sourceFile source img filename
    * @param string $targetFile move folder name
    * @param string $cropStartX image crop X position  
    * @param string $cropStartY image  crop Y position
    * @param string $cropW image  crop width
    * @param string $cropH image  crop Height
    * @param string $imgStartX image  start X position
    * @param string $imgStartY image  start Y position
    *
    * @return json obj
    */
    public function cropImage($imgfile, $targetfile, $cropStartX, $cropStartY, $cropW, $cropH, $imgStartX = 0, $imgStartY = 0)
    {
        $filetype = strtolower(substr($imgfile, strlen($imgfile) - 3));

        // Get the original size
        list($width, $height) = getimagesize($imgfile);

        switch ($filetype) {
        case 'jpg':
        case 'jpeg':
            // Create two images
            $origimg = ImageCreateFromJPEG($imgfile);
            $cropimg = ImageCreateTrueColor($cropW, $cropH);
            $white = imagecolorallocate($cropimg, 255, 255, 255);
            imagefill($cropimg, 0, 0, $white);
            ImageCopyResampled($cropimg, $origimg, $imgStartX, $imgStartY, $cropStartX, $cropStartY, $width, $height, $width, $height);
            ImageJPEG($cropimg, $targetfile, 95);
            break;
        case 'gif':
            // Create two images
            $origimg = ImageCreateFromGIF($imgfile);
            $cropimg = ImageCreateTrueColor($cropW, $cropH);
            $white = imagecolorallocate($cropimg, 255, 255, 255);
            imagefill($cropimg, 0, 0, $white);
            ImageCopyResampled($cropimg, $origimg, $imgStartX, $imgStartY, $cropStartX, $cropStartY, $width, $height, $width, $height);
            ImageGIF($cropimg, $targetfile);
            break;
        default:
            break;
        }

        // destroy the images
        imagedestroy($cropimg);
        imagedestroy($origimg);
    }

    /**
    * Returns an json obj of check the given folder exists, if not exists create the folder.
    * @param string $path source image path
    * @param string $imagefolder image folder name
    * @param string $temp image tempory   path
    *
    * @return json obj
    */
    public function checkAndCreateFolder($path, $imagefolder, $temp = 0, $mogile = 0)
    {

        // echo $path.'<br>';
        // echo $imagefolder.'<br>'; 

        $currentdatefolder = date('Ymd');

        if (!is_dir($path.$imagefolder)) {
            $this->createFolder($path, $imagefolder, '', $temp, $mogile);
        }

        $path = $path.$imagefolder.DS;

        //If there is not inner folder name then creates one and saves the name to table for the client id
        if ($currentdatefolder || !is_dir($path.$currentdatefolder)) {
            $imageuploadfolder = $this->createFolder($path, '', $currentdatefolder, $temp, $mogile);
            if ($imageuploadfolder) {
                $newpath['imagefolder'] = $imageuploadfolder;
            }
        }

        //Put the new folder name in an array
        $newpath['gallerypath'] = $path.$imageuploadfolder;
        $newpath['absgallerypath'] = realpath($path.$imageuploadfolder).DS;
        $newpath['absthumbnailpath'] = realpath($path.$imageuploadfolder.DS.'s').DS;
        $newpath['parentfoldername'] = $imagefolder;
        $newpath['subfoldername'] = $imagefolder;
        $newpath['absmediumpath'] = realpath($path.$imageuploadfolder.DS.'m').DS;

        return $newpath;
    }
     /**
    * Returns an json obj of create the folder
    * @param string $path source image path
    * @param string $imagefolder image folder name
    * @param string $foldername inner folder name
    * @param string $temp image tempory   path
    *
    * @return void
    */
    public function createFolder($path, $imagefolder, $foldername = false, $temp = 0, $mogile = 0)
    {
        if ($imagefolder) {
            if (!is_dir($path.$imagefolder)) {
                mkdir($path.$imagefolder, 0777, true); //@mkdir($path . $imagefolder);
            }
        }
        if ($foldername) {
            if (!$temp && !$mogile) {
                if (!is_dir($path.DS.'s')) {
                    @mkdir($path.DS.'s');
                }
                if (!is_dir($path.DS.'m')) {
                    @mkdir($path.DS.'m');
                }
            }
            //return $subfolder;
        }
    }
    /**
    * Returns an json obj of remove the folder and it files
    * @param string $dirname remove directory name
    *
    * @return void
    */
    public function removeDir($dirname)
    {
        if (is_dir($dirname)) {    //Operate on dirs only
            $result = array();
            if (substr($dirname, -1) != '/') {
                $dirname .= '/';
            }    //Append slash if necessary
            $handle = opendir($dirname);
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..') {    //Ignore . and ..
                    $path = $dirname.$file;
                    if (is_dir($path)) {    //Recurse if subdir, Delete if file
                        $result = array_merge($result, $this->removeDir($path));
                    } else {
                        unlink($path);
                        $result[] .= $path;
                    }
                }
            }
            closedir($handle);
            rmdir($dirname);    //Remove dir
            $result[] .= $dirname;

            return $result;    //Return array of deleted items
        } else {
            return false;    //Return false if attempting to operate on a file
        }
    }
     /**
    * Returns an json obj of check the image type and it convert to jpg if it is not jpg type
    * @param string $fileWithPath image with path
    *
    * @return void
    */
    public function force_jpeg($fileWithPath)
    {
        $img = getimagesize($fileWithPath);

        if ($img === false) {
            return false;
        }
        $type = $img[2];
        switch ($type) {
        case 1: // for gif
            $imagehandle = imagecreatefromgif($fileWithPath);
            break;
        case 2: // for jpg
            return true;
                break;
        case 3: // for png
            $imagehandle = @imagecreatetruecolor($img[0], $img[1]);
            $imagehandle = @imagecreatefrompng($fileWithPath);
            break;
        default :
            return false;
        }

        return imagejpeg($imagehandle, $fileWithPath, 90);
    }

    
     /**
    * Returns an json obj of verifies whether file with such name already exists and if so, construct safe filename name (to avoid collision).
    * @param string $fileName file name
    * @param string $check_folder  path
    *
    * @return void
    */
    public function getSafeFileName($fileName, $check_folder)
    {
        $filetype = strtolower(substr($fileName, strlen($fileName) - 3));
        $newFileName = $fileName;
        $inccnt = 1;
        while (file_exists($check_folder.$newFileName)) {
            $newFileName = $inccnt.'_'.$fileName;
            $inccnt = $inccnt + 1;
        }
        //Extension Changed to jpg
        $newFileNameWOE = substr($newFileName, 0, strrpos($newFileName, '.'));
        //chnaged by charles for file type
        if (preg_match('/\\.(exe|com|bat|zip|pdf|psd|doc|txt)$/i', $fileName)) {
            $jpgFileName = $newFileNameWOE.'.'.$filetype;
        } elseif (preg_match('/\\.(jpg|jpeg|gif)$/i', $fileName)) {
            $jpgFileName = $newFileNameWOE.'.jpg';
        } elseif (preg_match('/\\.(mp4|flv|mpeg|3gb|avi|wmv|ogg|webm)$/i', $fileName)) {
            $jpgFileName = $newFileNameWOE.'.'.$filetype;
        } elseif (preg_match('/\\.(png)$/i', $fileName)) {
            $jpgFileName = $newFileNameWOE.'.'.$filetype;
        }
        //return $newFileName;
        return $jpgFileName;
        //end
    }

    //Function    : checkAndCreateFolder()
    //Arguments   : Path, imagefolder name
    //Description : The function will check the given folder exists, if not exists create the folder
    /*
    public function checkAndCreateCroppedFolder($path, $imagefolder, $temp = 0, $mogile = 0, $currentdatefolder)
    {

        //$currentdatefolder 	= date('Ymd');

        if (!is_dir($path.$imagefolder)) {
            $this->createCroppedFolder($path, $imagefolder, '', $temp, $mogile);
        }

        $path = $path.$imagefolder.DS;

        /* //If there is not inner folder name then creates one and saves the name to table for the client id
        if($currentdatefolder || !is_dir($path . $currentdatefolder)) {                             
            $imageuploadfolder  = $this->createCroppedFolder($path, '',$currentdatefolder,$temp,$mogile);
            if($imageuploadfolder){
                $newpath['imagefolder'] = $imageuploadfolder;
            }
        }
        * /
        //Put the new folder name in an array
        $newpath['gallerypath'] = $path.$imageuploadfolder;
        $newpath['absgallerypath'] = realpath($path.$imageuploadfolder).DS;
        $newpath['absthumbnailpath'] = realpath($path.$imageuploadfolder.DS.'s').DS;
        $newpath['parentfoldername'] = $imagefolder;
        $newpath['subfoldername'] = $currentdatefolder;
        $newpath['absmediumpath'] = realpath($path.$imageuploadfolder.DS.'m').DS;

        return $newpath;
    }
    *
    //Function    : createFolder()
    //Arguments   : Path, imagefolder name, inner foldername
    //Description : The function will create the folder
    public function createCroppedFolder($path, $imagefolder, $foldername = false, $temp = 0, $mogile = 0)
    {
        if ($imagefolder) {
            if (!is_dir($path.$imagefolder)) {
                @mkdir($path.$imagefolder);
            }
        }
        if ($foldername) {
            if ($foldername) {
                $subfolder = $foldername;
            }

            if (!is_dir($path.$subfolder)) {
                @mkdir($path.$subfolder);
            }
            if (!$temp && !$mogile) {
                if (!is_dir($path.$subfolder.DS.'s')) {
                    @mkdir($path.$subfolder.DS.'s');
                }
                if (!is_dir($path.$subfolder.DS.'m')) {
                    @mkdir($path.$subfolder.DS.'m');
                }
            }

            return $subfolder;
        }
    }

     /**
    * Returns an json obj of checks and replace space in the filename with underscore and 
    *  removes special characters from the file name and returns the newly generated filename
    * @param string $fileName file name
    *
    * @return void
    */
    public function fileNameOnlyCharacter($FileName)
    {
        $FileName = str_replace(' ', '_', $FileName);
        $SAFE_OUT_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890._-';
        $result = '';
        for ($i = 0; $i < strlen($FileName); ++$i) {
            if (strchr($SAFE_OUT_CHARS, $FileName{$i})) {
                $result .= $FileName{$i};
            }
        }

        return $result;
    }
} // Class End.
;
