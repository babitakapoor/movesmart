<?php

//session_start();
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package TestingMeasuring
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle testing and measuring related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class testingAndMeasuring extends common
{
    /* Common function for retrive list of users */
    /**
     * Returns an json obj of retrive list of users
     * @param $param
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getUserList($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'getUserList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
                'result' => $result['movesmart']['memberList'],
                'totalCount' => $result['movesmart']['totalCount']
        );
    }

    /* Insert biometric test data*
    public function insertBiometricTestData($param)
    {
        $testingMeasuringId = $this->insertTestingMeasuring($param);
        if ($testingMeasuringId > 0) {
            $param['testingMeasuringId'] = $testingMeasuringId;

            return $this->updateTestItemData($param);
        }
    }

    /* Insert biometric test data*
    public function insertMedicalTestData($param)
    {
        $testingMeasuringId = $this->insertTestingMeasuring($param);
        if ($testingMeasuringId > 0) {
            $param['testingMeasuringId'] = $testingMeasuringId;

            return $this->updateTestItemData($param);
        }
    }

    /* To Add Testing measuring Functionality test data */
    public function insertFunctionalityTestData($param)
    {
        $testingMeasuringId = $this->insertTestingMeasuring($param);
        if ($testingMeasuringId > 0) {
            $param['testingMeasuringId'] = $testingMeasuringId;

            return $this->updateTestItemData($param);
        }
        return null;
    }

    /* To Add Testing measuring Cardio vascular test data */
    public function insertCardioVascularTestData($param)
    {
       
        $testingMeasuringId = $this->insertTestingMeasuring($param);
        if ($testingMeasuringId > 0) {
            $param['testingMeasuringId'] = $testingMeasuringId;

            return $this->updateTestItemData($param);
        }
        return null;
    }

    /* To Add Testing measuring body composition test data*/
    public function insertBodyCompositionTestData($param)
    {
		
	   
        //if ($param['testId'] == TEST_BODY_COMPOSITION) {
			if ($param['testId'] == 7) {
            $testingMeasuringId = $this->insertTestingMeasuring($param);
            if ($testingMeasuringId > 0) {
            
                $param['testingMeasuringId'] = $testingMeasuringId;
                $return_result = $this->inserBodyComptiondata($param);
                $level = $this->get_levels($param['testingMeasuringId']);
				
                $body_composition=round($level['body_composition']);
				if($body_composition<=1)
				{
					$body_composition=1;
				}
				else if($body_composition>=7){
					$body_composition=7;
				}
                    $v_level=$level['visceral_fat'];
					if($v_level<=1)
				{
					$v_level=1;
				}
				else if($v_level>=7){
					$v_level=7;
				}
                    $c_level=$level['cholestrol'];
					if($c_level<=1)
				{
					$c_level=1;
				}
				else if($c_level>=7){
					$c_level=7;
				}
                    $g_level=$level['glucose'];
					if($g_level<=1)
				{
					$g_level=1;
				}
				else if($g_level>=7){
					$g_level=7;
				}
				
                    $correct_body_level=$body_composition*40/100;
                    $correct_vis_level=$v_level*40/100;
                    $correct_chol_level=$c_level*10/100;
                    $correct_glu_level=$g_level*10/100;
              
                    $total_eat_level = round($correct_body_level+$correct_vis_level+$correct_chol_level+$correct_glu_level); 
                
                    $testdate=$param['testdate'];
                    $c_date = date('Y-m-d h:i:s', strtotime('-3 month', strtotime($testdate)));
                    $userid=$param['userId'];
                $check_all = "select * from `t_test_overall_result_level` where `r_user_id`=".$userid;
                $result_all = mysql_query($check_all);
                
                if(mysql_num_rows($result_all)>=1)
                {   
                    while($check_date=mysql_fetch_array($result_all))
                    {
                        $is_level_dt=$check_date['test_date'];    
                        $id=$check_date['t_test_overall_result_level_id'];
						$body_level = $check_date['body_composition_level'];
                        //echo $id;
                    }
                    
                    //die;
                   // if($c_date<=$is_level_dt)
					/**********   if($body_level=='' && $body_level==NULL)
                        {
                            
                    //$status = "followup";
                    $str_update = "update `t_test_overall_result_level` set `body_composition_level`=$body_composition,`visceral_fat_level`=$v_level,`cholesterol_level`=$c_level,`glucose_level`=$g_level,`total_eat_level`=$total_eat_level where `t_test_overall_result_level_id`=$id"; 
        //echo $str_update;
                    mysql_query($str_update);
                        }
                        else{ ************/
                            
                            $status="followup";
        $str_insert = "insert into `t_test_overall_result_level` (`body_composition_level`, `visceral_fat_level`, `cholesterol_level`,`glucose_level`,`type`,`r_user_id`,`test_date`,`total_eat_level`)
        values ('$body_composition', '$v_level','$c_level','$g_level','$status',$userid,'$testdate','$total_eat_level')";
                     
		
                    
                    mysql_query($str_insert);
					$id = mysql_insert_id();
					
					$_SESSION['record_id'] = $id;
				
                       // }
                }
                else{
				
                    $status="first_consult";
        $str_insert = "insert into `t_test_overall_result_level` (`body_composition_level`, `visceral_fat_level`, `cholesterol_level`,`glucose_level`,`type`,`r_user_id`,`test_date`,`total_eat_level`)
        values ('$body_composition', '$v_level','$c_level','$g_level','$status',$userid,'$testdate','$total_eat_level')";
                   
                    
                    mysql_query($str_insert);
					$id = mysql_insert_id();
				
					$_SESSION['record_id'] = $id;
					
                   }
                
                return $return_result;
                
                
            } else {
                return $this->getFormatedMessage('Problem creating test record.', 'error');
            }
        } 
        return $this->getFormatedMessage('Invalid Test type', 'error');
    }
    /*********get level****/
public function get_levels($mesuring_id)

{

     $bmi=0;
     $waist=0;
     $fat_percentage=0;
     $cholestrol = 0;
     $glu = 0;
     $vis_fat = 0;
     $data=0;
    $query = 'select * from `t_testing_measuring_transaction` where `r_testing_measuring_id`='.$mesuring_id;
   //echo $query;
   
    $r_data = mysql_query($query);
    while($row_data=mysql_fetch_array($r_data)){
        

        $item_id = $row_data['r_test_item_id'];
        $user_id = $row_data['r_user_id'];
        $item_value = $row_data['value'];
        $str_user = 'select * from `t_users` where `user_id`='.$user_id;
        $res_user = mysql_query($str_user);
        while($info=mysql_fetch_array($res_user))
        {
            $gender = $info['gender'];

        }
		 $str_userpersonal = 'select * from `t_personalinfo` where `r_user_id`='.$user_id;
        $res_userpersonal = mysql_query($str_userpersonal);
        while($info1=mysql_fetch_array($res_userpersonal))
        {
            $birthdate = $info1['dob'];

        }
	
		  $birthdate = explode("-", $birthdate);
		  //get age from date or birthdate
		  $age = (date("md", date("U", mktime(0, 0, 0, $birthdate[1], $birthdate[2], $birthdate[0]))) > date("md")
			? ((date("Y") - $birthDate[0]) - 1)
			: (date("Y") - $birthDate[0]));
        $str = 'select `test_item` from `t_test_items` where `test_item_id`='.$item_id;
        $result = mysql_query($str);
        
        while($row=mysql_fetch_array($result))
        {
    
    if($row['test_item']!='' && $row['test_item']=='BODY_MASS_INDEX')
    {
        
        $str_bmi = 'select `BMI_level` from `t_bmi_level` where `bmi_min` <='.$item_value.' and `bmi_max` >='.$item_value; 
            
            $res_bmi = mysql_query($str_bmi);
            
            while($bm_level=mysql_fetch_array($res_bmi))
            {
                
                $bmi = $bm_level['BMI_level'];
                
                
            }
            
    }
    else if($row['test_item']!='' && $row['test_item']=='FAT_PERCENTAGE')
    {   
      //  $fat_percentage = $item_value;
	  $str_bmi = 'select `fatlevel` from `t_fat_level` where `fat_min` <='.$item_value.' and `fat_max` >='.$item_value.' and gender='.$gender.' and `age_min` <='.$age.' and `age_max` >='.$age; 
            
            $res_fat = mysql_query($str_fat);
            
            while($fat_level=mysql_fetch_array($res_fat))
            {
                
                $fat_percentage = $fat_level['fatlevel'];
                
                
            }
        
    }
    else if($row['test_item']!='' && $row['test_item']=='BELLY_GRITH')
    {
        $str_waist = 'select `level` from `t_waist_level` where `waist_min` <='.$item_value.' and `waist_max` >'.$item_value.' and gender='.$gender; 
            $res_waist = mysql_query($str_waist);
            while($wst_level=mysql_fetch_array($res_waist))
            {
                
                $waist = $wst_level['level'];
                            
            }
    }
    else if($row['test_item']!='' && $row['test_item']=='CHOLESTROL')
    {
        $str_chol = 'select `level` from `t_cholestrol_level` where `c_min` <='.$item_value.' and `c_max` >'.$item_value; 
            $res_chol = mysql_query($str_chol);
            while($chol_level=mysql_fetch_array($res_chol))
            {
                
                $cholestrol = $chol_level['level'];
                            
            }
    }
    else if($row['test_item']!='' && $row['test_item']=='VISCERAL_FAT_PERCENTAGE')
    { 
        $str_vis_fat = 'select `level` from `t_visceral_fat_level` where `fat_min` <='.$item_value.' and `fat_max` >'.$item_value; 
            $res_vis_fat = mysql_query($str_vis_fat);
            while($vis_level=mysql_fetch_array($res_vis_fat))
            {
                
                $vis_fat = $vis_level['level'];
                            
            }
    }
    else if($row['test_item']!='' && $row['test_item']=='GLUCOSE_SOBER')
    {
        $str_glucose = 'select `level` from `t_glucose_level` where `glucose_min` <='.$item_value.' and `glucose_max` >'.$item_value.' and `sober`=1'; 
            $res_glucose = mysql_query($str_glucose);
            while($glucose_level=mysql_fetch_array($res_glucose))
            {
                
                $glu = $glucose_level['level'];
                            
            }
    }
    else if($row['test_item']!='' && $row['test_item']=='GLUCOSE_NOT_SOBER')
    {
        $str_glucose = 'select `level` from `t_glucose_level` where `glucose_min` <='.$item_value.' and `glucose_max` >'.$item_value.' and `sober`=0'; 
            $res_glucose = mysql_query($str_glucose);
            while($glucose_level=mysql_fetch_array($res_glucose))
            {
                
                $glu = $glucose_level['level'];
                            
            }
    }
    else{
        continue;
    }   
    
}
    }
    
    


    $body_composition = ($bmi + ($waist*2) + ($fat_percentage*2))/5;
	
    //$body_composition = isset($bmi)?$bmi:'0' + ((isset($waist)?$waist:'0')*2) + ((isset($fat_percentage)?$fat_percentage:'0')*2/5);
       $data=array('body_composition'=>$body_composition,
                    'cholestrol'=>$cholestrol,
                    'visceral_fat'=>$vis_fat,
                    'glucose'=>$glu
                    );
					
       /*$data=array('body_composition'=>$body_composition*40/100,
                    'cholestrol'=>$cholestrol*10/100,
                    'visceral_fat'=>$vis_fat*40/100,
                    'glucose'=>$glu*10/100
                    );*/
     // $data['visceral_fat']=$vis_fat*40/100;
      //$data['glucose']=$glu*10/100;
        //print_r($data);
        return $data;

}

    public function insertFlexibilityTestData($param)
    {
        if ($param['testId'] == TEST_FLEXIBILITY) {
            $testingMeasuringId = $this->insertTestingMeasuring($param);
            if ($testingMeasuringId > 0) {
                $param['testingMeasuringId'] = $testingMeasuringId;

                return $this->insertFlexblitytestdata($param);
            } else {
                return $this->getFormatedMessage('Problem creating test record.', 'error');
            }
        } else {
            return $this->getFormatedMessage('Invalid Test type', 'error');
        }
    }

    /* To Add FlexiblityData ***/

    public function insertFlexblitytestdata($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'insertFlexblitytestdata';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            if ($result['status_id'] == 200) {
                $params = $result['params'];
                $result = $this->updateTestItemData($params);
                if ($result['status'] == 'success') {
                    return $this->getFormatedMessage('Test succesfully created.', 'success');
                } else {
                    return $result;
                }
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Add/Update testing and measuring data */
    public function insertTestingMeasuring($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'insertTestingMeasuring';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if ($result['status'] == 'success') {
            return $result['testingMeasuringId'];
        } else {
            return false;
        }
    }

    public function inserBodyComptiondata($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'inserBodyComptiondata';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            if ($result['status_id'] == 200) {
                $params = $result['params'];
                $result = $this->updateTestItemData($params);
                if ($result['status'] == 'success') {
                    return $this->getFormatedMessage('Test succesfully created.', 'success');
                } else {
                    return $result;
                }
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Add/Update testing and measuring transaction data */
    public function updateTestItemData($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'updateTestItemData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Get testing and measuring transaction data */
    public function getTestingMeasuringData($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'getTestingMeasuringData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            if (isset($result['rows']) && !isset($result['rows'][0])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    public function getTestingMeasuringCardioData($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'getTestingMeasuringCardioData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            if (isset($result['rows']) && !isset($result['rows'][0])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        //return $result;
        return array('result' => $result['movesmart']['rows'], 'totalCount' => $result['movesmart']['totalCount']);
    }

    /* To delete testing and measuring transaction data */
    public function deleteTestingMeasuringData($param)
    {
        try {
            $param['mod'] = 'testingandmeasuring';
            $param['method'] = 'deleteTestingMeasuringData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
}
