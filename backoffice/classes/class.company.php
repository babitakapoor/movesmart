<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Company
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle company related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class company extends common
{
     /**
    * Returns an json obj of company add/update in db.
    * @param string $dataString service parameter
    *
    * @return json obj
    */  
    public function getCompany($dataString)
    {
        global $LANG;
        $this->result = parent::webServiceXMLToString(EXTERNAL_WEBSERVICE_PATH.'getUsersTypes.php'.QN.$dataString);
        $getArrayList = json_decode($this->result, true);
        $getLists = $getArrayList['Response']['companylist']['company'];
        if (is_array($getLists) && count($getLists) > 0) {
            foreach ($getLists as $getList) {
                $companyId = $getList['company_id'];
                $companyName = $getList['company_name'];
                $queryStrings = array('mod' => 'company', 'method' => 'getCompanyAddOrUpdate', 'param' => array('company_id' => $companyId, 'company_name' => $companyName));
                $dataStringIn = json_encode($queryStrings);
                $dataStringIns = urlencode($dataStringIn);
                echo $this->resultSet = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$dataStringIns);
                //$getSResult = json_decode($this->resultSet, true);
            }
        } else {
            echo $LANG['noResult'];
        }
    }

     /**
    * Returns an json obj of list of all company.
    * @param string $dataString service parameter
    *
    * @return json obj
    */  
    public function getCompanyList($dataString)
    {
        $this->result = parent::webServiceXMLToString(EXTERNAL_WEBSERVICE_PATH.'getUsersTypes.php'.QN.$dataString);

        return json_decode($this->result, true);
    }
}
