<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Cron
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle cron related functions.
 */
  /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
  /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class cron extends common
{
     /**
    * Returns an json obj of Club Details Add/Update in db.
    * @param string $params service parameter
    *
    * @return json obj
    */  
    public function getClubDetails()
    {
        global $LANG;
        if (THEME_NAME == 'movesmart') {
            $company_id = 2;
        } else {
            $company_id = 1;
        }
        $this->result = parent::webServiceXMLToString(EXTERNAL_WEBSERVICE_PATH.'getClubList.php');
        $getResult = json_decode($this->result, true);
        $response = $getResult[THEME_NAME]['clubdetails']['club'];

        if (is_array($response) && count($response) > 0) {
            foreach ($response as $value) {
                $company_club_id = $value['company_club_id'];
                $club_name = $value['club_name'];
                $street = $value['street'];
                $number = $value['number'];
                $bus = $value['bus'];
                $post_code = $value['post_code'];
                $location_id = $value['location_id'];
                $email_id = $value['email_id'];
                $queryString = array('mod' => 'cron', 'method' => 'addClubDetails', 'param' => array('company_club_id' => $company_club_id, 'club_name' => $club_name, 'street' => $street, 'number' => $number, 'bus' => $bus, 'post_code' => $post_code, 'location_id' => $location_id, 'email_id' => $email_id, 'company_id' => $company_id));
                $dataString = json_encode($queryString);
                $dataString = urlencode($dataString);
                $this->result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$dataString);
                //$getSResult = json_decode($this->result, true);
            }
        } else {
            echo $LANG['noResult'];
        }
    }

    /**
    * Returns an json obj of Members add/update in db.
    * @param string $params service parameter
    *
    * @return json obj
    */  
    public function getMembers()
    {
        global $LANG;
        $this->result = parent::webServiceXMLToString(EXTERNAL_WEBSERVICE_PATH.'getMembersList.php');
        $getArrayList = json_decode($this->result, true);
        $getLists = $getArrayList[THEME_NAME]['membersList']['members'];
        if (THEME_NAME == 'movesmart') {
            $company_id = 2;
        } else {
            $company_id = 1;
        }
        if (is_array($getLists) && count($getLists) > 0) {
            foreach ($getLists as $getList) {
                $club_id = $getList['club_id'];
                $status_id = $getList['status_id'];
                $first_name = $getList['first_name'];
                $last_name = $getList['last_name'];
                $gender = $getList['gender'];
                $email = $getList['email'];
                $username = $getList['username'];
                $password = $getList['password'];
                $person_id = $getList['person_id'];

                $queryStrings = array('mod' => 'cron', 'method' => 'getCoachAddOrUpdate', 'param' => array('company_id' => $company_id, 'club_id' => $club_id, 'status_id' => $status_id, 'first_name' => $first_name, 'last_name' => $last_name, 'gender' => $gender, 'email' => $email, 'username' => $username, 'password' => $password, 'person_id' => $person_id));

                $dataStringIn = json_encode($queryStrings);
                $dataStringIns = urlencode($dataStringIn);

                echo $this->resultSet = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.$dataStringIns);
                //$getSResult = json_decode($this->resultSet, true);
            }
        } else {
            echo $LANG['noResult'];
        }
    }

     /**
    * Returns an json obj of change the Status of Interupt Test.
    * @param string $params service parameter
    *
    * @return json obj
    */  
    public function changeStatusForInterruptedTest($params)
    {
        try {
            $params['mod'] = 'cron';
            $params['method'] = 'changeStatusForInterruptedTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of change cycle assigned status.
    * @param string $params service parameter
    *
    * @return json obj
    */  
    public function changeCycleAssignedStatus($params)
    {
        try {
            $params['mod'] = 'cron';
            $params['method'] = 'changeCycleAssignedStatus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
}
