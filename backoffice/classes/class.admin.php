<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle admin related functions.
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class admin extends common
{
    /* Add/Update plan details *

    public function updatePlanDetails($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'updatePlanDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* get plan list by company id */
    
    /**
    * Returns an json obj of plan List By Company.
    *
    * @param string $params service parameter
    *
    * @return array object object
    */    

    public function getPlanListByCompany($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getPlanListByCompany';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * get plan detail by plan id 
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getPlanDetailById($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getPlanDetailById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
    
    /**
    * soft delete row by plan id
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    
    public function deletePlanById($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'deletePlanById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
    
    /**
    * update plan club details
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function updatePlanClubDetails($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'updatePlanClubDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
    /**
    * get plan club list
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */

    public function getPlanClubList($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getPlanClubList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['clubPlanlist'][0])) {
            $result['movesmart']['clubPlanlist'] = array($result['movesmart']['clubPlanlist']);
        }

        return $result['movesmart']['clubPlanlist'];
    }

    
    /**
    * get strength machine list
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */

    public function getStrengthMachineList($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getStrengthMachineList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['strengthMachine'][0])) {
            $result = array($result);
        }

        return $result;//['strengthMachine'];
    }
    
    /**
    * get machine list by program id
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getMachineListByProgramId($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getMachineListByProgramId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
    /**
    * get strength machine club list
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getStrengthMachineClubList($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getStrengthMachineClubList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['strengthMachine'][0])) {
            $result['movesmart']['strengthMachine'] = array($result['movesmart']['strengthMachine']);
        }

        return $result['movesmart']['strengthMachine'];
    }

    
    /**
    * get activities
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getActivities($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getActivities';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['activities'][0])) {
            $result['movesmart']['activities'] = array($result['movesmart']['activities']);
        }

        return $result['movesmart']['activities'];
    }

    
    /**
    * get activities for a club
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getActivitiesClub($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getActivitiesClub';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['activitiesClub'][0])) {
            $result['movesmart']['activitiesClub'] = array($result['movesmart']['activitiesClub']);
        }

        return $result['movesmart']['activitiesClub'];
    }

   
    /**
    * to add or update strwngth machine details
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function updateStrengthMachineDetails($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'updateStrengthMachineDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    
    
    /**
    * get memebers by club id
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getMemberByClub($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getMemberByClub';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    
    
    /**
    * to update message
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function updateMessage($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'InsertMessageEmail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
    
    /**
    * to update activity details
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function updateActivitiesDetails($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'updateActivitiesDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

   
     
     
    /**
    * get Bcomp method
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getBcompMethod($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getBcompMethod';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['bcompmethods'][0])) {
            $result['movesmart']['bcompmethods'] = array($result['movesmart']['bcompmethods']);
        }

        return $result['movesmart']['bcompmethods'];
    }

    /**
     * get the Already Saved Body Composition Method for the Club 05052015
     * Author        : JahabarYusuff M
     * Created Date  : 05-May-2015
     * param type $params
     * return type.
     * @param $params
     * @return
     */
    public function getActiveBcompMethods($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'getActiveBcompMethods';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if (!isset($result['movesmart']['activeBcompMethods'][0])) {
            $result['movesmart']['activeBcompMethods'] = array($result['movesmart']['activeBcompMethods']);
        }

        return $result['movesmart']['activeBcompMethods'];
    }

    /**
     * Function TO update the Body Composition Method for the Club 05052015
     * Author        : JahabarYusuff M
     * Created Date  : 05-May-2015
     * param type $params
     * return type.
     * @param $params
     * @return array object|string
     */
    public function updateClubBcompMethods($params)
    {
        try {
            $params['mod'] = 'admin';
            $params['method'] = 'updateClubBcompMethods';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
}
