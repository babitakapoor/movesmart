<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package LanguageType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle languageType related functions.
 */
  /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class languageType extends common
{
    /**
     * Returns an json obj of get all the language List.
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getLanguageType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getLanguageType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of update language type.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function editLangType($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'editLangType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of add or update Language Permissions.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertLanguageTypePermissions($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertLanguageTypePermissions';
			
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of add or update Language Programs.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertLanguage($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertLanguage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	public function insertLanguageKey($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'insertLanguageKey';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
}
