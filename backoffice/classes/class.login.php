<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle login related functions.
 */
    /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class login extends common
{
        /**
    * Returns an json obj of authenticate the user using password(login functionality).
    * @param string $params service parameter
    *
    * @return array object object
    */
     public function authenticate($params)
     {
         try {
             $params['mod'] = 'login';
             $params['method'] = 'authenticate';
             $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
         } catch (Exception $e) {
             $result = 'Caught Exception:'.$e->getMessage();
         }

         return $result['movesmart']['data'];
     }
        /**
        * Returns an json obj of get user page permissions.
        * @param string $params service parameter
        *
        * @return array object object
        */
        public function getUserPagePermissions($params)
        {
            try {
                $params['mod'] = 'login';
                $params['method'] = 'getUserPagePermissions';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
                if (isset($result['userPermission'])) {
                    $result = $result['userPermission'];
                }
            } catch (Exception $e) {
                $result = 'Caught Exception:'.$e->getMessage();
            }

            return $result;
        }
}
