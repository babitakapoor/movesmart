<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 * @description Class to handle userType related functions.
 */
class userType extends common
{
    /**
    * Returns an json obj of users List.
    *
    * @param string $dataString service parameter
    *
    * @return array object object
    */    
    public function getUserType($dataString)
    {
        $result = parent::webServiceXMLToString(
            LOCAL_WEBSERVICE_PATH.'getUserType.php'.QN.$dataString
        );

        return json_decode($result, true);
    }
}
