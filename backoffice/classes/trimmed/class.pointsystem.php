<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package PointSystem
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle point system related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class pointsystem extends common
{
    /**
    * Returns an json obj of  get the Points to Achive
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPoints($params)
    {
        return $params;
    }

     /**
    * Returns an json obj of  get heartrate activity data from web service
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getHeartrateZoneActivity($params)
    {
        try {
            $qryStr = '?path='.$params['path'];
            $qryStr .= '&method='.$params['method'];
            $qryStr .= '&gender='.$params['gender'];
            $qryStr .= '&age='.$params['age'];
            $qryStr .= '&fitness_level='.$params['fitness_level'];
            $qryStr .= '&training_type='.$params['training_type'];
            $qryStr .= '&defl_run_load='.$params['defl_run_load'];
            $qryStr .= '&defl_run_HR='.$params['defl_run_HR'];
            $qryStr .= '&A1_Run='.$params['A1_Run'];
            $qryStr .= '&B1_Run='.$params['B1_Run'];
            $qryStr .= '&test_machine='.$params['test_machine'];
            $qryStr .= '&language='.$params['language'];
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get adjusted sportmed graph data
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getAdjustSportmedData($params)
    {
        try {
            $qryStr = '?path='.$params['path'];
            $qryStr .= '&method='.$params['method'];
            $qryStr .= '&gender='.$params['gender'];
            $qryStr .= '&age='.$params['age'];
            $qryStr .= '&weight='.$params['weight'];
            $qryStr .= '&test_machine='.$params['test_machine'];
            $qryStr .= '&start_level='.$params['start_level'];
            $qryStr .= '&defl_run_load='.$params['defl_run_load'];
            $qryStr .= '&defl_run_HR='.$params['defl_run_HR'];
            $qryStr .= '&max_run_load='.$params['max_run_load'];
            $qryStr .= '&max_run_HR='.$params['max_run_HR'];
            $qryStr .= '&intervals_count='.$params['intervals_count'];
            $qryStr .= '&spm_points='.$params['spm_points'];
            $qryStr .= '&action='.$params['action'];
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of get sport specific data from point system
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getSportSpecific($params)
    {
        try {
            $qryStr = '?path='.$params['path'];
            $qryStr .= '&method='.$params['method'];
            $qryStr .= '&defl_run_load='.$params['defl_run_load'];
            $qryStr .= '&defl_run_HR='.$params['defl_run_HR'];
            $qryStr .= '&A1_Run='.$params['A1_Run'];
            $qryStr .= '&B1_Run='.$params['B1_Run'];
            $qryStr .= '&test_machine='.$params['test_machine'];
            $qryStr .= '&weight='.$params['weight'];
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get user test sport specific data
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertSportSpecific($params)
    {
        try {
            $params['mod'] = 'pointsystem';
            $params['method'] = 'insertSportSpecific';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of add heartrate activity data
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertHeartrateZoneActivity($params)
    {
        try {
            $params['mod'] = 'pointsystem';
            $params['method'] = 'insertHeartrateZoneActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of user basic information for point system
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getUserInfoForPointSystem($params)
    {
        try {
            $params['mod'] = 'pointsystem';
            $params['method'] = 'getUserInfoForPointSystem';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get user training data to achieve
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPointsToAchieve($params)
    {
        try {
            $qryStr = '?path='.$params['path'];
            $qryStr .= '&method='.$params['method'];
            $qryStr .= '&gender='.$params['gender'];
            $qryStr .= '&age='.$params['age'];
            $qryStr .= '&fitness_level='.$params['fitnessLevel'];
            $qryStr .= '&training_type='.$params['trainingType'];
            $qryStr .= '&keepfit='.$params['keepfit'];
            $qryStr .= '&training_weeks='.$params['trainingNoOfWeeks'];
            $qryStr .= '&test_machine='.$params['testMachine'];
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of add points to achieve
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function insertPointsToachieve($params)
    {
        try {
            $params['mod'] = 'pointsystem';
            $params['method'] = 'insertPointsToachieve';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of generate start date and end date for the configured weeks
     * @param $testStartDate
     * @param $noOfweeks
     * @return array object object
     * @internal param string $params service parameter
     */
    public function getTrainingWeekDates($testStartDate, $noOfweeks)
    {
        $trainingStartDate = date('Y-m-d', strtotime($testStartDate));
        for ($i = 1; $i <= 7;++$i) {
            $datem = date('Y-m-d', strtotime($trainingStartDate.'+'.$i.' days'));
            $daten = date('N', strtotime($trainingStartDate.'+'.$i.' days'));
            if ($daten == 1) {
                $startDate = date('Y-m-d', strtotime($datem));
                break;
            }
        }

        /*Note:
            --> Now only first week start date starts from training start date
            --> Other weeks from monday - sunday 
            --> Number of weeks configured in include/define file
        */

        $weeks = $noOfweeks;
        $days = 6;
        $nextDate = $startDate;
        for ($i = 1; $i <= $weeks; ++$i) {
            $endDate = date('Y-m-d', strtotime($nextDate.'+'.$days.' days'));

            //Start 1 week from training date
            if ($i == 1) {
                $nextDate = $trainingStartDate;
            }

            $weekArray[$i] = array('start_date' => $nextDate, 'end_date' => $endDate);
            $nextDate = date('Y-m-d', strtotime($endDate.'+1 days'));
        }

        return $weekArray;
    }

    /**
    * Returns an json obj of get Points External Activity
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPointsExternalActivity($params)
    {
        try {
            $params['mod'] = 'pointsystem';
            $params['method'] = 'getPointsExternalActivity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get calculate points for activity
     * @param $param
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getActivityCalculatePoint($param)
    {
        try {
            $param['gender'] = 'M';
            $param['age'] = 25;
            $param['fitnessLevel'] = 5;
            $param['activityId'] = 2;
            $param['timeInZone'] = '10:20';
            $qryStr = '?gender='.$param['gender'].'&age='.$param['age'].'&fitness_level='.$param['fitnessLevel'];
            $qryStr .= '&activity_id='.$param['activityId'].'&time_in_zone='.$param['timeInZone'];
            $result = parent::webServiceJsonToArray(EXTERNAL_WEBSERVICE_PATH.'getActivityCalculatePoint.php'.$qryStr);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of calculate Points from PS
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function calculatePointsFromPs($params)
    {
        try {
            $qryStr = '?path=cardiotraining';
            $qryStr .= '&method=calculate_points';
            $qryStr .= '&gender='.$params['gender'];
            $qryStr .= '&age='.$params['age'];
            $qryStr .= '&fitness_level='.$params['fitness_level'];
            $qryStr .= '&training_type='.$params['training_type'];
            $qryStr .= '&activity_id='.$params['activity_id'];
            $qryStr .= '&time_in_zone='.$params['time_in_zone'];
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr);
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get points for each training devices in activity
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getCollectedPointsForEachTrainingDevices($params)
    {
        try {
            $params['mod'] = 'pointsystem';
            $params['method'] = 'getCollectedPointsForEachTrainingDevices';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['collectedDevices']['0'])) {
            $result['movesmart']['collectedDevices'] = array($result['movesmart']['collectedDevices']);
        }

        return $result['movesmart']['collectedDevices'];
    }
}
