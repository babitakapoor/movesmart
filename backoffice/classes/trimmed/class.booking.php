<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Booking
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle booking related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class booking extends common
{
    /* Get All Slot Types */
    /**
     * Returns an json obj of All Slot Types.
     * @return array object object
     * @internal param string $params service parameter
     *
     */    

    public function getSlotType()
    {
        try {
            $params['mod'] = 'booking';
            $params['method'] = 'getSlotType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['booking'][0])) {
            $result['booking'] = array($result['booking']);
        }

        return $result;
    }

    
    
    /**
    * get all bookings
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getAllBooking($params)
    {
        try {
            $params['mod'] = 'booking';
            $params['method'] = 'getAllBooking';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if (!isset($result['message'][0])) {
            $result['message'] = array($result['message']);
        }

        return $result;
    }

        
      
       
    
    /**
    * cancel booking
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function cancelBooking($params)
    {
        try {
            $params['mod'] = 'booking';
            $params['method'] = 'cancelBooking';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if (!isset($result['message'][0])) {
            $result['message'] = array($result['message']);
        }

        return $result;
    }

        
    /**
    * delete booking
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function deleteBooking($params)
    {
        try {
            $params['mod'] = 'booking';
            $params['method'] = 'deleteBooking';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        if (!isset($result['message'][0])) {
            $result['message'] = array($result['message']);
        }

        return $result;
    }
} // End Class.
