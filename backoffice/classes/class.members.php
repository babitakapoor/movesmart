<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Member
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle member related functions.
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class members extends common
{
      /**
    * Returns an json obj of get all the mambers List.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMembersList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMembersList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['memberlist'];
    }

     /**
    * Returns an json obj of get member basic details.
    * @param string $params service parameter
    *
    * @return array object object
    */



    public function getMemberBasicDetail($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberBasicDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            $result = $result['userDetails'];
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	 public function getMemberBasicDetailbyfb($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberBasicDetailbyfb';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            //$result = $result['userDetails'];
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get member by name security answers.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMemberByNameSecurityAnswer($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberByNameSecurityAnswer';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            //$result = $result['userDetails'];
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	
	 public function edit_machine_subtype()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'edit_machine_subtype';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            //$result = $result['userDetails'];
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of get member test basic details.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMemberTestDetail($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberTestDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            $result = $result['userDetails'];
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get member latest test id with any status.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMemberMaxTestId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberMaxTestId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get member FAT % from biometric latest test.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMemberFatPercentage($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberFatPercentage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of get user list if email matches the records and used in LMO pages.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getExistingEmailList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getExistingEmailList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of assign device to member.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function assignDevice($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'assignDevice';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of unlink device to member.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function unlinkDevice($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'unlinkDevice';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of get member basic details and test details.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMemberBasicAndTestDetail($params)
    {
        $basic = $this->getMemberBasicDetail($params);
        $params['testStatus'] = 'completed';
        $test = $this->getMemberTestDetail($params);
        $result = $basic;
        if (is_array($test)) {
            $result = array_merge($basic, $test);
        }

        return $result;
    }
     /**
    * Returns an json obj of get Member Details Using UserId.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getListEditMembers($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getListEditMembers';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['members'];
    }
     /**
    * Returns an json obj of Member Signup.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function signupClient($params)
    {

        try {
            $email = $params['email'];
			
            $loginreffieldid = (empty($params['loginreffieldid']) ||$params['loginreffieldid']==0 ?1:$params['loginreffieldid']);
            
			if ($email) { 
				
                $param['email'] = $params['email'];
                $chkEmailExist = $this->checkMemberEmailExist($param);
				
                if ($chkEmailExist['movesmart']['status'] == 'Error') {
                    $result = $chkEmailExist;
                } else {
                    $params['mod'] = 'members';
                    $params['method'] = 'signupClient';
                    $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
				
                }

            }
			} catch (Exception $e) {
				
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if(!isset($result)){
            $result =   array();
        }

        return $result;
			
          /*  if ($email && $loginreffieldid) {

                $result = array(
                            'movesmart' => array(
                                'status' => 'Success',
                                'status_code' => 200,
                            ),
                        );
            }*/

        
    }
      /**
    * Returns an json obj of save Security Question.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function saveSecurityQues($params)
    {
        try {
            $regid = $params['regid'];
            if ($regid) {
                $param['regid'] = $params['regid'];
                $param['question'] = $params['question'];
                $params['mod'] = 'members';
                $params['method'] = 'SecurityQuesAns';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if(!isset($result)){
            $result =   array();
        }
        return $result;
    }
    /**
    * Returns an json obj of get register user data.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getRegUserData($params)
    {
        try {
            if (isset($params['regid']) ) {
                $params['mod'] = 'members';
                $params['method'] = 'getRegUserData';
                $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if(!isset($result)){
            $result =   array();
        }
        return $result;
    }
     /**
    * Returns an json obj of save image profile.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getSaveProfileImage($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getSaveProfileImage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get status list.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getStatusList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getStatusList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['stauslist'];
    }
    */
    /**
     * Returns an json obj of get coach list by user.
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    /*public function getCoachListbyUser()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getCoachListbyUser';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['coachByUser'][0])) {
            $result['movesmart']['coachByUser'] = array($result['movesmart']['coachByUser']);
        }

        return $result['movesmart']['coachByUser'];
    }
    */
     /**
    * Returns an json obj of get city list.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getCityList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getCityList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        $citylist = array();
        if ($result['movesmart']['status_code'] == 1) {
            $citylist = $result['movesmart']['cityList'];
            if (!isset($result['movesmart']['cityList'][0])) {
                $citylist = array($result['movesmart']['cityList']);
            }
        }

        return $citylist;
    }

     /**
    * Returns an json obj of get city By id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getCityId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getCityId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['cityId'];
    }

    /**
    * Returns an json obj of Get Profession List.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getProfessionList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getProfessionList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['professionList'];
    }

       /**
    * Returns an json obj of Get Profession List By Id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getProfessionId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getProfessionId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['professionId'];
    }

       /**
    * Returns an json obj of Get Nationality List.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getNationalityList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getNationalityList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['nationalityList'];
    }
    
       /**
    * Returns an json obj of Get Activity List.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getActivityList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getActivityList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of Get Nationlity By Id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getNationalityId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getNationalityId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['nationalityId'];
    }

      /**
    * Returns an json obj of Get Company List.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getCompanyList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getCompanyList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['companyList'];
    }

    /* Member Add/Edit Details *
    public function getEditSaveMembersList($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getEditSaveMembersList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Member Add LMO *
    public function updateLmoMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateLmoMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of Get LMO Details BY Id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getLmoDetailsById($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getLmoDetailsById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

      /**
    * Returns an json obj of Get Plan Details BY Id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPlanDetailsById($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getPlanDetailsById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Function to Add LMO plans BY LMO Id *
    public function updateLmoPlans($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateLmoPlans';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

       /**
    * Returns an json obj of LMO plans by club id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPlansByClubId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getPlansByClubId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));

            if (isset($result['rows']) && !isset($result['rows'][0])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of LMO plans by LMO id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPlansByLmoId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getPlansByLmoId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));

            if (isset($result['rows']) && !isset($result['rows'][0])) {
                $result['rows'] = array($result['rows']);
            }
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /*
    public function changeTestStatus($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'changeTestStatus';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Member Medical Info Add/Edit Details *
    public function getMedicalAgainst($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMedicalAgainst';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Member Medical Info Add/Edit Details *
    public function getMedicalJoint($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMedicalJoint';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Member Medical Info Add/Edit Details * /
    public function getMedicalMuscle($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMedicalMuscle';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Member Change Password *
    public function changePassword($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'changePassword';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* Member Medical Info Add/Edit Details *
    public function getMedicalRiskFactor($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMedicalRiskFactor';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /* get List of Medical Against Datas */
    /*public function getListMedicalAgainst($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getListMedicalAgainst';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['medicalAgainst'];
    }
    */
      /**
    * Returns an json obj of get List of Medical Joint Datas .
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getListMedicalJoint($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getListMedicalJoint';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['medicalJoint'];
    }
    */
     /**
    * Returns an json obj of get List of Medical Muscles Datas .
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*
    public function getListMedicalMuscle($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getListMedicalMuscle';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['medicalMuscle'];
    }
    */
    /**
    * Returns an json obj of get List of Medical Risk Factors Datas.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*
    public function getListMedicalRiskFactor($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getListMedicalRiskFactor';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['medicalRiskFactor'];
    }
    */
    /**
    * Returns an json obj of retrive list of users / coach.
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getUserList($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getUserList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
		
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }
	
        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }

     /**
    * Returns an json obj of get members with status test to do for ergo test.
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getMembersErgoTestToDo($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getMembersErgoTestToDo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }
       /**
    * Returns an json obj of get members cardio todo list.
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function cardioTodoMembers($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'cardioTodoMembers';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }

      /**
    * Returns an json obj of get members list gained to many points this week.
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getMembersToManyPointsThisWeek($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getMembersToManyPointsThisWeek';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }

    /**
    * Returns an json obj of get members with status test to do for body composition test.
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getMembersBodycompositionTestToDo($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getMembersBodycompositionTestToDo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }
      /**
    * Returns an json obj of get members flexibility compostion todo.
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function getflexibilitycompostiontodo($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getflexibilitycompostiontodo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }
     /**
    * Returns an json obj of get members cardio test print or send.
    * @param string $param service parameter
    *
    * @return array object object
    */
    /*public function getMembersCardioPrintOrSend($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getMembersCardioPrintOrSend';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'], 'totalCount' => $result['movesmart']['totalCount']
        );
    }
    */
    /**
    * Returns an json obj of Get Member Test to analyze.
    * @param string $param service parameter
    *
    * @return array object object
    */
    /*public function getMembersTestToAnalyze($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getMembersTestToAnalyze';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['memberList'][0])) {
            $result['movesmart']['memberList'] = array($result['movesmart']['memberList']);
        }

        return array(
            'result' => $result['movesmart']['memberList'],
            'totalCount' => $result['movesmart']['totalCount']
        );
    }
    */
    /* Function used to LMO list*
    public function getLmoList($param)
    {
        try {
            $param['mod'] = 'members';
            $param['method'] = 'getLmoList';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (!isset($result['movesmart']['rows'][0])) {
            $result['movesmart']['rows'] = array($result['movesmart']['rows']);
        }

        return array('result' => $result['movesmart']['rows'], 'totalCount' => $result['movesmart']['totalCount']);
    }

     /**
    * Returns an json obj of get Member Id for Navigation.
    * @params array $params service parameter
    *
    * @return array object object
    */
    public function getMemberIdForNavigation($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberIdForNavigation';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['navigation'];
    }

     /**
    * Returns an json obj of get Employee Id for Navigation.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getEmployeeIdForNavigation($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getEmployeeIdForNavigation';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['navigation'];
    }

    /**
    * Returns an json obj of get member device details.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMapDevice($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMapDevice';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        if (is_array($result['movesmart']['mapDevice']) && !isset($result['movesmart']['mapDevice'][0])) {
            $result['movesmart']['mapDevice'] = array($result['movesmart']['mapDevice']);
        }

        return $result['movesmart']['mapDevice'];
    }

    /**
    * Returns an json obj of get start levels.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getStartLevels($params)
    {
        //Get start level latest change completed and hiding temporarly begins
        $load = array('A' => 3, 'B' => 4, 'C' => 5, 'D' => 6, 'E' => 7, 'F' => 8);
        $weight = round($params['weight']);
        $watt   =   array();
        foreach ($load as $key => $speed) {
            $value = round(((($speed * 2.98 + 4.25) * 0.9  * $weight - 320.0) / 11.35));
            $watt[$key] = $value;
        }

        return json_encode($watt);
        //Get start level latest change completed and hiding temporarly ends

        /*try {
            $qryStr = '?path=cardiotest';
            $qryStr .= '&method=get_start_levels';
            $qryStr .= '&weight='.$params['weight'];			
            $qryStr .= '&test_machine='.$params['testMachine'];
            $userId=(isset($params['userId']) && $params['userId']<>"")?$params['userId']:"0";
            $result = parent::webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr,$userId);	
        } catch (Exception $e) {
            $result = "Caught Exception:" . $e->getMessage();
        }	
        $getStartLevels = $result;
        $res = '{}';
        
        // Lines Update for fixing the bug - identify that MFP_JSON is started or not 13072015 #92782 
        $validateExe = parent::validateMFP_JSON($result);	
        if(!$validateExe){		
            if(is_array($getStartLevels)) {
                if(isset($getStartLevels['result'])) {
                    $res =  $getStartLevels['result'];
                    unset($res['json_code']);
                    $res = json_encode($res);
                }
            }
        }else{
            $res = json_encode($result);
        }	
        // End 16072015 		
        return $res;*/
    }

    /**
    * Returns an json obj of check the Person Number Exist.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getPersonNumberExist($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getPersonNumberExist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of check the Member Email Exist.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function checkMemberEmailExist($params)
    {
		
        try {
            /* Check Email Exist Status Message */
                $param['email'] = $params['email'];
            $param['mod'] = 'members';
            $param['method'] = 'checkMemberEmailExist';
            if (isset($params['userId'])) {
                $param['userId'] = $params['userId'];
                $param['username'] = $params['username'];
                $param['password'] = $params['password'];
            }
			
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
            
/* End Email Check */
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of get Id by name.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getStatusId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getStatusId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['statusId']['status_id'];
    }
    */
    /**
    * Returns an json obj of Get Age On Test.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getAgeOnTest($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getAgeOnTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['ageOnTest'];
    }
    */
     /**
    * Returns an json obj of update user details.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateUserDetail($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateUserDetail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of Check Person Id Exist.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function personIdExist($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'personIdExist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['personCount'];
    }

    /**
    * Returns an json obj of Last Test Exist.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function lastTestExist($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'lastTestExist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['userTestId'];
    }

     /**
    * Returns an json obj of primary Club Update.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function primaryClubUpdate($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'primaryClubUpdate';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }

     /**
    * Returns an json obj of check last test progress.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function checkLastTestProgress($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'checkLastTestProgress';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['testCount'];
    }

     /**
    * Returns an json obj of Get Booking for Member.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getBookingForMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getBookingForMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart'];
    }

    
     /**
    * Returns an json obj of update member security question.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateSecurityQuestion($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateSecurityQuestion';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of update forgot password details in user table.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function UpdateForgotPasswordDetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'UpdateForgotPasswordDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of check member security question and answers.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getSecurityAnswers($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getSecurityAnswers';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
     /**
    * Returns an json obj of check member security question and answers.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getSecurityAnswersMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getSecurityAnswersMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of update member password.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateMemberPasswordWithKey($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateMemberPasswordWithKey';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get member current test count.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getCurrentTestCount($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getCurrentTestCount';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
     /**
    * Returns an json obj of get test summery by member.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getTestSummaryByMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getTestSummaryByMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get user datalevel by descorder.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getUserDataalevelbydesc($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getUserDataalevelbydesc';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get user datalevel by ascorder.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getUserDataalevelbyasc($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getUserDataalevelbyasc';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /**
    * Returns an json obj of get user fat percentage by DESC.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*
    public function getFatpercentLevelbydesc($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getFatpercentLevelbydesc';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /**
    * Returns an json obj of get user fat percentage by ASC.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getFatpercentLevelbyasc($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getFatpercentLevelbyasc';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of update personal information by user.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updatePersonalInfo($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updatePersonalInfo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of update activity member.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateActivityMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateActivityMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of update member medical info.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateMemberMedicalInfo($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateMemberMedicalInfo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of list machine data.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function listMachinesData($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'listMachinesData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of  machine data.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function machinesData($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'machinesData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  get list if device data.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getListDeviceData($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getListDeviceData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  get list if device data By Id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getDeviceById($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getDeviceById';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  save device  data.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function saveDeviceData($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'saveDeviceData';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  getting last test By Id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getLastTestID($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getLastTestID';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
     /**
    * Returns an json obj of  getting last test result.
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function getlastTestResult($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getlastTestResult';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    */
    /**
     * Returns an json obj of  get brand List.
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getbrandslist()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getbrandslist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of  get group List.
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getgrouplist()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getgrouplist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of  get type List.
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function gettypelist()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'gettypelist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of  get subtype List.
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getsubtypelist()
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getsubtypelist';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of  save new machine data.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function newmachinedetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'newmachinedetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get item value from test id.
     * @param $test_item
     * @param $data
     * @return array object object
     * @internal param string $params service parameter
     */
    public function getItemValueFromTestItem($test_item, $data)
    {
        if (count($data) > 0) {
            foreach ($data as $eachitem) {
                if ($eachitem['test_item'] == $test_item) {
                    return isset($eachitem['value']) ? $eachitem['value'] : 0;
                }
            }
        }

        return 0;
    }
     /**
    * Returns an json obj of get last culb details.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getlastclubiddetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getlastclubiddetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
     * Returns an json obj of get date format.
     * @param $data
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getFormatDate($data)
    {
        $dateobj = new DateTime($data);
        $date = $dateobj->format('d-m-Y');

        return $date;
    }

    /**
     * Returns an json obj of get test level.
     * @param $key
     * @return array object object
     * @internal param string $params service parameter
     *
     */
    public function getTestLevel($key)
    {
        $value = '';
        if ($key == 'A') {
            $value = 'A = 40 Watt';
        }
        if ($key == 'B') {
            $value = 'B = 55 Watt';
        }
        if ($key == 'C') {
            $value = 'C = 71 Watt';
        }
        if ($key == 'D') {
            $value = 'D = 86 Watt';
        }
        if ($key == 'E') {
            $value = 'E = 101 Watt';
        }
        if ($key == 'F') {
            $value = 'F = 117 Watt';
        }

        return $value;
    }
    /**
    * Returns an json obj of get last test detail.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getLastTestDetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getLastTestDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of Test list To be Analysed.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMemberTestTobeAnalysed($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMemberTestTobeAnalysed';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get all the test details for the club and date .
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMembersTestResultdataForClubandDate($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMembersTestResultdataForClubandDate';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get  cardio test by id .
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getCardioTestByTestID($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getCardioTestByTestID';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of Cardio Test Memebers List on Monitor Page.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function CardiotestDetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'CardiotestDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of new machine details by id.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function editNewMachinedetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'editNewMachinedetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of multiple delete machine details in grid.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function delNewMachineDetails($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'delNewMachineDetails';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of multiple delete machine details in list.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function deletemachinedetaislbyId($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'deletemachinedetaislbyId';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of GET Heartrate Monitor List.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getHeartrateDevices($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getHeartrateDevices';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of Get all mapped device info.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getMappedDeviceInfo($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getMappedDeviceInfo';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    
     /**
    * Returns an json obj of Update HeartRate On every Pulse.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateHeartRateOnTest($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateHeartRateOnTest';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of print/Mail AnalysusReport.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function MailAnalysisReportPDF($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'MailAnalysisReportPDF';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of Machine upload Image.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getSaveMachineImage($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getSaveMachineImage';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

     /**
    * Returns an json obj of Get User Detail By User ID.
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getUserDetailByID($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getUserDetailByID';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of get client personal info
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getClientDetailByID($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getClientDetailByID';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['members'];
    }
      /**
    * Returns an json obj of get client activity By id
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getClientActivityByID($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getClientActivityByID';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result['movesmart']['members'];
    }
     /**
    * Returns an json obj of get week training program
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getweekTrainProgram($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getweekTrainProgram';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of save new program point
    * @param string $params service parameter
    *
    * @return array object object
    */
    /*public function saveNewProgramPoint($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'saveNewProgramPoint';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/
    /**
    * Returns an json obj of add coach member
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function addCoachMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'addCoachMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of mapped club and member
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function mapClubAndMember($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'mapClubAndMember';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of update mapped member device
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function updateMappedMemberDevice($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'updateMappedMemberDevice';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of cardio test and analysis
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getfitlevelforagesexspeed($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getfitlevelforagesexspeed';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    /**
    * Returns an json obj of get 12 week training plan for age sex fitlevel
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function get12weekstraninigplanfor_age_sex_fitlevel($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'get12weekstraninigplanfor_age_sex_fitlevel';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
      /**
    * Returns an json obj of get point activity for testid
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function getpointsper_activity_for_testid_iantw($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'getpointsper_activity_for_testid_iantw';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of post point achieve data
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function postpointachieveddata($params)
    {
        try {
            $params['mod'] = 'members';
            $params['method'] = 'postpointachieveddata';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
     /**
    * Returns an json obj of insert message email
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function InsertMessageEmail($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'InsertMessageEmail';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    
}
