<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Paginator
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle paginator related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class paginator
{
    public $itemsPerPage;
    public $numPages;
    public $offset;
    public $page;
    protected $offsetArray;
    protected $queryString;
    protected $return;

    /* The common values are here. Filter box values, Pagination box values etc */
    public function __construct($offsetArray = PAGINATION_LIST, $offsetStart = PAGINATION_SHOW_PER_PAGE)
    {
        // Defining the list values in define page
        $offsetArray = explode(',', $offsetArray);

        $this->numPages = $offsetStart; /*Pagination box limit*/

        $this->offsetArray = $offsetArray;
        $this->offsetStart = $offsetStart;
        $this->itemsPerPage = (isset($_GET['offset'])) ? $_GET['offset'] : $this->numPages;
        $this->default_offset = $this->offsetStart;
        if (!is_numeric($this->itemsPerPage) || $this->itemsPerPage <= 0) {
            $this->itemsPerPage = $this->numPages;
        }

        /*
        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] != '') {
            $this->offset = $_REQUEST['offset'];
        } else {
            $this->offset = $this->numPages;
        }
        */
        $this->offset = (
            isset($_REQUEST['offset'])
            && $_REQUEST['offset'] != ''
        ) ? $_REQUEST['offset'] : $this->numPages;
        $this->page = (
            isset($_REQUEST['page'])
            && $_REQUEST['page'] != ''
        ) ? $_REQUEST['page'] : 1;
        /*
        if (isset($_REQUEST['page']) && $_REQUEST['page'] != '') {
            $this->page = $_REQUEST['page'];
        } else {
            $this->page = 1;
        }
        */
        $this->pagesStart = (($this->page - 1) * $this->offset);
        $this->totalPage = $this->page * $this->offset;
        $this->pagesStart = 0;

        $this->getUrlLink = (
                !isset($this->getUrlLink)
                && isset($_SESSION['getUrlLink'])
        ) ? $_SESSION['getUrlLink'] : '';

        /*
        if (!isset($this->getUrlLink)) {
            $this->getUrlLink = isset($_SESSION['getUrlLink']) ? $_SESSION['getUrlLink'] : '';
        }*/
        if (!isset($_SESSION['getUrlLink'])) {
            $this->getUrlLink = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
        }
    }
    /**
     * Returns an json obj of  Get current link which is not consider ajax page
     * @param string $param service parameter
     *
     * @return array object object
     */
    public function getUrlLink($param)
    {
        $this->getUrlLink = isset($_SESSION['getUrlLink']) ? $_SESSION['getUrlLink'] : '';
        if ($param == 1) {
            $_SESSION['getUrlLink'] = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
            $this->getUrlLink = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
        }
        $this->formatPageAndOffset();

        return $this->getUrlLink;
    }
    /**
     * Returns an json obj of  format page anf offset
     *
     * @return array object object
     */
    public function formatPageAndOffset()
    {
        $queryString = preg_replace('/[&&]+/', '&', $this->getUrlLink);
        $args = explode('&', $queryString);
        $count = 0;
        $queryStringUpdated = '';
        foreach ($args as $arg) {
            $keyVal = explode('=', $arg);
            $concatString = '';
            if (count($args) > 1) {
                $concatString = '&';
            }

            if (
                ($keyVal[0] != 'page')
                && ($keyVal[0] != 'offset')
                && ($keyVal[0] != 'searchType' || !isset($_SESSION['searchType']))
                && ($keyVal[0] != 'searchValue' || !isset($_SESSION['searchValue']))
            ) {
                $queryStringUpdated .= $arg.$concatString;
            }
            $count = $count + 1;
        }
        $queryStringUpdated = preg_replace('/[&&]+/', '&', $queryStringUpdated);
        $queryStringUpdated = rtrim($queryStringUpdated, '&');

        //Construction search type and search value
        if (isset($_SESSION['searchValue'])){
            $queryStringUpdated .=  '&searchValue='.$_SESSION['searchValue'];
        }
        if (isset($_SESSION['searchType'])){
            $queryStringUpdated .=  '&searchType='.$_SESSION['searchType'];
        }
        $this->getUrlLink = $queryStringUpdated;
    }
    /**
     * Returns an json obj of  display iems pagination
     * @param string $arrayList pack of array values to be displayed
     * @param array $arrayColumnList column values  to be displayed
     * @param string $tagList loop can contain table row or div elements
     *
     * @return array object object
     */
    public function displayItemsPagination($arrayList, $arrayColumnList, $tagList)
    {
        $totalMemberList = count($arrayList);
        $countMem = 0;

        for ($tMem = $this->pagesStart;$tMem <= $this->totalPage;++$tMem) {
            $startTag = $tagList['startTag'];
            $midTagOpen = $tagList['midTagOpen'];
            $midTagClose = $tagList['midTagClose'];
            $endTag = $tagList['endTag'];
            $noResultText   =   isset($LANG['noResult']) ? $LANG['noResult']:'';
            if (empty($arrayList[0]) && count($arrayList) == 1) {
                $totalMemberList = 0;
                echo $tagList['startTag'].'<td colspan="20">'.$noResultText.'</td>'.$tagList['endTag'];
            } elseif ($totalMemberList == 0) {
                echo $tagList['startTag'].'<td colspan="20">'.$noResultText.'</td>'.$tagList['endTag'];
            }
            if ($countMem == $this->offset
                || $tMem >= $totalMemberList
                || $tMem < 0 || (empty($arrayList[0])
                    && count($arrayList) == 1)
            ) {
                break;
                //echo $tagList['startTag'].'<td colspan="20">'.$LANG['noResult'].'</td>'.$tagList['endTag'];
            }

            $arrayColumnListCount = 1;
            foreach ($arrayColumnList as $keyarrayColumnList => $memArray) {

                /*Tags placing here.*/
                if ($arrayColumnListCount == 1) {
                    $frontTag = $startTag;
                    $backTag = '';
                } elseif ($arrayColumnListCount == count($arrayColumnList)) {
                    $frontTag = '';
                    $backTag = $endTag;
                } else {
                    $frontTag = '';
                    $backTag = '';
                }

                /*If array need any static content, we can give key in string.
                    So it can directly place that value.
                ex: array('specValues2'=>'Delete','specValues3'=>'Edit').*/
                if (!is_int($keyarrayColumnList) && is_array($arrayList)) {
                    $arrayValue = $memArray;
                } else {
                    if (isset($arrayList[$tMem][$memArray])) {
                        $arrayValue = $arrayList[$tMem][$memArray];
                    } else {
                        echo $arrayValue = '';
                    }
                }

                if (strpos($memArray, '$') !== false) {
                    $memArrays = explode('$', $memArray);

                    for ($hCount = 0;$hCount < count($memArrays);++$hCount) {
                        if ($hCount % 2) {
                            if ($memArrays[$hCount] != '') {
                                if (isset($_SESSION['createSessionArray'])) {
                                    if ($_SESSION['createSessionArray'] == '') {
                                        $createSessionArray = $memArray;
                                    } else {
                                        $createSessionArray = $_SESSION['createSessionArray'];
                                    }
                                } else {
                                    $createSessionArray = $memArray;
                                }

                                if (isset($arrayList[$tMem][$memArrays[$hCount]])) {
                                    //$memArrays[$hCount]."--".$arrayList[$tMem][$memArrays[$hCount]];
                                        $_SESSION['createSessionArray'] = str_replace(
                                                '$'.$memArrays[$hCount].'$',
                                                $arrayList[$tMem][$memArrays[$hCount]],
                                            $createSessionArray
                                        );
                                } else {
                                    $_SESSION['createSessionArray'] = $createSessionArray;
                                }
                                $arrayValue = $_SESSION['createSessionArray'];
                            }
                        }
                    }
                    $_SESSION['createSessionArray'] = '';
                }

                //if(isset($arrayValue)){
                // $sectionDisplay=$frontTag.$midTagOpen.$arrayValue.$midTagClose.$backTag;
                // } else {
                // $sectionDisplay="";
                // }
                /*
                if (strpos($filename, $match)===false) {

                }*/
                $iconFound = strpos($arrayValue, 'icon');
                if ($iconFound == true) {
                    $frontTag = '';
                    $midTagOpen = "<td class='txt-center'>";
                }
                $sectionDisplay = $frontTag.$midTagOpen.$arrayValue.$midTagClose.$backTag;

                //$frontTag="<td class='txt-center'>"; $backTag="</td>";

                $arrayColumnListCount = $arrayColumnListCount + 1;

                echo $sectionDisplay;
            }
            $countMem = $countMem + 1;
        }
    }

    /**
     * Returns an json obj of  display items Pagination By Query
     * @param $paramQuery
     * @param string $arrayColumnList column values  to be displayed
     * @param string $tagList loop can contain table row or div elements
     * @return array object object
     * @internal param string $arrayList pack of array values to be displayed
     */
    /*public function displayItemsPaginationByQuery($paramQuery, $arrayColumnList, $tagList)
    {
        $limitQuery = mysqli_query($paramQuery);
        $countMem = 0;
        $tMem = 0;
        $totalMemberList = count($limitQuery);
        foreach ($limitQuery as $limitQuerys) {
            $startTag = $tagList['startTag'];
            $midTagOpen = $tagList['midTagOpen'];
            $midTagClose = $tagList['midTagClose'];
            $endTag = $tagList['endTag'];
            if ($countMem == $this->offset || $tMem >= $totalMemberList || $tMem < 0) {
                break;
            }
            $arrayColumnListCount = 1;
            if( is_array($arrayColumnList)
                and (count($arrayColumnList)>0)
            ) {
                foreach ($arrayColumnList as $keyarrayColumnList => $memArray) {
                    if ($arrayColumnListCount == 1) {
                        $frontTag = $startTag;
                        $backTag = '';
                    } elseif ($arrayColumnListCount == count($arrayColumnList)) {
                        $frontTag = '';
                        $backTag = $endTag;
                    } else {
                        $frontTag = '';
                        $backTag = '';
                    }

                    if (!is_int($keyarrayColumnList)) {
                        $arrayValue = $memArray;
                    } else {
                        $arrayValue = $limitQuerys[$tMem][$memArray];
                    }

                    echo $frontTag . $midTagOpen . $arrayValue . $midTagClose . $backTag;
                    $arrayColumnListCount = $arrayColumnListCount + 1;

                }
            }
            $countMem = $countMem + 1;
            $tMem = $tMem + 1;
        }
    }
    */
    /**
     * Returns an json obj of  Pagination Area
     * @param string $totalDynamic  list of  array elements
     *
     * @return array object object
     */
    public function displayPagination($totalDynamic)
    {
        $paginate = '';
        if ($totalDynamic <= $this->offset && $totalDynamic <= $this->offsetStart) {
            return null;
        }
        $backPage   =   1;
        $frontPage  =   1;
        $option = '';
        if (!$totalDynamic <= $this->offsetStart) {
            $paginate .= '<div class="pagination">';

                /* Filter Box */
                $items = '';
            foreach ($this->offsetArray as $offset_opt) {
                if ($offset_opt == $this->itemsPerPage) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }
                $items .= "<option ".$selected." value=\"$offset_opt\">$offset_opt</option>\n";
            }
            $paginate .= "<div class=\"page-view\">
                            <form>
                                <label></label>
                                <div class=\"select-custom\">
                                    <select id=\"view\" class=\"paginate\" 
                                        onchange=\"window.location='$this->getUrlLink&amp;' +
                                         'page=1&amp;' +
                                          'offset='+this[this.selectedIndex].value+'';return false\">
                                        $items
                                    </select>
                                 </div>
                            </form>
                           </div>\n";

                /* Choosing Pagination Number Box */

            //$totalPages = $this->page + 2;

            $navPagination = PAGINATION_NAVIGATION_VALUE;
            if ((floor($this->page / $navPagination).'0') == '00'){
                $startPag =  1;
            }else{
                $startPag =  floor($this->page / $navPagination).'1';
            }
            if (is_float($this->page / $navPagination)){
                $startPage =  $startPag;
            }else{
                $startPage =  ($this->page / $navPagination).'0';
            }
            $option = '';
            for ($pageI = $startPage;$pageI <= ($startPage + 9);++$pageI) {
                if ($pageI == $this->page) {
                    $current = 'current-page';
                } else {
                    $current = '';
                }
                if ($pageI > ceil($totalDynamic / $this->offset)) {
                } else {
                    $option .= "<a class=\"paginate btn-link $current\" 
                                onclick=\"window.location='$this->getUrlLink&amp;page=$pageI&amp;' +
                                 'offset=$this->offset'\">
                                $pageI
                    </a>";
                }
            }

            if ($this->page > 1) {
                $backPage = $this->page - 1;
            } else {
                $backPage = 1;
            }
            $frontPage = $this->page + 1;
        }

            /* Checking whether the total number of quantity is greater than current page. */
            //echo "totalMemberList".$totalDynamic;
            //echo "Offset".$pageI*$this->offset;
            //Calculation Backup : ($pageI*$this->offset)>$totalDynamic
            $outOfValue = ceil($totalDynamic / $this->offset);
        if ($this->page == $outOfValue) {
            $displayStatus = 'disabledColor noCursor';
            $disableAnchor = 'this.preventDefault();';
        } else {
            $displayStatus = '';
            $disableAnchor = '';
        }
        $displayState = '';
        $disableAnc = '';
        if ($this->page == 1) {
            $displayState = 'disabledColor noCursor';
            $disableAnc = 'this.preventDefault();';
        }
        $onclick=   "onclick=\"".$disableAnc."
            window.location='$this->getUrlLink&amp;page=1&amp;offset=$this->offset'\"";//Page-1
        $onclickback="onclick=\"$disableAnc
                        window.location='$this->getUrlLink&amp;page=$backPage&amp;offset=$this->offset'\"";
        $onclickfwd="onclick=\" $disableAnchor
                        window . location = '$this->getUrlLink&amp;page=$frontPage&amp;offset=$this->offset'\"";
        $onclickLast="onclick=\"$disableAnchor
                        window . location = '$this->getUrlLink&amp;page=$outOfValue&amp;offset=$this->offset'\"";

        $clsgofirst   =   "class=\"paginate btn-link icon-arr-first $displayState\"";
        $clsgoleft =   "class=\"paginate btn-link icon-arr-first $displayState\"";
        //$clsgoright =   "class=\"paginate btn - link icon - arr - right $displayStatus\"";
		$clsgoright =   "class=\"paginate btn-link icon-arr-last $displayStatus\"";
        $clsgolast =   "class=\"paginate btn-link icon-arr-last $displayStatus\"";

        /**/
        $htmlpagelist   =   file_get_contents("../templates/pagination/pagelist.cust");
        $pharr  =   array(
            '{{CLSGOFIRST}}',
            '{{CLICKGOFIRST}}',
            '{{CLSGOBACK}}',
            '{{CLICKGOBACK}}',
            '{{CLSGOFWD}}',
            '{{CLICKGOFWD}}',
            '{{CLSGOLAST}}',
            '{{CLICKGOLAST}}',
            '{{OPTION}}',
            '{{OUTOFVALUE}}'
        );
        $reparr =   array(
            $clsgofirst,$onclick,$clsgoleft,$onclickback,
            $clsgoright,$onclickfwd,$clsgolast,$onclickLast,$option,$outOfValue
        );
        $paginate .=    str_replace($pharr,$reparr,$htmlpagelist);/**/
        /*

        $paginate .= '<div class="page-list">
                        <a '.$clsgofirst.' '. $onclick.' >&nbsp;</a>
                        <a '.$clsgoleft.' '.$onclickback.'>'.$option.'</a>
                        <a '.$clsgoright.' '.$onclickfwd.'>&nbsp;</a>
                        <a '.$clsgolast.' '.$onclickLast.'>&nbsp;</a>
                        <span> of&nbsp;&nbsp;&nbsp; '.$outOfValue.'</span>
            </div>';
        */
        $paginate .= '</div>';
        unset($_SESSION['searchType']);
        unset($_SESSION['searchValue']);

        if ($totalDynamic == 0) {
            $paginate = '';
        }

        return $paginate;
    }
    /**
     * Returns an json obj of  display pagination page
     *
     * @return array object object
     */
    public function display_pages()
    {
        return $this->return;
    }
}