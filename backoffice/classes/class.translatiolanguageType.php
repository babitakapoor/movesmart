<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package TranslationLangType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle translation language Type related functions.
 */
class translatiolanguageType extends common
{
    /*This function used to get all the translationlanguage List.*/
    /*public function getTranslationLanguageType($dataString){
        $params['mod'] = 'settings';
        $params['method'] = 'getTranslationLanguageType';
        $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        return json_decode($this->$result,true);
    }*/
    public function getTranslationLanguageType()
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'getTranslationLanguageType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
    public function editTransType($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'editTransType';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }
	/**********************************
	For auto search custom option in the translation page
	********************************/
	/* public function gettranslationbyautosearch($params)
    {
        try {
            $params['mod'] = 'settings';
            $params['method'] = 'gettranslationbyautosearch';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    } */
	
	
	
}
