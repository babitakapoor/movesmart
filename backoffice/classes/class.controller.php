<?php
/**
 * PHP version 5.

 * @category Classes

 * @package Controller

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @link http://movesmart.company/admin/

 * @description Class to handle controller related functions.

 */

require_once BASE_DIR.'classes'.DS.'class.log.php';
require_once BASE_DIR.'classes'.DS.'class.common.php';
require_once BASE_DIR.'classes'.DS.'class.jsmin.php';
require_once BASE_DIR.'classes'.DS.'class.paginator.php';
require_once BASE_DIR.'classes'.DS.'class.login.php';
require_once BASE_DIR.'classes'.DS.'class.test.php';
require_once BASE_DIR.'classes'.DS.'class.members.php';
require_once BASE_DIR.'classes'.DS.'class.message.php';
require_once BASE_DIR.'classes'.DS.'class.coach.php';
require_once BASE_DIR.'classes'.DS.'class.club.php';
require_once BASE_DIR.'classes'.DS.'class.settings.php';
require_once BASE_DIR.'classes'.DS.'class.admin.php';
require_once BASE_DIR.'classes'.DS.'class.testingandmeasuring.php';
require_once BASE_DIR.'classes'.DS.'class.strength.php';
require_once BASE_DIR.'classes'.DS.'class.languageType.php'; // added by PK;
require_once BASE_DIR.'classes'.DS.'class.translatiolanguageType.php'; // added by PK;
require_once BASE_DIR.'classes'.DS.'class.questionaries.php'; // MK Added -  Questionaries;
require_once BASE_DIR.'classes'.DS.'class.quickadd.php'; // Added by saranya
require_once BASE_DIR.'classes'.DS.'class.ios.php';
require_once BASE_DIR.'classes'.DS.'class.machine.php';
require_once BASE_DIR.'classes'.DS.'class.notifications.php';
class controller
{
    public $minjs = '';
    public $common = '';
    public $paginator = '';
    public $coach = '';
    public $login = '';
    public $test = '';
    public $members = '';
    public $club = '';
    public $settings = '';
    public $admin = '';
    public $testingAndMeasuring = '';
    public $strength = '';
    public $languageType = ''; // add by PK;
    public $translatiolanguageType = '';// add by PK;
    public $quickAdd = '';   // Added by saranya
    public $ios = '';
    public $message = '';
    public $questionaries = '';

    /*XX
    public $minjs ='';
    public $common ='';
    --public $paginator ='';
    --public $coach ='';
    --public $members ='';
    public $company ='';
    public $center ='';
    public $cron ='';
    --public $login ='';
    --public $ios ='';
    --public $test ='';
    --public $club ='';
    public $imageDesign = '';
    public $pointSystem = '';
    
    --public $settings = '';
    public $import = '';
    public $log = '';
    public $database = '';
    --public $admin = '';
    --public $testingAndMeasuring = '';
    --public $strength = '';
    public $message = '';
    public $booking = '';
    --public $languageType = ''; // add by PK;
    --public $translatiolanguageType='';// add by PK;
    */

    /*We are including here the basic content like header, body,
        footer and so on in construct method,
        This method automatically calls once the class initialized.*/
    /**
     * controller constructor.
     */
    public function __construct()
    {
        global $isCronVarApi;
        $currentDir = CURRENT_DIR;
        if ($this->isValidUser()) {
            $this->InitializationObject();

            if (strstr(CURRENT_DIR, 'member')) {
                $currentDir = str_replace('member', 'coach', CURRENT_DIR);
            }

            if (isset($_SESSION['language'])) {
                if ($_SESSION['language'] == 'EN') {
                    include_once '../lang/en.php';
                }
                if ($_SESSION['language'] == 'FR') {
                    include_once '../lang/fr.php';
                }
                if ($_SESSION['language'] == 'NL') {
                    include_once '../lang/nl.php';
                }
            } else {
                include_once '../lang/en.php';
            }

            $flagvar = 1;
                // This is added not to include the Header and footer for the pages like (cron,Ajax).
            if (isset($isCronVarApi)) {
                $flagvar = 0;
            }

            if ($flagvar == 1) {

                /* Check page permission begins */

                //Please note : This is common functionality used to check page user access.
                //Future included code should be tested properly
                //  and do not affect any of the functionality in this system

                $checkAccess = 1; // if set to 1 it checks user page permission

                //Setting default sessions for page access
                $_SESSION['page_view'] = 0;
                $_SESSION['page_add'] = 0;
                $_SESSION['page_edit'] = 0;
                $_SESSION['page_delete'] = 0;

                //Remove user access specified users
                if (isset($_SESSION['user']['usertype_id'])) {
                    $siteUserTypeId = $_SESSION['user']['usertype_id'];
                    $noCheckAccessArr = array();
                    if (in_array($siteUserTypeId, $noCheckAccessArr)) {
                        $checkAccess = 0;
                        $_SESSION['page_view'] = 1;
                        $_SESSION['page_add'] = 1;
                        $_SESSION['page_edit'] = 1;
                        $_SESSION['page_delete'] = 1;
                    }
                }
                if ((isset($_REQUEST['p'])) && ($_REQUEST['p'] == 'login')) {
                    $checkAccess = 0;
                }
                //XX var_dump(USER_RIGHTS_CHECK_FOR_APPLICATION);
                // If value set to zero no user rights applied in all the pages in this application
                if (USER_RIGHTS_CHECK_FOR_APPLICATION == 0) {
                    $checkAccess = 0;
                    //$noAccessFlag = 0;
                    $_SESSION['page_view'] = 1;
                    $_SESSION['page_add'] = 1;
                    $_SESSION['page_edit'] = 1;
                    $_SESSION['page_delete'] = 1;
                }
                /* Check page permission ends */

                if ($checkAccess == 1) {

                    //Get all the permission from page permission table
                    if ((isset($_REQUEST['p']))) {
                        $paramPg['fileName'] = $_REQUEST['p'];
                    } else {
                        $paramPg['fileName'] = '';
                    }
                    if ((isset($_SESSION['user']['usertype_id']))) {
                        $paramPg['userTypeId'] = $_SESSION['user']['usertype_id'];
                    } else {
                        $paramPg['userTypeId'] = '';
                    }

                    $perm = $this->login->getUserPagePermissions($paramPg);
                    $noAccessFlag = 0;

                    //Setting sessions for page access
                    if (is_array($perm) && isset($perm['view_f'])) {
                        $_SESSION['page_view'] = $perm['view_f'];
                        $_SESSION['page_add'] = $perm['add_f'];
                        $_SESSION['page_edit'] = $perm['edit_f'];
                        $_SESSION['page_delete'] = $perm['delete_f'];
                    }

                    //This checks the requested page view access only
                    if ($_SESSION['page_view'] != 1) {
                        $noAccessFlag = 1;
                    }

                    //If member, do not allow to use other id
                    if ($paramPg['userTypeId'] == '3'
                        && isset($_REQUEST['id'])
                        && $_REQUEST['id'] != $_SESSION['user']['user_id']
                    ) {
                        $noAccessFlag = 1;
                    }

                    if ($noAccessFlag == 1 && ((!isset($_REQUEST['p'])) || ($_REQUEST['p'] != 'login'))) {
                        $_REQUEST['p'] = 'noAccess';
                    }
                }

                //including header file
                if (isset($_REQUEST['p']) && $_REQUEST['p'] != 'login') {
                    include_once HEADER_FILE_PATH;
                }
                //including dynamic body content file
                if (isset($_REQUEST['p'])) {
                    if(isset($_REQUEST['notification_tab']))
					{
						$filename = $_REQUEST['p'].'_'.$_REQUEST['notification_tab'];
					}
					else
					{
						$filename = $_REQUEST['p'];
					}
					if (file_exists($currentDir . '/' . $filename . '.php')) 
					{
                        $innerPage = $currentDir . '/' . $filename . '.php';
                    } else {
                        $innerPage = BASE_DIR . '/error.php';
                    }
                } else {
                    $innerPage = BASE_DIR . '/error.php';
                }
                if( file_exists( $innerPage ) ) {
                    include_once $innerPage;
                }

                if ($_REQUEST['p'] != 'login') {
                    //including footer file
                    if( file_exists( FOOTER_FILE_PATH ) ) {
                        include_once FOOTER_FILE_PATH;
                    }
                }
            } else {
				if (isset($_REQUEST['p'])) {
					if (file_exists($currentDir.'/'.$_REQUEST['p'].'.php')){
						$innerPage =  $currentDir.$_REQUEST['p'].'.php';
					}else{
						$innerPage =  BASE_DIR.'/error.php';
					}
				}
				if (isset($innerPage)) {
                if( file_exists( $innerPage ) ) {
                    include_once $innerPage;
                }
				}
            }
        } else {
            // if the user is not valid user than redirect to Loginpage.
                //echo "--".$currentDir.'movesmart'.'/login.php'."---";
                    include_once $currentDir.'/login.php';
                    //include_once($currentDir.'movesmart'.'/login.php');
        }
    }

    /*The InitializationObject() function used to declare or grant the permission to each type login users.*/
    public function InitializationObject()
    {
        // InitializationObject the object based on the Modules.	
        $this->minjs = new JSMin('');
        $this->common = new common();
        $this->paginator = new paginator();
        $this->coach = new coach();
        $this->login = new login();
        $this->test = new test();
        $this->members = new members();
        $this->message = new message();
        $this->club = new club();
        $this->settings = new settings();
        $this->admin = new admin();
        $this->testingAndMeasuring = new testingAndMeasuring();
        $this->strength = new strength();
        $this->languageType = new languageType(); // add by PK;
        $this->translatiolanguageType = new translatiolanguageType(); // add by PK;
        $this->questionaries = new questionaries(); // MK Added - Questionaries;
        $this->quickAdd = new quickadd();    // Added by saranya
        $this->ios = new ios();
		$this->machine = new machine();
		$this->notifications = new notifications();
        /*XX
        --$this->minjs = new JSMin('');
        --$this->common = new common();
        --$this->paginator = new paginator();
        --$this->coach = new coach();
        --$this->members = new members();
        $this->company = new company();
        $this->center = new center();
        $this->cron = new cron();		
        --$this->login = new login();
        --$this->ios = new ios();
        --$this->test = new test();
        --$this->club = new club();
        $this->imageDesign = new ImageDesign();
        $this->pointSystem = new pointsystem();
        $this->quickAdd = new quickadd();
        --$this->settings = new settings();
        $this->import = new import();
        $this->log = new log();
        --$this->admin = new admin();		
        --$this->testingAndMeasuring =  new testingAndMeasuring();
        --$this->strength = new strength();
        $this->message = new message();
        $this->booking = new booking();
        --$this->languageType = new languageType();
        --$this->translatiolanguageType = new translatiolanguageType();
        */
    }
     /*The isValidUser() used to check whether the user or admin logged in.*/
    public function isValidUser()
    {
        // Check the Login User and return true if the session is active.
        // If the page is from ajax OR from cron OR from ios, we don't need the login			 
        if (MODULE_NAME == 'cron' || MODULE_NAME == 'ios' || MODULE_NAME == 'ajax' || MODULE_NAME == 'import') {
            return true;
        }
        if (isset($_SESSION['user'])) {
            return true;
        } else {
            return false;
        }
    }
    /*XX
    //Function to check whether MFP JSON service is running or not
    public function checkMFPJsonRunning() {
        $qryStr = '?path=cardiotest';
        $qryStr .= '&method=get_start_levels';
        $qryStr .= '&weight=80';			
        $qryStr .= '&test_machine=1';
        $userId = 0;
        $result = $this->common->webServiceJsonToArray(POINT_SYSTEM_PATH.$qryStr,$userId);
        if($result['system_error'] == 1) {
            echo json_encode($result);
            exit;
        }
    }
    XX*/
}
