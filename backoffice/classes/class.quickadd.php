<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package QuickPopup
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle quick popup related functions.
 */
 /**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class quickadd extends common
{
    public $mod = 'quickadd';

     /**
    * Returns an json obj of  insert city
    * @param string $param service parameter
    *
    * @return array object object
    */
    public function insertCity($param)
    {
        try {
            $param['mod'] = $this->mod;
            $param['method'] = 'insertCity';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }

    /**
    * Returns an json obj of  Add/Edit the company, use can add company while creating the user
    * @param string $param service parameter
    *
    * @return array object object
    */
    /*public function insertCompany($param)
    {
        try {
            $param['mod'] = $this->mod;
            $param['method'] = 'insertCompany';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/

     /**
    * Returns an json obj of  Add/Edit the profession, user can add profession while creating the user
    * @param string $param service parameter
    *
    * @return array object object
    */
    /*public function insertProfession($param)
    {
        try {
            $param['mod'] = $this->mod;
            $param['method'] = 'insertProfession';
            $result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($param));
        } catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }

        return $result;
    }*/
}
