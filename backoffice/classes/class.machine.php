<?php
/**
 * PHP version 5.
 
 * @category Classes
 
 * @package Strength
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Class to handle strength related functions.
 */
/**
 * Class to handle userType related functions.
 
 * @category Classes
 
 * @package UserType
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/

 */
class machine extends common
{
      /**
    * Returns an json obj of  calculate BMI
    * @param string $params service parameter
    *
    * @return array object object
    */
    public function addMachine($params = array())
	{
		try {
			///echo"ggdf";die;
			$params['mod'] = 'machine';
            $params['method'] = 'addMachine';
			
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function getMachineList($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'getMachineList';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
		
	public function setMachinePairing($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'setMachinePairing';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function getClientReadyForMachine($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'getClientReadyForMachine';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function sendTrainingData($params = array())
	{
		try {
			
			$params['mod'] = 'machine';
            $params['method'] = 'sendTrainingData';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	public function registerClientHRDevice($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'registerClientHRDevice';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	public function getCurrentTrainingStatus($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'getCurrentTrainingStatus';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function validateCoach($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'validateCoach';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function searchClientByFirstName($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'searchClientByFirstName';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function startTraining($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'startTraining';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function updateTrainingCredits($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'updateTrainingCredits';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
	public function updateTrainingStatus($params = array())
	{
		try {
			$params['mod'] = 'machine';
            $params['method'] = 'updateTrainingStatus';
			$result = parent::webServiceXMLToString(WEBSERVICE_PATH.QN.http_build_query($params));
		} catch (Exception $e) {
            $result = 'Caught Exception:'.$e->getMessage();
        }
        return $result;
	}
	
}
