<?php

header('Access-Control-Allow-Origin: *');
require_once 'config.php';
include 'utility/cryptojs-aes.php';
require_once 'utility/securityUtil.php';
require_once 'utility/class.phpmailer.php';
//$authority = include 'service_connect.php';
//echo base64_encode('p=notification&action=getInappNotification&screen=1&userid=713');die;
$urlstring = extractURLVariables($_REQUEST['_tk']);

$postData = $_POST;
$result = null;
$httpResponse = null;

if ($urlstring != '') 
{
	$curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	$result = curl_exec($curl);
	$httpResponse = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ($httpResponse == '404') {
        throw new exception('This page doesn\'t exists.');
    }
	curl_close($curl);
    $returndata = [];
	printLog($result);
	$returndata['data'] = cryptoJsAesEncryptBase64Covered(ENCKEY, $result);
	echo json_encode($returndata); 
	die;
   //echo $returndata;  
	
}

