<?php
 
header('Access-Control-Allow-Origin: *');
require_once 'config.php';
include 'utility/cryptojs-aes.php';

require_once 'utility/securityUtil.php';
require_once 'utility/class.phpmailer.php';
//$authority = include 'service_connect.php';      



$urlstring = extractURLVariables($_REQUEST['_tk']);  
$postData = $_POST;
$result = null;
$httpResponse = null;
//Upload type
/*
echo $urlstring."<br>";
/*
echo $_GET['first_name'];
print_r($_POST);
*/

define('UPLOAD_TYPE_USER_PROFILE', 1);
define('UPLOAD_TYPE_MACHINE_IMAGE', 2);
if (isset($_FILES['image'])) {
	
    $file = $_FILES['image'];
    $uploadtype = isset($_POST['uploadtype']) ? $_POST['uploadtype'] : UPLOAD_TYPE_USER_PROFILE;
    $filename = $file['name'];
    $filenamearr = explode('?', $filename);
    $filename = $filenamearr[0];
    $fileextarr = explode('.', $filename);
    $ext = end($fileextarr);
    $newfilename = date('Ymdhis.').$ext;
    $mode = isset($_POST['modeUploadonly']) ? $_POST['modeUploadonly'] : 0;
    $tmp_name = $file['tmp_name'];
    $isUploaded = 0;
    $dir = SERVICE_PROFILE_MEMBERURL;
    //$dir = '';
   
    if ($uploadtype == UPLOAD_TYPE_USER_PROFILE) 
	{
        $dir = SERVICE_PROFILE_MEMBERURL;
    } elseif ($uploadtype == UPLOAD_TYPE_MACHINE_IMAGE) {
        $dir = SERVICE_MACHINE_IMAGEURL;
    }
	
    if (move_uploaded_file($tmp_name, $dir.$newfilename)) {
        $isUploaded = 1;
    }
	
    if ($isUploaded) {
		
        if ($uploadtype == UPLOAD_TYPE_USER_PROFILE) 
		{
			
            if ($mode == 0) {
                $userid = $_POST['id'];
                $urlstring = 'action=getSaveProfileImage&p=members&userid='.$userid.'&userimage='.$newfilename;
            } else {
                $asJSon = isset($_POST['responseAsJson']) ? $_POST['responseAsJson'] : 0;
				
                if ($asJSon == 1) 
				{
                    $imagedetails = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Successfully Image Uploaded',
                        'imagefile' => $newfilename,
                    );
                    echo json_encode($imagedetails);
                    exit;
                } else {
                    echo trim($newfilename);
                    $urlstring = '';
                    exit;
                }
            }
			
        } else {
            if ($mode == 0) {
                //print_r($_POST);
            } else {
                /* SK if mode is not equal to Zero, Retrun image name */
                $imagedetails = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully Image Uploaded',
                    'imagefile' => $newfilename,
                );
                echo json_encode($imagedetails);
            }
        }
    }
}
if (isset($_FILES['imgval'])) {
	
    $file = $_FILES['imgval'];
    $uploadtype = isset($_POST['uploadtype']) ? $_POST['uploadtype'] : UPLOAD_TYPE_USER_PROFILE;
    $filename = $file['name'];
    $filenamearr = explode('?', $filename);
    $filename = $filenamearr[0];
    $fileextarr = explode('.', $filename);
    $ext = end($fileextarr);
    $newfilename = date('Ymdhis.').$ext;
    $mode = isset($_POST['modeUploadonly']) ? $_POST['modeUploadonly'] : 0;
    $tmp_name = $file['tmp_name'];
    $isUploaded = 0;
    $dir = SERVICE_PROFILE_MEMBERURL;
    //$dir = '';
    if ($uploadtype == UPLOAD_TYPE_USER_PROFILE) 
	{
        $dir = SERVICE_PROFILE_MEMBERURL;
    } elseif ($uploadtype == UPLOAD_TYPE_MACHINE_IMAGE) {
        $dir = SERVICE_MACHINE_IMAGEURL;
    }
	//echo "<pre>";print_r($_FILES);
    if (move_uploaded_file($tmp_name, $dir.$newfilename)) {
        $isUploaded = 1;
    }
	//echo  $isUploaded;
	//die;
    if ($isUploaded) {
		if ($uploadtype == UPLOAD_TYPE_USER_PROFILE) {
			
            if ($mode == 0) {
                $userid = $_POST['id'];
                $urlstring = 'action=getSaveProfileImage&p=members&userid='.$userid.'&userimage='.$newfilename;
				
            } else {
                $asJSon = isset($_POST['responseAsJson']) ? $_POST['responseAsJson'] : 0;
				if ($asJSon == 1) {
                    $imagedetails = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Successfully Image Uploaded',
                        'imagefile' => $newfilename,
                    );
                    echo json_encode($imagedetails);
                    exit;
                } else {
                    echo trim($newfilename);
                    $urlstring = '';
                    exit;
                }
            }
        } else {
            if ($mode == 0) {
                //print_r($_POST);
            } else {
                /* SK if mode is not equal to Zero, Retrun image name */
                $imagedetails = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully Image Uploaded',
                    'imagefile' => $newfilename,
                );
                echo json_encode($imagedetails); 
            }
        }
		
    }
}
if ($urlstring != '') {
    $curl = curl_init();
	if(isset($_POST['is_reporting']) && $_POST['is_reporting'] != '')
	{
		curl_setopt($curl, CURLOPT_URL, trim($_POST['is_reporting']));
		
	}
	else
	{

		curl_setopt($curl, CURLOPT_URL, trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));
	}
	
	printLog(trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));  
	//echo trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis'));die;
   // curl_setopt($curl, CURLOPT_URL, trim(SERVICE_AJAXURL.'?'.$urlstring.'&'.date('Ymdhis')));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	
   	   $result = curl_exec($curl);
	
    $httpResponse = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ($httpResponse == '404') {
        throw new exception('This page doesn\'t exists.');
    }   
	  
      	
    $returndata = [];
	
    $returndata['data'] = cryptoJsAesEncryptBase64Covered(ENCKEY, $result);
	//$returndata['data'] = $result;
	
    echo json_encode( $returndata );   

    curl_close($curl);      
	
}

