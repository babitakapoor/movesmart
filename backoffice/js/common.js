var showSportmedZoom = 0;
var hrZoomDisplay = 0;
var SERVICE_QUESTION_ICON='../images/uploads/movesmart/quesicon/';
var BANNER_IMAGE_PATH='../images/uploads/movesmart/bannerimage/';
var _PushMessageCount	=	255;
var USER_STATUS_ACTIVE=1;
var LANGUAGE = [];
$(document).ready(function() {
    $(window).load(function() {
        if (navigator.userAgent.match(/iPad/)) {
            $(".txt-bx-readonly").each(function() {
                if ($(this).find("select").attr("disabled", "disabled")) {
                    $(this).parent().append("<div class='inactive-blocker'></div>");
                    $(this).parent().addClass("inactive");
                }
            });
        }
    });
    /* Hide zoom graph icon when pressing graph both view icon #92782  */
    $(".graph-both-view").click(function() {
        $('.icon-zoom').hide();
    });




    /** Plan centers Mapping Tab **/
    $(".move_right").bind('click', function() {
        var $selectedcenters    =   $("#centerListLf  option:selected");
        $selectedcenters.appendTo("#centerListRt");
        $selectedcenters.remove();
        $("#centerListRt").html($("#centerListRt option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    $(".move_left").bind('click', function() {
        var $selectedcenters    =   $("#centerListRt option:selected");
        $selectedcenters.appendTo("#centerListLf");
        $selectedcenters.remove();
        $("#centerListLf").html($("#centerListLf option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    /** Plan centers Mapping Tab Activities**/
    $(".move_rights").bind('click', function() {
        var $selectedcenters    =   $("#centerListLtact  option:selected");
        $selectedcenters.appendTo("#centerListRtact");
        $selectedcenters.remove();
        $("#centerListRtact").html($("#centerListRtact option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    $(".move_lefts").bind('click', function() {
        var $selectedcenters    =   $("#centerListRtact  option:selected");
        $selectedcenters.appendTo("#centerListLtact");
        $selectedcenters.remove();
        $("#centerListLtact").html($("#centerListLtact option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    /*$(".left_unCheckAll").on('click', function() {
     $(".left_CheckAllImg").show();
     $(".left_unCheckAllImg").hide();
     $("#centerListLf > option").each(function() {
     $(this).prop('selected', false);
     });
     });*/
   /*  $('.page-info-toggle-btn').click(function() {
        $(this).find('span').toggleClass('icon-up');
        $(this).closest('body').find('.info-toggle-container').slideToggle();
    }); */

    $(".left_CheckAll").on('click', function() {
        //$(".left_CheckAllImg").hide();
        //$(".left_unCheckAllImg").show();
        $("#centerListLf > option").each(function() {
            $(this).prop('selected', true);
        });
        var $selectcenters   =   $("#centerListLf  option:selected")
        $selectcenters.appendTo("#centerListRt");
        $selectcenters.remove();
        $("#centerListRt").html($("#centerListRt option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    $(".right_CheckAll").on('click', function() {
        $("#centerListRt > option").each(function() {
            $(this).prop('selected', true);
        });
        var $selectcenters   =   $("#centerListRt  option:selected");
        $selectcenters.appendTo("#centerListLf");
        $selectcenters.remove();
        $("#centerListLf").html($("#centerListLf option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    $(".left_CheckAlls").on('click', function() {
        $("#centerListLtact > option").each(function() {
            $(this).prop('selected', true);
        });
        var $selectcenters   =   $("#centerListLtact  option:selected");
        $selectcenters.appendTo("#centerListRtact");
        $selectcenters.remove();
        $("#centerListRtact").html($("#centerListRtact option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });

    $(".right_CheckAlls").on('click', function() {
        $("#centerListRtact > option").each(function() {
            $(this).prop('selected', true);
        });
        var $selectcenters  =$("#centerListRtact  option:selected");
        $selectcenters.appendTo("#centerListLtact");
        $selectcenters.remove();
        $("#centerListLtact").html($("#centerListLtact option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });






    /*$(".right_unCheckAll").on('click', function() {
     $("#centerListRt > option").each(function() {
     $(this).prop('selected', false);
     });
     });*/

    //Show password in Edit Screen
    $(".showPassword").on('click', function() {
        //alert($(this).find('input').is(':checked'));showPassword
        if ($(this).find('input').is(':checked') == true) {
            $('.passwordtxt').show();
        } else {
            $('.passwordtxt').hide();
        }
    });

    /*LightBox Height Cover Background color black*/
    $("#customPopoverlay").css("height", $('body .pagewrapper').height());



    $("#toggle").click(function() {
        $('.autoCompletebox').toggle();
    });

    $('#trainingWorkLoad').click(function() {
        var tri = $('input[name=equ1Club1]:radio:checked').val();
        if (tri != '') {
            showQuickAddPop('Edit WorkLoad', 'editWorkLoad', tri);
        }
    });

    /* Quick add popup ends */
    $('.show-popup').click(function() {
        var gethref = $(this).attr('href');
        var $closepagewrapper=$(this).closest('.pagewrapper');
        var posx = $closepagewrapper.find(gethref).width() / 2;
        var posy = $closepagewrapper.find(gethref).height() / 2;
        $closepagewrapper.find(gethref).css({'margin-left': -posx, 'margin-top': -posy});
        $closepagewrapper.find(gethref).fadeIn();
        $closepagewrapper.find('.popup-mask').fadeIn();
        $(this).closest('body').css('overflow', 'hidden');
    });
    $('.icon-popupcls').click(function() {
        $(this).closest('.popup-holder').fadeOut();
        $(this).closest('.pagewrapper').find('.popup-mask').fadeOut();
        $(this).closest('body').css('overflow', 'inherit');
        $(".com-alert-msg").hide();
    });


    $('.get-check').click(function() {
        if ($(this).find("input[type='checkbox']").attr('checked') == "checked") {
            $(this).find("input[type='checkbox']").prop('checked', false);
            $(this).find("input[type='checkbox']").removeAttr('checked');
        } else {
            $(this).find("input[type='checkbox']").prop('checked', true);
            $(this).find("input[type='checkbox']").attr('checked', 'checked');
        }
        var radioName = $(this).find("input[type='radio']").attr("name");
        $('.get-check').each(function() {
            $(this).find("input[name='" + radioName + "']").prop('checked', false);
            $(this).find("input[name='" + radioName + "']").removeAttr('checked');
        });
        $(this).find("input[type='radio']").prop('checked', true);
        $(this).find("input[type='radio']").attr('checked', 'checked');
    });

    /* for Double click in coach List page */
    $("#coachListGridTab > tbody > tr td:not(:nth-last-child(1))").dblclick(function() {
        var memberId = $(this).parent().find('.testId').val();
        var lastTestId = '';
        if (memberId != "") {
            redirectEdit(memberId, lastTestId, 2, "dbclick");
        }
    });



    /* for Double click in members List page */
    $("#memberListGridTab > tbody > tr td:not(:nth-last-child(1))").dblclick(function() {
        var memberId = $(this).parent().find('.testId').val();
        var lastTestId = '';
        if (memberId != "") {
            redirectEdit(memberId, lastTestId, 2, "dbclick");
        }
    });


    var touchstart = 1;
    $('#memberListGridTab > tbody > tr td:not(:nth-last-child(1))').on({
        'touchstart': function() {
            if (touchstart > 1) {
                var memberId = $(this).parent().find('.testId').val();
                var lastTestId = '';
                if (memberId != "") {
                    redirectEdit(memberId, lastTestId, 2, "dbclick");
                }
                touchstart = 1;
            }
            setTimeout(function() {
                touchstart = 1;
            }, 500);
            touchstart++;
        },
        'touchmove': function() {
            touchstart = 1;
        }

    });

    /*
    $('#memberListGridTab > tbody > tr td:not(:nth-last-child(1))').on({'touchmove': function() {
        touchstart = 1;
    }});*/




    /*for Double click in Club List Page */
    $("#testResultListGridTab > tbody > tr td:not(:nth-last-child(1))").dblclick(function() {
        var memberId = $(this).parent().find('.testId').val();
        var lastTestId = $(this).parent().find('.lastTestId').val();
        if (memberId != "") {
            redirectEdit(memberId, lastTestId, 2, "dbclick");
        }
    });



 $('.checkouter').click(function() {
        var $this	=	$(this);
        var $assoc	=	$this.find('input[type="checkbox"]');
        var _class	=	$assoc.attr("class");
        var _name	=	$assoc.attr("name");
        var _hrefvalue	=	$assoc.attr("hrefvalue");
        if($assoc.is(":checked")){
            $assoc.removeAttr("checked");
            var $ipbyhrefval   =$("input[hrefvalue="+_hrefvalue+"]");
            $ipbyhrefval.attr("checked",true);
            $ipbyhrefval.parent().addClass("checkbtn");
        } else {
            $assoc.attr("checked",true);
            var $machinedetails    =   $(".machinedetails");
            $machinedetails.removeAttr("checked",false);
            $machinedetails.parent().removeClass("checkbtn");
            var $ipelembyname   =$("input[name="+_name+"]");
            $ipelembyname.attr("checked",true);
            $ipelembyname.parent().addClass("checkbtn");
        }
        $("." + _class).trigger("change");
});



    //This below code is commented which is not working correct right now, Need to Implement in some other way
    $('body').click(function(e) {
        if ($(e.target).parents('.left-menu').attr('class') == "left-menu") {
        } else {
            var $nvtoggle   =   $('.nav-toggle');
            if ($nvtoggle.hasClass('nav-delete')) {
            } else {
                if ($nvtoggle.hasClass('nav-active')) {
                    $('.left-menu-sec').animate({left: -230}, 500);
                    $(".con-title-sec").animate({left: 0}, 500);
                    $('.content-wrapper').animate({right: 0}, 500);
                    $nvtoggle.removeClass('nav-active');
                }
            }
        }
    });
    $('body').on("tap", function(e) {
        if ($(e.target).parents('.left-menu').attr('class') == "left-menu") {
        } else {
            var $nvtoggle   =   $('.nav-toggle');
            if ($nvtoggle.hasClass('nav-delete')) {
            } else {
                if ($nvtoggle.hasClass('nav-active')) {
                    $('.left-menu-sec').animate({left: -230}, 500);
                    $(".con-title-sec").animate({left: 0}, 500);
                    $('.content-wrapper').animate({right: 0}, 500);
                    $nvtoggle.removeClass('nav-active');
                }
            }
        }
    });

    /*This function is being selected in radio button*/
    $('.customRadioCycle').each(function() {
        //Checking If test completed.
        if ($(this).attr("status") != 3 && $(this).attr("customselect") == $(this).val()) {
            $(this).attr("checked", "checked");
        }
    });
    /*$('.cycleAllocate').each(function() {
        var clubId = $(this).attr('clubId');
        var userId = $(this).attr('userId');
        var status = $(this).attr('status');
        var thisQuery = $(this);
        var action = "getCycleAvailableForClub";
        var data = "clubId=" + clubId + "&userId=" + userId + "&status=" + status + "&action=" + action;
        $.ajax({
            url: '../ajax/index.php?p=linkCycle',
            type: "POST",
            data: data,
            success: function(result) {
                thisQuery.html(result);
            }
        });
    });
    */


    $('.zipCodeAjax').blur(function() {
        var cityzip = $(this).val();
        $.ajax({
            type: "POST",
            url: "../ajax/index.php?p=city",
            data: 'cityzip=' + cityzip,
            cache: false,
            success: function(result) {
                $('.zipCodeAjaxGet').html(result);
            }
        });
    });

    /*var update = function() {
     jQuery(this).parent().attr('title', jQuery(this).children(':selected').text());
     };
     jQuery(document).on('click change update', '.select-custom>select', update);
     jQuery('.select-custom>select').trigger('update');*/
    $(".tabs li > a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tabscontent").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
    $(".innertab li a").click(function(event) {
        event.preventDefault(event);
        $(this).parent().addClass("innercurrent");
        $(this).parent().siblings().removeClass("innercurrent");
        var innertab = $(this).attr("href");
        $(".innertabscontent").not(innertab).css("display", "none");
        $(innertab).fadeIn();
    });

    show = 0;
    $('.nav-toggle').click(function() {
        if (show) {
            $(this).removeClass('nav-active');
            $('.left-menu-sec').animate({left: -230}, 500);
            $(".con-title-sec").animate({left: 0}, 500);
            $('.content-wrapper').animate({right: 0}, 500);
            show = 0;
        }
        else {

            $(this).addClass('nav-active');
            $(this).addClass('nav-delete');
            setTimeout(function() {
                $('.nav-toggle').removeClass('nav-delete');
            },100);
            $('.left-menu-sec').animate({left: 0}, 500);
            $('.content-wrapper').animate({right: -230}, 500);
            $(".con-title-sec").animate({left: 230}, 500);
            show = 1;
        }
    });
    $(".left-menu > li > a").click(function() {
        $(".left-menu > li a").removeClass('active');
        if (false == $(this).next().is(':visible')) {
            $('.left-menu ul').slideUp(300).css({'margin-top': '-15px'});
            $(this).addClass('active');
        }
        $(this).next().slideToggle(300);
    });
    // $('.left-menu ul:eq(0)').show().css({'margin-top': '-15px'});
    var $usersetholder  =   jQuery('.user-set-holder');
    var ushig = $usersetholder.height();
    $usersetholder.css({height: ushig + 'px'});
    jQuery('.user-set-pointer').hover(function() {
        jQuery(this).stop(true, true).addClass('current');

        jQuery(this).find('.user-set-holder').stop(true, true).slideDown(200);
    }, function() {
        jQuery(this).stop(true, true).removeClass('current');
        jQuery(this).find('.user-set-holder').stop(true, true).slideUp();
    });
    jQuery('.lang-chose a').click(function() {
        jQuery('.lang-chose a').removeClass('cur-lang');
        var curlang = $(this).text();
        $('.user-lang').empty().append(curlang);
        $(this).addClass('cur-lang');
        if (curlang != '') {
            $.ajax({
                url: '../ajax/index.php?p=language',
                type: "POST",
                data: 'lang=' + curlang,
                success: function() {
                    location.reload();
                }
            });
        }
    });
    $(".accordion-holder > .acc-row h2,.accordion-holder > .acc-row h3").click(function() {
        $(this).toggleClass('current-sec');
        $(this).next('.acc-content').slideToggle(250);
        $(this).find('.icon').toggleClass('icon-up');
    });

    $('#exportActiveMembers').click(function() {
        var exportType = $('#exportType').val();
        var labelField = $('#labelField').val();
        var sortType = $('#sortType').val();
        var userType = $('#userType').val();
        var searchType = $('#searchType').val();
        var searchValue = $('#searchValue').val();
        var url = '../ajax/index.php?p=export_clubs&exportType=' + exportType + '&searchType=' + searchType +
            '&searchValue=' + searchValue + '&labelField=' + labelField +
            '&sortType=' + sortType + '&userType=' + userType;
        if (userType != 'clubList') {
            var clubId = $('#clubId').val();
            url = '../ajax/index.php?p=export_members&exportType=' + exportType +
                '&clubId=' + clubId + '&searchType=' + searchType +
                '&searchValue=' + searchValue + '&labelField=' + labelField +
                '&sortType=' + sortType + '&userType=' + userType;
        }
        window.location = url;
    });

//Cear Member Details.
    $("#clear_search").bind('click', function() {
        $("#searchType, #clubId, #searchValue").val('');
        $("#membersearch").submit();
    });



    $("#clear_search").bind('click', function() {
        $("#searchType, #clubId, #searchValue").val('');
        $("#membersearch").submit();
    });

   

//Ajax request for login function
    $('#authentication').submit(function(e) {
        e.preventDefault();
        var $username=   $('#username');
        var $password=   $('#password');
        data = {'method': 'authenticate', 'username': $username.val(), 'password': $password.val()};
        if ($.trim($username.val()) == '' || $.trim($password.val()) == '') {
            $('.login-error').html(LANG['enterUsernamePwd']).show();
            $username.focus();
            return false;
        }
        ajLoaderOn();
        $.ajax({
            type: "POST",
            url: "../ajax/index.php?p=login",
            data: data,
            cache: false,
            dataType: 'json',
            success: function(result) {
                ajLoaderOff();
                /**
                 * @param result.status_code string  - this is used to check fot status value
                 * @param result.usertype_id int  - to get the user type id
                 */
                var $loginError   =   $('.login-error');
                if (typeof result.status_code != "undefined") {
                    var statuscode = parseInt(result.status_code);
                    switch (statuscode) {
                        case 1:
                            var redirectUrl = 'index.php?p=dashboard';
                            if (result.usertype_id == 3) {
                                redirectUrl = '../member/index.php?p=dashboard';
                                $loginError.html(LANG['notAuthorized']).show();
                            }else{
                                window.location = 'index.php?p=settings';
                            }
                            break;
                        case 2:
                            $loginError.html(LANG['enterValidUsernamePwd']).show();
                            break;
                        case 3:
                            $loginError.html(LANG['notAuthorized']).show();
                            break;
                        case 4:
                            $loginError.html(LANG['accountDeactivated']).show();
                            break;
                        default:
                            $loginError.html(LANG['unableToLogin']).show();
                    }
                } else {
                    $loginError.html(LANG['unableToLogin']).show();
                }
                $("#username").focus();
            }
        });
    });

    var currentYear = new Date().getFullYear() + 1;
    $("#datepicker").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1900:' + currentYear
    });

    //Coach workstarton and workendon
    $("#workStart").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:' + currentYear
    });
    $("#workEnd").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:' + currentYear
    });

    $("#testDate").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:' + currentYear});
    $("#newTestDate").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:' + currentYear
    });
    $("#testPrepared").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:' + currentYear
    });

    // Field used in testing and measuring pages
    $("#sessionStartDate").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:' + currentYear
    });


    //While Deleting the Image show the Photo Not Available image 
    $("#delete-member-image").on('click', function() {
        confirms(LANG['deleteImageConfirmation'], function(confirmResult) {
            if (confirmResult == "true") {
                $('.ajax_image_loader').css('display', 'block');
                //$("#member_image_err").html('Image Deleted');
                var fullFilePath = $(this).attr('data-role-defpath') + $(this).attr('data-role-deleteimg');
                $("#imagePreview").attr('src', fullFilePath);
                $("#client_image").val($(this).attr('data-role-deleteimg'));
                $('#delete-member-image').css('display', 'none');

                //Update deleted image
                var updateImageName = $(this).attr('data-role-deleteimg');
                var memberId = $('#hiddenuserId').val();
                updateMemberProfileImage(memberId, updateImageName);
            } else {
                closeMsg();
            }
        });
    });


        
    //  function getSelectBox(){
    //    if(group_name == ''){
    //     alert("Plz fill all boxes");

    //     }

    // }        


    //Validation Message
    var $successmsg =   $(".successSetMessgae > div");
    if ($successmsg.length == 1 && $successmsg.hasClass("fadeMsg") == true) {
        $(".successSetMessgae").hide("slow");
    }
    /*****
     Apply theme
     *******/

    /*Custom File upload Button*/
    $('.show-popup').click(function() {
        var gethref = $(this).attr('href');
        var $closepagewrapper=$(this).closest('.pagewrapper');
        $closepagewrapper.find(gethref).fadeIn();
        $closepagewrapper.find('.popup-mask').fadeIn();
    });
    $('.icon-popupcls').click(function() {
        $(this).closest('.popup-holder').fadeOut();
        $(this).closest('.pagewrapper').find('.popup-mask').fadeOut();
    });


    /* Delete employee club roles */
    $(".emp_role_club_delete").click(function() {
        var roleDeleteId = $(this).attr('deleteId');
        confirms(LANG['deleteConfirmation'], function(confirmResult) {
            if (confirmResult == "true") {
                //Ajax loader on
                ajLoaderOn();
                //To get external popup content 
                $.ajax({
                    url: '../ajax/index.php?p=deleteEmployeeRole',
                    type: "POST",
                    data: 'roleDeleteId=' + roleDeleteId,
                    success: function() {
                        //Ajax loader off
                        ajLoaderOff();
                        closeMsg();
                        $("#employee_club_role_tr_" + roleDeleteId).remove();
                    }
                });
            } else {
                closeMsg();
            }
        });
    });
    /* Delete employee club roles */

    //Validate search filter form
    /* $('#searchValue').keyup(function() {
     validateSearchForm();
     return false;
     });*/

    $('#searchFilterSubmit').click(function() {
        validateSearchForm();
        return false;
    });
    //Validate search filter form

    $('.toggle-label td').click(function() {
        var linkData = $(this).parent().attr('linkData');
        var splitQuote = $(this).find('span a').attr('onclick');
        data = splitQuote.split("'");
        $.ajax({
            url: "../ajax/index.php?p=members&action=changeLabelSession&labelField=" + data[1] +
                "&linkData=" + linkData,
            type: "POST",
            success: function() {
                location.reload();
            }
        });
    });

    //Edit strength values begins
    $(".strength_value").dblclick(function() {
        var thisObj = $(this);
        $(".strength_val_text").show();
        $(".strength-val-edit-box").hide();
        $(thisObj).find(".strength_val_text").hide();
        $(thisObj).find(".strength-val-edit-box").show().focus();
    });
    $(".strength_val_edit_text").keyup(function(e) {

        if (e.keyCode == 13 || e.keyCode == 27) {
            var thisId = $(this).attr('id');
            var parentId = "edit_text_parent_" + thisId.split("edit_text_id_")[1];
            var thisObj = $("#" + parentId);
            $(this).hide();
            $(thisObj).find(".strength_val_text").html($(this).val()).show();
            $(thisObj).find(".strength_val_edit_hidden_text").val($(this).val());
        }
    });
    //Edit strength values ends	

    //Show alert and change test status when test member status in test analysis mode
    $(".showAlertAndStopLastTest").click(function() {
        var userId = $(this).attr('userid');
        confirms(LANG['alertTestNotAnalyzed'], function(confirmResult) {
            if (confirmResult == "true") {
                closeMsg();
                //Ajax loader on
                ajLoaderOn();
                $.ajax({
                    url: '../ajax/index.php?p=members',
                    type: "POST",
                    dataType: 'json',
                    data: 'action=cancelMemberCurrentTest&userId=' + userId,
                    success: function(response) {
                        //Ajax loader off
                        ajLoaderOff();
                        if (response.status == 'success') {
                            redirectNewTestUser(userId);
                        }
                        else {
                            flashMsgDisplay(LANG['alertOops'], 'failure-msg');
                        }
                    }
                });

            } else {
                closeMsg();
            }
        });
    });

    //Show alert new test in training mode
    $(".showAlertTestIntraining").click(function() {
        var userId = $(this).attr('userid');
        confirms(LANG['alertTestIntraining'], function(confirmResult) {
            if (confirmResult == "true") {
                closeMsg();
                redirectNewTestUser(userId);
            } else {
                closeMsg();
            }
        });
    });

    //Show alert new test in training mode
    $(".createNewTestMember").click(function() {
        var userId = $(this).attr('userid');
        redirectNewTestUser(userId);
    });
    /*For Body Composition tab*/
    /* Below two functions added M.JahabarYusuff For pick and move the Bodycomposition methods for the club.  05052015*/
    /**
     * Common Function for pick and Move the single Option(left to right / right to left)
     */
    $(".interchange_bcomp").bind('click', function() {
        //centerBcompMethods  - left Select Box
        //savedBcompMethods - right Select Box 
        var select_left = 'centerBcompMethods';
        var select_right = 'savedBcompMethods';
        var currentClick = $(this).attr('data-mode');
        var destinationSelect = select_right;
        var sourceSelect = select_left;
        if (currentClick == 'right-left') {
            sourceSelect = select_right;
            destinationSelect = select_left;
        }
        var $selsource  =   $("#" + sourceSelect + "  option:selected");
        $selsource.appendTo("#" + destinationSelect);
        $selsource.remove();
        $("#" + destinationSelect).html($("#" + destinationSelect + " option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });
    /**
     * Common Function for pick and Move the All Option(left to right / right to left)
     */
    $(".moveall_bcomp").on('click', function() {
        var currentClick = $(this).attr('data-mode');
        var select_left = 'centerBcompMethods';
        var select_right = 'savedBcompMethods';
        var destinationSelect = select_right;
        var sourceSelect = select_left;
        if (currentClick == 'right-all-left') {
            sourceSelect = select_right;
            destinationSelect = select_left;
        }
        var $selsource  =   $("#" + sourceSelect + "  option:selected");
        $("#" + sourceSelect + " > option").each(function() {
            $(this).prop('selected', true);
        });
        $selsource.appendTo("#" + destinationSelect);
        $selsource.remove();
        $("#" + destinationSelect).html($("#" + destinationSelect + " option").sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }))
    });
    //Show alert new test in training mode
    $("#save_bodycomp").click(function() {
        $("#savedBcompMethods > option").each(function() {
            $(this).prop('selected', true);
        });

        $("#bodyCompositionSelect").submit();
    });
    /*End body Composition tab 05052015*/
    $(".question_box .pg_htr_h2").click(function(){
        var $element	=	$(this).parent().find(".question_box_inr");
        $(".question_box_inr").slideUp(1000);
        if($element.css('display')=='none'){
            $element.slideDown(1000);
        }
    });
    $(".qtn_inr h2").click(function(){
        var $element	=	$(this).parent().find(".qtn_inr_bx");
        $(".qtn_inr_bx").slideUp(1000);
        if($element.css('display')=='none'){
            $element.slideDown(1000);
        }
    });
    $(".focus_pushcharcounts .messagetext").on("keydown",function(){
        var $this	=	$(this);
        var $label	=	$this.parent().find('.leftchars');
        var runchars=	$this.val().split('').length;
        var leftchar=	(_PushMessageCount-runchars);
        if(leftchar>=0) {
            $label.html(leftchar+ " Chars left");
        } else {
            $label.html(0+ " Chars left");
            return false;
        }
    });
});

//Redirect member to initiate test page
function redirectNewTestUser(userId) {
    window.location = "index.php?p=testResult&id=" + userId + "&newTest=1";
}

//Validate search filter form
function validateSearchForm() {
    var checkChar = 0;
    var flg = 0;
    var $searchType = $("#searchType");
    var $searchValue = $("#searchValue");
    var $clubId = $("#clubId");
    var $successSetMessgae = $(".successSetMessgae");
    var validationResult='';
    if ($clubId.attr('type') == 'hidden') {
        flg = 1;
    }
    if ($clubId.val() == undefined) {
        flg = 1;
    }
    /*if($('#clubId').val()==''){
     flg = 1;
     }*/
    if (($searchType.val() == '') && ($searchValue.val() == '') && (flg == 1)) {
        $successSetMessgae.show();
        $('.success-msg').show();
        $successSetMessgae.append("<div class='col9 validationMsg searchValue'>Please Select Any One Options</div>");
        setTimeout(function() {
            $successSetMessgae.hide('slow');
            $successSetMessgae.html("");
        }, 2000);
        checkChar = 0;
    } else {
        checkChar = 1;
    }

    if (($searchType.val() != '') && ($searchValue.val() == '')) {
         validationResult = validationForm([
             'textbox&searchValue&' + LANG['alertSearchValue']
         ], 'searchFilterForm', '2');
        checkChar = 0;
    }

    if (($searchType.val() == '') && ($searchValue.val() != '')) {
         validationResult = validationForm([
             'dropdown&searchType&' + LANG['alertSearchValue']
         ], 'searchFilterForm', '2');
        checkChar = 0;
    }
    if(validationResult){
        //NEed work later
    }
    var searchValue = $searchValue.val();
    //if (validationResult) {
    if (checkChar == 1) {
        ajLoaderOn();
        $('#searchFilterForm').submit();
        return false;
    }
    // }
}
//Validate search filter form

/******
 Author: Punitha Subramani
 Created Date:25-Sep-2014
 Description:titleGraph is title of the graph,
    LeftCaption for left label,
    bottomCaption for bottom label , Passing total datas in totalDatas
 *******/

function graphOnload(titleGraph, bottomCaption, totalDatas, yAxisValue, popUpId) {
    var popUp = 'largePopUp';
    if (popUpId == undefined) {
        popUp = 'weightFat';
    }

    $('#' + popUp).highcharts({
        chart: {type: 'column', options3d: {
                enabled: true,
                alpha: 5,
                beta: 5,
                viewDistance: 0,
                depth: 50,
                frame: {
                    side: {size: 2, color: '#FCFB87', border: '1'},
                    bottom: {size: 2, color: '#AFAFAF'}
                }
            }, marginTop: 80,
            events: {
                click: function() {
                    showGraphZoomPopup('Graph', 'graphPopUp', '', 'large');
                    graphOnload(titleGraph, bottomCaption, totalDatas, yAxisValue, 'popup');
                }
            }

        },
        title: {text: titleGraph},
        xAxis: {categories: bottomCaption, labels: {rotation: -55}, 'gridLineColor': '#bbb'},
        yAxis: yAxisValue,
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> ' +
                '{series.name}: {point.y} / {point.stackTotal}'
        },
        plotOptions: {column: {stacking: 'normal', depth: 80}},
        series: totalDatas
    });
}

/*Script for Upload Image*/
function uploadImage(imageNameContains, uploadFor) {
    confirms(LANG['confirmUploadImage'], function(confirmResult) {
    var file_data = $('#uploadBtn').prop('files')[0];
    if (file_data == undefined) {
        //alerts(LANG['selectFile']);
        flashMsgDisplay(LANG['selectFile'], 'failure-msg');
        return false;
    }
    

        if (confirmResult == "true") {
            ajLoaderOn();
            $('.ajax_image_loader').css('display', 'block');

            var form_data = new FormData();
            if (form_data) {
                form_data.append("file", file_data);
                form_data.append("imageNameContains", imageNameContains);
                form_data.append("uploadFor", uploadFor);
            }
            /**
             * @param data.imagefile This is image file name
             */
            $.ajax({
                url: "../ajax/index.php?p=uploadImage",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data) {
                 $('#menuIconDetails').val(data.imagefile);
                if(uploadFor=="questionIcons"){
                    $('#quesicon').val(data.imagefile);
                }
				 if(uploadFor=="bannerImage"){
                    $('#pagebannerimg').val(data.imagefile);
                }
                if(uploadFor=="profileCoachImage"){
                    $('#userimage').val(data.imagefile);
                }
                if(uploadFor=="machineIcons"){
                    $('#machineicon').val(data.imagefile);
                }
                if(uploadFor=="uploadImportFile"){
                    $('#fileimport').val(data.imagefile);
                }
                    var $photocls= $('.photo-cls');
                    flashMsgDisplay(LANG['uploadSuccess'], 'success-msg');
                    $photocls.show();
                    $photocls.css('display', 'block');
                    closeMsg();
                    ajLoaderOff();
                    /*if($.trim(uploadFor)=="profileCoachImage"){
                     location.reload();
                     }*/
                    $('.ajax_image_loader').css('display', 'none');
                }
            });
        } else {
            closeMsg();
        }
    });
}



function displayImage(input, shortImage, userId, uploadFor)
 {
	 console.log("123");

    var imagePreview = document.getElementById('imagePreview');
    var imgName = input.files[0].name;
    var Extension = imgName.substring(imgName.lastIndexOf('.') + 1).toLowerCase();
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray(Extension, fileExtension) == -1) {
        //alerts(LANG['validImageUploadFormat']);
        flashMsgDisplay(LANG['validImageUploadFormat'], 'failure-msg');
        input.value = '';
        return false;
    }

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            if (shortImage == "y") {
                var shortImg = document.getElementById('shortImg');
                shortImg.setAttribute('src', e.target.result);
            }
            imagePreview.setAttribute('src', e.target.result);
            /*$('#imagePreview').Jcrop({
             });*/
        };
        reader.readAsDataURL(input.files[0]);
        //$('#uploadBtns').show();
        if (userId != '') {
            uploadImage(userId, uploadFor); //commented by sankar....since we dont want to upload them directly 
        }if(uploadFor!='profileCoachImage'){
             uploadImage(userId, uploadFor);
        }
    } else {
        imagePreview.setAttribute('src', 'placeholder.png');
    }
}

function sortingField(labelField, sortType, pageName) {
    var sortArray = {'labelField': labelField,
        'pageName': pageName,
        'sortType': sortType};
    $.ajax({
        url: "../ajax/index.php?p=sort",
        dataType: 'text',
        cache: false,
        data: sortArray,
        type: 'post',
        success: function() {
            location.reload();
        }
    });
}

// Function that validates email address through a regular expression.
function validateEmail(sEmail) {
    var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return pattern.test(sEmail);
}


/*Update Member Profile Image Name into DB*/
function updateMemberProfileImage(userId, ImageName) {
    var client_image_old = $('#client_image_old').val();
    var data = {
        action: 'update-coach-profile-image',
        user_image: '',
        user_id: userId,
        client_image_old: client_image_old
    };
    var url = "../ajax/index.php?p=coach";
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "json",
        async: false,
        success: function() {
            //alerts(LANG['imageSuccessDelete']);
            flashMsgDisplay(LANG['imageSuccessDelete'], 'success-msg');
            $('.ajax_image_loader').css('display', 'none');
            var $delmemimage    =   $('#delete-member-image');
            var append1 = $delmemimage.attr('data-role-defpath');
            var append2 = $delmemimage.attr('data-role-deleteimg');
            $('#imagePreview').attr('src', append1 + append2);
            closeMsg();
            return false;
        }
    });
}

/*
 First Parameter : A set of Element,Fields and Display messge.
 Example Parameter : textbox&firstName&Required First Name //Param ElementName  &ID name & Display Error messgae.
   If its a Radio element, instead of ID , function using name.
 Second Parameter: A form Name // This will be mainly used for, If the third param to be form submit.
 Form name will required primarily.
 Third Parameter : Checking whether is it form submit or ajax submit.
 */
function validationForm(elementWithId, formName, ajaxOrFormSave) {
	//personalInfoForm

    var validateArray = [];
    var elementTag ='';
	
    $('.com-alert-msg .alert-content').html("");
    $.each(elementWithId, function() {
        var elementChecking = this.split("&");

        /*If the Element is Textbox*/
        if ($.trim(elementChecking[0]) == "textbox") {
             elementTag = $("#" + $.trim(elementChecking[1])).val();
        }

        /*If the Element is Dropdown*/
        if ($.trim(elementChecking[0]) == "dropdown") {
             elementTag = $("#" + $.trim(elementChecking[1] + "  option:selected")).val();
        }
        /*If the Element is Radio*/
        if ($.trim(elementChecking[0]) == "radio") {
            if ($('input[name=' + $.trim(elementChecking[1]) + ']:checked').length == 0) {
                 elementTag = "";
            }
        }
        if (elementTag == "") {
            //$('.successSetMessgae').show();
            //$('.successSetMessgae').children('.fadeMsg').hide();
            if ($('.successSetMessgae').children('.' + $.trim(elementChecking[1])).length > 0) {
            } else {
                //$('.successSetMessgae').show();
                //$('.success-msg').show();
                //$('.successSetMessgae').append("<div class='col9 validationMsg " + $.trim(elementChecking[1]) + "'>"
                // + $.trim(elementChecking[2]) + "</div>");
                $('.com-alert-msg').show();
                $('.popup-mask-alert').show();
                $('.com-alert-msg .alert-content').append("<div>" + $.trim(elementChecking[2]) + "</div>");
            }

            validateArray.push('1'); // If Error Found
        } else {

            /*$('.successSetMessgae')
                .children('.' + $.trim(elementChecking[1]))
                .addClass('.overwriteSuccess').hide('slow',
             function() {
             $('.successSetMessgae').children('.' + $.trim(elementChecking[1])).remove();
             });*/
        }
    });


    setTimeout(function() {
        $('.successSetMessgae').hide('slow');
    }, 2000);

    if ($.inArray("1", validateArray) == -1 && ajaxOrFormSave == "1") {
        if (formName == 'frmManageMembers') {
            var personId = $("#userId").val();
            var email = $("#email").val();
            var companyIdLogin = $("#companyIdLogin").val();
            var dataString = '';
            //Fitclass doesnt need login details
            var hiddenuserId = $("#hiddenuserId").val();
            if (companyIdLogin != 1) {
                var username = $("#username").val();
                var password = $("#password").val();
                var cpassword = $("#cpassword").val();
                 dataString = "hiddenuserId=" + hiddenuserId + "&personId=" + personId +
                     "&email=" + email + "&username=" + username +
                     "&password=" + password + "&action=getPersonNumberExist";

                if (cpassword != password) {
                    //flashMsgDisplay(LANG['passwordnotmatch'], 'failure-msg');
                    //return false;
                }
            } else {
                 dataString = "hiddenuserId=" + hiddenuserId + "&personId=" + personId +
                     "&email=" + email + "&action=getPersonNumberExist";
            }
            /**
             * @param   result.personNumber This is Person Number
             */
            $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=members",
                data: dataString,
                cache: false,
                dataType: 'json',
                success: function(result) {
                    var eflag = 0;
                    if (hiddenuserId == '') {
                        if (result.personNumber == "0") {
                            //alerts(LANG['personNumberExist']);
                            flashMsgDisplay(LANG['personNumberExist'], 'failure-msg');
                            $("#userId").focus();
                            return false;
                        }
                    }
                    if (result.email == "0") {

                        if (hiddenuserId > 0) {
                            flashMsgDisplay(LANG['emailPasswordExistError'], 'failure-msg');
                            return false;
                        }

                        eflag = 1;
                        //alerts(LANG['emailExist']);
                        confirms(LANG['emailExistConfirmation'], function(confirmResult) {
                            if (confirmResult == "true") {
                                eflag = 0;
                                var element = $("<input>").attr({
                                    id: 'setNewPasswordFlag',
                                    name: 'setNewPasswordFlag',
                                    type: 'hidden'
                                });
                                $('#frmManageMembers').append(element).submit();
                                closeMsg();
                            } else {
                                closeMsg();
                            }
                        });
                        $("#email").focus();

                    }
                    if (eflag == 1) {
                        return false;
                    }
                    $("#" + formName).submit();
                }
            });
        } else {
            $("#" + formName).submit();
        }
    } else {

    }
    if ($.inArray("1", validateArray) == -1 && ajaxOrFormSave == "2") {
        $('.successSetMessgae').hide('slow');
        return true;
    } else {
        return false;
    }


}
/***
 Start Custom Message Box:
 1) ALERT: We have to call alert to alerts
 2) CONFIRM:
 **/
function customConfirmation(confirmMsg) {
	//alert("Hi I am ere");
    $("#customPopoverlay").show();
    $("#customPopbox").animate({'width': '30%', opacity: '0.90'}, "fast");
    var $customPopmessage  =   $("#customPopmessage");
    $customPopmessage.animate({'opacity': '0'}, "fast");
    $customPopmessage.animate({'opacity': '2.0'}, "fast");
    $customPopmessage.html(confirmMsg);
    $customPopmessage.append('<div class="appendEnd">' +
        '<img src="../images/movesmart/nobutton.png" ' +
            'style="width:40px;height:25px;" class="clicking customMsgBtn" ' +
            'dataStringvalue="false" />' +
        '<img src="../images/movesmart/yesbutton.png" ' +
            'style="width:40px;height:25px;margin-right:-10px;" ' +
            'class="clicking customMsgBtn" dataStringvalue="true" />' +
        '<div style="clear:both; height: 0px;">&nbsp;</div>' +
    '</div>');
    return true;
}
function confirms(confirmMsg, confirmResult) {
    $("#customPopbox").animate({'width': '15%', opacity: '1'}, "fast").show();
    customConfirmation(confirmMsg);
    $('.clicking').click(function() {
        confirmResult($(this).attr('dataStringvalue'), confirmMsg);
    });
}
function closeMsg() {
	   
    var $customPopbox  =   $("#customPopbox")
    $customPopbox.animate({opacity: '0'}, "fast");
    $("#customPopoverlay").hide();
    $customPopbox.animate({'width': '0%', opacity: '0'}, "fast");
    $("#customPopmessage").animate({'opacity': '0'}, "fast");
}
function alerts(contents) {
    $("#customPopoverlay").show();
    $("#customPopbox").animate({'width': '15%', opacity: '1'}, "fast").show();
    var $customPopmessage  =   $("#customPopmessage");
    $customPopmessage.animate({'opacity': '0'}, "fast");
    $customPopmessage.animate({'opacity': '2.0'}, "fast");
    $customPopmessage.html(contents).append(
        '<div>' +
            '<img src="../images/movesmart/okbutton.png" ' +
                'style="width:25px;height:25px;" class="customMsgBtn" onclick="closeMsg()" />' +
        '</div>'
    );
}

/***
 END Custom Message Box:
 **/

/******
 Test Screen Functions Start
 ****/
/*function getValueByAjax(currentValue, tranferTagId, action) {
    window.location.href = currentValue;
    /* var dataString = "currentValue="+currentValue+"&action="+action;    
     $.ajax({
     type: "POST",
     url: "../ajax/index.php?p=testScreen",
     data: dataString,
     cache: false,
     success: function(result) {
     if($.trim(action)=="getUserWithClubId"){
     $('#'+tranferTagId).html('<option value="" selected=selected>--Select User--</option>'+result);					   
     $("#"+tranferTagId).parent().attr('title','--Select User--');
     if(result==""){
     $("#userEmail").html('');
     }					
     } else {					
     $('#'+tranferTagId).val(result);
     }
     
     $("#"+tranferTagId).parent().attr('title',topResult);
     $("#"+tranferTagId).parent().attr('title','--Select User--');
     
     if($.trim(action)=="getEmailWithUserId"){					
     var topResult = result;
     $("#"+tranferTagId).val(topResult);
     } else {
     var topResult = '';
     $("#userEmail").parent().attr('title','');
     $("#userEmail").val('');
     $('#'+tranferTagId).val('');
     }
     
     }
     });* /
}*/
/*
function searchTestScreen(getValueToAppendUrl, thisAttribute) {
    var valueToAppendUrl = $('#clubUser option:selected').val();
    var clubToAppendUrl = $('#clubList option:selected').val();
    var clubUserAppendUrl = $('#clubUser option:selected').val();
    var userEmailAppendUrl = $('#userEmail').val();
    if (clubToAppendUrl == "") {
        //alerts(LANG['requireClub']);
        flashMsgDisplay(LANG['requireClub'], 'failure-msg');
        $('#clubList').focus();
        return false;
    }
    if (clubUserAppendUrl == "") {
        //alerts(LANG['requireUserName']);
        flashMsgDisplay(LANG['requireUserName'], 'failure-msg');
        $('#clubUser').focus();
        return false;
    }
    if (userEmailAppendUrl == "") {
        //alerts(LANG['requireEmail']);
        flashMsgDisplay(LANG['requireEmail'], 'failure-msg');
        $('#userEmail').focus();
        return false;
    }
    if (!isNaN(valueToAppendUrl) && valueToAppendUrl > 0) {
        var attrHref = $(thisAttribute).attr('attrHref');
        window.location = attrHref + '&id=' + valueToAppendUrl;
    }
}*/
/******
 Test Screen Functions End
 ****/

/***
 On key up search for members and coach search page. Redirect while edit.
 ***/

function redirectEdit(path, lastTestId, action, dbclick) {
    var hrefLink = encodeURIComponent($('.paramNone').val());
    var appendLink ='';
    if (dbclick == 'dbclick') {
        appendLink = path;
    } else {
        appendLink = $(path).attr('hrefValue');
    }
    var searchValue = $('#searchValue').val();
    var clubId = $('#clubId').val();
    var searchType = $('#searchType option:selected').val();
    var dataString = "currentLink=" + hrefLink + "&appendLink=" + appendLink + "&action=" + action +
        "&searchValue=" + searchValue + "&clubId=" + clubId + "&searchType=" + searchType;

    var uid = $(path).attr('uid');
    if (uid != undefined) {
        dataString += "&uid=" + uid;
    }
    var testPathId = '';

    if (lastTestId != '') {
        //Its Test Result List Page Only.	
        if (hrefLink == '%3Fp%3DtestResultsListDetail')
            testPathId = "&testId=" + lastTestId;
    }
    ajLoaderOn();
    $.ajax({
        type: "POST",
        url: "../ajax/index.php?p=redirectLink",
        data: dataString,
        cache: false,
        success: function(result) {
            ajLoaderOff();
            window.location.href = "index.php" + result + testPathId;
        }
    });
}

/* Time Validations */
function isAllowedColen(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 58)
        return false;
}

function isAllowedColenDecimal(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 58 && charCode != 46)
        return false;
}

function emptyLblName(obj, objValue) {
    if (obj.value == objValue) {
        obj.value = '';
    }
}
/*
function checkTimeField(fieldId, compare1) {
    jQuery(function($) {
        //Get the Control value.
        var $fieldid    =   $("#" + fieldId)
        var actualFieldValue = $fieldid.val();
        if (actualFieldValue != '00:00') {
            //':'checking
            if (actualFieldValue.indexOf(":") != -1) {
                //Assign before ':' value
                var firstValue = actualFieldValue.split(":");
                //Checking hours value.
                if (firstValue[0] > 23) {
                    //alerts(LANG['invalidTimeFormat']);
                    flashMsgDisplay(LANG['invalidTimeFormat'], 'failure-msg');
                    $fieldidval("");
                    $fieldid.focus();
                }
                //Checking Minute value.
                if (firstValue[1] > 59) {
                    //alerts(LANG['minuteValidate']);
                    flashMsgDisplay(LANG['minuteValidate'], 'failure-msg');
                    $fieldid.val("");
                    $fieldid.focus();
                }
            } else if (actualFieldValue > 23) {
                //alerts(LANG['invalidTimeFormat']);
                flashMsgDisplay(LANG['invalidTimeFormat'], 'failure-msg');

                $fieldid.val("");
                $fieldid.focus();
            }
        }
    });
}
function displayLblName(obj, objValue) {
    if (obj.value == '') {
        obj.value = objValue;
    }
}
 */
/* End Time Validations */


//Ajax loader on 
function ajLoaderOn() {
    $("#pageLoader").show();
    $("#pageLoaderLightBox").show();
}

//Ajax loader off
function ajLoaderOff() {
    $("#pageLoader").hide();
    $("#pageLoaderLightBox").hide();
}


/* Quick add popup begins */


/** Created by: Nesarajan M 
 Created on: 11-Oct-2014 
 Description: To create centralized popup to create quick items 
 popTitle: param for popup title
 popPageName: param to load html popup content
 **/ 
function showQuickAddPop(popTitle, popPageName, editId, popUpType,onSucessHandler) {
	
    if (editId == undefined) {
        editId = '';
    }

    if (popUpType == undefined) {
        popUpType = 'small-popup';
    } else {
        popUpType = 'large-popup';
    }
    var $quickaddpopdisp= $(".quick_add_pop_disp");
    $quickaddpopdisp.removeClass('large-popup');

    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=popup',
        type: "POST",
        data: 'action=' + popPageName + '&editId=' + editId,
        success: function(response) {
            //Ajax loader off
            ajLoaderOff();  
            //Add Class	
            $quickaddpopdisp.addClass(popUpType);

            //Set popup title			
            $(".quick_add_pop_disp .quick_pop_title").html(popTitle);

            //Set popup title			
            $(".quick_add_pop_disp .quick_pop_content").html(response);
            if(onSucessHandler){
                onSucessHandler();
            }
            //Show popup content
            $quickaddpopdisp.show();
            $(".popup-mask").show();
            $quickaddpopdisp.show();
        }
    });
}

/** Created by: Gaurav Thakur(Copied above written function)
 Created on: 26October2016
 Description: To create centralized popup to create quick items 
 popTitle: param for popup title
 popPageName: param to load html popup content
 **/
function showAddNewEntry(popTitle, popPageName, editId, popUpType,onSucessHandler) {
    if (editId == undefined) {
        editId = '';
    }

    if (popUpType == undefined) {
        popUpType = 'small-popup';
    } else {
        popUpType = 'large-popup';
    }
    var $quickaddpopdisp= $(".quick_add_pop_disp");
    $quickaddpopdisp.removeClass('large-popup');

    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=popup',
        type: "POST",
        data: 'action=' + popPageName + '&editId=' + editId,
        success: function(response) {
            //Ajax loader off
            ajLoaderOff();
            //Add Class	
            $quickaddpopdisp.addClass(popUpType);

            //Set popup title			
            $(".quick_add_pop_disp .quick_pop_title").html(popTitle);

            //Set popup title			
            $(".quick_add_pop_disp .quick_pop_content").html(response);
            if(onSucessHandler){
                onSucessHandler();
            }
            //Show popup content
            $quickaddpopdisp.show();
            $(".popup-mask").show();
            $quickaddpopdisp.show();
        }
    });
}

/*
function showQuickdeletePop(popTitle, popPageName, deleteid){
    ajLoaderOn();
    $.ajax({
        url:'../ajax/index.php?p=settings',
        type: "POST",
        data: 'action=' + popPageName + '&deleteid=' + deleteid,
        success: function(response) {
            ajLoaderOff();
            //alert(JSON.stringify(response));
            response = JSON.parse(response);
              $('.popup-mask-alert').show();
             if (response.status == '1') {
                location.reload();
             }
        }
    });
}*/
function addQuesContent(){
    var htmlcnt = '<div class="row-sec mb15">'
                         +'<div class="row-sec mb15">'
                             +'<label class="fl">Question Text<span class="required">*</span></label>'
                            +'<input type="text" id="questtxt" value="">'
                         +'</div>	'
                         +'<span class="addquescnt" onclick="removeQuesContent()">-</span>'
                 +'</div>';
    $('.questiontext').after(htmlcnt);
}
function addRadioTxt($self){
    var formFiled	=	$self.parent();
    var $options	=	$(".radioopt");
    var optindex	=	($options.length-1);
    var $lastitem	=	$(".radioopt_"+($options.length-1));
    var addradiotxtcnt  = '<div class="row-sec mb15 radioopt radioopt_'+(optindex+1)+'">'
                        +'<input type="hidden" id="hdnformfiledid" value="'+formFiled+'" />';
                        +'<label class="fl">&nbsp;</label>';
    /**
     * @param response.languageTypeDetails This is language type details
     * @param row.language_id This is language id
     */
    ajLoaderOn();
    $.ajax({
        url:'../ajax/index.php?p=settings',
        type: "POST",
        data: 'action=getLanguageActiveType',
        success: function(response) {
            ajLoaderOff();
            response = JSON.parse(response);
            $.each(response.languageTypeDetails,function(i,row){
                 addradiotxtcnt+='<input type="text" ' +
                     'class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'" ' +
                     'value="" placeholder="Text In '+row.title+'"  ' +
                     'languageid="'+row.language_id+'">';
                 //console.log(addradiotxtcnt);
            });
             addradiotxtcnt+='<input type="text" id="radiopoint_'+(optindex+1)+'" ' +
                 'class="three_fields" value="" placeholder="Points">'
                        +'<span class="addquescnt" onclick="removeTxt($(this))">-</span>'
                    +'</div>';
            $lastitem.after(addradiotxtcnt);
        }
    });
        /*var addradiotxtcnt='';
         addradiotxtcnt= '<div class="row-sec mb15 radioopt radioopt_'+(optindex+1)+'">'
                        +'<label class="fl">&nbsp;</label>'
                        +'<input type="text" class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'"
                            value="" placeholder="Text In Engligh" languageid="9">'
                        +'<input type="text" class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'"
                            value="" placeholder="Text In Russian" languageid="10">'
                        +'<input type="text" class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'"
                            value="" placeholder="Text In Dutch" languageid="11">'
                        +'<input type="text" id="radiopoint_'+(optindex+1)+'" class="three_fields"
                            value="" placeholder="Points">'
                        +'<span class="addquescnt" onclick="removeTxt($(this))">-</span>'
                    +'</div>';*/

}
function addCheckboxTxt($self){
    var formFiled	=	$self.parent();
    var $options	=	$(".checkopt");
    var optindex	=	($options.length-1);
    var $lastitem	=	$(".checkopt_"+($options.length-1));
    var addradiotxtcnt='';
    addradiotxtcnt= '<div class="row-sec mb15 checkopt checkopt_'+(optindex+1)+'">'
                        +'<input type="hidden" id="hdnformfiledid" value="'+formFiled+'" />';
                        +'<label class="fl">&nbsp;</label>';
    ajLoaderOn();
    $.ajax({
        url:'../ajax/index.php?p=settings',
        type: "POST",
        data: 'action=getLanguageActiveType',
        success: function(response) {
            ajLoaderOff();
            response = JSON.parse(response);
            $.each(response.languageTypeDetails,function(i,row){
                 addradiotxtcnt+='<input type="text" ' +
                     'class="three_fields checkopttxt quesoptcheck_'+(optindex+1)+'" ' +
                     'value="" placeholder="Text In '+row.title+'"  ' +
                     'languageid="'+row.language_id+'">';
                 //console.log(addradiotxtcnt);
            });
             addradiotxtcnt+='<input type="text" id="checkpoint_'+(optindex+1)+'" ' +
                    'class="three_fields chkpointvalue" value="" placeholder="Points">'
                        +'<span class="addquescnt" onclick="removeTxt($(this))">-</span>'
                    +'</div>';
            $lastitem.after(addradiotxtcnt);
        }
    });
        /*var addradiotxtcnt='';
         addradiotxtcnt= '<div class="row-sec mb15 radioopt radioopt_'+(optindex+1)+'">'
                        +'<label class="fl">&nbsp;</label>'
                        +'<input type="text"
                            class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'"
                            value="" placeholder="Text In Engligh" languageid="9">'
                        +'<input type="text"
                            class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'"
                            value="" placeholder="Text In Russian" languageid="10">'
                        +'<input type="text" class="three_fields radiobtntxt quesoptradio_'+(optindex+1)+'"
                            value="" placeholder="Text In Dutch" languageid="11">'
                        +'<input type="text" id="radiopoint_'+(optindex+1)+'" class="three_fields"
                            value="" placeholder="Points">'
                        +'<span class="addquescnt" onclick="removeTxt($(this))">-</span>'
                    +'</div>';*/

}
function addRangeNum($self){
    //var formFiled	=	$self.parent();
    var $options	=	$(".numoptrangepts");
    var optindex	=	($options.length-1);
    var $lastitem	=	$(".numoptrangepts_"+($options.length-1));
    var addnumrangeptscnt= '<div class="row-sec mb15 numoptrangepts numoptrangepts_'+(optindex+1)+'">'
                        +'<label class="fl">&nbsp;</label>'
                        +'<input type="text" class="three_fields numoptmin_'+(optindex+1)+'" ' +
                                'value="" placeholder="Min">'
                        +'<input type="text" class="three_fields numoptmax_'+(optindex+1)+'" ' +
                                'value="" placeholder="Max">'
                        +'<input type="text" class="three_fields  numoptrange_'+(optindex+1)+'" ' +
                                'value="" placeholder="Range Points">'
                        +'<input type="hidden" class="three_fields numoptid_'+(optindex+1)+'" value="">'
                        +'<span class="addquescnt" onclick="removeTxt($(this))">-</span>'
                    +'</div>';
    $lastitem.after(addnumrangeptscnt);
}
function removeTxt($self){
    $self.parent().remove();
}
/** Created by: Nesarajan M 
 Created on: 11-Oct-2014 
 Description: To display graph in zoom view
 popTitle: param for popup title
 popPageName: param to load html popup content
 **/
function showGraphZoomPopup(popTitle, popPageName, editId, popUpType) {

    popUpType = 'large-popup';
    var $popupdisp  =   $(".quick_add_pop_disp");
    //Add Class	
    $popupdisp.addClass(popUpType);

    //Set popup title			
    //$(".quick_add_pop_disp .quick_pop_title").html(popTitle);
    $popupdisp.find(".quick_pop_title").html(popTitle);

    //Set popup title			
    $popupdisp.find(".quick_pop_content").html('<div id="largePopUp" ></div>');/*style="height1:auto;"*/

    //$(".quick_add_pop_disp").show();
    $(".popup-mask").show();
    $popupdisp.show();
}

function quickAddCompany() {
    var companyName = $("#companyName").val();

    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=addCompany',
        type: "POST",
        data: 'companyName=' + companyName,
        success: function(response) {
            //Ajax loader off
            ajLoaderOff();

            //split the response message
            splitResponse = response.split("#");
            var $ajaxMsg   =   $('.ajaxMsg');
            //status message
            $ajaxMsg.show();
            $ajaxMsg.html(splitResponse[0]);

            setTimeout(function() {
                $ajaxMsg.slideUp();
            }, 2000);

            //If get success notice only, popup will close and company will displayed in text box
            if (splitResponse[1] == "success") {
                setTimeout(function() {
                    var $clscompany   =   $(".company");
                    $clscompany.find(".custom-combobox-input").val(companyName);
                    $clscompany.find("select#company").append("<option value='" + splitResponse[2] + "'>"
                        + companyName + "</option>"
                    );
                    $("#company option").removeAttr("selected");
                    $("#company option[value='" + splitResponse[2] + "']").attr("selected", "selected");
                    $(".quick_add_pop_disp").hide();
                    $(".popup-mask").hide();
                }, 2000);
            } else {
                $("#companyName").focus();
            }

        }
    });
}

function quickAddCity() {
    var zipCode = $("#zipCode").val();
    var cityName = $("#cityName").val();

    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=addCity',
        type: "POST",
        data: 'zipCode=' + zipCode + '&cityName=' + cityName,
        success: function(response) {
            //Ajax loader off
            ajLoaderOff();

            //split the response message
            splitResponse = response.split("#");
            var $ajaxMsg   =   $('.ajaxMsg');
            //status message
            $ajaxMsg.show();
            $ajaxMsg.html(splitResponse[0]);

            setTimeout(function() {
                $ajaxMsg.slideUp();
            }, 2000);

            //If get success notice only, popup will close and company will displayed in text box
            if (splitResponse[1] == "success") {
                setTimeout(function() {
                    var $clszipCode   =   $(".zipCode");
                    $clszipCode.find(".custom-combobox-input").val(zipCode);
                    $clszipCode.find("select#zipCode").append("<option value='" + splitResponse[2] + "'>"
                        + zipCode + "</option>"
                    );
                    var $cityName  =   $(".cityName");
                    $cityName.find(".custom-combobox-input").val(cityName);
                    $cityName.find("select#city").append("<option value='" + splitResponse[2] + "'>"
                        + cityName + "</option>"
                    );
                    $(".quick_add_pop_disp").hide();
                    $(".popup-mask").hide();
                    if (splitResponse[2] > 0) {
                        $("#city option").removeAttr("selected");
                        $("#city option[value='" + splitResponse[2] + "']").attr("selected", "selected");
                        $("#zipCode option").removeAttr("selected");
                        $("#zipCode option[value='" + splitResponse[2] + "']").attr("selected", "selected");
                    }
                }, 2000);
            } else {
                $("#cityName").focus();
            }

        }
    });
}

function quickAddProfession() {
    var professionName = $("#professionName").val();

    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=addProfession',
        type: "POST",
        data: 'professionName=' + professionName,
        success: function(response) {
            //Ajax loader off
            ajLoaderOff();

            //split the response message
            splitResponse = response.split("#");

            //status message
            var $ajaxmsg    =   $('.ajaxMsg');
            $ajaxmsg.show();
            $ajaxmsg.html(splitResponse[0]);

            setTimeout(function() {
                $ajaxmsg.slideUp();
            }, 2000);

            //If get success notice only, popup will close and company will displayed in text box
            if (splitResponse[1] == "success") {
                setTimeout(function() {
                    var $proffession    =   $(".profession");
                    $proffession.find(".custom-combobox-input").val(professionName);
                    $proffession.find("select#profession").append("<option value='" + splitResponse[2] + "'>"
                        + professionName + "</option>"
                    );
                    /*$("#profession option").removeAttr("selected");*/

                    $proffession.find("option").removeAttr("selected");
                    $("#profession option[value='" + splitResponse[2] + "']").attr("selected", "selected");
                    $(".quick_add_pop_disp").hide();
                    $('.popup-mask').hide();
                }, 2000);
            } else {
                $("#professionName").focus();
            }

        }
    });
}


function changeEmployeeCompany() {

    var clubId = $("#changeEmployeeClubContainer input[name=club_name]:checked").val();

    if (clubId > 0) {
        //Ajax loader on
        ajLoaderOn();

        //To get external popup content 
        $.ajax({
            url: '../ajax/index.php?p=login',
            type: "POST",
            data: 'method=changeEmployeeClub&clubId=' + clubId,
            success: function(response) {
                ajLoaderOff();
                if (response == 1) {
                    location.reload();
                }

            }
        });
    }
}


$(function() {
    //To closed quick add popup on close
    $(".icon-popupcls").click(function() {
        $(".quick_add_pop_disp").hide();
        $(".popup-mask").hide();
    });

    $(".close_link_container").click(function() {
        $(".quick_add_pop_disp").hide();
        $(".popup-mask").hide();

        //Reset hr graph popup data
        resetHRGraphZoomData();
    });



    $(document).on('click', '.pop_cancel_btn', function() {
        $(".popup-holder").hide();
        $(".popup-mask").hide();
    });

});

//Reset hr graph popup data
function resetHRGraphZoomData() {
    var hrGraphViewFlag='';
    if (hrZoomDisplay == 1) {
        hrZoomDisplay = 0;
        $("#hr_graph_display_data").remove();
        if (hrGraphViewFlag == 1) {
            $("#testData").prop('checked', true);
           // hrGraphTestData();
        }
        if (hrGraphViewFlag == 2) {
            $("#fullData").prop('checked', true);
           // hrGraphFullData();
        }
    }
}

/* Quick add popup ends */

/* Show change club list popup for employee login */
function showChangeClubPop() {
    $("#changeClubPopup").show();
    $(".popup-mask").show();
}

/* Show change club list popup for employee login */

$(function() {
    $('.popup-holder').on("click", "input[name=isParent]:checked", function() {
        if ($(this).val() == 0) {
            $("#parentMenuContainer").show();
        } else {
            $("#parentMenuContainer").hide();
        }
    });
});


/* To manage Add/Edit training workload */

function trainingWorkLoadPopupValidation() {
    eFlag = 0;
    var $popajaxmsg =$(".popup-holder .ajaxMsg");
    $popajaxmsg.html('');

    if ($.trim($('#oriWorkLoad').val()) == '') {
        error = 'Please enter Original Workload';
        eFlag = 1;
    }
    else if ($.trim($('#actWorkLoad').val()) == '') {
        error = 'Please enter Actual Workload';
        eFlag = 1;
    }

    if (eFlag == 1) {
        $popajaxmsg.html(error);
        return false
    }
    else {
        return true;
    }
}

function trainingWorkLoadPopup() {
    var oriWorkLoad = $('#oriWorkLoad').val();
    var actWorkLoad = $('#actWorkLoad').val();
    var trainingId = $('#trainingId').val();

    if (!trainingWorkLoadPopupValidation()) {
        return false;
    }

    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=trainingWorkLoad',
        type: "POST",
        data: 'action=saveTraining&oriWorkLoad=' + oriWorkLoad +
                '&actWorkLoad=' + actWorkLoad + '&trainingId=' + trainingId,
        success: function(response) {
            response = JSON.parse(response);
            ajLoaderOff();
            if (response.status == 'success') {
                hideQuickAddPopup();
                location.reload();
            }
        }
    });
}
/* To manage Add/Edit training workload  ends*/

/* To hide popup after success */
function hideQuickAddPopup() {
    $(".popup-holder").hide();
    $(".popup-mask").hide();
}
/* To hide popup after success */

/* hide flash messge */

setTimeout(function() {
    $(".flash").hide();
}, 5000);
/* hide flash messge */

//Send Email
function mailTo(getEmailId) {
    var toEmail = $('.' + getEmailId).val();
    document.location.href = "mailto:" + toEmail;
}

//To hide common flash message
function hideFlashMessage() {
    setTimeout(function() {
        $(".flash").hide();
    }, 5000);
}
function sportmedGraph(
    intervals, heartRate,
    heartRateNoUsed, intervalPlot,
    minValue, maxValue,
    deflHeartRate, minLoadValue, maxLoadValue, crossLine, cycleLoadFirst, cycleLoadSecond, userType, popUpId
) {
    var popUp ='';
    if (popUpId == undefined) {
         popUp = 'sportMedGraph';
    } else if (popUpId == "icon-popup") {
        showQuickAddPop('SportMed Graph', 'graphPopUp', '', 'large');
        setTimeout(function() {
            sportmedGraph(
                intervals,
                heartRate, heartRateNoUsed,
                intervalPlot, minValue, maxValue, deflHeartRate, minLoadValue, maxLoadValue,
                crossLine, cycleLoadFirst, cycleLoadSecond, userType, 'popup'
            );
        }, 300);
        return false;
    }
    else if (popUpId != "") {
         popUp = popUpId;
    }
    else {
         popUp = 'largePopUp';
    }
    var ranges = cycleLoadFirst;
    //var rangeUp = cycleLoadSecond;
    var chart = $('#' + popUp).highcharts({
        tooltip: {
            enabled: false
        },
        xAxis: [{
                title: {text: 'Watt'},
                min: minLoadValue,
                max: maxLoadValue,
                gridLineColor: '#5CD15E',
                gridLineWidth: 0,
                tickInterval: 10,
                labels: {
                    step: 2
                },
                plotLines: intervalPlot
            }],
        yAxis: [{
                min: minValue,
                max: maxValue,
                tickInterval: 10,
                gridLineColor: '#CCCCCC',
                gridLineWidth: 1,
                title: {
                    text: 'HR/ Min'
                }
            }],
        title: {
            text: '    '
        },
        series: [{
                showInLegend: false,
                type: 'line',
                data: deflHeartRate,
                marker: {
                    enabled: true,
                    radius: 5,
                    fillColor: "#810785"
                },
                lineColor: '#2A00FF',
                lineWidth: 1,
                zIndex: "100000",
                states: {
                    hover: {
                        enabled: true,
                        lineWidth: 1
                    }
                }
            }, {
                showInLegend: false,
                regression: false, // This functionality disabled as requested by yves on 13-July-2015 
                regressionSettings: {
                    type: 'linear',
                    color: '#000',
                    lineWidth: 1
                },
                type: 'scatter',
                name: 'Heart Rate',
                data: crossLine,
                zIndex: "5000",
                allowPointSelect: true,
                marker: {
                    radius: 0,
                    symbol: 'square',
                    fillColor: '#FF0000'
                }
            }, {
                showInLegend: false,
                data: cycleLoadFirst,
                type: 'arearange',
                lineWidth: 0,
                linkedTo: ':previous',
                fillOpacity: 0.3,
                fillColor: '#FFA6A6',
                zIndex: "-2005"
            }, {
                showInLegend: false,
                data: cycleLoadSecond,
                type: 'arearange',
                lineWidth: 0,
                linkedTo: ':previous',
                fillOpacity: 0.3,
                fillColor: '#F5D8EC',
                zIndex: "-2005"
            }, {
                showInLegend: false,
                type: 'scatter',
                name: ' ',
                data: heartRateNoUsed,
                zIndex: "10000",
                marker: {
                    radius: 3,
                    symbol: 'square',
                    fillColor: '#FFFFFF',
                    lineWidth: 1,
                    lineColor: '#787878'
                },
                point: {
                    events: {
                        click: function(e) {
                            if (userType == 6) {
                                var point = e.point;
                                var pointsValue = e.point.series.points.indexOf(point);
                                e.point.series.points[pointsValue].update({
                                    marker: {fillColor: '#FF0000'}
                                });
                                currentIndexValue = pointsValue;
                                skipValues = 1;
                                getAdjustSportmedGraphData(0);//used value in param 1 or 0

                                var d = $($.map(c, function(el) {
                                    return $.makeArray(el)
                                }));
                                console.log(JSON.stringify(chart.highcharts().series[0]));
                                e.point.series.points.indexOf(point).update({marker: {fillColor: '#FF0000'}});
                            }
                        }
                    }
                }
            }, {
                showInLegend: false,
                regression: true, // this should be enabled nesa
                regressionSettings: {
                    type: 'linear',
                    color: '#000',
                    lineWidth: 1
                },
                type: 'scatter',
                name: 'Heart Rate',
                data: heartRate,
                zIndex: "5000",
                marker: {
                    radius: 3,
                    symbol: 'square',
                    fillColor: '#FF0000'
                },
                point: {
                    events: {
                        click: function(e) {
                            if (userType == 6) {
                                var point = e.point;
                                var pointsValue = e.point.series.points.indexOf(point);
                                e.point.series.points[pointsValue].update({
                                    marker: {fillColor: '#FFFFFF', lineWidth: 1, lineColor: '#787878'}
                                });
                                currentIndexValue = pointsValue;
                                skipValues = 1;
                                getAdjustSportmedGraphData(1);//used value in param 1 or 0

                                var d = $($.map(c, function(el) {
                                    return $.makeArray(el)
                                }));
                                console.log(JSON.stringify(chart.highcharts().series[0]));
                                e.point.series.points.indexOf(point).update({
                                    marker: {fillColor: '#FFFFFF', lineWidth: 1, lineColor: '#787878'}
                                });
                            }
                        }
                    }
                }
            }],
        chart: {
            marginTop: 30,
            events: {
                load: function() {
                    this.series[0].data[0].update({marker: {enabled: false}});
                },
                click: function() { //e
                    if (popUpId == "popup") {

                    } else {
                        if (showSportmedZoom == 1) {
                            showGraphZoomPopup('SportMed Graph', 'graphPopUp', '', 'large');
                            sportmedGraph(
                                intervals, heartRate, heartRateNoUsed, intervalPlot, minValue,
                                maxValue, deflHeartRate, minLoadValue, maxLoadValue, crossLine,
                                cycleLoadFirst, cycleLoadSecond, userType, 'popup'
                            );
                            showSportmedZoom = 0;
                        }
                    }
                }
            },
            plotBorderWidth: 1
        }
    });
}
function toggleShowIant() {

    var showIant = ($("#toggleShowIant").attr('custom-value') == 0);
    var $toggleShowIant= $(".toggleShowIant");
    var $heartRateGraph= $(".heartRateGraph");
    $heartRateGraph.highcharts().series[1].show();
    if (hrZoomDisplay == 1) {
        $("#largePopUp").highcharts().series[1].show();
    }
    $toggleShowIant.val("Hide IANT");
    $toggleShowIant.attr("custom-value", 0);

    if (showIant) {
        $heartRateGraph.highcharts().series[1].hide();
        if (hrZoomDisplay == 1) {
            $("#largePopUp").highcharts().series[1].hide();
        }
        $toggleShowIant.val("Show IANT");
        $toggleShowIant.attr("custom-value", 1);
    }


}




//To zoom the graph on icon
function zoomGraph(idName) {
    if (idName == 'sportMedGraph') {
        showSportmedZoom = 1
    }
    $("#" + idName).highcharts().options.chart.events.click();
}


/******
 Author: Punitha Subramani
 Created Date:25-Sep-2014
 Description:titleGraph is title of the graph, LeftCaption for left label,
        bottomCaption for bottom label , Passing total datas in totalDatas
 Type Head 0 => Test Data,1 => Full Data
 *******/

showIant = 0;

function graphHeartRate(
    dataValue, intervalPlotsHeartRate, minYaxis,
    maxYaxis, typeAxis, minXAxis, iant, coolDown, coolDownTime, popUpId
) {
    var popUp='';
    if (popUpId == undefined) {
         popUp = 'heartRateGraph';
    } else {
         popUp = 'largePopUp';
    }

    showIant = ($("#toggleShowIant").attr('custom-value') == 0);
    $('#' + popUp).highcharts({
        chart: {
            marginTop: 30,
            plotBorderWidth: 1,
            events: {
                click: function() {//e
                    if (popUpId == "popup") {

                    } else {
                        showGraphZoomPopup('Heart Rate Graph', 'graphPopUp', '', 'large');
                        if (hrZoomDisplay == 1)
                           // showHrAdditionalData();
                        graphHeartRate(
                            dataValue, intervalPlotsHeartRate, minYaxis,
                            maxYaxis, typeAxis, minXAxis, iant,
                            coolDown, coolDownTime, 'popup'
                        );
                    }
                }
            }
        },
        tooltip: {
            enabled: false
        },
        title: {
            text: ' '
        },
        xAxis: {
            plotBands: [typeAxis, coolDown],
            type: 'time',
            title: {
                text: 'Time'
            },
            min: minXAxis,
            max: coolDownTime,
            tickInterval: 60,
            labels: {
                    formatter: function() {

                        if (this.value == 0) {
                            return "";
                        } else {
                        if (minXAxis == 0) {

                            return Math.floor(this.value / 60);
                        } else {
                            if (Math.floor(this.value / 60) - 2 == 0) {
                                return "";
                            } else {
                                return Math.floor(this.value / 60) - 2;
                            }
                        }
                    }


                }
            },
            plotLines: intervalPlotsHeartRate
        },
        yAxis: {
            min: minYaxis,
            max: maxYaxis,
            tickInterval: 10,
            title: {
                text: 'Heart rate'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            line: {
                marker: {
                    enabled: false
                }
            }
        },
        series: [
            {
                data: dataValue,
                turboThreshold: 3000, //accept longer character in datas
                lineColor: '#FF2727',
                lineWidth: 1,
                states: {
                    hover: {
                        enabled: true,
                        lineWidth: 1
                    }
                }
            }, {
                data: iant,
                name: 'showIant',
                visible: showIant,
                marker: {
                    enabled: true,
                    radius: 5,
                    symbol: 'circle'
                }
            },
{
                data: iant,
                name: 'showIant',
                visible: false,
                marker: {
                    enabled: true,
                    radius: 5,
                    symbol: 'circle'
                }
            }
        ]
    });
    //toggleShowIantData();
}

//Show Toggle to show heart rate
function showHearRate() {
    var $heartRateGraph=  $("#heartRateGraph");
    $heartRateGraph.removeClass("float-left");
    $heartRateGraph.addClass("float-none");
    $heartRateGraph.highcharts().setSize(660, 360);
    $heartRateGraph.show();
    $("#sportMedGraph").hide();
}

//Show Sport Med graph
function showSportMed() {
    var $sportMedGraph= $("#sportMedGraph");
    $sportMedGraph.removeClass("float-right");
    $sportMedGraph.addClass("float-none");
    $sportMedGraph.highcharts().setSize(660, 360);
    $sportMedGraph.show();
    $("#heartRateGraph").hide();
}

function displayGraphTypeSession(sessionValue, userIdPg, testId) {
    $.ajax({
        url: '../ajax/index.php?p=testScreen',
        type: "POST",
        data: 'action=displayGraphTypeSession&sessionValue=' + sessionValue,
        success: function() {//response
            ajLoaderOff();
            if (sessionValue == 1) {
                window.location.href = "index.php?p=heartRateGraph&id=" + userIdPg + "&testId=" + testId + "#graph";
            } else if (sessionValue == 2) {
                window.location.href = "index.php?p=fitGraph&id=" + userIdPg + "&testId=" + testId + "#graph";
            } else {
                showHeartRateSportMed(userIdPg)
            }
        }
    });

}
//Show Heart rate and Sport Med graph
function showHeartRateSportMed(pageName) {
    //var sportMedGraphHeight = "360";
    //var heartRateGraphHeight = "340";

    if (pageName == "heartRateGraph") {
         //sportMedGraphHeight = "360";
         //heartRateGraphHeight = "340";
    }
    $("#sportMedGraph").show();
    $("#heartRateGraph").show();
}


ajLoaderOff();
/**
 * To display flash message dynamically.
 * param {type} msgTxt (messages that to be displayed).
 * param {type} msgType (success or failure).
    Add class "success-msg" for any success messages and "faliure-msg" for failures.
 */
function flashMsgDisplay(msgTxt, msgType) {
    $('#flashMsg').attr('class', '').addClass(msgType).html(msgTxt).fadeIn().delay(5000).fadeOut('slow');
}

//Send selected value to hidden element
function sendDataToHidden(data, destinate) {
    $("#" + destinate).val(data);
}

// Get User Edit mode page
function showUserEditPage() {
    ajLoaderOn();
    $.ajax({
        url: '../ajax/index.php?p=userProfile',
        type: "POST",
        data: 'action=getUserProfilePage',
        success: function(response) {
            ajLoaderOff();
            window.location.href = response;
        }
    });
}

/* Hide dialog box on key up */
$(document).keyup(function(e) {

    if (e.keyCode == 27) { /* When use Escape key actions */

        //Hide alert dialog boxes
        $(".com-alert-msg").hide();
        $(".popup-mask").hide();

        //Hide alerts box
        $("#customPopbox").css({width: '0%', opacity: '0'});
        $("#customPopoverlay").hide();

        //Quick add popup hide
        $(".quick_add_pop_disp").hide();

    }
});
/*
function goback() {//backPage
    //window.location.href=backPage;
    history.go(-1);
}

function backToPage() {
    history.go(-1);
}*/

//BMI calculation
/*
function bmiCalculate(integer) {
    var weight = $("#weightBmi").val();//weight value
    var height = $("#heightBmi").val();//height value

    if (weight > 0 && height > 0) {

        //BMI = Weight (kg) / (height(m) * height(m))  , weight in kg, height in meters
        height = height / 100; //height into meters
        var bmiValue = weight / (height * height);
        $("#bmi").val(bmiValue.toFixed(2));
    }
}*/
//BMI calculation



/**		
 * Author: Punitha Subramani
 * Descripton : Zip Code and City
 * Created On : 22 Jan 2015
 **/
function uiwidgetcustomzipCode(zipcode) {
    var zipCode = $(zipcode).val();
    var $widgetcity= $("input.ui-widget-custom-city");
    var $slctcity= $("select#city");
    if (zipCode == "") {
        $slctcity.val("");
        $widgetcity.val("");
        $widgetcity.css("display", "none");
    } else {
        var dataString = "cityzip=" + zipCode;
        $.ajax({
            type: "POST",
            url: "../ajax/index.php?p=city",
            data: dataString,
            cache: false,
            success: function(result) {
                var results = result.split("___");
                var positions = $widgetcity.position();
                $slctcity.val("");
                $widgetcity.val("");
                $slctcity.html(results[0]);
                $widgetcity.html(results[1]);
                $widgetcity.attr("style",
                    "width: 186px; display: block;left:" + positions.left + "px;top:" + (positions.top + 30) + "px;"
                );
            }
        });
    }
}

function autoCompleteLi(listElement, tagValue) {
    var liIdSplit = $(listElement).attr("id");
    var liId = liIdSplit.split("-");
    $("select#" + tagValue).val(liId[2]);
    $("input.ui-widget-custom-" + tagValue).val($(listElement).text());
    $("ul.ui-widget-custom-" + tagValue).css("display", "none");
}



// Fitgraph reanalysis and save data begins

$(function() {

    $(document).on('click', "#btnSaveTestResult", function(e) {

        var is_reanalyzed_test = $("#is_reanalyzed_test").val();
        if(is_reanalyzed_test > 0){
            e.preventDefault();
            var currentUserId=$("#currentUserId").val();
            showQuickAddPop('Training Program','trainingType',currentUserId);
        }else{
            sportMedSave();
        }
        //
        // To enable popup
        /*e.preventDefault();
        var currentUserId=$("#currentUserId").val();
        showQuickAddPop('Training Program','trainingType',currentUserId);*/
    });


    //Change graph edit action type
    $(document).on('click',
        '.sportmed_graph_edit_icons .toggle-icon, .sportmed_graph_edit_icons .edit_graph_action_text',
        function() {
            var $graphedit =$("#graph_edit_action");
            var editAction = $graphedit.val();
            if (editAction == 'IANT') {
                $graphedit.val('TRIM');
                $(".edit_graph_action_text").html('TRIM');
            }
            else if (editAction == 'TRIM') {
                $graphedit.val('IANT');
                $(".edit_graph_action_text").html('IANT');
            }
        }
    );

    //Adjust sportmed graph data 
    $(document).on('click', '.sportmed_graph_edit_icons .up_down_icon', function() {

        var $graphedit =$("#graph_edit_action");
        var editAction = $graphedit.val();

        if (editAction == 'IANT') {
            if ($(this).attr('actionvalue') == 1)
                editActionValue = 2;
            else
                editActionValue = 1;
        }
        else if (editAction == 'TRIM') {
            if ($(this).attr('actionvalue') == 1)
                editActionValue = 4;
            else
                editActionValue = 3;
        }

        $("#editActionValue").val(editActionValue);
        skipValues = 0;
        getAdjustSportmedGraphData(1);//used value in param 1 or 0

    });

    //Adjust sportmed graph data 
    $("#reanalyze_user_test").click(function() {
        confirms(LANG['reanalyzeTest'], function(confirmResult) {
            if (confirmResult == "true") {
                closeMsg();
                var userId = $("#currentUserId").val();
                var userTestId = $("#currentTestId").val();
                //Ajax loader on
                ajLoaderOn();

                //To get external popup content 
                $.ajax({
                    url: '../ajax/index.php?p=cardioPro',
                    type: "POST",
                    data: "action=reanalyzeTest&userId=" + userId + "&userTestId=" + userTestId,
                    success: function() {//response
                        window.location = "index.php?p=fitGraph&id=" + userId;
                    },
                    error: function() {//response
                        window.location = "index.php?p=fitGraph&id=" + userId;
                    }
                });
            } else {
                closeMsg();
            }
        });
    });

    //Put print in waiting list 
    $("#put_in_waitinglist").click(function() {
        var userId = $("#currentUserId").val();
        var userTestId = $("#currentTestId").val();
        //Ajax loader on
        ajLoaderOn();

        //To get external popup content 
        $.ajax({
            url: '../ajax/index.php?p=cardioPro',
            type: "POST",
            dataType: "json",
            data: "action=put_in_waitinglist&userId=" + userId + "&userTestId=" + userTestId,
            success: function(response) {
                //Ajax loader off
                ajLoaderOff();
                if (response.status == 'success') {
                    flashMsgDisplay(LANG['alertPutWaitingList'], 'success-msg');
                    window.parent.location.reload();
                }
            }
        });
    });
    $(document).on('change','.phase_name,.group_name',function(){
        var phase_name = $('.phase_name').val();
        var group_name = $('.group_name').val();
        if(phase_name==2 && group_name==1){
            $('.activityques').css('display','block');
            $('.questiontext').css('display','none');
        }
        else{
            $('.questiontext').css('display','block');
            $('.activityques').css('display','none');
        }
    });
    $(document).on('change','.questype',function(){
        var questype = $('.questype').val();
        if(questype==1){
            $('.ansoptions1').css('display','block');
            $('.rangebasedpts').css('display','block');
            $('.ansoptions2').css('display','none');
            $('.ansoptions3').css('display','none');
            $('.ansoptions4').css('display','none');
            $('.ansoptions5').css('display','none');
            $('.ansoptions6').css('display','none');
        }
        if(questype==2){
            $('.ansoptions2').css('display','block');
            $('.rangebasedpts').css('display','block');
            $('.ansoptions1').css('display','none');
            $('.ansoptions3').css('display','none');
            $('.ansoptions4').css('display','none');
            $('.ansoptions5').css('display','none');
            $('.ansoptions6').css('display','none');
        }
        if(questype==3){
            $('.ansoptions3').css('display','block');
            $('.rangebasedpts').css('display','block');
            $('.ansoptions1').css('display','none');
            $('.ansoptions2').css('display','none');
            $('.ansoptions4').css('display','none');
            $('.ansoptions5').css('display','none');
            $('.ansoptions6').css('display','none');
        }
        if(questype==4){
            $('.ansoptions4').css('display','block');
            $('.rangebasedpts').css('display','block');
            $('.ansoptions1').css('display','none');
            $('.ansoptions2').css('display','none');
            $('.ansoptions3').css('display','none');
            $('.ansoptions5').css('display','none');
            $('.ansoptions6').css('display','none');
        }
        if(questype==5){
            $('.ansoptions5').css('display','block');
            $('.rangebasedpts').css('display','block');
            $('.ansoptions1').css('display','none');
            $('.ansoptions2').css('display','none');
            $('.ansoptions3').css('display','none');
            $('.ansoptions4').css('display','none');
            $('.ansoptions6').css('display','none');
        }
        if(questype==6){
            $('.ansoptions6').css('display','block');
            $('.rangebasedpts').css('display','block');
            $('.ansoptions1').css('display','none');
            $('.ansoptions2').css('display','none');
            $('.ansoptions3').css('display','none');
            $('.ansoptions4').css('display','none');
            $('.ansoptions5').css('display','none');
        }
    });
    $(document).on('change','.quesicon',function(){
        var formData = new FormData(_("addques"));
        formData.append('imagefile', 'image');
        formData.append('image', $('input[type=file]')[0].files[0]);
        $.ajax({
            url: '../ajax/index.php?p=settings',
            type: "POST",
            data: formData,
            success: function(response) {
                response = JSON.parse(response);
                ajLoaderOff();
                if (response.status == 'success') {
                    hideQuickAddPopup();
                    location.reload();
                }
            }
        });
    });
    $(document).on('change','#phasetype,#grouplist',function(){
        var phaseid = $('#phasetype').val();
        var groupid = $('#grouplist').val();
        getPhasQroupQuestion(phaseid,groupid);
    });

});

var editActionValue = $("#editActionValue").val();
var currentIndexValue = -1;
var skipValues = 0;

//used value in param 1 or 0
function getAdjustSportmedGraphData(usedValue) {

    var editActionValue = $("#editActionValue").val();
    var currentTestId = $("#currentTestId").val();
    var currentUserId = $("#currentUserId").val();

    var gender = 'F';
    if ($("#gender").val() == 'male')
        gender = 'M';

    var adjustDataUrl = 'gender=' + gender;
    adjustDataUrl += '&age=' + $("#ageOnTest").val();
    adjustDataUrl += '&weight=' + $("#weightOnTest").val();
    adjustDataUrl += '&test_machine=1';
    adjustDataUrl += '&start_level=' + $("#startLevel").val();
    adjustDataUrl += '&defl_run_load=' + $("#iantP").val();
    adjustDataUrl += '&defl_run_HR=' + $("#iantHR").val();
    /*adjustDataUrl += '&max_run_load=' + $("#maxP").val();
     adjustDataUrl += '&max_run_HR=' + $("#maxHR").val();*/
    adjustDataUrl += '&intervals_count=' + $("#intervalsCount").val();
    adjustDataUrl += '&spm_points=' + $.trim($("#spmPoints").html());
    adjustDataUrl += '&action=' + editActionValue;
    adjustDataUrl += '&currentTestId=' + currentTestId;
    adjustDataUrl += '&currentUserId=' + currentUserId;
    adjustDataUrl += '&indexValue=' + currentIndexValue;
    adjustDataUrl += '&usedValue=' + usedValue;
    if (skipValues == 1) {
        adjustDataUrl += '&skipValues=' + skipValues;
    }

    /**
     * @param response.system_error This is system error
     * @param response.error_msg This is  error message
     * @param response.minHrCommon This is  Min heart rate common
     * @param response.spmPoints This is spmPoints
     * @param response.intervalsCount This is intervals count
     * @param response.iantHR This is iantHR
     * @param response.iantWatt This is iantWatt
     * @param response.iantP This is iantP
     * @param response.regrCoeff This is regrCoeff
     * @param response.fitnessLevel This is fitness level
     * @param response.countIntervals This is count Intervals
     * @param response.maxHrCommon This is max heart rate common
     * @param response.deflAxis This is defl Axis
     * @param response.minLoadValue This is min load value
     * @param response.maxLoadValue This is max load value
     * @param response.cycleLoadFirst This is cycle load first
     * @param response.cycleLoadSecond This is cycle load second
     * @param response.iantTime This is iantTime
     * @param response.deflHR This is deflHR
     */
    //Ajax loader on
    ajLoaderOn();

    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=getAdjustSportmedData&testpage=sportmed1',
        type: "POST",
        dataType: "json",
        data: adjustDataUrl,
        success: function(response) {

            //Ajax loader off
            ajLoaderOff();

            if(response.system_error) { return showMFPJsonSTopAlert(); }

            if(response.error == 1) {
                flashMsgDisplay('We found following error when accessing point ' +
                    'system for this service: '+response.error_msg, 'failure-msg'
                );
                return false;
            }

            if (response.heartRate.length <= 2) {
                flashMsgDisplay(LANG['unableToShowGraph'], 'failure-msg');
                return false;
            }


            $("#spmPoints").html(response.spmPoints);
            $("#intervalsCount").val(response.intervalsCount);
            $("#iantHR").val(response.iantHR);
            $("#iantWatt").val(response.iantWatt);
            $("#iantP").val(response.iantP);
            $("#regrCoeff").val(response.regrCoeff);
            $("#fitnessLevel").val(response.fitnessLevel);



            if (response.heartRate.length > 2)
                response.heartRate = JSON.parse(response.heartRate);
            if (response.heartRateNoUsed.length > 2)
                response.heartRateNoUsed = JSON.parse(response.heartRateNoUsed);
            if (response.intervalPlot.length > 2) {
                response.intervalPlot = JSON.parse(response.intervalPlot);
            }
            if (response.heartRateCrossLine.length > 2)
                response.heartRateCrossLine = JSON.parse(response.heartRateCrossLine);

            popUpId = undefined;
            if (hrZoomDisplay == 1)
                popUpId = 'fitgraphPopup';

            //Render sportmed graph	
            sportmedGraph(
                response.countIntervals, response.heartRate,
                response.heartRateNoUsed, response.intervalPlot, response.minHrCommon,
                response.maxHrCommon, response.deflAxis, response.minLoadValue,
                response.maxLoadValue, response.heartRateCrossLine, response.cycleLoadFirst,
                response.cycleLoadSecond, response.status, popUpId
            );

            //Render heartrate graph
            if (response.heartRateValue.length > 2)
                response.heartRateValue = JSON.parse(response.heartRateValue);
            if (response.intervalPlotsWarmUp.length > 2)
                response.intervalPlotsWarmUp = JSON.parse(response.intervalPlotsWarmUp);

            /*graphHeartRate(response.heartRateValue,
            response.intervalPlotsWarmUp, response.minHrCommon, response.maxHrCommon,
            response.warmupPeriodStyle, response.warmupPeriod, response.deflAxisHr,
            response.coolDownStartEnd, response.coolDownStart);*/

            showIantAnalyzedPoint(response.iantTime, response.deflHR);
            //$("#heartRateGraph").show();

        },
        error: function() {//response
            //Ajax loader off
            ajLoaderOff();
        }
    });
}

function invalidJsonToValidJson(invalidJson) {
    var jsontemp = invalidJson.replace((/([\w]+)(:)/g), "\"$1\"$2");
    var correctjson = jsontemp.replace((/'/g), "\"");
    correctjson = JSON.parse(correctjson);
    return correctjson;
}

//save test analysis data after popup
/*function sportMedSave_withpopup(){

        $(".ajaxMsg").hide();
        if(1) {

            var currentTestId = $("#currentTestId").val();
            var currentUserId = $("#currentUserId").val();
            var fitnessLevel = $("#fitnessLevel").val();
            var maxHR = $("#maxHR").val();
            var iantHR = $("#iantHR").val();
            var maxP = $("#maxP").val();
            var iantP = $("#iantP").val();
            var regrCoeff = $("#regrCoeff").val();
            var keepFit = $("#keep_fit").val();
            var testDate = $("#testDate").val();
            var testDate = $("#testDate").val();

            var trainingType = 0;
            if($("#endurance").prop('checked') == true)
                trainingType = 1;

            var weeks_points_to_improve = 0;
            if($("#improve").prop('checked') == true)
                weeks_points_to_improve = 1;

            var start_training_on_date = 0;
            if($("#onDate").prop('checked') == true) {
                start_training_on_date = 1;
            }


            var training_date = $('#trainingDate').val();

            if(start_training_on_date == 1 && $.trim(training_date) == '') {
                $(".ajaxMsg").html('Choose training date').show();
                return false;
            }


            var dataActivityList = [];
            $('.activity_lists:checked').each(function() {
                dataActivityList.push($(this).val());
            });

            //Ajax loader on
            ajLoaderOn();

            //To get external popup content
            $.ajax({
                url: '../ajax/index.php?p=cardioPro',
                type: "POST",
                data: 'action=saveHeartRateData&page_name=sportmed&fitnessLevel='+fitnessLevel+
                '&currentTestId='+currentTestId+'&maxHR='+maxHR+'&iantHR='+iantHR+'&maxP='+maxP+
                '&iantP='+iantP+'&regrCoeff='+regrCoeff+'&testDate='+testDate+'&currentUserId=
                '+currentUserId+'&training_type='+trainingType+'&keep_fit='+keepFit+
                '&weeks_points_to_improve='+weeks_points_to_improve+
                '&start_training_on_date='+start_training_on_date+
                '&training_date='+training_date+'&dataActivityList='+dataActivityList,
                success: function(response) {
                    //Ajax loader off
                    hideQuickAddPopup();
                    ajLoaderOff();
                    confirms(LANG['alertAnalysisDone'], function(confirmResult) {
                        if (confirmResult == "true") {
                            //Ajax loader on
                            ajLoaderOn();
                            window.location = 'index.php?p=managePointsEdit&id='+currentUserId;
                            closeMsg();
                        } else {
                            //Ajax loader on
                            ajLoaderOn();
                            location.reload();
                            closeMsg();
                        }
                    });
                }
            });
        }
}*/

/*     Note: All the popup removed as per change request by client
 Now clicking save do not ask for any popup data and background validation
 done only for club activity exist to choose activities
 */
function sportMedSave() {



    var currentTestId = $("#currentTestId").val();
    var currentUserId = $("#currentUserId").val();
    var fitnessLevel = $("#fitnessLevel").val();
    var maxHR = $("#maxHR").val();
    var iantHR = $("#iantHR").val();
    var maxP = $("#maxP").val();
    var iantP = $("#iantP").val();
    var regrCoeff = $("#regrCoeff").val();
    var keepFit = $("#keep_fit").val();
    var testDate = $("#testDate").val();
    var coach_advice = $("#coach_advice").val();

    var trainingType = 0;
    if ($("#train_endurance").prop('checked') == true)
        trainingType = 1;

    var weeks_points_to_improve = 1;
    var start_training_on_date = 1;
    var dataActivityList = '';
    var training_date = '';

    //Ajax loader on
    ajLoaderOn();


    var is_reanalyzed_test = $("#is_reanalyzed_test").val();
    //var radioName = $(this).find("input[type='radio']").attr("name");

    //alert(radioName);
    var is_reanalyzed_test_param = '&is_reanalyzed_test='+is_reanalyzed_test;
        if(is_reanalyzed_test > 0){
            var reAnalyze_option = $('input[name=start_training_on_date]:radio:checked').attr('id');
            if(reAnalyze_option == 'onDate'){
                var reAnalyze_test_date = $('#trainingDate').val();
            }
            is_reanalyzed_test_param = '&is_reanalyzed_test='+is_reanalyzed_test+
                '&reAnalyze_option='+reAnalyze_option+
                '&reAnalyze_test_date='+reAnalyze_test_date;
        }else{

        }
        //alert(is_reanalyzed_test_param);
    //	return false;
    //To get external popup content 
    $.ajax({
        url: '../ajax/index.php?p=cardioPro',
        type: "POST",
        dataType: 'json',
        data: 'action=saveHeartRateData&page_name=sportmed&fitnessLevel=' + fitnessLevel +
        '&currentTestId=' + currentTestId + '&maxHR=' + maxHR +
        '&iantHR=' + iantHR + '&maxP=' + maxP + '&iantP=' + iantP +
        '&regrCoeff=' + regrCoeff + '&testDate=' + testDate +
        '&currentUserId=' + currentUserId + '&training_type=' + trainingType +
        '&keep_fit=' + keepFit + '&weeks_points_to_improve=' + weeks_points_to_improve +
        '&start_training_on_date=' + start_training_on_date + '&training_date=' + training_date +
        '&dataActivityList=' + dataActivityList + '&coach_advice=' +
        coach_advice+is_reanalyzed_test_param,
        success: function(response) {

            //Ajax loader off
            ajLoaderOff();

            if (response.system_error == 1) {
                return showMFPJsonSTopAlert();
            }

            //Return error if no activities found for the member club	
            if (response.status == 'error' && response.result == 'no_activity_found') {
                flashMsgDisplay(LANG['alertNoActivityFound'], 'failure-msg');
                return false;
            }

            //Return error if no activities found for the member club	
            if (response.status == 'error' && response.result == 'save_training_data_failed') {
                alerts(response.message);
                $("#btnSaveTestResult").hide();
                $(".sportmed_graph_edit_icons").hide();
                return false;
            }

            if (response.status == 'success' && response.result != 1) {
                $("#pdf_icon_dialog").remove();
            }
            flashMsgDisplay(LANG['alertAnalysisDone'], 'success-msg');
            $('#test_analysis_success_dialog').dialog({
                modal: true,
                draggable: false,
                close: function() {//event, ui
                    //Ajax loader on
                    ajLoaderOn();
                    //location.reload();
                    window.parent.location.reload();
                }
            });

        }
    });
}

/* function to Validate The Curl result, is ,exe is running or not */
function validateMFP_JSON(MFP_JSON) {
    return !(("system_error" in MFP_JSON) && (( MFP_JSON.system_error == '1')));

}

// Fitgraph reanalysis and save data ends

//Show MFP JSON stop alert
function showMFPJsonSTopAlert() {
    flashMsgDisplay(LANG['mfpJsonNotStarted'], 'failure-msg');
    return false;
}

//Show IANT analyzed point
function showIantAnalyzedPoint(iantTime, deflHR) {
    var id = '#heartRateGraph';

    if(hrZoomDisplay == 1)
         id = '#largePopUp';

    $(id).highcharts().series[2].setData([[iantTime,parseInt(deflHR)]]);
    $(id).highcharts().series[1].show();
    $(id).highcharts().series[2].show();

    $(".toggleShowIant").attr("custom-value", 1);
    toggleShowIant();

}
/*PK - Added save Language & translation 20160205*/
function saveLanguageTrnslation(id){
    var manageMenuLanguageTypeId =$("#manageMenuLanguageTypeId").val();
    var manageMenutranslationkeyTypeId =$("#manageMenutranslationkeyTypeId").val();
	  
    var menuDisplayName =$("#menuDisplayName").val();
    $.ajax({
         url: '../ajax/index.php?p=translation',
         type: "POST",
         data:{
             'action':'saveLanguageTranslation',
             manageMenuLanguageTypeId:manageMenuLanguageTypeId,
             manageMenutranslationkeyTypeId:manageMenutranslationkeyTypeId,
             menuDisplayName:menuDisplayName,
             'id':id
         },
         success: function() { //response
             ajLoaderOff();
             flashMsgDisplay(LANG['updatetranslationSuccess'], 'success-msg');
             setTimeout(function() {
                 location.reload();
             }, 1500);
         }
     });
}

/*PK - Added save Language & translation 20160205*/
function saveLanguageTrnslationNewEntry(id){
    var translation_key =$("#translation_key").val();
    var translation_key_description =$("#translation_key_description").val();
	if(translation_key == '' || translation_key == 'undefined')
	{
		alert('Please select translation key');
	}
	else if(translation_key_description == '' || translation_key_description == 'undefined')
	{
		alert('Please add translation key description');
	}
	else
	{
		$.ajax({
			 url: '../ajax/index.php?p=translation',
			 type: "POST",
			 data:{
				 'action':'saveLanguageTranslationNewEntry',
				 translation_key:translation_key,
				 translation_key_description:translation_key_description,
			},
			success: function() { //response
				 ajLoaderOff();
				 flashMsgDisplay(LANG['updatetranslationSuccess'], 'success-msg');
				 setTimeout(function() {
					 location.reload();
				 }, 1500);
			 }
		 });
	}
    
}

/*PK - Added save Language & translation 20160208*/
function displayLanguageTrnslation(){
    var manageMenuLanguageTypeId =$("#manageMenuLanguageTypeId").val();
    var manageMenutranslationkeyTypeId =$("#manageMenutranslationkeyTypeId").val();
    var menuDisplayName =$("#menuDisplayName").val();
    $.ajax({
         url: '../ajax/index.php?p=translation',
         type: "POST",
         data:{'action':'getTranslationLanguageType'},
        success: function(response) {
             $('.trnslang').html(response);
         }
     });
}
/*PK - Added save Language & translation 20160208*/
function saveLanguage(id){
    var manageMenuLanguage =$("#manageMenuLanguage").val();
    // var menuDisplayName =$("#menuDisplayName").val();
    var checkval=$("input[name=isParent]:checked").val();
    $.ajax({
         url: '../ajax/index.php?p=language',
         type: "POST",
         data:{'action':'saveLanguage',
             manageMenuLanguage:manageMenuLanguage,
             checkval:checkval,'id':id
         },
         success: function() {//response
             ajLoaderOff();
             flashMsgDisplay(LANG['updatelanguageSuccess'], 'success-msg');
             setTimeout(function() {
                 location.reload();
             }, 1500);
         }
     });
}


/*PK - Added save Language & translation 20160208*/
function displayLanguage(){
    //var manageMenuLanguage =$("#manageMenuLanguageT").val();
    $.ajax({
         url: '../ajax/index.php?p=language',
         type: "POST",
         data:{'action':'getLanguageType'},
        success: function(response) {
          $('.langappnt').html(response);
         }

     });
}

function editLanguage(title,page,id){
    /**
     * @param response.total_records This is total records
     */
    showQuickAddPop(title,page,id);
    $.ajax({
         url: '../ajax/index.php?p=language',
         type: "POST",
          dataType: 'json',
         data:{'action':'editLanguageType','id':id},
        success: function(response) {
             var title=(response.total_records.title);
             var active=(response.total_records.active);
             $('#manageMenuLanguage').val(title);
             if(active == 1){
                  $('input[name="isParent"][value="1"]').attr('checked','checked');
                  $('#is_parent_yes').attr('checked',true);
             }else{
                 $('input[name="isParent"][value="0"]').attr('checked','checked');
                  $('#is_parent_no').attr('checked',true);
             }

         }

     });
}
function editTranslation(title,page,id){
    /**
     * @param response.total_records.transkey This is transkey
     * @param response.total_records.translation_text This is translation text
     */
    showQuickAddPop(title,page,id, null, function(){
    $.ajax({
         url: '../ajax/index.php?p=translation',
         type: "POST",
          dataType: 'json',
         data:{'action':'editTranslationType','id':id},
        success: function(response){
             $('#manageMenuLanguageTypeId').val(response.total_records.languageid);
             $('#manageMenutranslationkeyTypeId').val(response.total_records.transkey);
             $('#menuDisplayName').val(response.total_records.translation_text);
         }

       });
    });
}
 function showQuickdeletePop(popTitle, popPageName, DeleteId){
    //ajLoaderOn();

      $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data:{'action':'deletelanguage','DeleteId':DeleteId},
        success: function(response) {
             if (response.status == '1') {
                location.reload();
             }
             displayLanguage();
             location.reload();
        }
    });
}
 
 function showQuickdelete(popTitle, popPageName, DeleteId){
    //ajLoaderOn();

      $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data:{'action':'deletetranslation','DeleteId':DeleteId},
        success: function(response) {		
            if (response.status == '1') {
                location.reload();
             }
            displayLanguageTrnslation();
            location.reload();

        }
    });
}
function saveManageBrand(id){
    var manageBrandName=$("#brandName").val();
    var manageBrandNameDescription=$("#brandDescription").val();
	 var loggedUserId=$(".loggedUserId").val();
	if(manageBrandName==''){
		alert("Please fill Brand name");
	}
	else if(manageBrandNameDescription==''){
		alert("please fill brand description");
		
	}
	else{
   
    $.ajax({
         url: '../ajax/index.php?p=settings',
         type: "POST",
         data:{
             'action':'saveBrandType',
             manageBrandName:manageBrandName,
             manageBrandNameDescription:manageBrandNameDescription,
             loggedUserId:loggedUserId,'id':id
         },
         success: function(data) {
			 var response = JSON.parse(data);
			 
			 var code  = response.status_code;

			if(code && code==1){        
			flashMsgDisplay('Insert successfully', 'success-msg');
			}
			else if(code && code==2)
				{
				
			 flashMsgDisplay('Already Exist', 'success-msg');
			}
			$(".popup-holder").hide();
         $(".popup-mask").hide();
            setTimeout(function() {
           // location.reload();
        }, 6000);
         
         //location.reload();
        }
     });
}
}
/*
function deleteBrand(popTitle, popPageName, DeleteId){
      $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data:{'action':'deleteBrand','DeleteId':DeleteId},
        success: function(response) {
             if (response.status == '1') {
                location.reload();
             }
             location.reload();
        }
    });
}*/
/* To manage Add/Edit functionality begins */
function manageMenuPopupValidation() {
    eFlag = 0;
    var $popajaxmsg =$(".popup-holder .ajaxMsg");
    $popajaxmsg.html('');
    var isParent = $('input[name=isParent]:checked').val();
    var position = $('input[name=position]:checked').val();
    var menuOrder = $('#menuOrder').val();
    if ($.trim($('#menuDisplayName').val()) == '') {
        error = 'Please enter display name';
        eFlag = 1;
    }	
    else if ($.trim($('#menuName').val()) == '') {
        error = 'Please enter menu name';
        eFlag = 1;
    }
    else if (isParent != 1 && isParent != 0) {
        error = 'Please choose is parent';
        eFlag = 1;
    }
    else if (isParent == 0) {
        if ($.trim($('#parentId').val()) < 1) {
            error = 'Please choose menu parent';
            eFlag = 1;
        }
    }
    else if (position != 'top' && position != 'left') {
        error = 'Please choose position';
        eFlag = 1;
    }
    else if ($.trim(menuOrder) == '') {
        error = 'Please enter menu order';
        eFlag = 1;
    }
    else if (!(/^\+?(0|[1-9]\d*)$/.test(menuOrder))) {
        error = 'Menu order accepts only integer and value above -1';
        eFlag = 1;
    }
    else if ($.trim($('#fileName').val()) == '') {
        error = 'Please enter file name';
        eFlag = 1;
    }
    if (eFlag == 1) {
        $popajaxmsg.html(error);
        return false
    }
    else {
        return true;
    }
}
function manageMenuPopup(id) {
    var $parent =   $('#parentId');
    var menuDisplayName 	= encodeURIComponent($('#menuDisplayName').val());
    var menuName 			= encodeURIComponent($('#menuName').val());
    var isParent 			= $('input[name=isParent]:checked').val();
    var parentId 			= $parent.val();
    var menu_type 			= $( "#menu_type option:selected" ).val();
    var menuIcon 			= $('#menuIconDetails').val();
    var position 			= $('input[name=position]:checked').val();
    var menuOrder 			= $('#menuOrder').val();
    var fileName 			= encodeURIComponent($('#fileName').val());
    var menuPageId 			= $('#menuPageId').val();
    var isVisible 			= $('input[name=isVisible]:checked').val();
      if (!manageMenuPopupValidation()) {
         return false;
     }
    var parentvalue = '';
     if(isParent == 1){
         parentvalue = 0;
     }else{
         parentvalue = $parent.val();
     }
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        dataType:'json',
        data:{
            'action':'AddEditMenu',menuDisplayName:menuDisplayName,menuName:menuName,
            parentvalue:parentvalue,parentId:parentId,menu_type:menu_type,menuIcon:menuIcon,
            menuOrder:menuOrder,position:position,fileName:fileName,
            menuPageId:menuPageId,isVisible:isVisible,'id':id
        },
        success: function() {//response
            ajLoaderOff();
            flashMsgDisplay(LANG['updatemenuSuccess'], 'success-msg');
            setTimeout(function() {
            location.reload();
        }, 1500);
    }
    });
            closeMsg();
        }



function saveManageGroup(id){
    var manageGroupName=$("#groupName").val();
    var loggedUserId=$(".loggedUserId").val();
    $.ajax({
         url: '../ajax/index.php?p=settings',
         type: "POST",
         data:{'action':'saveGroup',manageGroupName:manageGroupName,loggedUserId:loggedUserId,'id':id},
         success: function() {//response
         //$(".popup-holder").hide();
         //$(".popup-mask").hide();
          ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['updategroupSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
         }
     });
    }
/*
function saveManageBrand(id){
    var manageBrandName=$("#brandName").val();
    var manageBrandNameDescription=$("#brandDescription").val();
    var loggedUserId=$(".loggedUserId").val();
    $.ajax({
         url: '../ajax/index.php?p=settings',
         type: "POST",
         data:{
         'action':'saveBrandType',manageBrandName:manageBrandName,
         manageBrandNameDescription:manageBrandNameDescription,loggedUserId:loggedUserId,'id':id},
         success: function(response) {
         //$(".popup-holder").hide();
         //$(".popup-mask").hide();
          ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['updatebrandSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                    }
     });
    }
    */
function deleteMenu(popTitle, popPageName, menuPageId){
    confirms(LANG['confirmDeleteMenu'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            //var dataString = "menuPageId=" + menuPageId + "&action=deleteMenuPage";
             $.ajax({
                    url: '../ajax/index.php?p=settings',
                    type: "POST",
                    data: 'action=deleteMenu&menuPageId='+menuPageId,
                    success: function() {//response
                    ajLoaderOff();
                    setTimeout(function() {
                    location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}
function deleteBrand(popTitle, popPageName, brandId){
    confirms(LANG['confirmDeleteBrand'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "brandId=" + brandId + "&action=deleteBrand";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletebrandSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}
function deleteMachine(popTitle, popPageName, machineId){
    confirms(LANG['confirmDeleteBrand'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "machineId=" + machineId + "&action=deleteMachine";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletebrandSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}
function deleteGroup(popTitle, popPageName, groupId){
    confirms(LANG['confirmDeleteGroup'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "groupId=" + groupId + "&action=deleteGroup";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletegroupSuccess'], 'success-msg');
                    setTimeout(function() {
                      location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}

function deletetranslation(popTitle, popPageName, translationId){
    confirms(LANG['confirmDeletetranslation'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "translationId=" + translationId + "&action=deletetranslation";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletetranslationSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}


function deletelanguage(popTitle, popPageName, languageId){
    confirms(LANG['confirmDeletelanguage'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "languageId=" + languageId + "&action=deletelanguage";
             $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=settings",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletelanguageSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}


function deleteClub(clubId) {
    confirms(LANG['confirmDeleteClub'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var dataString = "clubId=" + clubId + "&action=deleteClubById";
            $.ajax({
                type: "POST",
                url: "../ajax/index.php?p=adminLight",
                data: dataString,
                cache: false,
                success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deleteClubSuccess'], 'success-msg');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            });
            closeMsg();
        } else {
            closeMsg();
        }
    });
}


/* To manage Add/Edit functionality begins */
function manageQuestionnairePopupValidation() {
    eFlag = 0;
    var $popajaxmsg =   $(".popup-holder .ajaxMsg");
    $popajaxmsg.html('');
    if ($.trim($('#questionnaireTitle').val()) == '') {
        error = 'Please enter Questionnaire title';
        eFlag = 1;
    }
    else if ($.trim($('#questionnaireText').val()) == '') {
        error = 'Please enter Questionnaire text';
        eFlag = 1;
    }	
         if (eFlag == 1) {
             $popajaxmsg.html(error);
        return false
    }
    else {
        return true;
    }
}
function saveQuesttion() {
    var $txtquestion    =$('#questtxt');
    var $selquestype    =$('#questype');
    var $selquestopic    =$('#questopic');
    var questionnaireText=$txtquestion.val();
    var group_name=$('#groupid').val();
    var phase_name=$('#phaseid').val();
    var questopic=$selquestopic.val();
    //var questopictxt=$('#questopic:selected').text();
    var questopictxt=$selquestopic.val();
    var order=$('#ordercls').val();
    if(order=='N/A'){
        order	=	$('#orderstatic').val()
    }
    //var activityques=$('#activity_name').val();
	
    var questype=$selquestype.val();
    var questypeoldvalue=$selquestype.attr('oldvalue');
    var quesId=$('#quesId').val();
    var isquestypechange = 0;
    var quesscore	=	$("#quesscore").val();
    if((quesId) && (questype!=questypeoldvalue)){
        isquestypechange=1;
    }
    // var quesinfo = valuett.getData();
    //CKEDITOR.instances['quesinfo'+row.language_id].getData();
    var quesinfo=$('.quesinfo').val();
    var quesicon=$('#quesicon').val();
    //var quesval=$('#question_info').val();//What value do we need to insert
    var questionnaireId=$('#questionnaireId').val();
    var formId=$('#formId').val();
    var startval=$('#startval').val();
    var questxt=[];
    var quesopt=[];
    //var questtxtval = $txtquestion.val();
    $('.questxt').each(function(i,relem){
        var $relem  =   $(relem);
        var questxtval=$relem.val();
        var queslanguageid=$relem.attr('languageid');
		
        var queslangeditid  = $('.queslangid_'+queslanguageid).val();
        questxt.push({
            queslangeditid:queslangeditid,
            queslanguageid:queslanguageid,
            value:questxtval,quesinfo:CKEDITOR.instances['quesinfo'+queslanguageid].getData()
        });
    });
    if(questype==1){
        var optnumval = '';
        $('.quesoptnum').each(function(i,row){
            var quesoptnumval=$(row).val();
            optnumval=optnumval+quesoptnumval+'_';
        });
        var optpropid = $('#optpropid').val();
        var optnumvalarr =[];
        $('.numoptrangepts').each(function(index,value){
            //$('.numoptrangepts_'+index).each(function(i,r){
                var numoptmin = $('.numoptmin_'+index).val();
                var numoptmax = $('.numoptmax_'+index).val();
                var numoptrange = $('.numoptrange_'+index).val();
                var numoptid = $('.numoptid_'+index).val();
                optnumvalarr.push({
                    numoptid:numoptid,min:numoptmin,max:numoptmax,range:numoptrange
                });
            //});
        });
        quesopt.push({opteditid:optpropid,value:optnumval,optnumvalarr:optnumvalarr});
    }
    if(questype==2){
        $('.radioopt').each(function(index,value){
            $('.radioopt_'+index).each(function(){//i,r
                var quesoptlang=[];
                var radiopoint = $('#radiopoint_'+index).val();
                var radioopteditid = $('#radiooptid_'+index).val();
                $('.quesoptradio_'+index).each(function(key,row){
                    var quesoptradioval=$(row).val();
                    var optradiolanguageid=$(row).attr('languageid');
                    var opteditlangid=$('#opteditlangid_'+optradiolanguageid+'_'+index).val();
                    quesoptlang.push({
                        optradiolanguageid:optradiolanguageid,value:quesoptradioval,opteditlangid:opteditlangid
                    });
                });
                quesopt.push({opteditid:radioopteditid,quesoptlang:quesoptlang,radiopoint:radiopoint});
            });
        });
    }
    if(questype==3){
        $('.yesnoopt').each(function(i,relem){
            var $relem  =   $(relem);
            var yesoptval = $relem.val();
            var yesnooptid = $relem.attr('yesnooptid');
            quesopt.push({order:i,value:yesoptval,opteditid:yesnooptid});
        });
    }
    if(questype==4){
        $('.smilyopt').each(function(i,relem){
            var $relem  =   $(relem);
            var smilyopt = $relem.val();
            var smilyoptid = $relem.attr("smilyoptid");
            quesopt.push({order:i,value:smilyopt,opteditid:smilyoptid});
        });
    }
    if(questype==5){
        $('.checkopt').each(function(index,value){
            $('.checkopt_'+index).each(function(){//i,r
                var quesoptlang=[];
                var checkboxpoint = $('#checkpoint_'+index).val();
                var checkopteditid = $('#checkoptid_'+index).val();
                $('.quesoptcheck_'+index).each(function(key,row){
                    var quesoptcheckval=$(row).val();
                    var optchecklanguageid=$(row).attr('languageid');
                    var opteditlangid=$('#opteditlangid_'+optchecklanguageid+'_'+index).val();
                    quesoptlang.push({
                        optchecklanguageid:optchecklanguageid,value:quesoptcheckval,opteditlangid:opteditlangid
                    });
                });
                quesopt.push({opteditid:checkopteditid,quesoptlang:quesoptlang,checkpoint:checkboxpoint});
            });
        });
    }
    if(questype==6){
        $('.likdislikopt').each(function(i,relem){
            var $relem    =   $(relem);
            var likdisoptval = $relem.val();
            var likdislikoptid = $relem.attr('likdislikoptid');
            quesopt.push({order:i,value:likdisoptval,opteditid:likdislikoptid});
        });
    }
    var quesitems = JSON.stringify(questxt);
    var quesoptnumitems = JSON.stringify(quesopt);
    if(validateSteps('#questopicbox')){
        ajLoaderOn();
        var url = 'action=AddEditQuestion&questionnaireText='+encodeURIComponent(questionnaireText)+
            '&group_name='+group_name+'&phase_name='+phase_name+'&questopic='+questopic+
            '&questxt='+encodeURIComponent(quesitems)+'&activityques='+activityques+'&questype='+questype+
            '&quesinfo='+quesinfo+'&quesicon='+quesicon+
            '&quesoptnumitems='+quesoptnumitems+'&questionnaireId='+questionnaireId+
            '&quesId='+quesId+'&formId='+formId+'&isquestypechange='+isquestypechange+
            '&orderid='+order+'&score='+quesscore+'&questopictxt='+questopictxt;
        $.ajax({
            url: '../ajax/index.php?p=settings',
            type: "POST",
            data: url,
            success: function(response) {
                response = JSON.parse(response);
                ajLoaderOff();
                if (response.status == 200) {
                    flashMsgDisplay(LANG['updateQuestionSuccess'], 'success-msg');
                        setTimeout(function() {
                            $(".icon-popupcls").trigger("click");
                            //location.reload();
                        }, 1500);
                    getQuestion(group_name,phase_name);
                }
            }
        });
    }
}
function editQuestionary(title,page,id){
    /**
     * @param response.form_name This is form name
     * @param response.form_text This is form text
     * @param response.activitylink This is activity link
     * @param response.form_id This is form id
     */
    var $txtquestion    =$('#questtxt');
    var $selquestype    =$('#questype');
    showQuickAddPop(title,page,id);
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        dataType: 'json',
        data:{'action':'editQuestionary','questionnaireId':id},
        success: function(response) {
            var formname = response.form_name;
        //	var formnamevalue = (formname!='') ?formname.toString():'';
            var formnamevalue=formname.split('-');
            var groupid = formnamevalue[0].split('_')[1];
            var phaseid = formnamevalue[1].split('_')[1];
            $('.group_name').val(groupid);
            $('.phase_name').val(phaseid);
            if(response.form_text!=''){
                $('.questiontext').css("display","block");
            }
            if(response.activitylink!=0){
                $('.activity_name').val(response.activitylink);
                $('.activityques').css("display","block");
            }
            $txtquestion.val(response.form_text);
            $selquestype.val(response.type);
            $('#questionnaireId').val(response.form_id);
            $('#quesId').val(response.id);
         }
     });
}
function manageTextRange(){
    var group_name=$('#groupid').val();
    var phase_name=$('#phaseid').val();
    var pointweight=$('#pointweight').val();
    var maxweight=$('#maxweight').val();
    var rangeid=$('#rangeid').val();
    var sucesstxt=[];
    var sucesstxtarr=[];
    var quickwintxt=[];
    var quickwintxtarr=[];
    var goaltxt=[];
    var goaltxtarr=[];
    var reducetxt=[];
    var reducetxtarr=[];
    var phaserangearr=[];
    $('.sucesstxt').each(function(i,row){
        //var sucesstxtval=$(row).val();
        var sucesslanguageid=$(row).attr('languageid');
        sucesstxt.push({
            languageid:sucesslanguageid,value:CKEDITOR.instances['text_type1_'+sucesslanguageid].getData()
        });
    });
    var minrangepts = $('#minrangepts').val();
    var maxrangepts = $('#maxrangepts').val();
    sucesstxtarr.push({Text:sucesstxt});
    $('.quickwintxt').each(function(i,row){
        //var quickwintxtval=$(row).val();
        var quickwinlanguageid=$(row).attr('languageid');
        quickwintxt.push({
            languageid:quickwinlanguageid,value:CKEDITOR.instances['text_type2_'+quickwinlanguageid].getData()
        });
    });
    quickwintxtarr.push({Text:quickwintxt});
    $('.goaltxt').each(function(i,row){
        //var goaltxtval=$(row).val();
        var goallanguageid=$(row).attr('languageid');
        goaltxt.push({languageid:goallanguageid,value:CKEDITOR.instances['text_type3_'+goallanguageid].getData()});
    });
    var mingoalpts = $('#mingoalpts').val();
    var maxgoalspts = $('#maxgoalspts').val();
    var topicid		=$("#selectopic").val();
    goaltxtarr.push({Text:goaltxt});
    $('.reducetxt').each(function(i,row){
        //var reducetxtval=$(row).val();
        var reducelanguageid=$(row).attr('languageid');
        reducetxt.push({
            languageid:reducelanguageid,value:CKEDITOR.instances['text_type4_'+reducelanguageid].getData()
        });
    });
    var minreducepts = $('#minreducepts').val();
    var maxreducepts = $('#maxreducepts').val();
    reducetxtarr.push({Text:reducetxt});
    phaserangearr.push({
        min:minrangepts,max:maxrangepts,Text:{1:sucesstxtarr,2:quickwintxtarr,3:goaltxtarr,4:reducetxtarr}
    });
    var phaserange = JSON.stringify(phaserangearr);
    ajLoaderOn();
    var url = 'action=savePhaseRange&phase_name='+phase_name+'&group_name='+group_name+
        '&pointweight='+pointweight+'&maxweight='+maxweight+
        '&phaserange='+encodeURIComponent(phaserange)+'&rangeid='+rangeid+'&topicid='+topicid;
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            response = JSON.parse(response);
            ajLoaderOff();
            if (response.status == 200) {
                hideQuickAddPopup();
                //  location.reload();
                 //flashMsgDisplay(LANG['updateQuestionSuccess'], 'success-msg');
                getQuesPhaseRange(group_name,phase_name);
           }
        }
    });
}
function managePhaseWeightage(phaseid,groupid){
    /*Dont think its used
    var group_name=$('#group_name').val();
    var phase_name=$('#phase_name').val();
    */
    var pointweight=$('#idphasequestionbox'+phaseid+' #idgroupquestionbox'+groupid+' #pointweight').val();
    var maxweight=$('#idphasequestionbox'+phaseid+' #idgroupquestionbox'+groupid+' #maxweight').val();
    var phaseweightageid=$('#phaseweightageid_'+phaseid+'_'+groupid).val();
    ajLoaderOn();
    var url = 'action=savePhaseWeight&phase_id='+phaseid+'&group_id='+groupid+
        '&pointweight='+pointweight+'&maxweight='+maxweight+'&phaseweightageid='+phaseweightageid;
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            response = JSON.parse(response);
            ajLoaderOff();
            if (response.status == 200) {
                hideQuickAddPopup();
                //  location.reload();

           }
        }
    });
}
function editPhageRange(title,page,id,phaseid,groupid){
    showQuickAddPop(title,page,id,'',function(){
        // $(".clshtmlcontentbox").each(function(idx, obj){
            // CKEDITOR.replace( $(obj).attr("id"));
        // });
        /**
         * @param response.minrange This is min range
         * @param response.maxrange This is max range
         * @param response.text_type This is text type
         * @param response.topic This is topic
         */
        setTimeout(function(){
            $('#phaseid').val(phaseid);
            $('#groupid').val(groupid);
            var url = 'action=getPhaseRangeById&id='+id;
            $.ajax({
                url: '../ajax/index.php?p=settings',
                type: "POST",
                data: url,
                success: function(response) {
                    response = JSON.parse(response);
                    ajLoaderOff();
                   //console.log(JSON.stringify(response));
                   $('#minrangepts').val(response.minrange);
                   $('#maxrangepts').val(response.maxrange);
                   $('#rangeid').val(id);
                   //var i=1;
                   // console.log(response);
                   // setTimeout(function(){
                       // CKEDITOR.instances['text_type1_3'].setData('Hi');
                   // }, 0);
                   if(response){
                       if (response.data){
                           $.each(response.data,function(key,row){
                                $.each(row,function(index,value){ //Each language
                                    //XX console.log(
                                    // 'text_type'+value.text_type+'_'+value.languageid+' - '+value.text
                                    // );
                                    // CKEDITOR.instances['text_type'+value.text_type+'_'+value.languageid].setData(
                                    // value.text
                                    // );
                                    //CKEDITOR.instances['text_type1_3'].setData(value.text);
                                       CKEDITOR.instances['text_type'+value.text_type+'_'+value.languageid].setData(
                                           value.text
                                       );
                                });
                            });
                            /*MK Added - Load Topics*/
                            var selectedtopic	=	(response.topic) ? response.topic:null;
                            loadTopic(groupid,function(){

                            },"#selectopic",selectedtopic);
                       }
                   }
                    /* $('.text_type'+i).each(function(){
                                    var languageid =  $('.text_type_'+i).attr('languageid');
                                      // console.log(languageid);
                                       //console.log(i);
                                       // if(i===1){
                                          // var text_type = '.sucesstxt';
                                       // }
                                     // if(languageid==value.languageid){
                                             //$('.text_type_'+i).val(value.text)
                                             /*
                                             console.log('text_type'+i+'_'+value.languageid);
                                             console.log(value.text);
                                             CKEDITOR.instances['text_type'+i+'_'+value.languageid].setData(value.text);
                                      // }
                                       /*$('.sucesstxt').each(function(i,txt){
                                            var sucesslanguageid=$(txt).attr('languageid');
                                            console.log('sucesstxt'+sucesslanguageid);
                                            console.log(value.text);
                                            CKEDITOR.instances['sucesstxt'+sucesslanguageid].setData(value.text);
                                        });* /
                                   });
                                    i=i+1;
                                    */
                  /* $.each(response,function(key,row){
                       //console.log(1);
                       /*
                       $.each(row,function(index,value){

                          $('.text_type'+i).each(function(){
                            var languageid =  $('.text_type_'+i).attr('languageid');
                              // console.log(languageid);
                               //console.log(i);
                               // if(i===1){
                                  // var text_type = '.sucesstxt';
                               // }
                             // if(languageid==value.languageid){
                                     //$('.text_type_'+i).val(value.text)
                                     console.log('text_type'+i+'_'+value.languageid);
                                     console.log(value.text);
                                     CKEDITOR.instances['text_type'+i+'_'+value.languageid].setData(value.text);
                              // }
                               /*$('.sucesstxt').each(function(i,txt){
                                    var sucesslanguageid=$(txt).attr('languageid');
                                    console.log('sucesstxt'+sucesslanguageid);
                                    console.log(value.text);
                                    CKEDITOR.instances['sucesstxt'+sucesslanguageid].setData(value.text);
                                });* /
                           });
                       });
                       * /
                        i=i+1;
                   });*/

                }
            });
        },0)
    });
}
function deletePhaseRange(id,phaseid,groupid){
    var url = 'action=deletePhaseRangeById&id='+id;
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function() {//response
            ajLoaderOff();
            getQuesPhaseRange(groupid,phaseid);
        }
    });
}
function managePhaseActivity(phaseid,groupid){
    var activity  = $('#activityques').val();
    var activquescredit  = $('#activquescredit').val();
    var activityperweek  = $('#activityperweek').val();
    var quesactid  = $('#quesactid').val();
    var ques_actpointid  = $('#ques_actpointid').val();
    var quesacttopicid  = $('#quesacttopicid').val();
    var minutes= $('#minutesid').val();
    ajLoaderOn();
    var url = 'action=saveQuesActivity&activity='+activity+'&activquescredit='+activquescredit+
        '&activityperweek='+activityperweek+'&groupid='+groupid+'&phaseid='+phaseid+
        '&quesactid='+quesactid+'&ques_actpointid='+ques_actpointid+
        '&quesacttopicid='+quesacttopicid+'&minutes='+minutes;
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            response = JSON.parse(response);
            ajLoaderOff();
            if (response.status == 200) {
                //hideQuickAddPopup();
                 flashMsgDisplay(LANG['updateQuestionSuccess'], 'success-msg');
                    setTimeout(function() {
                        $(".icon-popupcls").trigger("click");
                        //location.reload();
                    }, 1500);
                //  location.reload();

           }
        }
    });
}
function loadAddQuestionPopup(title,page,phaseid,groupid){
    showQuickAddPop(title,page,'','',function(){
        // $(".clshtmlcontentbox").each(function(idx, obj){
            // CKEDITOR.replace( $(obj).attr("id"));
        // });
        $('#groupid').val(groupid);
        $('#phaseid').val(phaseid);
        loadTopic(groupid);
    });
}
function loadTopic(groupid,onLoadTopicSuccess,selector,selectedvalue){
    ajLoaderOn();
    var url = 'action=getGroupTopics&groupid='+groupid;
    var topicselectbox	=	selector ? selector:'#questopic';
    $(topicselectbox).html('<option value="0">Loading, Please wait..</option>');
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            response = JSON.parse(response);
            ajLoaderOff();
            var htmlcnt = '<option value="0">Select Topic</option>';
            /**
             * @param row.topic_id This is topic id
             * @param row.topic_name This is topic name
             */
            $.each(response,function(i,row){
                htmlcnt+='<option value="'+row.topic_id+'">'+row.topic_name+'</option>';
            });
            $(topicselectbox).html(htmlcnt);
            if(selectedvalue || selectedvalue==0){
                $(topicselectbox).val(selectedvalue);
            }
            if(onLoadTopicSuccess){
                onLoadTopicSuccess();
            }
        }
    });
}/******Backup *******/
function editQuestion(title,page,id,groupid,phaseid,isedit){
    showQuickAddPop(title,page,id,'',function(){
        getLanguage(function(){
            $('#groupid').val(groupid);
            $('#phaseid').val(phaseid);
            /**
             * @param response.quesid This is question id
             * @param response.score This is score
             * @param response.ui_order This is ui_order
             * @param response.queslang This is question language
             * @param response.quesopt This is question option
             * @param response.quesoptadio This is question option radio
             * @param response.is_static This is is_static
             * @type {string}
             */
            var url = 'action=getQuestionById&id='+id;
            $.ajax({
                url: '../ajax/index.php?p=settings',
                type: "POST",
                data: url,
                success: function(response) {
                    var $ordercls = $('#ordercls');
                    response = JSON.parse(response);
                    $('#formId').val(id);
                    $('#quesId').val(response.quesid);
                    //$('#questopic').val(response.topicid);
                    $("#quesscore").val(response.score);
                    $('#orderstatic').val(response.ui_order);
                    if(response.is_static==1){
                        $ordercls.attr("disabled","disabled");
                        $ordercls.val('N/A');
                    } else {
                        $ordercls.val(response.ui_order);
                        $ordercls.removeAttr("disable");
                    }
                    /**
                     * @param row.language_id This is language id
                     * @param row.question This is question
                     * @param row.question_info This is question info
                     * @param row.queslangid This is question language id
                     */
                  /*  $.each(response.queslang,function(i,row){
                        $('.questxt_'+row.language_id).val(row.question);
                        CKEDITOR.instances['quesinfo'+row.language_id].setData(row.question_info);
                        $('.queslangid_'+row.language_id).val(row.queslangid);
                    });										*/							 $.each(response.queslang,function(i,row){                        $('.questxt_'+row.language_id).val(row.question);                        if (CKEDITOR.instances['quesinfo'+row.language_id]){							CKEDITOR.instances['quesinfo'+row.language_id].setData(row.question_info);						}                        $('.queslangid_'+row.language_id).val(row.queslangid);                    });
                    var NUM_OPT_HTML ='';    
                    var RADIO_OPT_HTML ='';
                    var index =0;
                    //$('.numoptrangepts').removeClass('numoptrangepts_0');
                    //$('.numoptrangepts').html('');
                    var $questype= $('#questype');
                    $questype.val(response.type);
                    $questype.attr('oldvalue',response.type);
                    $('#quesicon').val(response.icon);
                    $('#imagePreview').attr('src',SERVICE_QUESTION_ICON+response.icon);
                    if(parseInt(response.type)==1){
                        /**
                         * @param row.formelement_option This is form element option
                         * @param row.formelempropid This is form element property id
                         */
                    $.each(response.quesopt,function(i,row){
					 
                        //if(parseInt(row.type)==1){
                            $('.ansoptions1').css('display',"block");
                            var formelement_option = row.formelement_option;
                            var formeleme_propid = row.formelempropid;
                            var start_end = formelement_option.split('_');
                            $('#startval').val(start_end[0]);
                            $('#endval').val(start_end[1]);
                            $('#stepval').val(start_end[2]);
                            $('#optpropid').val(formeleme_propid);
                            NUM_OPT_HTML+=getQuestionOptHtml(i,row);
                        //}
                        index = index+1;
                    });
                    }
                    if(response.type==2){
                        $('.ansoptions2').css('display',"block");
                        var key=0;
                        $.each(response.quesoptadio,function(i,row){
                        /*	$.each(row,function(index,value){
                                console.log(JSON.stringify(value));
                                console.log(JSON.stringify(value.type));

                                console.log(JSON.stringify(value));*/
                                RADIO_OPT_HTML+=getQuestionRadioOptHtml(key,row);
                                    key =key+1;

                        /*	});	*/
                        });
                    }
                    if(response.type==3){
                        $('.ansoptions3').css('display',"block");
                        var indx = 0;
                        $.each(response.quesoptadio,function(i,row){
                            //$('#yesnoopt_'+i).val(row.range_points);
                            var $yesnooptidx= $('#yesnoopt_'+indx);
                            $.each(row,function(key,value){
                                $yesnooptidx.val(value.range_points);
                                $yesnooptidx.val(value.range_points);
                                $yesnooptidx.attr('yesnooptid',value.formelemoptid);
                            });
                            indx = indx+1;
                        });
                    }
                    if(response.type==4){
                        $('.ansoptions4').css('display',"block");
                        var indexkey = 0;
                        /**
                         * @param value.range_points This is range points
                         * @param value.formelemoptid This is form element  option id
                         */
                        $.each(response.quesoptadio,function(i,row){
                            var $smilyoptidx = $('#smilyopt_'+indexkey);
                            $.each(row,function(key,value){
                                $smilyoptidx.val(value.range_points);
                                $smilyoptidx.attr('smilyoptid',value.formelemoptid);
                            });
                            indexkey = indexkey+1;
                        });
                    }
                    if(response.type==5){
                        $('.ansoptions5').css('display',"block");
                        var keyval=0;
                        $.each(response.quesoptadio,function(i,row){
                        /*	$.each(row,function(index,value){
                                console.log(JSON.stringify(value));
                                console.log(JSON.stringify(value.type));

                                console.log(JSON.stringify(value));*/
                                RADIO_OPT_HTML+=getQuestionCheckOptHtml(keyval,row);
                            keyval =keyval+1;

                        /*	});	*/
                        });
                    }
                    if(response.type==6){
                        $('.ansoptions6').css('display',"block");
                        var indexval = 0;
                        $.each(response.quesoptadio,function(i,row){
                            //$('#yesnoopt_'+i).val(row.range_points);
                            var $likdislikooptidx= $('#likdislikoopt_'+indexval);
                            $.each(row,function(key,value){
                                $likdislikooptidx.val(value.range_points);
                                $likdislikooptidx.val(value.range_points);
                                $likdislikooptidx.attr('likdislikoptid',value.formelemoptid);
                            });
                            indexval = indexval+1;
                        });
                    }
                    /*$.each(response.quesoptadio,function(i,row){
                        console.log(JSON.stringify(row));
                        $.each(row,function(index,value){
                            console.log(JSON.stringify(value));
                            console.log(JSON.stringify(value.type));
                            if(value.type==2){
                                $('.ansoptions2').css('display',"block");
                                console.log(JSON.stringify(value));
                                RADIO_OPT_HTML+=getQuestionRadioOptHtml(index,value);
                            }
                        });
                    });*/
                //	$('.edtnumoptpoint').html(NUM_OPT_HTML);
                    // console.log(RADIO_OPT_HTML);
                    // $('.ansoptions2').html(RADIO_OPT_HTML);
                    if(isedit==1){
                        $('.iseditdisable').attr("disabled","disbled");
                    }
                    if(response.is_static==1){
                        $('#questopic').removeClass('req-nonzero');
                    }
                    loadTopic(groupid,function(){
                        $('#questopic').val(response.topicid);
                    });
                    ajLoaderOff();
                }
            });
        });
    });
}
/**
 *
 * @param index
 * @param value
 * @param value.min_value This is min value
 * @param value.max_value This is max value
 * @param value.range_points This is range point
 * @param value.formelemoptid This is form element optionid
 */
function getQuestionOptHtml(index,value){
    var $options	=	$(".numoptrangepts");
    var $lastitem	=	$(".numoptrangepts_"+($options.length-1));
    var btn = '<span class="addquescnt" onclick="removeTxt($(this))">-</span>';
    if(index==0){
        btn = '<span class="addquescnt" onclick="addRangeNum($(this))">+</span>';
    }
    var HTML = '<div class="row-sec mb15 numoptrangepts numoptrangepts_'+index+'" id="numoptrangepts">'
            +'<label class="fl">&nbsp;</label>'
            +'<input type="text" ' +
                'class="three_fields numoptmin_'+index+' iseditdisable" ' +
                'value="'+value.min_value+'" placeholder="Min">'
            +'<input type="text" class="three_fields numoptmax_'+index+' iseditdisable" ' +
                'value="'+value.max_value+'" placeholder="Max">'
            +'<input type="text" class="three_fields numoptrange_'+index+' iseditdisable" ' +
                'value="'+value.range_points+'" placeholder="Range Points">'
            +'<input type="hidden" class="three_fields numoptid_'+index+' iseditdisable" ' +
                'value="'+value.formelemoptid+'" placeholder="Range Points">'
            +btn+'</div>';
        // $('.numoptmin_'+index).val(value.min_value);
        // $('.numoptmax_'+index).val(value.max_value);
        // $('.numoptrange_'+index).val(value.range_points);
        if(index==0){
                $('.edtnumoptpoint').html(HTML);
            }
            else{
                $lastitem.after(HTML);
            }
        return HTML;
}
function getQuestionRadioOptHtml(index,value,sucessHandler){
    var HTML='';
    var $options	=	$(".radioopt");
    //var optindex	=	($options.length-1);
    var $lastitem	=	$(".radioopt_"+($options.length-1));1
    var btn = '<span class="addquescnt" onclick="removeTxt($(this))">-</span>';
    var labelradiotxtcnt = '<label class="fl">&nbsp;</label>';
    if(index==0){
        btn = '<span class="addquescnt" onclick="addRadioTxt($(this))">+</span>';
        labelradiotxtcnt = '<label class="fl">Answer Options</label>';
    }
    //var addradiotxtcnt = (index!=0) ?'<label class="fl">&nbsp;<span class="required">*</span></label>':'';
    var addradiotxtcnt = ""; // (index!=0) ?'<label class="fl">&nbsp;</label>':'Answer Options';

    /*ajLoaderOn();
    $.ajax({
        url:'../ajax/index.php?p=settings',
        type: "POST",
        data: 'action=getLanguageActiveType',
        success: function(response) {
            ajLoaderOff();
            response = JSON.parse(response);*/
            var key = 0;
            addradiotxtcnt+='<div class="row-sec mb15 radioopt radioopt_'+index+'" id="radioopt">'+labelradiotxtcnt;
            $.each(LANGUAGE,function(i,row){
                 addradiotxtcnt+='<input type="text" ' +
                     'class="three_fields radiobtntxt quesoptradio_'+(index)+' ' +
                        'optradio_'+key+' optradiolang_'+row.language_id+'_'+index+' iseditdisable" ' +
                     'value="" placeholder="Text In '+row.title+'"  ' +
                     'languageid="'+row.language_id+'">'
                 +'<input type="hidden" id="opteditlangid_'+row.language_id+'_'+index+'">';
                key = key+1;
            });
             addradiotxtcnt+='<input type="text" id="radiopoint_'+(index)+'" ' +
                 'class="three_fields iseditdisable" value="" placeholder="Points">'
                        +btn+'<input type="hidden" id="radiooptid_'+index+'" value=""></div>';
            //$lastitem.after(addradiotxtcnt);
            HTML+=addradiotxtcnt;
            if(index==0){
                $('.edtradioopt').html(HTML);
            }
            else{
                $lastitem.after(HTML);
            }
    /**
     * @param r.optionid This is option id
     * @param r.optlangid This is option language id
     */
            $('.radioopt_'+index).each(function(){//key,row
                $.each(value,function(i,r){
                    $('.optradiolang_'+r.language_id+'_'+index).val(r.options);
                    $('#opteditlangid_'+r.language_id+'_'+index).val(r.optlangid);
                    $('#radiopoint_'+index).val(r.range_points);
                    $('#radiooptid_'+index).val(r.optionid);
                });
            });
    if(sucessHandler){
        sucessHandler();
    }
       /* }
    });*/
}
function getQuestionCheckOptHtml(index,value,sucessHandler){
    var $options	=	$(".checkopt");
    //var optindex	=	($options.length-1);
    var $lastitem	=	$(".checkopt_"+($options.length-1));
    var btn ='<span class="addquescnt" onclick="removeTxt($(this))">-</span>';
    var labelradiotxtcnt ='<label class="fl">&nbsp;</label>';
    if(index==0){
        btn ='<span class="addquescnt " onclick="addCheckboxTxt($(this))">+</span>';
        labelradiotxtcnt ='<label class="fl">Answer Options</label>'
    }

    var addradiotxtcnt = ""; // (index!=0) ?'<label class="fl">&nbsp;</label>':'Answer Options';

    /*ajLoaderOn();
    $.ajax({
        url:'../ajax/index.php?p=settings',
        type: "POST",
        data: 'action=getLanguageActiveType',
        success: function(response) {
            ajLoaderOff();
            response = JSON.parse(response);*/
            var key = 0;
            addradiotxtcnt+='<div class="row-sec mb15 checkopt checkopt_'+index+'" id="checkopt">'+labelradiotxtcnt;
            $.each(LANGUAGE,function(i,row){
                 addradiotxtcnt+='<input type="text" ' +
                     'class="three_fields checkopttxt quesoptcheck_'+(index)+' optcheck_'+key+' ' +
                        'optchecklang_'+row.language_id+'_'+index+' iseditdisable" ' +
                     'value="" placeholder="Text In '+row.title+'"  ' +
                     'languageid="'+row.language_id+'">'
                 +'<input type="hidden" id="opteditlangid_'+row.language_id+'_'+index+'">';
                key = key+1;
            });
             addradiotxtcnt+='<input type="text" id="checkpoint_'+(index)+'" ' +
                                'class="three_fields chkpointvalue iseditdisable" ' +
                                'value="" placeholder="Points">'+btn+
                                '<input type="hidden" id="checkoptid_'+index+'" ' +
                                        'class="chkoptionid" value="">' +
                 '</div>';
            //$lastitem.after(addradiotxtcnt);
            if(index==0){
                $('.edtcheckopt').html(addradiotxtcnt);
            }
            else{
                $lastitem.after(addradiotxtcnt);
            }
            $('.checkopt_'+index).each(function(){
                $.each(value,function(i,r){
                    $('.optchecklang_'+r.language_id+'_'+index).val(r.options);
                    $('#opteditlangid_'+r.language_id+'_'+index).val(r.optlangid);
                    $('#checkpoint_'+index).val(r.range_points);
                    $('#checkoptid_'+index).val(r.optionid);
                });
            });
            if(sucessHandler){
                sucessHandler();
            }
       /* }
    });*/
}
function getLanguage(onSucess){
    ajLoaderOn();
    $.ajax({
        url:'../ajax/index.php?p=settings',
        type: "POST",
        data: 'action=getLanguageActiveType',
        success: function(response) {
            ajLoaderOff();
             response = JSON.parse(response);
            LANGUAGE = response.languageTypeDetails;
            if(onSucess){
                onSucess();
            }
        }
    });
}
function loadAddRangePopup(title,page,phaseid,groupid){
    showQuickAddPop(title,page,'','',function(){
        $('#groupid').val(groupid);
        $('#phaseid').val(phaseid);
        loadTopic(groupid,function(){

        },"#selectopic");
    });
}
function getQuestion(groupid,phaseid){
    ajLoaderOn();
    var url = 'action=getQuestion&phaseid='+phaseid+'&groupid='+groupid;
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            //response = JSON.parse(response);
            ajLoaderOff();
            //$('.gridcnt').html(response);
            //console.log('questionarylist_' + phaseid + '_' + groupid)
            $('.questionarylist_' + phaseid + '_' + groupid).html(response);
        }
    });
}
function getQuesPhaseRange(groupid,phaseid){
    ajLoaderOn();
    var url = 'action=getQuesPhaseRange&phaseid='+phaseid+'&groupid='+groupid;
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            //response = JSON.parse(response);
            ajLoaderOff();
            $('.rangegrid').html(response);
        }
    });
}
function deleteQuestion(quesid,groupid,phaseid){
    confirms(LANG['confirmDeleteQuestion'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var url = 'action=deleteQuestionById&id='+quesid;
                $.ajax({
                url: '../ajax/index.php?p=settings',
                type: "POST",
                data: url,
                success: function() {//response
                    //response = JSON.parse(response);
                    ajLoaderOff();
                    getQuestion(groupid,phaseid);
                    flashMsgDisplay(LANG['deleteQuestionSuccess'], 'success-msg');
                            setTimeout(function() {
                                location.reload();
                            }, 1500);
                }
            });
        } else {
            closeMsg();
        }
    });
}
function _(el){
    try {
        return document.getElementById(el);
    } catch (e) {
        alert("_", e);
    }
}
function savePageContent(id){
    var pagename=$('.pagename').val();
    var pagecontent = $('#pagecontent').val();
	var main_id = $('#pagecntentid').val();
	
    var pagecntid = $('#pagecntid').val();
	//var title = $('#conttitle').val();
	var bannerImg=$('#pagebannerimg').val();
	var pagecont =[];
	var titlecon  =[];
	//var titleval = $('#conttitle').val();

	 var title=JSON.stringify(titlecon);
    $('.pagecontent').each(function(i,row){
        var pagecontval=$(row).val();
        var contentlanguageid=$(row).attr('languageid');
        var titleval    =   $("#conttitle"+contentlanguageid).val();
        pagecont.push({
            contentlanguageid:contentlanguageid,
            title:titleval,
            value:CKEDITOR.instances['pagecontent'+contentlanguageid].getData()

        });
    });
    var contentitems = JSON.stringify(pagecont);
    var url = 'action=AddEditPageContent&pagename='+encodeURIComponent(pagename)+'&contentitems='+encodeURIComponent(contentitems)+'&pagecntid='+pagecntid+'&main_id='+main_id+'+&bannerImg='+bannerImg;
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(result) {
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['updatePagecontentSuccess'], 'success-msg');
                    setTimeout(function() {
                        getPageContent();
                        $(".icon-popupcls").trigger("click");
                    }, 1500);
                }
            });
            closeMsg();
        }    
            
function uploadBannerImgFile(){
    var  imgfile = $('#imgfile').prop('files')[0];
	var form_data = new FormData();
    if (form_data) {
        form_data.append("file", imgfile);   
    }
	var url = 'action=uploadfile&form_data='+form_data;
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
        
        }
    });
}
		
		
function getPageContent(){
    var url = 'action=getPageContent';
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            $('.pgcnt').html(response);
        }
    });
}
function addPageContent(title,page){
    showQuickAddPop(title,page, null, null, function(){
        // $(".clshtmlcontentbox").each(function(idx, obj){
            // CKEDITOR.replace( $(obj).attr("id"));
        // });
    });
}
function editPageContent(title,page,id,c_id){
	
	
    showQuickAddPop(title,page, null, null, function(){
		   
        // $(".clshtmlcontentbox").each(function(idx, obj){
            // CKEDITOR.replace( $(obj).attr("id"));
        // });
		
        setTimeout(function(){
            /**
             * @param response.pageid This is page id
             * @type {string}
             */
            var url = 'action=getPageContentById&id='+id+'&c_id='+c_id;
            $.ajax({
                url: '../ajax/index.php?p=settings',
                type: "POST", 
                data: url,
                success: function(response) {
                    response = JSON.parse(response);
                    $('#pagename').val(response.pageid);
					
					$('#pagecntentid').val(c_id);
					
                    $('#pagecntid').val(id);    
                    $.each(response[id],function(i,row){
						$('#conttitle'+row.language_id).val(row.t_title);
						$('#imagePreview').attr('src',BANNER_IMAGE_PATH+row.page_banner);
                        //$('#pagename').val(row.t_pageid);
                        //$('.pagecontent_'+row.language_id).val(row.content);
                        CKEDITOR.instances['pagecontent'+row.language_id].setData(row.content);
                    });
                }
            });
        }, 0);
    });
}

function deletePageContent(id,c_id) {
	
    confirms(LANG['confirmDeletePagecontent'], function(confirmResult) {
        if (confirmResult == "true") {
            ajLoaderOn();
            var url = 'action=deletePageContentById&id='+id+'&c_id='+c_id;
            $.ajax({
            url: '../ajax/index.php?p=settings',
            type: "POST",
            data: url,
            success: function() {//result
                    ajLoaderOff();
                    //alerts(LANG['deleteSuccess']);
                    flashMsgDisplay(LANG['deletePagecontentSuccess'], 'success-msg');
                    setTimeout(function() {
                       // location.reload();
                       $(".icon-popupcls").trigger("click");
                        getPageContent();
                    }, 1500);
                }
            });
            closeMsg();
		    
        } 
    });
}
function manageDisease(){
    var $distopicrow=$('.diseasetopicdatarow');
    var diseasecount =  $distopicrow.length-1;
    var topiccount =  $('#thcount').children('th').length-1;
    //console.log(topiccount);
    var diseasetopicval = [];
    var $headcnt =   $('#thcount th');
    for(var i=0;i<=diseasecount;i++){
        var deseaseid = $distopicrow.eq(i).attr("deseaseid");
        var topicval = [];
        for(var j=1;j<=topiccount;j++){
            var topicid = $headcnt.eq(j).attr("topicid");
            topicval.push({topicid:topicid, val:$('.disease_'+deseaseid+'tdcount_'+topicid).val()});
        }
        diseasetopicval.push({diseaseid:deseaseid, value:topicval});
        //console.log(JSON.stringify(topicval));
    }
    //console.log(JSON.stringify(diseasetopicval));
    //return;
     ajLoaderOn();
        var url = 'action=saveDisease&diseasetopicval='+JSON.stringify(diseasetopicval);
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function() {//result
                ajLoaderOff();
                //alerts(LANG['deleteSuccess']);
                flashMsgDisplay(LANG['updateParameterSucess'], 'success-msg');
                setTimeout(function() {
                    location.reload();
                   //$(".icon-popupcls").trigger("click")
                    //getPageContent();
                }, 1500);
            }
        });
        closeMsg();
    }
function copyQuestion(phaseid,groupid){
    var cpyfase = $('#cpyfase_'+phaseid+'_'+groupid).val();
    var cpygroup = $('#cpygroup_'+phaseid+'_'+groupid).val();
     ajLoaderOn();
        var url = 'action=copyQuestion&phaseid='+cpyfase+
            '&groupid='+cpygroup+'&curphaseid='+phaseid+'&curgroup='+groupid;
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
                ajLoaderOff();
                console.log(JSON.stringify(response));
                if (response.status == 200) {
                    flashMsgDisplay(LANG['updateQuestionSuccess'], 'success-msg');
                        setTimeout(function() {
                            $(".icon-popupcls").trigger("click");
                            //location.reload();
                        }, 1500);
                    getQuestion(groupid,phaseid);
                }
            }
        });
        closeMsg();
}
function closeConfirmationPopup(){

    $(".customPopcontent").hide();
    $("#customPopoverlay").hide();
    $("#customPopbox").hide();
}
var EXCEPTION_PAGE_TOBE_ADDED		=	"Yet to receive the design.";

function saveQuestionaryHintText(){
    var queshintid=$('#queshintid').val();
    var group_name=$('#grouplist').val();
    var phase_name=$('#phasetype').val();
    var noofdays=$('#noofdays').val();
    var queshinttxt =[];
    var questionid  = $('#questionlist').val();
    $('.queshint').each(function(i,row){
        var queshinttxtval=$(row).val();
        var queslanguageid=$(row).attr('languageid');
        var queshintlangeditid  = $('.queshintlang'+queslanguageid).val();
        queshinttxt.push({queshintlangeditid:queshintlangeditid,queslanguageid:queslanguageid,value:queshinttxtval});
    });
    var queshintitems = JSON.stringify(queshinttxt);
    // var url = 'action=AddEditQuestionHints&questionaryText='+questionaryText+'&group_name='+group_name+
    // '&phase_name='+phase_name+'&queshintstxt='+quesitems;
    var url = 'action=AddEditQuestionHints&&questionid='+questionid+'&queshintitems='+queshintitems+
        '&noofdays='+noofdays+'&group_name='+group_name+'&phase_name='+phase_name+
        '&queshintid='+queshintid;
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            response = JSON.parse(response);
            ajLoaderOff();
            if (response.status_code == 200) {
                flashMsgDisplay(LANG['updateQuestionSuccess'], 'success-msg');
                setTimeout(function() {
                    $(".icon-popupcls").trigger("click");
                       //location.reload();
                }, 1500);
                getQuestionHint();
            }
        }
    });
}
function editQuesHint(title,page,id){
    showQuickAddPop(title,page,id);
    $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        dataType: 'json',
        data:{'action':'editQuestionaryHint','queshintId':id},
        success: function(response) {
            //response = JSON.parse(response);
            console.log(JSON.stringify(response));
            ajLoaderOff();
            /**
             * @param value.queshintid This is question hint id
             * @param value.phaseid This is phase id
             * @param value.groupid This is group id
             * @param value.nodays This is no of days
             * @param value.hinttxt This is hint txt
             * @param value.hintlangid This is hint language id
             * @param value.langid This is value language id
             */
            $.each(response,function(i,row){
                console.log(JSON.stringify(row));
                $.each(row,function(key,value){
                    $('#queshintid').val(value.queshintid);
                    $('#phasetype').val(value.phaseid);
                    $('#grouplist').val(value.groupid);
                    $('#noofdays').val(value.nodays);
                    $('.questhint'+value.langid).val(value.hinttxt);
                    $('.queshintlang'+value.langid).val(value.hintlangid);
                    getPhasQroupQuestion(value.phaseid,value.groupid,function(){
                        $('#questionlist').val(value.quesid);
                    });

                });

            });
        }
    });
}
function getPhasQroupQuestion(phaseid,groupid,sucessHandler){
    var url = 'action=getQuestionaries&phaseid='+phaseid+'&groupid='+groupid;
        $.ajax({
            url: '../ajax/index.php?p=settings',
            type: "POST",
            data: url,
            success: function(response) {
                ajLoaderOff();
                $('#questionlist').html(response);
                if(sucessHandler){
                    sucessHandler();
                }
            }
        });
}
function deleteQuestionHint(queshintid){
    confirms(LANG['confirmDeleteQuestion'], function(confirmResult) {
        ajLoaderOn();
        if(confirmResult) {
            var url = 'action=deleteQuestionHintById&id=' + queshintid;
            $.ajax({
                url: '../ajax/index.php?p=settings',
                type: "POST",
                data: url,
                success: function (response) {
                    //	response = JSON.parse(response);
                    ajLoaderOff();
                    getQuestionHint();
                    if (response == 1) {
                        flashMsgDisplay(LANG['deleteQuestionSuccess'], 'success-msg');
                        setTimeout(function () {
                            //$(".icon-popupcls").trigger("click");
                            closeMsg();
                        }, 1500);
                    }
                }

            });
        }
    });
}
function getQuestionHint(){
    ajLoaderOn();
    var url = 'action=getQuestionHint';
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(response) {
            //response = JSON.parse(response);
            ajLoaderOff();
            $('.queshintcnt').html(response);
            //console.log('questionarylist_' + phaseid + '_' + groupid)
        }
    });
}
function editResetUser(title,page,id){
    showQuickAddPop(title,page,id,'',function(){
        $('#resetuserid').val(id);
    });

}
function resetMonitor(groupid,phaseid){
    var resetuserid = $('#resetuserid').val();
   // var phaseId = phaseid;
    /*var activityques = 0;
    if(groupid==1 && phaseid==2){
        activityques = 1;
    }*/
    var url = 'action=resetQuestionMonitor&userId='+resetuserid+'&phaseId='+phaseid+'&groupId='+groupid;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        success: function() {//response
            ajLoaderOff();
        }
    });
}
function resetAvailableTest(){
    var resetuserid = $('#resetuserid').val();
    var url = 'action=resetAvailableTest&userId='+resetuserid;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        success: function() {//response
            ajLoaderOff();
        }
    });
}
function resetTraining(){
    var resetuserid = $('#resetuserid').val();
    var url = 'action=resetTraining&userId='+resetuserid;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        success: function() {//response
            ajLoaderOff();
        }
    });
}
function ResetUserData(userid){
    var url = 'action=resetUserData&userId='+userid;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        success: function() {//response
            ajLoaderOff();
        }
    });
}
function removeResetUser(){
    var email = $('#resetusername').val();
    var loggedUserId = $('.loggedUserId').val();
    var url = 'action=removeResetUser&email='+email+'&loggedUserId='+loggedUserId;
    ajLoaderOn();
    /**
     * @param  response.status_message This is status message
     */
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        dataType:'JSON',
        success: function(response) {
            ajLoaderOff();
            //if(response.status_message){
                flashMsgDisplay(response.status_message, 'success-msg');
                    setTimeout(function() {
                        closeMsg();
						url = '../admin/index.php?p=resetusers';
						window.location=url;
                    }, 1500);
            //}
        }
    });
}
function resetPassword(){
    var email = $('#resetusername').val();
    var url = 'action=resetPassword&email='+email+'&password='+USER_STATUS_ACTIVE;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        dataType:'JSON',
        success: function(response) {
            ajLoaderOff();
            //if(response.status_message){
                flashMsgDisplay(response.status_message, 'success-msg');
                    setTimeout(function() {
                        closeMsg();
						url = '../admin/index.php?p=resetusers';
						window.location=url;
                    }, 1500);
            //}
        }
    });
}
function resetActive(){
    var email = $('#resetusername').val();
    var url = 'action=resetPassword&email='+email+'&active='+USER_STATUS_ACTIVE;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        dataType:'JSON',
        success: function(response) {
            ajLoaderOff();
            //if(response.status_message){
                flashMsgDisplay(response.status_message, 'success-msg');
                    setTimeout(function() {
                        closeMsg();
						url = '../admin/index.php?p=resetusers';
						window.location=url;
                    }, 1500);
            //}
        }
    });
}
function removecomplete(){
    var email = $('#resetusername').val();
    var loggedUserId = $('.loggedUserId').val();
    var url = 'action=removecomplete&email='+email+'&loggedUserId='+loggedUserId;
    ajLoaderOn();
    /**
     * @param  response.status_message This is status message
     */
        $.ajax({
        url: '../ajax/index.php?p=questionaries',
        type: "POST",
        data: url,
        dataType:'JSON',
        success: function(response) {
            ajLoaderOff();
            //if(response.status_message){
                flashMsgDisplay(response.status_message, 'success-msg');
                    setTimeout(function() {
                        closeMsg();
						url = '../admin/index.php?p=resetusers';
						window.location=url;
                    }, 1500);
            //}
        }
    });
}
function saveStrengthProgrm(){
    var description = $("#description").val();
    var circutType 	= $("#strengthpgmtype").val();
    var loggedUserId=$(".loggedUserId").val();
    var strengthProgramId=$("#strengthProgramId").val();
    var url = 'action=createWeekAssignProgram&description='+description+'&val='+circutType+
        '&loggedUserID='+loggedUserId+'&strengthProgramId='+strengthProgramId;
    ajLoaderOn();
    /**
     * @param response.strength_program_id This is strength_program_id
     */
        $.ajax({
        url: '../ajax/index.php?p=members',
        type: "POST",
        data: url,
        dataType:'JSON',
        success: function(response) {
            ajLoaderOff();
            console.log(JSON.stringify(response));
            //if(response.status_message){
                flashMsgDisplay(response.status_message, 'success-msg');
                    setTimeout(function() {
                        closeMsg();
                    }, 1500);
                    if(response.strength_program_id){
                        loadStrengthProgramSeries(response.strength_program_id);
                    }

            //}
        }
    });
}
function loadStrengthProgramSeries(strengthpgmid){
    $('.strengthpgmseries').css("display",'block');
    $('#strengthProgramIdRefer').val(strengthpgmid);
}
function saveStrengthPgmTraining(){
    var strengthProgramId 	= $("#strengthProgramIdRefer").val();
    var training_week 		= $("#training_week").val();
    var series 			= $("#series").val();
    var reputation 		= $("#reputation").val();
    var time 				= $("#time").val();
    var strength_percentage = $("#strength_percentage").val();
    var rest 				 = $("#rest").val();
    var loggedUserId 		 = $('.loggedUserId').val();
    var url = 'action=strengthProgramTrainingAddUpdate&strengthProgramId='+strengthProgramId+
        '&training_week='+training_week+'&series='+series+'&reputation='+reputation+
        '&strength_percentage='+strength_percentage+'&rest='+rest+'&time='+time+
        '&loggedUserId='+loggedUserId;
    ajLoaderOn();
        $.ajax({
        url: '../ajax/index.php?p=members',
        type: "POST",
        data: url,
        dataType:'JSON',
        success: function(response) {
            ajLoaderOff();
            if(response.status_message){
                $('.strengthtraining').css("display",'block');
                $('.training_cls').val('');
                loadStrengthTraining(strengthProgramId);
                flashMsgDisplay(response.status_message, 'success-msg');
                setTimeout(function() {
                    closeMsg();
                }, 1500);
            }
        }
    });
}
function loadStrengthTraining(strengthProgramId){
    var url = 'action=strengthProgramTraining&tapvalue='+strengthProgramId;
        $.ajax({
        url: '../ajax/index.php?p=members',
        type: "POST",
        data: url,
        success: function(response) {
            ajLoaderOff();
            $('.strngthtrain').html(response);
        }
    });
}
function editStrengthProgram(programid){

}
function saveMachine(){
	
	
    var machineid 	= $("#machineid").val();
	var machine_number = $("#machine_number").val();
    var nameofmachine 	= $("#machinename").val();
    var brand 			= $('#brandtype').val();
    var type 			= $('#machinetype').val();
    var group 			= $('#machinegroup').val();
    var subtype			= $('#subtype').val();
    var description 		= $('#machinedescription').val();
    var loggedUserId 		 = $('.loggedUserId').val();
    var imagename 		 = $('#machineicon').val();
	var video_url        = $('#video_url').val();
    var coach_club 		 = $('#club').val();
    var $activemachine   =  $('input[name="activemachine"]:checked');
    var $outofordermachine   =  $('input[name="outofordermachine"]:checked');
    var $allowmixtrain   =  $('input[name="allowmixtrain"]:checked');
    var $allowcircuittrain   =  $('input[name="allowcircuittrain"]:checked');
    var $allowfreetrain   =  $('input[name="allowfreetrain"]:checked');
    var $certifymyfitplan   =  $('input[name="certifymyfitplan"]:checked');
	var image_url = $('#image_url').val();
	var confactor = $('#confactor').val();
    var activemachine= ( $activemachine.val() ) ? $activemachine.val():0;
    var outofordermachine= ( $outofordermachine.val() ) ? $outofordermachine.val():0;
    var allowmixtrain 		 = ($allowmixtrain.val()) ? $allowmixtrain.val():0;
    var allowcircuittrain 		 = ($allowcircuittrain.val()) ? $allowcircuittrain.val():0;
    var allowfreetrain 		 = ($allowfreetrain.val()) ? $allowfreetrain.val():0;
    var certifymyfitplan 		 = ($certifymyfitplan.val()) ? $certifymyfitplan.val():0;
	if(nameofmachine==''){
		alert("Please fill machine name");
	}
	else if(machine_number==''){
		alert("Please fill machine number");
	}
	else if(brand==''){
		alert("Please select machine brand");
	}
	else if(type==''){
		alert("Please select machine type");
	}
	else if(group==''){
		alert("Please select machine group");
	}
	else if(subtype==''){
		alert("Please select machine subtype");
	}
	else if(coach_club==''){
		alert("Please select club");
	}
	else if(confactor==''){
		alert("Please fill confaction factor");
	}
	
	else{
		var weights= new Array();
		var pinpositions= new Array();
					$('input[name^="weight"]').each(function() 
					{
						
					weights.push($(this).val());
					});
					$('input[name^="pinposition"]').each(function() 
					{
					pinpositions.push($(this).val());
					});
					
					
    //var active 			= ($('.rdgrp_active').is(":checked"))?1:2;

        $.ajax({
            url: '../ajax/index.php?p=members',
            type: "POST", 
            data: {action:'saveNewMachineDetails',nameofmachine:nameofmachine,
                brand:brand,type:type,group:group,
				clubautoid:machine_number,
				subtype:subtype,description:description,
				createdby:loggedUserId,imagename:imagename,image_url:image_url,video_url:video_url,
                coach_club:coach_club,editID:machineid,confactor:confactor,
                active:activemachine,machine_order:outofordermachine,
                mixtraining:allowmixtrain,circuittraining:allowcircuittrain
	,freetraining:allowfreetrain,certified:certifymyfitplan,weights:weights,pinpositions:pinpositions},
            success: function() {//response
                ajLoaderOff();     
                flashMsgDisplay("Machine Added sucessfully", 'success-msg');
                    setTimeout(function() {
                        closeMsg();
						url = '../admin/index.php?p=managemachine';
						window.location=url;
                    }, 1500);  
            }
        });  
	}		
}
function saveMachineMapping(){
    var clubid = $('#scltclub').val();
    var machines = [];
    $.each($(".centerListRt option:selected"), function(i,row){
        machines.push($(row).val());
    });
    var machine  = JSON.stringify(machines);
    var loggedUserId 		 = $('.loggedUserId').val();
    var url = 'action=saveMachineMapping&clubid='+clubid+'&machine='+machine+'&loggedUserId='+loggedUserId;
        $.ajax({
        url: '../ajax/index.php?p=members',
        type: "POST",
        data: url,
        success: function(response) {
            ajLoaderOff();
            $('.strngthtrain').html(response);
        }
    });
}
function pushMessageClub(){
    var clubid  = $('#slctculb').val();
    var messagetext  = $('#messagetext').val();
    var loggedUserId 		 = $('.loggedUserId').val();
    var url = 'action=savePushMessage&clubid='+clubid+
                '&messagetext='+encodeURIComponent(messagetext)+
                '&loggedUserId='+loggedUserId;
    $.ajax({
        url: '../ajax/index.php?p=members',
        type: "POST",
        data: url,
        success: function(response) {
            ajLoaderOff();
            flashMsgDisplay(response, 'success-msg');
                setTimeout(function() {
                    closeMsg();
                }, 1500);
        }
    });
}
function validateSteps(parentSelector){//errordisplay
    try {
        var _nonempty	=	jQuery(parentSelector + " .req-nonempty");
        var _none_zero	=	jQuery(parentSelector + " .req-nonzero");
        var _email		=	jQuery(parentSelector + " .req-email");
        var _emptyselect=	jQuery(parentSelector + " .req-emptyselect");
        //var _ukpostcode	=	jQuery(parentSelector + " .req-ukpostcode");

        var noError		=	true;
        _nonempty.each(function(){
            var $this=	jQuery(this);
            if(jQuery.trim($this.val())==''){
                noError	=	false;
                var errormsg=$this.attr("validate-fail");
                //var htmlcnt=(errormsg) ? errormsg:'';
                if(errormsg){
                    $(".info_icon").css("display","inline-block");
                }
                //var elem = $this.parent().find('label').html(htmlcnt);
                $this.css({"border":"1px solid red"});
                $this.keyup(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.next('label').html('');
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();
                });
                $this.change(function(){
                    $this.css({"border":"1px solid #000"});
                    //$this.parent().find('span').removeClass('info_icon');
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();

                    //$this.parent().find('label').html(htmlcnt);
                });
                $('.quick_pop_content').scrollTop(0);
            } else {
                $this.css({"border":"1px solid #000"});
                // $this.parent().find('span').hide();
                // $this.parent().find('.error_info').hide();
            }
        });
        _email.each(function(){
            var $this	=	jQuery(this);
            if(!validateEmail($this.val())){
                noError	=	false;
                $this.css({"border":"1px solid red"});
                $this.keyup(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();
                });
                $('.quick_pop_content').scrollTop(0);
            } else{
                $this.css({"border":"1px solid #000"});
            }
        });
        _emptyselect.each(function(){
            var $this	=	jQuery(this);
            noError		=	false;
            var errormsg='';
            errormsg+= $this.attr("validate-fail");
            var html='<div>'+errormsg+'</div>';
            $this.after(html);
            if((!$this.find("option:selected").text()) || ($this.find("option:selected").text()=="")){
                $this.css({"border":"1px solid red"});
                $this.change(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();
                });
            } else{
                $this.css({"border":"1px solid #000"});
            }
        });
        _none_zero.each(function(){
            var $this	=	jQuery(this);
            if($this.val()=='0'){
                noError	=	false;
                var errormsgzero=$this.attr("validate-fail");
                //var htmlcnt=errormsgzero;
                if(errormsgzero){
                    $(".info_icon").css("display","inline-block");
                }
                //var elem = $this.parent().find('span').addClass('info_icon');
                //$this.next().html('<span class="info_icon"></span>');
                //var elem = $this.parent().find('label').html(htmlcnt);
                //$this.parent().find("label").before('<span class="info_icon"></span>');
                $this.css({"border":"1px solid red"});
                $this.change(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.next('label').html('');
                    //$this.parent().find(".info_icon").css("display","none");
                    $this.parent().find('span').removeClass('info_icon');
                });
                $('.quick_pop_content').scrollTop(0);
            } else{
                $this.css({"border":"1px solid #000"});
            }
        });
       /* if(_ukpostcode) {
            _ukpostcode.each(function(){
                var $this=	jQuery(this);
                if(!validateUKPostCode($this.val())){
                    noError	=	false;
                    $this.css({"border":"1px solid red"});
                    $this.siblings('.cval-lbl-error').html("Please enter valid UK Post code");
                } else{
                    $this.siblings('.cval-lbl-error').html("");
                }

            });
        }*/
        return noError;
    } catch (e) {
        alert("ValidateSteps " + e);
    }
}
function loadAddEducationPopup(title,page,groupid){
    showQuickAddPop(title,page,'','',function(){
        $('#groupid').val(groupid);
    });
}
function saveEducation(groupid){
    var educationarr = [];
    //var eduinfotxtarr = [];
    //var groupid   = $('#groupid').val();
    /*$('.edutitle'+groupid).each(function(i,row){
        var edutitletxt=$(row).val();
        var edutitletxtlangid=$(row).attr('languageid');
        var edulangeditid  = 0;//$('.edutitletxtlangid_'+edutitletxtlangid).val();
        edutitletxtarr.push({edulangeditid:edulangeditid,value:edutitletxt,edutitletxtlangid:edutitletxtlangid});
    });*/
    //alert(CKEDITOR.instances['eduinfotxt31'].getData());
    //return;
    for(var i=1;i<=14;i++){
        //var eduindexarr= [];
        var eduinfotxtarr= [];
        var edueditid = $('.eduid'+groupid+i).val();
        var indexgroup = $('.indextype_'+i).val();
        var days = i;
        $('.eduinfotxt_'+groupid+i).each(function(key,row){
            //var edutitletxt=$(row).val();
            var eduinfotxtlangid=$(row).attr('languageid');
            var edulangeditid  = $('.edutitletxtlangid_'+groupid+i+eduinfotxtlangid).val();
            var edutextval = CKEDITOR.instances['eduinfotxt'+eduinfotxtlangid+groupid+i].getData();
            eduinfotxtarr.push({
                edulangeditid:edulangeditid,
                eduinfotxtlangid:eduinfotxtlangid,
                edutitleinfo:edutextval
                /*CKEDITOR.instances['eduinfo'+edutitletxtlangid].getData()*/
            });
        });
        //educationarr.push({eduindexarr:eduindexarr,edulanginfo:eduinfotxtarr});
        educationarr.push({edueditid:edueditid,indexgroup:indexgroup,days:days,info:eduinfotxtarr});
    }
    var eduinfotxtitems = JSON.stringify(educationarr);
    //console.log(JSON.stringify(educationarr));
    //return;
    ajLoaderOn();
        //var url = 'action=AddEditEducation&eduinfotxtitems='+eduinfotxtitems+'&group_name='+groupid;
        $.ajax({
            url: '../ajax/index.php?p=settings',
            type: "POST",
            data: {"action":"AddEditEducation", "eduinfotxtitems":eduinfotxtitems, "group_name":groupid},
            success: function(response) {
                response = JSON.parse(response);
                ajLoaderOff();
                if (response.status == 200) {
                    flashMsgDisplay(LANG['updateQuestionSuccess'], 'success-msg');
                        setTimeout(function() {
                            $(".icon-popupcls").trigger("click");
                            location.reload();
                        }, 1500);
                }
            }
        });
}
function manageGoal(){
    var $gtopicrow =   $('.goaltopicdatarow');
    var goalcount =  $gtopicrow.length-1;
    var $headcnt  =   $('#thcount th');
    //var topiccount =  $('#thcount').children('th').length-1;
    var topiccount =  $headcnt.length-1;

    var goaltopicval = [];
    for(var i=0;i<=goalcount;i++){
        var goalid = $gtopicrow.eq(i).attr("goalid");
        var topicval = [];
        for(var j=1;j<=topiccount;j++){
            var topicid = $headcnt.eq(j).attr("topicid");
            topicval.push({topicid:topicid, val:$('.goal_'+goalid+'tdcount_'+topicid).val()});
        }
        goaltopicval.push({goalid:goalid, value:topicval});
        console.log(JSON.stringify(topicval));
    }
     ajLoaderOn();
        var url = 'action=saveGoal&goaltopicval='+JSON.stringify(goaltopicval);
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function() {//result
                ajLoaderOff();
                //alerts(LANG['deleteSuccess']);
                flashMsgDisplay(LANG['updateParameterSucess'], 'success-msg');
                setTimeout(function() {
                    location.reload();
                   //$(".icon-popupcls").trigger("click")
                    //getPageContent();
                }, 1500);
            }
        });
        closeMsg();
    }
function exportTranslationList(){
     ajLoaderOn();
        var url = 'action=exportTranslationList';
        $.ajax({
        url: '../ajax/index.php?p=settings',
        type: "POST",
        data: url,
        success: function(result) {
                window.location.href	=	'../temp/'+result;
                ajLoaderOff();
                //alerts(LANG['deleteSuccess']);
                flashMsgDisplay("Translation Export Sucessfully", 'success-msg');
                setTimeout(function() {
                    //location.reload();
                   //$(".icon-popupcls").trigger("click")
                    //getPageContent();
                }, 1500);
            }
        });
        closeMsg();
    }
function importTranslationList(){
    showQuickAddPop('Upload File','uploadimportfile','','',function(){
    });

}
function uploadImportFile(){
    uploadImage('', 'uploadImportFile');

}
function importFile(){
    var importfile = $('#fileimport').val();
     ajLoaderOn();
        var url = 'action=importTranslationList&importfile='+importfile;
        $.ajax({
            url: '../ajax/index.php?p=settings',
            type: "POST",
            data: url,
            success: function() {//result
                ajLoaderOff();
                //alerts(LANG['deleteSuccess']);
                flashMsgDisplay("Translation Import Sucessfully", 'success-msg');
                setTimeout(function() {
                    location.reload();
                   //$(".icon-popupcls").trigger("click")
                    //getPageContent();
                }, 1500);
            }
        });
}


