/**        
 * Descripton : Coach Personal Information Updates 
 * Created On : 12 Sep 2014
 **/
$(document).ready(
    function() {
        $("#btnSaveCoach").on(
            'click', function(e) {
                e.preventDefault();
                var validationResult = validationForm([
                    'textbox&first_name&' + LANG['enterFirstName'],
                    'textbox&last_name&' + LANG['enterLastName'],
                    'textbox&mobile&' + LANG['enterGSM'],
                    'textbox&doorNo&' + LANG['enterDoorNo'],
                    'textbox&address&' + LANG['enterStreet'],
                    'textbox&workStart&' + LANG['enterWorkStart'],
                    'textbox&club_id&' + LANG['selectClub'],
                    'textbox&email&' + LANG['enterEmail'],
                    'textbox&password&' + LANG['enterPwd']
                ], 'personalInfoForm', '2');
                if (validationResult) {
                    var resetPwd = $('.resetPwd').prop('checked');
                    var userId = $('#userId').val();
                    var $password = $('#password');
                    var $cpwd = $('#cpassword');
                    var $emailob = $('#email');
                    var emailId = $.trim($emailob.val());

                    if (resetPwd || userId == "") {

                        if ($.trim($emailob.val()) == '') {
                            //alerts(LANG['enterEmail']);
                            flashMsgDisplay(LANG['enterEmail'], 'failure-msg');
                            $emailob.focus();
                            return false;
                        } else {
                            var validEmail = validateEmail(emailId);
                            if (!validEmail) {
                                //alerts(LANG['invalidEmail']);
                                flashMsgDisplay(LANG['invalidEmail'], 'failure-msg');
                                $emailob.focus();
                                return false;
                            } else {
                                validEmail = validateEmail(emailId);
                                if (!validEmail) {
                                    //alerts(LANG['invalidEmail']);
                                    flashMsgDisplay(LANG['invalidEmail'], 'failure-msg');
                                    $emailob.focus();
                                    return false;
                                }
                            }
                            if ($.trim($password.val()) == '') {
                                //alerts(LANG['enterPwd']);
                                flashMsgDisplay(LANG['enterPwd'], 'failure-msg');
                                $password.focus();
                                return false;
                            }
                            if ($.trim($cpwd.val()) == '') {
                                //alerts(LANG['enterConfirmPwd']);
                                flashMsgDisplay(LANG['enterConfirmPwd'], 'failure-msg');
                                $cpwd.focus();
                                return false;
                            }
                            if ($.trim($password.val()) != $.trim($cpwd.val())) {
                                //alerts(LANG['mismatchPwd']);
                                flashMsgDisplay(LANG['mismatchPwd'], 'failure-msg');
                                $password.focus();
                                return false;
                            }
                        }
                    }

                    var workStart = $.trim($('#workStart').val());
                    var workEnd = $.trim($('#workEnd').val());
                    var date1 = workStart.split('-');
                    var date2 = workEnd.split('-');
                    date1[2] = date1[2];
                    date2[2] = date2[2];
                    date1 = new Date(date1[2], date1[1], date1[0]);
                    date2 = new Date(date2[2], date2[1], date2[0]);
                    var date1_unixtime = parseInt(date1.getTime() / 1000);
                    var date2_unixtime = parseInt(date2.getTime() / 1000);
                    var timeDifference = date2_unixtime - date1_unixtime;
                    var timeDifferenceInHours = timeDifference / 60 / 60;
                    var timeDifferenceInDays = timeDifferenceInHours / 24;
                    if (timeDifferenceInDays < 0) {
                        //alerts(LANG['validStartonEndon']);
                        flashMsgDisplay(LANG['validStartonEndon'], 'failure-msg');
                        return false;
                    }

                    ajLoaderOn();
                    var dataPost = $("#personalInfoForm").serialize();

                    var file_data = $('#uploadBtn').prop('files')[0];
                    var form_data = new FormData();
                    if (form_data) {
                        form_data.append("file", file_data);
                        form_data.append("dataPost", dataPost);
                    }
                    //var clubId = $("#club_id").val();
                    var uid = $("#uid").val();
                    $.ajax(
                        {
                            type: "POST",
                            url: "../ajax/index.php?p=coach&action=updateCoach",
                            data: form_data,
                            cache: false,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            success: function(result) {
                                ajLoaderOff();
                                /**
                                 * @param result.movesmart object
                                 * @param result.user_id object
                                 */
                                if (result.movesmart.status == 'Error') {
                                    //alerts(LANG['loginIdExist']);
                                    flashMsgDisplay(LANG['loginIdExist'], 'failure-msg');
                                    return false;
                                }
                                //Image Upload
                                if (userId == "") {
                                    var file_data = $('#uploadBtn').val();
                                    if (result.user_id != "" && file_data != "") {
                                        //uploadImage(result.user_id,'profileCoachImage');               
                                    }
                                }
                                location.href = "index.php?p=coachList";
                                /*if (result.myfitplan.userId > 0) {
                                var locationRedirect = "index.php?p=coachManagementEdit&clubId=" + clubId + "&
                                uid=" + uid + "&id=" + result.myfitplan.userId;
                                }*/
                                //location.href = locationRedirect;
                                //End
                            }
                        }
                    );

                }
            }
        );
        //Validate User Role Assign.  
        $("#assignRole").on(
            'click', function() {
                var clubIdRole = $("#clubIdRole").val();
                var userTypeId = $("#userTypeId").val();
                if (clubIdRole == "") {
                    //alerts(LANG['selectClub']);
                    flashMsgDisplay(LANG['selectClub'], 'failure-msg');
                    return false;
                }
                if (userTypeId == "") {
                    //alerts(LANG['selectRole']);
                    flashMsgDisplay(LANG['selectRole'], 'failure-msg');
                    return false;
                }
            }
        );

    }
);

function personIdExist(e) {
    $.ajax(
        {
            url: '../ajax/index.php?p=members',
            type: "POST",
            data: 'action=personIdExist&userId=' + $(e).val(),
            success: function(response) {
                //Ajax loader off
                ajLoaderOff();

                if (response > 0 && $(e).val() != "") {
                    //alerts('Person Id Already Exist');
                    flashMsgDisplay('Person Id Already Exist', 'failure-msg');
                    $(e).val('');
                    $(e).focus();
                }
            }
        }
    );
}

function primaryClubUpdate(userId,clubId,employeeCenterId,isPrimary){
    if(isPrimary==1) {
        return false;
    }
    confirms(
        LANG['primaryConfirmation'],function(confirmResult){
            if(confirmResult=="true") {
                $.ajax(
                    {
                        url: '../ajax/index.php?p=members',
                        type: "POST",
                        data: 'action=primaryClubUpdate&userId='+
                                    userId+'&clubId='+clubId+'&employeeCenterId='+employeeCenterId,
                        success: function() {
                            //Ajax loader off
                            ajLoaderOff();
                            closeMsg();
                            window.location.href="index.php?p=coachManagementEdit&id="+userId;            
                        }
                    }
                );
            } else {
                closeMsg();
                window.location.href="index.php?p=coachManagementEdit&id="+userId;
            }
        }
    );    
}

function deleteRow(uniqueId){
    confirms(
        LANG['confirmDeleteUser'],function(confirmResult){
            if(confirmResult=="true") {
                ajLoaderOn();    
                var dataString = "userId="+uniqueId+"&action=deleteCoach";
                $.ajax(
                    {
                        type: "POST",
                        url: "../ajax/index.php?p=coach",
                        data: dataString,
                        cache: false,
                        success: function() {
                            ajLoaderOff();
                            // alerts(LANG['deleteSuccess']);
                            setTimeout(
                                function(){
                                    window.location.href="index.php?p=coachManagementEdit";
                                },1500
                            );
                        }
                    }
                );
            } else {
                closeMsg();
            }
        }
    );
}

