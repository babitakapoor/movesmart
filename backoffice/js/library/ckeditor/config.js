﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	/**/
	var rootpath = "http://localhost:8080/fitclass_apps/adminnew/js/library/ckfinder/"
	config.filebrowserBrowseUrl = rootpath+'ckfinder.html';
    config.filebrowserImageBrowseUrl = rootpath+'ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = rootpath+'ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = rootpath+'core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = rootpath+'core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl  = rootpath+'core/connector/php/connector.php?command=QuickUpload&type=Flash';
	/**/
};
