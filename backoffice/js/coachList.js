/**
 * Author        : Punitha Subramani
 * Since         : 06-Oct-2014.
 * Modified By   : Punitha Subramani
 * Modified Date : 11-Oct-2014
 * Description   : Coach List Js.
 **/
$(document).ready(
    function() {
        $(".clearSearch").on(
            'click', function() {
                $(".coach-search-sec select option").removeAttr("selected");
                $(".coach-search-sec :text").val("");
                $(".coach-search-sec form").submit();
            }
        );
    }
);

function deleteRow(uniqueId) {
    confirms(
        LANG['confirmDeleteCoach'], function(confirmResult) {
            if (confirmResult == "true") {
                ajLoaderOn();
                var dataString = "userId=" + uniqueId + "&action=deleteCoach";
                $.ajax(
                    {
                        type: "POST",
                        url: "../ajax/index.php?p=coach",
                        data: dataString,
                        cache: false,
                        success: function() {
                            ajLoaderOff();
                            //alerts(LANG['deleteSuccess']);
                            flashMsgDisplay(LANG['deletecoachSuccess'], 'success-msg');
                            setTimeout(
                                function() {
                                    location.reload();
                                }, 1500
                            );
                        }
                    }
                );
            } else {
                closeMsg();
            }
        }
    );
}
