/**        
 * Descripton : Add or Update club details
 * Created On : 04-Nov-2014
 **/  
$(document).ready(
    function() {
        $("#btnSaveClub").on(
            'click', function(e) {
                e.preventDefault();
                var validationResult = validationForm([
                    'textbox&club_name&' + LANG['enterClubName'],
                    'textbox&email_id&' + LANG['enterEmailId'],
                    'textbox&street&' + LANG['enterStreetName'],
                    'textbox&number&' + LANG['enterNumber'],
                    'textbox&bus&' + LANG['enterBus'] ,
                    'textbox&post_code&' + LANG['enterPostCode'] ,
                    'textbox&location&' + LANG['enterLocation'] ,
                    'textbox&commercial_name&' + LANG['enterCommercial'] ,
					 'dropdown&group_name&Select group name' ,
                    'radio&status_id&' + LANG['selectStatus']
                ], 'clubDetailsForm', '2');  
             
                if (validationResult) {
                    var $emailelem = $('#email_id');
                    var $factmedic = $('#ismedical');
                    var validEmail = validateEmail($emailelem.val());
                    if (!validEmail) {
                        //alerts(LANG['invalidEmail']);
                        flashMsgDisplay(LANG['invalidEmail'], 'failure-msg');
                        $emailelem.focus();
                        return false;
                    } else {
                        validEmail = validateEmail($emailelem.val());
                        if (!validEmail) {
                            //alerts(LANG['invalidEmail']);
                            flashMsgDisplay(LANG['invalidEmail'], 'failure-msg');
                            $emailelem.focus();
                            return false;
                        }
                    }
                    if($factmedic.is(":checked")) {
                        $factmedic.val(1);
                    }else{
                        $factmedic.val(0);
                    }
                    ajLoaderOn();
                    var dataPost = $("#clubDetailsForm").serialize();
                    $.ajax(
                        {
                            type: "POST",
                            url: "../ajax/index.php?p=adminLight&action=clubAddUpdate",
                            data: dataPost,
                            cache: false,
                            dataType: 'json',
                            success: function(result) {
                                ajLoaderOff();
                                /**
                                 * @param result.status string  - this is used to check fot status value
                                 * @param result.club_id string  - this is used to check fot status value
                                 */
                                if (result.status == 'success') {
                                    location.reload();
                                }
                                else if (result.status == 'insert_success') {
                                    window.location = "index.php?p=manageClubEdit&id=" + result.club_id;
                                }
                            }
                        }
                    );

                }
            }
        );
    }
);

    $(document).delegate(
        '.check_box',"click",function(){
            console.log('.check_box click');
            var $this        =    $(this);
            var $chkinput    =    $this.find("input");
            if($this.hasClass('checked')) {
                console.log('.check_box click remove');
                $chkinput.removeAttr('checked');
                $this.removeClass("checked");
            } else {
                console.log('.check_box click check');
                $this.find("input").attr('checked', 'checked');
                $this.addClass("checked");
            }
        }
    );    

    /* To delete the Employee Center */
    function deleteRow(uniqueId) {    
        confirms(
            LANG['confirmDeleteUser'], function(confirmResult) {
                if (confirmResult == "true") {
                    ajLoaderOn();
                    var dataString = "empCenterId=" + uniqueId + "&action=deleteEmployeeCentre";
                    $.ajax(
                        {
                            type: "POST",
                            url: "../ajax/index.php?p=adminLight",
                            data: dataString,
                            cache: false,
                            success: function() {
                                //alerts(LANG['deleteSuccess']);
                                ajLoaderOff();
                                flashMsgDisplay(LANG['deleteSuccess'], 'success-msg');
                                setTimeout(
                                    function() {
                                        location.reload();
                                    }, 1500
                                );
                            }
                        }
                    );
                    closeMsg();
                } else {
                    closeMsg();
                }
            }
        );
    }

