<?php
/**
 * PHP version 5.

 * @category Include

 * @package QuickAddPopup

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @link http://movesmart.company/admin/

 * @description content which need to show for quick add popup.
 */
?>
    <!-- Quick add pop begins -->
    <!--<div class="quick_add_pop_disp">-->
    <!--<div class="quick_add_pop_disp_layer"></div>-->
    <div class="quick_add_pop_disp popup-holder small-popup">
        <div class="close_link_container">
            <a href="javascript:void('0')" class="icon icon-popupcls">close</a>
        </div>
        <div class="popup-header">
            <h2 class="quick_pop_title"></h2>
        </div>
        <div class="pop-content quick_pop_content"></div>
    </div>
    <div class="popup-mask"></div>
    <!--</div>-->
    <!-- Quick add pop ends -->