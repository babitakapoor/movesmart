<?php
/**
 * PHP version 5.

 * @category Include

 * @package ChangeClubPopup

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @link http://movesmart.company/admin/

 * @description functions related to change club popup.
 */
?>


  <!--Club change pop begins -->
    <?php
        $changeClubPopDisp = 'none';
    if (isset($_SESSION['showClubChangePop'])) {
        //$changeClubPopDisp = 'block';
        unset($_SESSION['showClubChangePop']);
    }
    ?>
    <div class="popup-holder change_club_center" id="changeClubPopup" style="display:<?php echo $changeClubPopDisp; ?>">
        <a href="javascript:void('0')" class="icon icon-popupcls">close</a>
        <div class="popup-header">
        <h2 class="quick_pop_title">Change Club</h2>
        </div>
        <div class="pop-content">

        <div id="changeEmployeeClubContainer">

    <?php

            $clubsArr = isset($_SESSION['associatedClubs']) ? $_SESSION['associatedClubs'] : array();
            $html = '';

    foreach ($clubsArr as $club) {
        $eclubid    =   $club['r_club_id'];
        $sel = ($_SESSION['currentClubId'] == $eclubid) ? 'checked="checked"' : '';
        ?>
        <div class="table">
            <div class="table-row">
                <div class="table-cell">
                    <div class="input-group">
                        <div class="in-cell cus-check">
                            <input type="radio" id="<?php echo $eclubid;?>" name="club_name"
                                   value="<?php echo $eclubid?>" <?php echo $sel;?> class="oxy">
                            <label for="<?php echo $eclubid;?>">
                                <?php echo $club['club_name']."(".$club['usertype'].")"; ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?php
        /*$html .= <<<HTML
                    <div class="table">
                            <div class="table-row">
                                <div class="table-cell">
                                    <div class="input-group">
                                        <div class="in-cell cus-check">
                                            <input type="radio" 
                                                    id="{$club['r_club_id']}" 
                                                    name="club_name" 
                                                    value="{$club['r_club_id']}" {$sel} 
                                                    class="oxy">
                                            <label for="{$club['r_club_id']}">
                                                {$club['club_name']} ({$club['usertype']})
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            </div>
                    
HTML;*/
    }
            //echo $html;
        ?>
        </div>
        <div class="row-sec btn-sec">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="Cancel">
        <input type="button" onclick="changeEmployeeCompany();" class="btn black-btn fr" value="Change">
        </div>

        </div>
    </div>
    <div class="popup-mask" style="display:<?php echo $changeClubPopDisp; ?>"></div>
  <!--Club change pop ends -->