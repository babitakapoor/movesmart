<?php
/**
 * PHP version 5.
 
 * @category Include
 
 * @package Define
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description this file is used to define any constants.
 */
ini_set('display_errors', '1');
ini_set('date.timezone', 'Europe/Amsterdam');
set_time_limit(0);
ini_set('max_execution_time', 0);

require_once 'session.php';
require_once 'dbconfig.php';

define('DIR_ROOT_PATH', dirname(__FILE__));
define('DIR_ROOT_ADMIN_PATH', DIR_ROOT_PATH.'/');

/*
Defining the version for clean the cache by using CSS and JS. 
**/
define('MY_VERSION', '20150817'); // Format year month date
define('DEFAULT_LANG', 1);

define('LANG_ENG', 1);
define('LANG_DUT', 2);
define('LANG_RUS', 3);

//Role
define('ROLE_ADMIN', '2');
define('ROLE_BACKOFFICE', '4');
define('ROLE_COACH', '1');

//Local database connection
define('MODULE_VERSION', 'Version 1.1.2818');
define('MODULE_VERSION_DATE', '15-07-2015');

/* To Enable/Disable user rights and permissions */
define('USER_RIGHTS_CHECK_FOR_APPLICATION', 1);
    //Note: If value is set to 0, Any user can access any page in this application.
define('TRAINING_NO_OF_WEEKS', 12); // defined by sai for Temperory ;

define('TRAINING_TYPE_ACTIVITY', 1); // defined by SNK trainig type;
define('TRAINING_TYPE_CARDIO', 2); // defined by SNK trainig type;

/*Path settings*/
$currentDirectory = getcwd();
    $moduleArray = array('coach', 'cron', 'ajax', 'ios', 'admin', 'import', 'member');
    $rootPath = str_replace($moduleArray, '', $currentDirectory);

    $themeId = isset($_SESSION['theme']) ? $_SESSION['theme'] : 2;

    define('THEME_ID', $themeId);

if (THEME_ID == '1') {
    define('THEME_NAME', 'movesmart');

    //define('TITLE','Movesmart'); /* Site Title */	

    define('DISPLAY_MEDICAL_INFO', '1'); /* Display Medical Info details for members */
} else {
    define('THEME_NAME', 'movesmart');

    //define('TITLE','Movesmart'); /* Site Title */	

    //define('DISPLAY_MEDICAL_INFO','1'); /* Display Medical Info details for members */
}

//XX define('BASE_URL','http://'.$_SERVER['HTTP_HOST'].'/fitclass_apps/fitclass');
//XX MK Uncommanded
//MK Commanded And moved to dbconfig
//define('BASE_URL','http://'.$_SERVER['HTTP_HOST'].'/adminnew/');
//XX define('POINT_SYSTEM_PATH', 'http://127.0.0.1/ps/index.php');

/*Common Constants*/
define('QN', '?');

/* GET DIRECTORY NAME: getcwd() PHP function helps to get the current directory */
define('CURRENT_DIR', rtrim(getcwd(), DS).DS);

$currentDirectory = getcwd();
$rootPath = dirname($currentDirectory);

define('BASE_DIR', $rootPath.DS);

define('WRITE_SERVICE_LOG', false);
define('WEBSERVICE_LOG_PATH', BASE_DIR.'log'.DS);
define('FOLDER_PATH', dirname(pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME)).'/');
define('CSS_PATH', 'css/');
define('JS_PATH', 'js/');
define('IMG_PATH', FOLDER_PATH.'images');
define('IMAGE_PROFILE_PATH', BASE_DIR.'images'.DS.'uploads'.DS.'movesmart'.DS.'profile'.DS);
define('HEADER_FILE_PATH', BASE_DIR.'include'.DS.'header.php');
define('FOOTER_FILE_PATH', BASE_DIR.'include'.DS.'footer.php');
define('PROFILEIMAGE_PATH', '..'.DS.'images'.DS.'uploads'.DS.'movesmart'.DS.'profile'.DS);
define('SERVICE_MENU_ICON', '../images/uploads/movesmart/appicons/'); // path for store app icons
define('SERVICE_QUESTION_ICON', '../images/uploads/movesmart/quesicon/'); // path for store app icons
define('SERVICE_MACHINE_ICON', '../images/uploads/movesmart/machines/'); // path for store app icons
define('SERVICE_COACH_PROFILE', '../images/uploads/movesmart/coach/'); // path for store coach profile
define('SERVICE_IMPORT_TRANSLATION_FILE', '../datafiles/uploads/translations/'); // path for store coach profile
define('SERVICE_EXPORT_TRANSLATION_FILE', '../datafiles/exports/translations/'); // path for store coach profile
define('BANNER_IMAGE_PATH', '../images/uploads/movesmart/bannerimage/'); // path for store page content banner image


$moduleName = '';
$moduleArray = array('coach', 'cron', 'ajax', 'ios', 'admin', 'import', 'member');
foreach ($moduleArray as $key => $value) {
    if (strrpos($_SERVER['SCRIPT_NAME'], $value) > 0) {
        $moduleName = $value;
        break;
    }
}
if ($moduleName) {
    define('MODULE_NAME', $moduleName);
} else {
    define('MODULE_NAME', '');
}

//Pagination
define('PAGINATION_LIST', '10,20,40,60,100');
define('PAGINATION_SHOW_PER_PAGE', 10);
define('PAGINATION_NAVIGATION_VALUE', 10);

/* This variable used in get member list for training service,
    Slot wise member display start and end time flexible duration*/
$arrTime = array('1' => array('start_time' => '-20', 'end_time' => '40'),
           '2' => array('start_time' => '-20', 'end_time' => '40'),
           '3' => array('start_time' => '-20', 'end_time' => '40'), );
define('SLOT_TIME_DURATIONS', json_encode($arrTime));

//Strength program type
$strengthProgramType = array('1' => 'Rep', '2' => 'Circuit', '3' => 'Point Circuit');
define('STRENGTH_PROGRAM_TYPE', json_encode($strengthProgramType));

$strengthMachineDetails = array('213' => '60', '212' => '50'); // SK declared statically
define('STRENGTH_MACHINE_TYPE', json_encode($strengthMachineDetails));

//Sport specific training program array
$sportSpecificArr = array(
    1 => 'Recuperation', 2 => 'Fatburning', 3 => 'Endurance', 4 => 'Resistance', 5 => 'resistance and tempo training'
);
define('SPORT_SPECIFIC_TRAINING_PROGRAM_ARR', json_encode($sportSpecificArr));

$bodyCopositionMethodOpt = array(
    '1' => 'Futrex', '2' => 'Futrex (incl water)', '3' => 'Calliper', '4' => 'Omron', '5' => 'Tanita', '6' => 'Other'
);
define('BODY_COMPOSITION_OPT', json_encode($bodyCopositionMethodOpt));

// Type of Test Method
define('TEST_BODY_COMPOSITION', 7);
define('TEST_FLEXIBILITY', 8);

//Test Status
define('TEST_STATUS_YETTO_START', 0);
define('TEST_STATUS_STARTED', 1);
define('TEST_STATUS_COOLDOWN', 2);
define('TEST_STATUS_TODO', TEST_STATUS_YETTO_START);
define('TEST_STATUS_CARDIO_COMPLETE', 3);
define('TEST_STATUS_COMPLETE', 5);
define('TEST_STATUS_CANCELLED', 304);
define('TEST_STATUS_RESET_FROM_COACH', 50);
//User Status
define('USER_STATUS_ACTIVE', 1);
define('USER_STATUS_InACTIVE', 1);//MK 22/04/2016 USER_STATUS_InACTIVE changed to 2 FROM 1
define('USER_STATUS_TEST_TODO', 5);
define('USER_STATUS_TEST_ANALYSIS', 6);
/*SN Added - removes user iactive status*/
define('USER_REMOVED_STATUS_ID', 2);
//Coach and Member Image Settings *XX
define('STORAGEPATHCOACH', '..'.DS.'images'.DS.'user'.DS.'coach'.DS);
define('STORAGECOACHPATHFRMUSR', 'user'.DS.'coach'.DS);
define('STORAGEPATHMEMBER', '..'.DS.'images'.DS.'user'.DS.'member'.DS);
define('THUMB_WIDTH', '200');
define('THUMB_HEIGHT', '200');
define('LOGO_WIDTH', '35');
define('DEFAULTPROFILEIMAGE', 'user-up-photo.jpg');

define('GROUP_MOVESMART', 1);
define('GROUP_EATFRESH', 2);
define('GROUP_MINDSWITCH', 3);

define('GROUP_DUMMY_General', 999999);
define('PHASE_Check', 1);
define('PHASE_phase1', 2);
define('PHASE_phase2', 3);
define('PHASE_phase3', 4);
define('PHASE_phase4', 5);

define('QUESTIOARIES_TYPE_question', 1);
define('QUESTIOARIES_TYPE_activity', 2);
define('REGISTER_CLIENT_COMPANY', 2);

/*Redeem*/
define('REDEEM_STATUS_unknown', -1);
define('REDEEM_STATUS_inprocess', 0);
define('REDEEM_STATUS_redeemable', 1);
define('REDEEM_STATUS_redeemed', 2);

define('CCA_UPDATE_TIME_INTERVAL',    5);

define('QUESTION_OPEN_DAYS', 14);

define('RESET_USER_test', 6);
define('RESET_USER_checkfase', 5);
define('RESET_USER_MVS_fase1', 22);
define('RESET_USER_EF_fase1', 25);
define('RESET_USER_MS_fase1', 24);
define('RESET_USER_Training', 23);
//define('RESET_USER_Analysed',51);
define('RESET_USER_Analysed', 3);
define(
    'RESET_USERS', RESET_USER_test.',
    '.RESET_USER_checkfase.',
    '.RESET_USER_MVS_fase1.',
    '.RESET_USER_EF_fase1.','.RESET_USER_MS_fase1.','.RESET_USER_Analysed
);

/*MK Added - APP Static Questions - Starts*/
define('QUESTION_STATIC_preference', 41);//Static Question ID:
define('QUESTION_STATIC_take_test', 42);//Static Question ID;
define('QUESTION_STATIC_target', 9999999);//Static Question ID
/*MK Added - APP Static Questions - Ends*/
/*SN Added-Activity Question Topic ID*/

define('ACTIVITY_QUES_TOPIC_ID', 2);

/*MK ADDED QUESTION TYPE*/
define('QUESTION_TYPE_NUMBER', 1);
define('QUESTION_TYPE_OPTION', 2);
define('QUESTION_TYPE_YN', 3);
define('QUESTION_TYPE_SMILEY', 4);
define('QUESTION_TYPE_CHECKBOX', 5);
define('QUESTION_TYPE_LIKDISLIK', 6);

/*Success , Quick win , Goal Text Types*/
define('TEXTTYPE_SUCCESS', 1);
define('TEXTTYPE_QUICKWIN', 2);
define('TEXTTYPE_GROAL', 3);
define('TEXTTYPE_ADVICE', 4);
/*
--define('WEBSERVICE_LOG_PATH',BASE_DIR.'log'.DS);	

--define('WRITE_SERVICE_LOG', false);

define('HEADER_FILE_PATH',BASE_DIR.'include'.DS.'movesmart'.DS.'header.php');

define('FOOTER_FILE_PATH',BASE_DIR.'include'.DS.'movesmart'.DS.'footer.php');


--define('IMG_PATH',FOLDER_PATH.'images'); 

define('USER_PHOTOS','/images/'.THEME_NAME.'/user/'); 

--define('MODULE_VERSION','Version 1.1.2818');

--define('MODULE_VERSION_DATE', '15-07-2015');	

define('CURRENT_PARAMETER',$queryString); 

define('TRAINING_NO_OF_WEEKS', 12);

define('TEST_MACHINE', 1);

define('KEEP_FIT', 1);

define('TRAINING_TYPE', 1);

define('PROFILEIMAGE_PATH','..'.DS.'images'.DS.THEME_NAME.DS.'user'.DS);

define('GET_HEART_RATE_INTERVAL', 5);

/* Coach and Member Image Settings *XX
define('STORAGEPATHCOACH','..'.DS.'images'.DS.THEME_NAME.DS.'user'.DS.'coach'.DS);
define('STORAGECOACHPATHFRMUSR',"user".DS."coach".DS);
define('STORAGEPATHMEMBER','..'.DS.'images'.DS.THEME_NAME.DS.'user'.DS.'member'.DS);
define('THUMB_WIDTH','200');
define('THUMB_HEIGHT','200');
define('LOGO_WIDTH','35');
define('DEFAULTPROFILEIMAGE','user-up-photo.jpg');	 

//Pagination
define('PAGINATION_LIST','10,20,40,60,100');	
define('PAGINATION_SHOW_PER_PAGE', 10);	
define('PAGINATION_NAVIGATION_VALUE', 10);	

// Settings for Importing the Bookings for the Fitclass. 
define('ABS/CORE' ,'1');
define('BELLEN','2');
define('BODYSCULPTING','3');
define('CARDIO','4');
define('CIRCUIT','5');
define('COACHINGMOMENT','6');
define('CROSS','7');
define('FIETSTOCHT','8');
define('FITADVIES','9');
define('FITADVIES SD WORX','10');
define('GO','11');
define('INTRODUCTIELES','12');
define('LEAD','13');
define('LEADBOXEN','14');
define('LEDENADMINISTRATIE','15');
define('LEDENOPVOLGING','16');
define('MIX','17');
define('MOVE','18');
define('NEW','19');
define('ONDERHOUD','20');
define('ONDERHOUD TOESTELLEN','21');
define('OPEN','22');
define('OUTDOOR ACTIVITEIT','23');
define('OUTREACH','24');
define('RECEPTIE','25');
define('SPINNING','26');
define('SPORTCLUB PRESENTATIE','27');
define('STUDENT TEST','28');
define('TEAMVERGADERING','29');
define('VERKOOPSGESPREK','30');
define('VOEDINGSADVIES','31');
define('WANDELING','32');
define('YOGA ZAAL OP','33');

//graph first 150 seconds warm up period
//define('WARMUP_PERIOD',36);// 12*3
define('WARMUP_PERIOD',150);
define('COOLDOWN_AUTOSTOP',160);

//Default max value if cool down start time not found
//Default test taking maximum time in seconds
// 2100 = 35 minutes
define('START_PERIOD', 2100);


*/;
