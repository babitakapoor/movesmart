<?php
     /**
      * Author        : Jeyaram A
      * Since         : 16-Oct-2014
      * Modified By   : 
      * Modified Date : 
      * Description   : Edit Work Load popup page.
      **/
    require '../lang/en.php';
    //To get menus list
    global $LANG;
    if(!isset($param)){
        $param  =array();
    }
$param['workLoadId'] = $editId;
    $trainingWorkLoadArr = $this->test->getTrainingWorkLoad($param);
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <div class="acc-content">
        <div class="row-sec mb15">
            <div class="col5">
                <label class="fl">
                    <?php echo $LANG['originalWorkload']; ?>:<span class="required">*</span>
                </label>
                <input type="text" id="oriWorkLoad" name="oriWorkLoad"
                       class="form-control" placeholder="Original Workload"
                       value="<?php echo $trainingWorkLoadArr['original_workload']; ?>"  />
            </div>
        </div>
        <div class="row-sec mb15">
            <div class="col5">
                <label class="fl">
                    <?php echo $LANG['actualWorkload']; ?>:<span class="required">*</span>
                </label>
                <input type="text" id="actWorkLoad" name="actWorkLoad"
                       class="form-control" placeholder="Actual Workload"
                value="<?php echo $trainingWorkLoadArr['actual_workload']; ?>"  />
            </div>
        </div>
        <div class="clear"></div>
        <div class="row-sec btn-sec">
            <input type="hidden" name="trainingId" id="trainingId"
                   value="<?php echo $trainingWorkLoadArr['training_workload_id']; ?>">
            <input type="button" class="pop_cancel_btn btn black-btn fr" value="Cancel">
            <input type="button" onclick="trainingWorkLoadPopup();"class="btn black-btn fr" value="Save">
        </div>
    </div>
</div>
<!-- Add/Edit menu popup -->