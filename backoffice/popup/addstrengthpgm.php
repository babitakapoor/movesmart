<?php  
 require '../lang/en.php';
global $LANG;
if(!isset($param)){
    $param  =array();
}
$param['programId'] = '';  
if (isset($editId)) {
	
    $param['programId'] = $editId;
    $getProgramDetail = $this->settings->getprogramDetailbyId($param);
$program = $getProgramDetail['program'];
//$program = $getProgramDetail['week'];

}
$param['companyId'] = COMPANY_ID;
$arrayListMember = $this->club->getClubListByCompany($param);
 
if (!isset($arrayListMember['rows'][0])) {
    $clublist =  array($arrayListMember['rows']);
} else { 
    $clublist =  $arrayListMember['rows'];
}
$params['clubid'] = 1;
?>
<div class="popup-holder small-popup pop-strength" style="display: block;">
    <div class="close_link_container">
        <a href="javascript:void('0')" class="icon icon-popupcls pop_cancel_btn">close</a>
    </div>
    <div class="popup-header">
        <h2 class="quick_pop_title">Add Program</h2>
    </div>
	<form id="program" method="post">
    <div class="pop-content quick_pop_content">
			<select title="Select Strength Program Type" name="program_type" id="type">
                <option value="">Select Type</option>
				<?php    ?>
                <option value="1" <?php if($program['type']==1){ ?> selected="selected" <?php }?>>Free</option>
                <option value="2" <?php if($program['type']==2){ ?> selected="selected" <?php }?>>Mix</option>
                <option value="3"<?php if($program['type']==3){ ?> selected="selected" <?php }?> >Circuit</option>
				
			</select>
		<!--select title="Select club" name="club">
                <option value="">--Select club--</option-->
				<?php // foreach($clublist as $club){
					//if($program['clubid']==$club['club_id']){
					?>
				<!--option value="<?php //echo $club['club_id']; ?>" selected="selected">
				<?php // echo $club['club_name']; ?></option>
				<?php// }
				//else{ ?>
					<option value="<?php //echo $club['club_id']; ?>">
				<?php //echo $club['club_name']; ?></option>
			<?php 	//}
			//	}				?>
				</select-->
            <div class="backgro">
				<input type="hidden" name="strength_program_id" value="<?php echo $editId; ?>">
                <input type="text" class="width80" name="title_english" title="Strength Program" placeholder="Title in English" id="title_english" value="<?php if(isset($program['title_english'])) { echo $program['title_english']; } else { echo ''; } ?>"/>
                <input type="text" class="width80" name="title_dutch" title="Strength Program Search" placeholder="Title in Dutch" id="title_dutch" value="<?php if(isset($program['title_dutch'])) { echo $program['title_dutch']; } else { echo ''; } ?>"/>
                <input type="hidden" name="user_id" value="<?php echo $_SESSION['user']['user_id']; ?>"/>
				 <input type="hidden" name="company_id" value="<?php echo COMPANY_ID; ?>"/>
				<div class="cus-check fr">
                    <input type="radio" class="oxy" value="1"<?php if(isset($program['f_isdefault'])) { ?> checked="checked" <?php  } ?>" name="status_id" id="act" />
                    <label for="act">Default</label>
                </div>
                <div class="qtn_inr_bx grid-block week ">
                    <table>
                        <tr>
                            <th>Week<th>
                            <th colspan="5" >Parameters</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Series</td>
                            <td>Reps</td>
                            <td>Time(sec)</td>
                            <td>strenght(%)</td>
                            <td>Pause(sec)</td>
                        </tr>
						<?php for($i=1;$i<=12;$i++)
						{ 
					?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><input type="text" name="series<?php echo $i; ?>" id="series<?php echo $i; ?>" value="<?php if(!empty($getProgramDetail['week'.$i]['series'])) { echo $getProgramDetail['week'.$i]['series']; } else { echo ''; } ?>"></td>
                            <td><input type="text" name="Reps<?php echo $i; ?>" id="Reps<?php echo $i; ?>" value="<?php if(!empty($getProgramDetail['week'.$i]['reps'])) { echo $getProgramDetail['week'.$i]['reps']; } else { echo ''; } ?>"></td>
                            <td><input type="text" name="Time<?php echo $i; ?>" id="Time<?php echo $i; ?>" value="<?php if(!empty($getProgramDetail['week'.$i]['time_sec'])) { echo $getProgramDetail['week'.$i]['time_sec']; } else { echo ''; } ?>"></td>
                            <td><input type="text" name="strenght<?php echo $i; ?>" id="strenght<?php echo $i; ?>" value="<?php if(!empty($getProgramDetail['week'.$i]['strength'])) { echo $getProgramDetail['week'.$i]['strength']; } else { echo ''; } ?>"></td>
							 <td><input type="text" name="Pause<?php echo $i; ?>" id="Pause<?php echo $i; ?>" value="<?php if(!empty($getProgramDetail['week'.$i]['pause'])) { echo $getProgramDetail['week'.$i]['pause']; } else { echo ''; } ?>"></td>
                        </tr>
						<?php } ?>
                       

                    </table>
                </div>
                <div class="row-sec btn-sec">
				
                    <input type="button" class="pop_cancel_btn btn black-btn fr" value="Cancel">
                    <input type="button" class="btn black-btn fr" value="Save" onclick="saveProgram()">
                </div>
			
            </div>
        </div>
		</form>
		
</div>
<script type="text/javascript">
function saveProgram(){
var type = $('#type').val();
var title_en  = $('#title_english').val();
var title_du = $('#title_dutch').val();
for(i=1;i<=12;i++){
	
	var series = $('#series'+i).val();
	var Reps = $('#Reps'+i).val();
	var Time = $('#Time'+i).val();
	var strenght = $('#strenght'+i).val();
	var Pause = $('#Pause'+i).val();
	if(isNaN(series) || isNaN(Reps) || isNaN(Time) || isNaN(strenght) || isNaN(Pause))
	{
		alert("Please fill numeric value");
		return false;
	}
else if(series=='' || Reps=='' || Time=='' || strenght=='' || Pause==''){
	alert("Please fill all inputs");
	return false;
}	
}

if(type==''){
	alert("Please choose program type");
	return false;
}
else if(title_en==''){
	alert("Please fill program title");
	return false;
}
else if(title_du==''){
	alert("Please fill program title");
	return false;
}

else{
	var datastring = $("#program").serialize();
	 var url = 'action=saveStrengthProgram&'+datastring;
$.ajax({
    type: "POST",
    url: "../ajax/index.php?p=settings",
    data: url,
    dataType: "json",
    success: function(response) {
		
		if(response.status_code==1){
		ajLoaderOff();     
                flashMsgDisplay("program Added sucessfully", 'success-msg');
		}
		else if(response.status_code==2){
			ajLoaderOff();     
                flashMsgDisplay("Already Exist", 'success-msg');
		}
                    setTimeout(function() {
                        closeMsg();
						url = '../admin/index.php?p=strengthProgram';
						window.location=url;
                    }, 1500);
    },
    error: function(res) {
       // alert(res);
    }
});
}
}
</script>
