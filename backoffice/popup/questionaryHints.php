<?php	
/**
 * PHP version 5.
 
 * @category Popup
 
 * @package QuestionHints
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Popup screen to show the question hints.
 */
require"../lang/en.php";
global $LANG;
$param['menuPageId'] = '';
if(isset($editId)) {
    $param['menuPageId'] = $editId;
    $getMenuPageDetail = $this->settings->getMenuPageDetail($param);
    $row = $getMenuPageDetail['getMenuPageDetail'];
}
//To get language list
///*PK Add 2016/02/04*/
$phaselist = $this->settings->getPhaseType();
$phase =    isset($phaselist['getPhaseType']) ? $phaselist['getPhaseType']:array();
if($phaselist['total_records']==1) {
    $phase    =    array($phase);
}
//To get translationkey list
/*PK Add 2016/02/04*/
$grouplist = $this->settings->getGroupListType();
$group =    isset($grouplist['getGroupListType']) ? $grouplist['getGroupListType']:array();
if($grouplist['total_records']==1) {
    $group    =    array($group);
}
$languages = $this->settings->getLanguageActiveType();
$languageactivelist = isset($languages['languageTypeDetails']) ? $languages['languageTypeDetails']:array();
if($languages['total_records']==1) {
    $languageactivelist    =    array($languageactivelist);
}
$questionlist = $this->settings->getQuestionsType();
$question =    isset($questionlist['getQuestionsType']) ? $questionlist['getQuestionsType']:array();
if($questionlist['total_records']==1) {
    $question    =    array($question);
}
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <form method="post" action = "<?php echo $_PHP_SELF ?>" >
        <div class="acc-content">
            <div class="row-sec mb15">
                <div class="col5">
                    <label class="fl" for="phasetype">Phase :<span class="required">*</span></label>
                    <select id="phasetype" name="phasetype" >
                        <option value="">--<?php echo $LANG['select']; ?>--</option>
                        <?php
                        foreach($phase as $row){ ?>
                            <option value="<?php echo $row['phase_id'] ?>" >
                                <?php echo ucfirst(strtolower($row['phase_name'])) ?>
                            </option>
                        <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl" for="grouplist"> Group List:<span class="required">*</span></label>
                        <select id="grouplist" name="grouplist" >
                            <option value="">--<?php echo $LANG['select']; ?>--</option>
                            <?php
                            foreach($group as $row){ ?>
                                <option value="<?php echo $row['group_id']?>">
                                    <?php echo ucfirst(strtolower($row['group_name']));?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl" for="questionlist"> Question List:<span class="required">*</span></label>
                        <select id="questionlist" name="questionlist" >
                            <option value="">--<?php echo $LANG['select']; ?>--</option>
                        </select>
                    </div>
                </div>
                <div class="row-sec mb15">
                    <div class="col5">
                        <label class="fl" for="noofdays"> Days:<span class="required">*</span></label>
                        <select id="noofdays" name="noofdays" >
                            <option value="">--<?php echo $LANG['select']; ?> Days--</option>
        <?php
        for($i=1;$i<=QUESTION_OPEN_DAYS;$i++){
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
                        </select>
                    </div>
                </div>
                <div class="row-sec mb15 " >
        <?php $i=0;
        foreach($languageactivelist as $lang) {
            $questxtlabel = ($i==0) ?'Questionary hints':'&nbsp;'; ?>
            <div class="row-sec mb15">
                <label class="fl"><?php echo $questxtlabel;?></label>
                <input type="text" id="queshint" value=""
                       class="queshint questhint<?php echo $lang['language_id'];?>"
                       placeholder="Question  Hints in <?php echo $lang['title']?>"
                       languageid="<?=$lang['language_id'];?>">
                <input type="hidden" id="questxtlang"
                       value="" class="queshintlang queshintlang<?php echo $lang['language_id'];?>">
            </div>
            <?php
            $i=($i+1);
        } ?>
                </div>
        </div>
</div>
<div class="clear"></div>
<div class="row-sec btn-sec">
    <input type="hidden" name="phaseid" id="phaseid" value="">
    <input type="hidden" name="groupid" id="groupid" value="">
    <input type="hidden" name="queshintid" id="queshintid" value="">
    <input type="button" class="pop_cancel_btn btn black-btn fr"
           value="<?php echo $LANG['btnCancel'];?>">
    <input type="button" onclick="saveQuestionaryHintText()"
           class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
</div>
<!-- Add menu popup -->
