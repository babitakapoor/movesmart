<?php
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   :
     * Modified Date :
     * Description   : Add/Edit menu popup page.
     **/
    require '../lang/en.php';  
global $LANG;
if(!isset($param)){
    $param  =array();
}
$param['brandId'] = '';
if (isset($editId)) {
    $param['brandId'] = $editId;
    $getBrandDetail = $this->settings->getBrandDetail($param);
    $row = $getBrandDetail['getBrandDetail'];
}
//To get menus list
$brands = $this->settings->getBrand();
?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <div class="acc-content">
        <div class="row-sec mb15">
            <label class="fl" for="brandName">
                <?php echo $LANG['brandName']; ?> :<span class="required">*</span>
            </label>
            <input type="text" id="brandName" name="brandName" class="form-control"
                   placeholder="Brand Name"
                   value="<?php echo (isset($row['brand_name'])) ? $row['brand_name'] : ''; ?>"  />
        </div>
    </div>
    <div class="acc-content">
        <div class="col5">
            <label class="fl">
                <?php echo $LANG['brandDescription']; ?> :<span class="required">*</span>
            </label>
            <input type="text" id="brandDescription"
                   name="brandDescription" class="form-control" placeholder="Brand Description"
                   value="<?php echo (isset($row['brand_description'])) ? $row['brand_description'] : ''; ?>"  />
        </div>
    </div>
    <div class="clear"></div>
    <div class="row-sec btn-sec">
        <input type="hidden" name="brandId"
               id="brandId" value="<?php echo $param['brandId']; ?>">
        <input type="button" class="pop_cancel_btn btn black-btn fr"
               value="<?php echo $LANG['btnCancel'];?>">
        <input type="button" onclick="saveManageBrand('<?php echo $param['brandId']; ?>');"
               class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
    </div>
</div>
