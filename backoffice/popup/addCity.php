<?php
/**
 * PHP version 5.

 * @category Popup

 * @package Popup

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @link http://movesmart.company/admin/

 * @description Popup screen to show the city.
 */
global $LANG;
?>
<div class="col10">
    <div class="row-sec pad5">
        <div class="ajaxMsg"></div>
    </div>
    <div class="row-sec pad5">
        <div class="col5">
            <label class="fl" for="zipCode">
                <?php echo $LANG['zipCode']; ?> :<span class="required">*</span>
            </label>
            <input type="text" class="form-control" name="zipCode" id="zipCode"  />
        </div>
    </div>
    <div class="row-sec pad10">
        <div class="col5">
            <label class="fl" for="cityName">
                <?php echo $LANG['cityName']; ?> :<span class="required">*</span>
            </label>
            <input type="text" class="form-control" name="city" id="cityName" />
        </div>
    </div>
    <div class="row-sec btn-sec">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['cancel']; ?>" />
        <input type="button" onclick="quickAddCity();" class="btn black-btn fr" value="<?php echo $LANG['save']; ?>" />
    </div>
</div>