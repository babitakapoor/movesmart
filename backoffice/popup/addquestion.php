<?php
include("../lang/en.php");
global $LANG;
    $languages = $this->settings->getLanguageActiveType();
    $languageactivelist = isset($languages['languageTypeDetails']) ? $languages['languageTypeDetails']:array();
    if($languages['total_records']==1){
        $languageactivelist =   array($languageactivelist);
    }
?>

<!-- 20160428 -->
<style>
    .small-popup{
        max-height: 90% !important;
        width: 80% !important;
        max-width: 80% !important;
    }
    .pop-content.quick_pop_content{
        max-height: 85% !important;
    }
    textarea.quesinfo, input#questtxt{
        width: 95% !important;
        max-width: 95% !important;
    }
    .edtnumoptpoint{
        margin-top: 7px;
    }
    .mb15 {
        margin-bottom: 7px;
    }
</style>
<script>
//$(document).ready(function(){
    $(".clshtmlcontentbox").each(function(idx, obj){
        var attrid=$(obj).attr("id");
        var ins=CKEDITOR.instances[attrid];
        if(ins){
            CKEDITOR.remove(ins);
        }
        CKEDITOR.replace( attrid );
    });
//});

 </script>
<div class="row-sec mb15" id="questopicbox">
    <div class="row-sec mb16 qst_field_bx">
        <label class="fl" for="questopic">
            Topic<span class="required">*</span>
        </label>
        <select id="questopic" class="slecopt iseditdisable req-nonzero">
            <option value="0">Select Topic</option>
        </select>
    </div>
    <div class="row-sec mb12 qst_field_bx">
        <label class="fl" for="quesscore">Score<span class="required">*</span></label>
        <input type="text" id="quesscore" value="0"/>
    </div>
    <div class="row-sec mb12 qst_field_bx">
        <label class="fl" for="ordercls">Position</label>
        <input type="text" id="ordercls" />
        <input type="hidden" id="orderstatic" />
    </div>
    <div class="clear"></div>
</div>
<!--<div class="row-sec mb15">
    <div class="row-sec mb15 editques editques_0"></div>
</div>
<div class="row-sec mb15 activityques" style="display:none">
    <div class="col5">
        <label class="fl">Activity Question<span class="required">*</span></label>
        <select name='activity_name' id='activity_name' class='activity_name'>
            <option value="0">Select</option>
            <?php
            /*foreach($quesactivitylist as $activiy){ ?>
                <option value="<?php echo $activiy['activity_id']?>">
                    <?php echo $activiy['activity_name']?>
                </option>
        <?php }*/ ?>
        </select>
    </div>
</div>-->
<form class="questiontext" id="addques">
    <div class="row-sec mb15 " >
        <!--<div class="row-sec mb15">
            <label class="fl">Question Text<span class="required">*</span></label>
            <input type="text" id="questtxt" value=""
                class="questxt" placeholder="Question in English" languageid="9">
            <span class="addquescnt" onclick="addQuesContent()">+</span>
        </div>
        <div class="row-sec mb15">
            <label class="fl">&nbsp;</label>
            <input type="text" id="questtxt" value=""
                class="questxt" placeholder="Question in russian" languageid="10">
        </div>
        <div class="row-sec mb15">
            <label class="fl">&nbsp;</label>
            <input type="text" id="questtxt" value=""
                class="questxt" placeholder="Question in dutch" languageid="11">
        </div>-->
        <?php $i=0;
        foreach($languageactivelist as $lang){
         // Ed 20160428
            $questxtlabel = ($i==0) ? '<label class="fl">Question Text</label>': ""; //'Question Text':'&nbsp;';
        ?>
            <div class="clear" ></div>
            <div class="row-sec mb15">
            <?php
                echo $questxtlabel;
            ?>
                <input type="text" id="questtxt" value=""
                       class="questxt questxt_<?=$lang['language_id'];?>"
                       placeholder="Question in <?=$lang['title']?>"
                       languageid="<?php echo $lang['language_id'];?>">
                <input type="hidden" id="queslangid" value=""
                       class="queslangid queslangid_<?php echo $lang['language_id'];?> iseditdisable">
            </div>
        <?php  $i=$i+1;
        } ?>
    </div>
    <div class="row-sec mb15">
        <label class="fl" for="questype">
            Question Type<span class="required">*</span>
        </label>
        <select name='questype' id='questype' class='questype iseditdisable' oldvalue="">
            <option value="0">Select</option>
            <option value="1">Number</option>
            <option value="2">Option</option>
            <option value="3">Yes or No</option>
            <option value="4">Smilies</option>
            <option value="5">Checkbox</option>
            <option value="6">Like-Dislike</option>
            <!--SN Commanded 2016028-Slider option remove
            <option value="3">Slider</option>-->
        </select>
    </div>
    <div class="row-sec mb15 ansoptions1" style="display:none">
        <label class="fl">Answer Options<span class="required">*</span></label>
        <input type="text" id="startval" class="three_fields quesoptnum iseditdisable"
               value="" placeholder="Start" options="start">
        <input type="text" id="endval" class="three_fields quesoptnum iseditdisable"
               value="" placeholder="End" options="end">
        <input type="text" id="stepval" class="three_fields quesoptnum iseditdisable"
               value="" placeholder="Step" options="step">
        <input type="hidden" id="optpropid" class="three_fields optpropid iseditdisable" value="">
        <div class="clear"></div>
        <!--<div class="row-sec mb15">-->
        <div class="edtnumoptpoint">
            <div class="row-sec mb15 numoptrangepts numoptrangepts_0" id="numoptrangepts">
                <label class="fl">&nbsp;</label>
                <input type="text" class="three_fields numoptmin_0 iseditdisable" value="" placeholder="Min">
                <input type="text" class="three_fields numoptmax_0 iseditdisable" value="" placeholder="Max">
                <input type="text" class="three_fields numoptrange_0 iseditdisable"
                       value="" placeholder="Range Points">
                <input type="hidden" class="three_fields numoptid_0 iseditdisable" value="">
                <span class="addquescnt " onclick="addRangeNum($(this))">+</span>
                <div class="clear"></div>
            </div>

        </div>
        <!--</div>-->
    </div>
    <div class="row-sec mb15 ansoptions2" style="display:none">
        <div class="edtradioopt">
            <label class="fl">Answer Options<span class="required">*</span></label>
            <div class="row-sec mb15 radioopt radioopt_0" id="radioopt">
                <?php $i =0;
                foreach($languageactivelist as $lang){ ?>
                    <input type="text" id="radiobtntxt"
                           class="three_fields radiobtntxt quesoptradio_0 optradio_<?=$i;?> iseditdisable"
                           value="" placeholder="Text In <?=$lang['title'];?>"
                           languageid="<?php echo $lang['language_id']?>">
                <?php
                $i=$i+1;
                }?>
                <input type="text" id="radiopoint_0"
                       class="three_fields iseditdisable" value="" placeholder="Points">
                <span class="addquescnt " onclick="addRadioTxt($(this))">+</span>
            </div>
        </div>
    </div>
    <div class="row-sec mb15 ansoptions3" style="display:none">
        <div class="edtyesnoopt">
            <label class="fl">Answer Options<span class="required">*</span></label>
            <div class="row-sec mb15" id="yesnoopt">
                <input type="text" id="yesnoopt_0" class="three_fields yesnoopt iseditdisable"
                       value="" placeholder=" Yes Point" yesnooptid="" >
                <input type="text" id="yesnoopt_1" class="three_fields yesnoopt iseditdisable"
                       value="" placeholder=" No Point" yesnooptid="">
                <div class="clear"></div>
            </div>
            <!--<div class="row-sec mb15 yesnoopt" id="yesnoopt">

            </div>--->
        </div>
    </div>
    <div class="row-sec mb15 ansoptions4" style="display:none">
        <div class="edtyesnoopt">
            <label class="fl">Answer Options<span class="required">*</span></label>
            <div class="row-sec mb15" id="smilyopt">
                <label class="smilylbl"><img src="../images/smile4.png" alt="smile_sad"/>:</label>
                <input type="text" id="smilyopt_0" class="three_fields smilyopt iseditdisable"
                       value="" placeholder="Point" smilyoptid="">
                <label class="smilylbl"><img src="../images/smile3.png" alt="smile_sad"/></label>
                <input type="text" id="smilyopt_1" class="three_fields smilyopt iseditdisable"
                       value="" placeholder="Point" smilyoptid="">
                <label class="smilylbl"><img src="../images/smile2.png" alt="smile_sad"/></label>
                <input type="text" id="smilyopt_2"
                       class="three_fields smilyopt iseditdisable" value="" placeholder="Point" smilyoptid="">
                <label class="smilylbl"><img src="../images/smile1.png" alt="smile_sad"/></label>
                <input type="text" id="smilyopt_3"
                       class="three_fields smilyopt iseditdisable" value="" placeholder="Point" smilyoptid="">
            </div>
            <!--<div class="row-sec mb15 yesnoopt" id="yesnoopt">

            </div>--->
        </div>
    </div>
    <div class="row-sec mb15 ansoptions5" style="display:none">
        <div class="edtcheckopt">
            <label class="fl">Answer Options<span class="required">*</span></label>
            <div class="row-sec mb15 checkopt checkopt_0" id="checkopt">
                <?php $i =0;
                foreach($languageactivelist as $lang){ ?>
                    <input type="text" id="checkopttxt"
                           class="three_fields checkopttxt quesoptcheck_0 optcheck_<?php echo $i;?> iseditdisable"
                           value=""
                           placeholder="Text In <?php echo $lang['title'];?>"
                           languageid="<?php echo $lang['language_id']?>">
                <?php
                $i=$i+1;
                }?>
                <input type="text" id="checkpoint_0"
                       class="three_fields iseditdisable" value="" placeholder="Points">
                <span class="addquescnt " onclick="addCheckboxTxt($(this))">+</span>
            </div>
        </div>
    </div>
    <div class="row-sec mb15 ansoptions6" style="display:none">
        <div class="edtyesnoopt">
            <label class="fl" for="likdislikoopt_0">Answer Options<span class="required">*</span></label>
            <div class="row-sec mb15" id="likdislikopt">
                <input type="text" id="likdislikoopt_0" class="three_fields likdislikopt iseditdisable"
                       value="" placeholder="Like Point" title="Like Point" likdislikoptid="" >
                <input type="text" id="likdislikoopt_1" class="three_fields likdislikopt iseditdisable"
                       value="" placeholder=" Dislike Point" likdislikoptid="" title="Dislike Points">
                <div class="clear"></div>
            </div>
            <!--<div class="row-sec mb15 yesnoopt" id="yesnoopt">

            </div>--->
        </div>
    </div>
    <div class="row-sec mb15">
        <?php $i=0;
        foreach($languageactivelist as $lang){
            $quesinfotxtlabel = ($i==0) ?'Question Info':'&nbsp;';
        ?>
            <label class="">Question Info(<?=$lang['title'];?>)</label>
            <div class="clear" ></div>
            <div class="row-sec mb15">
                <!--<label class="fl"></label>-->
                <textarea name="editor" id="quesinfo<?php echo $lang['language_id']?>"
                          class="clshtmlcontentbox quesinfo quesinfo_<?php echo $lang['language_id'];?>"
                          placeholder="Question Info in <?php echo $lang['title']?>"
                          languageid="<?php echo $lang['language_id']?>"></textarea>
            </div>
        <?php  $i=$i+1;
        } ?>
    </div>
    <div class="row-sec mb15">
        <label class="fl">Question image icon<span class="required">*</span></label>
    </div>
    <div class="user-photo-holder dotline-sepl">
        <div class="photo-cls" style="display:block">
            <a id="delete-member-image" class="delete-image"
               data-role-deleteimg="user-up-photo.jpg"
               data-role-defpath="" style="">
        </div>
        <div class="user-up-photo">
            <img id="imagePreview"   width="146" height="128" alt="Question icon" />
        </div>
        <div class="row-sec mb15">
            <div class="fileUpload btn black-btn">
                <span><?php echo $LANG['browse']; ?></span>
                    <input type="file" class="upload"
                           onchange="displayImage(this,'n','','questionIcons')"
                           accept="image/*" id="uploadBtn" name="menuIcons" id="menuIcons" />
            </div>
            </div>
        <span style="display:none;" class="ajax_image_loader ajax-loader"><?php echo $LANG['loading']; ?></span>
    </div>
    <input type="hidden" value="" id="quesicon">
    <div class="clear"></div>
    <div class="row-sec btn-sec">
        <input type="hidden" name="questionnaireId" id="questionnaireId" value="">
        <input type="hidden" name="formId" id="formId" value="">
        <input type="hidden" name="quesId" id="quesId" value="">
        <input type="hidden" name="phaseid" id="phaseid" value="">
        <input type="hidden" name="groupid" id="groupid" value="">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['btnCancel'];?>">
        <input type="button" onclick="saveQuesttion()"class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
    </div>
</form>