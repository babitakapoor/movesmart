<?php	
    /**
     * Author        : Nesarajan M
     * Since         : 15-Oct-2014
     * Modified By   : 
     * Modified Date : 
     * Description   : Add/Edit menu popup page.
     **/
    require '../lang/en.php';
    global $LANG;
    if(!isset($param)){
        $param  =array();
    }
    $param['menuPageId'] = '';
if (isset($editId)) {
    $param['menuPageId'] = $editId;
    $getMenuPageDetail = $this->settings->getMenuPageDetail($param);
    $row = $getMenuPageDetail['getMenuPageDetail'];
}
if (isset($editId)) {
    $param['language_id'] = $editId;
    $languageList = $this->settings->getLanguageType();
    $languageList = isset($languageList['languageTypeDetails']) ? $languageList['languageTypeDetails'] : array();
}

?>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <form id="savelanguage">
        <div class="acc-content">
            <div class="lnagedit">
                <div class="row-sec mb15">
                        <div class="col5">
                            <label class="fl">Language :<span class="required">*</span></label>
                            <input type="text" id="manageMenuLanguage"
                                   name="manageMenuLanguage" class="form-control req-nonempty"
                                   placeholder="context"
                                   value="" />
                        </div>
                </div>
                <div class="input-group">
                <label class="fl">active :<span class="required">*</span></label>
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="1" name="isParent" id="is_parent_yes">
                    <label for="is_parent_yes"><?php echo $LANG['yes'];?></label>
                  </div>
                  <div class="in-cell cus-check">
                    <input type="radio" class="oxy" value="0" name="isParent" id="is_parent_no" >
                    <label for="is_parent_no"><?php echo $LANG['no'];?> </label>
                  </div>
                </div>
                </div>
                <div class="clear"></div>
                <div class="row-sec btn-sec">
                    <input type="hidden" name="languageTypeId" id="menuPageId"
                           value="<?php echo $param['menuPageId']; ?>">
                    <input type="button" class="pop_cancel_btn btn black-btn fr"
                           value="<?php echo $LANG['btnCancel'];?>">
                    <input type="button" onclick="saveLanguage('<?php echo $param['language_id']; ?>')"
                           class="btn black-btn fr" value="<?php echo $LANG['btnSave'];?>">
                </div>
        </div>
    </form>
</div>
<!-- Add menu popup -->
