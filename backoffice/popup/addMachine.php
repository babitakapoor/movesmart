<?php	
/**
 * PHP version 5.
 
 * @category Popup
 
 * @package AddMachine
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Popup screen to show the Machine.
 */
global $LANG;
$paramTest['strength_user_test_id'] = $editId;
$getStrengthTestActivities = $this->strength->getStrengthTestActivities($paramTest);
$getStrengthTestActivities = $getStrengthTestActivities['rows'];
$machineIds =   array();
foreach ($getStrengthTestActivities as $machine) {
    $machineIds[] = $machine['r_strength_machine_id'];
}
//print_r($machineIds);
// Get the Master Strength Machine Lists
$params['is_deleted'] = 0;
$strengthMachineList = $this->admin->getStrengthMachineList($params);

$getStrengthTestDetail = $this->strength->getStrengthTestDetail($paramTest);
$getStrengthTestDetail = $getStrengthTestDetail['rows'];
$userId = $getStrengthTestDetail['r_user_id'];

$paramUser['userId'] = $userId;
//Get user basic detail
$memberBasicDetail = $this->members->getMemberBasicDetail($paramUser);
?>
<div class="acc-row add-menu">
    <div class="acc-content">
    <!-- Strength machine begins -->
    <div class="grid-block" style="width: 100%; max-height: 300px; overflow-x:auto;">
            <form method="post" id="strength_test_activities_form">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <thead>
            <tr class="grid-title">
              <td>&nbsp;</td>
              <td><?php echo $LANG['machine']; ?></td>
            </tr>
            </thead>
            <tbody>
                <?php
                    $strength = 0;
                    $machineFlag = 0;
                    // Calculate Strength begins
                    $paramsP['userId'] = $userId;
                    $getMemberFatPercentage = $this->members->getMemberFatPercentage($paramsP);

                    $bmi = 0;
                    $fatPercentageMade = 0; //If body composition test was made then value is 1 else 0
    if ($getMemberFatPercentage['status'] == 'success') {
        $bmi = $getMemberFatPercentage['rows']['value'];
        $fatPercentageMade = 1;
    }

                    //Get weight of the member from current test
                    $weight = $getStrengthTestDetail['weight'];
                    $height = $getStrengthTestDetail['height'];
                    $gender = (strtolower(trim($memberBasicDetail['gender'])) == 'male') ? 1 : 0;

                    //Get BMI value
    if ($fatPercentageMade == 0) {
        $paramsBMI = array('height' => $height, 'weight' => $weight, 'gender' => $gender);
        $bmi = $this->strength->calculateBMI($paramsBMI);
    }
                    //Get BMI value

                    $paramsLean = array('bmi' => $bmi, 'weight' => $weight);
                    $lean = $this->strength->calculateLean($paramsLean);

                    // Calculate Strength ends					
    foreach ($strengthMachineList as $key => $value) {
        if (!in_array($value['strength_machine_id'], $machineIds)) {
            $machineFlag = 1;
            $strength = 0;
            //Calculate Strength   
            $coeft = ($gender == 1) ? $value['coef_man'] : $value['coef_woman'];
            $paramsStrength = array('lean' => $lean, 'coeft' => $coeft);
            $strength = $this->strength->calculateStrength($paramsStrength);
            //Calculate Strength
        ?>
<tr>
    <td>
        <input class="strength_test_activity_option"
               name="strength_machine[<?php echo $key;?>][strength_machine_id]"
               type="checkbox" value="<?php echo $value['strength_machine_id'];?>"
               title="Strength Machine ID" />
        <input name="strength_machine[<?php echo $key;?>][strength]"
               type="hidden" value="<?php echo $strength;?>" />
    </td>
    <td><?php echo $value['machine_name'];?></td>
</tr>
            <?php
        }
    } ?>
        <?php if ($machineFlag == 0) {
    ?>
            <tr>
                <td colspan="2">No machines found</td>
            </tr>
        <?php 
}    ?>

            </tbody>
            </table>
            </form>
    </div>
    <div>&nbsp;</div>
    <div class="row-sec btn-sec">
        <input type="button" class="pop_cancel_btn btn black-btn fr" value="<?php echo $LANG['cancel'];?>">
        <!--Later need to be called onclick:strengthTestAddActivities()-->
        <input type="button" onclick=""
               class="btn black-btn fr" value="<?php echo $LANG['save'];?>">
    </div>
    </div>
</div>
<!-- Add/Edit menu popup -->
