<?php 
require"../lang/en.php";
global $LANG;
$languages = array();
$languages = $this->settings->getLanguageActiveType(); 
$languageactivelist = isset($languages['languageTypeDetails']) ? $languages['languageTypeDetails']:array();
if($languages['total_records']==1) {
    $languageactivelist    =    array($languageactivelist);
}
$pagenamelist= array();
$pagenamelist = $this->settings->getPageName();
$pagelist = isset($pagenamelist['pagecontentDetails']) ? $pagenamelist['pagecontentDetails']:array();
if($pagenamelist['total_records']==1) {
    $pagelist    =    array($pagelist);
}
?>
<script>
    $(document).ready(function(){
        $(".clshtmlcontentbox").each(function(idx, obj){
            var attrid	=	$(obj).attr("id");
            var ins	=	CKEDITOR.instances[attrid];
            if(ins){
                CKEDITOR.remove(ins);
            }
            CKEDITOR.replace( attrid );
        });
    });
</script>
<div class="acc-row add-menu">
    <div class="ajaxMsg" style="display:none;"></div>
    <form method="post" action = "<?php echo $_PHP_SELF ?>" >
        <div class="acc-content">
                    <div class="row-sec mb15">
                    <label class="fl" for="pagename">Page Name<span class="required">*</span></label>
                    <select id="pagename" name="pagename" class="pagename" >
                            <option value="">--<?php echo $LANG['select']; ?>--</option>
        <?php
        foreach($pagelist as $list){?>
                                    <option value="<?=$list['page_id']?>"><?=$list['page_name']?></option>
        <?php
        } ?>
                        </select>
                    </div>
                    <div class="row-sec mb15">
        <?php $i=0;
        foreach($languageactivelist as $lang) { { { { {
                                $pagetxtlabel = ($i==0) ?'content':'&nbsp;'; } } } }
        ?>
                        <!-- <label class="fl">< ?=$pagetxtlabel;?></label> -->
                        <label class="fl">
                            Content<span class="required">*</span>(<span><?php echo $lang['title']?></span>)
                        </label>
                        <div class="clear" ></div>
                        <div class="row-sec mb15">
                            <textarea name="editor1" id="pagecontent<?php echo $lang['language_id']?>"
                                      class="clshtmlcontentbox pagecontent
                                        pagecontent_<?php echo $lang['language_id'];?>"
                                      placeholder="Content in  <?php echo $lang['title']?>"
                                      languageid="<?php echo $lang['language_id'];?>">
                            </textarea>
                        </div>
                        <?  $i=$i+1;
                        } ?>
                   </div>
                        </div>
                <div class="clear"></div>
                <div class="row-sec btn-sec">
                    <input type="hidden" name="pagecntid" id="pagecntid" value="">
                    <input type="button" class="pop_cancel_btn btn black-btn fr"
                           value="<?php echo $LANG['btnCancel'];?>">
                    <input type="button" onclick="savePageContent()" class="btn black-btn fr"
                           value="<?php echo $LANG['btnSave'];?>">
                </div>
        </div>
<!-- Add menu popup -->
