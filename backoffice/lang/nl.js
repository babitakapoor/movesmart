var LANG = new Array();
//Manage Members/Edit Page..
LANG['searchType'] = "Please Select Search Type";
LANG['alertPrimaryClub'] = "Enter Primary Clubssss";
LANG['personNumberExist'] = "Person number already exist";
LANG['emailExist'] = "Email already exist";
LANG['saveSuccess'] = "Saved Successfully";
LANG['updateFailed'] = "Update Failed";
LANG['noDeviceSelect'] = "No device selected";
LANG['deviceAssigned'] = "Already assigned same device";
LANG['loginExist']="Login Details already exist";
LANG['passwordnotmatch']="Password not matched";
LANG['alertTestNotAnalyzed']="You have not completed or not analyzed test. Do you want to delete its record and continue?";
LANG['alertTestIntraining']="Are you sure you want stop the current program and start new test?";
LANG['alertAnalysisDone']="Test analysis completed successfully. Would you like to see the training details?";
LANG['msgEmailSuccess'] = "Email Sent Successfully";

LANG['alertOops']="OOps, An error as occurred, please try after sometime";
LANG['emailPasswordExistError']="Update Failed. Email and password combination already exist.";

//login page
LANG['enterUsernamePwd'] = "Please enter both user name and password";
LANG['enterValidUsernamePwd'] = "Please enter a valid username and password.";
LANG['notAuthorized'] = "Sorry you are not authorized to login.";
LANG['accountDeactivated'] = "Sorry your account is deactivated. Please contact your admimintrator.";
LANG['unableToLogin'] = "Sorry unable to login.";

//Profile Picture Image Upload
LANG['selectFile'] = "Please select a file";
LANG['confirmUploadImage'] = "Are you sure want to upload the file?";
LANG['uploadSuccess'] = "Uploaded Successfully";
LANG['validImageUploadFormat'] = "Please select valid file format";
LANG['imageSuccessDelete'] = "Image Successfully Deleted";

//Test screen
LANG['requireClub'] = "Required Club Name";
LANG['requireUserName'] = "Required User Name";
LANG['requireEmail'] = "Required Email";
LANG['invalidTimeFormat'] = "Invalid Time Format";
LANG['minuteValidate'] = "Minutes should be less then 59";

//Coach List page
LANG['confirmDeleteUser'] = "Are you sure you want to delete this user?";
LANG['deleteSuccess'] = "Deleted Successfully";

LANG['confirmDeleteCoach'] = "Are you sure you want to delete this coach?";
LANG['updatecoachSuccess'] = "Coach updated successfully";
LANG['deletecoachSuccess'] = "Coach Deleted Successfully";

//Plan
LANG['confirmDeletePlan'] = "Are you sure you want to delete this plan?";
LANG['updateplanSuccess'] = "Plan updated successfully";
LANG['deleteplanSuccess'] = "Plan Deleted Successfully";

//Group
LANG['confirmDeleteGroup'] = "Are you sure you want to delete this group?";
LANG['updategroupSuccess'] = "Group updated successfully";
LANG['deletegroupSuccess'] = "Group Deleted Successfully";

//Brand
LANG['confirmDeleteBrand'] = "Are you sure you want to delete this brand?";
LANG['updatebrandSuccess'] = "Brand updated successfully";
LANG['deletebrandSuccess'] = "Brand Deleted Successfully";

//Menu
LANG['confirmDeleteMenu'] = "Are you sure you want to delete this menu?";
LANG['updatemenuSuccess'] = "Menu updated successfully";
LANG['deletemenuSuccess'] = "Menu Deleted Successfully";

//Translation
LANG['confirmDeletetranslation'] = "Are you sure you want to delete this translation?";
LANG['updatetranslationSuccess'] = "Translation updated successfully";
LANG['deletetranslationSuccess'] = "Translation Deleted Successfully";

//Language
LANG['confirmDeletelanguage'] = "Are you sure you want to delete this language?";
LANG['updatelanguageSuccess'] = "Language updated successfully";
LANG['deletelanguageSuccess'] = "Language Deleted Successfully";

//Page Content
LANG['confirmDeletePagecontent'] = "Are you sure you want to delete this pagecontent?";
LANG['updatePagecontentSuccess'] = "Page Content updated successfully";
LANG['deletePagecontentSuccess'] = "Page Content Deleted Successfully";

//Club
LANG['confirmDeleteClub'] = "Are you sure you want to delete this club?";
LANG['updateClubSuccess'] = "Club updated successfully";
LANG['deleteClubSuccess'] = "Club Deleted Successfully";


//Coach Management Edit Page
LANG['enterFirstName'] = "Enter First Name";
LANG['enterLastName'] = "Enter Last Name";
LANG['enterGSM'] = "Enter GSM";
LANG['enterDoorNo'] = "Enter Door No";
LANG['enterStreet'] = "Enter Street";
LANG['enterPostCode'] = "Enter Post Code";
LANG['enterLocation'] = "Enter Location";
LANG['enterWorkStart'] = "Enter Work Starton";
LANG['enterEmail'] = "Enter Email ID";
LANG['invalidEmail'] = "Invalid Email Address";
LANG['enterPwd'] = "Enter Password";
LANG['enterConfirmPwd'] = "Enter confirm password";
LANG['mismatchPwd'] = "Password or Confirm password mismatch";
LANG['validStartonEndon'] = "Please select the correct Work Starton and Work Endon";
LANG['loginIdExist'] = "Login ID already exist";
LANG['selectClub'] = "Please Select Club";
LANG['selectRole'] = "Please Select Role";

//Sportmed graph/Fitgraph page
LANG['unableToShowGraph'] = "Insufficient data. Unable to render graph";
LANG['alertNoActivityFound'] = "No activities associated in this club to created training activities";
LANG['alertPutWaitingList'] = "Test has been sucessfully added in waiting list";

//Test Result Page
LANG['fitnessLevelNumberOnly'] = "Please Enter Fitness Level 1 to 7 Numbers only";
LANG['iantHRNumbersOnly'] = "Please Enter IANT-HR Numbers only";
LANG['iantVo2NumbersOnly'] = "Please Enter IANT-Vo2 Numbers only";
LANG['maxHRNumbersOnly'] = "Please Enter Max-HR Numbers only";
LANG['iantPNumbersOnly'] = "Please Enter IANT-P Numbers only";
LANG['iantRelativeNumbersOnly'] = "Please Enter Relative IANT-VO2 Numbers only";
LANG['maxPNumbersOnly'] = "Please Enter Max-P Numbers only";
LANG['relativePNumbersOnly'] = "Please Enter Relative IANT-P Numbers only";
LANG['relativeMaxPNumbersOnly'] = "Please Enter Relative MAX- P Numbers only";
LANG['enterAgeOnTest'] = "Enter Age On Test";
LANG['testOptions'] = "Please Select Test Options";
LANG['enterTestPreparedOn'] = "Enter Test Prepared On";
LANG['enterWeightTestDate'] = "Enter Weight On Test";
LANG['selectTestLevel'] = "Please Select Test Level";
LANG['enterFitnessLevel'] = "Enter Fitness Level";
LANG['enterTestDate'] = "Enter Test Date";
LANG['enterTestTime'] = "Enter Test Time";
LANG['enterNewTestDate'] = "Enter New Test Date";
LANG['enterWeightOnTest'] = "Please Enter Weight on Test Date";
LANG['chooseTestOption'] = "Please Choose test options";

//Club management - Admin light
LANG['enterClubName'] = "Enter Club Name"; 
LANG['enterStreetName'] = "Enter Street Name"; 
LANG['enterNumber'] = "Enter Number"; 
LANG['enterBus'] = "Enter Bus";  
LANG['enterEmailId'] = "Enter Email Id";
LANG['enterCommercial'] = "Enter Commercial Name";
LANG['selectStatus'] = "Select Status";
LANG['confirmDeleteClub'] = "Are you sure you want to delete this club?";

//Plan management - Admin light
LANG['enterPlanName'] = "Enter Plan Name";
LANG['enterIndoorSession'] = "Enter Indoor Session";
LANG['choosePaymentType'] = "Choose Payment Type";


// Manage Points
LANG['selectActivity'] = "Please Select Activity";
LANG['enterAchievedPeriod'] = "Please Enter Achieved Period";

LANG['confirmDeleteActivity'] = "Are you sure you want to delete this activity?";

LANG['saveDetails'] = "Please save the details";

//LMO page js 
LANG['lmoTestDateError'] = "Test date should not allow past date";

//Confirmations
LANG['deleteConfirmationSelected'] = "Are you sure want to delete selected?";
LANG['deleteConfirmation'] = "Are you sure want to delete this?";
LANG['primarydeleteConfirmation'] = "Are you sure you want to delete this?";
LANG['primarydelete'] = "Deleted Successfully";
LANG['changeClubConfirmation'] = "Are you sure want to delete this?";
LANG['primaryConfirmation'] = "Are you sure you want to make this primary?";
LANG['deleteImageConfirmation'] = "Are you sure you want to delete the image?";
LANG['assignConfirmation'] = "Are you sure want to assign this?";
LANG['cycleAsssignedAlready'] = "Cycle already assigned to another user, Are you sure want to reassign this?";
LANG['changeStatusConfirmation'] = "Are you sure want to change the status?";
LANG['assignDeviceConfirmation'] = "Are you sure you want to Assign Device";
LANG['unlinkDeviceConfirmation'] = "Are you sure you want to Unlink Device?";
LANG['deleteMenuConfirmation'] = "Are you sure want to delete the menu?";
LANG['deleteGroupConfirmation'] = "Are you sure want to delete the group?";
LANG['deletePlanConfirmation'] = "Are you sure want to delete the plan?";
LANG['deleteBrandConfirmation'] = "Are you sure want to delete the plan?";
LANG['deleteCoachConfirmation'] = "Are you sure want to delete the coach?";
LANG['reanalyzeTest'] = "Are you sure you want reanalyze the test results ? We remind that all information from the running training program will disappear in this case !";
LANG['cancelTestConfirmation'] = "Are you sure you want cancel the test?";
LANG['resetTestConfirmation'] = "Are you sure you want Reset the Test?";
LANG['newTestConfirmation'] = "Are you want to create new test when reset this test?";
LANG['emailExistConfirmation'] = "Login combination already exist with current email. Do you want use the same email";


//Validation
LANG['sessionStartDate'] = "Choose Session Start Date";
LANG['alertSearchValue'] = "Please Enter Search Value";
LANG['alertChooseClub'] = "Choose Club";
LANG['alertSearchValue'] = "Please Enter Search Value";
LANG['alertAddress'] = "Enter Address";
LANG['alertPhone'] = "Enter Phone";
LANG['alertMobile'] = "Enter Mobile";
LANG['alertEmail'] = "Enter Email";
LANG['alertDOB'] = "Select Date of Birth";
LANG['alertGender'] = "Choose Gender";
LANG['alertAccNum'] = "Enter IBAN Account Number";
LANG['alertBicCode'] = "Choose BIC Code";
LANG['alertEquipment'] = "Please Select Equipment";
LANG['alertMaxHr'] = "Enter MAX-HR";
LANG['alertIantHr'] = "Enter IANT-HR";
LANG['alertMaxWatt'] = "Enter MAX-Watt";
LANG['alertIantWatt'] = "Enter IANT-Watt";
LANG['alertRegrCoef'] = "Enter Regr Coeff";

//Strength Program
LANG['alertDescription'] = "Enter Description";
LANG['alertStrengthProgramType'] = "Choose Program Type";
LANG['alertTrainingWeek'] = "Enter Training Week";
LANG['deleteTrainingConfirmation'] = "Are you sure want to delete?";
LANG['alertHeight'] = "Enter Height";
LANG['alertWeight'] = "Enter Weight";
LANG['alertTestOption'] = "Choose Test Option";
LANG['bodyCompositionMeasuredWarning'] = "No body composition measured. The 'Max Strength' will be calculated based on 80% of body weight";
LANG['sureLeavePage'] = "Are you sure want to leave the test";
LANG['alertCoachEmpty'] = "Choose Coach";

//Manage Members
LANG['alertOldPassword'] = "Enter Old Password";
LANG['alertNewPassword'] = "Enter New Password";
LANG['alertConfirmPassword'] = "Enter Confirm Password";
LANG['alertPasswordMatch'] = "Password do not match";

LANG['updateParameterSucess']="Update Parameter Sucessfully";

