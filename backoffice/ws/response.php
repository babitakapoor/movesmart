<?php
/**
 * PHP version 5.
 
 * @category General
 
 * @package WSResponse
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description handle webservice response details.
 */
/**
 * Include the array to xml class.
 */
require_once LIBRARY_PATH.DS.'array2xml.php';

/**
 * Response class to handle output.
 */
class response
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
    }

    /**
     * Generate JSON string based on input array
     * param array $result
     * return string.
     * @param $result
     * @return string
     */
    public function generateJSON($result)
    {
        $root_node = 'movesmart';
        $result = array($root_node => $result);
		
        return json_encode($result);
    }

    /**
     * Generate JSON string based on input array
     * param array $result
     * return string.
     * @param $result
     * @return
     */
    public function generateXML($result)
    {
        //$xml = Array2XML::createXML($root_node = 'movesmart', $result);
        $xml = Array2XML::createXML('movesmart', $result);

        return $xml->saveXML();
    }

    /**
     * Render the output based on output format
     * param array $result
     * param string $format
     * param boolean $return
     * return mixed.
     * @param $result
     * @param string $format
     * @param bool $return
     * @return string
     */
    public function render($result, $format = 'xml', $return = false)
    {
		switch ($format) {
        case 'xml':

            $xml = $this->generateXML($result);

            if ($return) {
                return $xml;
            } else {
                $this->setHeader('Content-type: application/xml; charset=ISO-8859-1');
                echo  $xml;
            }

            break;
        case 'json':

            $json = $this->generateJSON($result);
			if ($return) 
			{
				return $json;
            } else {
                $this->setHeader('Content-Type: application/json; charset=ISO-8859-1');
                echo $json;
            }

            break;

        case 'html':
                $this->setHeader('Content-Type: text/html; charset=ISO-8859-1');

            if (is_array($result)) {
                echo '<pre>';
                print_r($result);
            } else {
                echo $result;
            }
            break;

        default:

            $status = array(
                'response' => array(
                    'status' => 'error',
                    'status_code' => 600,
                    'status_message' => 'Undefined output format',
                ),
            );
            $this->render($status, 'xml');

            break;
        }
        return null;
    }

    /**
     * Set output headers based on output format
     * param string $header.
     * @param $header
     */
    public function setHeader($header)
    {
        header('Access-Control-Allow-Origin: *');
        header($header, true);
    }
}
