<?php
/**
 * PHP version 5.
 
 * @category General
 
 * @package DataConfig
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description include main lib files.
 */
// Include required general files.
require_once '..'.DS.'include'.DS.'dbconfig.php';
require_once '..'.DS.'ws'.DS.'config.php';
require_once '..'.DS.'ws'.DS.DIR_DB_CLASS.'adodb.inc.php';
require_once '..'.DS.'ws'.DS.'db.php';
require_once '..'.DS.'ws'.DS.'request.php';
require_once '..'.DS.'ws'.DS.'library'.DS.'array2xml.php';
require_once '..'.DS.'ws'.DS.'library'.DS.'ssp.class.php';
require_once '..'.DS.'ws'.DS.'response.php';
require_once '..'.DS.'ws'.DS.'rest.php';

// Include required module files.
$directory = opendir('..'.DS.'ws'.DS.'modules');
while (false !== ($file = readdir($directory))) {
    if (substr($file, -4) == '.php') {
        include_once '..'.DS.'ws'.DS.'modules'.DS.$file;
    }
}
closedir($directory);
//End

class data
{
    public $data = '';
    public $request = '';
    public $response = '';

    public function __construct()
    {
        $db_con = new Db();
        $this->dbcon = $db_con->getConnection();
        $this->response = new Response();
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string	
     */
    public function processRequest($data)
    {

        //starting adapding the class and method...
        parse_str($data, $data);
        $class = (!empty($data['mod'])) ? $data['mod'] : null;
        $method = (!empty($data['method'])) ? $data['method'] : null;
        $params = $data;
        $format = (!empty($data['format'])) ? $data['format'] : 'xml';

        $class_file = MODULE_PATH.DS.$data['mod'].'.php';

            //Check the class file
        if (file_exists($class_file)) {
            //Check the class
            $class      .= 'Model';
            if (class_exists($class)) {
                // Need to create instance for the corresponding class.	
                $service = new $class($this->dbcon);
                // check if method is exists				 
                if (method_exists($service, $method)) {
                    $result = $service->$method($params);
                } else {
                    $result = $this->getStatus('error', 404, 'Method not found');
                }//End method exists
            } else {
                $result = $this->getStatus('error', 404, 'Class not found');
            }//End class_exists
        } else {
            $result = $this->getStatus('error', 404, 'Class file not found');
        }//End file_exists

        return $this->response->render($result, $format, true);
    }

    /**
     * TODO : Need to implement in general.
     * @param $status
     * @param string $status_code
     * @param string $status_message
     * @return array object
     */
    public function getStatus($status, $status_code = '', $status_message = '')
    {
        $status = array(
            'response' => array(
                'status' => $status,
                'status_code' => $status_code,
                'status_message' => $status_message,
            ),
        );

        return $status;
    }
}
