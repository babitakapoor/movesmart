<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle message related DB access.
 */
define(
    'GET_MESSAGES_BY_USER_ID', "
        SELECT MSG.*,
        CONCAT(U.first_name,' ',U.last_name) sender_name,
        UT.usertype as sender_type, 
        DATE_FORMAT(updated_date, '%b %e') message_date, 
        DATE_FORMAT(updated_date, '%H:%i') message_time 
        FROM `t_messages` MSG
        LEFT JOIN `t_users` U ON U.user_id=MSG.sender_id
        LEFT JOIN `t_usertype` UT ON UT.usertype_id=U.r_usertype_id
        WHERE (MSG.sender_id=? OR MSG.receiver_id=?) 
            AND MSG.`is_deleted`=0 AND `message_parent_id`=0 "
);

define(
    'GET_MESSAGES_DETAIL_BY_ID', "SELECT MSG.*,
        CONCAT(U.first_name,' ',U.last_name) sender_name,UT.usertype as 	
        sender_type, DATE_FORMAT(updated_date, '%b %e') message_date,  
        DATE_FORMAT(updated_date, '%H:%i') message_time FROM `t_messages` MSG 
            LEFT JOIN `t_users` U ON U.user_id=MSG.sender_id
            LEFT JOIN `t_usertype` UT ON UT.usertype_id=U.r_usertype_id
            WHERE MSG.`message_parent_id`=? AND MSG.`is_deleted`=0 
                ORDER BY `message_id` DESC"
);

define('INERT_NEW_MESSAGE', 'SELECT * FROM `t_messages` WHERE `message_id`=? ');
define(
    'DELETE_MESSAGE_BY_ID', 'SELECT * FROM `t_messages` 
    WHERE `message_id`=? '
);
define(
    'MARK_MESSAGE_READ_BY_ID', 'SELECT * FROM `t_messages` 
    WHERE `message_id`=? '
);
/*
 * MK Moved From Members Modules
*/
define(
    'GET_MESSAGE_DETAILS', 'SELECT SQL_CALC_FOUND_ROWS 
    usr.first_name,usr.last_name,usr.email,msg.type,msg.message
    ,msg.added_date,msg.receiver_id FROM t_messages msg 
    LEFT JOIN t_users usr ON usr.user_id=msg.receiver_id WHERE msg.is_deleted != 1 
    ORDER BY updated_date DESC,added_date DESC'
);