<?php
/**
 * Password Encryption Decryption Created by ShanethaTech Development Team.
 * User: Shanetha-Tech Development Team
 * Date: 13-06-2016
 * Time: 03:43:PM
 */

/**
 * Returns an encrypted & utf8-encoded
 * @param string $encryptstr encoded
 * @return string $encryptstr - covered with char 'a'
 */
function coverencrypt($encryptstr){
    return "a".$encryptstr;
}

function uncoverencrypt($encryptstr){
    return substr($encryptstr,1);
}

function encrypt($pure_string, $encryption_key) {
    $encrypted_string = trim(
        base64_encode(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_256, $encryption_key, $pure_string, MCRYPT_MODE_ECB, mcrypt_create_iv(
                mcrypt_get_iv_size(
                    MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                ), MCRYPT_RAND
            ))
        )
    );
    return coverencrypt($encrypted_string);
}

/**
 * Returns decrypted original string
 * @param string $encrypted_string - String encrypted
 * @param string $encryption_key - String encrypted Key
 * @return string $decrypted_string
 */
function decrypt($encrypted_string, $encryption_key) {
    $encrypted_string = uncoverencrypt($encrypted_string);
    $decrypted_string = trim(
        mcrypt_decrypt(
            MCRYPT_RIJNDAEL_256, $encryption_key, base64_decode($encrypted_string),
            MCRYPT_MODE_ECB,
            mcrypt_create_iv(
                mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND
            )
        )
    );
    return $decrypted_string;
}
function encryptdefault($pure_string){
    //$keyvalu = ENCRYPTKEY;
	$keyvalu = 'shanetha12345678';
    return encrypt($pure_string, $keyvalu);
}

function decryptdefault($pure_string){
//$keyvalu = ENCRYPTKEY;
    $keyvalu = 'shanetha12345678';
    return decrypt($pure_string, $keyvalu);
}