<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Club
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle club related DB access.
 */
/*define(
    'GET_CLUB_QUERY', 'SELECT fitclass_center_id as clubid,
        fitclass_center_name as clubname FROM t_fitclass_center where status=?'
);*/

/*define(
    'GET_COACHLISTBYCENTERWISE', "SELECT e.employee_id as coachid,
        concat(e.first_name,' ',e.last_name)as coachname, 
        fc.fitclass_center_id as centerid,fc.fitclass_center_name as centername,
        e.email_id as email,e.gender,e.street,e.pincode,e.city_id as cityid,
        e.phone_number as phonenumber FROM t_application_user au 
        left outer join t_employee e 
            on e.application_user_id=au.application_user_id 
        left outer join t_fitclass_center_user fcu 
            on fcu.employee_id=e.employee_id 
        left outer join t_fitclass_center fc 
            on fc.fitclass_center_id=fcu.fitclass_center_id 
        where au.user_type=? and au.company_id=? and au.status=? 
            and e.status=? and fcu.user_type_id=? LIMIT 5"
);
*/
define(
    'GET_CLUB_DETAIL_DROPDOWN', 'SELECT * FROM t_clubs 
        WHERE is_deleted = 0'
);

define(
    'GET_CLUB_LIST_BY_COMPANY', 'SELECT SQL_CALC_FOUND_ROWS * FROM t_clubs 
        WHERE r_company_id = ? AND is_deleted = 0'
);

define(
    'GET_CLUB_DETAIL_BY_CLUB_ID', 'SELECT * FROM t_clubs 
        WHERE club_id = ?'
);
/*
define(
    'GET_MANAGE_CYCLE',
    "SELECT cl.club_id,e.equipment_id,e.equipment_name,cl.club_name,
        IF((SELECT status FROM t_equipment_available ea 
            WHERE e.equipment_id=ea.r_equipment_id AND ea.r_club_id=cl.club_id 
        LIMIT 1 )=1 ,'1','0') AS statusmsg FROM t_clubs cl JOIN t_equipment e"
);*/
/*
define(
    'GET_MANAGE_CYCLE_EDIT',
    "SELECT cl.club_id,e.equipment_id,e.equipment_name,cl.club_name,
        IF((SELECT status FROM t_equipment_available ea 
            WHERE e.equipment_id=ea.r_equipment_id AND ea.r_club_id=cl.club_id 
        LIMIT 1 )=1 ,'1','0') AS statusmsg FROM t_clubs cl JOIN t_equipment e"
);
*/
define(
    'GET_EQUIPMENT_DROPDOWN',
    'SELECT e.equipment_id,e.equipment_name FROM t_equipment e'
);

/*define(
    'GET_EQUIPMENT_AVAILABLE',
    'SELECT * FROM t_equipment_available WHERE r_club_id=? AND r_equipment_id=?'
);*/

/*define(
    'GET_LINKCYCLE_WITH_MEMBER',
    "SELECT DISTINCT u.user_id, CONCAT(u.first_name,' ',u.last_name) AS usersname,
        ut.status,ut.r_equipment_id,e.equipment_name,c.club_id,c.club_name
        FROM t_users u
        LEFT OUTER JOIN t_user_test ut
            ON ut.user_test_id = (SELECT ut1.user_test_id FROM t_user_test ut1
        WHERE u.user_id = ut1.r_user_id  ORDER BY ut1.user_test_id DESC LIMIT 1)
        LEFT OUTER JOIN t_clubs c ON c.club_id=u.r_club_id
        LEFT OUTER JOIN t_equipment e ON e.equipment_id=ut.r_equipment_id
        WHERE r_status_id=5 and u.is_deleted<>1"
);
*/
/*define(
    'GET_CYCLE_LIST_DROPDOWN', 'SELECT DISTINCT e.equipment_id,e.equipment_name
             FROM t_equipment e'
);
*/
/*
define(
    'GETCYCLEMEMBER_ADDORUPDATE', 'SELECT * 
                                    FROM t_user_test
                                    WHERE r_user_id=? AND status<>"3"'
);
*/
/*
define(
    'GETCYCLEOTHER_UPDATECYCLE_MAKEEMPTY', 'SELECT * 
        FROM t_user_test
        WHERE r_user_id!=? AND r_club_id=? AND r_equipment_id=?'
);
*/
/*
define(
    'GETCYCLECOUNT_MEMBER', 'SELECT count(user_test_id) as count 
        FROM t_user_test
        WHERE r_equipment_id=? AND r_club_id=? A
            ND r_user_id<>? AND status<>"3" 
            AND user_test_id=(SELECT max(user_test_id) 
            FROM t_user_test ut WHERE r_equipment_id=? 
            AND r_club_id=? AND r_user_id<>?)'
);
*/
define(
    'GETCYCLEAVAILABLE_FORCLUB', 'SELECT ea.r_equipment_id,
        ea.r_club_id,e.equipment_name
        FROM t_equipment_available ea
        LEFT OUTER JOIN t_equipment e 
            ON e.equipment_id=ea.r_equipment_id
        WHERE ea.r_club_id=? AND ea.status=1'
);

define(
    'GET_EMPLOYEE_CLUB_ROLES', 'SELECT * FROM `t_employee_centre` 
            WHERE `r_user_id`=? AND `r_club_id`=? 
            AND `r_user_type_id`=? AND `is_deleted`=0'
);

define(
    'UPDATE_CLUB_MAIN_CONTACT', 'UPDATE `t_employee_centre` 
                    SET `is_main_contact`=0 
                    WHERE `r_club_id`=?'
);

/*SN Added To checkclub mandatory for the Club - t_ismedicalinfo */
define(
    'GET_EMPLOYEE_COMPANY_CLUB_ROLES', 'SELECT t.`usertype`, t.`usertype_id`,
        c.`club_name`, c.`t_ismedicalinfo`,ec.`is_primary`,
        ec.* FROM `t_employee_centre` ec 
          INNER JOIN `t_clubs` c ON c.`club_id`=ec.`r_club_id`
          INNER JOIN `t_usertype` t ON t.`usertype_id`=ec.`r_user_type_id`		
          WHERE ec.`r_user_id`=? AND ec.`r_company_id`=? AND ec.`is_deleted`=0 
          ORDER BY ec.`is_primary` DESC'
);

define(
    'GET_EMPLOYEE_PRIMARY_CLUB', 'SELECT t.`usertype`, c.`club_name`,
        u.r_company_id,
        u.r_club_id,u.user_id as r_user_id,
        u.r_usertype_id as r_user_type_id, 
        1 is_primary FROM `t_users` u 
          INNER JOIN `t_clubs` c ON c.`club_id`=u.`r_club_id`
          INNER JOIN `t_usertype` t ON t.`usertype_id`=u.`r_usertype_id`		
          WHERE u.`user_id`=?'
);

/*define(
    'GET_USERS_PRIMARY_CLUB', 'SELECT t.usertype, c.club_name,r_usertype_id, 
            u.* FROM t_users u
            INNER JOIN t_clubs c ON c.club_id=u.r_club_id
            INNER JOIN t_usertype t ON t.usertype_id=u.r_usertype_id
            WHERE u.user_id=? AND u.r_company_id=? AND u.is_deleted=0'
);*/

define(
    'GET_EMPLOYEE_CENTRE', 'SELECT * FROM `t_employee_centre` 
        WHERE employee_centre_id=?'
);

/* define(
    'GET_QUERY_LAST_TOTAL_COUNT', "SELECT FOUND_ROWS() as total_count");
*/
define(
    'GET_EMPLOYEE_CENTER_CLUB', 'SELECT *,s.status status 
        FROM t_employee_centre AS ec 
            LEFT JOIN t_users AS u ON ec.r_user_id = u.user_id 
            LEFT JOIN t_personalinfo AS pi ON pi.r_user_id = u.user_id 
            LEFT JOIN t_status AS s ON s.status_id = u.r_status_id
            LEFT JOIN t_usertype AS ut ON ut.usertype_id = u.r_usertype_id
            WHERE ec.is_deleted=0 AND ec.r_club_id=?'
);

//Get the User Soft Delete
define(
    'GET_EMPLOYEE_DELETED', 'SELECT * FROM t_employee_centre 
        WHERE employee_centre_id = ?'
);

define(
    'GET_COMPANY_EMPLOYEES_BY_ID', 'SELECT `user_id`,`first_name`,`last_name`,
        `r_usertype_id` FROM `t_users` 
        WHERE `r_company_id`=? AND `r_usertype_id` IN(1,2) AND `r_status_id`=1'
);

define(
    'GET_CLUB_ID_EXIST', 'SELECT * FROM `t_clubs` WHERE club_id=?'
);

define(
    'UPDATE_CLUB_ID_SOFT_DELETE', 'UPDATE `t_clubs` SET `is_deleted`=1 
    WHERE club_id=?'
);

define(
    'GET_CLUB_IMAGES_BY_CLUB_ID', 'SELECT * FROM `t_club_images` 
        WHERE r_club_id=? AND r_company_id=? AND is_deleted=0'
);
/*define(
    'GET_CLUB_IMAGE_BY_IMAGE_ID', 'SELECT * FROM `t_club_images`
            WHERE club_image_id=? AND r_club_id=? AND r_company_id=?'
);*/