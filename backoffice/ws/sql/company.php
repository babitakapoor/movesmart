<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Company
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle company related DB access.
 */
/*define(
    'GET_COMPANY_DETAILS', 'SELECT * FROM t_company WHERE company_name=?'
);*/

define(
    'GET_COMPANY_QUERY', 'SELECT * FROM t_company where status=?'
);
define(
    'GET_COMPANY_ADDOREDITQUERY', 'SELECT * FROM t_company 
    where company_id=?'
);
