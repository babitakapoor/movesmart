<?php
/**
 * PHP version 5.

 * @category SQL

 * @package Questionaries

 * @author Shanetha Tech <info@shanethatech.com>

 * @license movesmart.company http://movesmart.company

 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle questionaries related DB access.

 */
/*Load Questionaries Values*/
define('GET_FORM_TOPICS', 'SELECT * FROM t_form_topics WHERE f_isdelete<>1');
define(
    'GET_QUESTIONTYPE_TOPIC', 'SELECT tf.r_topic_id,tf.form_id,
        tfq.id as formelemid,tfq.`type`
        FROM t_form tf 
        INNER JOIN t_form_element tfq 
            ON (tfq.form_id=tf.form_id AND tfq.is_delete<>1) 
        WHERE  tf.r_topic_id=?
            ORDER BY tf.r_topic_id'
);/*INNER used becauseNo Need to Consider the No Question Topics*/
define(
    'GET_QUESTION_OPTIONS', 'SELECT * FROM t_formelement_options 
    WHERE f_isdelete<>1'
);
define(
    'GET_TOPIC_USERCREDITS', 'SELECT 
        f.r_topic_id as topic_id, f.r_quesgroup_id as group_id,
        mvdt.user_id,mv.`type`,SUM(mvdt.value) as usercredits 
    FROM t_mv_data mvdt
        LEFT JOIN t_monitor_variable mv ON mvdt.monitor_variable_id=mv.id
        LEFT JOIN t_form_element fe 
          ON (fe.id=mvdt.r_form_element_id AND fe.is_delete<>1)
        LEFT JOIN t_form f ON (f.form_id=fe.form_id AND f.is_delete<>1)
    WHERE mvdt.user_id=? AND mv.`type`=\''.QUESTIOARIES_TYPE_question.'\'
        AND f.r_topic_id<>0
        GROUP BY f.r_topic_id,f.r_quesgroup_id;'
);
define(
    'GET_TOPIC_RANGE', 'SELECT pr.*,topic.topic_name FROM t_ques_phase_range pr 
            INNER JOIN t_form_topics topic
                ON (topic.topic_id=pr.r_topic_id AND topic.f_isdelete<>1)
            WHERE pr.f_isdelete<>1 AND pr.r_topic_id<>1'
);/*INNER JOIN TO AVOID UN MAPPED TOPICS*/
define(
    'GET_TOPIC_RANGE_LANGUAGE', 'SELECT * FROM t_ques_phase_range_language 
    WHERE f_isdelete<>1'
);
define(
    'GET_TOPIC_QUESTIONS', 'SELECT tt.topic_id,tt.group_id,tt.topic_name, 
        tf.form_id,tfq.id as formelemid,tfq.`type`
        FROM t_form_topics tt
        INNER JOIN t_form tf 
            ON (tf.r_topic_id=tt.topic_id AND tf.is_delete<>1)
        INNER JOIN t_form_element tfq 
            ON (tfq.form_id=tf.form_id AND tfq.is_delete<>1) 
        WHERE tt.f_isdelete<>1 ORDER BY tf.r_topic_id;'
);
/*INNER used becauseNo Need to Consider the No Question Topics*/
//define('GET_MAXPOPINTS_TOPIC', 'SELECT * FROM t_form_topics WHERE f_isdelete<>1');
define('GET_QUESTION_LANGUAGE', 'SELECT * FROM t_question_language');
define('GET_QUESTION_GROUP', 'SELECT * FROM t_ques_group');
define(
    'GET_QUESTION_GROUP_BYID', 'SELECT * FROM t_ques_group 
    WHERE group_id = ?'
);
define(
    'GET_QUESTION_PHASE', 'SELECT * FROM t_ques_phase phase 
        LEFT OUTER JOIN t_ques_phase_weightage weightage 
        ON weightage.r_phase_id=phase.phase_id'
);
define(
    'GET_QUESTION_WEIGHT_BYGROUPPHASEID', 'SELECT * FROM t_ques_phase_weightage 
        WHERE r_phase_id=? AND r_group_id=? AND f_isdelete != 1 '
);
define(
    'GET_QUESTION_WEIGHT', 'SELECT * FROM t_ques_phase_weightage 
        WHERE f_isdelete != 1 '
);
define(
    'GET_QUESTION_PHASE_BYID', 'SELECT * FROM t_ques_phase phase 
    WHERE phase.phase_id=?'
);
define(
    'GET_QUESTION_PHASE_RANGE', 'SELECT* FROM t_ques_phase_range 
    WHERE r_phase_id=? and r_group_id=?'
);
define(
    'GET_QUESTION_PHASE_RANGE_LANG', 'SELECT* 
    FROM t_ques_phase_range_language 
    WHERE r_ques_phase_range_id=?'
);
//define('GET_TOPICS', 'SELECT * FROM t_form');
define(
    'GET_QUESTIONARIES', 'SELECT * FROM t_form_element ques 
        LEFT OUTER JOIN t_form topic ON topic.form_id=ques.form_id
        WHERE ques.is_delete<>1'
);

define(
    'GET_USER_PHASEID_BYGROUP', 'SELECT * FROM t_ques_phase_user 
    WHERE r_user_id=? AND r_group_id=? AND completed_date IS NULL 
        ORDER BY r_phase_id ASC LIMIT 0, 1'
);
define(
    'GET_USER_PHASEID', "SELECT phase_id
        FROM t_ques_phase phase,
        (SELECT r_phase_id FROM t_ques_phase_user 
        WHERE r_user_id=? AND completed_date!='' 
            ORDER BY r_phase_id DESC,completed_date DESC LIMIT 1) laspase
        WHERE (phase.phase_id > laspase.r_phase_id) 
            ORDER BY phase.phase_id ASC LIMIT 0,1"
);
define(
    'GET_USER_PHASEID_WITHGROUP', "SELECT phase_id
        FROM t_ques_phase phase,
        (SELECT r_phase_id FROM t_ques_phase_user 
        WHERE r_user_id=? AND r_group_id=? AND completed_date!='' 
            ORDER BY r_phase_id DESC,completed_date DESC LIMIT 1) laspase
        WHERE (phase.phase_id > laspase.r_phase_id) 
    ORDER BY phase.phase_id ASC LIMIT 0,1"
);
define(
    'GET_ACTIVITIES_BY_GROUPPHASE', 'SELECT act.*,
        actphase.topic_id,actphase.id as phaseact_id,felem_mv.* 
        FROM t_ques_phase_activity actphase 
        LEFT OUTER JOIN t_ques_activity act 
            ON act.activity_id =actphase.ques_activity_id
        LEFT OUTER JOIN t_formelement_monitorvariable felem_mv 
            ON (felem_mv.formelement_id=act.activity_id 
            AND felem_mv.monitor_element_type=2)
        WHERE  actphase.phase_id=? AND actphase.group_id=? '
);
/* felem_mv.monitor_element_type denotes the Input is Activity*/
define(
    'GET_ACTIVITY_POINTS', 'SELECT * FROM t_ques_activity_points
    WHERE r_ques_phase_activity_id=? '
);
define(
    'GET_ACTIVITY_USERANSWERS', "SELECT mvdata.* 
                FROM t_formelement_monitorvariable mvvar
                LEFT JOIN t_mv_data mvdata 
                    ON mvvar.monitorvariable_id=mvdata.monitor_variable_id
                WHERE mvdata.user_id=? AND (mvvar.formelement_id=? 
                    AND mvvar.monitor_element_type='2') 
                    ORDER BY mvdata.`date` DESC"
);

define(
    'GET_QUESTIONARIES_BY_GROUPPHASE', "SELECT ques.*,topic.*,ques_mv.*,
        property.*,topicdetail.topic_name
        FROM t_form_element ques 
        LEFT OUTER JOIN t_form topic ON topic.form_id=ques.form_id 
        INNER JOIN t_formelement_monitorvariable ques_mv 
            ON (ques_mv.formelement_id=ques.id 
            AND ques_mv.monitor_element_type='1')
        LEFT JOIN t_formelment_property property 
            ON (property.r_formelement_id=ques.id AND 
            (property.f_isdelete<>1 OR property.f_isdelete IS NULL))
        LEFT JOIN t_form_topics topicdetail 
            ON topicdetail.topic_id=topic.r_topic_id
        WHERE topic.r_quesgroup_id=? AND topic.r_quesphase_id=? AND
        (ques.is_delete<>1 OR ques.is_delete IS NULL)  
            ORDER BY ques.ui_order ASC,ques.id ASC"
);

//MK Static questions to be at end of the QuesList 
define(
    'GETQUESTION_OPTIONS', 'SELECT * FROM t_formelement_options
        WHERE r_form_elementid=? and f_isdelete<>1'
);
define(
    'GETQUESTION_OPTIONS_MAXVALUE', 'SELECT MAX(range_points) 
        AS option_maxval FROM t_formelement_options 
        WHERE r_form_elementid=? and f_isdelete<>1'
);
define(
    'GETQUESTION_OPTIONS_SUMVALUE', 'SELECT SUM(range_points) 
        AS option_maxval FROM t_formelement_options 
        WHERE r_form_elementid=? and f_isdelete<>1'
);
define(
    'GETQUESTION_PROPERTIES', 'SELECT * FROM t_formelment_property 
    WHERE r_formelement_id=? AND f_isdelete<>1'
);
define(
    'QUESTIONARIES_TYPE', 'SELECT * FROM `t_ques_phasegrouptype` 
        WHERE r_phase_id=? AND r_group_id=?'
);
/*Get User Answers*/
define(
    'GET_USERANSWERS', 'SELECT mvdt.*, mva.actmv_point, mva.actmv_parameter,
        mva.actmv_otherdata FROM t_mv_data mvdt 
        LEFT OUTER JOIN t_mvactual_answers mva  
            ON mvdt.id = mva.actmv_id 
        WHERE mvdt.user_id=? ORDER BY mvdt.date DESC'
);
/*Get Data To update User Phase*/
define(
    'GET_USER_PHASE', 'SELECT * FROM t_ques_phase_user 
    WHERE r_user_id=? AND r_phase_id=? AND r_group_id=?'
);
define(
    'INSERT_USER_PHASE', 'INSERT INTO t_ques_phase_user (r_user_id,
        r_phase_id,r_group_id,created_date,modified_date)
        VALUES(?,?,?,?,?)'
);
/*Restricted - Club with Company*/
define('GET_CLUB_LIST', 'SELECT * FROM t_clubs WHERE r_company_id=? ');
/*Restricted - Club AND Coach Type Users = > r_usertype_id*/
define(
    'GET_CLUB_COACHES', "SELECT coaches.*,pers.*,nation.nationality_name,
        city.city_name FROM t_users coaches 
        LEFT JOIN t_personalinfo pers ON pers.r_user_id=coaches.user_id
        LEFT JOIN t_nationality nation 
            ON nation.nationality_id=pers.r_nationality_id
        LEFT JOIN t_city city ON city.city_id=pers.r_city_id
        WHERE coaches.r_club_id=? AND coaches.r_usertype_id='1' AND coaches.r_status_id='1' AND coaches.is_deleted='0' AND FIND_IN_SET(1, relation_coach)"
);

/*UPDATE MV DATA - Based On the */

define(
    'GET_INSERTED_MVDATA', "SELECT * FROM t_mv_data 
    WHERE monitor_variable_id=? AND user_id=? AND 
    DATE_FORMAT(`date`,'%Y-%m-%d')=?"
);
define(
    'GET_INSERTED_MVACTUAL_POINTS', 'SELECT * FROM t_mvactual_answers 
    WHERE actmv_id=? '
);
define(
    'INSERT_MVACTUAL_POINT', 'INSERT INTO t_mvactual_answers(actmv_id,
    actmv_userid,actmv_point,actmv_date) VALUES(?,?,?,?)'
);

/*MK Added - Getting DESEASE PARAMETERS FROM TOPIC*/
define(
    'GET_TOPIC_DESEASE_PARAMS', 'SELECT disease_id,value 
    FROM t_topic_disease 
    WHERE topic_id=? ORDER BY value DESC LIMIT 0,1'
);

/*MK Added - Getting GOAL PARAMETERS FROM TOPIC*/
define(
    'GET_TOPIC_GOAL_PARAMS', 'SELECT goal_id,value FROM t_topic_goal 
    WHERE topic_id=? ORDER BY value DESC LIMIT 0,1'
);

/*MK Added - Anwser Options Languages list*/
define(
    'GET_OPTION_LANGBY_OPTION', 'SELECT * FROM t_formelem_option_language 
    WHERE f_isdelete<>1 AND optionid=?'
);

/*MK Added - */
define(
    'GET_PREVIOUS_ANSWERS', "SELECT * FROM t_mv_data
    WHERE monitor_variable_id=? AND user_id=? AND 
        DATE_FORMAT(`date`,'%Y-%m-%d')<? ORDER BY `date` DESC LIMIT 0,1"
);

/*MK Get Club & Coach With Group ID*/
define(
    'GET_MEMBER_CLUBLIST', 'SELECT * FROM t_employee_centre WHERE r_user_id=?'
);
define(
    'GET_MEMBER_COACHLIST', 'SELECT cmemb.*,cusr.first_name,cusr.last_name,
        cusr.r_club_id as coach_club FROM t_coach_member cmemb 
            JOIN t_users cusr ON cusr.user_id=cmemb.r_coach_id
            WHERE cmemb.r_user_id=?'
);
/*PK added (2016-03-24)*/
define(
    'GET_CITY_NATIONAL', 'SELECT city.city_name,national.nationality_name 
            FROM t_personalinfo info 
            LEFT JOIN t_city city 	ON city.city_id=info.r_city_id 
            LEFT JOIN t_nationality national
                ON national.nationality_id=info.r_nationality_id 
            WHERE info.r_user_id=?'
);
/*MK Added Question ID BY MVKEY*/
define(
    'GET_QUESTIONBY_MVID', 'SELECT fommv.formelement_id,
        form.r_quesgroup_id AS groupid,form.r_quesphase_id AS phaseid
        FROM t_formelement_monitorvariable fommv
        LEFT JOIN t_form_element formelem ON formelem.id=fommv.formelement_id
        LEFT JOIN t_form form ON form.form_id=formelem.form_id
        WHERE fommv.monitorvariable_id=?'
);
define(
    'GET_ACTIVITYBY_MVID', 'SELECT fommv.formelement_id,
        phaseact.group_id AS groupid,
        phaseact.phase_id AS phaseid 
        FROM t_formelement_monitorvariable fommv
        LEFT JOIN t_ques_phase_activity phaseact 
            ON phaseact.ques_activity_id=fommv.formelement_id
        WHERE fommv.monitorvariable_id=?'
);
define('GET_USER_BYUSERID', 'SELECT * FROM t_users WHERE user_id=?');
define(
    'GET_USERTEST_BYID', "SELECT * FROM t_user_test 
    WHERE is_analysed='1' AND r_user_id=? 
    ORDER BY created_date DESC LIMIT 0,1"
);
define(
    'GET_USER_LAST_ANALYSED_TEST_BY_USERID', "SELECT * FROM t_user_test uts 
        LEFT OUTER JOIN t_user_test_parameter utsprm 
            ON uts.user_test_id = utsprm.r_user_test_id  
        WHERE uts.is_analysed='1' AND uts.r_user_id=? 
            ORDER BY uts.created_date DESC LIMIT 0,1"
);
define(
    'GET_TRAINING_ACHIVEPOINTS', "SELECT * FROM t_training_points_achieved 
    WHERE f_userid=? AND DATE_FORMAT(f_creditdttm,'%Y-%m-%d')=? 
        AND f_refid=? AND f_reftypeid=?"
);
define(
    'GET_ACHIVE_CREDITS', 'SELECT tcredits.f_userid,tcredits.f_status,
        tcredits.f_groupid,tcredits.f_phaseid, 
        SUM(tcredits.f_points) AS credits 
        FROM t_training_points_achieved tcredits  
        WHERE tcredits.f_userid=? 
            GROUP BY tcredits.f_groupid,tcredits.f_status'
);
//May need to be added tcredits.f_phaseid
define(
    'GET_ACHIVE_CREDITS_BYPHASE', 'SELECT tcredits.f_userid,tcredits.f_status,
            tcredits.f_groupid,tcredits.f_phaseid, 
            SUM(tcredits.f_points) AS credits 
            FROM t_training_points_achieved tcredits  
            WHERE tcredits.f_userid=? 
                GROUP BY tcredits.f_phaseid,tcredits.f_groupid,tcredits.f_status'
);
define(
    'GET_ACHIVE_POINTS_BYUSER_STATUS', 'SELECT * FROM t_training_points_achieved 
    WHERE f_userid=? AND f_status=? AND f_isdelete!=1'
);

/*PK Added get Educationlist*/
define(
    'GET_EDUCATION', 'SELECT ed.education_id,ed.index_val,ed.day,
        ed.education_id,ed.r_group_id AS group_id,ld.languagae_desc,
        ld.r_language_id AS language_id
        FROM  t_education ed 
        LEFT JOIN t_language_description ld ON ld.ref_id=ed.education_id '
);


define(
    'GET_ACTIVE_TRAINING_FOR_DATE_USER', 'SELECT ctrain.*,utp.fitness_level 
            FROM t_cardiotraining ctrain 
            LEFT OUTER JOIN t_user_test utst 
                ON ctrain.f_testid = utst.user_test_id 
            LEFT OUTER JOIN t_user_test_parameter utp 
                ON ctrain.f_testid = utp.r_user_test_id 
            WHERE utst.r_user_id = ? AND ctrain.f_trainingstdate <= ? 
                AND ctrain.f_trainingeddate >= ? and ctrain.f_isdelete != 1'
);
define(
    'GET_ACTIVE_TRAINING_PLAN_BYTRAININGID', 'SELECT 
    cplan.f_cardiotrainingplanid, cplan.f_weeknr, cplan.f_points, 
    cplan.f_min as creditmin, cplan.f_max as creditmax, cplan.f_min_a,
    cplan.f_max_a,cplan.f_crdttm
    FROM t_cardiotrainingplan cplan
    LEFT OUTER JOIN t_cardiotraining ctrain 
    ON ctrain.f_cardiotrainingid=cplan.f_cardiotrainingid
    WHERE cplan.f_cardiotrainingid=? and cplan.f_isdelete != 1'
);
define(
    'GET_ACHIVE_POINTS_BYTRAINING_STATUS', 'SELECT * 
    FROM t_training_points_achieved 
    WHERE f_trainingid=? AND f_status=? AND f_isdelete!=1'
);
define(
    'GET_TOPGROUP_PHASE_TOPICS', 'SELECT * FROM t_form_topics ftopic 
    WHERE ftopic.f_isdelete<>1'
);
define(
    'GET_ACHIVE_POINTS_BYTRAINING', 'SELECT SUM(f_points) as points,
    f_creditdttm as date 
    FROM t_training_points_achieved 
    WHERE f_trainingid=? AND f_status=0 GROUP BY f_creditdttm'
);
define(
    'RESET_MONITOR_VARIABLE', 'DELETE FROM t_mv_data 
    WHERE user_id=? AND monitor_variable_id=?'
);
define(
    'RESET_MONITORANSWER_VARIABLE', 'DELETE FROM t_mvactual_answers 
    WHERE actmv_userid=? AND actmv_id=?'
);
define(
    'RESET_TRAINING_ACEIEVE_POINTS', 'DELETE FROM t_training_points_achieved 
    WHERE f_userid=? 
    AND f_refid=? AND f_reftypeid=?'
);
define(
    'GET_MVDATA_ID', 'SELECT id FROM t_mv_data WHERE user_id=? 
    AND monitor_variable_id=?'
);
define(
    'GET_PHASE_GROUP_QUESTION_MONITOR_ID', 'SELECT formelem.id as quesid,
    formelemmonitor.monitorvariable_id as monitorid FROM t_form form 
    LEFT OUTER JOIN t_form_element formelem 
        ON formelem.form_id=form.form_id 
    LEFT OUTER JOIN t_formelement_monitorvariable formelemmonitor 
        ON formelemmonitor.formelement_id=formelem.id
    WHERE form.r_quesgroup_id=? AND form.r_quesphase_id=?'
);
define(
    'GET_ACTIVITY_MVDATA_ID', 'SELECT quesact.activity_id as quesid, 
    formelemmonitor.monitorvariable_id as monitorid 
    FROM t_ques_activity quesact
    LEFT OUTER JOIN t_formelement_monitorvariable formelemmonitor 
        ON formelemmonitor.formelement_id=quesact.activity_id
    WHERE formelemmonitor.monitor_element_type='.QUESTIOARIES_TYPE_activity
);
define(
    'RESET_AVAILABLE_TEST', "UPDATE t_user_test SET `status`='0',
    is_analysed='0',test_start_date=? 
    WHERE r_user_id=? ORDER BY test_start_date DESC LIMIT 1"
);
define(
    'RESET_TRAINING', "UPDATE t_user_test SET `status`='0',is_analysed='0'
    WHERE r_user_id=? ORDER BY test_start_date DESC LIMIT 1"
);
define(
    'RESET_ANAYLSIS', "UPDATE t_user_test SET is_analysed='0' 
    WHERE r_user_id=? ORDER BY test_start_date DESC LIMIT 1"
);
define(
    'GET_MONITOR_VARIABLES_BYPHASEID', 'SELECT 
    felem.id as quesid,felemmv.monitorvariable_id as monitorid 
    FROM t_form f 
    JOIN t_form_element felem ON felem.form_id=f.form_id
    JOIN t_formelement_monitorvariable felemmv 
        ON (felemmv.formelement_id=felem.id 
        AND felemmv.monitor_element_type=1)
    WHERE f.r_quesphase_id=?'
);
define(
    'CHECK_RESET_USERS', 'SELECT * FROM t_users  
    WHERE email=? AND is_deleted<>1'
);
define(
    'DELETE_RESET_USERS', 'DELETE FROM t_users  
    WHERE user_id=?'
);
define(
    'GET_USER_PHASE_REMINDER','SELECT * 
        FROM t_phase_activity_reminder 
    WHERE f_phase_id=? AND f_group_id=? AND f_user_id=?'
);
