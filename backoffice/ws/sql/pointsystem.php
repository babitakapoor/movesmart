<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Point_System
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle point system related DB access.
 */
define(
    'GET_HEARTRATE_ZONE_ACTIVITY', 'SELECT * 
	FROM `t_heartrate_zone_activity` 
	WHERE `r_user_id`=? AND `r_user_test_id`=? AND `activity_id`=?'
);

define(
    'GET_USER_TEST_EXISTS_FOR_SPORT_SPECIFIC', 'SELECT * FROM `t_test_result`
	WHERE `r_user_id`=? AND `r_user_test_id`=?'
);

define(
    'GET_TRAINING_DEVICE_QUERY', 'SELECT * FROM `t_activity`
	WHERE activity_id=?'
);

define(
    'GET_USER_INFO_POINT_SYSTEM', 'SELECT * FROM `t_users` u 
		INNER JOIN t_user_test ut ON ut.r_user_id=u.user_id 
			AND ut.user_test_id=(SELECT MAX(`user_test_id`) 
			FROM `t_user_test` WHERE `r_user_id`=? AND `status` IN(3)) 
		 INNER JOIN `t_user_test_parameter` tp 
			ON  tp.r_user_test_id=ut.user_test_id 
		WHERE u.user_id=?'
);

define(
    'GET_POINTS_ACHIEVE_EXIST', 'SELECT * FROM `t_points_achieve` 
		WHERE `r_user_id`=? AND `r_user_test_id`=? AND `week`=?'
);

define(
    'GET_USER_POINTS_ACTIVITY', 'SELECT * FROM `t_heartrate_zone_activity` 
	WHERE r_user_id=? AND r_user_test_id=? AND r_training_device_id=? LIMIT 1'
);

define(
    'GET_USER_POINTS_ACTIVITY_CUR', 'SELECT th.*,tp.week,
		FORMAT(SUM( ptr.achieved_point ),1) achieved_point,
		tp.target_points FROM t_heartrate_zone_activity th 
		LEFT JOIN t_training_points_transaction ptr
			ON ptr.r_user_test_id=th.r_user_test_id
		LEFT JOIN t_points_achieve tp
			ON tp.r_user_id=th.r_user_id AND tp.r_user_test_id=th.r_user_test_id 
		WHERE th.r_user_id=? AND th.r_user_test_id=? 
				AND th.r_training_device_id=? AND 
			(CURDATE() BETWEEN tp.week_start_date AND tp.week_end_date) LIMIT 1'
);

define(
    'GET_COLLECTEDPOINT_DEVICE', 'SELECT td.training_device_id,
		td.training_device_name,hz.activity_id,hz.activity_name,
		SUM(tp.achieved_point) achieved_point,tp.training_points_transaction_id 
		FROM t_training_device td 
		LEFT JOIN t_heartrate_zone_activity hz 
			ON hz.r_training_device_id = td.training_device_id 
		LEFT JOIN  t_training_points_transaction tp 
			ON tp.activity_id = hz.activity_id  AND tp.`is_deleted`=0
		WHERE  tp.r_user_id=? AND tp.r_user_test_id=? '
);

define(
    'GET_COLLECTEDPOINT_ACTIVITY', "SELECT ta.activity_id, 
		ta.activity_name, workout_period,achieved_point,
		date_format(tp.points_collected_date, '%d-%m-%Y') as 
		points_collected_date,tp.training_points_transaction_id
	FROM t_activity ta
	LEFT JOIN t_training_points_transaction tp 
		ON tp.activity_id = ta.activity_id AND tp.`is_deleted`=0
	WHERE tp.r_user_id=? AND tp.r_user_test_id=?"
);
