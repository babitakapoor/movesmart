<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Coach
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle coach related DB access.
 */
define(
    'GET_COACH_ADDOREDITQUERY', 'SELECT * FROM t_users 
        WHERE email = ? AND r_company_id = ?'
);

define(
    'GET_COACH_USERID', 'SELECT * FROM t_users 
        WHERE (r_usertype_id =1 || r_usertype_id =2 || r_usertype_id =4)   
            AND user_id = ?'
);

//Get the user list based on the user type
/*define(
    'GET_USER_LIST_BASED_ON_TYPE', 'SELECT u.*,pI.*,s.* FROM `t_users` AS u
          LEFT JOIN t_usertype AS utype 
            ON utype.usertype_id = u.r_usertype_id
          LEFT JOIN t_personalinfo AS pI 
            ON pI.r_user_id = u.user_id
          LEFT JOIN t_status AS s 
            ON s.status_id	= u.r_status_id
          WHERE (u.r_usertype_id =1 || 
                u.r_usertype_id =2 || 
                u.r_usertype_id =4) AND (is_deleted<>1)'
);*/
//Get the user personal info from the tbl_users and t_personalinfo table
define(
    'GET_COACH_PERSONAL_INFO', "SELECT u.*,p.*,
        if(u.gender=0,'male','female') as gender 
            FROM `t_users` AS u 
        LEFT JOIN t_personalinfo AS p ON
        p.r_user_id = u.user_id 
        WHERE u.user_id = ? AND u.is_deleted<>1"
);

//Get the Personal details of Member/Coach
define(
    'GET_COACH_PERSONALINFO', 'SELECT * FROM t_personalinfo 
        WHERE r_user_id = ?'
);

//Get Training Program Details of Member
/*define(
    'GET_TRAINING_INFO', 'SELECT p.dob,us.gender,s.status,tp.fitness_level,
        tp.target,tp.intensity,r_coach_id,
        ha.* FROM `t_heartrate_zone_activity` ha
             LEFT JOIN `t_user_test_parameter` tp
                ON tp.r_user_test_id=ha.r_user_test_id
             LEFT JOIN `t_users` us
                ON us.user_id=ha.r_user_id
             LEFT JOIN `t_personalinfo` p
                ON p.r_user_id=ha.r_user_id
             LEFT JOIN `t_coach_member` c
                ON c.r_user_id=ha.r_user_id
             LEFT JOIN `t_status` s
                ON s.status_id=us.r_status_id
            WHERE ha.r_user_id=? AND us.is_deleted<>1'
);
*/
//Get the User Soft Delete
define(
    'GET_COACH_DELETED', 'SELECT * FROM t_users 
        WHERE user_id = ?'
);
define(
    'COACH_DELETED', 'DELETE FROM t_users 
        WHERE user_id = ?'
);
define(
    'GET_CENTRE_EMPLOYEE_EXIST', 'SELECT * FROM `t_employee_centre` 
        WHERE `r_company_id` = ? AND `r_club_id` = ? 
            AND `r_user_id` = ? AND `is_deleted`=0'
);

define(
    'GET_COACH_USERINFO', 'SELECT * FROM t_users WHERE user_id=?'
);
