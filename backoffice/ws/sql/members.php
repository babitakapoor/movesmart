<?php
/**
 * PHP version 5.

 * @category SQL
 
 * @package Member
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle member related DB access.
 */

define('GET_MEMBER', 'SELECT * FROM t_users');
define(
    'GET_USER_BY_ID_AND_EMAIL', 'SELECT * FROM `t_users` 
    WHERE `user_id`=? and `email`=?'
);

define(
    'GET_USER_BY_EMAIL_ID', 'SELECT * FROM `t_users` 
    WHERE `email`=?'
);

define(
    'GET_USER_INFO_BY_EMAIL_AND_KEY', 'SELECT * FROM `t_users` 
    WHERE `email`=? AND `reset_password_key`=? AND `reset_password_flag`=1'
);

define(
    'GET_USER_SEQURITY_ANSWERS_BY_ID', 'SELECT `security_questions`,
    `user_id` FROM `t_users` WHERE `user_id`=?'
);

define(
    'GET_USER_TEST_ID_BASED', 'SELECT * FROM t_user_test 
    where user_test_id=?'
);

define(
    'GET_MEMBER_TEST_WITH_ANY_STATUS', 'SELECT * FROM t_user_test 
    where r_user_id=? ORDER BY user_test_id DESC LIMIT 1'
);

define(
    'GET_MEMBER_DETAIL_BY_EMAIL', 'SELECT * FROM `t_users` U 
        LEFT JOIN `t_usertype` UT ON UT.usertype_id=U.r_usertype_id
        WHERE U.email=?'
);

define(
    'GET_MEMBER_QUERY',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name, u.middle_name,
    u.last_name, coach.r_coach_id as coach_id, personal.phone, if(u.gender=0,'male','female') as gender,u.r_status_id,
    cl.club_name,if(u.email='',u.username,u.email) as emailusername,
    u.email,u.person_id,personal.gsm,s.status,u.uid,
    (SELECT GROUP_CONCAT(if(ec.is_primary,
    CONCAT(clu.club_name,' (p)'),clu.club_name),'<br>') 
    from t_employee_centre AS ec 
    LEFT JOIN t_clubs clu ON clu.club_id=ec.r_club_id  
    WHERE ec.r_user_id = u.user_id AND 
        (u.r_usertype_id=1 || u.r_usertype_id=2 || u.r_usertype_id=4)) as clubs
        FROM t_users u
        LEFT OUTER JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
		LEFT OUTER JOIN t_coach_member coach ON coach.r_user_id=u.user_id
        LEFT OUTER JOIN t_company co ON co.company_id=u.r_company_id
        LEFT OUTER JOIN t_clubs cl ON cl.club_id=u.r_club_id 
        LEFT OUTER JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id
        LEFT JOIN t_status AS s ON s.status_id=u.r_status_id
"
);
/*MK Added userimage with the Result*/
define(
    'GET_MEMBER_QUERY_ERGO_STATUS_TEST_TO_DO',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name, u.middle_name, 
    u.last_name, if(u.gender=0,'male','female') as gender,u.r_status_id,
    cl.club_name,if(u.email='',u.username,u.email) as emailusername,
    u.email,u.person_id,personal.gsm,personal.userimage,
    cl.club_name clubs
    FROM t_users u
    INNER JOIN `t_user_test` cardiotest ON cardiotest.r_user_id=u.user_id 
        AND `cardiotest`.status=".TEST_STATUS_TODO.' 
    LEFT JOIN t_personalinfo personal 
        ON personal.r_user_id=u.user_id
    LEFT JOIN t_company co ON co.company_id=u.r_company_id
    LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id 
    LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id 
 '
);
/*MK Added Repeated Memebers On To-do CardioTest*/
define(
    'GET_MEMBERS_CARDIO_TODO', "SELECT SQL_CALC_FOUND_ROWS u.user_id,
        u.first_name, u.middle_name, u.last_name, 
        if(u.gender=0,'male','female') as gender,u.r_status_id, 
        cl.club_name,if(u.email='',u.username,u.email) as emailusername,
        u.email,u.person_id,personal.gsm,personal.userimage,
        cl.club_name clubs,cardiotest.test_start_date
        FROM t_users u
            INNER JOIN `t_user_test` cardiotest 
                ON cardiotest.r_user_id=u.user_id AND cardiotest.status in (?) 
            LEFT JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
            LEFT JOIN t_company co ON co.company_id=u.r_company_id
            LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id 
            LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id "
);

define(
    'GET_MEMBER_QUERY_TO_MANY_POINTS_THIS_WEEK',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name,
    u.middle_name, u.last_name, 
    if(u.gender=0,'male','female') as gender,
    u.r_status_id, cl.club_name,
    if(u.email='',u.username,u.email) as emailusername,
    u.email,u.person_id,personal.gsm,
    cl.club_name clubs
    FROM t_users u
    INNER JOIN t_points_achieve tpa ON tpa.r_user_id=u.user_id
    INNER JOIN `t_user_test` utest 
        ON utest.user_test_id = tpa.r_user_test_id AND utest.`status`=3
    LEFT JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
    LEFT JOIN t_company co ON co.company_id=u.r_company_id
    LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id 
    LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id"
);

define(
    'GET_MEMBER_QUERY_BODYCOMPOSITION_STATUS_TEST_TO_DO',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name, 
     u.middle_name, u.last_name, if(u.gender=0,'male','female') as gender,
     u.r_status_id, cl.club_name,
     if(u.email='',u.username,u.email) as emailusername,
     u.email,u.person_id,personal.gsm,cl.club_name clubs
    FROM t_users u
    INNER JOIN `t_bodycomposition_test` bodycomtest 
        ON bodycomtest.r_user_id=u.user_id 
        AND `bodycomtest`.test_status= ".TEST_STATUS_TODO.'
    LEFT JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
    LEFT JOIN t_company co ON co.company_id=u.r_company_id
    LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id 
    LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id'
);

define(
    'GET_MEMBER_QUERY_CARDIO_PRINT_OR_SEND',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name, 
     u.middle_name, u.last_name, if(u.gender=0,'male','female') as gender,
     u.r_status_id, cl.club_name,
     if(u.email='',u.username,u.email) as emailusername,
     u.email,u.person_id,personal.gsm,
     cl.club_name clubs,utest.test_start_date test_date,utest.user_test_id
    FROM t_users u
    INNER JOIN `t_user_test` utest 
        ON utest.r_user_id=u.user_id AND `utest`.status=3 
        AND `utest`.put_in_waitinglist=1
    LEFT JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
    LEFT JOIN t_company co ON co.company_id=u.r_company_id
    LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id 
    LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id "
);
define(
    'GET_FLEXIBILITY_DATA_TODO', "SELECT SQL_CALC_FOUND_ROWS u.user_id, 
     u.first_name, u.middle_name, u.last_name, 
     if(u.gender=0,'male','female') as gender,u.r_status_id, 
     cl.club_name,if(u.email='',u.username,u.email) as emailusername,
     u.email,u.person_id,personal.gsm,cl.club_name clubs 
    FROM t_users u 
    INNER JOIN `t_flexibility_test` flex 
        ON flex.r_user_id=u.user_id AND `flex`.test_status=".TEST_STATUS_TODO.' 
    LEFT JOIN t_personalinfo personal ON personal.r_user_id=u.user_id 
    LEFT JOIN t_company co ON co.company_id=u.r_company_id 
    LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id  
    LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id'
);
define(
    'GET_MEMBER_LIST_TEST_TO_ANALYZE',
    "SELECT u.user_id, u.first_name, u.middle_name, u.last_name, 
        if(u.gender=0,'male','female') as gender,u.r_status_id, 
        if(u.email='',u.username,u.email) as emailusername,u.email,
        u.person_id,personal.gsm,s.status
    FROM `t_user_test` test 
    INNER JOIN `t_users` u 
        ON u.user_id=test.r_user_id AND u.`r_status_id`='6' 
        AND u.r_company_id=? AND u.r_club_id=?
    LEFT OUTER JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
    LEFT OUTER JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id
    LEFT JOIN t_status AS s ON s.status_id=u.r_status_id 
    WHERE u.user_id>0 GROUP BY u.user_id"
);

define(
    'GET_LMO_LIST',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name, 
        u.middle_name, u.last_name, if(u.gender=0,'male','female') as gender, 
        cl.club_name,if(u.email='',u.username,u.email) as emailusername,
        u.email,u.person_id,personal.gsm,s.status,u.uid,
        DATE_FORMAT(LM.created_date, '%d-%m-%Y %h:%i:%s') lmo_date,LM.lmo_id
             FROM `t_lmo` LM
             LEFT JOIN t_users u ON u.user_id=LM.r_user_id	
             LEFT JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
             LEFT JOIN t_company co ON co.company_id=u.r_company_id
             LEFT JOIN t_clubs cl ON cl.club_id=u.r_club_id 
             LEFT JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id
             LEFT JOIN t_status AS s ON s.status_id=u.r_status_id
             WHERE u.r_company_id=? and u.is_deleted<>1"
);

// define('GET_QUERY_LAST_TOTAL_COUNT', "SELECT FOUND_ROWS() as total_count");	

define(
    'GET_MEMBER_BASIC_DETAIL', "SELECT u.*,p.dob, p.height, 
        if(u.gender=0,'male','female') as gender,p.phone,p.mobile, 
        s.status as member_status,c.r_coach_id,u.r_club_id,u.email,
        company.company_name,club.club_name FROM `t_users` u
          LEFT JOIN `t_company` company ON company.company_id=u.r_company_id
          LEFT JOIN `t_clubs` club ON club.club_id=u.r_club_id
          LEFT JOIN `t_personalinfo` p ON p.r_user_id=u.user_id
          LEFT JOIN `t_coach_member` c ON c.r_user_id=u.user_id
          LEFT JOIN `t_status` s ON s.status_id=u.r_status_id 
          WHERE u.user_id=? AND u.is_deleted<>1"
);

define(
    'GET_MEMBER_BASIC_DETAIL_BY_PERSONID', "SELECT u.*,p.dob, 
        if(u.gender=0,'male','female') as gender,p.phone,p.mobile, 
        s.status as member_status,c.r_coach_id,u.r_club_id,u.email 
            FROM `t_users` u
              LEFT JOIN `t_personalinfo` p ON p.r_user_id=u.user_id
              LEFT JOIN `t_coach_member` c ON c.r_user_id=u.user_id
              LEFT JOIN `t_status` s ON s.status_id=u.r_status_id 
              WHERE u.person_id=? AND u.r_company_id=? AND u.is_deleted<>1"
);

define(
    'GET_MEMBER_TEST_DETAIL', 'SELECT *,ut.status test_status 
        FROM `t_user_test` ut 
          LEFT JOIN `t_user_test_parameter` tp 
            ON  tp.r_user_test_id=ut.user_test_id 
          WHERE ut.r_user_id=? AND ut.`status` IN(?) 
          AND ut.user_test_id=(SELECT MAX(`user_test_id`) 
            FROM `t_user_test` WHERE `r_user_id`=?) '
);

define(
    'GET_MEMBER_QUERY_SEARCH',
    "SELECT u.user_id, u.first_name, u.last_name, 
        if(u.gender=0,'male','female') as gender, cl.club_name,
        if(u.email='',u.username,u.email) as emailusername,
        u.person_id FROM t_users u  
        LEFT OUTER JOIN t_company co 
            ON co.company_id=u.r_company_id  
        LEFT OUTER JOIN t_clubs cl ON cl.club_id=u.r_club_id  
        LEFT OUTER JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id "
);

define(
    'GET_LISTEDITMEMBER_QUERY', 'SELECT u.user_id, u.r_status_id,
        u.r_company_id,u.first_name,u.middle_name,u.last_name,
        u.email,u.person_id,u.r_status_id,p.address,p.zipcode,
        p.bus,p.r_city_id,p.r_profession_id,p.r_nationality_id,
        p.phone,p.mobile,p.fax,p.dob,u.gender,u.username,u.password,
        p.nativelanguage,p.profession_labortype,p.company,p.hobby,
        p.training_freq, p.training,p.userimage,p.location_fitnessclub,
        p.location_hometraining,p.location_outdoor,p.notes,cm.r_coach_id,
        tut.test_start_date,p.doctor,p.doctor_phone,p.doctor_specification,
        p.company_doctor,p.company_doctor_phone,p.company_specification,
        p.specialist,p.specialist_phone,p.specialist_specification,p.signature,
        c.city_name as location,p.height,p.weight,u.r_club_id,cl.club_name,
        cl.t_ismedicalinfo as isclubmedicalinfo,com.company_name,p.bus,
        p.doorno,pr.profession_name,
        na.nationality_name,p.doctor_email,p.doctor_doorno,p.doctor_address,
        p.doctor_bus,p.doctor_zipcode,p.doctor_city,p.doctor_country,
        medicalinfo.have_smoke,medicalinfo.have_medication,
        medicalinfo.have_stress_test,medicalinfo.have_injury,
        medicalinfo.have_injury,medicalinfo.smoke_amount,
        medicalinfo.medication_description,
        medicalinfo.stress_test_on,medicalinfo.injury_description,
        medicalinfo.feel_today,u.logintype,u.loginreffield_id
             FROM t_users u 
             LEFT OUTER JOIN t_personalinfo p ON p.r_user_id=u.user_id
             LEFT OUTER JOIN t_clubs cl ON cl.club_id=u.r_club_id
             LEFT OUTER JOIN t_company com ON com.company_id=u.r_company_id
             LEFT OUTER JOIN t_coach_member cm ON cm.r_user_id=u.user_id
             LEFT OUTER JOIN t_city c ON c.city_id =p.r_city_id
             LEFT OUTER JOIN t_city z ON z.city_id =p.zipcode
             LEFT OUTER JOIN t_profession pr 
                ON pr.profession_id =p.r_profession_id
             LEFT OUTER JOIN t_nationality na 
                ON na.nationality_id=p.r_nationality_id
             LEFT OUTER JOIN t_user_test tut ON tut.r_user_id=u.user_id
             LEFT OUTER JOIN t_member_medicalinfo medicalinfo 
                ON medicalinfo.r_user_id=u.user_id
             where u.user_id=?'
);

define(
    'GET_STATUS_QUERY',
    'SELECT status_id, status 
          FROM t_status'
);

define(
    'GET_CITY_QUERY',
    'SELECT city_id,city_name,zipcode
                          FROM t_city'
);

define(
    'GET_CITY_QUERY_WITH_CONDITION',
    'SELECT city_id,city_name,zipcode
              FROM t_city WHERE zipcode=?'
);

define(
    'GET_PROFESSION_QUERY',
    'SELECT profession_id, profession_name 
          FROM t_profession'
);

define(
    'GET_NATIONALITY_QUERY',
    'SELECT nationality_id, nationality_name 
          FROM t_nationality'
);

define(
    'GET_COMPANYLIST_QUERY',
    'SELECT company_id, company_name 
          FROM t_company
          WHERE status=1 AND is_deleted=0'
);

define(
    'GET_EDITSELECTMAIN_QUERY',
    'SELECT * 
             FROM t_users where user_id=?'
);

define(
    'GET_MEMBER_EMAIL_UPDATE_MODE',
    'SELECT * 
        FROM t_users where user_id!=? AND ((username=? AND password=?) 
        OR (email=? AND password=?))'
);

define(
    'GET_EDITSELECTPERSONALINFO_QUERY',
    'SELECT * 
        FROM t_personalinfo where r_user_id=?'
);

define(
    'GET_EDITSELECTMEDICALRISKINFO_QUERY',
    'SELECT * 
        FROM t_medical_riskfactor where r_user_id=?'
);

define(
    'GET_EDITSELECTMEDICALAGAINSTINFO_QUERY',
    'SELECT * 
        FROM t_medical_against where r_user_id=?'
);

define(
    'GET_EDITSELECTMEDICALJOINTINFO_QUERY',
    'SELECT * 
        FROM t_medical_jointproblem where r_user_id=?'
);

define(
    'GET_EDITSELECTMEDICALMUSCLEINFO_QUERY',
    'SELECT * 
        FROM t_medical_muscleinjury where r_user_id=?'
);

define(
    'GET_COACHLISTFORUSER_QUERY',
    "SELECT u.user_id as coach_id, CONCAT(u.first_name,' ',u.last_name) as coach_name
         FROM t_users u where u.r_usertype_id=1 and u.is_deleted<>1"
);

define(
    'GET_COACHEDITBYUSER_QUERY',
    'SELECT r_coach_id,r_user_id
        FROM t_coach_member where r_user_id=?'
);

define(
    'GET_PROFILEIMAGE_QUERY',
    'SELECT userimage,r_user_id
        FROM t_personalinfo where r_user_id=?'
);

define('GET_USER_TEST_TO_DO', 'SELECT * FROM t_user_test where r_user_id=?');

define(
    'GET_MEMBER_PERSON_NUMBER', 'SELECT * FROM t_users 
    WHERE person_id = ?'
);

define(
    'GETMEMBERID_FOR_NAVIGATION',
    'SELECT 
    (
    SELECT user_id
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS tu 
        ON tu.usertype_id = u.r_usertype_id  
    WHERE is_deleted=0 AND user_id < ? AND tu.usertype_id=3 
        ORDER BY user_id DESC LIMIT 1
    ) AS previd,
    (
    SELECT user_id
    FROM t_users AS u 
    LEFT OUTER JOIN t_usertype AS tu ON tu.usertype_id = u.r_usertype_id  
    WHERE is_deleted=0 AND user_id > ? AND tu.usertype_id=3 
        ORDER BY user_id ASC LIMIT 1
    ) AS nextid,
    (
    SELECT min(user_id) 
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS ut 
        ON ut.usertype_id=u.r_usertype_id
    WHERE is_deleted=0 AND usertype_id=3  
    ) AS min_user_id,
    (
    SELECT max(user_id) 
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS ut 
        ON ut.usertype_id=u.r_usertype_id
    WHERE is_deleted=0 AND usertype_id=3  
    ) AS max_user_id'
);

define(
    'GETEMPLOYEEID_FOR_NAVIGATION',
    'SELECT 
    (
    SELECT user_id
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS tu 
        ON tu.usertype_id = u.r_usertype_id  
    WHERE user_id < ? AND (tu.usertype_id=1 || 
        tu.usertype_id=2 || tu.usertype_id=4) 
        ORDER BY user_id DESC LIMIT 1
    ) AS previd,
    (
    SELECT user_id
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS tu 
        ON tu.usertype_id = u.r_usertype_id  
    WHERE user_id > ? AND (tu.usertype_id=1 ||
        tu.usertype_id=2 || tu.usertype_id=4) ORDER BY user_id ASC LIMIT 1
    ) AS nextid,
    (
    SELECT min(user_id) 
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS ut 
        ON ut.usertype_id=u.r_usertype_id
    WHERE (tu.usertype_id=1 || tu.usertype_id=2 || tu.usertype_id=4)
    ) AS min_user_id,
    (
    SELECT max(user_id) 
    FROM t_users AS u LEFT OUTER JOIN t_usertype AS ut 
        ON ut.usertype_id=u.r_usertype_id
    WHERE (tu.usertype_id=1 || tu.usertype_id=2 
        || tu.usertype_id=4)
    ) AS max_user_id'
);

//Get Member Email Exist..													
define('GET_MEMBER_EXIST_EMAIL', 'SELECT * FROM t_users WHERE email = ?');

//Get Medical Informations in Members Info 
define(
    'GET_LIST_MEDICALAGAINST', 'SELECT * FROM t_medical_against 
    where r_user_id=?'
);
define(
    'GET_LIST_MEDICALJOINT', 'SELECT * FROM t_medical_jointproblem 
    where r_user_id=?'
);
define(
    'GET_LIST_MEDICALMUSCLE', 'SELECT * FROM t_medical_muscleinjury 
    where r_user_id=?'
);
define(
    'GET_LIST_MEDICALRISKFACTOR', 'SELECT * FROM t_medical_riskfactor 
    where r_user_id=?'
);

define(
    'GET_LIST_MAPDEVICE', 'SELECT * FROM t_device_member DM
        LEFT OUTER JOIN t_device D ON D.device_id=DM.r_device_id
        WHERE DM.r_user_id=? AND is_deleted != 1 '
);

define('GET_DEVICE_DETAILS', 'SELECT * FROM t_device WHERE device_id = ?');
define('GETCITYID', 'SELECT * FROM t_city WHERE city_name=? LIMIT 1');
define(
    'GETPROFESSIONID', 'SELECT * FROM t_profession 
    WHERE profession_name=? LIMIT 1'
);
define(
    'GETNATIONALITYID', 'SELECT * FROM t_nationality 
    WHERE nationality_name=? LIMIT 1'
);
define('GET_STATUS_ID', 'SELECT status_id FROM t_status WHERE status=?');
define('GET_AGE_ON_TEST', 'SELECT TIMESTAMPDIFF( YEAR,?,CURDATE()) AS age');
define('GET_UPDATEUSER', 'SELECT * FROM t_users where user_id=?');

define('GET_MEMBER_LMO_EXIST', 'SELECT * FROM `t_lmo` WHERE `lmo_id`=?');
define('GET_PLAN_DETAILS_BY_ID', 'SELECT * FROM `t_plan` WHERE `plan_id`=?');

define(
    'GET_PLANS_BY_CLUB_ID', 'SELECT * FROM `t_plan_clubs` CP 
        INNER JOIN `t_plan` P ON P.plan_id=CP.r_plan_id 
        WHERE CP.`r_club_id`=? AND P.`is_deleted`=0'
);
define(
    'GET_LMO_PLAN_EXIST', 'SELECT * FROM `t_lmo_detail` 
    WHERE `r_lmo_id`=? AND `r_plan_id`=?'
);
define(
    'DELETE_LMO_PLANS', 'DELETE FROM `t_lmo_detail` 
    WHERE `r_lmo_id`=?'
);
define(
    'GET_PERSONIDEXIST', 'SELECT count(user_id) as personCount FROM t_users
    WHERE person_id=?'
);
define('UPDATE_USER_CLUB_PRIMARY', 'SELECT * FROM t_users WHERE user_id=?');
define(
    'UPDATE_EMPLOYEE_CENTER', 'SELECT * FROM t_employee_centre 
    WHERE r_user_id=?'
);
define(
    'UPDATE_ISPRIMARY_CENTER', 'SELECT * FROM t_employee_centre
    WHERE employee_centre_id=?'
);
define(
    'GET_PLANS_BY_LMO_ID', 'SELECT * FROM `t_lmo_detail` 
    WHERE `r_lmo_id`=? AND `is_deleted`=0'
);
define(
    'GET_DEVICE_MEMBER_EXIST', 'SELECT * FROM `t_device_member` 
    WHERE `device_member_id`=? AND `r_user_id`=?'
);
define(
    'UPDATE_DEVICE_MEMBER_STATUS', 'UPDATE `t_device_member` SET `is_deleted`=1 
    WHERE `r_user_id`=?'
);
define(
    'GET_DEVICE_ACTIVE_STATUS', 'SELECT * FROM `t_device` 
    WHERE `device_id`=? AND `status`=0'
);
define(
    'GET_LAST_TEST_PROGRESS', 'SELECT status as testCount FROM t_user_test 
    WHERE r_user_id=? ORDER BY user_test_id DESC LIMIT 1'
);
define(
    'GET_BOOKING_FOR_MEMBER', 'SELECT * FROM t_booking WHERE r_user_id=? 
    ORDER BY start_date DESC'
);
define(
    'GET_BOOKING_FOR_MEMBER_COUNT', 'SELECT count(*) as bookNotCancel 
    FROM t_booking WHERE r_user_id=? AND is_canceled=0'
);
define(
    'GET_MEMBER_EMAIL_UPDATE', 'SELECT * FROM t_users 
    WHERE email = ? AND user_id!=? AND password=? '
);
define(
    'GET_BIO_METRIC_FAT_MEMBER', 'SELECT * FROM `t_testing_measuring` TM 
    INNER JOIN `t_testing_measuring_transaction` TMT 
        ON TMT.r_testing_measuring_id=TM.testing_measuring_id
        AND TMT.r_test_item_id=28
    WHERE TM.`r_user_id`=? 
    ORDER BY TM.`test_date` DESC LIMIT 1'
);
define(
    'GET_CHANGE_PASSWORD', 'SELECT * FROM t_users 
    WHERE user_id=? AND password = ?'
);
define(
    'INSERT_MEMBER_QUERY', 'INSERT INTO `t_users`(r_company_id,first_name,
    last_name,gender,email,r_usertype_id,r_status_id,password,created_date,phone,
    logintype,loginreffield_id,r_language_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)'
);
define(
    'INSERT_MEMBER_PERSONALINFO_QUERY', 'INSERT INTO `t_personalinfo`(
    r_user_id,phone,userimage,dob) VALUES(?,?,?,?)'
);
define(
    'INSERT_MEMBER__SECURITY_QUERY', 'UPDATE t_users SET security_questions=? 
    WHERE user_id=?'
);

 /* MK Added Registeration Steps */

define(
    'GET_EXIST_PERSONINFO_USER', 'SELECT * FROM t_personalinfo 
    WHERE r_user_id=?'
);

 /*MK Added Get Activities List - in future may be restricted by Club & Coach*/

define('GET_ACTIVTY_QUERY', 'SELECT * FROM t_activity');

define(
    'GET_ACTIVTY_MEMBER', 'SELECT * FROM t_activity_member 
    WHERE r_user_id= ? AND r_activity_id = ?'
);

define(
    'INSERT_ACTIVTY_MEMBER', 'INSERT INTO t_activity_member (r_user_id,
    r_activity_id,frequency_type,average_time,created_by,created_date) 
    VALUES(?,?,?,?,?,?)'
);

define(
    'GET_MEMEBER_MEDICALINFO', 'SELECT * FROM t_member_medicalinfo 
    WHERE r_user_id = ? '
);

define(
    'INSERT_FLEXIBILITY_DETAILS', 'INSERT INTO `t_flexibility_test` (r_user_id,
    r_testid,created_date)VALUES(?,?,?)'
);
define(
    'GET_CURRENT_TEST_COUNT', 'SELECT (SELECT count(*) FROM t_bodycomposition_test ) 
    as bodycomposition,
        (SELECT count(*) FROM t_flexibility_test) as flexibility,
        (SELECT count(*) FROM t_strength_user_test) as strength,
        (SELECT count(*) FROM t_testing_measuring tmes 
        WHERE tmes.r_test_id = 7) as cardio'
);

define(
    'GETUSERDATAVALUES_DESC', 'SELECT testparm.fitness_level as fitlevel, 
    utest.weight as weight FROM t_user_test utest 
    LEFT OUTER JOIN t_user_test_parameter testparm 
        ON utest.user_test_id= testparm.r_user_test_id
    WHERE utest.r_user_id = ?
        ORDER BY utest.user_test_id DESC LIMIT 1'
);
/*MK Added - User Based Test is not retreived by old query GETUSERDATAVALUES_ASC */
define(
    'GETUSERDATAVALUES_ASC', 'SELECT testparm.fitness_level as fitlevel, 
    utest.weight as weight FROM t_user_test utest 
    LEFT OUTER JOIN t_user_test_parameter testparm 
        ON utest.user_test_id= testparm.r_user_test_id
    WHERE utest.r_user_id = ? 
    ORDER BY utest.user_test_id ASC LIMIT 1'
);

define(
    'GET_USER_DATA_FAT_LEVEL_DESC', 'SELECT tmst.value,tmst.r_test_item_id 
    FROM t_bodycomposition_test bct 
    LEFT OUTER JOIN t_testing_measuring_transaction tmst 
        ON tmst.r_testing_measuring_id = bct.testing_measuring_id
    WHERE bct.r_user_id = ? AND tmst.r_test_item_id IN(28,29) 
        ORDER BY bct.bodycomposition_test_id
        DESC LIMIT 0,2'
);
define(
    'GET_USER_DATA_FAT_LEVEL_ASC', 'SELECT tmst.value,tmst.r_test_item_id
        from t_bodycomposition_test bct 
        LEFT OUTER JOIN t_testing_measuring_transaction tmst 
            ON tmst.r_testing_measuring_id = bct.testing_measuring_id
        WHERE bct.r_user_id = ? AND tmst.r_test_item_id IN(29,28)  
            ORDER BY bct.bodycomposition_test_id ASC  LIMIT 2'
);
/*SK - Members Mapped to the Device*/
define(
    'GET_MAPPED_USER_DETAILS', "SELECT * FROM t_user_test tu 
    LEFT OUTER JOIN  t_device_member dm 
        ON dm.device_member_id = tu.r_device_id
    WHERE tu.r_user_id = ? AND tu.`status`= '".TEST_STATUS_TODO."'"
);
define(
    'GET_LIST_MACHINES_DATA', 'SELECT SQL_CALC_FOUND_ROWS 
    sm.strength_machine_id as machineid,sm.uniqueid as clubname,
    sm.machine_name as nameofmachine, brnd.brand_name,
    typ.machine_type as `type`,grup.group_name as groupname,
    sbty.subtype_name as subtype,sm.machineimage as image,
    stats.`status` as `status`
        FROM t_strength_machine sm 
        LEFT OUTER JOIN t_clubs clb ON clb.club_id = sm.clubid
        LEFT OUTER JOIN t_machine_brands brnd ON brnd.brand_id = sm.brand
        LEFT OUTER JOIN t_machine_group grup 
            ON  grup.group_id = sm.r_group
        LEFT OUTER JOIN t_machine_subtype sbty 
            ON sbty.subtype_id = sm.subtype
        LEFT OUTER JOIN t_machine_types typ 
            ON typ.machine_type_id = sm.r_type
        LEFT OUTER JOIN t_status stats 
            ON stats.status_id = sm.active '
);
/*
/*MK Commented SK's QUERY*/
define(
    'GET_MAX_TEST_DETAILS', 'SELECT max(testing_measuring_id) as max 
    FROM t_testing_measuring WHERE r_user_id = ? AND r_test_id = ?'
);
/**/
define(
    'GET_LASTTEST_DETAILS', 'SELECT tmt.r_test_item_id as itemdata, 
    tmt.value as value,flextest.remarks 
    FROM t_testing_measuring_transaction tmt 
        LEFT OUTER JOIN t_flexibility_test flextest 
            ON tmt.r_testing_measuring_id=flextest.testing_measuring_id
        WHERE tmt.r_testing_measuring_id=(SELECT MAX(testing_measuring_id) 
            FROM t_testing_measuring WHERE r_user_id = ? AND r_test_id = ?)'
);
/*MK Added Last Cardio Test ID*/
define(
    'GET_LASTCARDIO_TEST', 'SELECT * 
    FROM t_user_test_parameter utestparam 
    LEFT OUTER JOIN t_user_test utest 
        ON utest.user_test_id=utestparam.r_user_test_id
    WHERE utestparam.r_user_test_id=(SELECT user_test_id FROM t_user_test 
        WHERE r_user_id=? ORDER BY user_test_id DESC LIMIT 0,1);'
);
define(
    'GET_LASTCARDIO_TEST_ALL_MEM', "SELECT *,
    utest.r_user_id as userid, usr.first_name, usr.last_name,
    usrper.dob,usrper.userimage, utest.weight as testweight 
    FROM t_user_test_parameter utestparam 
    LEFT OUTER JOIN t_user_test utest 
        ON utest.user_test_id=utestparam.r_user_test_id
    LEFT OUTER JOIN t_users usr ON usr.user_id=utest.r_user_id
    LEFT OUTER JOIN t_personalinfo usrper ON usrper.r_user_id=usr.user_id
    WHERE utest.r_club_id = ? AND 
        DATE_FORMAT(utest.test_start_date,'%Y-%m-%d') = ? AND 
        utest.`status` IN (?) AND test_active IN (?) "
);//MK Changed Status to IN

/*MK CARDIO TEST BY MEMBER*/
define(
    'GET_LASTCARDIO_TEST_BY_MEM', "SELECT *,utest.r_user_id AS userid,
    usr.first_name, usr.last_name,usrper.dob,usrper.userimage, 
    utest.weight as testweight 
    FROM t_user_test_parameter utestparam 
        LEFT OUTER JOIN t_user_test utest 
            ON utest.user_test_id=utestparam.r_user_test_id
        LEFT OUTER JOIN t_users usr ON usr.user_id=utest.r_user_id
        LEFT OUTER JOIN t_personalinfo usrper 
            ON usrper.r_user_id=usr.user_id
    WHERE utest.r_club_id = ? AND
        DATE_FORMAT(utest.test_start_date,'%Y-%m-%d') = ? AND 
        utest.`status` IN (?) AND test_active IN (?)  AND utest.r_user_id=? 
        ORDER BY utest.created_date DESC LIMIT 0,1 ;"
);

define(
    'GET_LAST_TEST_DETAILS', 'SELECT tmt.r_test_item_id as itemdata, 
    tmt.value as value FROM t_testing_measuring tm 
    LEFT OUTER JOIN t_testing_measuring_transaction tmt
        ON tmt.r_testing_measuring_id = tm.testing_measuring_id
    WHERE tmt.r_user_id = ?  AND tmt.r_testing_measuring_id = ? '
);
// define('GET_LIST_MACHINES_DATA',"SELECT * FROM t_user_test");
define('GET_BRAND_LIST', 'SELECT * FROM t_machine_brands');
define('GET_TYPE_LIST', 'SELECT * FROM t_machine_types');
define('GET_GROUP_LIST', 'SELECT * FROM t_machine_group');
define('GET_SUBTYPE_LIST', 'SELECT * FROM t_machine_subtype');
/*Comments: To handle save new machine data.TASKID = '4307'
* author SK Shanethatech * 
* Created Date  : FEB-05-2016
*/
define(
    'INSERT_NEW_MACHINE_DETAILS', 'INSERT INTO t_strength_machine 
    (`uniqueid`,`machine_name`,`brand`,`r_type`,
    `r_group`,`subtype`,`clubid`,`description`,`active`,`outoforder`,
    `allow_mix`,`allow_circuit`,`allow_free_training`,`is_certified`,
    `machineimage`,`videourl`,`image_url`,`f_confactor`,`created_date`,`created_by`) 
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
);
define(
    'UPDATE_NEW_MACHINE_DETAILS', 'UPDATE t_strength_machine SET `uniqueid`=?,
    `machine_name`=?,`brand`=?,`r_type`=?,`r_group`=?,`subtype`=?,`clubid`=?,
    `description`=?,`active`=?,`outoforder`=?,`allow_mix`=?,`allow_circuit`=?,
 `allow_free_training`=?,`is_certified`=?,`machineimage`=?,`videourl`=?,
 `image_url`=?,`f_confactor`=?,`modified_date`=?,`modified_by`=? WHERE strength_machine_id= ?'
);
define(
    'GET_MAX_AUTOCLUBID', 'SELECT max(uniqueid)as maxclubid FROM t_strength_machine 
    WHERE clubid = ?'
);
define(
    'EDIT_NEW_MACHINE_DETAILS', 'SELECT * from t_strength_machine sm 
    WHERE sm.strength_machine_id = ?'
);
define(
    'DELETE_NEW_MACHINE_DETAILS', 'UPDATE `t_strength_machine` SET `is_deleted` = 1 
    WHERE strength_machine_id IN(?)'
);

/*
MK Added - To get Heartrate Devices List Device Type ID is hardcored 
need to send from Fron-End Later
*/
define(
    'GET_HEARTRATE_DEVICES', "SELECT device.*, 
    COUNT(devmember.device_member_id) as concout,devmember.r_user_id
    FROM t_device device 
        LEFT JOIN t_device_member devmember 
            ON ( devmember.r_device_id=device.device_id 
            AND DATE(devmember.`date`)=CURDATE())
        WHERE device.r_device_type_id='1' GROUP BY device.device_id"
);

define(
    'GET_HEARTRATE_DEVICES_ATTACHED_USER', "SELECT device.device_code, 
    usrs.last_name, usrs.first_name, usrs.user_id, devmember.r_device_id
    FROM t_device device 
        LEFT OUTER JOIN t_device_member devmember
            ON ( devmember.r_device_id=device.device_id)
        LEFT OUTER JOIN t_users usrs 
            ON usrs.user_id = devmember.r_user_id
    WHERE device.r_device_type_id='1' "
);

/*MK Added - Update Heart Rate RPM group on test*/
define('GET_USERTESTBYID', 'SELECT * FROM t_user_test WHERE user_test_id=?');
/**/
define(
    'GET_CARDIO_TESTBY_ID', 'SELECT utestparam.*, utest.*, usr.gender 
    FROM t_user_test_parameter utestparam 
        LEFT OUTER JOIN t_user_test utest 
            ON utest.user_test_id=utestparam.r_user_test_id
        LEFT OUTER JOIN t_users usr 
            ON usr.user_id=utest.r_user_id
        WHERE utestparam.r_user_test_id=? '
);
/*MK Added - User Detail By User ID*/
define('GET_USERDETAIL_BYID', 'SELECT * FROM t_users WHERE user_id=?');
define(
    'GET_WEEK_PROGRAM', 'SELECT st.strength_program_training_id as id,
    st.training_week as tweek,st.series as series,
    st.reputation as reputation,st.time as time,
    st.strength_percentage as strengthpercentage,st.rest as rest  
    FROM t_strength_program sp  
    LEFT OUTER JOIN t_strength_program_training st 
        ON st.r_strength_program_id = sp.strength_program_id 
    WHERE sp.`type` = ? AND sp.clubid = ?'
);
define(
    'GET_WEEK_PROGRAM_PID', 'SELECT st.strength_program_training_id as id,
    st.strength_program_training_id,st.training_week as tweek,
    st.series as series,st.reputation as reputation,st.time as time,
    st.strength_percentage as strengthpercentage,st.rest as rest   
    FROM t_strength_program sp  
    LEFT OUTER JOIN t_strength_program_training st 
    ON st.r_strength_program_id = sp.strength_program_id
    LEFT OUTER JOIN t_strength_user_test sut 
    ON sut.r_strength_program_id = st.r_strength_program_id
    WHERE  sut.r_strength_program_id = ? AND sp.clubid = ? 
    GROUP BY st.strength_program_training_id'
);

/*SN added get client personal info*/
define(
    'GET_CLIENTINFORMATION_QUERY', 'SELECT u.user_id, u.r_status_id,
    u.r_company_id,u.first_name,u.middle_name,u.last_name,u.email,
    u.person_id,u.r_status_id,p.address,p.zipcode,p.bus,p.r_city_id,
    p.r_profession_id,p.r_nationality_id,p.phone,p.mobile,p.fax,
    p.dob,u.gender,u.username,u.password,p.nativelanguage,
    p.profession_labortype,p.company,p.hobby,p.training_freq, 
    p.training,p.userimage,p.location_fitnessclub,p.location_hometraining,
    p.location_outdoor,p.notes,cm.r_coach_id,tut.test_start_date,
    p.doctor,p.doctor_phone,p.doctor_specification,p.company_doctor,
    p.company_doctor_phone,p.company_specification,p.specialist,
    p.specialist_phone,p.specialist_specification,p.signature,
    c.city_name as location,p.height,
    p.weight,u.r_club_id,cl.club_name,
    cl.t_ismedicalinfo as isclubmedicalinfo,com.company_name,
    p.bus,p.doorno,pr.profession_name,
    na.nationality_name,p.doctor_email,p.doctor_doorno,
    p.doctor_address,p.doctor_bus,p.doctor_zipcode,p.doctor_city,
    p.doctor_country,
    medicalinfo.have_smoke,medicalinfo.have_medication,
    medicalinfo.have_stress_test,medicalinfo.have_injury,
    medicalinfo.have_injury,medicalinfo.smoke_amount,
    medicalinfo.medication_description,
    medicalinfo.stress_test_on,medicalinfo.injury_description,
    medicalinfo.feel_today,
    actmem.r_activity_id,actmem.frequency_type,actmem.average_time,
    activity.activity_name,p.entry_date,p.doorno,cl.t_ismedicalinfo
         FROM t_users u 
         LEFT OUTER JOIN t_personalinfo p ON p.r_user_id=u.user_id
         LEFT OUTER JOIN t_clubs cl ON cl.club_id=u.r_club_id
         LEFT OUTER JOIN t_company com ON com.company_id=u.r_company_id
         LEFT OUTER JOIN t_coach_member cm ON cm.r_user_id=u.user_id
         LEFT OUTER JOIN t_city c ON c.city_id =p.r_city_id
         LEFT OUTER JOIN t_city z ON z.city_id =p.zipcode
         LEFT OUTER JOIN t_profession pr 
            ON pr.profession_id =p.r_profession_id
         LEFT OUTER JOIN t_nationality na 
            ON na.nationality_id=p.r_nationality_id
         LEFT OUTER JOIN t_user_test tut ON tut.r_user_id=u.user_id
         LEFT OUTER JOIN t_member_medicalinfo medicalinfo 
            ON medicalinfo.r_user_id=u.user_id
         LEFT OUTER JOIN t_activity_member actmem 
            ON actmem.r_user_id=u.user_id
        LEFT OUTER JOIN t_activity activity 
            ON activity.activity_id=actmem.r_activity_id
         where u.user_id=?'
);
define(
    'GET_CLIENTINFORMATION_ACTIVITY_QUERY', 'SELECT * FROM 
        t_activity_member actmem
        LEFT OUTER JOIN t_activity activity 
            ON activity.activity_id=actmem.r_activity_id
         WHERE actmem.r_user_id=?'
);

/*MK - Added Club Equipment List
status='1' denotes the active equipment*/
define(
    'GET_CLUB_EQUIPMENTS', 'SELECT * FROM t_equipment 
    WHERE r_club_id=? AND status=1'
);

/*MK Cardio Test Details for the Todo members*/
define(
    'GET_CARDIOTEST_MEMBERS', "SELECT *,tusr.r_user_id as userid  
    FROM t_user_test tusr 
    LEFT OUTER JOIN t_user_test_parameter utestparam 
        ON tusr.user_test_id=utestparam.r_user_test_id
    WHERE tusr.r_user_id in (?) AND DATE_FORMAT(tusr.test_start_date,'%Y-%m-%d')=? 
    AND tusr.`status`=? 
    GROUP BY tusr.r_user_id 
    ORDER BY tusr.r_user_id, test_start_date "
);

/*MK - Get Test Summary For a member */
define(
    'GET_TESTCOUNT_BC', 'SELECT COUNT(*) as cnt FROM t_bodycomposition_test
    WHERE r_user_id=?'
);
define(
    'GET_TESTCOUNT_STRENGTH', 'SELECT COUNT(*) as cnt FROM t_flexibility_test
    WHERE r_user_id=?'
);
define(
    'GET_TESTCOUNT_FLEX', 'SELECT COUNT(*) as cnt FROM t_strength_user_test
    WHERE r_user_id=?'
);
/*TEST TYPE r_test_id 7 DENOTES THE CARDIO need to Keep as CONSTANT*/
define(
    'GET_TESTCOUNT_CARDIO', "SELECT utest.is_analysed,COUNT(*) as cnt
    FROM t_user_test utest 
    WHERE utest.r_user_id=? AND utest.status='".TEST_STATUS_CARDIO_COMPLETE."' 
    GROUP BY utest.is_analysed"
);

define(
    'GET_TESTFOR_ANALYSE', "SELECT usrtst.*, usr.gender, 
    per.dob FROM t_user_test usrtst 
    LEFT OUTER JOIN t_users usr ON usrtst.r_user_id = usr.user_id 
    LEFT OUTER JOIN t_personalinfo per ON per.r_user_id = usr.user_id
    WHERE usrtst.r_user_id=? 
        AND usrtst.status='".TEST_STATUS_CARDIO_COMPLETE."'"
);

/**/
define(
    'GET_COACH_MEMBER_BY_COACH_MEM', 'SELECT * FROM t_coach_member 
    WHERE r_coach_id=? AND r_user_id=? AND r_group_id=?'
);
define(
    'GET_CLUBMEMBER', 'SELECT * FROM t_employee_centre 
    WHERE r_user_id=? AND r_company_id=? AND r_club_id=?'
);

/*MK Client Device Mapping*/
define('GET_DEVICE_BY_DEVICECODE', 'SELECT * FROM t_device WHERE device_code=?');
define(
    'UPDATE_EXIST_DEVICE_UNMAP', "UPDATE t_device_member SET is_deleted='1',
    modified_by=?,modified_date=? 
    WHERE r_device_id=?"
);
define(
    'INSERT_DEVICE_MEMBER', 'INSERT INTO t_device_member(r_user_id,
    r_device_id,date,time,created_by,created_date) VALUES (?,?,?,?,?,?)'
);

/*SNK For Cardio test and analysis*/
define(
    'GET_FITLEVEL_DATA_FOR_AGE_SEX_SPEED', 'SELECT * FROM t_fitlevel 
    where f_agemin <= ? and f_agemax >= ? and f_gender = ? 
    and f_speed_min <= ? and f_speed_max >= ?'
);
define(
    'GET_FITLEVEL_DATA_FOR_AGE_SEX_FITLEVEL', 'SELECT f_weeknr, f_points, 
    f_min, f_max, f_min_a, f_max_a FROM t_fitpoints 
    where f_age_min <= ? and f_age_max >= ? and f_gender = ? and f_fitlevel = ?'
);
define(
    'GET_VALID_CREDIT_TO_REDEEM', 'SELECT * FROM t_training_points_achieved ptach 
    where f_userid = ? and f_status = 0 and f_groupid in (?)'
);
define(
    'INSERT_TO_POINTS_ACHIVED', 'INSERT INTO `t_training_points_achieved` 
    (`f_userid`, `f_trainingid`, `f_trainingtype`, `f_phaseid`, `f_groupid`, 
    `f_status`, `f_hrdata`, `f_creditdttm`, `f_points`, `f_refid`,
    `f_reftypeid`, `f_isdelete`, `f_crdttm`) VALUES (?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?, ?, ?, ?)'
);

/*MK Added - Update Heart Rate RPM group on test*/
define(
    'GET_USERTEST_PARAM_BYTESTID', 'SELECT * FROM t_user_test_parameter 
    WHERE r_user_test_id=?'
);
define(
    'GET_CARDIO_TRAINING_BYTESTID', 'SELECT * FROM t_cardiotraining 
    WHERE f_testid=? and f_isdelete != 1'
);
define(
    'GET_CARDIO_TRAINING_PLAN_BYTRAININGID', 'SELECT * FROM t_cardiotrainingplan
    WHERE f_cardiotrainingid=? and f_isdelete != 1'
);
define(
    'GET_POINTS_PER_ACTIVITY', 'SELECT actpoint.*,act.activity_id,
    act.activity_name,act.activity_image,act.activity_type,
    act.r_training_device_id, act.r_activity_group_id,act.f_uiorder,act.f_hrzonereq 	
    FROM t_activity act 
    LEFT OUTER JOIN t_activitypoints actpoint 
    ON actpoint.f_activityid = act.activity_id 
    WHERE actpoint.f_min_level <= ?  And actpoint.f_max_level >= ? and
    act.r_activity_group_id > 0  
    order by act.r_activity_group_id, act.f_uiorder'
);
/**/
/*SN - Added Tablet Settings*/
define('GET_DEVICE_DATA', 'SELECT SQL_CALC_FOUND_ROWS * FROM t_tabletsettings');
define(
    'GET_DEVICE_MACHINE_DATA', 'SELECT SQL_CALC_FOUND_ROWS * 
    FROM t_tabletsettings tblt 
    LEFT OUTER JOIN t_strength_machine machine 
        ON machine.strength_machine_id=tblt.r_machineid
    WHERE r_clubid = ? AND createdby=?'
);
define(
    'GET_DEVICE_EXIST', 'SELECT * FROM t_tabletsettings WHERE createdby=?'
);
/*PK added - Message information */
define(
    'INSERT_MESSAGE_EMAIL', 'INSERT INTO `t_messages` (`message`,
    `sender_id`,`type`,`receiver_id`,`r_club_id`,`added_date`) 
    VALUES (?, ?, ?, ?,?,?)'
);

define(
    'GET_TEST_REPORTPRINT_BYTESTID', 'SELECT test.*,users.first_name,
    users.last_name,users.email,club.club_name FROM t_user_test test 
        LEFT JOIN t_users users ON users.user_id=test.r_user_id
        LEFT JOIN t_clubs club  ON users.r_club_id=club.club_id
    WHERE test.user_test_id=? '
);
/*temp added get security question answers*/
define(
    'GET_MEMBER_SECURITY_ANSWERS', 'SELECT * FROM t_users usr 
    WHERE usr.email=? AND usr.password=?'
);
/*PK added -manage-report*/
define ('GET_REPORTS_ACTIVITY','SELECT
                    ach.f_trainingid,
                    ach.f_activityid,
                    ach.f_groupid,
                    SUM(ach.f_points) as tpoints,
                    at.activity_name,
                    at.activity_image 
                FROM t_training_points_achieved ach 
                    LEFT JOIN t_activity at ON at.activity_id=ach.f_activityid 
                WHERE ach.f_userid=? GROUP BY ach.f_activityid,ach.f_groupid,ach.f_groupid,ach.f_trainingid'
);
define('GET_USERTEST_BYUSERID',"SELECT 
                utest.user_test_id, 
                utest.r_club_id, 
                utest.weight, 
                utest.weeks_points_to_improve,
                utest.r_test_id, 
                cardio.f_cardiotrainingid,
                cardio.f_trainingstdate as stdate,
                cardio.f_trainingeddate as enddate, 
                utp.fitness_level
        FROM t_user_test utest 
            INNER JOIN t_cardiotraining cardio 
                ON cardio.f_testid=utest.user_test_id
            LEFT OUTER JOIN t_user_test_parameter utp 
                ON utp.r_user_test_id=utest.user_test_id
        WHERE utest.r_user_id='5'"
);

define(
    'GET_SUBTYPE_LISTS', 'SELECT * FROM t_machine_subtype left join t_machine_group on t_machine_subtype.r_group_id = t_machine_group.group_id'
);