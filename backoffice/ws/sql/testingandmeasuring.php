<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Testing_Measuring
  
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle testing and measuring  related DB access.
 */
define(
    'GET_MEMBER_QUERY_TESTING_MEASURING',
    "SELECT SQL_CALC_FOUND_ROWS u.user_id, u.first_name, u.middle_name, 
        u.last_name, if(u.gender=0,'male','female') as gender, 
        cl.club_name,if(u.email='',u.username,u.email) as emailusername,
        u.email,u.person_id,personal.gsm,s.status,u.uid
     FROM t_users u
     LEFT OUTER JOIN t_personalinfo personal ON personal.r_user_id=u.user_id
     LEFT OUTER JOIN t_company co ON co.company_id=u.r_company_id
     LEFT OUTER JOIN t_clubs cl ON cl.club_id=u.r_club_id 
     LEFT OUTER JOIN t_usertype ut ON ut.usertype_id=u.r_usertype_id
     LEFT JOIN t_status AS s ON s.status_id=u.r_status_id
     WHERE u.r_company_id=? and u.is_deleted<>1"
);

define(
    'GET_TESTING_MEASURING_EXIST',
    'SELECT * FROM `t_testing_measuring` WHERE `r_user_id`=? AND `r_test_id`=? AND 
        `test_date`=? ORDER BY testing_measuring_id DESC'
);
define(
    'DELETE_TESTING_MEASURING_EXIST', 'SELECT * FROM `t_testing_measuring` WHERE 
        `testing_measuring_id`=?'
);
define(
    'GET_TESTING_MEASURING_TRANSACTION_EXIST', 'SELECT * FROM 
        `t_testing_measuring_transaction` WHERE `r_user_id`=? AND `r_test_id`=? 
        AND `r_test_item_id`=? AND `r_testing_measuring_id`=?'
);
define(
    'GET_TESTING_MEASURING_TRANSACTION_VALUES', 'SELECT * FROM 
        `t_testing_measuring` TM 
        INNER JOIN `t_testing_measuring_transaction` TT ON 
        TT.`r_testing_measuring_id`=TM.`testing_measuring_id`
        INNER JOIN `t_test_items` TI ON TI.`test_item_id`=TT.`r_test_item_id`
        WHERE TM.`r_user_id`=? AND TM.`r_test_id`=?'
);
define(
    'INSERT_BODY_COMPTION_DATA', 'INSERT INTO `t_bodycomposition_test` 
        (r_user_id,testing_measuring_id,created_date,test_status,created_by) 
        VALUES(?,?,?,?,?)'
);
define(
    'INSERT_FLEXIBILITY_TEST_DATA', 'INSERT INTO `t_flexibility_test` 
    (r_user_id,testing_measuring_id,created_date,test_status,created_by,remarks,flex_level)
    VALUES(?,?,?,?,?,?,?)'   
);
/*SN - Cardio Test Report*/
define(
    'GET_TESTING_MEASURING_CARDIO_VALUES', 'SELECT SQL_CALC_FOUND_ROWS * FROM 
        `t_user_test` usrtest LEFT OUTER JOIN `t_user_test_parameter` 
        usrparam ON usrtest.user_test_id=usrparam.r_user_test_id
        WHERE usrtest.r_user_id=? and  usrtest.`status`=1'
);
