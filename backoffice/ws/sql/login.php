<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle login related DB access.
 */
/* SN Added - To Check client - t_ismedicalinfo By Club */
define(
    'GET_USER_LOGIN', 'SELECT user.security_questions,user.r_status_id,user.user_id,user.uid,
        user.r_club_id, user.r_usertype_id AS usertype_id, user.first_name,
        user.middle_name, user.last_name,user.external_application_user_id, 
        user.email, user.username, usertype.usertype,user.is_deleted,
        c.club_name,c.t_ismedicalinfo as isclubmedicalinfo,user.r_company_id,
        user.external_application_user_id
          FROM t_users AS user
          LEFT JOIN t_usertype AS usertype 
            ON usertype.usertype_id = user.r_usertype_id
          LEFT JOIN t_clubs AS c ON c.club_id = user.r_club_id
          WHERE (username = ?  OR email = ? ) AND password = ? 
          AND r_status_id != 2 AND FIND_IN_SET(2, relation_coach)'
); 

define(
    'GET_USER_PAGE_PERMISSION', 'SELECT `menu_page_id`,p.* FROM `t_menu_pages` m 
            INNER JOIN `t_page_permission` p
                ON p.r_menu_page_id=m.menu_page_id AND `r_user_type_id`=?
            WHERE m.`file_name`=? LIMIT 1'
);
/*MK Added 04 Jan 2015 Task No.3946 */
/*SN added 20160203*/
/* SN Added - To Check client - t_ismedicalinfo By Club */
 define(
    'GET_USER_LOGIN_BYTYPE', 'SELECT user.user_id,user.r_status_id,user.uid,user.r_club_id, 
        user.r_usertype_id AS usertype_id, user.first_name,user.middle_name, 
        user.last_name,user.external_application_user_id, user.email, 
        user.username, usertype.usertype, user.is_deleted,c.club_name,
        c.t_ismedicalinfo as isclubmedicalinfo, user.r_company_id, 
        user.security_questions,user.gender, user.external_application_user_id,
        user.phone,personalinfo.personal_track,personalinfo.userimage,
        user.r_language_id, personalinfo.dob,personalinfo.height,
        personalinfo.weight,personalinfo.step_entry_type AS stepgame_type
            FROM t_users AS user
          LEFT OUTER JOIN t_personalinfo AS personalinfo 
            ON personalinfo.r_user_id=user.user_id
          LEFT OUTER JOIN t_usertype AS usertype 
            ON usertype.usertype_id = user.r_usertype_id
          LEFT OUTER JOIN t_clubs AS c ON c.club_id = user.r_club_id
          WHERE (username = ?  OR email = ? ) AND password = ? 
            AND user.r_usertype_id= ? AND r_status_id != 2'
); 
 define(
    'GET_USER_LOGIN_MEMBER', 'SELECT user.user_id,user.r_status_id,user.uid,user.r_club_id, 
        user.r_usertype_id AS usertype_id, user.first_name,user.middle_name, 
        user.last_name,user.external_application_user_id, user.email, 
        user.username, usertype.usertype, user.is_deleted,c.club_name,
        c.t_ismedicalinfo as isclubmedicalinfo, user.r_company_id, 
        user.security_questions,user.gender, user.external_application_user_id,
        user.phone,personalinfo.personal_track,personalinfo.userimage,
        user.r_language_id, personalinfo.dob,personalinfo.height,
        personalinfo.weight,personalinfo.step_entry_type AS stepgame_type
            FROM t_users AS user
          LEFT OUTER JOIN t_personalinfo AS personalinfo 
            ON personalinfo.r_user_id=user.user_id
          LEFT OUTER JOIN t_usertype AS usertype 
            ON usertype.usertype_id = user.r_usertype_id
          LEFT OUTER JOIN t_clubs AS c ON c.club_id = user.r_club_id
          WHERE (username =? OR email =?) AND password = ? AND user.r_usertype_id= ?'
);  
/*MK Added 04 Jan 2015 Task No.3946 - Ends*/;
