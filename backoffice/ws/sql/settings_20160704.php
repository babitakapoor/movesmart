<?php
/**
 * PHP version 5.
 
 * @category SQL
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description Sql Queries to handle settings related DB access.
 */

/**
 * GET USER TYPE QUERY.
 **/
define(
    'GET_USER_TYPE', 'SELECT * FROM `t_usertype`
    WHERE `status`=1 ORDER BY `usertype` ASC'
);

/* MENU QUERIES **/
define(
    'GET_MENU_PAGES', 'SELECT p.*, m.name parent_name FROM `t_menu_pages` p 
         LEFT JOIN `t_menu_pages` m ON m.`menu_page_id`=p.`parent_id`
         WHERE p.is_delete=0 ORDER BY p.`menu_order` ASC'
);

define(
    'GET_MENU_PAGES_ONLY_VISIBLE', 'SELECT M.*,
    GROUP_CONCAT(MP.file_name) as sub_file_names FROM `t_menu_pages` M 
    LEFT JOIN `t_menu_pages` MP 
        ON MP.parent_id=M.menu_page_id AND MP.is_visible != 1 
    WHERE M.`is_visible`=1  GROUP BY M.menu_page_id 
        ORDER BY M.`menu_order` ASC'
);

define(
    'GET_MENU_PAGES_ONLY_VISIBLE_USER', 'SELECT M.*,
        GROUP_CONCAT(MP.file_name) as sub_file_names FROM `t_menu_pages` M 
        INNER JOIN `t_page_permission` PP 
            ON PP.r_user_type_id=? AND M.menu_page_id=PP.r_menu_page_id
        LEFT JOIN `t_menu_pages` MP 
            ON MP.parent_id=M.menu_page_id AND MP.is_visible != 1 
        WHERE M.`is_visible`=1  GROUP BY M.menu_page_id 
        ORDER BY M.`menu_order` ASC'
);

define(
    'GET_MENU_PAGES_PARENT', 'SELECT * FROM `t_menu_pages`
    WHERE `parent_id`=0 ORDER BY `name` ASC'
);

define(
    'DELETE_MENU_PAGE_PARENT_EXIST', 'SELECT * FROM `t_menu_pages` 
    WHERE `parent_id`=?'
);

define(
    'GET_MENU_PAGE_EXIST', 'SELECT * FROM `t_menu_pages` 
    WHERE `menu_page_id`=?'
);

//define('GET_MENU_PARENT', 'SELECT * FROM `t_menu_pages` WHERE `parent_id`=0');

define(
    'INSERT_MENU', 'INSERT `t_menu_pages`(name,menu_name,parent_id,menu_type,
    icon,menu_order,position,file_name,is_visible,created_on) 
    VALUES (?,?,?,?,?,?,?,?,?,NOW())'
);

define(
    'UPDATE_MENU', 'UPDATE `t_menu_pages` SET name=?,menu_name=?,
    parent_id=?,menu_type=?,icon=?,menu_order=?,position=?,file_name=?,
    is_visible=?,modified_on=NOW() WHERE menu_page_id=?'
);

define(
    'DELETE_BY_MENU_ID', 'UPDATE `t_menu_pages` SET is_delete =1 
    WHERE `menu_page_id`=?'
);

/* PAGE PERMISSION QUERIES **/

define(
    'GET_PAGE_PERMISSION', 'SELECT * FROM `t_page_permission` 
    WHERE `permission_id`=? AND `r_user_type_id`=? LIMIT 1'
);

define(
    'GET_PAGE_USER_TYPE_PERMISSION', 'SELECT * FROM `t_page_permission`
    WHERE `r_user_type_id`=?'
);

define(
    'DELETE_USER_TYPE_PAGE_PERMISSIONS', 'DELETE FROM `t_page_permission` 
    WHERE `r_user_type_id`=?'
);

define(
    'DELETE_MENU_PAGE', 'DELETE FROM `t_menu_pages`
    WHERE `menu_page_id`=?'
);

/* GROUP QUERIES - Added by SR **/

define(
    'GET_GROUP_EXIST', 'SELECT * FROM `t_group` 
    WHERE created_by = ? '
);

define(
    'GET_GROUP', 'SELECT * FROM `t_group` WHERE  f_isdelete =0'
);

define(
    'GET_BY_GROUP_ID', 'SELECT * FROM `t_group` WHERE group_id = ?'
);

define(
    'DELETE_BY_GROUP_ID', 'UPDATE `t_group` SET f_isdelete =1
    WHERE `group_id`=?'
);

define(
    'UPDATE_GROUP', 'UPDATE `t_group` SET group_name =?,modified_on=NOW()
    WHERE `group_id`=?'
);

define(
    'INSERT_GROUP', 'INSERT `t_group`(group_name,created_by,created_on) 
    VALUES (?,?,NOW())'
);

/* BRAND QUERIES - Added by PK**/

define(
    'GET_BRAND_EXIST', 'SELECT * FROM `t_machine_brands` 
    WHERE created_by = ? '
);

define(
    'GET_BY_BRAND_ID', 'SELECT * FROM `t_machine_brands` 
    WHERE brand_id = ?'
);

define(
    'DELETE_BY_BRAND_ID', 'UPDATE `t_machine_brands` SET f_isdelete =1 
    WHERE `brand_id`=?'
);

define(
    'UPDATE_BRAND', 'UPDATE `t_machine_brands` SET brand_name =?,
    brand_description=?,modified_on=NOW() WHERE `brand_id`=?'
);

define(
    'GET_BRAND', 'SELECT * FROM `t_machine_brands` 
    WHERE  f_isdelete =0'
);

define(
    'INSERT_BRAND', 'INSERT `t_machine_brands`(brand_name,brand_description,
    created_by,created_on) VALUES (?,?,?,NOW())'
);

/* QUESTIONARIES QUERIES - Added by SN **/

define(
    'GET_GROUP_TOPICS', 'SELECT * FROM `t_form_topics` 
    WHERE group_id=? AND f_isdelete<>1'
);

define('GET_QUES_GROUP', 'SELECT * FROM `t_ques_group`');

define('GET_QUES_PHASE', 'SELECT * FROM `t_ques_phase`');

define(
    'GET_DISEASE', 'SELECT disease.id as diseaseid,disease.disease_name,
        topicdis.topic_id,formtopic.topic_name,topicdis.value
        FROM t_disease disease 
        LEFT OUTER JOIN t_topic_disease topicdis 
            ON topicdis.disease_id=disease.id
        LEFT OUTER JOIN t_form_topics formtopic
            ON formtopic.topic_id=topicdis.topic_id 
            ORDER BY disease_name, topic_name'
);

define(
    'UPDATE_DISEASE', 'UPDATE t_topic_disease SET value=? 
    WHERE disease_id=? AND topic_id=?'
);

define(
    'GET_QUES_PHASE_RANGE', 'SELECT pr.r_phase_id, pr.r_group_id, pr.id,
    pr.max_range,pr.min_range,
    prlang_txt1.`text` as txt_success,
    prlang_txt2.`text` as txt_aw,
    prlang_txt3.`text` as txt_goal,
    prlang_txt4.`text` as txt_adv,
    quesgroup.group_name
        FROM t_ques_phase_range pr 
        LEFT JOIN t_ques_phase_range_language prlang_txt1 
            ON (prlang_txt1.r_ques_phase_range_id=pr.id 
            AND prlang_txt1.text_type=1 )
        LEFT JOIN t_ques_group quesgroup 
            ON quesgroup.group_id=pr.r_group_id
        LEFT JOIN t_ques_phase_range_language prlang_txt2 
            ON (prlang_txt2.r_ques_phase_range_id=pr.id 
            AND prlang_txt2.text_type=2 ) 
        LEFT JOIN t_ques_phase_range_language prlang_txt3 
            ON (prlang_txt3.r_ques_phase_range_id=pr.id 
            AND prlang_txt3.text_type=3) 
        LEFT JOIN t_ques_phase_range_language prlang_txt4 
            ON (prlang_txt4.r_ques_phase_range_id=pr.id 
            AND prlang_txt4.text_type=4 ) 
        WHERE prlang_txt1.languageid=? AND prlang_txt2.languageid=? 
        AND prlang_txt3.languageid=? AND prlang_txt4.languageid=?  
        AND prlang_txt1.f_isdelete<>1 AND prlang_txt2.f_isdelete<>1 
        AND prlang_txt3.f_isdelete<>1 AND prlang_txt4.f_isdelete<>1
        AND pr.f_isdelete<>1'
);

define(
    'DELETE_BY_PHASE_RANGE_ID', 'UPDATE t_ques_phase_range SET f_isdelete=1 
    WHERE id=?'
);

define(
    'GET_FORM_PHASE_GROUP_TOPIC', 'SELECT * FROM t_form 
    WHERE r_quesphase_id=? AND r_quesgroup_id=? AND r_topic_id = ?'
);

define(
    'GET_QUES_PHASE_GROUP_TYPE', 'SELECT * FROM t_ques_phasegrouptype 
    WHERE r_phase_id=? AND r_group_id=?'
);

define('GET_QUES_PHASE_GROUP', 'SELECT * FROM t_ques_phasegrouptype');

define('GET_QUES_ACTIVITY', 'SELECT * FROM `t_ques_activity`');

define(
    'GET_QUESTIONNAIRE', 'SELECT *,formelem.id as formelemid, 
            form.form_id as formid 
            FROM t_form form
            LEFT JOIN t_form_element formelem 
                ON formelem.form_id=form.form_id
            LEFT JOIN t_form_topics formtopic
                ON formtopic.topic_id=form.r_topic_id
            LEFT OUTER JOIN t_question_language queslang 
                ON queslang.formelement_id=formelem.id 
            WHERE queslang.language_id=? AND form.r_quesphase_id=?  
                AND form.r_quesgroup_id=? AND formelem.is_delete<>1 
                AND queslang.f_isdelete<>1'
);

define(
    'GET_BY_QUESTION_ID', 'SELECT form.form_id,form.r_topic_id,formelem.id,
        formelem.type,formelem.ui_order,formelem.is_static,queslang.question,
        queslang.language_id,queslang.question_info,queslang.id as queslangid,
        score,formelem.icon
        FROM t_form form 
        LEFT JOIN t_form_element formelem 
            ON formelem.form_id=form.form_id
        LEFT JOIN t_question_language queslang 
            ON queslang.formelement_id=formelem.id
        WHERE formelem.id=? AND form.is_delete<>1 
            AND formelem.is_delete<>1 AND queslang.f_isdelete<>1'
);

/*MK Edit is not retained 
define(
    'GET_BY_QUESTION_OPTION_ID',"SELECT 
    formelem.id,formelem.`type`,formelem.icon,formelemopt.id as formelemoptid,
    formelemopt.min_value,formelemopt.max_value,
    formelemopt.range_points,formelemoptlang.options,formelemoptlang.id 
    as optlangid,
    formelemoptlang.language_id,formelemproperty.formelement_option
    ,formelemoptlang.optionid,formelemproperty.property_id as formelempropid
    FROM t_form_element formelem 
    LEFT OUTER JOIN t_formelement_options formelemopt ON 
    formelemopt.r_form_elementid=formelem.id
    LEFT OUTER JOIN t_formelem_option_language formelemoptlang ON 
    formelemoptlang.optionid=formelemopt.id
    LEFT OUTER JOIN t_formelment_property formelemproperty ON 
    formelemproperty.r_formelement_id=formelem.id
    WHERE formelem.id=? AND (formelem.is_delete<>1 OR formelem.is_delete IS NULL)
    AND (formelemopt.f_isdelete<>1 OR formelemopt.f_isdelete IS NULL)  
    AND (formelemproperty.f_isdelete<>1 OR formelemproperty.f_isdelete IS NULL)"
);
*/
define(
    'GET_BY_QUESTION_OPTION_ID', 'SELECT 
            formelem.id,formelem.`type`,formelem.icon,
            formelemopt.id as formelemoptid,formelemopt.min_value,
            formelemopt.max_value,
            formelemopt.range_points,formelemoptlang.options,
            formelemoptlang.id as optlangid,
            formelemoptlang.language_id,formelemproperty.formelement_option,
            formelemoptlang.optionid,
            formelemproperty.property_id as formelempropid
            FROM t_form_element formelem 
            LEFT OUTER JOIN t_formelement_options formelemopt 
                ON ( formelemopt.r_form_elementid=formelem.id  AND
                (formelemopt.f_isdelete<>1 OR formelemopt.f_isdelete IS NULL) )
            LEFT OUTER JOIN t_formelem_option_language formelemoptlang 
                ON formelemoptlang.optionid=formelemopt.id 
            LEFT OUTER JOIN t_formelment_property formelemproperty ON 
                ( formelemproperty.r_formelement_id=formelem.id
                AND (formelemproperty.f_isdelete<>1 
                OR formelemproperty.f_isdelete IS NULL) )
            WHERE formelem.id=? AND (formelem.is_delete<>1 
            OR formelem.is_delete IS NULL) '
);

define(
    'DELETE_BY_QUESTIONNAIRE_ID', 'UPDATE `t_form` SET is_delete = 0 
    WHERE `form_id`=?'
);

define(
    'UPDATE_QUESTIONNAIRE', 'UPDATE t_form SET form_name =? 
    WHERE `form_id`=?'
);

define(
    'UPDATE_QUESTION', 'UPDATE t_form_element SET form_text=? 
    WHERE `form_id`=?'
);

define(
    'INSERT_FORM_QUESTION', 'INSERT `t_form`(form_name,r_quesphase_id,
        r_quesgroup_id,r_topic_id,created_date,created_by) VALUES (?,?,?,?,?,?)'
);

define(
    'UPDATE_FORM_QUESTION', 'UPDATE t_form SET form_name=?,
    modified_Date=?,created_by=?,r_topic_id=? WHERE form_id=?'
);

define(
    'INSERT_MONITOR_VARIABLE', 'INSERT `t_monitor_variable`(name) VALUES (?)'
);

define(
    'GET_FORM_EXTST', 'SELECT form_id FROM `t_form` WHERE form_name=?'
);

define(
    'GET_FORM_EXTST_BY_ID', 'SELECT form_id FROM `t_form` WHERE form_id=?'
);

define(
    'GET_FORMELEMENT_EXTST', 'SELECT id FROM `t_monitor_variable` WHERE name=?'
);

define(
    'GET_FORMELEMENT_MV_EXIST', 'SELECT monitorvariable_id FROM 
    `t_formelement_monitorvariable` WHERE formelement_id=?'
);

define(
    'INSERT_FORMELEMONITORVARIABLE', 'INSERT INTO 
    t_formelement_monitorvariable(formelement_id,monitorvariable_id) 
    VALUES(?,?)'
);

define(
    'INSERT_QUESTION', 'INSERT `t_form_element`
    (form_id,form_text,icon,activitylink,created_date,created_by,
    type,ui_order,score) VALUES (?,?,?,?,?,?,?,?,?)'
);

define(
    'INSERT_QUESTION_LANGUAGE', 'INSERT `t_question_language`(formelement_id,
    question,language_id,question_info,createdon) VALUES (?,?,?,?,?)'
);

define(
    'UPDATE_QUESTION_LANGUAGE', 'UPDATE `t_question_language` 
    SET formelement_id=?,question=?,language_id=?,question_info=?,modifiedon=? 
    WHERE id=?'
);

define(
    'INSERT_QUESTION_OPTION_PROPERTY', 'INSERT `t_formelment_property`
    (r_formelement_id,formelement_option,createdon) VALUES (?,?,?)'
);

define(
    'UPDATE_QUESTION_OPTION_PROPERTY', 'UPDATE `t_formelment_property` 
    SET r_formelement_id=?,formelement_option=?,modifiedon=? 
    WHERE property_id=?'
);

define(
    'UPDATE_QUESTION_OPTION_PROPERTY_QUESTYPE', 'UPDATE `t_formelment_property` 
    SET f_isdelete=1 WHERE r_formelement_id=?'
);

define(
    'INSERT_QUESTION_OPTION', 'INSERT `t_formelement_options`
    (r_form_elementid,answer_option,min_value,max_value,range_points,
    createdon,ui_order) VALUES (?,?,?,?,?,?,?)'
);

define(
    'UPDATE_QUESTION_OPTION_QUESTYPE', 'UPDATE `t_formelement_options` 
    SET f_isdelete=1 WHERE r_form_elementid=?'
);

define('GETOPTION_BYID', 'SELECT * FROM `t_formelement_options` WHERE id=?');

define(
    'UPDATE_QUESTION_OPTION', "UPDATE `t_formelement_options` 
    SET r_form_elementid=?,min_value=?,max_value=?,range_points=?,modifiedon=?,
    f_isdelete='0' WHERE id=?"
);

define(
    'INSERT_QUESTION_OPTION_LANG', 'INSERT `t_formelem_option_language`
    (optionid,options,language_id,createdon) VALUES (?,?,?,?)'
);

define(
    'UPDATE_QUESTION_OPTION_LANG', 'UPDATE `t_formelem_option_language` 
    SET optionid=?,options=?,language_id=?,modifiedon=? WHERE id=?'
);

define(
    'UPDATE_QUESTION_OPTION_LANG_QUESTYPE', 'UPDATE t_formelem_option_language 
    SET f_isdelete=1 WHERE optionid IN(SELECT  id FROM t_formelement_options 
    WHERE r_form_elementid=?)'
);

define(
    'INSERT_QUES_PHASE_RANGE', 'INSERT `t_ques_phase_range`
    (r_phase_id,r_group_id,max_range,min_range,created_date,r_topic_id)
    VALUES (?,?,?,?,?,?)'
);

define(
    'UPADTE_QUES_PHASE_RANGE', 'UPDATE `t_ques_phase_range` 
    SET r_phase_id=?,r_group_id=?,max_range=?,min_range=?,modified_date=?,
    r_topic_id=?  WHERE id=?'
);

define(
    'GET_BY_PHASE_RANGE_ID', 'SELECT *,
        phaserange.id as phaserangeid FROM t_ques_phase_range phaserange 
        LEFT OUTER JOIN t_ques_phase_range_language phaserangelang 
            ON phaserangelang.r_ques_phase_range_id=phaserange.id 
        WHERE phaserange.id=? and phaserangelang.f_isdelete != 1 and 
        phaserange.f_isdelete != 1 '
);

define(
    'INSERT_QUES_PHASE_WEIGHT', 'INSERT `t_ques_phase_weightage`(r_phase_id,
    r_group_id,index_weightage,max_weightage,createdon) VALUES (?,?,?,?,?)'
);

define(
    'UPDATE_QUES_PHASE_WEIGHT', 'UPDATE `t_ques_phase_weightage` 
    SET r_phase_id=?,r_group_id=?,index_weightage=?,max_weightage=?,
    modifiedon=? WHERE id=?'
);

define(
    'GET_QUES_PHASE_WEIGHTAGE', 'SELECT * FROM t_ques_phase_weightage phaseweightage 
        WHERE phaseweightage.r_phase_id=? AND phaseweightage.r_group_id=? 
        AND phaseweightage.f_isdelete<>1'
);

define(
    'INSERT_PHASE_QUES_ACTIVITY', 'INSERT `t_ques_phase_activity`
    (ques_activity_id,phase_id,group_id,createdon,topic_id) VALUES (?,?,?,?,?)'
);

define(
    'UPDATE_PHASE_QUES_ACTIVITY', 'UPDATE `t_ques_phase_activity` 
    SET ques_activity_id=?,phase_id=?,group_id=?,modifiedon=?,topic_id=? WHERE id=?'
);

define(
    'INSERT_PHASE_QUES_ACTIVITY_POINTS', 'INSERT `t_ques_activity_points`
    (r_ques_phase_activity_id,weekcredits,perweek,createdon) VALUES (?,?,?,?)'
);

define(
    'UPDATE_PHASE_QUES_ACTIVITY_POINTS', 'UPDATE `t_ques_activity_points` 
    SET r_ques_phase_activity_id=?,weekcredits=?,perweek=?,activity_seconds=?,
    modifiedon=? WHERE id=?'
);

define(
    'INSERT_QUES_PHASE_RANGE_LANGUAGE', 'INSERT `t_ques_phase_range_language`
    (r_ques_phase_range_id,languageid,text_type,text,createdon) VALUES (?,?,?,?,?)'
);

define(
    'UPADTE_QUES_PHASE_RANGE_LANGUAGE', 'UPDATE `t_ques_phase_range_language` 
    SET f_isdelete=1,modifiedon=? WHERE r_ques_phase_range_id=?'
);

define(
    'UPADTE_QUESTION', 'UPDATE `t_form_element` 
    SET form_id=?,form_text=?,activitylink=?,modified_Date=?,type=?,
    icon=?,ui_order=?,score=? WHERE id=?'
);

define(
    'DELETE_BY_QUESION_ID', 'UPDATE `t_form_element` SET is_delete=1
    WHERE id=?'
);

define(
    'GET_QUESTION_OPTION', 'SELECT id FROM t_formelement_options 
    WHERE r_form_elementid=?'
);

define(
    'UPDATE_QUESTION_OPTION_VALUES', 'UPDATE t_formelement_options 
            SET f_isdelete=1 WHERE r_form_elementid=?'
);
/*define(
    'GET_STREGTH_PROGRAM_BY_COMPANY', "SELECT SQL_CALC_FOUND_ROWS *,
       IF(type=1,'Rep',IF(type=2,'Circuit','Point Circuit')) as type 
       FROM `t_strength_program` HAVING 1=1 AND `r_company_id`=? 
       AND `is_deleted`=0 "
);
*/
/* SK- changes */

define(
    'GET_STRENGTH_PROGRAM_EXIST', 'SELECT * FROM `t_strength_program` 
    WHERE `strength_program_id`=?'
);

define(
    'GET_STREGTH_PROGRAM_BY_COMPANY', "SELECT SQL_CALC_FOUND_ROWS 
    sp.strength_program_id as programid,sp.description as description,
    IF(sp.type=1,'Rep',IF(sp.type=2,'Circuit','Point Circuit')) as type 
    FROM t_strength_program sp 
    WHERE sp.r_company_id= ? AND sp.`is_deleted`=0  HAVING 1=1 "
);

define(
    'GET_STRENGTH_PROGRAM_DEFAULT_ID', 'SELECT  sp.strength_program_id as programid 
    FROM t_strength_program sp
    WHERE sp.r_company_id= ? AND sp.`is_deleted`=0 AND f_isdefault=1'
);

define(
    'GET_STRENGTH_PROGRAM_TRAINING_EXIST', 'SELECT * FROM 
    `t_strength_program_training` 
    WHERE `strength_program_training_id`=? 
    AND `r_strength_program_id`=? AND `is_deleted`=0'
);

define(
    'GET_STRENGTH_PROGRAM_TRAINING_BY_ID', 'SELECT * 
    FROM `t_strength_program_training` 
    WHERE `strength_program_training_id`=?'
);

/*define(
    'GET_STRENGTH_PROGRAM_TRAINING_LIST', "SELECT SQL_CALC_FOUND_ROWS * FROM 
        `t_strength_program_training` WHERE `r_strength_program_id`=? AND 
        `is_deleted`=0 ORDER BY `training_week` ASC"
);*/
/* SK- changes */
define(
    'GET_STRENGTH_PROGRAM_TRAINING_LIST', 'SELECT SQL_CALC_FOUND_ROWS 
    sp.strength_program_training_id  as strength_program_training_id,
    sp.training_week as tweek,sp.series as series,sp.reputation as reputation,
    sp.time as time,sp.series as series,sp.rest as rest,
    strength_percentage as strengthpercentage  
    FROM `t_strength_program_training` sp
        WHERE sp.`r_strength_program_id`=? AND sp.`is_deleted`=0 
        ORDER BY sp.`training_week` ASC'
);

/*MK Added Jan 07 2016 - Master Sequirity Questions*/

define(
    'GET_SECURITY_QUESTIONS', 'SELECT * FROM t_secure_question'
);

/* LANGUAGE QUERIES **/

define(
    'GET_LANGUAGE_TYPE', 'SELECT SQL_CALC_FOUND_ROWS * FROM `t_languages` 
    WHERE f_isdelete<>1 ORDER BY `title` ASC'
);
define(
    'GET_LANGUAGE_ACTIVE_TYPE', 'SELECT SQL_CALC_FOUND_ROWS * FROM `t_languages` 
    WHERE active=1 AND f_isdelete<>1 ORDER BY `title` ASC'
);
define(
    'EDIT_LANGUAGE_TYPE', 'SELECT * FROM `t_languages` WHERE `language_id`=?'
);
define(
    'EDIT_TRANSLATION_TYPE', 'SELECT trans.translation_id,
    transaction_key_desc,translation_text,trans.r_translation_key as transkey,
    trans.r_language_id as languageid,title,trans.created_on as createdon,
    trans.modified_on as modifiedon FROM t_translations trans 
    LEFT JOIN t_translation_key tkey 
        ON tkey.translationkey_id=trans.r_translation_key 
        LEFT JOIN t_languages tlang ON tlang.language_id=trans.r_language_id 
        WHERE `translation_id`=?'
);
define(
    'GET_TRANSLATIONKEY_TYPE', 'SELECT * FROM `t_translation_key` 
            ORDER BY `translation_key` ASC'
);
define(
    'GET_TRANSLATIONLANGUAGE_TYPE', 'SELECT trans.translation_id,
        transaction_key_desc,trans.r_translation_key as transkey,
        trans.r_language_id as languageid,translation_text,title,
        trans.created_on as createdon,trans.modified_on as modifiedon
            FROM t_translations trans 
            LEFT JOIN t_translation_key tkey 
                ON tkey.translationkey_id=trans.r_translation_key 
            LEFT JOIN t_languages tlang 
                ON tlang.language_id=trans.r_language_id 
            WHERE trans.f_isdelete<>1 
            ORDER by transaction_key_desc, r_language_id'
);
define(
    'GET_TRANSLATION', 'SELECT * FROM t_translations trans
      WHERE trans.r_translation_key=? '
);
define(
    'GET_TRANSLATIONKEY_BY_ID', 'SELECT * FROM t_translation_key 
        WHERE translationkey_id=?'
);
define(
    'GET_TRANSLATION_BY_ID', 'SELECT * FROM t_translations 
        WHERE r_translation_key=? AND r_language_id=?'
);
define(
    'SET_LANGUAGE_PROGRAM', 'INSERT INTO `t_translations`
    (r_translation_key,r_language_id,translation_text,created_on)
    VALUES(?,?,?,NOW())'
);
define(
    'SET_LANGUAGE', 'INSERT INTO `t_languages`(title,active,created_on)
    VALUES(?,?,NOW())'
);
define(
    'UPDATE_LANGUAGE', 'UPDATE `t_languages` SET title =?,active=?,
    modified_on=NOW() WHERE `language_id`=?'
);
define(
    'UPDATE_TRANSLATION', 'UPDATE `t_translations` 
    SET r_translation_key =?,r_language_id=?,translation_text=?,
    modified_on=NOW() WHERE `translation_id`=? '
);
define(
    'DELETE_BY_LANGUAGE_ID', 'UPDATE `t_languages`SET f_isdelete=1
    WHERE `language_id`=?'
);
define(
    'DELETE_BY_TRANSLATION_ID', 'UPDATE `t_translations` SET f_isdelete=1 
    WHERE `translation_id`=?'
);
define(
    'GET_LANGUAGE_DETAILS', 'SELECT tlang.language_id,tkey.translationkey_id,
        trans.translation_text FROM t_translations trans 
        LEFT JOIN t_translation_key tkey 
            ON tkey.translationkey_id=trans.r_translation_key 
        LEFT JOIN t_languages tlang 
            ON tlang.language_id=trans.r_language_id 
        WHERE trans.f_isdelete<>1'
);

/* PAGE CONTENT QUERIES - Added by PK**/
define('GET_PAGE_NAME', 'SELECT * FROM `t_page`');

define(
    'INSERT_PAGE_CONTENT', 'INSERT `t_pagecontent_language`
    (t_pageid,language_id,content,createdon,modifiedon) 
    VALUES(?,?,?,NOW(),NOW())'
);

define(
    'UPDATE_PAGE_CONTENT', 'UPDATE `t_pagecontent_language`
    SET f_isdelete=1 WHERE t_pageid=?'
);

define(
    'GET_PAGECONTENT_EXIT', 'SELECT t_pageid  FROM t_pagecontent_language 
    WHERE t_pageid=? AND f_isdelete<>1'
);

/*SN - Added Client Page content*/
define(
    'GET_PAGECONTENT_TYPE', 'SELECT p.page_id,pl.id,pl.content,p.page_name,
        l.title,pl.createdon,pl.modifiedon,pl.language_id as languageid 
        FROM t_pagecontent_language pl 
        LEFT OUTER JOIN t_page p ON pl.t_pageid=p.page_id 
        LEFT OUTER JOIN t_languages l 
            ON l.language_id=pl.language_id 
        WHERE pl.language_id=? AND 
        pl.f_isdelete<>1 AND p.f_isdelete<>1'
);

define(
    'GET_LANGUAGE_PAGECONTENT', 'SELECT p.page_id,pl.id,pl.content,p.page_name,
        l.title,pl.createdon,pl.modifiedon,pl.language_id as languageid 
            FROM t_pagecontent_language pl 
            LEFT OUTER JOIN t_page p ON pl.t_pageid=p.page_id 
            LEFT OUTER JOIN t_languages l ON l.language_id=pl.language_id 
            WHERE  pl.f_isdelete<>1 AND p.f_isdelete<>1'
);

define(
    'GET_BY_PAGECONT_ID', 'SELECT * FROM t_page page 
    LEFT OUTER JOIN t_pagecontent_language pgcont 
        ON (pgcont.t_pageid=page.page_id AND pgcont.f_isdelete<>1) 
    WHERE page.page_id=?'
);

define(
    'GET_QUES_PHASE_GROUP_ACTIVITY_POINTS', 'SELECT 
        phaseact.id,phaseact.ques_activity_id,phaseact.phase_id,
        phaseact.group_id,phaseactpoint.id as actpointid,
        phaseactpoint.weekcredits,phaseactpoint.perweek,
        phaseactpoint.activity_seconds FROM t_ques_phase_activity phaseact 
        LEFT OUTER JOIN t_ques_activity_points phaseactpoint
            ON phaseactpoint.r_ques_phase_activity_id=phaseact.id 
        WHERE phaseact.phase_id=? AND phaseact.group_id=?'
);

/**Questionary Hints- Added By PK **/
define(
    'GET_PHASE', 'SELECT phase_id,phase_name FROM t_ques_phase 
    WHERE options=2 AND is_delete<>1'
);
define(
    'GET_GROUPLIST', 'SELECT group_id,group_name FROM 
    t_ques_group WHERE is_delete<>1'
);
define(
    'GET_QUESTIONHINTS', 'SELECT quest.question FROM t_form form  
        LEFT JOIN t_question_language quest 
            ON quest.formelement_id=form.r_quesphase_id
        LEFT JOIN t_form_element element 
            ON element.form_id=form.r_quesphase_id 
        WHERE `r_quesgroup_id`=1 AND `r_quesphase_id`=1 AND form.is_delete<>1'
);

define(
    'INSERT_QUES_HINTS', 'INSERT INTO `t_ques_hints`
    (r_question_id,r_phase_id,r_group_id,no_days,created_on) 
    VALUES(?,?,?,?,?)'
);

define(
    'UPDATE_QUES_HINTS', 'UPDATE t_ques_hints 
    SET r_question_id=?,r_phase_id=?,r_group_id=?,no_days=?,
    modified_on=? WHERE t_ques_hint_id=?'
);

define(
    'DELETE_BY_QUESION_HINT_ID', ' UPDATE t_ques_hints 
    SET f_isdelete=1 WHERE t_ques_hint_id=?'
);

define(
    'INSERT_QUES_HINT_LANG', 'INSERT INTO t_ques_hints_lang(r_ques_hint_id,
    t_language_id,t_lang_hint,created_on) VALUES(?,?,?,?)'
);

define(
    'UPDATE_QUES_HINT_LANG', 'UPDATE t_ques_hints_lang 
        SET r_ques_hint_id=?,t_language_id=?,t_lang_hint=?,modified_on=? 
        WHERE id=?'
);

define(
    'GET_QUESTION_HINTS', 'SELECT queshint.t_ques_hint_id,queshint.no_days,
        queshintlang.t_lang_hint,grp.group_name,phase.phase_name,
        queshint.created_on FROM t_ques_hints  queshint
            LEFT OUTER JOIN  t_ques_hints_lang queshintlang 
                ON queshintlang.r_ques_hint_id=queshint.t_ques_hint_id
            LEFT OUTER JOIN t_ques_group grp 
                ON grp.group_id=queshint.r_group_id
            LEFT OUTER JOIN t_ques_phase phase 	
                ON phase.phase_id=queshint.r_phase_id
            WHERE queshintlang.t_language_id=? AND queshint.f_isdelete<>1 
            ORDER BY queshint.r_phase_id,queshint.r_group_id,queshint.no_days'
);

define(
    'GET_BY_QUESTION_HINT_ID', 'SELECT hint.t_ques_hint_id as queshintid,
    hint.r_question_id as quesid,hint.r_phase_id as phaseid,
    hint.r_group_id as groupid,hint.no_days as nodays,
    hintlang.id as hintlangid,hintlang.t_language_id as
    langid,hintlang.t_lang_hint as hinttxt FROM t_ques_hints hint 
    LEFT OUTER JOIN t_ques_hints_lang hintlang 
        ON hintlang.r_ques_hint_id=hint.t_ques_hint_id
    WHERE hint.t_ques_hint_id=? AND hint.f_isdelete<>1'
);

define(
    'GET_FITLEVEL_ID', 'SELECT testparm.r_user_test_id as testid,
    testparm.fitness_level as fitlevel,testparm.iant_hr,
    testparm.iant_p,testparm.age_on_test,testparm.weight_on_test as weight,
    cardio.f_trainingstdate,cardio.f_trainingeddate,cardiotrainplan.f_weeknr,
    cardiotrainplan.f_points, cardio.f_cardiotrainingid, cardio.f_trainingtype, 
    cardiotrainplan.f_cardiotrainingplanid, cardiotrainplan.f_min, 
    cardiotrainplan.f_max, cardiotrainplan.f_min_a, cardiotrainplan.f_max_a 
    FROM t_cardiotraining cardio 
    LEFT OUTER JOIN t_cardiotrainingplan cardiotrainplan 
        ON cardiotrainplan.f_cardiotrainingid=cardio.f_cardiotrainingid
    LEFT OUTER JOIN t_user_test_parameter testparm 
        ON testparm.r_user_test_id=cardio.f_testid
    WHERE testparm.r_user_id=? and cardio.f_isdelete != 1 
    and cardiotrainplan.f_isdelete != 1 '
);
define(
    'GET_QUESTION_HINTS_LANG', 'SELECT lang.t_lang_hint,hint.no_days 
        FROM t_ques_hints hint 
        LEFT OUTER JOIN t_ques_hints_lang lang 
            ON lang.r_ques_hint_id=hint.t_ques_hint_id 
        WHERE hint.r_question_id=? AND hint.f_isdelete<>1'
);

define('GET_RESET_USERS', 'SELECT * FROM t_users usr WHERE usr.user_id IN(?)');
define(
    'GET_CLUBSETTINGS_VLAUE', 'SELECT * FROM t_club_settings 
    WHERE is_deleted<>1 AND r_clubid=? AND settings_key=? 
        ORDER BY modified_dt DESC LIMIT 0,1'
);
/*MK Calendar Settings*/
//r_company_id=? AND type IN circuit,point circute/mixed
define(
    'GET_PROGRAMS_LIST', 'SELECT * FROM t_strength_program 
    WHERE is_deleted<>1 AND clubid=? AND type IN (2,3)'
); 
define(
    'GET_CALENTER_DETAILS', 'SELECT calendar.id,calendar.text,
    calendar.text,calendar.r_program_id as programid,
    calendar.max_members,calendar.start_period as start_date,
    calendar.end_period AS end_date 
    FROM t_calender calendar
    WHERE calendar.is_deleted<>1 AND calendar.r_user_id=?'
);
define('GET_CALENDAR_BYID', 'SELECT * FROM t_calender WHERE id=?');
/*SN addded Education*/
define('GET_EDUCATION_BY_ID', 'SELECT * FROM t_education WHERE education_id=?');
define(
    'GET_EDUCATION_LANGUAGE_BYID', 'SELECT * FROM t_education_lang 
    WHERE edulangid=?'
);
define(
    'GET_EDUCATION_GROUP', 'SELECT * FROM t_education edu
        LEFT OUTER  JOIN t_education_lang edulang
            ON edulang.r_edu_id=edu.r_group_id
        WHERE edu.r_group_id=? AND edulang.r_lang_id=?'
);
define(
    'GET_EDUCATION_LANGUAGE_DESC_BYID', 'SELECT * 
        FROM t_language_description 
        WHERE language_desc_id=?'
);
define(
    'GET_EDUCATION_BY_GROUP', 'SELECT edu.education_id as eduid,
        edu.index_val as indextype,edu.day as days,
        langdesc.languagae_desc as info,
        langdesc.r_language_id as langid,
        langdesc.language_desc_id langdescid FROM t_education edu
        LEFT OUTER JOIN t_language_description langdesc 
            ON langdesc.ref_id=edu.education_id
        WHERE edu.r_group_id=?'
);

/*PK Get Goal Details*/
define(
    'GET_GOAL', 'SELECT goal.id as goalid,goal.goal_name,
        topicgl.topic_id,formtopic.topic_name,topicgl.value
        FROM t_goal goal 
        LEFT OUTER JOIN t_topic_goal topicgl 
            ON topicgl.goal_id=goal.id
        LEFT OUTER JOIN t_form_topics formtopic 
            ON formtopic.topic_id=topicgl.topic_id 
            ORDER BY GOAL_name, topic_name'
);
define(
    'UPDATE_GOAL', 'UPDATE t_topic_goal SET value=? 
    WHERE goal_id=? AND topic_id=?'
);
