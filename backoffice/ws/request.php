<?php
/**
 * PHP version 5.
 
 * @category General
 
 * @package WSRequest
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description handle webservice request details.
 */
class request
{
    public $get = array();
    public $post = array();

    public $class;
    public $method;
    public $params;
    public $format;
    public $apikey;

    /**
     * Class constructor.
     */
    public function __construct()
    {

        //Clean the global $_GET and post $_POST
        $this->get = $this->clean($_GET);
        $this->post = $this->clean($_POST);

        //Initialize the variables
        $this->class = '';
        $this->method = '';
        $this->params = array();
        $this->format = 'xml';
        $this->apikey = '39802830831bed188884e193d8465226';

        //Split the url parameters
        $this->splitUrl();
    }

    /**
     * Returns the request method    
     * return string.
     */
    public function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Returns the referer    
     * return string.
     */
    public function getReferer()
    {
        return $_SERVER['HTTP_REFERER'];
    }

    /**
     * Returns the values based on global $_GET key or the global $_GET array
     * param string
     * return mixed.
     * @param string $key
     * @return array object|mixed
     */
    public function get($key = '')
    {
        return isset($this->get[$key]) ?  $this->get[$key] : $this->get;
    }

    /**
     * Returns the values based on global $_POST key or the global $_POST array
     * param string
     * return mixed.
     * @param string $key
     * @return array object|mixed
     */
    public function post($key = '')
    {
        return isset($this->post[$key]) ?  $this->post[$key] : $this->post;
    }

    /**
     * Returns the necessary class
     * return string.
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Returns the necessary method
     * return string.
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Returns the necessary parameters
     * return string.
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Returns the output format
     * return string.
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Returns the output format
     * return string.
     */
    public function getApiKey()
    {
        return $this->apikey;
    }

    /**
     * Split the parameters and assign the values to necessary variables.
     */
    public function splitUrl()
    {
        switch ($this->getRequestMethod()) {
        case 'GET':
            $this->class = isset(
                $this->get['mod']
            ) ? $this->get['mod'] : ''; unset($this->get['mod']);
            $this->method = isset(
                $this->get['method']
            ) ? str_replace('_', '', $this->get['method']) : ''; unset($this->get['method']);
            $this->format = isset(
                $this->get['format']
            ) ? $this->get['format'] : $this->format; unset($this->get['format']);
            $this->apikey = isset(
                $this->get['apikey']
            ) ? $this->get['apikey'] : $this->apikey;
            $this->params = $this->get;
            break;
        case 'POST':
            $this->class = isset(
                $this->post['mod']
            ) ? $this->post['mod'] : ''; unset($this->post['mod']);
            $this->method = isset(
                $this->post['method']
            ) ? str_replace('_', '', $this->post['method']) : ''; unset($this->post['method']);
            $this->format = isset(
                $this->post['format']
            ) ? $this->post['format'] : $this->format; unset($this->post['format']);
            $this->apikey = isset(
                $this->post['apikey']
            ) ? $this->post['apikey'] : $this->apikey;
            $this->params = $this->post;
            break;
        }
    }

    /**
     * Clean the input data
     * param mixed $data
     * return mixed $data.
     * @param $data
     * @return mixed
     */
    public function clean($data)
    {
        /*if (is_array($data)) {
              foreach ($data as $key => $value) {
                unset($data[$key]);
                
                $data[$this->clean($key)] = $this->clean($value);
              }
        } else { 
              $data = htmlspecialchars($data, ENT_COMPAT, 'UTF-8');
        }*/

        return $data;
    }

    /**
     * Check the wheather the input data is serialised
     * param mixed $data
     * return boolean.
     * @param $data
     * @return bool
     */
    public function is_serialized($data)
    {
        if (!is_string($data)) {
            return false;
        }

        $data = trim($data);

        if ('N;' == $data) {
            return true;
        }

        if (!preg_match('/^([adObis]):/', $data, $badions)) {
            return false;
        }

        switch ($badions[1]) {
        case 'a' :
        case 'O' :
        case 's' :

            if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data)) {
                return true;
            }

            break;
        case 'b' :
        case 'i' :
        case 'd' :

            if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data)) {
                return true;
            }

            break;
        }

        return false;
    }
}
