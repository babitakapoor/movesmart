<?php
/**
 * PHP version 5.
 
 * @category General
 
 * @package APICall
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description functions to handle api calls.
 */
// Include required general files.

set_time_limit(0);
ini_set('max_execution_time', 0);
require_once '../include/dbconfig.php';
//require_once '../include/define.php';
require_once 'config.php';
require_once DIR_DB_CLASS.'adodb.inc.php';
require_once 'db.php';
require_once 'request.php';
require_once 'response.php';
require_once 'rest.php';
require_once 'library'.DS.'ssp.class.php';

// Include required module files.
$directory = opendir(MODULE_PATH);
while (false !== ($file = readdir($directory))) {
    if (substr($file, -4) == '.php') {
        include_once MODULE_PATH.DS.$file;
    }
}
closedir($directory);
//End

class api extends rest
{
    public $data = '';
    public $request = '';
    public $response = '';
    public $dbcon = '';

    public function __construct()
    {
        parent::__construct();    // Init parent contructor	

        $db_con = new Db();
        $this->dbcon = $db_con->getConnection();

        $this->request = new Request();
        $this->response = new Response();
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string	
     */
    public function processApi()
    {

        //starting adapding the class and method...
        $class = ucfirst($this->request->getClass());
        $method = $this->request->getMethod();
        $params = $this->request->getParams();
        $format = $this->request->getFormat();
        $apikey = $this->request->getApiKey();

        if ($this->authRequest($apikey)) {
            $class_file = MODULE_PATH.DS.$this->request->getClass().'.php';
			
            //Check the class file
            if (file_exists($class_file)) {
                //Check the class
                $class   .= 'Model';
                if (class_exists($class)) {
                    // Need to create instance for the corresponding class.	
                    $service = new $class($this->dbcon);
                    // check if method is exists
					if (method_exists($service, $method)) {
						$result = $service->$method($params);
                    } else {
                        $result = $this->getStatus('error', 404, 'Method not found');
                    }//End method exists
                } else {
                    $result = $this->getStatus('error', 404, 'Class not found');
                }//End class_exists
            } else {
                $result = $this->getStatus('error', 404, 'Class file not found');
            }//End file_exists
        } else {
            $result = $this->getStatus('error', 601, 'Authentication failed.');
        }//End authRequest
		//echo $format;die;
		$this->response->render($result, $format);
    }

    /**
     *
     */
    public function listenRequest()
    {
    }

    /**
     * TODO: Need to implement the actual logic.
     * @param null $apikey
     * @return bool
     */
    public function authRequest($apikey = null)
    {
        if ($apikey) {
            //Need to implement
        }

        return true;
    }

    /**
     * TODO : Need to implement in general.
     * @param $status
     * @param string $status_code
     * @param string $status_message
     * @return array object
     */
    public function getStatus($status, $status_code = '', $status_message = '')
    {
        $status = array(
            'response' => array(
                'status' => $status,
                'status_code' => $status_code,
                'status_message' => $status_message,
            ),
        );

        return $status;
    }
}
// Initiiate Library

$api = new API();
$api->processApi();
