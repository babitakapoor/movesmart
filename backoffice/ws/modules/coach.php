<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Coach
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle coach related actions.
 */
require_once SQL_PATH.DS.'coach.php';
/**
 * Class to handle Coach Related Functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class CoachModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
    }

    /**
    * Add / Update A Coach Details for a company.
    *
    * @param array $params service parameter, Keys:CompanyId(company_id),UserId
    *
    * @return array object
    */
    
    public function getCoachAddOrUpdate($params)
    {
	
	  
	$workstart = date("Y-m-d", strtotime($params['workStart']));
	$workend =isset($params['workEnd']) && $params['workEnd']!='' ? date("Y-m-d", strtotime($params['workEnd'])) : '0000-00-00';
	
        $company_id = isset($params['company_id']) ? $params['company_id'] : '';
        $email = isset($params['email']) ? $params['email'] : '';
        $dateTime = date('Y-m-d H:i:s');
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $data = array(
                'r_usertype_id' => isset($params['user_type_id']) ? 
                    $params['user_type_id'] : '',
                'r_company_id' => $company_id,
                'r_club_id' => isset($params['club_id']) ? $params['club_id'] : '',
                'r_status_id' => isset($params['status_id']) ? 
                    $params['status_id'] : '',
                'first_name' => isset($params['first_name']) ? 
                    $params['first_name'] : '',
                'middle_name' => isset($params['middle_name']) ? 
                    $params['middle_name'] : '',
                'last_name' => isset($params['last_name']) ? 
                    $params['last_name'] : '',
                'gender' => isset($params['gender']) ? $params['gender'] : '',
                'email' => $email,
                'username' => isset($params['username']) ?
                    $params['username'] : '',
                'password' => isset($params['password']) ?
                    $params['password'] : '',
                'person_id' => isset($params['person_id']) ?
                    $params['person_id'] : '',
                'created_by' => '1',
                'created_date' => $dateTime,
                'modified_by' => '1',
                'modified_date' => $dateTime,
				'relation_coach' => isset($params['relation_coach']) ? $params['relation_coach'] : '',
        );
		
		
                $r_city_id = isset($params['city']) ? $params['city'] : '';
                $doorno = isset($params['doorNo']) ? $params['doorNo'] : '';
                $address = isset($params['address']) ? $params['address'] : '';
                $zipcode = isset($params['zipCode']) ? $params['zipCode'] : '';
                $bus = isset($params['bus_no']) ? $params['bus_no'] : '';
                $phone = isset($params['phone']) ? $params['phone'] : '';
                $mobile = isset($params['mobile']) ? $params['mobile'] : '';
                $company = isset($params['company_id']) ? $params['company_id'] : '';
                $location = isset($params['city']) ? $params['city'] : '';
                $securityno = isset($params['securityNo']) ? $params['securityNo'] : '';
                $gsm = isset($params['mobile']) ? $params['mobile'] : '';
		
		
		
		
		
            //echo mysql_affected_rows();
		
		
        $rsobj = $this->dbcon->Execute(GET_COACH_USERID, array($userId));
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
			
            $this->dbcon->Execute($rsUpdates);
			$str="UPDATE `t_personalinfo` SET `r_city_id`='".$r_city_id."',`doorno`='".$doorno."',`address`='".$address."',`zipcode`='".$zipcode."',`bus`='".$bus."',`phone`='".$phone."',`mobile`='".$mobile."',`company`='".$company."',`location`='".$location."',`securityno`='".$securityno."',`work_starton`='".$workstart."',`work_endon`='".$workend."',`modified_by`=1,`modified_date`='".$dateTime."',`gsm`='".$gsm."' where `r_user_id`='".$userId."'"; 
		
	
			mysql_query($str);
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Users successfully Updated.',
                'user_id' => $rsobj->fields['user_id'],
                // 'sql' => $rsobj->sql,
                'userId' => $userId,
            );
        } else {
           
            /*SM Added MD5 converstionissue*/
            $data['uid'] = md5(date('Y-m-dh:m:s'));

            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
          
            $this->dbcon->Execute($rsInserts);
            $userId = $this->dbcon->Insert_ID();
			
			$str="INSERT INTO `t_personalinfo` (`r_city_id`,`doorno`,`address`,`zipcode`,`bus`,`phone`,`mobile`,`company`,`location`,`securityno`,`work_starton`,`work_endon`,`created_by`,`created_date`,`gsm`,`r_user_id`) VALUES ('".$r_city_id."','".$doorno."','".$address."','".$zipcode."','".$bus."','".$phone."','".$mobile."','".$company."','".$location."','".$securityno."','".$workstart."','".$workend."',1,'".$dateTime."','".$gsm."','".$userId."')"; 
			 
			
			mysql_query($str);

            $uid = md5($userId.'_'.$company_id.'_'.$params['club_id'].'_'.$email);
            $this->dbcon->Execute(
                "UPDATE `t_users` SET `uid`='$uid' WHERE `user_id`='$userId'"
            );
            //Update user uid ends

            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Users successfully Added.',
                'user_id' => $userId,
                // 'sql' => $rsobj->sql,
                'userId' => $userId,
            );
        }

        //Add/Update employee club begins
        if ($status['user_id'] > 0) {
            $userTypes = array(1, 2, 4, 9);
            if (in_array($params['user_type_id'], $userTypes)) {
                $params['userId'] = $userId;
                $this->updateEmployeeClub($params);
            }
        }
        //Add/Update employee club ends    

        return $status;
    }

   /**
    * Add / Update A Coach Details for a Club.
    *
    * @param array $params service parameter, Keys:CompanyId(company_id),UserId
    *
    * @return array object
    */
    
    public function updateEmployeeClub($params)
    {
        $companyId = $params['company_id'];
        $clubId = $params['club_id'];
        $userId = $params['userId'];

        $data = array(
                'r_user_type_id' => $params['user_type_id'],
                'r_company_id' => $params['company_id'],
                'r_club_id' => $params['club_id'],
                'r_user_id' => $params['userId'],
                'is_primary' => 1,
        );

        $rsobj = $this->dbcon->Execute(
            GET_CENTRE_EMPLOYEE_EXIST, array($companyId, $clubId, $userId)
        );

        if ($rsobj->RecordCount()) {

            //Remove older primary club if exist
            $this->dbcon->Execute(
                "DELETE FROM `t_employee_centre` WHERE `r_company_id`='$companyId' 
                AND `r_user_id`='$userId' AND `r_club_id`!='$clubId' 
                AND `is_primary`=1"
            );

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Employee club update success',
                // 'sql' => $rsobj->sql,
            );
        } else {

            //Remove older primary club if exist
            $this->dbcon->Execute(
                "DELETE FROM `t_employee_centre` WHERE `r_company_id`='$companyId' 
                AND `r_user_id`='$userId' AND `is_primary`=1"
            );

            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Employee club add success',
                // 'sql' => $rsobj->sql,
            );
        }

        return $status;
    }

    /**
    * Add / Update A Coach/User Personal Details
    *
    * @param array $params service parameter, Keys:UserId(user_id)
    *
    * @return array object
    */
       public function getUserPersonalInfoDetails($params)
       {
		 
           if (!isset($params['user_id'])) {
               return 'Invalid Data Send';
           }   
           $rsobj = $this->dbcon->Execute(
            GET_COACH_PERSONAL_INFO, array($params['user_id'])
           );
		 
           if ($rsobj->RecordCount()) {
               $personalInfo = $rsobj->fields;
               $this->status = array(
             'status' => 'success',
             'status_code' => 1,
             'status_message' => 'Get Personal Info Details Received',
             'total_records' => $rsobj->RecordCount(),
             // 'sql' => $rsobj->sql,
             'personalInfoDetails' => $personalInfo,
            );
           } else {
               $this->status = array(
             'status' => 'error',
             'status_code' => 0,
             'status_message' => 'Get Personal Info Details Not Found.',
             'total_records' => 0,
             // 'sql' => $rsobj->sql,
             'personalInfoDetails' => '',
            );
           }
        //Return the result array
	
        return array(
            'movesmart' => $this->status,
        );
       }

        /**
         * Fucntion return the list of all the users based on the usertype 
            sent in param
         * Query String Format : http://localhost/movesmart/ws/api.php?
            usertype=Coach&search_type=&members_center_id=&
            txt_search_val=&mod=coach&method=getCoachList.
         *
         public function getCoachList($params)
         {
         $usertype = (isset($params['usertype'])) ? $params['usertype'] : '';
         $coachListArr = array();
         $condition = '';
         if (!$usertype) {
         return 'Invalid User type';
         }
         $baseQuery = GET_USER_LIST_BASED_ON_TYPE;
         if (
            isset($params['members_center_id']) && 
            $params['members_center_id'] != ''
         ) {
         $condition .= ' AND u.r_club_id ='.$params['members_center_id'];
         }
         if (
            isset($params['search_type']) && 
            $params['search_type'] != '' && 
            $params['txt_search_val'] != ''
         ) {
         switch ($params['search_type']) {
         case 'first_name':
         $condition .= " AND first_name LIKE '%".$params['txt_search_val']."%' ";
         break;
         
         case 'last_name':
         $condition .= " AND last_name LIKE '%".$params['txt_search_val']."%' ";
         break;
         
         case 'email':
         $condition .= " AND email LIKE '%".$params['txt_search_val']."%' ";
         break;
         
         case 'gsm':
         $condition .= " AND gsm LIKE '%".$params['txt_search_val']."%' ";
         break;
         
         case 'empcode':
         $condition .= " AND user_id LIKE '%".$params['txt_search_val']."%' ";
         break;
         
         case 'status':
         $condition .= " AND s.status = '".$params['txt_search_val']."'";
         break;
         }
         }
         
         $rsobj = $this->dbcon->Execute($baseQuery.$condition);
         
         if ($rsobj->RecordCount()) {
         while (!$rsobj->EOF) {
         $coachListArr[] = $rsobj->fields;
         $rsobj->MoveNext();
         }
         $this->status = array(
         'status' => 'success',
         'status_code' => 1,
         'status_message' => 'Get Coach Details Received',
         'total_records' => $rsobj->RecordCount(),
         // 'sql' => $rsobj->sql,
         'coachListArr' => $coachListArr,
         );
         } else {
         $this->status = array(
         'status' => 'error',
         'status_code' => 0,
         'status_message' => 'Get Coach Details Not Found.',
         'total_records' => $rsobj->RecordCount(),
         // 'sql' => $rsobj->sql,
         'coachListArr' => '',
         );
         }
         //Return the result array    
         return array(
         'movesmart' => $this->status,
         );
         }
         
       /**
        * Add / Update A Coach/User Personal Details 
            (Compateble with the Device Coach/Client Application)
        *
        * @param array $params service parameter, Keys:UserId(user_id)
        *
        * @return array object
        */
       public function addCoachPersonalDetails($params)
        {
			
            $userId = isset($params['userId']) ? $params['userId'] : '';
            $rsobj = $this->dbcon->Execute(
                GET_COACH_PERSONALINFO, array($userId)
            );
            $dateTime = date('Y-m-d H:i:s');
            $data = array(
                'r_user_id' => $userId,
               /*  'r_city_id' => isset($params['city']) ? 
                    $params['city'] : '',
                'doorno' => isset($params['doorNo']) ? 
                    $params['doorNo'] : '',
                'address' => isset($params['address']) ? 
                    $params['address'] : '',
                'zipcode' => isset($params['zipCode']) ? 
                    $params['zipCode'] : '',
                'bus' => isset($params['bus_no']) ? 
                    $params['bus_no'] : '',
                'phone' => isset($params['phone']) ? 
                    $params['phone'] : '',
                'mobile' => isset($params['mobile']) ? 
                    $params['mobile'] : '',
                'company' => isset($params['company_id']) ? 
                    $params['company_id'] : '',
                'location' => isset($params['city']) ? 
                    $params['city'] : '',
                'securityno' => isset($params['securityNo']) ? 
                    $params['securityNo'] : '',
                'work_starton' => isset($params['workStart']) ? 
                    $params['workStart'] : '',
                'work_endon' => (isset($params['workEnd']) && 
                    $params['workEnd'] != '') ? $params['workEnd'] : '0000-00-00',
                'created_by' => '1',
                'created_date' => $dateTime,
                'modified_by' => '1',
                'modified_date' => $dateTime,
                'gsm' => isset($params['mobile']) ? $params['mobile'] : '', */
            );
            if ($rsobj->RecordCount()) {
				
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
				
               $this->dbcon->Execute($rsUpdates);
				 
				//Set the status message
                $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Personal Details Successfully Updated.',
                // 'sql' => $rsobj->sql,
                'userId' => $userId,
                );
            } else {
                $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
				
                $this->dbcon->Execute($rsInserts);
                //Set the status message
                $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Personal Details Successfully Added.',
                // 'sql' => $rsobj->sql,
                'userId' => $userId,
                );
            }

            return array(
            'movesmart' => $this->status
            );
        }

        /**
        * Get & Returns the Records for Training Programs
            (Compateble with the Device Coach/Client Application)
        *
        * @param array $params service parameter, Keys:UserId(user_id)
        *
        * @return array object
        */
        /*public function getTrainingProgramDetails($params)
        {
            $traininglInfo = array();
            if (!isset($params['user_id'])) {
                return 'Invalid Data Send';
            }
            $rsobj = $this->dbcon->Execute(
                GET_TRAINING_INFO, array($params['user_id'])
            );
            if ($rsobj->RecordCount()) {
                while (!$rsobj->EOF) {
                    $traininglInfo[] = $rsobj->fields;
                    $rsobj->MoveNext();
                }
                $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Coach Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'trainingInfoDetails' => $traininglInfo,
                );
            } else {
                $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Coach Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'trainingInfoDetails' => '',
                );
            }
            //Return the result array    
            return array(
            'movesmart' => $this->status,
            );
        }
        */
    /**
    * Deletes & Returns Result on Coach Delete
    *
    * @param array $params service parameter, Keys:UserId(user_id)
    *
    * @return array object
    */
    public function getCoachDelete($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
                'user_id' => isset($params['userId']) ? $params['userId'] : '0',
                'is_deleted' => '1',
                'modified_by' => '1',
                'modified_date' => $dateTime,
            );
        $rsobj = $this->dbcon->Execute(GET_COACH_DELETED, array($userId));
        if ($rsobj->RecordCount()) {
			  $rsUpdates = $this->dbcon->Execute(COACH_DELETED, array($userId));
            //$rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
		
            $this->dbcon->Execute($rsUpdates);
                //Set the status message
                $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User Deleted.',
                // 'sql' => $rsobj->sql,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
    }
    /**
    * Add/Update User Profile & Basic Login Information
    *
    * @param array $params service parameter, Keys:UserId(user_id)
    *
    * @return array object
    */
    public function updateUserProfile($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $data = array(
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            );
            /* 
             * Comments: Commented by SK , 
             * To handler Forget password Security Question.
              * author SK Shanethatech * 
              * Created Date  : JAN-25-2016 \
              */
            if (isset($params['question']) and $params['question'] != '' 
                and (json_decode($params['question'], true))
            ) {
                $data['security_questions'] = $params['question'];
            }
        if (isset($params['passwrd']) and $params['passwrd'] != '') {
            $data['password'] = $params['passwrd'];
        }
        if (isset($params['phone']) and $params['phone'] != '') {
            $data['phone'] = $params['phone'];
        }
        if (isset($params['gender']) and $params['gender'] != '') {
            $data['gender'] = $params['gender'];
        }
		/*********************************************************************
		Change the Minus Value into Null and By default go 0 in database
		********************************************************************/
		else
		{
			$data['gender'] = '';
		}
		/*********************************************************************
		Change the Minus Value into Null and By default go 0 in database
		********************************************************************/
        if (isset($params['language']) and $params['language'] != '') {
            $data['r_language_id'] = $params['language'];
        }
        $rsobj = $this->dbcon->Execute(GET_COACH_USERINFO, array($userId));
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            return $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User Updated successfully.',
                //"sql" => $rsobj->sql 
                );
        } else {
            return $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Usernot updated .',
                'total_records' => 0,
                //"sql" => $rsobj->sql
                );
        }
    }
} // End Class.
;
