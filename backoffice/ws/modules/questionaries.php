<?php
error_reporting(0);
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Questionaries
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle questionaries related actions.
 */
require_once SQL_PATH.DS.'questionaries.php';
 /**
 * Class for functions to handle questionaries related actions.
 
 * @category Modules
 
 * @package Questionaries
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class questionariesModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'No result found',
        );
    }     
    /**
    * To load Questionaries on the client login.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function loadClientQuestionaries($params)
    {
		
        try {
            $response = array();
            /*Questionaries Group*/
            $userId = isset($params['userId']) ? $params['userId'] : 0;
            if ($userId == 0) {
                return null;
            }
            $groupqry = GET_QUESTION_GROUP;
            $groupparamarr = array();
            if (isset($params['getgroupId'])) {
                $groupqry = GET_QUESTION_GROUP_BYID;
                $groupparamarr = array($params['getgroupId']);
            }
            $group_rs = $this->dbcon->Execute($groupqry, $groupparamarr);
			
            $group = array();
            if ($group_rs->RecordCount()) {
                $group = $group_rs->GetRows();
            }
            $quest_lang_rs = $this->dbcon->Execute(GET_QUESTION_LANGUAGE);
            $questionlang = array();
            if ($quest_lang_rs->RecordCount()) {
                $questionlangdata = $quest_lang_rs->GetRows();
                foreach ($questionlangdata as $questionlangdet) {
                    $questionlangdet['question'] = utf8_decode($questionlangdet['question']);
                    $fid    =   $questionlangdet['formelement_id'];
                    $lid    =   $questionlangdet['language_id'];
                    $questionlang['formelmid_'.$fid]['lang_'.$lid] = $questionlangdet;
                }
            }
            //$i = 0;
            $activitycredits = array();
            /* Fetching All Answers */
            //$redeem = array();
            //todo ,quickfix implemented date check on answer value of EAT and MIND so that if it doesnt match the current date, the answer value is 0 ------
            $userAnswers = array();
            $answers_rs = $this->dbcon->Execute(GET_USERANSWERS, array($userId));
            if ($answers_rs->RecordCount() and $answers_rs->RecordCount() > 0) {
                $answers = $answers_rs->GetRows();
                foreach ($answers as $eans) {
                    //new added code by jeet on 2 feb
                    $anserdavedt = new DateTime($eans['date']);
                    $newanserdavedt = $anserdavedt->format('Y-m-d');
                    /*if($newanserdavedt != date("Y-m-d"))
                    {
                        $eans['value'] = 0;
                    }*/
                    //----code end---
                    $userAnswers['mv_'.$eans['monitor_variable_id']][] = $eans;
                }
            }
			
            //todo end----
            $phasecount = 0;
            foreach ($group as $egroup) {
                //foreach($phase as $ephase){
               // echo "phase check ".PHASE_Check."<br/>";
                $phasestartdate = '';
                $phaseparams['phaseId'] = PHASE_Check;
                $grpid =   $egroup['group_id'];
                $lastphase_rs = $this->dbcon->Execute(
                    GET_USER_PHASEID_BYGROUP, 
                    array($params['userId'], $grpid)
                );
                if (
                    ( $lastphase_rs->RecordCount() ) 
                    and ($lastphase_rs->RecordCount() > 0)
                ) {
                    $userphase = $lastphase_rs->fields;
                    $phaseparams['phaseId'] = $userphase['r_phase_id'];
                    $phasestartdate = $userphase['created_date'];
                }
                $phase_rs = $this->dbcon->Execute(
                    GET_QUESTION_PHASE_BYID, 
                    $phaseparams
                );
                //$phase = array();
                $phasecount = 0;
                if ($phase_rs->RecordCount()) {
                    $phasecount = $phase_rs->RecordCount();
                    $phase = $phase_rs->GetRows();
                    if ($phase) {
                        $ephase = $phase[0];
                        $rowdata = $egroup;
                        $phase_range_rs = $this->dbcon->Execute(
                            GET_QUESTION_PHASE_RANGE, 
                            array($ephase['phase_id'], $grpid)
                        );
                        $phaserange = array();
                        //$phaserangecnt = 0;
                        if ($phase_range_rs->RecordCount()) {
                            //$phaserangecnt = $phase_range_rs->RecordCount();
                            $arr_prange = $phase_range_rs->GetRows();
                            foreach ($arr_prange as $prange) {
                                $prgid  =   $prange['r_group_id'];
                                $prange_lang_rs = $this->dbcon->Execute(
                                    GET_QUESTION_PHASE_RANGE_LANG, 
                                    array($prange['id'])
                                );
                                $prange['_languagecount'] = 0;
                                $prange['_language'] = array();
                                if ($prange_lang_rs->RecordCount()) {
                                    $prange['_languagecount']
                                        =$prange_lang_rs->RecordCount();
                                    $prange['_language']
                                        =$prange_lang_rs->GetRows();
                                }
                                $phaserange['rangegroup'.$prgid][] = $prange;
                            }
                        }
                        //$egroup['_phase'] = $ephase;
                        $rowdata['_phase'] = array();
                        //XX foreach($group as $egroup){
                        /*Activity - Type*/
                        $ephase['_activitiescount'] = 0;
                        $ephase['_activities'] = array();
                        /*Hard Cored due to $activity_id implement Later*/
                        $act_check_rs = $this->dbcon->Execute(QUESTIONARIES_TYPE, array($ephase['phase_id'], $grpid));
                        if ($act_check_rs->RecordCount()) {
                            $ques_type = $act_check_rs->fields;
                            $qtid   =   $ques_type['r_contenttypeid'];
                            if (
                                $qtid==QUESTIOARIES_TYPE_activity
                            ) {
                                //$activity_id = 1;
                                $activities_rs = $this->dbcon->Execute(
                                    GET_ACTIVITIES_BY_GROUPPHASE, 
                                    array($ephase['phase_id'], $grpid)
                                );
                                if ($activities_rs->RecordCount()) {
                                    $ephase['_activitiescount']
                                        =$activities_rs->RecordCount();
                                    $rsactivities
                                        =$activities_rs->GetRows();
                                    $activities = array();
                                    foreach ($rsactivities as $activity) {
                                        $actpoints_rs = $this->dbcon->Execute(
                                            GET_ACTIVITY_POINTS, 
                                            array($activity['phaseact_id'])
                                        );
                                        $activity['_pointscount'] = 0;
                                        $activity['_points'] = array();
                                        if ($actpoints_rs->RecordCount()) {
                                            $activity['_pointscount']
                                                =$actpoints_rs->RecordCount();
                                            $activity['_points']
                                                =$actpoints_rs->GetRows();
                                            $activitycredits
                                                =$activity['_points'];
                                        }
                                        /*Get Answers for each 
                                            Activity- By User ID*/
                                        $activity['_answerscount'] = 0;
                                        $activity['_answers'] = array();
                                        $answers_rs = $this->dbcon->Execute(
                                            GET_ACTIVITY_USERANSWERS, 
                                            array(
                                                $userId, 
                                                $activity['activity_id']
                                            )
                                        );
                                        if ($answers_rs->RecordCount()) {
                                            $activity['_answerscount']
                                                =$answers_rs->RecordCount();
                                            $answers = $answers_rs->GetRows();
                                            //todo ,quickfix implemented date check on answer value of MOVE so that if it doesnt match the current date, the answer value is 0 ------
                                            foreach ($answers as $eachanswer) {
                                                $actanswers_rs = $this->dbcon->Execute(
                                                    GET_INSERTED_MVACTUAL_POINTS,
                                                    array($eachanswer['id'])
                                                );
                                                //new added code by jeet on 3 feb
                                                $actanserdavedt = new DateTime($eachanswer['date']);
                                                $newactanserdavedt = $actanserdavedt->format('Y-m-d');
                                                /*if($newactanserdavedt != date("Y-m-d"))
                                                {
                                                    $eachanswer['value'] = 0;
                                                }*/
                                                //code end-----
                                                $eachanswer['_actualvaluescount']=0;
                                                $eachanswer['_actualvalues'] = array();
                                                if ($actanswers_rs->RecordCount()) {
                                                    $eachanswer['_actualvaluescount']
                                                        =$actanswers_rs->RecordCount();
                                                    $eachanswer['_actualvalues']
                                                        =$actanswers_rs->GetRows();
                                                }
                                                $activity['_answers'][]=$eachanswer;
                                            }
                                            //todo end---
                                        }
                                        $activities[] = $activity;
                                    }
                                    $ephase['_activities'] = $activities;
                                }
                            }
                        }
                            /*Questions - Phase Range info*/
                            $ephase['_phaserange'] = array();
                        $ephase['_phaserangecount'] = 0;
                        if (isset($phaserange['rangegroup'.$grpid])) {
                            $ephase['_phaserange']
                                =$phaserange['rangegroup'.$grpid];
                            $ephase['_phaserangecount']
                                =count($ephase['_phaserange']);
                        }
                        
                        /*Questions - Weigtage info*/
                        $ephase['_creditinfo'] = array();
                        $ephase['_creditinfocount'] = 0;
                        $ewight_rs = $this->dbcon->Execute(
                            GET_QUESTION_WEIGHT_BYGROUPPHASEID, 
                            array(
                                $ephase['phase_id'], $grpid
                            )
                        );
                        if ($ewight_rs->RecordCount()) {
                            $ephase['_creditinfocount'] = $ewight_rs->RecordCount();
                            $creditinfo = $ewight_rs->GetRows();
                            $ephase['_creditinfo'] = $creditinfo;
                        }
                        
                        $questionsobj=$this->getQuestionsByGroupIdPhaseId(
                            $grpid, 
                            $ephase['phase_id'], 
                            $userAnswers, 
                            $questionlang
                        );
                        $ephase['_questionscount']
                            =$questionsobj['_questionscount'];
                        $ephase['_questions'] = $questionsobj['_questions'];
                        $rowdata['_phase'][] = $ephase;
                        $rowdata['_phasestartdate'] = $phasestartdate;
                        //}
                        //$response[]    =    $rowdata;
                        $rowdata['_generalquestioncount'] = 0;
                        $rowdata['_coachcount'] = 0;
                        $rowdata['_coachlist'] = array();
                        $response['group'.$grpid] = $rowdata;
                    }
                }
            }
            $userpreference = array();
            $userrs = $this->dbcon->Execute(
                GET_USER_BYUSERID, array($params['userId'])
            );
            if ($userrs->RowCount()) {
                $userpreference = $userrs->fields;
            }
            /*Get Club List*/
            $clublist = array();
            $clubcount = 0;
            $clubrs = $this->dbcon->Execute(GET_MEMBER_CLUBLIST, array($userId));
            if ($clubrs->RowCount()) {
                $clublist = $clubrs->GetRows();
                $clubcount = $clubrs->RowCount();
            }
            /*Get Coach List*/
            //$coachlist = array();
            //$coachcount = 0;
            $coachrs = $this->dbcon->Execute(GET_MEMBER_COACHLIST, array($userId));
            if ($coachrs->RowCount()) {
                $coachlist = $coachrs->GetRows();
                foreach ($coachlist as $ecoach) {
                    if (isset($response['group'.$ecoach['r_group_id']])) {
                        ++$response['group'.$ecoach['r_group_id']]['_coachcount'];
                        $response['group'.$ecoach['r_group_id']]['_coachlist'][]
                            =$ecoach;
                    }
                }
            }
            $usertest = array();
            $usertest_rs = $this->dbcon->Execute(
                GET_USER_LAST_ANALYSED_TEST_BY_USERID, $params['userId']
            );
            if ($usertest_rs->RowCount()) {
                $usertest = $usertest_rs->GetRows();
            }
            /*Get Weightage */
            $phase_weightage = array();
            $rsweightage = $this->dbcon->Execute(GET_QUESTION_WEIGHT);
            //$phasecredits = array();
            //$phasecreditscount = 0;
            if ($rsweightage->RecordCount()) {
                //$phasecreditscount = $rsweightage->RecordCount();
                $phasecredits = $rsweightage->GetRows();
                foreach ($phasecredits as $eweightage) {
                    $wpid=  $eweightage['r_phase_id'];
                    $wgid=  $eweightage['r_group_id'];
                    $phase_weightage['phase'.$wpid]['group'.$wgid] = $eweightage;
                }
            }
            /*Get Topic Max Points*/
            //topic Options
            $rsoptions = $this->dbcon->Execute(GET_QUESTION_OPTIONS);
            $questionoptions = array();
            if ($rsoptions->RecordCount()) {
                $qoptions = $rsoptions->GetRows();
				if(!empty($qoptions)){
                foreach ($qoptions as $qopt) {
					
                    $qusid = $qopt['r_form_elementid'];
                    if (!isset($questionoptions['q'.$qusid])) {
                        $questionoptions['q'.$qusid] = array();
                    }
                    $questionoptions['q'.$qusid][] = $qopt;
					
					
                }
			}
            }
            //topic_maxpoints
            $rsprtopic = $this->dbcon->Execute(GET_TOPIC_RANGE);
            //$success = array();
            $prtopic = array();
            if ($rsprtopic->RecordCount()) {
                $prtop = $rsprtopic->GetRows();
                foreach ($prtop as $p) {
                    if (!isset($prtopic['topic'.$p['r_topic_id']])) {
                        $prtopic['topic'.$p['r_topic_id']] = array();
                    }
                    array_push($prtopic['topic'.$p['r_topic_id']], $p);
                }
            }
            $rstopic = $this->dbcon->Execute(GET_TOPIC_QUESTIONS);
            $topic_maxpoints = array();
            if ($rstopic->RecordCount()) {
                $arrtopics = $rstopic->GetRows();
				if(is_object($arrtopics) && is_array($arrtopics)){
                foreach ($arrtopics as $eachtopic) {
                    $topic_id = $eachtopic['topic_id'];
                    $group_id = $eachtopic['group_id'];
                    if (!isset($topic_maxpoints['group'.$group_id])) {
                        $topic_maxpoints['group'.$group_id] = array();
                    }
                    $etopictrack = array();
                    $etopictrack['topic_id'] = $eachtopic['topic_id'];
                    $etopictrack['group_id'] = $eachtopic['group_id'];
                    $etopictrack['topic_name'] = $eachtopic['topic_name'];
                    //$etopictrack['maxcredits']    =    
                    $q = $eachtopic['formelemid'];
                    $qtype = $eachtopic['type'];
                    $tqoptions = $questionoptions['q'.$q];
                    $maxcredits = $this->GetMaxByQuestionType(
                        $q, $qtype, $tqoptions
                    );   
                    if (
                        !isset($topic_maxpoints['group'.$group_id]['topic'.$topic_id]['maxvalue'])
                    ) {
                        $topic_maxpoints['group'.$group_id]['topic'.$topic_id]['maxvalue'] = 0;
                    }
                    $topic_maxpoints['group'.$group_id]['topic'.$topic_id]['maxvalue']
                        +=$maxcredits['option_maxval'];
                    $topic_maxpoints['group'.$group_id]['topic'.$topic_id]['topic']
                        =$etopictrack;
                    /*$topic_maxpoints['group'.$group_id]
                        ['topic'.$topic_id]['success']
                        =isset($prtopic['topic'.$topic_id]) ? 
                        $prtopic['topic'.$topic_id]
                        :array();
                    $topic_maxpoints['group'.$group_id]['topic'.$topic_id]['quickwin']
                        =isset($prtopic['topic'.$topic_id]) ? 
                        $prtopic['topic'.$topic_id]:array();*/
                }
			}
            }
            /*Phase Rang By Topic Quick Win Text*/
            $prlang = array();
            $prlanggroupphase = array();
            $rsprlang = $this->dbcon->Execute(
                GET_TOPIC_RANGE_LANGUAGE
            );
            if ($rsprlang->RecordCount()) {
                $prl = $rsprlang->GetRows();
                foreach ($prl as $pl) {
                    $qprid=$pl['r_ques_phase_range_id'];
                    $texttypekey = 'TextAdvice';
                    if ($pl['text_type'] == TEXTTYPE_SUCCESS) {
                        $texttypekey = 'TextSuccess';
                    } elseif ($pl['text_type'] == TEXTTYPE_QUICKWIN) {
                        $texttypekey = 'TextQuickWin';
                    } elseif ($pl['text_type'] == TEXTTYPE_GROAL) {
                        $texttypekey = 'TextGoals';
                    } /*else { //if($pl['text_type']==TEXTTYPE_ADVICE){
                        $texttypekey = 'TextAdvice';
                    }*/
                    if (!isset($prlang['prange'.$qprid])) {
                        $prlang['prange'.$qprid] = array();
                    }
                    if (!isset($prlang['prange'.$qprid][$texttypekey])) {
                        $prlang['prange'.$qprid][$texttypekey] = array();
                    }
                    $prlang['prange'.$qprid][$texttypekey]['lang'.$pl['languageid']]=$pl;
                }                
            }
            $rsusrtopic	=	$this->dbcon->Execute(
                GET_TOPIC_USERCREDITS,array($params['userId'])
            );
            $topic_usrcredits	=	array();
            if($rsusrtopic->RecordCount()){
                $topicusrcredits=$rsusrtopic->GetRows();
                foreach($topicusrcredits as $tusrc){
                    $tgid=$tusrc['group_id'];
                    $ttid=$tusrc['topic_id'];
                    $topic_usrcredits['group'.$tgid]['topic'.$ttid]=$tusrc;
                }
            }
            $rsprtopic = $this->dbcon->Execute(
                GET_TOPIC_RANGE
            );
            if ($rsprtopic->RecordCount()) {
                $trange = $rsprtopic->GetRows();
                foreach ($trange as $tr) {
                    $gid = $tr['r_group_id'];
                    //$pid=$tr['r_phase_id'];
                    $tid=$tr['r_topic_id'];
                    if (!isset($prlanggroupphase['group'.$gid])) {
                        $prlanggroupphase['group'.$gid] = array();
                    }
                    $mxpoints	=	0;
                    if(isset($topic_maxpoints['group'.$gid]['topic'.$tid])) { 
                        $mxpoints
                            =$topic_maxpoints['group'.$gid]['topic'.$tid]['maxvalue'];
                    }
                    $usrpoints	= 0;
                    if(isset($topic_usrcredits['group'.$gid]['topic'.$tid]))  {
                        $usrpoints
                            =$topic_usrcredits['group'.$gid]['topic'.$tid]['usercredits'];
                    }
                    if($usrpoints>$mxpoints){
                        $usrpoints	=	$mxpoints;
                    }
                    $prlang['prange'.$tr['id']]['topic_id'] = $tid;
                    $prlang['prange'.$tr['id']]['topic_name'] = $tr['topic_name'];
                    $prlang['prange'.$tr['id']]['topic_maxcredits']	=	$mxpoints;
                    $prlang['prange'.$tr['id']]['topic_usercredits']=	$usrpoints;
                    $prlang['prange'.$tr['id']]['topic_difference']
                        =($mxpoints-$usrpoints);
                    $prlanggroupphase['group'.$gid][]=$prlang['prange'.$tr['id']];
                    $prlanggroupphase['counttopicsgroup'.$gid]
                        =count($prlanggroupphase['group'.$gid]);
                }
            }
            $topicsrange	=array();
            foreach($prlanggroupphase as $groupkey=>$prgp){
                if($prgp and is_array($prgp)) {
                    $topicsrange[$groupkey]	=	custom_multisort(
                        $prgp,'topic_difference',SORT_ASC
                    );
                } else {
                    $topicsrange[$groupkey]	=	$prgp;
                }
            }
            unset($prlanggroupphase);
            unset($prlang);
            /*Get Credit Achive Points*/
            $credits = array();
            $credits_phase = array();
            //$creditcount = 0;
            $credits_rs = $this->dbcon->Execute(
                GET_ACHIVE_CREDITS, array($params['userId'])
            );
            if ($credits_rs->RowCount()) {
                //$creditcount = $credits_rs->RowCount();
                $creditsdata = $credits_rs->GetRows();
                foreach ($creditsdata as $eachrow) {
                    if (!isset($credits['group'.$eachrow['f_groupid']])) {
                        $credits['group'.$eachrow['f_groupid']] = array(
                            'unknown' => 0, 
                            'inprocess' => 0, 
                            'redeemable' => 0, 
                            'redeemed' => 0
                        );
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_unknown) {
                        // $creditdetail['unknown']    =    $eachrow['credits'];
                        $credits['group'.$eachrow['f_groupid']]['unknown']
                            +=$eachrow['credits'];
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_inprocess) {
                        // $creditdetail['inprocess']    =    $eachrow['credits'];
                        $credits['group'.$eachrow['f_groupid']]['inprocess']
                            +=$eachrow['credits'];
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_redeemable) {
                        // $creditdetail['redeemable']    =    $eachrow['credits'];
                        $credits['group'.$eachrow['f_groupid']]['redeemable']
                            +=$eachrow['credits'];
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_redeemed) {
                        // $creditdetail['redeemed']    =    $eachrow['credits'];
                        $credits['group'.$eachrow['f_groupid']]['redeemed']
                            +=$eachrow['credits'];
                    }
                    //$credits['group'.$eachrow['f_groupid']][]+=$creditdetail;
                }
            }
            $credits_rs = $this->dbcon->Execute(
                GET_ACHIVE_CREDITS_BYPHASE, 
                array($params['userId'])
            );
            if ($credits_rs->RowCount()) {
                //$creditcount = $credits_rs->RowCount();
                $creditsdata = $credits_rs->GetRows();
                foreach ($creditsdata as $eachrow) {
                    $p = $eachrow['f_phaseid'];
                    $g = $eachrow['f_groupid'];
                    if ((!isset($credits_phase['group'.$g]))) {
                        $credits_phase['group'.$g] = array();
                    }
                    if (!isset($credits_phase['group'.$g]['phase'.$p])) {
                        $credits_phase['group'.$g]['phase'.$p] = array(
                            'unknown' => 0, 
                            'inprocess' => 0, 
                            'redeemable' => 0, 
                            'redeemed' => 0
                        );
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_unknown) {
                        // $creditdetail['unknown']    =    $eachrow['credits'];
                        $credits_phase['group'.$g]['phase'.$p]['unknown']
                            +=$eachrow['credits'];
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_inprocess) {
                        // $creditdetail['inprocess']    =    $eachrow['credits'];
                        $credits_phase['group'.$g]['phase'.$p]['inprocess']
                            +=$eachrow['credits'];
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_redeemable) {
                        // $creditdetail['redeemable']    =    $eachrow['credits'];
                        $credits_phase['group'.$g]['phase'.$p]['redeemable']
                            +=$eachrow['credits'];
                    }
                    if ($eachrow['f_status'] == REDEEM_STATUS_redeemed) {
                        // $creditdetail['redeemed']    =    $eachrow['credits'];
                        $credits_phase['group'.$g]['phase'.$p]['redeemed']
                            +=$eachrow['credits'];
                    }
                    $credits_phase['group'.$g]['phase'.$p]['weightage'] =array();
                    if(isset($phase_weightage['phase'.$p]['group'.$g])){ 
                        $credits_phase['group'.$g]['phase'.$p]['weightage']
                            =$phase_weightage['phase'.$p]['group'.$g];
                    }
                    $credits_phase['group'.$g]['phase'.$p]['activitycredits']
                        =array();
                    if($p == PHASE_phase1 && $g == GROUP_MOVESMART) { 
                        $credits_phase['group'.$g]['phase'.$p]['activitycredits']
                            =$activitycredits;
                    }
                }
            }
            /*Get Active Trainings Details*/
            $trainingdets = array();
            //$trainingdetcount = 0;
            $currdate = date('Y-m-d');
            $trainingdets_rs = $this->dbcon->Execute(
                GET_ACTIVE_TRAINING_FOR_DATE_USER, 
                array(
                    $params['userId'], 
                    $currdate, 
                    $currdate
                )
            );
            if ($trainingdets_rs->RowCount()) {
                //$trainingdetcount = $trainingdets_rs->RowCount();
                $trainingdatas = $trainingdets_rs->GetRows();

                foreach ($trainingdatas as $trainingdt) {
                    $trainingplandata = array();
                    $trainingplanrs = $this->dbcon->Execute(
                        GET_ACTIVE_TRAINING_PLAN_BYTRAININGID, 
                        array(
                            $trainingdt['f_cardiotrainingid']
                        )
                    );
                    if ($trainingplanrs->RecordCount()) {
                        $tplandata = $trainingplanrs->GetRows();
                        foreach ($tplandata as $tpdata) {
                            $trainingplandata[$tpdata['f_weeknr']] = $tpdata;
                        }
                    }
                    $pointsdata = array();
                    $pointachdets_rs = $this->dbcon->Execute(
                        GET_ACHIVE_POINTS_BYTRAINING_STATUS, 
                        array(
                            $trainingdt['f_cardiotrainingid'], 
                            REDEEM_STATUS_inprocess
                        )
                    );
                    if ($pointachdets_rs->RowCount()) {
                        $pointsdata = $pointachdets_rs->GetRows();
                    }

                    $weekno = 0;
                    $weekstdate =   date('Y-m-d');
                    $trainingdt['weekdatebreak']
                        = $this->getWeeksbasedtraininginformation(
                            $trainingdt['f_trainingstdate'], 
                            $trainingdt['f_trainingeddate'],
                            $weekstdate,
                            $weekno, 
                            $trainingplandata, 
                            $pointsdata
                        );
						
                    $trainingdt['currentweekno'] = $weekno;
					
                    $trainingdets[] = $trainingdt;
                }
            }
            $grouptopics = array();
            $topicrs = $this->dbcon->Execute(GET_TOPGROUP_PHASE_TOPICS);
            if ($topicrs->RowCount()) {
                $topics = $topicrs->GetRows();
                foreach ($topics as $topic) {
                    $tgid = $topic['group_id'];
                    if (!isset($grouptopics['group_'.$tgid])) {
                        $grouptopics['group_'.$tgid] = array();
                    }
                    //Need To change while applying the top three algorithm
                    if (count($grouptopics['group_'.$tgid]) < 3) {
                        $grouptopics['group_'.$tgid][] = $topic;
                    }
                }
            }

            $educationlang = $this->dbcon->Execute(GET_EDUCATION);
            $educationlist = array();
            if ($educationlang->RecordCount()) {
                $educationdata = $educationlang->GetRows();
                foreach ($educationdata as $education) {
                    $egid = $education['group_id'];
                    $eday = $education['day'];
                    $elan = $education['language_id'];
                    $educationlist['groupid_'.$egid]['days_'.$eday]['lang'.$elan]
                        =$education;
                }
            }
            $result['grouplist'] = $response;
			//*note* added code
            //echo "myscore";
            //$this->saveUserScoreInDb($response);
            //die;
            $result['generalquestions'] = $this->getQuestionsByGroupIdPhaseId(
                GROUP_DUMMY_General, PHASE_Check, $userAnswers, $questionlang
            );
			 //$week = isset($trainingdt['currentweekno'])?$trainingdt['currentweekno']:0;
			// if($week>0){
			 //$str = "update t_users set week=".$week." where user_id=".$params['userId'];
			// mysql_query($str);
			// }
            $result['clublist'] = $clublist;
            $result['clubcount'] = $clubcount;
            $result['phasecount'] = $phasecount;
            $result['userpreference'] = $userpreference;
            $result['usertest'] = $usertest;
            $result['credits'] = $credits;
            $result['activetraining'] = $trainingdets;
            $result['toptopics'] = $grouptopics;
            $result['education'] = $educationlist;
            $result['credits_byphase'] = $credits_phase;
            $result['topic_maxpoints'] = $topic_maxpoints;
            $result['topic_result'] = $topicsrange;
			//echo "<pre>";print_r($result);die;
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Got Questionaries Successfully',
                'data' => $result,
            );
        } catch (Exception $e) {
            $this->status = array(
                'status' => 'failed', 
                'status_code' => 0, 
                'status_message' => 'Exception: '.$e->getMessage()
            );
        }

        return $this->status;
    }
    
    /**
    * To save all answers for questions.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return response obj
    */
    public function saveAllAnswersQuestions($params)
    {
		//printLog('saving time');
		//printLog($params);
        $allanswers = (
            isset($params['allanswers']) 
            and $params['allanswers'] != ''
        ) ? json_decode($params['allanswers'], true) : array();
		
        $response   =   array();
        foreach ($allanswers as $eachans) {
			//echo "hello";
            $eachans['userId'] = $params['userId'];
            $response = $this->updateAnswerForQuestion($eachans);
        }

        return $response;
    }
    
    /**
    * To update answer for a question
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function updateAnswerForQuestion($params)
    {
		
		//print_r($params);
        try {
            $check_date = date('Y-m-d');
            $save_date =  date('Y-m-d H:i:s');
            if(isset($params['updDate'])){ 
                $check_date = date('Y-m-d', strtotime($params['updDate']));
                $save_date = date('Y-m-d H:i:s', strtotime($params['updDate']));
            }
            $select_rs=$this->dbcon->Execute(
                GET_INSERTED_MVDATA, 
                array($params['mvkey'], $params['userId'], $check_date)
            );
            $questionid = $groupid = $phaseid = 0;
            $query = '';
            if ($params['reftype'] == QUESTIOARIES_TYPE_question) {
                $query = GET_QUESTIONBY_MVID;
            } elseif ($params['reftype'] == QUESTIOARIES_TYPE_activity) {
                $query = GET_ACTIVITYBY_MVID;
            }
            $question_rs = $this->dbcon->Execute($query, array($params['mvkey']));
            if ($query != '') {
                if ($question_rs->RowCount()) {
                    $question = $question_rs->fields;
                    $questionid = $question['formelement_id'];
                    $groupid = $question['groupid'];
                    $phaseid = $question['phaseid'];
                }
            }
            /*Static App Controls*/
            $redeem_status = 0;
            //else {
            //$answer = array();
            //$mvdataid = 0;
            if ($select_rs->RecordCount()) {
                $mvdatarec = $select_rs->fields;
                $mvdataid = $mvdatarec['id'];
                $data['value'] = $params['value'];
                $data['r_form_element_option_id'] = $params['optionId'];
                $data['r_form_element_id'] = $questionid;
                $data['redeem_status'] = $redeem_status;
                $data['date'] = $save_date;
                $rsUpdates = $this->dbcon->GetUpdateSQL($select_rs, $data);
                $this->dbcon->Execute($rsUpdates);
                // echo $rsUpdates;
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Monitor variable data updated successfully ',
                );
            } else {
                $data['value'] = $params['value'];
                $data['r_form_element_option_id'] = $params['optionId'];
                $data['r_form_element_id'] = $questionid;
                $data['date'] = $save_date;
                $data['user_id'] = $params['userId'];
                $data['monitor_variable_id'] = $params['mvkey'];
                $data['redeem_status'] = $redeem_status;
                $insertSql = $this->dbcon->GetInsertSQL($select_rs, $data);
                $this->dbcon->Execute($insertSql);
                $insertid = $this->dbcon->Insert_ID();
                $mvdataid = $insertid;
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Monitor variable data added successfully.',
                );
            }
            /*update Actual Earned points*/
            if (isset($params['actualPoint'])) {
                $dataActpoint = array();
                $select_actualmap = $this->dbcon->Execute(
                    GET_INSERTED_MVACTUAL_POINTS, array($mvdataid)
                );
                if ($select_actualmap->RecordCount()) {
                    $dataActpoint['actmv_point'] = $params['actualPoint'];
                    $dataActpoint['actmv_date'] = $save_date;
                    if (isset($params['actualDeseaseParameter'])) {
                        $dataActpoint['actmv_parameter']
                            =$params['actualDeseaseParameter'];
                    }
                    if (isset($params['actualOtherData'])) {
                        $dataActpoint['actmv_otherdata']
                            =$params['actualOtherData'];
                    }
                    $rsUpdates = $this->dbcon->GetUpdateSQL(
                        $select_actualmap, $dataActpoint
                    );
                    $this->dbcon->Execute($rsUpdates);
                } else {
                    $dataActpoint = array();
                    $dataActpoint['actmv_id'] = $mvdataid;//$params['mvkey'];
                    $dataActpoint['actmv_userid'] = $params['userId'];
                    $dataActpoint['actmv_point'] = $params['actualPoint'];
                    $dataActpoint['actmv_date'] = $save_date;
                    if (isset($params['actualDeseaseParameter'])) {
                        $dataActpoint['actmv_parameter']
                            =$params['actualDeseaseParameter'];
                    }
                    if (isset($params['actualOtherData'])) {
                        $dataActpoint['actmv_otherdata'] = $params['actualOtherData'];
                    }
                    $actpointquery = $this->dbcon->GetInsertSQL(
                        $select_actualmap, 
                        $dataActpoint
                    );
                    $this->dbcon->Execute($actpointquery);
                    $this->dbcon->Insert_ID();
                }
                //}
            }
            $qstatic =   array(
                QUESTION_STATIC_preference, 
                QUESTION_STATIC_target, 
                QUESTION_STATIC_take_test
            );
            if (in_array($questionid, $qstatic)) {
                $redeem_status = -1;//To be taken Never for Redeem Data;
                if ($questionid == QUESTION_STATIC_preference) {
                    $userrs = $this->dbcon->Execute(
                        GET_USER_BYUSERID, 
                        array($params['userId'])
                    );
                    if ($userrs->RowCount()) {
                        $data['app_group_preference'] = $params['value'];
                        $updateqry = $this->dbcon->GetUpdateSQL($userrs, $data);
                        $this->dbcon->Execute($updateqry);
                    }
                }
            }
            $trainingdata = array();
            $trainingdata['f_status'] = $redeem_status;
            $trainingdata['f_userid'] = $params['userId'];
            $trainingdata['f_refid'] = $questionid;
            $trainingdata['f_reftypeid'] = $params['reftype'];
            $trainingdata['f_points'] = $params['value'];
            $trainingdata['f_groupid'] = $groupid;
            $trainingdata['f_phaseid'] = $phaseid;
            $trainingdata['f_creditdttm'] = $save_date;
            $trainingdata['f_trainingid'] = 0;
            //On training may be changed
            $trainingdata['f_trainingtype'] = TRAINING_TYPE_CARDIO;
            //In Future may be changed
            $achivepoints_rs = $this->dbcon->Execute(
                GET_TRAINING_ACHIVEPOINTS, 
                array(
                    $params['userId'], 
                    $check_date, 
                    $questionid, 
                    $params['reftype']
                )
            );
            if ($achivepoints_rs->RowCount()) {
                $trainingdata['f_moddttm'] = date('Y-m-d H:i:s');
                $achivepointsquery = $this->dbcon->GetUpdateSQL($achivepoints_rs, $trainingdata);
            } else {
                $trainingdata['f_crdttm'] = date('Y-m-d H:i:s');
                $achivepointsquery = $this->dbcon->GetInsertSQL(
                    $achivepoints_rs, $trainingdata
                );
            }
            $this->dbcon->Execute($achivepointsquery);
            //
            //f_userid,f_trainingid,f_trainingtype,f_phaseid,f_groupid,
            //f_status,f_creditdttm,f_points,f_refid,f_reftypeid,
            //f_crdttm,f_moddttm
            return $this->loadClientQuestionaries(
                array(
                'userId' => $params['userId'],
                )
            );/**/
            //return $this->status;
        } catch (Exception $e) {
            $this->status = array(
                'status' => 'failed', 
                'status_code' => 0, 
                'status_message' => 'Exception: '.$e->getMessage()
            );
        }

        return $this->status;
    }
    
    /**
    * To update user phase
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function updateUserPhase($params)
    {
        try {
            $phase_rs = $this->dbcon->Execute(
                GET_USER_PHASE, 
                array(
                    $params['userId'], 
                    $params['phaseId'], 
                    $params['groupId']
                )
            );
            $data = array();
            if ($phase_rs->RecordCount()) {
                $data['modified_date'] = date('Y-m-d H:i:s');
                if (isset($params['completed'])) {
                    $data['completed_date'] = date('Y-m-d H:i:s');
                }                
                $rsUpdates = $this->dbcon->GetUpdateSQL($phase_rs, $data);
                $this->dbcon->Execute($rsUpdates);
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'User Phase Updated Successfully',
                );
            } else {
                $data['userId'] = $params['userId'];
                $data['phaseId'] = $params['phaseId'];
                $data['groupId'] = $params['groupId'];
                $data['created_date'] = date('Y-m-d H:i:s');
                $this->dbcon->Execute(
                    INSERT_USER_PHASE, 
                    array(
                        $data['userId'], 
                        $data['phaseId'], 
                        $data['groupId'], 
                        $data['created_date'], 
                        $data['created_date']
                    )
                );
                //if($insertRs->LastInsertID()){
                    $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'User Phase Inserted Successfully',
                    );
                    /*} else {
                    $this->status = array(
                        "status" => "failed",
                        "status_code" => 404,
                        "status_message" => "Failed to insert pase",
                    );
                    }*/
            }
            /*Update / Add Reminder*/
            if(isset($params['dailyTimer'])) { 
                $updatedstatus =   $this->UpdateActivityReminderForActivity(
                    array(
                        'timeOn'=>$params['dailyTimer'],
                        'phaseId'=>$params['phaseId'],
                        'groupId'=>$params['groupId'],
                        'userId'=>$params['userId']
                    )
                );
                if($updatedstatus){
                    //required fixes on time updated
                }
            }
        } catch (Exception $e) {
            $this->status = array(
                'status' => 'failed', 
                'status_code' => 0, 
                'status_message' => 'Exception: '.$e->getMessage()
            );
        }

        return $this->status;
    }
    /**
    * To serach coach and club
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function searchCoachAndClub($params)
    {
		
        try {
            /*MK - Later Need to be updated with the Club Location Limit*/
            $club_rs = $this->dbcon->Execute(GET_CLUB_LIST, array($params['companyId']));
            $result = array();
            $clubs = array();
            if ($club_rs->RecordCount()) {
                $clubs = $club_rs->GetRows();
            }
            foreach ($clubs as $club) {
                $rowdata = $club;
                $coach_rs = $this->dbcon->Execute(GET_CLUB_COACHES, array($club['club_id']));
                $rowdata['_coachcount'] = 0;
                $rowdata['coachlist'] = array();
                $rowdata['citycountry'] = array();
				if(!empty($coach_rs)){
                if ($coach_rs->RecordCount()) {
                    $rowdata['_coachcount'] = $coach_rs->RecordCount();
                    $rowdata['coachlist'] = $coach_rs->GetRows();
                }
				}
                $result[] = $rowdata;
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'data' => $result,
                'status_message' => 'Result got successfully', );
        } catch (Exception $e) {
            $this->status = array(
                'status' => 'failed', 
                'status_code' => 0, 
                'status_message' => 'Exception: '.$e->getMessage()
            );
        }

        return $this->status;
    }
    
    /**
    * To update Answers From Previous Day
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    /*public function updateAnswersFromPreviousDay($params)
    {
        $mvkeys = json_decode($params['mvkeys'], true);
        $date =date('Y-m-d');
        if(isset($params['answeredDate'])) {
            $date =$params['answeredDate'];
        }
        //date('Y-m-d',strtotime("-1 day"));
        //$date    =    date('Y-m-d',strtotime("-1 day"));
        //$response['mvdata']    =    array();
        foreach ($mvkeys as $mvkey) {
            $rsobj = $this->dbcon->Execute(
                GET_PREVIOUS_ANSWERS, 
                array($mvkey, $params['userId'], $date)
            );
            if ($rsobj->RecordCount()) {
                $mvdata = $rsobj->fields;
                unset($mvdata['id']);
                //$mv_date = $mvdata['date'];
                $mvdata['date'] = date('Y-m-d H:i:s');
                $mvins_qry = $this->dbcon->GetInsertSql($rsobj, $mvdata);
                $insertid = $this->dbcon->Insert_ID();
                $this->dbcon->Execute($mvins_qry);
                $mvactdata_rs = $this->dbcon->Execute(
                    GET_INSERTED_MVACTUAL_POINTS, array($insertid)
                );
                if ($mvactdata_rs->RecordCount()) {
                    $mvactdata = $mvactdata_rs->fields;
                    /*unset to update New Data* /
                    unset($mvactdata['mvactanswer_id']);
                    $mvactdata['actmv_date'] = date('Y-m-d H:i:s');
                    $mvactdata_qry = $this->dbcon->GetInsertSql(
                        $mvactdata_rs, $mvactdata
                    );
                    $this->dbcon->Execute($mvactdata_qry);
                }
            }
        }

        return $this->loadClientQuestionaries(
            array(
            'userId' => $params['userId'],
            )
        );
        //return $this->status;
    }*/
    /*MK Added Questions By Group And Phase ID*/
    /**
    * To get Questions By GroupId and PhaseId
    *
    * @param int $groupid service parameter, group id 
    *
    * @param int $phaseid service parameter, phase id
    *
    * @param array $userAnswers service parameter, answer array 
    *
    * @param array $questionlang service parameter, quest lang 
    *
    * @return array object obj
    */
    public function getQuestionsByGroupIdPhaseId($groupid, $phaseid, $userAnswers, $questionlang)
    {
		  $question_rs = $this->dbcon->Execute(GET_QUESTIONARIES_BY_GROUPPHASE, array($groupid, $phaseid));
          $result = array();
        $result['_questions'] = array();
        $result['_questionscount'] = 0;
        //$redeem = array();
        if ($question_rs->RecordCount()) {
            $result['_questionscount'] = $question_rs->RecordCount();
            $questions = $question_rs->GetRows();
            //changed
            foreach ($questions as $question) {
                $qmvid  =   $question['monitorvariable_id'];
                $quesid  =   $question['id'];
                $quesoption_rs = $this->dbcon->Execute(
                    GETQUESTION_OPTIONS, 
                    array($quesid)
                );
                $question['langcount'] = 0;
                $question['lang'] = array();
                if (isset($questionlang['formelmid_'.$quesid])) {
                    $question['langcount']
                        =count($questionlang['formelmid_'.$quesid]);
                    $question['lang'] = $questionlang['formelmid_'.$quesid];
                }
                $question['_options'] = array();
                $cntrows = 0;
                if ($quesoption_rs->RecordCount() > 0) {
                    $cntrows = $quesoption_rs->RecordCount();
                    ///$question['_options']        =    $quesoption_rs->GetRows();
                    $options = $quesoption_rs->GetRows();
                    foreach ($options as $eachoption) {
                        $eachoption['_optlangcount'] = 0;
                        $eachoption['_optlang'] = array();
                        $optionlang_rs = $this->dbcon->Execute(
                            GET_OPTION_LANGBY_OPTION, array($eachoption['id'])
                        );
                        if ($optionlang_rs->RecordCount()) {
                            $eachoption['_optlangcount'] = $optionlang_rs->RecordCount();
                            $optlang = $optionlang_rs->GetRows();
                            $optionlang = array();
                            foreach ($optlang as $elang) {
                                $optionlang['lang_'.$elang['language_id']] = $elang;
                            }
                            $eachoption['_optlang'] = $optionlang;
                        }
                        $question['_options'][] = $eachoption;
                    }
                }
                $question['_optionscounts'] = $cntrows;
                $question['_answers'] = array();
                $question['_answercount'] = 0;
                //changed
                
                if (isset($userAnswers['mv_'.$qmvid])) {
                    $question['_answers'] = $userAnswers['mv_'.$qmvid];
                    $question['_answercount'] =count($userAnswers['mv_'.$qmvid]);
                }
                $topicId = $question['r_topic_id'];
                $question['prameter_value'] = 1;
                $dstopic_rs = $this->dbcon->Execute(
                    GET_TOPIC_DESEASE_PARAMS, array($topicId)
                );
                if ($dstopic_rs->RecordCount()) {
                    $dstopic_record = $dstopic_rs->fields;
                    $question['prameter_value'] = $dstopic_record['value'];
                }
                $question['parameter_goal'] = 1;
                $dstopic_rs = $this->dbcon->Execute(
                    GET_TOPIC_GOAL_PARAMS, array($topicId)
                );
                if ($dstopic_rs->RecordCount()) {
                    $dstopic_record = $dstopic_rs->fields;
                    $question['parameter_goal'] = $dstopic_record['value'];
                }
                $result['_questions'][] = $question;
                unset($quesoption_rs);
            }
        }

        return $result;
    }
    
    /**
    * To update achieved points
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function updateAchivePoints($params)
    {
        /*Now used only for Change Status may be extented later*/
        $rsobj = $this->dbcon->Execute(
            GET_ACHIVE_POINTS_BYUSER_STATUS, 
            array($params['userId'], REDEEM_STATUS_redeemable)
        );
        if ($rsobj->RecordCount()) {
            $data['f_status'] = REDEEM_STATUS_redeemed;
            $updateSql = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($updateSql);
        }

        return $this->loadClientQuestionaries(
            array(
            'userId' => $params['userId'],
            )
        );
    }

    
    /**
    * To get Weeks based training information
    *
    * @param datetime $date1 service parameter, Search arguments
    *
    * @param datetime $date2 service parameter, Search arguments
    *
    * @param string $selectdate service parameter, Search arguments
    *
    * @param int $weekidx service parameter, Search arguments 
    *
    * @param array $trainingplan service parameter, Search arguments 
    *
    * @param array $pointdatas service parameter, Search arguments 
    *
    * @return array $weekarry
    */
    public function getWeeksbasedtraininginformation(
        $date1, $date2, $selectdate, &$weekidx, $trainingplan, $pointdatas
    ) {
		
        $seldateobj = strtotime($selectdate);
        $date1obj = strtotime($date1);
        $date2obj = strtotime($date2);
        $weekarry = array();
        $incdate = 1;
        while ($date1obj <= $date2obj) {
            $weekstart = date('Y-m-d', $date1obj);
            $weekendobj = strtotime('+6 day', $date1obj);
            $weekend = date('Y-m-d', $weekendobj);
            if (($date1obj <= $seldateobj) && ($weekendobj >= $seldateobj)) {
                $weekidx = $incdate;
				
            }
            $training = $trainingplan[$incdate];
            $training['min'] = $weekstart;
            $training['max'] = $weekend;

            $totalpointsarr = array();
            $totalweekpoints = 0;
            $daypoints = array();
            foreach ($pointdatas as $pointdata) {
                $pointcrdt = $pointdata['f_creditdttm'];
                $pointcrdtobj = strtotime($pointcrdt);
                if (
                    ($date1obj <= $pointcrdtobj) 
                    && ($weekendobj >= $pointcrdtobj)
                ) {
                    $totalweekpoints += $pointdata['f_points'];
                    $totalpointsarr[] = $pointdata;
                    $pointdate = date('Ymd', $pointcrdtobj);
                    if (!isset($daypoints[$pointdate])) {
                        $daypoints[$pointdate] = array();
                        $daypoints[$pointdate]['date']
                            =date('Y-m-d', $pointcrdtobj);
                        $daypoints[$pointdate]['point'] = 0;
                    }
                    $daypoints[$pointdate]['point'] += $pointdata['f_points'];
                }
            }
            $training['daypoints'] = $daypoints;
            $training['pointdet'] = $totalpointsarr;
            $training['totalpoints'] = $totalweekpoints;

            //$weekarry[$incdate]    = array('min'=>$weekstart, 'max'=>$weekend);
            $weekarry[$incdate] = $training;
            $date1obj = strtotime('+7 day', $date1obj);
            ++$incdate;
        }
        /*
        for($incweek = $date1obj; $incweek <= $date2obj; $incweek=$incweek+7){
            //$weekarry[$incdate] = $incdate;
            //.'-'.date('Y-m-d', $date1obj)." - ".date('Y-m-d', $date2obj);
            $incdate++;
            if ($incdate > 14){
                break;
            }
            
        }*/
        return $weekarry;
    }
    /*MK - Reset User Data*/
    /**
    * To reset User Data For Testing Purpose
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function resetUserDataForTestingPurpose($params)
    {
        if ($params['userId'] == RESET_USER_test) {
            $params['startdate'] = date('Y-m-d H:i:s');
            $this->dbcon->Execute(
                RESET_AVAILABLE_TEST, 
                array(
                    $params['startdate'], 
                    $params['userId']
                )
            );
        } elseif ($params['userId'] == RESET_USER_Training) {
            $this->dbcon->Execute(RESET_TRAINING, array($params['userId']));
        } elseif ($params['userId'] == RESET_USER_Analysed) {
            $this->dbcon->Execute(RESET_ANAYLSIS, array($params['userId']));
        } elseif ($params['userId'] == RESET_USER_checkfase) {
            $rsobj = $this->dbcon->Execute(GET_MONITOR_VARIABLES_BYPHASEID, array(PHASE_Check));
            if ($rsobj->RecordCount() > 0) {
                $data = $rsobj->GetRows();
                $this->resetMVDataAnswers(
                    $data, 
                    QUESTIOARIES_TYPE_question, 
                    $params['userId']
                );
                $this->resetCompletedPhase($params['userId']);
                $this->updateUser($params['userId'], array('app_group_preference' => 0));
            }
        } elseif ($params['userId'] == RESET_USER_MVS_fase1) {
            $rsobj = $this->dbcon->Execute(GET_ACTIVITY_MVDATA_ID);
            if ($rsobj->RecordCount() > 0) {
                $data = $rsobj->GetRows();
                /*Run Update script*/
                $this->resetMVDataAnswers(
                    $data, 
                    QUESTIOARIES_TYPE_activity, 
                    $params['userId']
                );
                $this->resetCompletedPhase(
                    $params['userId'], 
                    PHASE_phase1, 
                    GROUP_MOVESMART
                );
                $this->updateUser($params['userId'], array('r_club_id' => 0));
            }
        } elseif ($params['userId'] == RESET_USER_EF_fase1) {
            $rsobj = $this->dbcon->Execute(
                GET_PHASE_GROUP_QUESTION_MONITOR_ID, 
                array(GROUP_EATFRESH, PHASE_phase1)
            );
            if ($rsobj->RecordCount() > 0) {
                $data = $rsobj->GetRows();
                /*Run Update script*/
                $this->resetMVDataAnswers(
                    $data, 
                    QUESTIOARIES_TYPE_question, 
                    $params['userId']
                );
                $this->resetCompletedPhase(
                    $params['userId'], 
                    PHASE_phase1, 
                    GROUP_EATFRESH
                );
            }
        } elseif ($params['userId'] == RESET_USER_MS_fase1) {
            $rsobj = $this->dbcon->Execute(
                GET_PHASE_GROUP_QUESTION_MONITOR_ID, 
                array(GROUP_MINDSWITCH, PHASE_phase1)
            );
            if ($rsobj->RecordCount() > 0) {
                $data = $rsobj->GetRows();
                /*Run Update script*/
                $this->resetMVDataAnswers(
                    $data, 
                    QUESTIOARIES_TYPE_question, 
                    $params['userId']
                );
                $this->resetCompletedPhase(
                    $params['userId'], 
                    PHASE_phase1, 
                    GROUP_MINDSWITCH
                );
            }
        }
        $this->status = array('status' => 'success');

        return $this->status;
    }
    
    /**
    * To reset monitor activity
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function resetMonitorActivity($params)
    {
        if (($params['userid'] != '')) {
            $rsobj = $this->dbcon->Execute(
                GET_MVDATA_ID, 
                array($params['userid'], $params['mvid'])
            );
            $mvdataid = 0;
            if ($rsobj->RecordCount() > 0) {
                $mvdata = $rsobj->fields;
                $mvdataid = $mvdata['id'];
                $params['activitytypeid'] = 2;
                $this->dbcon->Execute(RESET_MONITOR_VARIABLE, 
                    array($params['userid'], $params['mvid'])
                );
                $this->dbcon->Execute(
                    RESET_MONITORANSWER_VARIABLE, 
                    array($params['userid'], $mvdataid)
                );
                $this->dbcon->Execute(
                    RESET_TRAINING_ACEIEVE_POINTS, 
                    array(
                        $params['userid'], 
                        $params['activityid'], 
                        $params['activitytypeid']
                    )
                );
            }
            $this->status = array(
                'status_code' => 200,
                'status_message' => 'Updated successfully',
                'mvdataid' => $mvdataid,
            );
        } else {
            $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to update device',
            );
        }

        return $this->status;
    }
    
    /**
    * To reset MV data answers
    *
    * @param array $data service parameter, Search arguments 
    *
    * @param int $questiontype service parameter, Search arguments 
    *
    * @param int $userid service parameter, Search arguments 
    *
    * @return array void
    */
    public function resetMVDataAnswers($data, $questiontype, $userid)
    {
        foreach ($data as $ques) {
            $mvid = $ques['monitorid'];
            if ($mvid != '') {
                $rsobj = $this->dbcon->Execute(
                    GET_MVDATA_ID, array($userid, $mvid)
                );
                if ($rsobj->RecordCount() > 0) {
                    $mvdatarows = $rsobj->GetRows();
                    foreach ($mvdatarows as $mvdata) {
                        $mvdataid = $mvdata['id'];
                        $questionid = $ques['quesid'];
                        $this->dbcon->Execute(
                            RESET_MONITOR_VARIABLE, 
                            array($userid, $mvid)
                        );
                        $this->dbcon->Execute(
                            RESET_MONITORANSWER_VARIABLE, 
                            array($userid, $mvdataid)
                        );
                        $this->dbcon->Execute(
                            RESET_TRAINING_ACEIEVE_POINTS, 
                            array($userid, $questionid, $questiontype)
                        );
                    }
                }
            }
        }
    }
    
    /**
    * To reset completed phase
    *
    * @param int $userid service parameter, Search arguments 
    *
    * @param string $phaseid service parameter, Search arguments
    *
    * @param string $groupid service parameter, Search arguments
    *
    * @return void
    */
    public function resetCompletedPhase($userid, $phaseid = '', $groupid = '')
    {
        $query = "DELETE FROM t_ques_phase_user WHERE `r_user_id`='".$userid."'";
        if ($phaseid != '') {
            $query    .= " AND r_phase_id='$phaseid'";
        }
        if ($groupid != '') {
            $query .=    " AND r_group_id='".$groupid."'";
        }
        $this->dbcon->Execute($query);
    }
    
    /**
    * To remove reset user
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function removeResetUser($params)
    {
        $check_rs = $this->dbcon->Execute(
            CHECK_RESET_USERS, 
            array($params['email'])
        );
        if ($check_rs->RecordCount()) {
            $userdata = $check_rs->fields;
            $userid = $userdata['user_id'];
           // $data['email'] = $userdata['email'].
           //     '_removed_'.$params['loggedUserId'].'_'.$userid;
           // $data['first_name']=$userdata['first_name'].
          //      '_removed_'.$params['loggedUserId'].'_'.$userid;
		    $data['is_deleted'] = 1;
            $data['r_status_id'] = USER_REMOVED_STATUS_ID;
            $rsUpdateSql = $this->dbcon->GetUpdateSQL($check_rs, $data);
            $this->dbcon->Execute($rsUpdateSql);
            $this->status = array(
                'status_code' => 200,
                'status_message' => 'Removed successfully',
            );
        } else {
            $this->status = array(
                'status_code' => 404,
                'status_message' => 'Email does not exist',
            );
        }

        return $this->status;
    }
	
	
	 public function removecomplete($params)
    {
        $check_rs = $this->dbcon->Execute(
            CHECK_RESET_USERS, 
            array($params['email'])
        );
        if ($check_rs->RecordCount()) {
            $userdata = $check_rs->fields;
            $userid = $userdata['user_id'];
           // $data['email'] = $userdata['email'].
           //     '_removed_'.$params['loggedUserId'].'_'.$userid;
           // $data['first_name']=$userdata['first_name'].
          //      '_removed_'.$params['loggedUserId'].'_'.$userid;
		
            $rsDeleteSql = $this->dbcon->Execute(DELETE_RESET_USERS, array($userid));
            $this->dbcon->Execute($rsDeleteSql);
            $this->status = array(
                'status_code' => 200,
                'status_message' => 'Removed successfully',
            );
        } else {
            $this->status = array(
                'status_code' => 404,
                'status_message' => 'Email does not exist',
            );
        }

        return $this->status;
    }
    
    /**
    * To reset password
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function resetPassword($params)
    {
        $check_rs = $this->dbcon->Execute(
            CHECK_RESET_USERS, array($params['email'])
        );
        if ($check_rs->RecordCount()) {
            $userdata = $check_rs->fields;
             $userid = $userdata['user_id'];
			$first_name = $userdata['first_name'];
			$last_name = $userdata['last_name'];
            /*SN added reset active and reset password-20160505*/
            $data   =   array();
            if (isset($params['password'])) {
                $data['password'] = $params['password'];
            }
            if (isset($params['active'])) {
                $data['r_status_id'] = $params['active'];
            }
            $rsUpdateSql = $this->dbcon->GetUpdateSQL($check_rs, $data);
            $this->dbcon->Execute($rsUpdateSql);
            $this->status = array(
                'status_code' => 200,
                'status_message' => 'Updated successfully',
            );
			
				$params['subject'] = 'Movesmart - Activation';
			$imgtoplogo = BASE_URL.'images/mailtoplogo.png';
			$body = "<div style='padding: 0 10px' >
						<img src='".$imgtoplogo."' alt='logo' />
					</div>
					<br /><br />
					Beste ".$first_name." ".$last_name.",<br><br>
					<br>Dankjewel voor je registratie bij bij FeelGoodClub!
					<br />uw e-mail wordt geactiveerd<br /><br/>
					Login: ".$params['email']."<br />					
					<br/>
					<br/>
					Met vriendelijke groet,<br /><br />
					MOVESMART.company<br />T.+32(0)468 1941 85<br />
					M.<a href='mailto:support@movesmart.company'>support@movesmart.company</a>";
			
			
			
			$mail = new PHPMailer();
		
			//Enable SMTP 
			 //$mail1->IsSMTP(); 
			  $mail->SMTPDebug = true;  // debugging: 1 = errors and messages, 2 = messages only
			  $mail->SMTPAuth = true;  // authentication enabled
			  $mail->Host = 'mail.movesmart.offshoresolutions.nl';
			  $mail->Port = 25;
			  $mail->Username = 'movesmartinfo@movesmart.offshoresolutions.nl';
			  $mail->Password = 'welcome@108'; 
		  
			$mail->setFrom('movesmartinfo@movesmart.offshoresolutions.nl');
			$mail->addAddress($params['email']); 
			$mail->Subject = $params['subject'];
			
			$mail->msgHTML($body);
			$mail->addCC("babitakapoor.immanentsolutions@gmail.com");
			$mail->addBCC("prasenjeetd.immanentsolutions@gmail.com");
        	if (!$mail->send()) 
			{
					echo 'Mailer Error: '.$mail->ErrorInfo;
			}
			
			
        } else {
            $this->status = array(
                'status_code' => 404,
                'status_message' => 'Email does not exist',
            );
        }

        return $this->status;
    }
    
    /**
    * To update user
    *
    * @param int $userid service parameter, Search arguments 
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return void
    */
    public function updateUser($userid, $params)
    {
        $set = '';
        foreach ($params as $key => $value) {
            $set .= "`$key`='$value', ";
        }
        if ($set != '') {
            $set = rtrim($set, ',');
            $query = "UPDATE t_users SET $set WHERE user_id='$userid'";
            $this->dbcon->Execute($query);
        }
    }
    
    /**
    * To tion typeget max by qu
    *
    * @param int $qid service parameter, Search arguments 
    *
    * @param int $qtype service parameter, Search arguments 
    *
    * @param array $qtfoptions service parameter, Search arguments 
    *
    * @return array obj
    */
    public function GetMaxByQuestionType($qid, $qtype, $qtfoptions)
    {
		
        if ($qid) {
        }
        $options = array();
        $options['option_maxval'] = 0;
        switch ($qtype) {
        case QUESTION_TYPE_NUMBER:
            $max = 0;
			
            foreach($qtfoptions as $eoption) {
              
                if (
                    (
                        (!isset($eoption['max_value'])) 
                        or ($eoption['max_value'] == '') 
                        or ($eoption['max_value'] == 0)
                    ) 
                    and ($eoption['min_value'] != 0) 
                    and ($eoption['min_value'] != 0)
                ) {
                    $max = $eoption['range_points'];
                } else {
                    $max = $eoption['max_value'] * $eoption['range_points'];
                }
            }
            $options['option_maxval'] = $max;
            break;
            /*Same Way the Range points are 
                calculated for the Smiley,Yes/No, Options Type*/
        case QUESTION_TYPE_OPTION:
        case QUESTION_TYPE_YN:
        case QUESTION_TYPE_SMILEY:
        case QUESTION_TYPE_LIKDISLIK:
            $compare_arr = array();
            foreach ($qtfoptions as $option) {
                array_push($compare_arr, $option['max_value']);
            }
            $max = max($compare_arr);
            $options['option_maxval'] = $max;
            break;
        case QUESTION_TYPE_CHECKBOX:
            $max = 0;
            foreach ($qtfoptions as $option) {
                $max += $option['max_value'];
            }
            $options['option_maxval'] = $max;
            break;
        }
        return $options;
    }
    /**
    * To update Reminder Time By User  Phase Group
    *
    * @param array $params service parameter, Key:UserId,GroupId,PhaseId;
    *   Data:timeOn
    *
    * @return array obj
    */
    public function UpdateActivityReminderForActivity($params){
        $rsreminder=$this->dbcon->Execute(GET_USER_PHASE_REMINDER,array(
            $params['phaseId'],$params['groupId'],$params['userId']
        ));
        $statusmessage  ='Inserted Successfully';
        if($rsreminder->RecordCount()) {
            $data['f_remindme_on']  =   $params['timeOn'];
            $data['f_modifed_on']   =   date('Y-m-d H:i:s');
            $sqlUpdate=$this->dbcon->GetUpdateSQL($rsreminder,$data);
            $this->dbcon->Execute($sqlUpdate);
            $statusmessage  ='Updated Successfully';
        } else {
            $data['f_user_id']  =   $params['userId'];
            $data['f_group_id'] =   $params['groupId'];
            $data['f_phase_id'] =   $params['phaseId'];
            $data['f_remindme_on']  =   $params['timeOn'];
            $data['f_created_on']   =   date('Y-m-d H:i:s');
            $sqlInsert=$this->dbcon->GetInsertSQL($rsreminder,$data);
            $this->dbcon->Execute($sqlInsert);
            //$statusmessage  ='Inserted Successfully';
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => $statusmessage
        );
        return $this->status;
    }
    //*note* function to save day wise score in db by Jeet, 6 Mar
    //added code
    /* public function saveUserScoreInDb($userdata){
            //return $userdata;
		$currentindexobj = [];
		$groupobjs = $userdata;
		$totalindex = 0;
		
		foreach($groupobjs as $i => $groupobj) {
			$totalpoint = 0;
			$maxcreditindex = 0;
			$maxcreditlimit = 0;
			$creditindex = 0;
			if (isset($groupobj["_phase"])) {
				foreach($groupobj["_phase"] as $groupobjphase) {
					if($groupobjphase["_creditinfocount"] > 0){
						$maxcreditindex = $groupobjphase["_creditinfo"][0]["index_weightage"];
						$maxcreditlimit = $groupobjphase["_creditinfo"][0]["max_weightage"];
						$_questions = $groupobjphase["_questionscount"] == 1 ? [$groupobjphase["_questions"]] : $groupobjphase["_questions"];
						foreach($_questions as $questionobj) {
							if(isset($questionobj["_answercount"]) && $questionobj["_answercount"] > 0 ){
								$answers = $questionobj["_answercount"] == 1 ? [$questionobj["_answers"]] : $questionobj["_answers"];
								foreach($answers as $answer) {
									if (isset($answer) && (isset($answer[0]["value"]))) {
										$totalpoint += $answer[0]["value"];
									}  
								}   
							} 
						}	
					}else{
						if ($groupobjphase["_activitiescount"] > 0) {
							$activityobjs = $groupobjphase["_activities"];
							if ($groupobjphase["_activitiescount"] == 1) {
								$activityobjs = [$activityobjs];
							} 
							//maxcreditlimit //Need to find out how
							//maxcreditindex //Need to find out from the days they started this activity
							foreach($activityobjs as $activityobj) {
								if ((isset($activityobj["_answerscount"])) && ($activityobj["_answerscount"] > 0)) {
									$answers = $activityobj["_answerscount"] == 1 ? [$activityobj["_answers"]] : $activityobj["_answers"];
									print_r($answers);
									.$points = ($activityobj["_pointscount"] == 1) ? $activityobj["_points"] : $activityobj["_points"][0];
									foreach($answers as $answer) {
										if (isset($answer) && (isset($answer[0]["value"]))){
											totalpoint += $answer[0]["value"];
										}
									});
									//maxcreditlimit	=	GetAcitvityDashBoardData(answers,points,activityobj.activitysteps);
									maxcreditlimit = GetAcitvityDashBoardData(answers, points);
									maxcreditindex = maxcreditlimit 
								}else
								{
									echo "false";
								} 
							} 
						}
					}



				}
			}
		}
		    
    }  */
    //code end
};
function custom_multisort($array, $akey , $order=SORT_ASC) {
    $sroter		=	array();
    foreach($array as $key=>$rw){
        $sroter[$key]	=	$rw[$akey];
    }
    array_multisort($sroter,$order,$array);
    return $array;
}

