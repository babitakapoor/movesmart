<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Test
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle test related actions.
 */
require_once SQL_PATH.DS.'test.php'; 
/**
 * Class to handle test related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class testModel
{
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );

        $this->error_general = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
        );
    }
    
    /**
    * Returns an json obj of get Evolution List for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getEvolutionList($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_EVOLUTIONLIST_QUERY, array($userId));
        if ($rsobj->RecordCount()) {
            $evolutionList  =   array();
            while (!$rsobj->EOF) {
                $evolutionList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get evolution Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'evolutionList' => $evolutionList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get evolution Info Details Not Found.',
                'total_records' => 0,
                'evolutionList' => '',
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of insert temporary adjust sportmed data for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertTmpAdjustSportmedData($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TEST_RESULT_DATA_EXIST, array($params['userId'], $params['userTestId']));

        $data = array('tmp_last_adjust_sportmed_graph_data' => $params['tmp_adjust_sportmed_data']);
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Update temporary adjust sportmed data success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Update temporary adjust sportmed data error',
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get temporary adjust sportmed data for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTmpAdjustSportmedData($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TEST_RESULT_DATA_EXIST, array($params['userId'], $params['userTestId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'get temporary adjust sportmed data success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'get temporary adjust sportmed data error',
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get test sport specific data for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getSportSpecificData($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TEST_SPORT_SPECIFIC_DATA, array($params['userId'], $params['userTestId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get Test Sport Specific Data Success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Test Sport Specific Data Failed',
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get Test Items for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTestItems($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TEST_ITEMS_BY_ID, array($params['testId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get Test items Success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Test items Failed',
            );
        }
        //Return the result array    
        return $this->status;
    }

    /**
    * Returns an json obj of get Test Result Info a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTestResultInfo($params)
    {
        $params['userId'] = isset($params['user_test_id']) ? $params['user_test_id'] : $params['userId'];
        $arr = array();
        if (isset($params['getTestDetail'])) {
            $qry = GET_TEST_RESULT_BY_TEST_ID;
            $arr = array($params['userId'], $params['testId']);
        } else {
            $testStatus = '';
            if (isset($params['testStatus'])) {
                $testStatus = ' IN('.$params['testStatus'].')';
            }
            if (isset($params['testStatusNotIn'])) {
                $testStatus = ' NOT IN('.$params['testStatusNotIn'].')';
            }
            $qry = "SELECT *, 
                    IF(U.gender=0,'male','female') AS gender,
                    s.status,UT.test_level AS user_test_level,
                    UT.status current_test_status 
              FROM t_users AS U 
                LEFT JOIN t_user_test UT ON UT.r_user_id = U.user_id 
                    AND UT.status $testStatus 
                    AND UT.user_test_id = (
                      SELECT MAX(user_test_id) FROM t_user_test WHERE r_user_id=U.user_id AND status $testStatus) 
                LEFT JOIN t_user_test_parameter AS t ON t.r_user_test_id = UT.user_test_id 
                LEFT JOIN t_status AS s ON s.status_id = U.r_status_id 
                LEFT JOIN t_personalinfo AS p ON p.r_user_id = U.user_id 
                LEFT JOIN t_coach_member AS cm ON cm.r_user_id = U.user_id 
                LEFT JOIN t_test AS test ON test.test_id = UT.r_test_id 
              WHERE U.user_id=".$params['userId'].' order by UT.user_test_id DESC LIMIT 1 ';
             // echo $qry oreder_by t_user_test.user_test_id DESC;
             
			 
        }

        $rsobj = $this->dbcon->Execute($qry, $arr);
        if ($rsobj->RecordCount()) {
            $testResultInfoArr  =   array();
            while (!$rsobj->EOF) {
                $testResultInfoArr[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Test Result Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'testResultInfoArr' => $testResultInfoArr,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Test Result Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array  
        //echo "data found";
     //  print_r($this->status);  
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get Evolution Fitness Level a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getEvolutionFitnessLevel($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_EVOLUTIONFITNESSLIST_QUERY, array($userId));
        if ($rsobj->RecordCount()) {
            $evolFitLevel   =   array();
            while (!$rsobj->EOF) {
                $evolFitLevel[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get evolution Fitness Level Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'evolutionFitnessLevel' => $evolFitLevel,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get evolution Fitness Level Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Returns an json obj of get Evolution Bio Metrics  a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getEvolutionBioMetrics($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_EVOLUTIONBIOMETRICS_QUERY, array($userId));
        if ($rsobj->RecordCount()) {
            $evolutionBioMetrics    =   array();
            while (!$rsobj->EOF) {
                $evolutionBioMetrics[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get evolution Bio Metric Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'evolutionBioMetrics' => $evolutionBioMetrics,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get evolution Bio Metric Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get User With Club Id  a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getUserWithClubId($params)
    {
        $category = array();
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $statusId = isset($params['statusId']) ? $params['statusId'] : '1,2,3,4,5';
        $qry = GET_USERLISTWITHCLUB_DROPDOWN;
        $qry .= "AND r_club_id IN($clubId) AND r_status_id IN($statusId)";
        $rsobj = $this->dbcon->Execute($qry);

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'User List Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User List Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userList' => $category,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get Email With User Id a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getEmailWithUserId($params)
    {
        $category = array();
        $currentValue = isset($params['currentValue']) ? $params['currentValue'] : '';
        $rsobj = $this->dbcon->Execute(GET_EMAILLISTWITHUSER_DROPDOWN, array($currentValue));

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Email List Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'emailList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Email List Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'emailList' => $category,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get the Heart Rate values a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getHeartRateGraph($params)
    {
        $category = array();
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        if ($userTestId > 0) {
            $rsobj = $this->dbcon->Execute(GET_HEARTRATEUSERVALUE_GRAPH, array($userId, $userTestId));
        } else {
            $rsobj = $this->dbcon->Execute(GET_HEARTRATEUSERVALUE, array($userId));
        }
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Members For Heart Rate Retrieved',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'heartGraphList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Members For Heart Rate Not Found',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'heartGraphList' => $category,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get the All test Details a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTestList($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_TEST_QUERY);
        if ($rsobj->RecordCount()) {
            $testList   =   array();
            while (!$rsobj->EOF) {
                $testList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Test List Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'testList' => $testList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Test List Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of Save the Test Result a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function saveTestResult($params)    
    {
	
        $currentUserId = isset($params['currentuserId']) ? $params['currentuserId'] : '';
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
		    
        $rsobj = $this->dbcon->Execute(GET_USER_TEST_RESULT, array($userTestId, $currentUserId));
        $dateTime = date('Y-m-d H:i:s');

        $data = array(
                'r_user_id' => $currentUserId,
                'r_club_id' => isset($params['clubId']) ? $params['clubId'] : '',
                'r_test_id' => isset($params['testOptions']) ? $params['testOptions'] : '',
                'test_level' => isset($params['testLevel']) ? $params['testLevel'] : '',
                'test_date' => isset($params['testDate']) ? $params['testDate'] : ''
        );
        
        if (isset($params['totalLoadValue'])) 
        {
            $data['total_load_value'] = $params['totalLoadValue'];
        }
        
		$data['load_value'] ='';
        if ($rsobj->RecordCount()) 
        {
            $data['modified_by'] = isset($params['loggedinUserId']) ? $params['loggedinUserId'] : '';
            $data['modified_date'] = $dateTime;
			//$r_test_id=$data['r_test_id'];
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
			//echo  $rsUpdates;
            $this->dbcon->Execute($rsUpdates);
			// $str="UPDATE `t_user_test` SET r_test_id=$r_test_id WHERE user_test_id=$userTestId AND //r_user_id=$currentUserId AND status!=3";   
			//  mysql_query($str);	
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User Details Successfully Updated',
                'userTestId' => $rsobj->fields['user_test_id'],
            );
        } 
        else 
        {
			if(isset($params['newTest']))
            {
                $data['created_by'] = isset($params['loggedinUserId']) ? $params['loggedinUserId'] : '';
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
				$this->dbcon->Execute($rsInserts);
             }
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User Details Successfully Added.',
                'userTestId' => $this->dbcon->Insert_ID(),
            );
        }

        return $status;
    }
    
    /**
    * Returns an json obj of Save Test Parameters a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */    
    public function saveTestParameters($params)
    {   
      
        $currentUserId = isset($params['currentuserId']) ? $params['currentuserId'] : '';
				
        $testId = isset($params['userTestId']) ? $params['userTestId'] : '';
		
        $dateTime = date('Y-m-d H:i:s');
        $status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Invalid user test is passed.',
        );  
        /*User Test Parameters*/
        if ($testId!='') {
			
            $rsTest = $this->dbcon->Execute(GET_USER_TEST_PARAMETER, array($currentUserId, $testId));
			       
             $testdate = isset($params['newTestDate']) ? $params['newTestDate'] : '';
           // $testdate = isset($params['testDate']) ? $params['testDate'] : '';
            $testtime = isset($params['testTime']) ? $params['testTime'] : '';
            $dateTest = $testdate.' '.$testtime;
            $testData = array(
                    'r_user_id' => $currentUserId,
                    'r_user_test_id' => $testId,
                    'age_on_test' => isset($params['ageOnTest']) ? $params['ageOnTest'] : '',
                    'test_level' => isset($params['testLevel']) ? $params['testLevel'] : '',
                    'test_prepared_date' => isset($params['testPrepared']) ? $params['testPrepared'] : '',
                    'weight_on_test' => isset($params['weightOnTestDate']) ? $params['weightOnTestDate'] : '',
                    'start_load_test' => isset($params['startLoadTest']) ? $params['startLoadTest'] : '',
                    'conducted_by' => isset($params['coach']) ? $params['coach'] : '',
                    'fitness_level' => isset($params['fitnessLevel']) ? $params['fitnessLevel'] : '',
                    'test_date' => isset($params['testDate']) ? $params['testDate'] : '0000-00-00',
                    'status' => isset($params['testStatus']) ? $params['testStatus'] : 0,
                    'iant_hr' => isset($params['iantHR']) ? $params['iantHR'] : '',
                    'iant_vo2' => isset($params['iantVo2']) ? $params['iantVo2'] : '',
                    'max_hr' => isset($params['maxHR']) ? $params['maxHR'] : '',
                    'iant_p' => isset($params['iantP']) ? $params['iantP'] : '',
                    'relative_iant_vo2' => isset($params['relativeIantVo2']) ? $params['relativeIantVo2'] : '',
                    'max_p' => isset($params['maxP']) ? $params['maxP'] : '',
                    'relative_iant_p' => isset($params['relativeIantP']) ? $params['relativeIantP'] : '',
                    'relative_max_p' => isset($params['relativeMaxP']) ? $params['relativeMaxP'] : '',
                    'next_appointment' => $dateTest,  
            );  
			 
            if ($rsTest->RecordCount()) {
                $testData['modified_by'] = isset($params['loggedinUserId']) ? $params['loggedinUserId'] : '';
                $testData['modified_date'] = $dateTime;
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsTest, $testData);
                //echo $rsUpdates;
                $this->dbcon->Execute($rsUpdates);
                //Set the status message
                $status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'User Test Parameters Successfully Updated.',
                );
            } else {
				 
                $testData['created_by'] = isset($params['loggedinUserId']) ? $params['loggedinUserId'] : '';
                $testData['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSQL($rsTest, $testData);
				  
                $this->dbcon->Execute($rsInserts);
                //Set the status message
                $status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'User Test Parameters Successfully Added.',
                );
            }
				
			
        }

        return $status;
    }

    /**
    * Returns an json obj of update Adjust Sportmed Data for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function updateAdjustSportmedData($param)
    {
        $userTestId = (isset($param['userTestId'])) ? $param['userTestId'] : '-1';

        $rsobj = $this->dbcon->Execute(GET_TEST_RESULT_EXIST_BY_TEST_ID, array($userTestId));
        if ($rsobj->RecordCount() > 0) {
            $data['adjust_sportmed_graph'] = $rsobj->fields['tmp_last_adjust_sportmed_graph_data'];
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Adjust sportmed data update success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Adjust sportmed data update failed',
            );
        }
    }

    
    /**
    * Returns an json obj of get Details For Training Work Load for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getDetailsForTrainingWorkLoad($params)
    {
        $userTestId = isset($params['user_test_id']) ? $params['user_test_id'] : '';
        $rsobj = $this->dbcon->Execute(
            GET_TRAINING_WORKLOAD_DETAILS,
            array($userTestId, $userTestId, $userTestId, $userTestId)
        );
        if ($rsobj->RecordCount()) {
            $trainingWorkLoad   =   array();
            while (!$rsobj->EOF) {
                $trainingWorkLoad[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Training WorkLoad Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'trainingWorkLoad' => $trainingWorkLoad,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Training WorkLoad Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Returns an json obj of get Training Work Load for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTrainingWorkLoad($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $workLoadId = isset($params['workLoadId']) ? $params['workLoadId'] : '';
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        if ($workLoadId) {
            $rsobj = $this->dbcon->Execute(GET_WORKLOAD, array($workLoadId));
        } else {
            $rsobj = $this->dbcon->Execute(GET_TRAINING_WORKLOAD, array($userId, $userTestId));
        }
        if ($rsobj->RecordCount()) {
            $trainingWorkLoad   =   array();
            while (!$rsobj->EOF) {
                $trainingWorkLoad[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Training WorkLoad Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'trainingWorkLoad' => $trainingWorkLoad,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Training WorkLoad Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of Save Training workload Details for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function saveTrainingWorkLoad($param)
    {
        $trainingId = (isset($param['trainingId'])) ? $param['trainingId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_WORKLOAD, array($trainingId));
        $data = array(
                'original_workload' => $param['oriWorkLoad'],
                'actual_workload' => $param['actWorkLoad'],
                );
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $rsUpdate = $this->dbcon->Execute($rsUpdates);

            if ($rsUpdate) {
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Training WorkLoad details updated successfully',
                );
            } else {
                $this->status = $this->error_general;
            }
        }

        return $this->status;
    }
     
    /**
    * Returns an json obj of get Sport Med Data For Graph for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getSportMedDataForGraph($params)
    {
        $userId = $params['userId'];
        $userTestId = $params['userTestId'];

        $rsobj = $this->dbcon->Execute(GET_TEST_SPORTMED_GRAPH_DATA, array($userId, $userTestId));

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Get sportmed data graph success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'resultset' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get sportmed data graph failed',
                // 'sql' => $rsobj->sql,
            );
        }

        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of cancel Member Test for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function cancelMemberTest($params)
    {
        $rsobj = $this->dbcon->Execute(
            CANCEL_MEMBER_TEST_EXIST, array($params['userId'], $params['userTestId'])
        );
        $this->status = $this->error_general;
        if ($rsobj->RecordCount()) {
            $data['status'] = 30;
            $data['reason'] = 'Cancelled';
            $data['is_test_completed'] = 'cancelled';

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $rsUpdate = $this->dbcon->Execute($rsUpdates);

            if ($rsUpdate) {
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Member Test Cancel Success',
                );
            }
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of change Test Status To Interrupt for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function changeTestStatusToInterrupt($params)
    {
        $userTestId = $params['userTestId'];
        $kcal = $params['kcal'];
        $rsobj = $this->dbcon->Execute(UPDATEUSERTEST_INTERRUPT, array($userTestId));
        $isUpdated  =   0;
        $data = array(
                'status' => $params['status'],
                'reason' => $params['reason'],
                'is_test_completed' => 'interrupted',
                );

        $newUserTestId = 0;
        $errorCode = 1;
        $flagChkStatus = $params['flagflagInterruptReset'];
        if ($rsobj->RecordCount()) {
            /*$statusOfTest = $rsobj->fields['status'];
            if($statusOfTest==0){
                 $flagChkStatus = 1;
                 $data['status'] = 30;
                 $data['reason'] = 'Cancelled';    
            }*/
            $status = 0;
            if (isset($params['newTestStatus'])) {
                $status = $params['newTestStatus'];
            }
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $isUpdated  =   true;
            if ($flagChkStatus == 1) {
                $this->dbcon->Execute(INSERTUSERTEST_INTERRUPT, array($status, $kcal, $userTestId));
                $newUserTestId = $this->dbcon->Insert_ID();
                $errorCode = 0;
            }
        }

        $this->status = array(
            'status' => 'error',
            'error_code' => $errorCode,
            'status_message' => 'Test Started Successfully',
            'userTestId' => $newUserTestId,
            // 'sql' => $rsobj->sql,
        );
        if ($isUpdated) {
            $this->status['testUpdateSuccess'] = '1';
        }

        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of change Test Parameter To Interrupt for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function changeTestParameterToInterrupt($params)
    {
        $userTestId = $params['userTestId'];
        $existingUserTestId = $params['existingUserTestId'];

        $insUsrTestParam = "INSERT INTO t_user_test_parameter
        (r_user_test_id,r_user_id,
        age_on_test,weight_on_test,r_equipment_id,test_prepared_date,
        start_load_test,
        conducted_by,fitness_level,test_level,test_date,target,intensity,
        training_week,start_trainging,end_of_program,iant_hr,iant_p,relative_iant_p,
        iant_vo2,relative_iant_vo2,relative_max_p,max_hr,max_p,next_appointment,
        regr_coeff,status) SELECT $userTestId,r_user_id,age_on_test,
        weight_on_test,r_equipment_id,test_prepared_date,start_load_test,
        conducted_by,fitness_level,test_level,test_date
        ,target,intensity,training_week,start_trainging,
        end_of_program,iant_hr,
        iant_p,relativ
        e_iant_p,iant_vo2,relative_iant_vo2,relative_max_p,
        max_hr,max_p,next_appointment,
        regr_coeff,status FROM t_user_test_parameter 
        WHERE r_user_test_id=".$existingUserTestId;

        $rsobj = $this->dbcon->Execute($insUsrTestParam);

        $errorCode = 1;
        if ($rsobj->RecordCount()) {
            $errorCode = 0;
        }

        $this->status = array(
            'status' => 'error',
            'error_code' => $errorCode,
            'status_message' => 'Parameter Saved Successfully',
            // 'sql' => $rsobj->sql,
        );

        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get Test Result Member List for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTestResultMemberList($params)
    {
        //$params['authorizedClubId'] = $params['testAndClub'];
        $limit = '';
        if (isset($params['limitStart']) && isset($params['limitEnd'])) {
            $limitStart = $params['limitStart'];
            $limitEnd = $params['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }

        $filterQry = '';

        //To get filter data
        if (isset($params['searchType'])) {
            if (trim($params['searchValue']) != '') {
                $filterQry = 'AND U.'.$params['searchType']." LIKE '".trim($params['searchValue'])."%'";
            }
        }

        $filterQry .= (
            isset($params['clubId']) && $params['clubId'] != ''
        ) ? ' AND U.r_club_id='.$params['clubId'].'' : '';

        $qry = GET_TEST_MEMBER_LIST.
            ' AND U.`r_club_id` IN('.$params['authorizedClubId'].') '.$filterQry.' GROUP BY U.`user_id` ';
        $arraySet   =array();
        if (isset($params['companyId'])) {
            $arraySet = array($params['companyId']);
        }
        if (isset($params['testAndClub'])) {
            $qry = GET_TEST_MEMBER_LIST_ALONE.' '.$filterQry.' GROUP BY U.`user_id` ';
            $arraySet = array($params['userId']);
        }

        /*Used to get sorting*/
        if (isset($params['labelField']) && isset($params['sortType'])) {
            if ($params['labelField'] != '' && $params['sortType'] != '') {
                $qry = $qry.' ORDER BY '.$params['labelField'].' '.$params['sortType'];
            }
        }

        $qry = $qry.$limit;
        $rsobj = $this->dbcon->Execute($qry, $arraySet);

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get test result success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'testResultList' => $rsobj->GetRows(),
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get test result failed',
                // 'sql' => $rsobj->sql,
                'totalCount' => '0',
            );
        }

        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    
    /**
    * Returns an json obj of get Last Query Total Count  for a company.
    *
    * @return array object object
    */ 
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }

    /**
    * Returns an json obj of get test detail navigation for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTestDetailNavIds($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TEST_DETAIL_NAV_LINKS, array($params['userId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'get test detail navigation links success',
                'testDetailNavLinks' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'get test detail navigation links failed',
            );
        }

        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get Test Member Ids for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTestMemberIds($params)
    {
        $qry = GET_MEMBER_IDS_FOR_TEST.
            ' WHERE `r_club_id` IN ('.$params['authorizedClubId'].') AND status IN('.$params['testStatus'].') ';
        $qry .= ' GROUP BY `r_user_id` ';
        $rsobj = $this->dbcon->Execute($qry);

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'get test detail navigation links success',
                'testDetailNavLinks' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'get test detail navigation links failed',
            );
        }

        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * Returns an json obj of get Activity Details for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getActivity($params)
    {
        $rsobj = $this->dbcon->Execute(GET_ACTIVITY, $params['activity_id']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Activity Details Received',
                // 'sql' => $rsobj->sql,
                'activityName' => $rsobj->fields['activity_name'],
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Test List Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'activityName' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
     
    /**
    * Returns an json obj of get Training Points Detail for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTrainingPointsDetail($params)
    {
        $traininglInfo = array();
        $qry = '';
        if (!isset($params['user_id'])) {
            return 'Invalid Data Send';
        }
        if (isset($params['week'])) {
            $qry = ' AND ha.week='.$params['week'];
        }
        $rsobj = $this->dbcon->Execute(
            GET_TRAINING_POINTS_INFO.$qry,
            array($params['user_id'], $params['r_user_test_id'])
        );
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $traininglInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Training Points Detail Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'trainingInfoDetails' => $traininglInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Training Points detail Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'trainingInfoDetails' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
      
    /**
    * Returns an json obj of get Points Achieve Week for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getPointsAchieveWeek($params)
    {
        $userTestId = isset($params['r_user_test_id']) ? $params['r_user_test_id'] : '';
        $curDate = isset($params['achieveDate']) ? date('Y-m-d', strtotime($params['achieveDate'])) : date('Y-m-d');
        $rsPointsAchieve = $this->dbcon->Execute(GET_TRAINING_POINTS_ACHIEVE_WEEK, array($curDate, $userTestId));
        if ($rsPointsAchieve->RecordCount()) {
            $this->status = array(
               'status' => 'success',
               'status_code' => 1,
               'status_message' => 'Get Training Achieve Week Detail Received',
               'total_records' => $rsPointsAchieve->RecordCount(),
               // 'sql' => $rsPointsAchieve->sql,
               'trainingWeek' => $rsPointsAchieve->fields['week'],
            );
        } else {
            $this->status = array(
               'status' => 'error',
               'status_code' => 0,
               'status_message' => 'Get Training Achieve Week detail Not Found.',
               'total_records' => $rsPointsAchieve->RecordCount(),
               // 'sql' => $rsPointsAchieve->sql,
               'trainingWeek' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

         
    /**
    * Returns an json obj of get Member Training Points Achieve for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getMemberTrainingPointsAchieve($params)
    {
        $userTestId = isset($params['r_user_test_id']) ? $params['r_user_test_id'] : '';
        $rsPointsAchieve = $this->dbcon->Execute(GET_TRAINING_POINTS_ACHIEVE, array($userTestId));
        if ($rsPointsAchieve->RecordCount()) {
            $traininglInfo=array();
            while (!$rsPointsAchieve->EOF) {
                $traininglInfo[] = $rsPointsAchieve->fields;
                $rsPointsAchieve->MoveNext();
            }
            $pointAchieveWeek = $this->getPointsAchieveWeek($params);
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get Training Achieve Points Detail Received',
            'total_records' => $rsPointsAchieve->RecordCount(),
            // 'sql' => $rsPointsAchieve->sql,
            'trainingInfoDetails' => $traininglInfo,
            'trainingWeek' => $pointAchieveWeek['movesmart']['trainingWeek'],
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get Training Achieve Points detail Not Found.',
            'total_records' => 0,
            // 'sql' => $rsPointsAchieve->sql,
            'trainingInfoDetails' => '',
            'trainingWeek' => '0',
            );
        }
        //Return the result array    
        return array(
        'movesmart' => $this->status,
        );
    }
     
    /**
    * Returns an json obj of get Member Training Parameters for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getMemberTrainingParameters($params)
    {
        $userTestId = isset($params['r_user_test_id']) ? $params['r_user_test_id'] : '';
        $rsTrainingParam = $this->dbcon->Execute(GET_TRAINING_PARAMETERS, array($userTestId));
        if ($rsTrainingParam->RecordCount()) {
            $trainingParam  =array();
            while (!$rsTrainingParam->EOF) {
                $trainingParam[] = $rsTrainingParam->fields;
                $rsTrainingParam->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get Training Parameters Detail Received',
            'total_records' => $rsTrainingParam->RecordCount(),
            // 'sql' => $rsTrainingParam->sql,
            'trainingParm' => $trainingParam,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get Training Parameters detail Not Found.',
            'total_records' => 0,
            // 'sql' => $rsTrainingParam->sql,
            'trainingParm' => '',
            );
        }
        //Return the result array    
        return array(
        'movesmart' => $this->status,
        );
    }

    /**
    * Returns an json obj of get Achieved Point By Member  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */        
    public function getAchievedPointByMember($params)
    {
        $userId = isset($params['r_user_id']) ? $params['r_user_id'] : '';
        $userTestId = isset($params['r_user_test_id']) ? $params['r_user_test_id'] : '';
        //$data = $params;

        $status = 'error';
        $statusMsgs = 'Row is not found';
        $achievedPointByWeek = array();
        $achievedPointTotal = array();
        $rsobj = $this->dbcon->Execute(GETACHIEVEDPOINTBY_MEMBERWEEKLIST, array($userId, $userTestId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $achievedPointByWeek[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $rsPointsTotal = $this->dbcon->Execute(GET_ACHIEVED_POINTS_TOTAL, array($userId, $userTestId));
            $achievedPointTotal = $rsPointsTotal->fields['achieved_point'];
        }

        //Set the message
        $status = array(
        'status' => $status,
        'status_code' => 200,
        'status_message' => $statusMsgs,
        'achievedPointMemberByWeek' => $achievedPointByWeek,
        'TotalPoints' => $achievedPointTotal,
        );

        //Return the result array    
        return array(
        'movesmart' => $status,
        );
    }


    /**
    * Delete Points transations from table.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object
    */
    public function deletePointsTransction($params)
    {
        $transactionId = isset($params['transactionId']) ? $params['transactionId'] : '';
        $rsobj = $this->dbcon->Execute(GET_POINTS_TRANSATION_ID, array($transactionId));
        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Points transaction delete failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $data['is_deleted'] = 1;
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Points transaction delete success',
            );
        }
        //Set the message
        //Return the result array    
        return array(
        'movesmart' => $this->status,
        );
    }
     
    /**
    * Returns an json obj of get Training Points Transaction for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTrainingPointsTransaction($params)
    {
        $trainingPoints = array();
        $status = 'error';
        $transId = isset($params['training_points_transaction_id']) ? $params['training_points_transaction_id'] : '';
        $rsobj = $this->dbcon->Execute(GET_POINTS_TRANSATION_ID, array($transId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $trainingPoints[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $status = 'success';
        }
        //Set the message
        $status = array(
        'status' => $status,
        'status_code' => 200,
        //'status_message' => $statusMsgs,
        'trainingPoints' => $trainingPoints,
        );
        //Return the result array    
        return array(
        'movesmart' => $status,
        );
    }
 
    /**
    * Returns an json obj of get Member Test Last Id Set for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getMemberTestLastIdSet($params)
    {
        $testLastIdSet = array();
        $status = 'error';
        $statusMsg = 'No Test Found';
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_MEMBER_TEST_LASTID_SET, array($userId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $testLastIdSet[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $status = 'success';
            $statusMsg = 'Test Id Set retrieved';
        }
        //Set the message
        $status = array(
        'status' => $status,
        'status_code' => 200,
        'status_message' => $statusMsg,
        'testIdSet' => $testLastIdSet,
        );
        //Return the result array    
        return array(
        'movesmart' => $status,
        );
    }
    
    /**
    * Returns an json obj of get Collected Points Training Devices In Week for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */        
    public function getCollectedPointsTrainingDevicesInWeek($params)
    {
        if (isset($params['allWeek'])) {
            $rsobj = $this->dbcon->Execute(
                GET_COLLECTEDPOINT_DEVICE_ALL,
                array($params['userId'], $params['userTestId'])
            );
        } else {
            $rsobj = $this->dbcon->Execute(
                GET_COLLECTEDPOINT_DEVICE_WEEKS,
                array($params['userId'], $params['userTestId'], $params['week'])
            );
        }

        $collectedDevices = array();
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $collectedDevices[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Get Activity For Each Training Devices retrieved',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'collectedDevices' => $collectedDevices,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => ' Activity For Each Training Devices retrieved Not Found',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'collectedDevices' => '',
            );
        }
        //Return the result array    
        return array(
        'movesmart' => $this->status,
        );
    }     
    /**
    * Returns an json obj of get All Test Dates For Member for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getAllTestDatesForMember($params)
    {
        //$condition = '';
        $userId = isset($params['r_user_id']) ? $params['r_user_id'] : '';
        /*For checking in managePoints Page Activity dropdown*/
        //$rsTest = $this->dbcon->Execute(GET_ALL_TEST_DATES_MEMBER,array($userId));
        $rsTest = $this->dbcon->Execute(GET_ALL_TEST_DATES_MEMBER, array($userId));
        if ($rsTest->RecordCount()) {
            $testDates  =array();
            while (!$rsTest->EOF) {
                $testDates[] = $rsTest->fields;
                $rsTest->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get User All Test Detail Received',
            'total_records' => $rsTest->RecordCount(),
            // 'sql' => $rsTest->sql,
            'testDates' => $testDates,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get User All Test Detail Not Found.',
            'total_records' => 0,
            // 'sql' => $rsTest->sql,
            'testDates' => '',
            );
        }
        //Return the result array    
        return array(
        'movesmart' => $this->status,
        );
    }
     
    /**
    * Returns an json obj of get Coach Info for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */        
    public function getCoachInfo($params)
    {
        $userId = isset($params['user_id']) ? $params['user_id'] : '';
        $rsobj = $this->dbcon->Execute(GET_COACHINFO_QUERY, array($userId));
        if ($rsobj->RecordCount()) {
            $coachByUserList=array();
            while (!$rsobj->EOF) {
                $coachByUserList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get coach Details By User Details Received',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'coachByUser' => $coachByUserList,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get coach Details By User Details Not Found.',
            'total_records' => 0,
            // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
        'movesmart' => $this->status,
        );
    }

    /**
    * This function used to set test to put in waitinglist
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function saveTestPutInWaitingList($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_TEST_DATA_EXIST, array($params['userId'], $params['userTestId']));
        $data = array('put_in_waitinglist' => 1);

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
            'status' => 'success',
            'status_code' => '200',
            'status_message' => 'Save test put in waiting list success',
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Save test put in waiting list failed',
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get Test Levels for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTestLevels($params)
    {
        //$status = array();
        $getTestLevelsList = array();
        $memberId = isset($params['memberId']) ? $params['memberId'] : '';
        $rsobj = $this->dbcon->Execute(GET_TESTLEVEL_IOSQUERY, array($memberId));
        //Set the status message
        $this->status = array(
        'status' => 'success',
        'status_code' => 200,
        'status_message' => 'Test To Do List are successfully retrieved',
        );
        $testLevel=$loadValues=array();
        if ($rsobj && $rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $getTestLevelsList[] = $rsobj->fields;
                $testLevel[] = $rsobj->fields['test_level'];
                $loadValues[] = $rsobj->fields['load_value'];
                $rsobj->MoveNext();
            }
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 200,
            'status_message' => 'No results found!',
            );
        }

        //Return the result array
        return array(
        'Response' => array(
            'initialParameter' => $getTestLevelsList,
            'testLevel' => $testLevel,
            'loadValue' => $loadValues,
            'status' => $this->status,
        ),
        );
    }
    
    /**
    * Returns an json obj of insert Member For Test for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertMemberForTest($params)
    {
		
        //$status = array();
        $insertMemberForTest = array();
        $updatePointSql = '';
        //Get TestId for TestName
        $getTestId = $this->dbcon->Execute(GET_TEST_DETAILS, array($params['testName']));
		
        //End
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
        'r_user_id' => isset($params['memberId']) ? $params['memberId'] : '',
        'user_test_id' => isset($params['userTestId']) ? $params['userTestId'] : '',
       // 'r_test_id' => isset($getTestId->fields['test_id']) ? $getTestId->fields['test_id'] : 1,
	    'r_test_id' => isset($params['testOptions']) ? $params['testOptions'] : 1,
        'test_start_date' => isset($params['testDate']) ? $params['testDate'] : $dateTime,
         'weight' => isset($params['weight']) ? $params['weight'] : '',
         'r_equipment_id' => isset($params['cycleId']) ? $params['cycleId'] : '',
         'status' => isset($params['status']) ? $params['status'] : '',
         'reason' => isset($params['reason']) ? $params['reason'] : '',
         'kcal' => isset($params['kcal']) ? $params['kcal'] : '',

        );
        if ($data['user_test_id'] != '' && ctype_digit($data['user_test_id'])) 
        {
            $rsobj = $this->dbcon->Execute(GET_MEMBERLIST_INSERTFORTEST_IOSQUERY, array($data['user_test_id']));
            if ($rsobj->RecordCount()) 
            {
				//Auto interrupt the same device assigned for member for the same club
                $arrStatus = array(10, 20, 30);
                if (isset($params['status'])) 
                {
                    if (in_array($params['status'], $arrStatus)) 
                    {
                        $arr = $rsobj->fields;
                        $qryUpdate = 'UPDATE `t_user_test` SET `r_equipment_id`=0 
                                WHERE status IN(0,10,20) 
                                  AND `r_club_id`='.$arr['r_club_id'].' 
                                  AND `r_equipment_id`='.$params['cycleId'].' 
                                  AND `user_test_id` != '.$params['userTestId'];
                        $this->dbcon->Execute($qryUpdate);
                    }
                }
                //Auto interrupt the same device assigned for member for the same club

                $data['modified_by'] = 1;
                $data['modified_date'] = $dateTime;
                /*If test status completed(3) , test end date should be captured.*/
                if (isset($params['status']) && $params['status'] == '3') 
                {
                    $data['test_end_date'] = $dateTime;
                }

                if (isset($params['loadValue']) && $params['loadValue'] != '' && $params['getLoadValues'] == true) 
                {
                    $data['load_value'] = $params['loadValue'];
                    $params['loadValue'] = json_encode($params['loadValue']);
                    $updatePointSql = "UPDATE t_user_test SET load_value='".$params['loadValue']."' 
                        WHERE user_test_id='".$params['userTestId']."'";
                    $this->dbcon->Execute($updatePointSql);
                }
                //echo "<pre>"; print_r($params); echo "</pre>";die;                
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                $messge = 'Successfully Updated';
            } 
            else 
            {
                $data['created_by'] = 1;
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                $messge = 'Successfully Added';
            }
        } 
        else 
        {
            $messge = 'User Test ID Required';
        }
        //Set the status message
        $this->status = array(
							'status' => 'success',
							'status_code' => 200,
							'status_message' => $messge,
							'updatesql' => $updatePointSql,
						);
        //Return the result array
        return array(
			'insertMemberForTestList' => array(
				'insertMember' => $insertMemberForTest,
				'status' => $this->status,
				// 'sql' => $rsobj->sql,
			),
        );
    }
} //Class End.
;
