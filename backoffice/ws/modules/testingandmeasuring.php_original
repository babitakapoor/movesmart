<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Testing_Measuring
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle testing and measuring related actions.
 */
require_once SQL_PATH.DS.'testingandmeasuring.php';
 /**
 * Class for functions to handle testing and measuring related actions.
 
 * @category Modules
 
 * @package Testing_Measuring
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
    
 * @link http://movesmart.company/admin/
 */
class testingAndMeasuringModel
{
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
        $this->error_general = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
        );
    }

    /*
     * This Function used For Get user list.
     * Query String Format : 
    */
    
     /**
    * Get user list.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getUserList($param)
    {
        $whr = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }

        $whr .= ' AND r_usertype_id=3 ';

        /*Used to get search*/
        $whr .= (isset($param['clubId']) && $param['clubId'] != '') ? ' AND r_club_id='.$param['clubId'].'' : '';
        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if ($param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        $companyId = isset($param['companyId']) ? $param['companyId'] : '';

        /*Used to get sorting*/
        $getMemberQuery = GET_MEMBER_QUERY_TESTING_MEASURING.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;

        $rsobj = $this->dbcon->Execute($getMemberQuery, array($companyId));

        if ($rsobj->RecordCount()) {
            $category   =array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Insert testing measure.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function insertTestingMeasuring($params)
    {
        $testDate = isset($params['testdate']) ? date('Y-m-d', strtotime($params['testdate'])) : '';
        $rsobj = $this->dbcon->Execute(
            GET_TESTING_MEASURING_EXIST,
            array($params['userId'], $params['testId'], $testDate)
        );
        $data = array(
                'r_user_id' => isset($params['userId']) ? $params['userId'] : '',
                'r_test_id' => isset($params['testId']) ? $params['testId'] : '',
                'test_date' => $testDate,
        );
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'error_code' => 200,
                'status_message' => 'Testing and Measuring exist',
                'testingMeasuringId' => $rsobj->fields['testing_measuring_id'],
            );
        } else {
            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $testingMeasuringId = $this->dbcon->Insert_ID();
            $this->status = array(
                'status' => 'success',
                'error_code' => 200,
                'status_message' => 'Testing and Measuring added successfully',
                'testingMeasuringId' => $testingMeasuringId,
            );
        }

        return $this->status;
    }

    /**
    * To add or update test and measuring data
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function updateTestItemData($params)
    {
        $this->status = $this->error_general;

        $revertFlag = 0;
        $this->dbcon->BeginTrans();
        $itemobj = json_decode($params['items']);
        foreach ($itemobj as $eitemobj) {
            $item = (array) $eitemobj;
            $rsobj = $this->dbcon->Execute(
                GET_TESTING_MEASURING_TRANSACTION_EXIST,
                array(
                    $params['userId'], $params['testId'], $item['testItemId'], $params['testingMeasuringId']
                )
            );
            $data = array(
                    'r_user_id' => isset($params['userId']) ? $params['userId'] : '',
                    'r_test_id' => isset($params['testId']) ? $params['testId'] : '',
                    'r_testing_measuring_id' => isset(
                        $params['testingMeasuringId']
                    ) ? $params['testingMeasuringId'] : '',
                    'r_test_item_id' => isset($item['testItemId']) ? $item['testItemId'] : '',
                    'value' => isset($item['value']) ? $item['value'] : '',
                    'note' => isset($item['note']) ? $item['note'] : '',
                    'meas_value' => isset($item['measValue']) ? $item['measValue'] : '',
                    'test_score' => isset($item['testScore']) ? $item['testScore'] : '',
                    'refer' => isset($item['refer']) ? $item['refer'] : '',
                    'score' => isset($item['score']) ? $item['score'] : '',
            );

            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
            } else {
                $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
                $rsInsert = $this->dbcon->Execute($rsInserts);
                if (!$rsInsert) {
                    $revertFlag = 1;
                }
            }
        }

        if ($revertFlag === 1) {
            $this->dbcon->RollbackTrans();
        } else {
            $this->dbcon->CommitTrans();
            $this->status = array(
                'status' => 'success',
                'error_code' => 200,
                'status_message' => 'Testing and Measuring Transaction data updated successfully',
            );
        }

        return $this->status;
    }

     /**
    * To insert body composition data
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function inserBodyComptiondata($param)
    {
        $param['date'] = date('Y-m-d H:i:s');
        $rsobj = $this->dbcon->Execute(
            INSERT_BODY_COMPTION_DATA,
            array(
                $param['userId'],
                $param['testingMeasuringId'],
                $param['date'],
                TEST_STATUS_COMPLETE,
                $param['createdby']
            )
        );
        $lastInsertId = $this->dbcon->Insert_ID();
        $param['laset_insert_id'] = $lastInsertId;
        if (($rsobj) && ($lastInsertId > 0)) {
            $this->status = array(
                'status' => 'success',
                'params' => $param,
                'status_id' => 200,
                'status_message' => 'Body Comp. data created.',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
                'status_message' => 'Error creating Body Comp. data.',
            );
        }

        return  $this->status;
    }

    /**
    * To insert flexibility test data
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function insertFlexblitytestdata($param)
    {
        $param['date'] = date('Y-m-d H:i:s');
        $rsobj = $this->dbcon->Execute(
            INSERT_FLEXIBILITY_TEST_DATA,
            array(
                $param['userId'],
                $param['testingMeasuringId'],
                $param['date'],
                TEST_STATUS_COMPLETE,
                $param['createdby'],
                $param['remarks']
            )
        );
        $lastInsertId = $this->dbcon->Insert_ID();
        $param['laset_insert_id'] = $lastInsertId;
        if (($rsobj) && ($lastInsertId > 0)) {
            $this->status = array(
                'status' => 'success',
                'params' => $param,
                'status_id' => 200,
                'status_message' => 'Flexibility test data created.',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
                'status_message' => 'Error creating Flexibility test data.',
            );
        }

        return  $this->status;
    }

    /**
    * To get testing and measuring data
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getTestingMeasuringData($param)
    {
        $qry = GET_TESTING_MEASURING_TRANSACTION_VALUES;
        $arr = array($param['userId'], $param['testId']);

        if (isset($param['testingMeasuringId'])) {
            $qry = GET_TESTING_MEASURING_TRANSACTION_VALUES.' AND `testing_measuring_id`=?';
            $arr = array($param['userId'], $param['testId'], $param['testingMeasuringId']);
        }
        $rsobj = $this->dbcon->Execute($qry, $arr);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Testing and Measuring Transaction Data Success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Testing and Measuring Transaction Data failed',
            );
        }

        //Return the result array    
        return  $this->status;
    }
    
    /**
    * To get testing and measuring cardio data
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function getTestingMeasuringCardioData($param)
    {
        $qry = GET_TESTING_MEASURING_CARDIO_VALUES;
        $arr = $param['userId'];
        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $qry .= $limit;
        $rsobj = $this->dbcon->Execute($qry, $arr);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Testing and Measuring Transaction Data Success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'rows' => $rsobj->GetRows(),
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Testing and Measuring Transaction Data failed',
                'rows' => array(),
                'totalCount' => 0,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To delete testing and measuring data
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object obj
    */
    public function deleteTestingMeasuringData($param)
    {
        $testingMeasuringId = $param['testingMeasuringId'];
        $rsobj = $this->dbcon->Execute(DELETE_TESTING_MEASURING_EXIST, array($testingMeasuringId));

        if ($rsobj->RecordCount()) {
            $this->dbcon->Execute(
                "DELETE FROM `t_testing_measuring_transaction` WHERE `r_testing_measuring_id`='$testingMeasuringId'"
            );
            $this->dbcon->Execute(
                "DELETE FROM `t_testing_measuring` WHERE `testing_measuring_id`='$testingMeasuringId'"
            );

            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Delete Testing and Measuring Transaction Data Success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Delete Testing and Measuring Transaction Data failed',
            );
        }

        //Return the result array    
        return  $this->status;
    }

    /**
    * To get last query count
    *
    * @return array object obj
    */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }
} // Class End.
;
