<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle message related actions.
 */
require_once SQL_PATH.DS.'notifications.php'; 
 /**
 * Class to handle Message related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class notificationsModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 1,
            'status_message' => 'Opps an error as occurred',
        );
    }

    public function getNotifications($params)
    {
		$queryType = GET_NOTIFICATIONS_TYPE;
		$rsobjtype = $this->dbcon->Execute($queryType);	
		if ($rsobjtype->RecordCount()) 
		{
			$i =0 ;
			while (!$rsobjtype->EOF) 
			{
				$notificationTabs['tabs'][$i]['id'] = $rsobjtype->fields['id'];
				$notificationTabs['tabs'][$i]['name'] = $rsobjtype->fields['type_name'];
				$notificationTabs['tabs'][$i]['slug'] = $rsobjtype->fields['slug'];
				$i++;
				$rsobjtype->MoveNext();
			}
			foreach($notificationTabs['tabs'] as $nt)
			{
				
				$inner_query = GET_NOTIFICATIONS_MESSAGES;
				$rsobjInner = $this->dbcon->Execute($inner_query,array($params['id'],$nt['id']));
				if ($rsobjInner->RecordCount())
				{
					$j = 0;
					while (!$rsobjInner->EOF) 
					{
						$notificationTabs['data'][$nt['slug']][$j]['id'] = $rsobjInner->fields['not_message_id'];
						$notificationTabs['data'][$nt['slug']][$j]['not_day'] = $rsobjInner->fields['not_day_name'];
						$j++;
						$rsobjInner->MoveNext();
					}
				}
				else
				{
					$notificationTabs['data'][$nt['slug']] = array();
				}
			}
			
			$this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Message Details',
                'total_records' => $this->getLastQueryTotalCount(),
				'data' => $notificationTabs
            );
		} else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Notifications Not Found.',
                'recordsfound' => 0,
                'total_records' => $this->getLastQueryTotalCount(),
                // 'sql' => $rsobj->sql,
                'message' => '',
            );
        }
        //Return the result array
        return $this->status;
    }
	
	public function getNotificationsSettings($params)
	{
		$query = GET_NOTIFICATIONS_DAY;
		$queryType = GET_NOTIFICATIONS_TYPE;
		$queryID = GET_NOTIFICATIONS_PAGE_ID;
		//echo $query;die;
		$notificationSetings = array();
		$rsobj = $this->dbcon->Execute($query);
		if ($rsobj->RecordCount()) 
		{
			$i =0 ;
            while (!$rsobj->EOF) 
			{
				$notificationSetings['day'][$i]['id'] = $rsobj->fields['id'];
				$notificationSetings['day'][$i]['name'] = $rsobj->fields['day_name'];
				$i++;
				$rsobj->MoveNext();
            }
		}
		$rsobjtype = $this->dbcon->Execute($queryType);	
		if ($rsobjtype->RecordCount()) 
		{
			$i =0 ;
            while (!$rsobjtype->EOF) 
			{
				$notificationSetings['type'][$i]['id'] = $rsobjtype->fields['id'];
				$notificationSetings['type'][$i]['name'] = $rsobjtype->fields['type_name'];
				$i++;
				$rsobjtype->MoveNext();
            }
        }
		
		$rsobjId = $this->dbcon->Execute($queryID,array($params['slug']));
		
		if ($rsobjId->RecordCount()) 
		{
			$i =0 ;
            while (!$rsobjId->EOF) 
			{
				$notificationSetings['page']['id'] = $rsobjId->fields['id'];
				$rsobjId->MoveNext();
            }
        }
		$this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Notifications Settings',
               'data' => $notificationSetings
            );
        
        //Return the result array
        return $this->status;
	}
	
	public function getScoreCategories()
	{
		$query = GET_SCORE_CTEGORIES;
		$rsobj = $this->dbcon->Execute($query);
		//echo "<pre>";print_r($rsobj);die;
		if ($rsobj->RecordCount()) 
		{
			$i =0;
			while (!$rsobj->EOF) 
			{
				$scoreCategories[$i]['id'] = $rsobj->fields['id'];
				$scoreCategories[$i]['name'] = $rsobj->fields['name'];
				$i++;
				$rsobj->MoveNext();
            }
			$this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Score Categories',
               'data' => $scoreCategories
            );
		}
		else
		{
			$this->status = array(
                'status' => 'failure',
                'status_code' => 1,
                'status_message' => 'No Score Categories present',
            );
		}
		return $this->status;
	}
	
	public function save_message($params)
	{
		if(!empty($params))
		{
			//echo "<pre>";print_r($params);die;
			$checkQuery = CHECK_EXISTING_RECORD;
			$rsobjId = $this->dbcon->Execute($checkQuery,array($params['notification_pages_id'],$params['notification_types_id'],$params['profile_id'],$params['notification_days_id'],$params['profile_cat_id']));
			
			if (!$rsobjId->RecordCount()) 
			{
				$date  =date('Y-m-d H:i:s');
				$save = $this->dbcon->Execute(
							INSERT_NOTIFICATION_MESAGE,
							array(
								$params['notification_pages_id'], 
								$params['notification_types_id'],
								$params['profile_id'],
								$params['notification_days_id'], 
								$params['profile_cat_id'], 
								$date, 
								$date,
							)
						);
				
				$lastsavedId = $this->dbcon->Insert_ID();
				if(isset($params['notification_tips_type_id']) and !empty($params['notification_tips_type_id']))
				{
					foreach($params['notification_tips_type_id'] as $tipId)
					{
						$this->dbcon->Execute(
								ADD_NOTIFICATIONS_MESSAGE_TIP,
								array(
									$lastsavedId, 
									$tipId,
									$date, 
									$date,
								)
							);
					}
				}
				else if(isset($params['notification_tops_type_id']))
				{
					foreach($params['notification_tops_type_id'] as $tipId)
					{
						$this->dbcon->Execute(
								ADD_NOTIFICATIONS_MESSAGE_TOP,
								array(
									$lastsavedId, 
									$tipId,
									$date, 
									$date,
								)
							);
							//echo "<pre>";print_r($sr);die;
					}
				}
				
				$this->status = array(
					'status_code' => 200,
					'status_message' => 'Added successfully',
				);
			}
			else
			{
				$this->status = array(
					'status_code' => 101,
					'status_message' => 'We already have a record for this entry',
				);
			}
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to add notification day',
            );
        }

        return $this->status;
	}
	
	public function getNotificationsData($params)
	{
		$query = GET_NOTIFICATIONS_DATA;
		$notificationData = array();
		$rsobj = $this->dbcon->Execute($query,array($params['id']));
		if ($rsobj->RecordCount()) 
		{
			while (!$rsobj->EOF) 
			{
				//echo "<pre>";print_r($rsobj->fields);die;
				$notificationData['message_data'] = $rsobj->fields;
				$rsobjProfile = $this->dbcon->Execute(GET_MESSAGE_PROFILE,array($rsobj->fields['id']));
				if(!empty($rsobjProfile))
				{
					$i = 0;
					while (!$rsobjProfile->EOF) 
					{
						$notificationData['profile_data'][$i]['profile_id'] = $rsobjProfile->fields['profile_id'];
						$i++;
						$rsobjProfile->MoveNext();
					}
				}
				if($rsobj->fields['t_notification_type_id'] != 3)
				{
					$rsobjTip = $this->dbcon->Execute(GET_MESSAGE_TIP,array($rsobj->fields['id']));
					if(!empty($rsobjTip))
					{
						$j =0;
						while (!$rsobjTip->EOF) 
						{
							$notificationData['tip_data'][$j]['tip_id'] = $rsobjTip->fields['notification_tips_type_id'];
							$j++;
							$rsobjTip->MoveNext();
						}
					}
				}
				if($rsobj->fields['t_notification_type_id'] == 3)
				{
					//echo $rsobj->fields['id'];
					$rsobjTop = $this->dbcon->Execute(GET_MESSAGE_TOP,array($rsobj->fields['id']));
					//echo "<pre>";print_r($rsobjTop);die;
					if(!empty($rsobjTop))
					{
						$k =0;
						while (!$rsobjTop->EOF) 
						{
							$notificationData['top_data'][$k]['top_id'] = $rsobjTop->fields['notification_tops_type_id'];
							$k++;
							$rsobjTop->MoveNext();
						}
					}
				}
				$rsobj->MoveNext();
            }
			//echo "<pre>";print_r($notificationData);die;
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Notifications Message Data',
               'data' => $notificationData
            );
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		
        
        //Return the result array
        return $this->status;
	}
	
	public function getTopicName($params)
	{
		//$typeArray = array('movesmart'=>'1','mindswitch'=>'3','eatfresh'=>'2');
		
		$typeID = $params['type'];
		
		$query = GET_QUESTION_TOPICS;
		$notificationData = array();
		$rsobj = $this->dbcon->Execute($query,array($typeID));
		//echo "<pre>";print_r($rsobj);die;
		if ($rsobj->RecordCount()) 
		{
			$i =0;
			while (!$rsobj->EOF) 
			{
				$notificationData[$i] = $rsobj->fields;
				$i++;
				$rsobj->MoveNext();
            }
			
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Notifications Message Data',
               'data' => $notificationData
            );
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		//Return the result array
        return $this->status;
	}
	
	public function edit_message($params)
	{
		if(!empty($params))
		{
			$date  =date('Y-m-d H:i:s');
			$this->dbcon->Execute(
						UPDATE_NOTIFICATION_DATA,
						array(
							$params['profile_id'], 
							$params['profile_cat_id'],
							$date,
							$params['id'],
						)
					);
			
			if(isset($_REQUEST['notification_tips_type_id']) and !empty($_REQUEST['notification_tips_type_id']))
			{
				$this->dbcon->Execute(DELETE_EXISTING_TIP,array($params['id']));
				foreach($_REQUEST['notification_tips_type_id'] as $tipId)
				{
					$this->dbcon->Execute(
							ADD_NOTIFICATIONS_MESSAGE_TIP,
							array(
								$params['id'],
								$tipId,
								$date, 
								$date,
							)
						);
					
				}
			}
			elseif(isset($params['notification_tops_type_id']))
			{
				$this->dbcon->Execute(DELETE_EXISTING_TOP,array($params['id']));
				foreach($_REQUEST['notification_tops_type_id'] as $tipId)
				{
					$this->dbcon->Execute(
						ADD_NOTIFICATIONS_MESSAGE_TOP,
						array(
							$params['id'],
							$tipId,
							$date, 
							$date,
							
						)
					);
				}
				
			}
			
			$this->status = array(
                'status_code' => 200,
                'status_message' => 'Updated successfully',
            );
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to update notification message',
            );
        }

        return $this->status;
	}
	
	
	public function delete_message($params)
	{
		if(!empty($params))
		{
			$id = $params['id'];
			$this->dbcon->Execute('delete from t_notification_message_tips where notification_messages_id = '.$id);
			$this->dbcon->Execute('delete from t_notification_message_tops where notification_messages_id = '.$id);
			$res = 	$this->dbcon->Execute(
						DELETE_NOTIFICATION_DATA,
						array($id)
					);
			$this->status = array(
                'status_code' => 200,
                'status_message' => 'Deleted successfully',
            );
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to delete notification message',
            );
        }

        return $this->status;
	}
	
	
	public function delete_content($params)
	{
		if(!empty($params))
		{
			$id = $params['id'];
			$this->dbcon->Execute('delete from  t_notification_content_data where t_notification_content_id = '.$id);
			$this->dbcon->Execute('delete from  t_notification_contents where id = '.$id);
			$this->status = array(
                'status_code' => 200,
                'status_message' => 'Deleted successfully',
            );
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to delete notification message',
            );
        }

        return $this->status;
	}
	
	public function getSelectBox($params)
	{
		if(!empty($params))
		{
			if($params['type'] == 'rule_tip')
			{
				$query = GET_NOTIFCATION_TIP;
				$resObj = 	$this->dbcon->Execute($query);
			}
			else
			{
				$query = GET_NOTIFCATION_TOP;
				$resObj = 	$this->dbcon->Execute($query,array($params['top_type']));
			}
			
			$data = array();
			if ($resObj->RecordCount()) 
			{
				$i =0 ;
				while (!$resObj->EOF) 
				{
					$data[$i]['id'] = $resObj->fields['id'];
					$data[$i]['name'] = $resObj->fields['name'];
					$i++;
					$resObj->MoveNext();
				}
			}
			
			$this->status = array(
                'status_code' => 200,
                'status_message' => 'List',
				'data' =>$data
            );
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Server Error',
            );
        }

        return $this->status;
	}
	
    /**
     * Get Total Record Count. Common to All Message Model
     *
     * @return Integer totalCount
     */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }
	
	
	public function getProfileData()
	{
		$query = GET_PROFILES;
		//echo "<pre>";print_r($userObj);die;
		$proObj = $this->dbcon->Execute($query);
		if($proObj->RecordCount())
		{
			while (!$proObj->EOF) 
			{
				$data[] = $proObj->fields;
				$proObj->MoveNext();
			}
			$secondquery = GET_SCORE_CTEGORIES;
			$catObj = $this->dbcon->Execute($secondquery);
			if($catObj->RecordCount())
			{
				while (!$catObj->EOF) 
				{
					$catdata[] = $catObj->fields;
					$catObj->MoveNext();
				}
			}
			$thirdquery = GET_PROFILE_CTEGORIES;
			$prfCatObj = $this->dbcon->Execute($thirdquery);
			if($prfCatObj->RecordCount())
			{
				while (!$prfCatObj->EOF) 
				{
					$prfcatdata[] = $prfCatObj->fields;
					$prfCatObj->MoveNext();
				}
			}
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Profile data',
               'data' => array('profile'=>$data,'cat'=>$catdata,'prfCat'=>$prfcatdata)
            );
		}
		else
		{
			$this->status = array(
				'status_code' => 404,
				'status_message' => 'NO Profile present',
			);
		}
		return $this->status;
	}
	
	public function save_notification_content($params)
	{
		if(!empty($params))
		{
			if($params['notification_tab'] == 'top_special_content')
			{
				$res = $this->save_special_top($params);
			}
			else
			{
				$checkQuery = CHECK_TOPIC_CONTENT_RECORD;
				$rsobj = $this->dbcon->Execute($checkQuery,array($params['topic_id'],$params['notification_pages_id'],$params['notification_types_id']));
				//echo "<pre>";print_r($rsobj);die;
				if (!$rsobj->RecordCount()) 
				{
					$date  =date('Y-m-d H:i:s');
					$this->dbcon->Execute(
							INSERT_TOPIC_CONTENT,
							array(
								$params['topic_id'],
								$params['notification_pages_id'], 
								$params['notification_types_id'],
								0,
								0,
								$date, 
								$date,
							)
						);
					$lastsavedId = $this->dbcon->Insert_ID();
					
					foreach($params['content'] as $mnKey => $cnt)
					{
						$profileid = $mnKey;
						foreach($cnt as $mdKey => $mdCnt)
						{
							$scorecategorieId = $mdKey;
							foreach($mdCnt as $datKey => $datCnt)
							{
								if($datCnt != '')
								{
									$sa = $this->dbcon->Execute(
											INSERT_TOPIC_CONTENT_DATA,
											array(
												$lastsavedId, 
												$profileid,
												$scorecategorieId,
												$datKey,
												$datCnt,
												$date, 
												$date,
											)
										);
								}
							}
						}
					}
					$res['code'] = 200;
					$res['messgae'] = 'Added successfully';
				}
				else
				{
					$res['code'] = 101;
					$res['messgae'] = 'We already have a record for this entry';
				}
			}
		}
		else
		{
			$res['code'] = 404;
			$res['messgae'] = 'Failed to add notification message';
        }
		$this->status = array(
                'status_code' => $res['code'],
                'status_message' => $res['messgae'],
            );
        return $this->status;
	}
	
	public function save_special_top($params)
	{
		
		$checkQuery = CHECK_TOPIC_SPECIAL_CONTENT_RECORD;
		$rsobj = $this->dbcon->Execute($checkQuery,array($params['notification_pages_id'],$params['notification_types_id'],1));
		//echo "<pre>";print_r($rsobj);die;
		if (!$rsobj->RecordCount()) 
		{
			$date  =date('Y-m-d H:i:s');
			$this->dbcon->Execute(
					INSERT_TOPIC_CONTENT,
					array(
						0,
						$params['notification_pages_id'], 
						$params['notification_types_id'],
						$params['special_top_id'],
						0,
						$date, 
						$date,
					)
				);
			$lastsavedId = $this->dbcon->Insert_ID();
			
			foreach($params['content'] as $mnKey => $cnt)
			{
				$profileid = $mnKey;
				foreach($cnt as $mdKey => $mdCnt)
				{
					$scorecategorieId = $mdKey;
					foreach($mdCnt as $datKey => $datCnt)
					{
						if($datCnt != '')
						{
							$sa = $this->dbcon->Execute(
									INSERT_TOPIC_CONTENT_DATA,
									array(
										$lastsavedId, 
										$profileid,
										$scorecategorieId,
										$datKey,
										$datCnt,
										$date, 
										$date,
									)
								);
						}
					}
				}
			}
			$res['code'] = 200;
			$res['messgae'] = 'Added successfully';
		}
		else
		{
			$res['code'] = 101;
			$res['messgae'] = 'We already have a record for this entry';
		}
		return $res;
	}
	
	public function getContent($params)
	{
		//$typeArray = array('movesmart'=>'1','mindswitch'=>'3','eatfresh'=>'2');
		$typeID = $params['id'];
		$query = GET_CONTENT;
		$notificationData = array();
		$rsobj = $this->dbcon->Execute($query,array($typeID));
		if ($rsobj->RecordCount()) 
		{
			$i =0;
			while (!$rsobj->EOF) 
			{
				$notificationData[$rsobj->fields['slug']][$i]['id'] = $rsobj->fields['content_id'];
				$notificationData[$rsobj->fields['slug']][$i]['topic_name'] = $rsobj->fields['topic_name'];
				$i++;
				$rsobj->MoveNext();
            }
			
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Content List',
               'data' => $notificationData
            );
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		//Return the result array
        return $this->status;
	}
	
	public function getSpecialContent($params)
	{
		//$typeArray = array('movesmart'=>'1','mindswitch'=>'3','eatfresh'=>'2');
		$typeID = $params['id'];
		$query = GET_SPECIAL_CONTENT;
		$notificationData = array();
		$rsobj = $this->dbcon->Execute($query,array($typeID));
		if ($rsobj->RecordCount()) 
		{
			$i =0;
			while (!$rsobj->EOF) 
			{
				$notificationData[$rsobj->fields['slug']][$i]['id'] = $rsobj->fields['content_id'];
				$notificationData[$rsobj->fields['slug']][$i]['top_day_name'] = $rsobj->fields['top_day_name'];
				$i++;
				$rsobj->MoveNext();
            }
			
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Content List',
               'data' => $notificationData
            );
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		//Return the result array
        return $this->status;
	}
	
	public function getContentData($params)
	{
		//echo "dsfd";die;
		$typeID = $params['id'];
		$query = GET_CONTENT_RECORD;
		$notificationData = array();
		$rsobj = $this->dbcon->Execute($query,array($typeID));
		//echo "<pre>";print_r($rsobj);die;
		if ($rsobj->RecordCount()) 
		{
			$data['id'] = $rsobj->fields['content_id'];
			$data['topic_id'] = $rsobj->fields['form_id'];
			$data['special_top'] = $rsobj->fields['special_top'];
			$innerquery = GET_CONTENT_DATA;
			$rsinnerobj = $this->dbcon->Execute($innerquery,array($data['id']));
			if($rsinnerobj->RecordCount())
			{
				$i =0;
				while (!$rsinnerobj->EOF) 
				{
					$data['data'][$i] = $rsinnerobj->fields;
					$i++;
					$rsinnerobj->MoveNext();
				}
			}
			else
			{
				$data['data'] = array();
			}
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Content List',
               'data' => $data
            );
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		//Return the result array
        return $this->status;
	}
	
	
	public function edit_notification_content($params)
	{
		if(!empty($params))
		{
			$date  =date('Y-m-d H:i:s');
			$id  =$params['id'];
			$deleteQuery = "delete from t_notification_content_data where t_notification_content_id = $id";
			$this->dbcon->Execute($deleteQuery);
			foreach($params['content'] as $mnKey => $cnt)
			{
				$profileid = $mnKey;
				foreach($cnt as $mdKey => $mdCnt)
				{
					$scorecategorieId = $mdKey;
					foreach($mdCnt as $datKey => $datCnt)
					{
						if($datCnt != '')
						{
							$sa = $this->dbcon->Execute(
									INSERT_TOPIC_CONTENT_DATA,
									array(
										$id, 
										$profileid,
										$scorecategorieId,
										$datKey,
										$datCnt,
										$date, 
										$date,
									)
								);
						}
					}
				}
			}
			$this->status = array(
				'status_code' => 200,
				'status_message' => 'Added successfully',
			);
			
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to add content',
            );
        }

        return $this->status;
	}
	
	public function save_behaviour_content($params)
	{
		//echo "<pre>";print_r($params);die;
		if(!empty($params))
		{
			$checkQuery = CHECK_EXISTING_RECORD;
			$rsobjId = $this->dbcon->Execute($checkQuery,array($params['notification_pages_id'],$params['notification_types_id'],0,$params['notification_days_id'],0));
			
			if (!$rsobjId->RecordCount()) 
			{
				$date  =date('Y-m-d H:i:s');
				$this->dbcon->Execute(
							INSERT_NOTIFICATION_MESAGE,
							array(
								$params['notification_pages_id'], 
								$params['notification_types_id'],
								0,
								$params['notification_days_id'], 
								0, 
								$date, 
								$date,
							)
						);
				
				
				$checkQuery = CHECK_TOPIC_CONTENT_RECORD;
				//$rsobj = $this->dbcon->Execute($checkQuery,array(0,$params['notification_pages_id'],$params['notification_types_id']));
				
				//if (!$rsobj->RecordCount()) 
				//{
					$date  =date('Y-m-d H:i:s');
					$st = $this->dbcon->Execute(
							INSERT_TOPIC_CONTENT,
							array(
								0,
								$params['notification_pages_id'], 
								$params['notification_types_id'],
								0,
								$params['notification_days_id'],
								$date, 
								$date,
							)
						);
					//echo "<pre>";print_r($st);die;
					$lastsavedId = $this->dbcon->Insert_ID();
					
					foreach($params['content'] as $mnKey => $cnt)
					{
						$profileid = $mnKey;
						foreach($cnt as $mdKey => $mdCnt)
						{
							$scorecategorieId = $mdKey;
							foreach($mdCnt as $datKey => $datCnt)
							{
								if($datCnt != '')
								{
									$this->dbcon->Execute(
											INSERT_TOPIC_CONTENT_DATA,
											array(
												$lastsavedId, 
												$profileid,
												$scorecategorieId,
												$datKey,
												$datCnt,
												$date, 
												$date,
											)
										);
								}
							}
						}
					}
					$res['code'] = 200;
					$res['messgae'] = 'Added successfully';
			}
			else
			{
				$res['code'] = 101;
				$res['messgae'] = 'We already have a record for this entry';
			}
			
		}
		else
		{
			$res['code'] = 404;
			$res['messgae'] = 'Failed to add notification message';
        }
		$this->status = array(
                'status_code' => $res['code'],
                'status_message' => $res['messgae'],
            );
        return $this->status;
	}
	
	public function getBehaviourData($params)
	{
		$typeID = $params['id'];
		$query = GET_BEHAVIOUR_DATA;
		$rsobj = $this->dbcon->Execute($query,array($typeID));
		
		if ($rsobj->RecordCount()) 
		{
			$pageid = $rsobj->fields['notification_pages_id'];
			$typeid = $rsobj->fields['t_notification_type_id'];
			$dayid = $rsobj->fields['notification_days_id'];
			$getData = GET_BEHAVIOUR_CONTENT;
			$notificationData = array();
			$rsConobj = $this->dbcon->Execute($getData,array($pageid,$typeid,$dayid));
			
			if($rsConobj->RecordCount())
			{
				$notificationData['id'] = $rsobj->fields['id'];
				$notificationData['day_id'] = $rsobj->fields['notification_days_id'];
				$notid = $rsConobj->fields['content_id'];
				$getContentData = GET_BEHAVIOUR_CONTENT_DATA;
				$rsConDatobj = $this->dbcon->Execute($getContentData,array($notid));
				$i =0;
				while (!$rsConDatobj->EOF) 
				{
					
					$notificationData['content_data'][$i] = $rsConDatobj->fields;
					$i++;
					$rsConDatobj->MoveNext();
				}
				
				$this->status = array(
					'status' => 'success',
					'status_code' => 200,
					'status_message' => 'Content List',
				   'data' => $notificationData
				);
			}
			else
			{
				$this->status = array(
					'status' => 'Failure',
					'status_code' => 101,
					'status_message' => 'No Data Found',
				   'data' => $notificationData
				);
			}
			
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		//Return the result array
        return $this->status;
	}
	
	public function edit_behaviour_content($params)
	{
		if(!empty($params))
		{
			
			$pageid = $params['notification_pages_id'];
			$typeid = $params['notification_types_id'];
			$dayid = $params['notification_days_id'];
			$getData = GET_BEHAVIOUR_CONTENT;
			$notificationData = array();
			$rsConobj = $this->dbcon->Execute($getData,array($pageid,$typeid,$dayid));
			$contentID = $rsConobj->fields['content_id'];
			$this->dbcon->Execute('delete from t_notification_contents where id = '.$contentID);
			$this->dbcon->Execute('delete from t_notification_content_data where t_notification_content_id = '.$contentID);
			$date  =date('Y-m-d H:i:s');
			$this->dbcon->Execute(
					INSERT_TOPIC_CONTENT,
					array(
						0,
						$params['notification_pages_id'], 
						$params['notification_types_id'],
						0,
						$params['notification_days_id'],
						$date, 
						$date,
					)
				);
			//echo "<pre>";print_r($st);die;
			$lastsavedId = $this->dbcon->Insert_ID();
			
			foreach($params['content'] as $mnKey => $cnt)
			{
				$profileid = $mnKey;
				foreach($cnt as $mdKey => $mdCnt)
				{
					$scorecategorieId = $mdKey;
					foreach($mdCnt as $datKey => $datCnt)
					{
						if($datCnt != '')
						{
							$this->dbcon->Execute(
									INSERT_TOPIC_CONTENT_DATA,
									array(
										$lastsavedId, 
										$profileid,
										$scorecategorieId,
										$datKey,
										$datCnt,
										$date, 
										$date,
									)
								);
						}
					}
				}
			}
			$res['code'] = 200;
			$res['messgae'] = 'Updated successfully';
		}
		else
		{
			$res['code'] = 404;
			$res['messgae'] = 'Failed to add notification message';
        }
		$this->status = array(
                'status_code' => $res['code'],
                'status_message' => $res['messgae'],
            );
        return $this->status;
	}
	
	
	public function getInappNotification($params = array())
	{
		if(!empty($params))
		{
			if(isset($params['userid']))
			{ 
				
				$userid = $params['userid'];
				$screen = $params['screen'];
				$total_credits = $params['totalcredit'];
				$day = $params['day'];
				
				$questions = json_decode($params['question']);
				$userObj = $this->dbcon->Execute(GET_USER,array($userid));
				
				if($userObj->RecordCount())
				{
					$diff = $day;
					
					if($diff <= 14)
					{
						$getUserProfile = $this->GetUserProfile($userid);
						if(!empty($getUserProfile))
						{	
							$resultArray = array();
							$phaseid = $screen;
							
							$getDay = $this->dbcon->Execute(GET_NOTIFICATIONS_DAY);
							if ($getDay->RecordCount()) 
							{
								while (!$getDay->EOF) 
								{
									$dayArray[$getDay->fields['day_number']] = $getDay->fields['id'];
									$getDay->MoveNext();
								}
							}
							$scoreArray = array();
							$i =0;
							
							foreach($questions as $ques)
							{
								if($ques->ques_type != 'coach')
								{
									
									$questioniD[] = $ques->question;
									$getElementData = $this->dbcon->Execute(GET_FORM_ELEMENT_DATA,array($ques->question));
									$formIds = $getElementData->fields['form_id'];
									$formtype = $getElementData->fields['type'];
									$startPoint = $ques->start_point;
									$endPoint = $ques->end_point;
									$maxValue = ($startPoint>$endPoint)?$startPoint:$endPoint;
									if($maxValue != 0)
									{
										$questionScore = $getElementData->fields['score'];
										$formId = $getElementData->fields['form_id'];
										
										$getFormTopicId = $this->dbcon->Execute(GET_FORM_DATA,array($formId));
										$quesValue = $ques->value;
										$scoreArray[$i]['quesValue'] = $quesValue;
										if($screen == 1)
										{
											$scoreArray[$i]['topicId'] = 1;
											$scoreArray[$i]['topicName'] = 'Move norm';
											if($quesValue <= 10000 )
											{
												$scoreArray[$i]['score'] = 0;
											}
											else
											{
												$scoreArray[$i]['score'] = 100;
											}
											$scoreArray[$i]['maxCredits'] = $maxValue;
										}
										else
										{
											$scoreArray[$i]['topicId'] = $getFormTopicId->fields['r_topic_id'];
											$scoreArray[$i]['topicName'] = $getFormTopicId->fields['topic_name'];
											$answeredValue = $quesValue;
											$difference = $maxValue-$answeredValue;
											$division  = $difference/$maxValue;
											$scoreArray[$i]['score'] = ceil($division*100);
											$scoreArray[$i]['maxCredits'] = $maxValue;
										}
										
										
										
										$i++;
									}
								}
							}
							
							$tipResult = array();
							$behaviourData = array();
							$topResult = array();
							$behaviourResult = array();
							$specialTopResult = array();
							
							$dayObj = $this->dbcon->Execute(GET_DAY_ID,array($diff));
							if($dayObj->RecordCount())
							{
								$dayId = $dayObj->fields['id'];
							}
							$getSuitableTip = $this->dbcon->Execute(GET_SUITABLE_NOTIFICATION,array($screen,$dayId,2,$getUserProfile['profileID'],1,$getUserProfile['profileCat'],1));
							
							if($getSuitableTip->RecordCount())
							{
								$getQuickWin = $this->dbcon->Execute(GET_SUITABLE_QUICK_WIN,array($getSuitableTip->fields['id']));
								if($getQuickWin->RecordCount())
								{
									//$notTipData['profileCategoryId'] = $getSuitableTip->fields['t_notification_user_profile_id'];
									//$notTipData['profileId'] = $getSuitableTip->fields['t_notification_user_profile_id'];
									$notTipData['notificationId'] = $getSuitableTip->fields['id'];
									$notTipData['page'] = $screen;
									$notTipData['pageType'] = 2;
									$notTipData['user_id'] = $userid;
									$notTipData['day_id'] = $dayId;
									$notTipData['message_number'] = $getQuickWin->fields['tip_number'];
									$tipData = $this->getTips($scoreArray,$notTipData,$getUserProfile,$params['screenscore']);
										
									if(!empty($tipData))
									{
										$this->addUserNotifiactionData($tipData,$notTipData);
										$tipResult['message'] = $tipData['content'];
										$tipResult['notification_type'] = 'tip';
										$tipResult['screen'] = $screen;
										$tipResult['tip_type'] = 'rule_tip';
										$tipResult['notification_id'] = $tipData['id'];;
										$tipResult['notification_day'] = $diff;
									}
								}
							}
							$getSuitableTop = $this->dbcon->Execute(GET_SUITABLE_NOTIFICATION,array($screen,$diff,3,$getUserProfile['profileID'],1,$getUserProfile['profileCat'],1));
							if($getSuitableTop->RecordCount())
							{
								$getTop = $this->dbcon->Execute(GET_SUITABLE_TOP,array($getSuitableTop->fields['id']));
								
								if($getTop->RecordCount())
								{
									//$notTopData['profileCategoryId'] = $getSuitableTop->fields['t_notification_user_profile_id'];
								//	$notTopData['profileId'] = $getSuitableTop->fields['t_notification_user_profile_id'];
									$notTopData['notificationId'] = $getSuitableTop->fields['id'];
									$notTopData['page'] = $screen;
									$notTopData['pageType'] = 3;
									$notTopData['user_id'] = $userid;
									$notTopData['day_id'] = $dayId;
									$notTopData['message_number'] = $getTop->fields['top_number'];
									$topData = $this->getTops($scoreArray,$notTopData,$getUserProfile);
									//echo "<pre>";print_r($topData);die;
									if(!empty($topData))
									{
										$this->addUserNotifiactionData($topData,$notTopData);
										$topResult['message'] = $topData['content'];
										$topResult['notification_type'] = 'top';
										$topResult['screen'] = $screen;
										$topResult['tip_type'] = '';
										$topResult['notification_id'] = $topData['id'];
										$topResult['notification_day'] = $diff;
									}
								}
							}
							$specailTopDay = array(1,3,7,10,14);
							if(in_array($diff,$specailTopDay))
							{
							
								if($diff == 1)
								{
									$dayVal = 10;
								}
								if($diff == 3)
								{
									$dayVal = 11;
								}
								if($diff == 7)
								{
									$dayVal = 12;
								}
								if($diff == 10)
								{
									$dayVal = 13;
								}
								if($diff == 14)
								{
									$dayVal = 14;
								}
								
								$getSuitableSpecialTop = $this->dbcon->Execute(GET_SUITABLE_TOP_NOTIFICATION,array($screen,$dayVal,3));
								//echo "<pre>";print_r($getSuitableSpecialTop);die;
								if($getSuitableSpecialTop->RecordCount())
								{
									
									$notSpecailTopData['profileCategoryId'] = $getUserProfile['profileCat'];
									$notSpecailTopData['profileId'] = $getUserProfile['profileID'];
									$notSpecailTopData['notificationId'] = $getSuitableSpecialTop->fields['id'];
									$notSpecailTopData['page'] = $screen;
									$notSpecailTopData['pageType'] = 3;
									$notSpecailTopData['user_id'] = $userid;
									$notSpecailTopData['day_id'] = $dayVal;
									$notSpecailTopData['message_number'] = 1;
									
									$specailtopData = $this->getSpecialTops($scoreArray,$notSpecailTopData,$getUserProfile);
									//echo "<pre>";print_r($topData);die;
									if(!empty($specailtopData))
									{
										$specailtopData['special_top'] = 1;
										$this->addUserNotifiactionData($specailtopData,$notSpecailTopData);
										$specialTopResult['message'] = $specailtopData['content'];
										$specialTopResult['notification_type'] = 'special_top';
										$specialTopResult['screen'] = $screen;
										$specialTopResult['tip_type'] = '';
										$specialTopResult['notification_id'] = $specailtopData['id'];
										$specialTopResult['notification_day'] = $diff;
										
									}
								}
							}
							
							
							
							$getSuitableBahaviour = $this->dbcon->Execute(GET_SUITABLE_BEHAVIOUR_NOTIFICATION,array($screen,$diff,1));
							if($getSuitableBahaviour->RecordCount())
							{
								$notBehaviourData['profileCategoryId'] = $getSuitableBahaviour->fields['t_notification_user_profile_id'];
								$notBehaviourData['profileId'] = $getSuitableBahaviour->fields['t_notification_user_profile_id'];
								$notBehaviourData['notificationId'] = $getSuitableBahaviour->fields['id'];
								$notBehaviourData['page'] = $screen;
								$notBehaviourData['pageType'] = 1;
								$notBehaviourData['user_id'] = $userid;
								$notBehaviourData['day_id'] = $diff;
								$notBehaviourData['message_number'] = 1;
								
								$behaviuorData = $this->getBehaviour($scoreArray,$notBehaviourData,$getUserProfile);
								//echo "<pre>";print_r($behaviuorData);die;
								if(!empty($behaviuorData))
								{
									$this->addUserNotifiactionData($behaviuorData,$notBehaviourData);
									$behaviourResult['message'] = $behaviuorData['content'];
									$behaviourResult['notification_type'] = 'tip';
									$behaviourResult['screen'] = $screen;
									$behaviourResult['tip_type'] = 'behaviour_tip';
									$behaviourResult['notification_id'] = $behaviuorData['id'];
									$behaviourResult['notification_day'] = $diff;
									
								}
							}
							$resultArray = array();
							
						
							array_push($resultArray,$tipResult);
							array_push($resultArray,$behaviourData);
							array_push($resultArray,$topResult);
							array_push($resultArray,$specialTopResult);
							array_push($resultArray,$behaviourResult);
							
							$finalResult =  array();
							$ii = 0;
							foreach($resultArray as $resss)
							{
								if(isset($resss['message']))
								{
									$finalResult[$ii] = $resss;
									$ii++;
								}
							}
							//echo "<pre>";print_r($finalResult);die;
							if(!empty($finalResult))
							{
								$this->status = array(
									'status_code' => 200,
									'status_message' => 'In app notifications',
									'data' => $finalResult
								);
							}
							else
							{
								$this->status = array(
									'status_code' => 101,
									'status_message' => 'No Notification Present for this user',
								);
							}
						}
						else
						{
							$this->status = array(
									'status_code' => 101,
									'status_message' => 'No Profile associated with this client',
								);
						}
					}
					else
					{
						$this->status = array(
							'status_code' => 101,
							'status_message' => 'User registered for more then 14 days! No notification present',
						);
					}
				}
				else
				{
					$this->status = array(
						'status_code' => 101,
						'status_message' => 'Not valid user',
					);
				}
			}
		}
		else
		{	 
			$this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to fetch notifications. Missing request parameters',
            );
		}
		return $this->status;
	}
	
	public function GetUserProfile($userId)
	{
		
		$getForm = $this->dbcon->Execute(GET_FORM_DAT,array(999999,1,30));
		$formId = $getForm->fields['form_id'];
		$getFormElement = $this->dbcon->Execute(GET_FORM_ELEMENT_DATA_TEST,array($formId));
		
		$result = array();
		if ($getFormElement->RecordCount()) 
		{
			$i =0;
			while (!$getFormElement->EOF) 
			{
				$array[$i] = $getFormElement->fields['id'];
				$i++;
				$getFormElement->MoveNext();
			}
			$ids =  implode(",",$array);
			
			$getUserMV = $this->dbcon->Execute('Select value,r_form_element_id from t_mv_data where user_id = '.$userId.' and r_form_element_id in('.$ids.')');
			
		//	echo 'Select value,r_form_element_id from t_mv_data where user_id = '.$userId.' and r_form_element_id in('.$ids.')';die;
			
			if ($getUserMV->RecordCount()) 
			{
				while (!$getUserMV->EOF) 
				{
					$score[$getUserMV->fields['r_form_element_id']] = $getUserMV->fields['value'];
					$getUserMV->MoveNext();
				}
			}
			
			///179 x,178 y
			$yaxisScore = $score['179'];
			$xaxisScore = $score['178'];
			
			$getProfileId =  $this->dbcon->Execute('Select profile_id from t_notification_user_profiles_settings where x_axis = '.$xaxisScore.' and y_axis = '.$yaxisScore);
			
			$result['profileID'] = $getProfileId->fields['profile_id'];
			$getFormElementAll = $this->dbcon->Execute(GET_FORM_ELEMENT_All,array($formId));
			
			$j =0;
			while (!$getFormElementAll->EOF) 
			{
				$arrayAll[$j] = $getFormElementAll->fields['id'];
				$j++;
				$getFormElementAll->MoveNext();
			}
			$getAllUserMV = $this->dbcon->Execute('Select sum(value) as totalScore from t_mv_data where user_id = '.$userId.' and r_form_element_id in('.implode(",",$arrayAll).')');
			$totalScore = $getAllUserMV->fields['totalScore'];
			$prfCat = 3;
			if($totalScore>=62)
			{
				$prfCat = 2;
			}
			$result['profileCat'] = $prfCat;
			
		}
		
		return $result;
	}
	
	public function getTips($scoreArray,$notTipData,$getUserProfile,$screenscore)
	{
		
		$data = array();
		//if(($notTipData['profileCategoryId'] == 1 && $notTipData['profileId'] == 1) || ($notTipData['profileCategoryId'] == $getUserProfile['profileCat'] && $notTipData['profileId'] == $getUserProfile['profileID']))
	//	{
			$profileId = $getUserProfile['profileID'];
			$profileCatId = $getUserProfile['profileCat'];
			$letAdd =  true;
			$checkQuery = CHECK_NOTIFICATION;
			$getlastId = 0;
			$notData = $this->dbcon->Execute(CHECK_NOTIFICATION,array($notTipData['user_id'],$notTipData['page'],$notTipData['day_id'],$notTipData['pageType'],$profileId));
			//echo "<pre>";print_r($notData);die;
			$checkId = false;
			if($notData->RecordCount())
			{
				$checkId = true;
				$count = $notData->RecordCount();
				if($count == 3)
				{	
					$letAdd = false;
				}
			}
			if($letAdd == true)
			{
				
				$max = max(array_column($scoreArray, 'score'));
				//echo $max;die;
				if($max != 0)
				{
					$scoreCat = $this->getScoreCat($max,$scoreArray,'tips',$screenscore);
					//echo "<pre>";print_r($scoreCat);die;
					$contentId =  $this->dbcon->Execute(GET_SUITABLE_CONTENT,array($scoreCat['topicID'],$notTipData['page'],$notTipData['pageType']));
					
					if($contentId->RecordCount())
					{
						$content_id = $contentId->fields['id'];
						if($checkId == true)
						{
							$messageNumber = array();
							$index = 0;
							while (!$notData->EOF) 
							{
								$messageNumber[$index] = $notData->fields['message_number'];
								$index++;
								$notData->MoveNext();
							}
							$query =	"Select id,message_number,content from  t_notification_content_data 
										where 
										t_notification_content_id = {$content_id} and 
										t_notification_user_profile_id = {$profileId} and 
										t_notification_score_categorie_id = {$scoreCat['scoreCat']} and 
										message_number not in (".implode(",",$messageNumber).") 
										order by id asc limit 1";
						}
						else
						{
							$messageNumber  = ($notTipData['message_number'] == 0)?1:$notTipData['message_number'];
							$query =	"Select id,message_number,content from  t_notification_content_data 
										where 
										t_notification_content_id = {$content_id} and 
										t_notification_user_profile_id in({$profileId},1) and 
										t_notification_score_categorie_id in ({$scoreCat['scoreCat']},1) and 
										message_number = {$messageNumber}
										order by id asc limit 1";
						}
						//echo "<pre>";print_r($query);
						$contentData =  $this->dbcon->Execute($query);
						$data['id']= $contentData->fields['id'];
						$data['content']= $contentData->fields['content'];
						$data['message_number']= $contentData->fields['message_number'];
						$data['profile_id']= $profileId;
					}
				}
			}
		//}
		//echo "<pre>";print_r($data);die;
		return $data;
	}
	
	public function getTops($scoreArray,$notTipData,$getUserProfile)
	{
		$data = array();
		//if(($notTipData['profileCategoryId'] == 1 && $notTipData['profileId'] == 1) || ($notTipData['profileCategoryId'] == $getUserProfile['profileCat'] && $notTipData['profileId'] == $getUserProfile['profileID']))
		//{
			$profileId = $getUserProfile['profileID'];
			$profileCatId = $getUserProfile['profileCat'];
			
			$checkQuery = CHECK_NOTIFICATION;
			$getlastId = 0;
			$notData = $this->dbcon->Execute(CHECK_NOTIFICATION,array($notTipData['user_id'],$notTipData['page'],$notTipData['day_id'],$notTipData['pageType'],$profileId));
			
			if($notData->RecordCount() == 0)
			{
				$max = min(array_column($scoreArray, 'score'));
				if($max == 0)
				{
					$scoreCat = $this->getScoreCat($max,$scoreArray,'tops');
					//echo $scoreCat['topicID'];die;
					$contentId =  $this->dbcon->Execute(GET_SUITABLE_CONTENT,array($scoreCat['topicID'],$notTipData['page'],$notTipData['pageType']));
					//echo "<pre>";print_r($contentId);die;
					if($contentId->RecordCount())
					{
						$content_id = $contentId->fields['id'];
						$query =	"Select id,message_number,content from  t_notification_content_data 
										where 
										t_notification_content_id = {$content_id} and 
										t_notification_user_profile_id = {$profileId} and 
										t_notification_score_categorie_id = 0 and 
										message_number = 1
										order by id asc limit 1";
						$contentData =  $this->dbcon->Execute($query);
						//echo "<pre>";print_r($contentData);die;
						$data['id']= $contentData->fields['id'];
						$data['content']= $contentData->fields['content'];
						$data['message_number']= $contentData->fields['message_number'];
						$data['profile_id']= $profileId;
					}
				}
			}
		//}
		return $data;
	}
	
	public function getSpecialTops($scoreArray,$notTipData,$getUserProfile)
	{
		$data = array();
		$profileId = $getUserProfile['profileID'];
		$profileCatId = $getUserProfile['profileCat'];
		$contentid = $notTipData['notificationId'];
		//$checkQuery = CHECK_NOTIFICATION;
		$getlastId = 0;
		$notData = $this->dbcon->Execute(CHECK_SPECIAL_TOP_NOTIFICATION,array($notTipData['user_id'],$notTipData['page'],$notTipData['day_id'],$notTipData['pageType'],$profileId));
		
		if($notData->RecordCount() == 0)
		{
			$query =	"Select id,message_number,content from  t_notification_content_data 
							where 
							t_notification_content_id = {$contentid} and 
							t_notification_user_profile_id = {$profileId} and 
							t_notification_score_categorie_id = 0 and 
							message_number = 1
							order by id asc limit 1";
			$contentData =  $this->dbcon->Execute($query);
			
			if($contentData->RecordCount())
			{
				$data['id']= $contentData->fields['id'];
				$data['content']= $contentData->fields['content'];
				$data['message_number']= $contentData->fields['message_number'];
				$data['profile_id']= $profileId;
			}
		}
		
		return $data;
	}
	
	public function getBehaviour($scoreArray,$notTipData,$getUserProfile)
	{
		$data = array();
		$profileId = $getUserProfile['profileID'];
		$profileCatId = $getUserProfile['profileCat'];
		
		$checkQuery = CHECK_NOTIFICATION;
		$getlastId = 0;
		$notData = $this->dbcon->Execute(CHECK_NOTIFICATION,array($notTipData['user_id'],$notTipData['page'],$notTipData['day_id'],$notTipData['pageType'],$profileId));
		if($notData->RecordCount() == 0)
		{
			$contentId =  $this->dbcon->Execute(GET_SUITABLE_BEHAVIOUR_CONTENT,array($notTipData['page'],$notTipData['pageType'],$notTipData['day_id']));
			if($contentId->RecordCount())
			{
				$content_id = $contentId->fields['id'];
				$query =	"Select id,message_number,content from  t_notification_content_data 
								where 
								t_notification_content_id = {$content_id} and 
								t_notification_user_profile_id = {$profileId} and 
								t_notification_score_categorie_id = 0 and 
								message_number = 1
								order by id asc limit 1";
				$contentData =  $this->dbcon->Execute($query);
				$data['id']= $contentData->fields['id'];
				$data['content']= $contentData->fields['content'];
				$data['message_number']= $contentData->fields['message_number'];
				$data['profile_id']= $profileId;
			}
		}
		
		return $data;
	}
	
	public function getScoreCat($max,$scoreArray,$callingFrom,$screenscore = '')
	{
		
		$createNewArray = array();
		$i = 0;
		foreach($scoreArray as $mx)
		{
			if($mx['score'] == $max)
			{
				//echo "<pre>";print_r($mx);
				$createNewArray[$i]['topicID'] = $mx['topicId'];
				$createNewArray[$i]['maxCredits'] = $mx['maxCredits'];
				$res['topicID'] = $mx['topicId'];
				$maxCredits = $mx['maxCredits'];
				$i++;
			}
		}
		//echo "<pre>";print_r($createNewArray);
		//echo "<pre>";print_r($createNewArray);
		usort(
			$createNewArray, 
			function($a, $b) {
				$result = 0;
				if ($b['maxCredits'] > $a['maxCredits']) {
					$result = 1;
				} else if ($b['maxCredits'] < $a['maxCredits']) {
					$result = -1;
				}
				return $result; 
			}
		);	
		
		
		
		if($callingFrom == 'tops')
		{
			$index = 0;
		}
		else
		{
			//$arrayCount = count($createNewArray);
			//$index = $arrayCount-1;
			$index = 0;
		}
		
		$dataReuslt = $createNewArray[$index];
		$maxCredits = $dataReuslt['maxCredits'];
		$res['topicID'] = $dataReuslt['topicID'];
		
		if($screenscore != '')
		{
			if ($screenscore <= 20) 
			{
				$res['scoreCat'] = 3;
			} 
			else if (($screenscore > 20) && ($screenscore <= 60)) 
			{
				$res['scoreCat'] = 1;
			} 
			else 
			{
				$res['scoreCat'] = 2;
			}
		}
		else
		{
			$divide = round(($max/$maxCredits)*100);
			if ($divide <= 20) 
			{
				$res['scoreCat'] = 3;
			} 
			else if (($divide > 20) && ($divide <= 60)) 
			{
				$res['scoreCat'] = 1;
			} 
			else 
			{
				$res['scoreCat'] = 2;
			}
		
		}
		printLog($res);
		//printLog($res);
		return $res;
	}
	
	public function addUserNotifiactionData($data,$notData)
	{
		if(!isset($data['special_top']))
		{
			$data['special_top'] = 2;
		}
		$date  =date('Y-m-d H:i:s');
		$this->dbcon->Execute(
								INSERT_USER_NOTIFICATION_DATA,
								array(
									$notData['user_id'], 
									$notData['page'],
									$notData['day_id'],
									$notData['pageType'],
									$data['profile_id'],
									$data['id'],
									$data['special_top'],
									$data['message_number'],
									$date, 
									$date,
								)
							);
		
		return true;
	}
	
	///////////////////////////push notification functios ///////////////////////////
	public function getPushType()
	{
		$query = GET_PUSH_TYPE;
		$proObj = $this->dbcon->Execute($query);
		if($proObj->RecordCount())
		{
			while (!$proObj->EOF) 
			{
				$data[] = $proObj->fields;
				$proObj->MoveNext();
			}
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Push Type data',
               'data' => $data
            );
		}
		else
		{
			$this->status = array(
				'status_code' => 404,
				'status_message' => 'No Push Type present',
			);
		}
		return $this->status;
	}
	
	public function getPushNotificationsSettings()
	{
		$query = GET_NOTIFICATIONS_DAY;
		
		//echo $query;die;
		$notificationSetings = array();
		$rsobj = $this->dbcon->Execute($query);
		if ($rsobj->RecordCount()) 
		{
			$i =0 ;
            while (!$rsobj->EOF) 
			{
				$notificationSetings['day'][$i]['id'] = $rsobj->fields['id'];
				$notificationSetings['day'][$i]['name'] = $rsobj->fields['day_name'];
				$i++;
				$rsobj->MoveNext();
            }
		}
		$this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Notifications Settings',
               'data' => $notificationSetings
            );
        
        //Return the result array
        return $this->status;
	}
	
	public function getPushSelectBox($params)
	{
		if(!empty($params))
		{
			$query = GET_PUSH_NOTIFICATIONS_TYPE;
			$resObj = 	$this->dbcon->Execute($query,$params['type']);
			$data = array();
			if ($resObj->RecordCount()) 
			{
				$i =0 ;
				while (!$resObj->EOF) 
				{
					$data[$i]['id'] = $resObj->fields['id'];
					$data[$i]['name'] = $resObj->fields['name'];
					$i++;
					$resObj->MoveNext();
				}
			}
			
			$this->status = array(
                'status_code' => 200,
                'status_message' => 'List',
				'data' =>$data
            );
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Server Error',
            );
        }

        return $this->status;
	}
	
	public function save_push_message($params)
	{
		if(!empty($params))
		{
			
			$checkQuery = CHECK_PUSH_EXISTING_RECORD;
			$rsobjId = $this->dbcon->Execute($checkQuery,array($params['notification_type_id'],$params['notification_push_activity_id'],$params['notification_days_id']));
			if (!$rsobjId->RecordCount()) 
			{
				$profileId = 0;
				$profileCatId = 0;
				if(isset($params['profile_id']))
				{
					$profileId = $params['profile_id'];
				}
				if(isset($params['profile_cat_id']))
				{
					$profileCatId = $params['profile_cat_id'];
				}
				$date  =date('Y-m-d H:i:s');
				$res = $this->dbcon->Execute(
						INSERT_PUSH_NOTIFICATION_MESAGE,
						array(
							$params['notification_type_id'], 
							$params['notification_push_activity_id'],
							$params['notification_days_id'], 
							$profileId,
							$profileCatId,
							$date, 
							$date,
						)
					);
				
				$lastsavedId = $this->dbcon->Insert_ID();
				
				foreach($params['message'] as $mess)
				{
					$time = '00:00';
					if(isset($mess['time']))
					{
						$time = $mess['time'];
					}
					$res = $this->dbcon->Execute(
								INSERT_PUSH_MESAGE,
								array(
									$lastsavedId, 
									$time,
									$mess['mess'],
									$date, 
									$date,
								)
							);
				}
				$this->status = array(
					'status_code' => 200,
					'status_message' => 'Added successfully',
				);
			}
			else
			{
				$this->status = array(
					'status_code' => 101,
					'status_message' => 'We already have a record for this entry',
				);
			}
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to add notification message',
            );
        }

        return $this->status;
	}

	public function getPushNotificationsData($params)
	{
		$query = GET_PUSH_NOTIFICATIONS_DATA;
		$notificationData = array();
		$rsobj = $this->dbcon->Execute($query,array($params['id']));
		if ($rsobj->RecordCount()) 
		{
			while (!$rsobj->EOF) 
			{
				$notificationData['message_data'] = $rsobj->fields;
				$rsobjProfile = $this->dbcon->Execute(GET_MESSAGE_PROFILE,array($rsobj->fields['id']));
				if(!empty($rsobjProfile))
				{
					$i = 0;
					while (!$rsobjProfile->EOF) 
					{
						$notificationData['profile_data'][$i]['profile_id'] = $rsobjProfile->fields['profile_id'];
						$i++;
						$rsobjProfile->MoveNext();
					}
				}
				
				
				$rsobjMess = $this->dbcon->Execute(GET_PUSH_NOTIFICATIONS_MESSAGES,array($rsobj->fields['id']));
				if(!empty($rsobjMess))
				{
					$l =0;
					while (!$rsobjMess->EOF) 
					{
						$notificationData['mess_data'][$l]['message'] = $rsobjMess->fields['message'];
						$notificationData['mess_data'][$l]['time'] = $rsobjMess->fields['sending_time'];
						$l++;
						$rsobjMess->MoveNext();
					}
				}
				$rsobj->MoveNext();
            }
			//echo "<pre>";print_r($notificationData);die;
			$this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Notifications Message Data',
               'data' => $notificationData
            );
		}
		else
		{
			$this->status = array(
                'status' => 'Failure',
                'status_code' => 101,
                'status_message' => 'No Data Found',
            );
		}
		
        
        //Return the result array
        return $this->status;
	}
	
	public function edit_push_message($params)
	{
		if(!empty($params))
		{
			$date  =date('Y-m-d H:i:s');
			$mid =$params['id'];
			$profileId = 0;
			$profileCatId = 0;
			if(isset($params['profile_id']))
			{
				$profileId = $params['profile_id'];
			}
			if(isset($params['profile_cat_id']))
			{
				$profileCatId = $params['profile_cat_id'];
			}
			$this->dbcon->Execute(
					UPDATE_PUSH_NOTIFICATION_MESAGE,
					array(
						$params['notification_push_activity_id'], 
						$profileId,
						$profileCatId,
						$date,
						$params['id']
					)
				);
			
			
			$qry = $this->dbcon->Execute("delete from t_notification_push_message_data where notification_push_messages_id = $mid");
			foreach($params['message'] as $mess)
			{
				$time = '00:00';
				if(isset($mess['time']))
				{
					$time = $mess['time'];
				}
				$sa = $this->dbcon->Execute(
						INSERT_PUSH_MESAGE,
						array(
							$mid, 
							$time,
							$mess['mess'],
							$date, 
							$date,
						)
					);
				//echo"<pre>";print_r($sa);die;
			}
			
			$this->status = array(
				'status_code' => 200,
				'status_message' => 'Added successfully',
			);
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to add notification message',
            );
        }

        return $this->status;
	}
	
	public function getPushNotifications($params)
	{
		$query = GET_PUSH_NOTIFICATIONS_LIST;
		$rsobj = $this->dbcon->Execute($query);
		//echo "<pre>";print_r($rsobj);die;
		if ($rsobj->RecordCount()) 
		{
			$notificationTabs    =   array();
            $i =0 ;
            while (!$rsobj->EOF) 
			{
				
				$notificationTabs[$i]['id'] = $rsobj->fields['not_id'];
				$notificationTabs[$i]['day'] = $rsobj->fields['name'];
				$notificationTabs[$i]['notification_type'] = $rsobj->fields['notification_type_id'];
				$notificationTabs[$i]['type'] = $rsobj->fields['notification_push_activity_id'];
				$i++;
				$rsobj->MoveNext();
            }
			
			$this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Message Details',
                'recordsfound' => $rsobj->RecordCount(),
                'total_records' => $this->getLastQueryTotalCount(),
				'data' => $notificationTabs
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Notifications Not Found.',
                'recordsfound' => 0,
                'total_records' => $this->getLastQueryTotalCount(),
                // 'sql' => $rsobj->sql,
                'message' => '',
            );
        }
        //Return the result array
        return $this->status;
	}
	
	public function delete_pushnotification($params)
	{
		if(!empty($params))
		{
			$id = $params['id'];
			$this->dbcon->Execute("delete from t_notification_push_messages where id = $id");
			$this->dbcon->Execute("delete from t_notification_push_message_data where notification_push_messages_id = $id");
			$this->status = array(
                'status_code' => 200,
                'status_message' => 'Deleted successfully',
            );
		}
		else
		{
			 $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to delete notification message',
            );
        }

        return $this->status;
	}
	
	
	public function sendPushNotification($params)
	{
		echo "<pre>";print_r($params);die;
	}
	
	public function GetUserProfileTest()
	{
		
		$userId = '852';
		$getForm = $this->dbcon->Execute(GET_FORM_DAT,array(999999,1,30));
		$formId = $getForm->fields['form_id'];
		$getFormElement = $this->dbcon->Execute(GET_FORM_ELEMENT_DATA_TEST,array($formId));
		
		$result = array();
		if ($getFormElement->RecordCount()) 
		{
			$i =0;
			while (!$getFormElement->EOF) 
			{
				$array[$i] = $getFormElement->fields['id'];
				$i++;
				$getFormElement->MoveNext();
			}
			$ids =  implode(",",$array);
			
			$getUserMV = $this->dbcon->Execute('Select value,r_form_element_id from t_mv_data where user_id = '.$userId.' and r_form_element_id in('.$ids.')');
			
			if ($getUserMV->RecordCount()) 
			{
				while (!$getUserMV->EOF) 
				{
					$score[$getUserMV->fields['r_form_element_id']] = $getUserMV->fields['value'];
					$getUserMV->MoveNext();
				}
			}
			
			///179 x,178 y
			$yaxisScore = $score['179'];
			$xaxisScore = $score['178'];
			
			$getProfileId =  $this->dbcon->Execute('Select profile_id from t_notification_user_profiles_settings where x_axis = '.$xaxisScore.' and y_axis = '.$yaxisScore);
			
			$result['profileID'] = $getProfileId->fields['profile_id'];
			$getFormElementAll = $this->dbcon->Execute(GET_FORM_ELEMENT_All,array($formId));
			
			$j =0;
			while (!$getFormElementAll->EOF) 
			{
				$arrayAll[$j] = $getFormElementAll->fields['id'];
				$j++;
				$getFormElementAll->MoveNext();
			}
			$getAllUserMV = $this->dbcon->Execute('Select sum(value) as totalScore from t_mv_data where user_id = '.$userId.' and r_form_element_id in('.implode(",",$arrayAll).')');
			$totalScore = $getAllUserMV->fields['totalScore'];
			$prfCat = 3;
			if($totalScore>=62)
			{
				$prfCat = 2;
			}
			$result['profileCat'] = $prfCat;
		}
		echo "<pre>";print_r($result);
		die;
		return $result;
		
	}
} // End Class.
;
