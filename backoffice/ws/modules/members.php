<?php
error_reporting(0);
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Members
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle members related actions.
 */
require_once SQL_PATH.DS.'members.php'; 
 /**
 * Class for functions to handle memebers related actions.
 
 * @category Modules
 
 * @package Members
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class membersModel
{
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
        $this->error_general = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
        );
    }

    /**
    * Get & Returns Members Detail By Member ID
    *
    * @param array $params service parameter, Keys:userId,companyId(optional)
    *
    * @return array object
    */
    public function getMemberBasicDetail($params)
    {
        $query = GET_MEMBER_BASIC_DETAIL;
        $arrayData = array($params['userId']);
        if (isset($params['personalDataPersonId'])) {
            $query = GET_MEMBER_BASIC_DETAIL_BY_PERSONID;
            $arrayData = array($params['personalDataPersonId'], $params['companyId']);
        }
        $rsobj = $this->dbcon->Execute($query, $arrayData);

        if ($rsobj->RecordCount()) {
            $userInfo   =array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
			
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get User Basic Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userDetails' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Basic Information Not Found',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'userDetails' => '',
            );
        }
		
        //Return the result array    
        return $this->status;
    }
	
	
	
	public function getMemberBasicDetailbyfb($params)
    {
        $query = GET_MEMBER_BASIC_DETAIL;
        $arrayData = array($params['userId']);
        if (isset($params['personalDataPersonId'])) {
            $query = GET_MEMBER_BASIC_DETAIL_BY_PERSONID;
            $arrayData = array($params['personalDataPersonId'], $params['companyId']);
        }
        $rsobj = $this->dbcon->Execute($query, $arrayData);

        if ($rsobj->RecordCount()) {
            $userInfo   =array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
			
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get User Basic Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userDetails' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Basic Information Not Found',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'userDetails' => '',
            );
        }
		
        //Return the result array    
        return $this->status;
    }
    /**
    * Get & Returns Members Security Answers (for Forget password)
    *
    * @param array $params service parameter, Keys:userId,companyId(optional)
    *
    * @return array object
    */
    public function getMemberByNameSecurityAnswer($params)
    {
        $query = GET_MEMBER_SECURITY_ANSWERS;
        $arrayData = array($params['email'], $params['password']);
        $rsobj = $this->dbcon->Execute($query, $arrayData);

        if ($rsobj->RecordCount()) {
            $userInfo   =array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get User Basic Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userDetails' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Basic Information Not Found',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'userDetails' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }

   /**
    * Get & Returns Member's Details By Email (If The Email Exists)
    *
    * @param array $params service parameter, Keys:userId,companyId(optional)
    *
    * @return array object
    */
    public function getExistingEmailList($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MEMBER_DETAIL_BY_EMAIL, array($params['emailId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'stastatustus' => 'success',
                'status_code' => 200,
                'status_message' => 'Get email match rows success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get email match rows failed',
            );
        }
        //Return the result array    
        return $this->status;
    }
	
	 public function edit_machine_subtype($params)
    {
        $rsobj = $this->dbcon->Execute(GET_SUBTYPE_LISTS);

        if ($rsobj->RecordCount()) {
			$subgroupInfo   =array();
            while (!$rsobj->EOF) {
                $subgroupInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
				
            }
		
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get email match rows success',
                'subgroup' => $subgroupInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get email match rows failed',
            );
        }
        //Return the result array    
        return $this->status;
    }
	
	public function get_groupname($group_id){
		
		$str="select group_name from t_machine_group where group_id=".$group_id;
		$result = mysql_query($str);
		$row = mysql_fetch_array($result);
		
		$group_name = $row['group_name'];
		return $group_name;
		
	}
	
	public function time_slote($group_id){
		
		$str="select * from t_time_slots where id=".$id;
		$result = mysql_query($str);
		$row = mysql_fetch_array($result);
		
		$group_name = $row['group_name'];
		return $group_name;
		
	}
	
	
    /**
    * Assigns New or Update Device for a Member
    *
    * @param array $params service parameter, Keys:userId,deviceMemberId
    *
    * @return array object
    */
    public function assignDevice($params)
    {
        $rsobj = $this->dbcon->Execute(GET_DEVICE_MEMBER_EXIST, array($params['deviceMemberId'], $params['userId']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->fields;
            $deviceId = $row['r_device_id'];
            $rsDevice = $this->dbcon->Execute(GET_DEVICE_ACTIVE_STATUS, array($deviceId));

            if ($rsDevice->RecordCount()) {
                $this->dbcon->Execute(UPDATE_DEVICE_MEMBER_STATUS, array($params['userId']));

                $data['is_deleted'] = 0;
                $data['date'] = date('Y-m-d');
                $data['time'] = date('H:i:s');
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Member assign device success',
                );
            }
        }

        //Return the result array    
        return $this->status;
    }

   /**
    * Unlinks the Device If Mapped to a Member
    *
    * @param array $params service parameter, Keys:userId,deviceMemberId
    *
    * @return array object
    */
    public function unlinkDevice($params)
    {
        $rsobj = $this->dbcon->Execute(GET_DEVICE_MEMBER_EXIST, array($params['deviceMemberId'], $params['userId']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $data['is_deleted'] = 1;

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Member assign device success',
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Updates The Security Answers for a Member on Registeration/ Profile Update
    *
    * @param array $params service parameter, Keys:userId,deviceMemberId
    *
    * @return array object
    */
    public function updateSecurityQuestion($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_BY_ID_AND_EMAIL, array($params['userId'], $params['emailId']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $data['security_questions'] = $params['securityQuestion'];

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Member security question update success',
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Reset User & Password - On forgot password
    *
    * @param array $params service parameter, Keys:email
    *
    * @return array object
    */
    public function UpdateForgotPasswordDetails($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_BY_EMAIL_ID, array($params['email']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $data['reset_password_key'] = $params['randStr'];
            $data['reset_password_flag'] = 1;

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'user' => $rsobj->GetRows(),
                'status_message' => 'Member forgot password email key updated successfully',
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Get Security answers with Email & Key
    *
    * @param array $params service parameter, Keys:email,key
    *
    * @return array object
    */
    /*public function getSecurityAnswers($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_INFO_BY_EMAIL_AND_KEY, array($params['email'], $params['key']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'user' => $rsobj->GetRows(),
                'status_message' => 'Get member forgot password security answers',
            );
        }

        //Return the result array    
        return $this->status;
    }
    */
    /**
    * Get Security answers with User_ID 
        (compateble With the Coach & Client Application)
    *
    * @param array $params service parameter, Keys:email,key
    *
    * @return array object
    */
    /*public function getSecurityAnswersMember($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_SEQURITY_ANSWERS_BY_ID, array($params['user_id']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'user' => $rsobj->GetRows(),
                'status_message' => 'Get member forgot password security answers',
            );
        }

        //Return the result array    
        return $this->status;
    }
    */
   /**
    * Updates & Returns the Result Password By KEY
    *
    * @param array $params service parameter, Keys:email,key
    *
    * @return array object
    */
    public function updateMemberPasswordWithKey($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_INFO_BY_EMAIL_AND_KEY, array($params['email'], $params['key']));
        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            $data['reset_password_key'] = '';
            $data['reset_password_flag'] = 0;
            $data['password'] = $params['password'];

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Member password updated successfully',
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Get & Returns the Last Test Details Of the Member
    *
    * @param array $params service parameter, Keys:userId
    *
    * @return array object
    */
    public function getMemberMaxTestId($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MEMBER_TEST_WITH_ANY_STATUS, array($params['userId']));

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Member assign device success',
                'rows' => $rsobj->GetRows(),
            );
        }
        //Return the result array    
        return $this->status;
    }

    /**
    * Get & Returns the Member Test Details By Status, TestID & User
    *
    * @param array $params service parameter, Keys:userId
    *
    * @return array object
    */
    public function getMemberTestDetail($params)
    {
        $testStatus = '0,1,2,10,20';
        if (isset($params['testStatus'])) 
        {
            if ($params['testStatus'] == 'completed' || $params['testStatus'] == 3) 
            {
                $testStatus = '3';
            } 
            elseif ($params['testStatus'] == 'stopped') 
            {
                $testStatus = '30';
            }
        }
        $userId = $params['userId'];
		$userTestId = "(SELECT MAX(`user_test_id`) FROM `t_user_test` WHERE `r_user_id`=$userId AND `status` IN($testStatus) )";
		
		if (isset($params['userTestId']) && $params['userTestId'] > 0) 
		{
            $userTestId = $params['userTestId'];
        }
        $qry = "SELECT *,ut.status test_status FROM `t_user_test` ut 
				LEFT JOIN `t_user_test_parameter` tp ON  tp.r_user_test_id=ut.user_test_id 
                WHERE ut.r_user_id=$userId AND ut.`status` IN($testStatus) AND ut.user_test_id=$userTestId";
			
        $rsobj = $this->dbcon->Execute($qry);  
		
        if ($rsobj->RecordCount()) 
        {
            $userInfo   =array();
            while (!$rsobj->EOF) 
            {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
			     
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get User Basic Information For Point System',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userDetails' => $userInfo,
            );
        } 
        else 
        {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Basic Information Not Found',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,  
                'userDetails' => 0,
            );
        }
		
        //Return the result array    
        return $this->status;
    }

    /**
    * Get & Returns the Member Fat Percentage (Improvements)
    *
    * @param array $params service parameter, Keys:userId
    *
    * @return array object
    */
    public function getMemberFatPercentage($params)
    {
        $rsobj = $this->dbcon->Execute(GET_BIO_METRIC_FAT_MEMBER, array($params['userId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get bio-metric FAT Success',
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get bio-metric FAT Failed',
            );
        }
        //Return the result array

        return $this->status;
    }

    /**
    * Get & Returns the List Members
    *
    * @param array $params service parameter, Keys:ClubId,UserId
    *
    * @return array object
    */
    public function getListEditMembers($params)
    {
        if (!isset($params['user_id'])) {
            return 'Invalid Data Send!';
        }
        $rsobj = $this->dbcon->Execute(GET_LISTEDITMEMBER_QUERY, array($params['user_id']));
        if ($rsobj->RecordCount()) {
            $personalInfo = $rsobj->fields;
            $personalInfo['_clubEquipments'] = array();
            $personalInfo['_clubEquipmentsCount'] = 0;
            if (isset($personalInfo['r_club_id']) and $personalInfo['r_club_id'] != 0) {
                $clubID = $personalInfo['r_club_id'];
                $clubeqp_rs = $this->dbcon->Execute(GET_CLUB_EQUIPMENTS, array($clubID));
                if ($clubeqp_rs->RecordCount()) {
                    $personalInfo['_clubEquipmentsCount'] = $clubeqp_rs->RecordCount();
                    $personalInfo['_clubEquipments'] = $clubeqp_rs->GetRows();
                }
            }
            //$clubEquipments =
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'members' => $personalInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'members' => '',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Add/Register Members on Signup New Member
    *
    * @param array $params service parameter, Keys:companyId
    *
    * @return array object
    */
    public function signupClient($params)
    {
 
        /*SN changed 20160203*/
			//$this->dbcon->debug = true;
	  
        $rsobj = $this->dbcon->Execute(
            INSERT_MEMBER_QUERY,
            array(
                $params['companyId'], $params['first_name'],
                $params['name'], $params['gender'], $params['email'],
                $params['usertypeid'], $params['statusid'], $params['password'],$params['register_date'],
                $params['phno'], $params['logintype'], $params['loginreffieldid'], $params['language']
            )
			
        );

		
        $lastInsertId = $this->dbcon->Insert_ID();
        
		$params['user_insert_id'] = $lastInsertId;

        if ($lastInsertId > 0) {
            // echo INSERT_MEMBER_PERSONALINFO_QUERY;
            // die;
          $this->dbcon->Execute(
                INSERT_MEMBER_PERSONALINFO_QUERY,
                array($params['user_insert_id'], $params['phno'], $params['imgval'], $params['regdob'])
            );

        }
		$this->dbcon->SetFetchMode(ADODB_FETCH_ASSOC);
        $rsobj = $this->dbcon->Execute('select * from t_users where user_id='.$lastInsertId);
		  
        if ($lastInsertId) {
            $personalInfo = $rsobj->fields;
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_id' => 200,
                'status_message' => 'insert new users',
                'insertid' => $lastInsertId,
                'members' => $personalInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
            );
        }
		
		
		
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Add/Register Members on Signup New Member Security Answers 
    *
    * @param array $params service parameter, Keys:companyId
    *
    * @return array object
    */
    public function SecurityQuesAns($params)
    {
        $this->dbcon->Execute(INSERT_MEMBER__SECURITY_QUERY, array($params['question'], $params['regid']));
        if (isset($params['regid'])) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_id' => 200,
                'status_message' => 'insert new users security question',
                'insertid' => $params['regid'],
                'members' => array(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    /*SN added getregister userdata 20160125*/
    /**
    * Get Registered Member's Detail On Registeration
    *
    * @param array $params service parameter, Keys:RegisteredUserId
    *
    * @return array object
    */
    public function getRegUserData($params)
    {
        $rsobj = $this->dbcon->Execute(GET_LISTEDITMEMBER_QUERY, array($params['regid']));
        if ($rsobj->RecordCount()) {
            $count  =   $rsobj->RecordCount();
            $personalInfo = $rsobj->fields;
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $count,
                // 'sql' => $rsobj->sql,
                'user' => $rsobj->GetRows(),
                'members' => $personalInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'members' => '',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Get City List By Zipcode
    *
    * @param array $params service parameter, Keys:zipCode
    *
    * @return array object
    */
    public function getCityList($params)
    {
        $rsobj = $this->dbcon->Execute(GET_CITY_QUERY);
        if ($params['zipCode'] != '') {
            $rsobj = $this->dbcon->Execute(GET_CITY_QUERY_WITH_CONDITION, array($params['zipCode']));
        }
        if ($rsobj->RecordCount()) {
            $cityList   =   array();
            while (!$rsobj->EOF) {
                $cityList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get City Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'cityList' => $cityList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get City Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Status List
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    /*public function getStatusList($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_STATUS_QUERY);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Status Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'stauslist' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Status Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Proffession List
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    public function getProfessionList($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_PROFESSION_QUERY);
        if ($rsobj->RecordCount()) {
            $professionList =   array();
            while (!$rsobj->EOF) {
                $professionList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Profession List Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'professionList' => $professionList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Profession List Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Get Nationality List
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    public function getNationalityList($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_NATIONALITY_QUERY);
        if ($rsobj->RecordCount()) {
            $nationalityList =   array();
            while (!$rsobj->EOF) {
                $nationalityList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Nationality List Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'nationalityList' => $nationalityList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Nationality List Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'nationalityList' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Companies List For adding A Member Persional Details
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    /*public function getCompanyList($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_COMPANYLIST_QUERY);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $companyList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Nationality List Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'companyList' => $companyList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Nationality List Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'companyList' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Coach List For a Member Coach
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    /*public function getCoachListbyUser($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_COACHLISTFORUSER_QUERY);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $coachByUserList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get coach List By User Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'coachByUser' => $coachByUserList,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get coach List By User Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Saved Profile Images
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
     public function getSaveProfileImage($params)
     {


         //$status = array();
        //$saveProfileImage = array();
        $userId = isset($params['userId']) ? $params['userId'] : '';
        //$userId =313;
         $dateTime = date('Y-m-d H:i:s');
         $profileimg = isset($params['userImage']) ? $params['userImage'] : '';
         $data = array(
                'userimage' => $profileimg,
                'r_user_id' => $userId,
                'modified_by' => 1,
                'modified_date' => $dateTime,
        );
         $rsobj = $this->dbcon->Execute(GET_PROFILEIMAGE_QUERY, array($userId));

         if ($rsobj->RecordCount()) {
             $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
           
           
             $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully Image Uploaded',
                'imagefile' => $profileimg,
            );
         } else {
           
             $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            // echo $rsInserts;
             //die;
             $this->dbcon->Execute($rsInserts);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully Image Uploaded',
                'imagefile' => $profileimg,
            );
         }
        //Return the result array
        return $this->status;
     }

    
    /**
    * Add/Edit Member 
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    /* public function insertMember($params)
    {
        //$status = array();
        $email = isset($params['email']) ? $params['email'] : '';
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'first_name' => isset($params['firstname']) ? $params['firstname'] : '',
            'last_name' => isset($params['lastname']) ? $params['lastname'] : '',
            'email' => $email,
            'r_club_id' => isset($params['clubId']) ? $params['clubId'] : '',
            'r_company_id' => isset($params['companyId']) ? $params['companyId'] : '',
            'r_status_id' => 5,
            'r_usertype_id' => 3,
            'created_by' => 1,
            'created_date' => $dateTime,
            'modified_by' => 1,
            'modified_date' => $dateTime,
        );
        $rsobj = $this->dbcon->Execute(GET_MEMBER);
        $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
        $this->dbcon->Execute($rsInserts);
          //Set the status message
           $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Member Details Added Successfully',
                'user_id' => $this->dbcon->Insert_ID(),
            );
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Checks Member Exists or by Email, User
    *
    * @param array $params service parameter,
    *
    * @return array object
    */
    public function checkMemberEmailExist($params)
    {  
	   
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $email = isset($params['email']) ? $params['email'] : '';
        $username = isset($params['username']) ? $params['username'] : '';
        $password = isset($params['password']) ? $params['password'] : '';

        if ($userId > 0) {
            $rsobj = $this->dbcon->Execute(GET_MEMBER_EMAIL_UPDATE, array($email, $userId, $password));
        } elseif (isset($params['username'])) {
            $rsobj = $this->dbcon->Execute(
                GET_MEMBER_EMAIL_UPDATE_MODE,
                array($userId, $username, $password, $email, $password)
            );
        } else {
            $rsobj = $this->dbcon->Execute(GET_MEMBER_EXIST_EMAIL, array($email));
        }

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 1,
                'email' => 1,
                'status_message' => 'Login Details Already Exist',
                'user_id' => $rsobj->fields['user_id'],
				'fblogin' =>1
            );
        } else {
            $this->status = array(
                'status' => 'Success',
                'status_code' => 200,
                'email' => 0,
                'status_message' => 'Email Address Not Exist',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }

   

    /**
    * Get Members Medical Factors List By Member
    *
    * @param array $params service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    /*public function getListMedicalAgainst($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_LIST_MEDICALAGAINST, array($userId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $medicalAgainst[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Medical Against Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'medicalAgainst' => $medicalAgainst,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Navigation Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'medicalAgainst' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Members Medical Factors by JOINT Problems List By Member
    *
    * @param array $params service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    /*public function getListMedicalJoint($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_LIST_MEDICALJOINT, array($userId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $medicalJoint[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Medical Joint Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'medicalJoint' => $medicalJoint,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Medical Joint Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'medicalJoint' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Members Medical Factors by Muscles Problems List By Member
    *
    * @param array $params service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    /*public function getListMedicalMuscle($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_LIST_MEDICALMUSCLE, array($userId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $medicalMuscle[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Medical Muscle Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'medicalMuscle' => $medicalMuscle,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Medical Muscle Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'medicalMuscle' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Members Medical Risk Factors by All other
    *
    * @param array $params service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    /*public function getListMedicalRiskFactor($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $rsobj = $this->dbcon->Execute(GET_LIST_MEDICALRISKFACTOR, array($userId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $medicalRiskFactor[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Medical Risk Factor Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'medicalRiskFactor' => $medicalRiskFactor,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Medical Risk Factor Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'medicalRiskFactor' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Members List For Navigation
    *
    * @param array $params service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    public function getMemberIdForNavigation($params)
    {
        $userId = isset($params['user_id']) ? $params['user_id'] : '';
        $authorizedClubId = isset($params['authorizedClubId']) ? $params['authorizedClubId'] : '';
        $getMemIdForNav =
                'SELECT min(user_id) AS min_user_id,max(user_id) AS max_user_id,
                        (SELECT user_id FROM t_users 
                            WHERE r_usertype_id=3 
                              AND user_id>'.$userId.' 
                              AND r_club_id IN('.$authorizedClubId.') LIMIT 1
                        ) as nextid,
                        (SELECT user_id FROM t_users 
                                WHERE r_usertype_id=3 AND user_id<'.$userId.' 
                                    AND r_club_id IN('.$authorizedClubId.')
                                ORDER BY user_id DESC LIMIT 1
                        ) as previd
                        FROM t_users
                        WHERE r_usertype_id=3 AND is_deleted=0  AND r_club_id IN('.$authorizedClubId.')';
        $rsobj = $this->dbcon->Execute($getMemIdForNav);
        if ($rsobj->RecordCount()) {
            $navigation =   array();
            while (!$rsobj->EOF) {
                $navigation[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Navigation Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'navigation' => $navigation,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Navigation Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Coach ID List For Navigation
    *
    * @param array $params service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    public function getEmployeeIdForNavigation($params)
    {
        $userId = isset($params['user_id']) ? $params['user_id'] : '';
        $authorizedClubId = isset($params['authorizedClubId']) ? $params['authorizedClubId'] : '';
        $getEmpIdForNav =
                'SELECT min(user_id) AS min_user_id,max(user_id) AS max_user_id,
                        (SELECT user_id FROM t_users 
                            WHERE (r_usertype_id=1 || r_usertype_id=2 || r_usertype_id=4) 
                              AND user_id>'.$userId.' 
                              AND r_club_id IN('.$authorizedClubId.') AND is_deleted=0 LIMIT 1) as nextid,
                        (SELECT user_id FROM t_users 
                            WHERE (r_usertype_id=1 || r_usertype_id=2 || r_usertype_id=4) 
                                AND user_id<'.$userId.' AND r_club_id IN('.$authorizedClubId.') AND is_deleted=0
                            ORDER BY user_id DESC
                            LIMIT 1) as previd
                FROM t_users
                    WHERE (r_usertype_id=1 || r_usertype_id=2 || r_usertype_id=4) 
                        AND r_club_id IN('.$authorizedClubId.') AND is_deleted=0';
        $rsobj = $this->dbcon->Execute($getEmpIdForNav);
        if ($rsobj->RecordCount()) {
            $navigation =   array();
            while (!$rsobj->EOF) {
                $navigation[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Navigation Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'navigation' => $navigation,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Navigation Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get All User List By Arguments
    *
    * @param array $param service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    public function getUserList($param)
    {
 
        $whr = '';
        $leftJoin = '';
        $andClubCondtion = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;

        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 ';

        if (strtolower($param['userType']) == 'coach' && $param['loggedUserType'] == 4) {
            $whr .= ' AND (r_usertype_id=1 || r_usertype_id=2 || r_usertype_id=4)';
            if (isset($param['clubId']) && $param['clubId'] != '') {
                $leftJoin .= ' LEFT OUTER JOIN t_employee_centre tec ON tec.r_user_id=u.user_id';
                $andClubCondtion .= ' AND tec.r_club_id = '.$param['clubId'];
            }
        } elseif (
            strtolower($param['userType']) == 'coach'
            && ($param['loggedUserType'] == 1 || $param['loggedUserType'] == 2)
        ) {
            $whr .= ' AND (r_usertype_id=1 || r_usertype_id=2)';
            if (isset($param['clubId']) && $param['clubId'] != '') {
                $leftJoin .= ' LEFT OUTER JOIN t_employee_centre tec ON tec.r_user_id=u.user_id';
                $andClubCondtion .= ' AND tec.r_club_id = '.$param['clubId'];
            }
        }

        if (strtolower($param['userType']) == 'member') {
            $whr .= ' AND r_usertype_id=3 ';
        }

        /*Used to get search*/
        if ($andClubCondtion) {
            //this condition is only for Club List Page.
            $whr .= (isset($param['clubId']) && $param['clubId'] != '') ? $andClubCondtion : '';
        } else {
            //this condition is All other Pages.
            $whr .= (isset($param['clubId']) && $param['clubId'] != '') ? ' AND u.r_club_id='.$param['clubId'].'' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if ((!isset($param['clubId'])) or $param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*Used to get sorting*/
        $getMemberQuery = GET_MEMBER_QUERY.$leftJoin.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;
			
        $rsobj = $this->dbcon->Execute($getMemberQuery); 

        if ($rsobj->RecordCount()) {
			
            $category   =   array();
			$totalCount = $this->getLastQueryTotalCount();
            while (!$rsobj->EOF) {
				$coach_id = $rsobj->fields['coach_id'];
				if(!empty($coach_id))
				{
				$coach_name = $this->get_coach($coach_id);
				$rsobj->fields['coach_name'] = $coach_name;
				}
				$category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
			
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
				'totalCount' =>$totalCount
				 ,
               // 'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
public function get_coach($coach_id){
	$qry = "select * from t_users where user_id =".$coach_id;
	$query = mysql_query($qry);
	$result = mysql_fetch_array($query);
	if(!empty($result)){
	$first_name = $result['first_name'];
	$last_name = $result['last_name'];
	$coach_name	 = $first_name.' '.$last_name;
	return $coach_name;
	}
	else{
		return false;
	}
}
    /**
    * Get All User List By Arguments on Cardio Test Todo
    *
    * @param array $param service parameter, Key:UserId(Member)
    *
    * @return array object
    */
    public function getMembersErgoTestToDo($param)
    {
        $whr = '';
        $leftJoin = '';
        //$andClubCondtion = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;

        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 AND r_usertype_id=3 ';

        //this condition is All other Pages.
        $whr .= (isset($param['clubId']) && $param['clubId'] != '') ? ' AND u.r_club_id='.$param['clubId'].' ' : '';

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if (!isset($param['clubId']) or $param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND u.r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*Used to get sorting*/
        $getMemberQuery = GET_MEMBER_QUERY_ERGO_STATUS_TEST_TO_DO.$leftJoin.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;

        $rsobj = $this->dbcon->Execute($getMemberQuery);

        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Member Points for a week
    *
    * @param array $param service parameter, Key:UserId(Member),companyId(optional)
    *
    * @return array object
    */
    public function getMembersToManyPointsThisWeek($param)
    {
        $whr = '';
        $leftJoin = '';
        //$andClubCondtion = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;

        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 AND r_usertype_id=3 AND r_status_id=4 ';

        //this condition is All other Pages.
        $whr .= (isset($param['clubId']) && $param['clubId'] != '') ? ' AND u.r_club_id='.$param['clubId'].'' : '';

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if ((!isset($param['clubId'])) or $param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND u.r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        $param['dateToday'] = date('Y-m-d');

        $whr .= " AND ('".$param['dateToday'].
            "' BETWEEN tpa.week_start_date AND tpa.week_end_date) AND tpa.`achieved_point`>tpa.target_points ";

        /*Used to get sorting*/
        $getMemberQuery = GET_MEMBER_QUERY_TO_MANY_POINTS_THIS_WEEK.$leftJoin.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;

        $rsobj = $this->dbcon->Execute($getMemberQuery);

        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Body Composition Test
    *
    * @param array $param service parameter, Key:UserId(Member),companyId(optional)
    *
    * @return array object
    */
    public function getMembersBodycompositionTestToDo($param)
    {
        $whr = '';
        $leftJoin = '';
        //$andClubCondtion = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;

        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 AND r_usertype_id=3 ';

        //this condition is All other Pages.
        $whr .= (
            isset($param['clubId'])
            && $param['clubId'] != ''
        ) ? ' AND u.r_club_id='.$param['clubId'].'' : '';

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if ((!isset($param['clubId'])) or $param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND u.r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*Used to get sorting*/
        $getMemberQuery = GET_MEMBER_QUERY_BODYCOMPOSITION_STATUS_TEST_TO_DO.$leftJoin.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;

        $rsobj = $this->dbcon->Execute($getMemberQuery);

        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Cardio Test list Analysis/Print Status with Search arguments
    *
    * @param array $param service parameter, Key:UserId(Member),companyId(optional),SearchArguments
    *
    * @return array object
    */
    /*public function getMembersCardioPrintOrSend($param)
    {
        $whr = '';
        $leftJoin = '';
        //$andClubCondtion = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;

        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 AND r_usertype_id=3 ';

        //this condition is All other Pages.
        $whr .= (isset($param['clubId']) && $param['clubId'] != '') ? ' AND u.r_club_id='.$param['clubId'].'' : '';

        $whr .= (
                isset($param['searchType'])
                && $param['searchType'] != ''
                && $param['searchValue'] != ''
                && $param['searchType'] != 'status'
         ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.* /
        if ($param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND u.r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*Used to get sorting* /
        $getMemberQuery = GET_MEMBER_QUERY_CARDIO_PRINT_OR_SEND.$leftJoin.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;

        $rsobj = $this->dbcon->Execute($getMemberQuery);

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Members List Flexibility composition test Todo
    *
    * @param array $param service parameter, Key:UserId(Member),companyId(optional),SearchArguments
    *
    * @return array object
    */
    public function getflexibilitycompostiontodo($param)
    {
        $whr = '';
        $leftJoin = '';
        //$andClubCondtion = '';
        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;
        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 AND r_usertype_id=3 ';
        //this condition is All other Pages.
        $whr .= (
            isset($param['clubId'])
            && $param['clubId'] != ''
        ) ? ' AND u.r_club_id='.$param['clubId'].'' : '';

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if ((!isset($param['clubId'])) or $param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND u.r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*Used to get sorting*/
        $getMemberQuery = GET_FLEXIBILITY_DATA_TODO.$leftJoin.$whr.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;
        $rsobj = $this->dbcon->Execute($getMemberQuery);
	
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
				
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
   /**
    * Get Members List Whose Test to BE Analyzed
    *
    * @param array $params service parameter, Key:companyId,clubId
    *
    * @return array object
    */
    /*public function getMembersTestToAnalyze($params)
    {

        //$rs = $this->dbcon->Execute(
                GET_MEMBER_LIST_TEST_TO_ANALYZE,
                array(
                        $params['companyId'], $params['clubId'], $params['testDate']
                )
        );
        $rsobj = $this->dbcon->Execute(
                GET_MEMBER_LIST_TEST_TO_ANALYZE,
                array($params['companyId'], $params['clubId'])
        );

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $members[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Get Members Test to analyze success',
                'total_records' => $rsobj->RecordCount(),
                'memberList' => $members,
                'totalCount' => $rsobj->RecordCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => '0',
                'status_message' => 'Get Members Test to analyze failed',
                'total_records' => '',
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Total Record Count. Common to All Members Model
    *
    * @return array object
    */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }

    /**
    * Get Member Mapped Device list
    *
    * @param array $params service parameter, Key:companyId,clubId
    *
    * @return array object
    */
    public function getMapDevice($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        /*$getMapDevice =
                'SELECT dm.is_deleted,d.device_code FROM t_device_member dm
                        LEFT OUTER JOIN t_device d ON d.device_id=dm.r_device_id
                        WHERE dm.r_user_id=?';*/
        $rsobj = $this->dbcon->Execute(GET_LIST_MAPDEVICE, array($userId));
        if ($rsobj->RecordCount()) {
            $mapDevice  =   array();
            while (!$rsobj->EOF) {
                $mapDevice[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get ID For Device Code',
                'total_records' => $rsobj->RecordCount(),
                // "sql" => $rsobj->sql,
                'mapDevice' => $mapDevice,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get ID For Device Code',
                'total_records' => 0,
                // "sql" => $rsobj->sql,
                'mapDevice' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }

    /**
    * Get Added / Existing City ID
    *
    * @param array $params service parameter, Key:companyId,clubId
    *
    * @return array object
    */
    public function getCityId($params)
    {
        $cityName = isset($params['city']) ? $params['city'] : '';
        $rsobj = $this->dbcon->Execute(GETCITYID, array($cityName));
        if ($rsobj->RecordCount()) {
            $status = 'success';
            $statusMsg = 'City ID Details Received';
            $result = $rsobj->fields['city_id'];
        } else {
            $params['city_name'] = $params['city'];

            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSQL($rsobj, $params);
            $this->dbcon->Execute($insert_sql);
            $lastInsertId = $this->dbcon->Insert_ID();
            $status = 'success';
            $statusMsg = 'City Added';
            $result = $lastInsertId;
        }
        $this->status = array(
            'status' => $status,
            'status_code' => 0,
            'status_message' => $statusMsg,
            // 'sql' => $rsobj->sql,
            'cityId' => $result,
        );
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Added / Existing Profession ID
    *
    * @param array $params service parameter, Key:profession
    *
    * @return array object
    */
    public function getProfessionId($params)
    {
        $professionName = isset($params['profession']) ? $params['profession'] : '';
        $rsobj = $this->dbcon->Execute(GETPROFESSIONID, array($professionName));
        if ($rsobj->RecordCount()) {
            $status = 'success';
            $statusMsg = 'profession ID Details Received';
            $result = $rsobj->fields['profession_id'];
        } else {
            $params['profession_name'] = $params['profession'];

            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSQL($rsobj, $params);
            $this->dbcon->Execute($insert_sql);
            $lastInsertId = $this->dbcon->Insert_ID();
            $status = 'success';
            $statusMsg = 'profession Added';
            $result = $lastInsertId;
        }

        $this->status = array(
                'status' => $status,
                'status_code' => 0,
                'status_message' => $statusMsg,
                // 'sql' => $rsobj->sql,
                'professionId' => $result,
        );
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Added / Existing Nationality ID
    *
    * @param array $params service parameter, Key:profession
    *
    * @return array object
    */
    public function getNationalityId($params)
    {
        $nationalityName = isset($params['nationality']) ? $params['nationality'] : '';
        $rsobj = $this->dbcon->Execute(GETNATIONALITYID, array($nationalityName));

        if ($rsobj->RecordCount()) {
            $status = 'success';
            $statusMsg = 'Nationality ID Details Received';
            $result = $rsobj->fields['nationality_id'];
        } else {
            $params['nationality_name'] = $params['nationality'];

            //Dynamically construct insert query
            $insert_sql = $this->dbcon->GetInsertSQL($rsobj, $params);
            $this->dbcon->Execute($insert_sql);
            $lastInsertId = $this->dbcon->Insert_ID();
            $status = 'success';
            $statusMsg = 'Nationality Added';
            $result = $lastInsertId;
        }
        $this->status = array(
            'status' => $status,
            'status_code' => 1,
            'status_message' => $statusMsg,
            // 'sql' => $rsobj->sql,
            'nationalityId' => $result,
        );
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Person Number Availablity
    *
    * @param array $params service parameter, Key:profession
    *
    * @return array object
    */
    public function getPersonNumberExist($params)
    {
        $personId = isset($params['personId']) ? $params['personId'] : '';
        $rsobj = $this->dbcon->Execute(GET_MEMBER_PERSON_NUMBER, array($personId));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Error',
                'person' => '1',
                'status_code' => 0,
                'status_message' => 'Person number already exist',
            );
        } else {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'person' => '0',
                'status_message' => 'Person Number Available',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get Status
    *
    * @param array $params service parameter, Key:status
    *
    * @return array object
    */
    /*public function getStatusId($params)
    {
        $statusId = array();
        $status = isset($params['status']) ? $params['status'] : '';
        $rsobj = $this->dbcon->Execute(GET_STATUS_ID, array($status));
        while (!$rsobj->EOF) {
            $statusId[] = $rsobj->fields;
            $rsobj->MoveNext();
        }
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Error',
                'statusId' => $statusId,
                'status_code' => 0,
                'status_message' => 'Status id retrieved',
            );
        } else {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'statusId' => '',
                'status_message' => 'Status name not found',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get Age On the Test by DOB
    *
    * @param array $params service parameter, Key:dob
    *
    * @return array object
    */
    /*public function getAgeOnTest($params)
    {
        $dob = isset($params['dob']) ? $params['dob'] : '';
        $rsobj = $this->dbcon->Execute(GET_AGE_ON_TEST, array($dob));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 0,
                'status_message' => 'Age On Test Successfully retrieved',
                'ageOnTest' => $rsobj->fields['age'],
            );
        } else {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'person' => '0',
                'status_message' => 'No Records found',
                'ageOnTest' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Update User & Get Results
    *
    * @param array $param service parameter, Key:userId
    *
    * @return array object
    */
    public function updateUserDetail($param)
    {
		  
        $userId = (isset($param['userId'])) ? $param['userId'] : '-1';
        $rsobj  = $this->dbcon->Execute(GET_UPDATEUSER, array($userId));
        $data   = array();
        if (isset($param['r_status_id'])) 
        {
            $data['r_status_id'] = $param['r_status_id'];
        }
        /*MK Added for Updating Club Information */
        if (isset($param['userClubId'])) 
        {
            $data['r_club_id'] = $param['userClubId'];
        }
        if (isset($param['userGender'])) 
        {
            $data['gender'] = $param['userGender'];
        }
        if ($rsobj->RecordCount()) 
        {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //if ($rsUpdate) {
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'User status updated successfully',
                );
            //} else {    
                //$this->status = $this->error_general;
            //}
        }

        return $this->status;
    }
    /**
    * Check & Get Exist Person ID
    *
    * @param array $param service parameter, Key:userId
    *
    * @return array object
    */
    public function personIdExist($param)
    {
        $userId = (isset($param['userId'])) ? $param['userId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_PERSONIDEXIST, array($userId));

        $status = 'Success';
        $statusMsg = 'Person Id Not Exist';
        $count = 0;
        if ($rsobj->RecordCount()) {
            $status = 'Error';
            $statusMsg = 'Person Id Exist';
            $count = $rsobj->fields['personCount'];
        }
        $this->status = array(
            'status' => $status,
            'status_code' => 0,
            'status_message' => $statusMsg,
            'personCount' => $count,
        );

        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Update Member Primay Club Details
    *
    * @param array $param service parameter, Key:clubId
    *
    * @return array object
    */
    public function primaryClubUpdate($param)
    {
        $userId = (isset($param['userId'])) ? $param['userId'] : '-1';
        $clubId = (isset($param['clubId'])) ? $param['clubId'] : '-1';
        $employeeCenterId = (isset($param['employeeCenterId'])) ? $param['employeeCenterId'] : '-1';
        $dateTime = date('Y-m-d H:i:s');

        $rsobj = $this->dbcon->Execute(UPDATE_USER_CLUB_PRIMARY, array($userId));
        /*$data = array(
                'r_club_id' => $clubId,
                'modified_by' => 1,
                'modified_date' => $dateTime,
        );*/
        if ($rsobj->RecordCount()) {
            $this->dbcon->Execute(
                "UPDATE `t_users` SET `r_club_id`='$clubId',`modified_by`='1',`modified_by`='$dateTime' 
                  WHERE `user_id`=$userId"
            );
            //$rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            //$rsUpdate = $this->dbcon->Execute($rsUpdates);    
        }

        $this->dbcon->Execute('UPDATE `t_employee_centre` SET `is_primary`=0 WHERE `r_user_id`='.$userId);

        $rsCenters = $this->dbcon->Execute(UPDATE_ISPRIMARY_CENTER, array($employeeCenterId));
        $dataCenters = array(
                'is_primary' => 1,
        );
        if ($rsCenters->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsCenters, $dataCenters);
            $this->dbcon->Execute($rsUpdates);
        }

        $this->status = array(
            'status' => '',
            'status_code' => 0,
            'status_message' => 'Successfully Updated',
        );

        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Update Check Last Test Progress Returns Test count
    *
    * @param array $param service parameter, Key:userId
    *
    * @return array object
    */
    public function checkLastTestProgress($param)
    {
        $userId = (isset($param['userId'])) ? $param['userId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_LAST_TEST_PROGRESS, array($userId));
        $status = 'Success';
        $statusMsg = 'Test has completed';
        $count = 0;

        if ($rsobj->RecordCount()) {
            $status = 'Error';
            $statusMsg = 'Test is in Progress';
            if ($rsobj->fields['testCount'] == 0 || $rsobj->fields['testCount'] == 3) {
                $count = 0;
            } else {
                $count = $rsobj->fields['testCount'];
            }
        }
        $this->status = array(
            'status' => $status,
            'status_code' => 0,
            'status_message' => $statusMsg,
            'testCount' => $count,
        );

        return array(
            'movesmart' => $this->status,
        );
    }

   
    /**
    * Gets the current all open test counts
    *
    * @param array $params service parameter
    *
    * @return array object
    */
    /*public function getCurrentTestCount($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_CURRENT_TEST_COUNT);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Success',
                'status_code' => 200,
                'status_message' => 'Records Found Successfully',
                'count' => $rsobj->fields,
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'status_message' => 'No Result',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Member Levels (Fat&Fit)
    *
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    public function getUserDataalevelbydesc($params)
    {
        $rsobj = $this->dbcon->Execute(GETUSERDATAVALUES_DESC, $params['userId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Success',
                'status_code' => 200,
                'status_message' => 'Records Found Successfully',
                'count' => $rsobj->fields,
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'status_message' => 'No Result',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Member Levels (Fat&Fit) ASEC
    *
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
        /*public function getUserDataalevelbyasc($params)
    {
        $rsobj = $this->dbcon->Execute(GETUSERDATAVALUES_ASC, $params['userId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Success',
                'status_code' => 200,
                'status_message' => 'Records Found Successfully',
                'count' => $rsobj->fields,
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'status_message' => 'No Result',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Member Levels (Fat&Fit) DESC
    *
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    /*public function getFatpercentLevelbydesc($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_DATA_FAT_LEVEL_DESC, $params['userId']);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'Success',
                'status_code' => 200,
                'status_message' => 'Records Found Successfully',
                'count' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'status_message' => 'No Result',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Member Levels (Fat) DESC
    *
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    /*public function getFatpercentLevelbyasc($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_DATA_FAT_LEVEL_ASC, $params['userId']);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'Success',
                'status_code' => 200,
                'count' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Member Mapped Device Details
    *
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    public function clientMappedDetails($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MAPPED_USER_DETAILS, $params['userId']);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'count' => $category,
             );
        } else {
            $this->status = array(
              'status' => 'Error',
             );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Member Personal Details Update
    *
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    public function updatePersonalInfo($params)
    {
        $rsobj = $this->dbcon->Execute(GET_EXIST_PERSONINFO_USER, array($params['userId']));

        $this->status = $this->error_general;

        if ($rsobj->RecordCount()) {
            /*User Address Information Update*/
            if (isset($params['cityId'])) {
                $data['r_city_id'] = $params['cityId'];
            }
            if (isset($params['nationalityId'])) {
                $data['r_nationality_id'] = $params['nationalityId'];
            }
            if (isset($params['doorNo'])) {
                $data['doorno'] = $params['doorNo'];
            }
            if (isset($params['address'])) {
                $data['address'] = $params['address'];
            }
            if (isset($params['zipCode'])) {
                $data['zipcode'] = $params['zipCode'];
            }
            if (isset($params['bus'])) {
                $data['bus'] = $params['bus'];
            }
            /*User Persional Information Update*/
            if (isset($params['dob'])) {
                $data['dob'] = $params['dob'];
            }
            if (isset($params['height'])) {
                $data['height'] = $params['height'];
            }
            if (isset($params['weight'])) {
                $data['weight'] = $params['weight'];
            }
            if (isset($params['professionId'])) {
                $data['r_profession_id'] = $params['professionId'];
            }
            if (isset($params['phone'])) {
                $data['phone'] = $params['phone'];
            }
            /*MK GP Information Update*/
            if (isset($params['doctorName'])) {
                $data['doctor'] = $params['doctorName'];
            }
            if (isset($params['doctorDoorno'])) {
                $data['doctor_doorno'] = $params['doctorDoorno'];
            }
            if (isset($params['doctorStreet'])) {
                $data['doctor_address'] = $params['doctorStreet'];
            }
            if (isset($params['doctorBus'])) {
                $data['doctor_bus'] = $params['doctorBus'];
            }
            if (isset($params['doctorZipcode'])) {
                $data['doctor_zipcode'] = $params['doctorZipcode'];
            }
            if (isset($params['doctorPlace'])) {
                $data['doctor_city'] = $params['doctorPlace'];
            }
            if (isset($params['doctorCountry'])) {
                $data['doctor_country'] = $params['doctorCountry'];
            }
            if (isset($params['doctorPhone'])) {
                $data['doctor_phone'] = $params['doctorPhone'];
            }
            if (isset($params['doctorEmail'])) {
                $data['doctor_email'] = $params['doctorEmail'];
            }
            if (isset($params['professionWorkType'])) {
                $data['profession_labortype'] = $params['professionWorkType'];
            }
            /*PK Added Signature & created Date*/
            if (isset($params['clientSignature'])) {
                $data['signature'] = $params['clientSignature'];
                $data['entry_date'] = date('Y-m-d h:i:s', strtotime($params['clientEntryDate']));
            }
			
            /*Update User Type*/
            /*if (isset($params['stepTypeId'])) {
                $data['step_entry_type'] = $params['stepTypeId'];
            }*/
			
            /*SN added Register  personalinfo step 20160201*/
            if (isset($params['clientCurrentStep'])) {
                $data['personal_track'] = json_encode(array('step' => $params['clientCurrentStep']));
            }
            $data['modified_date'] = date('Y-m-d h:i:s');

            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
			$str="update t_users set phone=".$data['phone']." where user_id=".$params['userId'];
			mysql_query($str);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Member assign device success',
            );
        }
        if (isset($params['userGender']) and $params['userGender'] != '') {
            $this->updateUserDetail(array(
                'userId' => $params['userId'],
                'userGender' => $params['userGender'],
            ));
        }
        //Return the result array    
        return $this->status;
    }
    /**
    * Member Activity Details
    * 
    * @return array object
    */
    public function getActivityList()
    {
        $rsobj = $this->dbcon->Execute(GET_ACTIVTY_QUERY);
        if ($rsobj->RecordCount()) {
            $activites  =   array();
            while (!$rsobj->EOF) {
                $activites[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Activities List Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'activitesList' => $activites,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Nationality List Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'nationalityList' => '',
            );
        }
        //Return the result array
        return $this->status;
    }
   /**
    * Update/ Add Activity Details for member
    * 
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    public function updateActivityMember($params)
    {
        foreach ($params['activity'] as $activity) {
            $rsobj = $this->dbcon->Execute(GET_ACTIVTY_MEMBER, array($params['userId'], $activity['activity']));
            if ($rsobj->RecordCount()) {
                $data['frequency_type'] = $activity['freq'];
                $data['average_time'] = $activity['avgtime'];
                $data['modified_by'] = $params['userId'];
                $data['modified_date'] = date('Y-m-d h:i:s');
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
            } else {
                $this->dbcon->Execute(
                    INSERT_ACTIVTY_MEMBER,
                    array(
                        $params['userId'], $activity['activity'],
                        $activity['freq'], $activity['avgtime'],
                        $params['userId'], date('Y-m-d h:i:s')
                    )
                );
                //$lastInsertId = $this->dbcon->Insert_ID();
                //if($lastInsertId)
            }
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Activity member added success fully',
        );

        return $this->status;
    }
    /**
    * Update/ Add Register Member Medial Information
    * 
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    public function updateMemberMedicalInfo($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MEMEBER_MEDICALINFO, array($params['userId']));
        if (isset($params['injuryDesc'])) {
            $data['have_injury'] = 1;
            $data['injury_description'] = $params['injuryDesc'];
        }
        if (isset($params['haveMedication'])) {
            $data['have_medication'] = $params['haveMedication'];
            $data['medication_description'] = ($params['haveMedication'] == 1) ? $params['medicationDesc'] : '';
        }
        if (isset($params['haveStressTest'])) {
            $data['have_stress_test'] = $params['haveStressTest'];
            $data['stress_test_on']
                =($params['haveStressTest'] == 1) ? date('Y-m-d h:i:s', strtotime($params['stressTestOn'])) : '';
        }
        if (isset($params['haveSmoke'])) {
            $data['have_smoke'] = $params['haveSmoke'];
            $data['smoke_amount'] = ($params['haveSmoke'] == 1) ? $params['smokeAmount'] : '';
        }
        if (isset($params['howFeelToday'])) {
            $data['feel_today'] = $params['howFeelToday'];
        }
        if ($rsobj->RecordCount()) {
            $data['modified_by'] = $params['userId'];
            $data['modified_date'] = date('Y-m-d h:i:s');
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'injury Description Updated Successfully',
            );
        } else {
            $data['r_user_id'] = $params['userId'];
            $data['created_by'] = $params['userId'];
            $data['created_date'] = date('Y-m-d h:i:s');
            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully Image Uploaded',
            );
        }

        return $this->status;
    }
    /*SK Added fot getting Machines list of details*/
    /**
    * Get Member Machines list
    * 
    * @param array $param service parameter, Key:userId
    *
    * @return array object
    */
    public function listMachinesData($param)
    {  //1234
        $params['groupID'] = isset($param['groupID']) ? $param['groupID'] : '';
        $whr = '';
        //$leftJoin = '';
        //$andClubCondtion = '';

        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        //$companyId = isset($param['companyId']) ? $param['companyId'] : -1;
        $clubId = isset($param['clubId']) ? $param['clubId'] : '';

        $whr .= "Where sm.is_deleted = 0";

        $searchType = isset($param['searchType']) ? $param['searchType'] : '';
        $searchVal = isset($param['searchValue']) ? $param['searchValue'] : '';
	
	 if ($clubId != '') {
            $whr .= " AND sm.clubid = ".$clubId;
        }
		if ($params['groupID'] != '') {
            $whr .= " AND sm.r_group IN (".$params['groupID'].')';
        }
        if ($searchType != '' && $searchVal != '') {
            $whr .= ' AND sm.'.$searchType." LIKE '".$searchVal."%'";
        }
        $query = GET_LIST_MACHINES_DATA.$whr.$limit;
		//echo $query;
		//die;
        $rsobj = $this->dbcon->Execute($query);
        if ($rsobj && $rsobj->RecordCount()>0) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
                'status' => 'success',
                'error_code' => 200,
                'data' => $category,
                'count' => $this->getLastQueryTotalCount(),
                'rows' => $rsobj->_numOfRows,
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 404,
            );
        }

        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    /**
    * Get Last Test Member Id
    * 
    * @param array $params service parameter, Key:userId
    *
    * @return array object
    */
    /*public function getLastTestID($params)
    {
        /*MK Added For cardio */
        /* testType=6 said to be a cardio test  need to be Update Core in future* /
        $QUERY = GET_MAX_TEST_DETAILS;
        $pramsArr = array($params['userId'], $params['testType']);
        $type = 'other';
        if ($params['testType'] == 6) {
            $QUERY = GET_LASTCARDIO_TEST_ID;
            $pramsArr = array($params['userId']);
            $type = 'cardio';
        }
        $rsobj = $this->dbcon->Execute($QUERY, $pramsArr);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'error_code' => 200,
                'testrecord' => $rsobj->fields,
                'type' => $type,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 404,
            );
        }

        return  $this->status;
        // );
    }
    */
    /**
    * Get Last Test REsult for Member
    * 
    * @param array $params service parameter, Key:userId,lastId
    *
    * @return array object
    */
    /*public function getlastTestResult($params)
    {
        $rsobj = $this->dbcon->Execute(GET_LAST_TEST_DETAILS, array($params['userId'], $params['lastId']));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 200,
                'count' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 404,
            );
        }

        return  $this->status;
       // );
    }
    */
    /**
    * Get Last Test REsult for Member
    * 
    * @param array $param service parameter, Key:userId,lastId
    *
    * @return array object
    */
    public function machinesData($param)
    {
        $clubId = isset($param['clubid']) ? $param['clubid'] : 1;

        $whr = " WHERE sm.clubid = '".$clubId."'";
        $rsobj = $this->dbcon->Execute(GET_LIST_MACHINES_DATA.$whr);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                 'total_records' => $rsobj->RecordCount(),
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    /**
    * Get Device By ID
    * 
    * @param array $param service parameter, Key:userId,lastId
    *
    * @return array object
    */
    public function getDeviceById($param)
    {
        $clubId = isset($param['clubid']) ? $param['clubid'] : 1;
        $deviceid = isset($param['deviceid']) ? $param['deviceid'] : 0;
        $userid = isset($param['userid']) ? $param['userid'] : 0;

        $whr = " WHERE r_clubid = '".$clubId."' AND createdby='".$userid."' AND deviceid='".$deviceid."'";
        $rsobj = $this->dbcon->Execute(GET_DEVICE_DATA.$whr);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    /**
    * Get Device Data By User & Club
    * 
    * @param array $param service parameter, Key:userid,clubid
    *
    * @return array object
    */
    public function getListDeviceData($param)
    {
        $clubId = isset($param['clubid']) ? $param['clubid'] : 1;
        $userid = isset($param['userid']) ? $param['userid'] : 0;

        //$whr = " WHERE r_clubid = '".$clubId."' AND createdby='".$userid."'";
        $rsobj = $this->dbcon->Execute(GET_DEVICE_MACHINE_DATA, array($clubId, $userid));
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'sucess_code' => 1,
                'data' => $category,
                'total_records' => $rsobj->RecordCount(),

            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'sucess_code' => 0,
            );
        }

        return  $this->status;
    }
    /**
    * Add/Update Device Data By User & Club
    * 
    * @param array $param service parameter, Key:userid,clubid
    *
    * @return array object
    */
    public function saveDeviceData($param)
    {
        $clubId = isset($param['clubid']) ? $param['clubid'] : 1;
        $machineid = isset($param['machineid']) ? $param['machineid'] : 0;
        $userid = isset($param['userid']) ? $param['userid'] : 0;
        $date = date('Y-m-d H:i:s');
        $rsobj = $this->dbcon->Execute(GET_DEVICE_EXIST, array($userid));
        $data = array(
            'r_clubid' => $clubId,
            'r_machineid' => $machineid,
            'createdby' => $userid,
            'modifiedon' => $date,
        );
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => ' Update Successfully ',
            );
        } else {
            $machinedeviceid = $param['random'];
            $data['device'] = $machinedeviceid;
            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => ' Insert Successfully ',
            );
        }

        return  $this->status;
    }
    /**
    * Get Brand List
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getbrandslist($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_BRAND_LIST);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    
    /**
    * Get type List
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function gettypelist($params)
    {
        
        $rsobj = $this->dbcon->Execute(GET_TYPE_LIST);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    
    /**
    * Get group List
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getgrouplist($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_GROUP_LIST);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    
    /**
    * Get sub type List
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getsubtypelist($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_SUBTYPE_LIST);
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    
    /**
    * Get programs List
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    /*public function getProgramlist($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_PROGRAM_LIST);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'error_code' => 1,
                'data' => $category,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'error_code' => 0,
            );
        }

        return  $this->status;
    }
    */
    /**
    * Save new machine details
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function newmachinedetails($params)
    {
		// die;
        $params['date'] = date('Y-m-d H:i:s');
        $params['clubautoid'] = isset($params['clubautoid']) ? $params['clubautoid'] : 0;
	$params['description'] = isset($params['description']) ? $params['description'] : 0;
		
        $params['coach_club'] = isset($params['coach_club']) ? $params['coach_club'] : 0;
        $params['active'] = isset($params['active']) ? $params['active'] : 0;
        $params['machine_order'] = isset($params['machine_order']) ? $params['machine_order'] : 0;
        $params['mixtraining'] = isset($params['mixtraining']) ? $params['mixtraining'] : 0;
        $params['circuittraining'] = isset($params['circuittraining']) ? $params['circuittraining'] : 0;
        $params['freetraining'] = isset($params['freetraining']) ? $params['freetraining'] : 0;
        $params['certified'] = isset($params['certified']) ? $params['certified'] : 0;
        $is_EditID = isset($params['editID']) ? $params['editID'] : 0;
        $image = isset($params['imagename']) ? $params['imagename'] : '';
		 $image_url = isset($params['image_url']) ? $params['image_url'] : '';
		  $confactor = isset($params['confactor']) ? $params['confactor'] : '';
		 $video_url = isset($params['video_url']) ? $params['video_url'] : '';
        $exerciseVideo = isset($params['exercisevideo']) ? $params['exercisevideo'] : 0;
		$length = isset($params['weights'])?count($params['weights']):0;
		//echo $length;
        $lasUpdatedInserted = $is_EditID;
        if ($is_EditID > 0) {
			
             /*$this->dbcon->Execute(
                UPDATE_NEW_MACHINE_DETAILS,
                array(
                    $params['clubautoid'], $params['nameofmachine'],
                    $params['brand'], $params['type'],
                    $params['group'],
                    $params['subtype'], $params['coach_club'], $params['description'],
                    $params['active'], $params['machine_order'], $params['mixtraining'],
                    $params['circuittraining'], $params['freetraining'],
                    $params['certified'], $image, $exerciseVideo,
                    $params['date'], $params['createdby'], $is_EditID
                )
            );*/
$update = 'UPDATE t_strength_machine SET `uniqueid`="'.$params['clubautoid'].'",
`machine_name`="'.$params['nameofmachine'].'",`brand`='.$params['brand'].',
`r_type`='.$params['type'].',`r_group`='.$params['group'].',`subtype`='.$params['subtype'].',
`clubid`='.$params['coach_club'].',`description`="'.$params['description'].'",
`active`='.$params['active'].',`outoforder`='.$params['machine_order'].',
`allow_mix`='.$params['mixtraining'].',`allow_circuit`='.$params['circuittraining'].',
`allow_free_training`='.$params['freetraining'].',`is_certified`='.$params['certified'].',
`machineimage`="'.$image.'",`videourl`="'.$video_url.'",`image_url`="'.$image_url.'",
`f_confactor`='.$confactor.',`modified_date`="'.$params['date'].'",
`modified_by`='.$params['createdby'].' WHERE `strength_machine_id`= '.$is_EditID;
			
			mysql_query($update);
					
			
		if($length>0){
			//echo "hello";
			$club_id = $params['coach_club'];
			for($i=0;$i<$length;$i++){
				
				$WEIGHT = $params['weights'][$i];
				$pinposition = $params['pinpositions'][$i];
				
				$str="INSERT INTO `t_bolck_weight`(strength_machine_id,club_id,weight,position) VALUES ($is_EditID,$club_id,$WEIGHT,$pinposition)";
			//echo $str;
				mysql_query($str);
			}
		}
            $updatetype = 'Update';
            $isupdate = 1;
        } else {
			
            $q = $this->dbcon->Execute(
                INSERT_NEW_MACHINE_DETAILS,
                array(
                    $params['clubautoid'], $params['nameofmachine'],
                    $params['brand'], $params['type'], $params['group'],
                    $params['subtype'], $params['coach_club'],
                    $params['description'], $params['active'], $params['machine_order'],
                    $params['mixtraining'], $params['circuittraining'], $params['freetraining'],
                    $params['certified'], $image, $video_url,$image_url,$confactor, $params['date'], $params['createdby']
                )
            );
			
			//$length = count($params['weights']);
		
            $updatetype = 'Insert';
            $isupdate = 0;
            $lasUpdatedInserted = $this->dbcon->Insert_ID();
			if($length>0){
				//echo "hello";
				$club_id = $params['coach_club'];
			for($i=0;$i<$length;$i++){
				
				$WEIGHT = $params['weights'][$i];
				$pinposition = $params['pinpositions'][$i];
				//print_r($data);
				$str="INSERT INTO `t_bolck_weight`(strength_machine_id,club_id,weight,position) VALUES ($lasUpdatedInserted,$club_id,$WEIGHT,$pinposition)";
				//echo $str;
				mysql_query($str);
			}
			}
        }
        if ($lasUpdatedInserted) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_id' => 200,
                'status_message' => ''.$updatetype.' new machine Successfully',
                'insertid' => $lasUpdatedInserted,
                'update' => $isupdate,
				'sql' => $q->sql
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
				'sql' => $q->sql
            );
        }

        return $this->status;
    }

    /**
    * Get last club details
    * 
    * @param array $params service parameter
    *
    * @return array object
    */
    public function getlastclubiddetails($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MAX_AUTOCLUBID, $params['clubid']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                    'status' => 'success',
                    'error_code' => 200,
                    'result' => $rsobj->fields,
                );
        } else {
            $this->status = array(
                'status' => 'Error',
                'error_code' => 404,
                );
        }

        return  $this->status;
    }
    
    /**
    * get member test to be analyzed
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getMemberTestTobeAnalysed($params)
    {
		//printLog($params);
        $rsobj = $this->dbcon->Execute(GET_TESTFOR_ANALYSE, $params['userId']);
		//printLog($rsobj);
        //echo $rsobj->sql;
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'recordcount' => $rsobj->RecordCount(),
                'testrecord' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'error_code' => 404,
            );
        }
		
        return $this->status;
    }
    /*MK Added Function - To get Last Test Details*/
    
    /**
    * get latest test details
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getLastTestDetails($params)
    {
        $QUERY = GET_LASTTEST_DETAILS;
        $pramsArr = array($params['userId'], $params['testType']);
        $type = 'other';
        if ($params['testType'] == 6) {
            $QUERY = GET_LASTCARDIO_TEST;
            $pramsArr = array($params['userId']);
            $type = 'cardio';
        }
        $rsobj = $this->dbcon->Execute($QUERY, $pramsArr);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'testrecord' => (($type != 'other') ? $rsobj->fields : $rsobj->GetRows()),
                'type' => $type,
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'error_code' => 404,
            );
        }

        return $this->status;
    }
    /*SNK to get all the test details for the club and date */
    
    /**
    * get Members Test Result data For Club and Date
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getMembersTestResultdataForClubandDate($params)
    {
		
        $QUERY = GET_LASTCARDIO_TEST_ALL_MEM;
        $statusIn = (object) array('inobj' => $params['statusdet']);
        $ActiveIn = (object) array('inobj' => $params['isActive']);
        $pramsArr = array($params['clubId'], $params['testDate'], $statusIn, $ActiveIn);
        if (isset($params['userId'])) {
			
            $QUERY = GET_LASTCARDIO_TEST_BY_MEM;
            array_push($pramsArr, $params['userId']);
        }
        $rsobj = $this->dbcon->Execute($QUERY, $pramsArr);
		//printLog($rsobj);
		
      // echo "<pre>";print_r($rsobj->GetRows());
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'testrecord' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 404,
                'error_code' => 404,
            );
        }
	
        return $this->status;
    }
    /*MK Added Function - To get Last Test Details*/
    
    /**
    * get cardio Test by Id
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getCardioTestByTestID($params)
    {
        //$QUERY = GET_CARDIO_TESTBY_ID;
        $pramsArr = array($params['userTestID']);
        $rsobj = $this->dbcon->Execute(GET_CARDIO_TESTBY_ID, $pramsArr);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'testrecord' => $rsobj->fields,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'status_code' => 404,
            'error_code' => 404,
            );
        }

        return $this->status;
    }
    /*MK Added Function - To get Last Test Details*/
    
    /**
    * get cardio test details
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function CardiotestDetails($params)
    {
        $obj = (object) array('inobj' => $params['userId']);
        $qryparams = array($obj, $params['testDate'], $params['status']);
        //if(isset($params['status'])) {
            //$qryparams[]=    $params['status'];
            //$query        =    GET_CARDIOTEST_MEMBERS;//,$pramsArr);
        //}
        $rsobj = $this->dbcon->Execute(GET_CARDIOTEST_MEMBERS, $qryparams);
        //echo $rsobj->sql;
        if ($rsobj->RecordCount()) {
            $rowdata = $rsobj->GetRows();
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'result' => $rowdata,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'status_code' => 404,
            'error_code' => 404,
            'result' => array(),
            );
        }

        return $this->status;
    }

    /**
    * To get cardio to do members
    * 
    * @param array $param service parameter, Search arguments
    *
    * @return array object
    */
    public function cardioTodoMembers($param)
    {
        $whr = '';
        $leftJoin = '';
        //$andClubCondtion = '';
        $limit = '';
        if (isset($param['limitStart']) && isset($param['limitEnd'])) {
            $limitStart = $param['limitStart'];
            $limitEnd = $param['limitEnd'];
            $limit = ' LIMIT '.$limitStart.','.$limitEnd;
        }
        $companyId = isset($param['companyId']) ? $param['companyId'] : -1;
        $whr .= ' WHERE u.r_company_id='.$companyId.' AND u.is_deleted<>1 AND r_usertype_id=3 ';
        //this condition is All other Pages.
        $whr .= (
            isset($param['clubId'])
            && $param['clubId'] != ''
        ) ? ' AND u.r_club_id='.$param['clubId'].' ' : '';

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] != 'status'
        ) ? ' AND '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';

        /*If custom search not done all authorized Club Id will be displayed.*/
        if (!isset($param['clubId']) or $param['clubId'] == '') {
            $whr .= (
                isset($param['authorizedClubId'])
                && $param['authorizedClubId'] != ''
            ) ? ' AND u.r_club_id IN ('.$param['authorizedClubId'].')' : '';
        }

        $whr .= (
            isset($param['searchType'])
            && $param['searchType'] != ''
            && $param['searchValue'] != ''
            && $param['searchType'] == 'status'
        ) ? ' HAVING '.$param['searchType']." LIKE '".$param['searchValue']."%' " : '';
        $whr .= (
            isset($param['testDate'])
            && $param['testDate'] != ''
        ) ? " AND DATE_FORMAT(cardiotest.test_start_date,'%Y-%m-%d')='".$param['testDate']."' " : '';

        /*Used to get sorting*/
        $groupby = 'GROUP BY cardiotest.test_start_date,u.user_id ';
        $getMemberQuery = GET_MEMBERS_CARDIO_TODO.$leftJoin.$whr.$groupby.
            ' ORDER BY '.$param['labelField'].' '.$param['sortType'].$limit;
        $status = isset($param['status']) ? $param['status'] : '0';
        $statusobj = $status;
        if (count(explode(',', $status)) >= 1) {
            $statusobj = (object) array('inobj' => $status);
        }

        $rsobj = $this->dbcon->Execute($getMemberQuery, array($statusobj));
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                /* This is to get the list of devices for that client */
                //if (isset($param['attachdevice'])){
                    $rsobj->fields['attacheddevice'] = $this->getMapDevice(
                        array('userId' => $rsobj->fields['user_id'])
                    );
                //}
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => $category,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * To edit new machine details
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function editNewMachinedetails($params)
    {
        $rsobj = $this->dbcon->Execute(EDIT_NEW_MACHINE_DETAILS, array($params['editId']));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'result' => $rsobj->fields,
            );
        } else {
            $this->status = array(
            'status' => 'Error',
            'status_code' => 0,
            );
        }

        return  $this->status;
    }
    
    /**
    * To delete new machine details
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function delNewMachineDetails($params)
    {
        $deleteIds = isset($params['items']) ? $params['items'] : '';
        if ($deleteIds != '') {
            $rsobj = $this->dbcon->Execute(DELETE_NEW_MACHINE_DETAILS, array($deleteIds));
            if ($rsobj) {
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_id' => 200,
                    'status_message' => 'Machine Deleted Successfully from list',
                );
            } else {
                $this->status = array(
                'status' => 'Error',
                'status_code' => 0,
                );
            }
        } else {
            $this->status = array(
            'status' => 'Error',
            'status_code' => 0,
            );
        }

        return  $this->status;
    }
    
    /**
    * To delete machine details by id
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function deletemachinedetaislbyId($params)
    {
        $editid = isset($params['editID']) ? $params['editID'] : '';
        $rsobj = $this->dbcon->Execute(DELETE_NEW_MACHINE_DETAILS, $editid);
        if ($rsobj) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_id' => 200,
                'status_message' => 'Machine Deleted Successfully from list',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_id' => 404,
            );
        }

        return $this->status;
    }
    /*MK Added Heartrate Deveices List*/
    
     /**
    * To get heart rate devices
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getHeartrateDevices($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_HEARTRATE_DEVICES);
        $devices = array();
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $devices[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'devices' => $devices,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }

        return $this->status;
    }
    /*SNK Get all mapped device info*/
    
    /**
    * To get mapped device info
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getMappedDeviceInfo($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_HEARTRATE_DEVICES_ATTACHED_USER);
        $devices = array();
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $devices[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                'devices' => $devices,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                'devices' => '',
                'totalCount' => '0',
            );
        }

        return $this->status;
    }

    /*MK Added - Update Heartrate on Test*/
    /**
    * To update heart rate on test
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function updateHeartRateOnTest($params)
    {
        /* MK Added Query to Update Heartrate,RPM group & LoadValue on pulse */
        $rsobj = $this->dbcon->Execute(GET_USERTESTBYID, array($params['userTestID']));
        if ($rsobj->RecordCount()) {
            $loadData = $params['updatedata'];
            if (isset($loadData['loadData'])) {
                $updatedata['load_value'] = json_encode($loadData['loadData']);
            }
            if (isset($loadData['rpm'])) {
                $updatedata['rpm'] = $loadData['rpm'];
            }
            if (isset($loadData['rpmgroup'])) {
                $updatedata['rpm_group'] = $loadData['rpmgroup'];
            }
            if (isset($loadData['test_active'])) {
                $updatedata['test_active'] = $loadData['test_active'];
            }
            if (isset($loadData['heartrate'])) {
                $updatedata['heart_rate_value'] = $loadData['heartrate'];
            }
            if (isset($loadData['coolDownStart'])) {
                $updatedata['cool_down_start_time'] = $loadData['coolDownStart'];
            }
            if (isset($loadData['coolDownStop'])) {
                $updatedata['cool_down_stop_time'] = $loadData['coolDownStop'];
            }
            if (isset($loadData['testCompleted'])) {
                $updatedata['is_test_completed'] = $loadData['testCompleted'];
            }
            if (isset($loadData['coolDownHeartrate'])) {
                $updatedata['heart_rate_with_cool_down'] = $loadData['coolDownHeartrate'];
            }
            if (isset($loadData['weightOnTest'])) {
                $updatedata['weight'] = $loadData['weightOnTest'];
            }
            if (isset($loadData['startlevel'])) {
                $updatedata['test_level'] = $loadData['startlevel'];
            }
            if (isset($loadData['totalLoadValue'])) {
                $updatedata['total_load_value'] = $loadData['totalLoadValue'];
            }
            if (isset($loadData['status'])) {
                $updatedata['status'] = $loadData['status'];
            }
            if (isset($loadData['equipmentID'])) {
                $updatedata['r_equipment_id'] = $loadData['equipmentID'];
            }
            if (isset($loadData['TestElapsedTime'])) {
                $updatedata['current_test_time'] = $loadData['TestElapsedTime'];
            }
            if (isset($loadData['isAnalysed'])) {
                $updatedata['is_analysed'] = $loadData['isAnalysed'];
            }
            if (isset($loadData['dataAnalysis'])) {
                $updatedata['analysed_data'] = $loadData['dataAnalysis'];
                $rstestparam = $this->dbcon->Execute(GET_USERTEST_PARAM_BYTESTID, array($params['userTestID']));
                if ($rstestparam->RecordCount()) {
                    $updatepramdata = array();
                    $analyseddata = json_decode($loadData['dataAnalysis'], true);
				
                    if (isset($analyseddata['age'])) {
                        $updatepramdata['age_on_test'] = $analyseddata['age'];
                    }
                    if (isset($analyseddata['weight'])) {
                        $updatepramdata['weight_on_test'] = $analyseddata['weight'];
                    }
                    if (isset($analyseddata['fitlevel'])) {
                        $updatepramdata['fitness_level'] = $analyseddata['fitlevel'];
                    }
                    if (isset($analyseddata['iant_hr'])) {
                        $updatepramdata['iant_hr'] = $analyseddata['iant_hr'];
                    }
                     if (isset($analyseddata['max_max_load'])) {
                        $updatepramdata['max_max_load'] = $analyseddata['max_max_load'];
                    }
					  if (isset($analyseddata['max_load'])) {
                        $updatepramdata['max_load'] = $analyseddata['max_load'];
                    }
					  if (isset($analyseddata['max_heart_rate'])) {
                        $updatepramdata['max_heart_rate'] = $analyseddata['max_heart_rate'];
                    }
					 if (isset($analyseddata['min_hr_70'])) {
                        $updatepramdata['min_hr_70'] = $analyseddata['min_hr_70'];
                    }
					 if (isset($analyseddata['max_hr_90'])) {
                        $updatepramdata['max_hr_90'] = $analyseddata['max_hr_90'];
                    }
                    if (isset($analyseddata['defpoint'])) {
                        $updatepramdata['iant_p'] = $analyseddata['defpoint']['X'];
                    }
                    if (isset($analyseddata['regr1'])) {
                        $updatepramdata['reg_info1'] = json_encode($analyseddata['regr1']);
                    }
                    if (isset($analyseddata['regr2'])) {
                        $updatepramdata['reg_info2'] = json_encode($analyseddata['regr2']);
                    }

                    $rspramUpdates = $this->dbcon->GetUpdateSQL($rstestparam, $updatepramdata);
                    $this->dbcon->Execute($rspramUpdates);
					//insert into hr_zone_activity //
					
					$fitness=$analyseddata['fitlevel'];
			 
			$age = $analyseddata['age'];
			$weight = $analyseddata['weight'];
			$iand_p = $analyseddata['max_max_load'];
			$user_id = $params['userId'];
			$this->save_hrzone($fitness,$age,$weight,$iand_p,$user_id,$analyseddata['regr1'],$params['userTestID']);
			
					//end//
                }
            }
            if (isset($loadData['analysed_info'])) {
                $updatedata['analysed_info'] = $loadData['analysed_info'];
            }
            if (isset($loadData['analysis_graphdata'])) {
                $updatedata['analysis_graphdata'] = $loadData['analysis_graphdata'];
            }
            if (isset($loadData['fitplan'])) {
                $testdata = $rsobj->fields;
                //echo $testdata['test_start_date'];
                $fitdataarr = json_decode($loadData['fitplan'], true);
                $rstraining = $this->dbcon->Execute(GET_CARDIO_TRAINING_BYTESTID, array($params['userTestID']));
                $trainingdata = array();
                $trainingdata['f_testid'] = $params['userTestID'];
                $trainingdata['f_trainingtype'] = TRAINING_TYPE_CARDIO;

                $myDate = $testdata['test_start_date'];
                $next_monday = date('Y-m-d', strtotime('next monday', strtotime($myDate)));
                $trainingdata['f_trainingstdate'] = $next_monday;

                $trainingend = date('Y-m-d', strtotime('+12 week', strtotime($next_monday)));
                $trainingend = date('Y-m-d', strtotime('-1 day', strtotime($trainingend)));
                $trainingdata['f_trainingeddate'] = $trainingend;

                if ($rstraining->RecordCount()) {
                    $rstrainingdata = $rstraining->fields;
                    $trainingdata['f_mddttm'] = date('Y-m-d h:i:s');
                    $rstrainingUpdates = $this->dbcon->GetUpdateSQL($rstraining, $trainingdata);
                    $this->dbcon->Execute($rstrainingUpdates);
                    $trainingid = $rstrainingdata['f_cardiotrainingid'];
                } else {
                    $trainingdata['f_crdttm'] = date('Y-m-d h:i:s');
                    $rstrainingUpdates = $this->dbcon->GetInsertSQL($rstraining, $trainingdata);
                    $this->dbcon->Execute($rstrainingUpdates);
                    $trainingid = $this->dbcon->Insert_ID();
                }

                $rstrainingplan = $this->dbcon->Execute(GET_CARDIO_TRAINING_PLAN_BYTRAININGID, array($trainingid));
                $trainingplandata = array();
                if ($rstrainingplan->RecordCount()) {
                    $trainingplandata['f_isdelete'] = 1;
                    $rstrainplanupd = $this->dbcon->GetUpdateSQL($rstrainingplan, $trainingplandata);
                    $this->dbcon->Execute($rstrainplanupd);
                }
                foreach ($fitdataarr as $fitdata) {
                    $trainingplandata['f_isdelete'] = 0;
                    $trainingplandata['f_cardiotrainingid'] = $trainingid;
                    $trainingplandata['f_weeknr'] = $fitdata['f_weeknr'];
                    $trainingplandata['f_points'] = $fitdata['f_points'];
                    $trainingplandata['f_min'] = $fitdata['f_min'];
                    $trainingplandata['f_max'] = $fitdata['f_max'];
                    $trainingplandata['f_min_a'] = $fitdata['f_min_a'];
                    $trainingplandata['f_max_a'] = $fitdata['f_max_a'];
                    $trainingplandata['f_crdttm'] = date('Y-m-d h:i:s');

                    $rstrainplanupd = $this->dbcon->GetInsertSQL($rstrainingplan, $trainingplandata);
                    $this->dbcon->Execute($rstrainplanupd);
                }

                $userid = $testdata['r_user_id'];
                $statusIn = (object) array('inobj' => GROUP_MOVESMART);
                $rsreedobj = $this->dbcon->Execute(GET_VALID_CREDIT_TO_REDEEM, array($userid, $statusIn));
                $rsreeddata = array();
                if ($rsreedobj->RecordCount()) {
                    $rsreeddata['f_status'] = 1;
                    $rsreedobjupdates = $this->dbcon->GetUpdateSQL($rsreedobj, $rsreeddata);
                    //echo $rsreedobjupdates;
                    $this->dbcon->Execute($rsreedobjupdates);
                }
            }

            $updatedata['modified_date'] = date('Y-m-d h:i:s');
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $updatedata);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Updated Successfully',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Update failure',
            );
        }

        return array('movesmart' => $this->status);
    }
	public function save_hrzone($fitness,$age,$weight,$iand_p,$user_id,$reg1,$userTestID)
{
	$r_user_test_id = $userTestID;
	 $fpcsp = 'f_pcsp'.$fitness;
		$fdeflcoef = 'f_deflcoef'.$fitness;
			$statement  =   "SELECT ".$fpcsp.",".$fdeflcoef.",f_minhrcond,f_maxhrcond,activity_name,f_activityid FROM t_activitypoints as actpoint left join t_activity as act on actpoint.f_activityid = act.activity_id where actpoint.f_min_level <=".$fitness." AND f_max_level >=".$fitness;
		
			$row = mysql_query($statement);
			while($result = mysql_fetch_array($row))
			{
			$fpcsps = $result['f_pcsp'.$fitness];
			$fdeflcoefs = $result['f_deflcoef'.$fitness];
			$f_minhr = $result['f_minhrcond'];
			$f_maxhr = $result['f_maxhrcond'];
			$activity_name = $result['activity_name'];
			$activity_id = $result['f_activityid'];
			$user_str = "select * from t_users where user_id = ".$user_id;
			$row1 = mysql_query($user_str);
			$user = mysql_fetch_array($row1);
			if(!empty($user)){
			$gender = $user['gender'];
			}
			$statement2 = "SELECT f_point_speeda FROM t_fitlevel where f_agemin <=".$age." AND f_agemax >=".$age." AND f_fitlevel=".$fitness." AND f_gender=".$gender;
			$row3 = mysql_query($statement2);
			$point_speed = mysql_fetch_array($row3);
			$speed_point = $point_speed['f_point_speeda'];
			$activity_point =round((($speed_point*$fpcsps)/100),1);
			$B0 = $reg1['B0'];
			$B1 = $reg1['B1'];
			
			$wattdiflperactivty = round((((($iand_p * 11.35)+320)/0.9)-320)/11.35);
	
                $aWATTDeflectionPoint = (($wattdiflperactivty  * $fdeflcoefs) / 100);   

                $hrmin = $aWATTDeflectionPoint * ($f_minhr / 100);
                $hrmax = $aWATTDeflectionPoint * ($f_maxhr / 100);
			//$data['minHRZone']=round($hrmin);
                $minHRZone = round(($B1 *(((($hrmin*11.35 + 320)/(0.9 * $weight)) - 4.25 ) / 2.98)) + $B0);
               $maxHRZone  = round(($B1 *(((($hrmax*11.35 + 320)/(0.9 * $weight)) - 4.25 ) / 2.98)) + $B0);
				$hr_zone_a = $minHRZone.'-'.$maxHRZone;
				$check = "select * from t_heartrate_zone_activity where r_user_test_id=".$r_user_test_id." AND r_user_id=".$user_id;
				$row4 = mysql_query($check);
				$check_record = mysql_fetch_array($row4);
				if(!empty($check_record)){
					$update="UPDATE t_heartrate_zone_activity set r_user_id=".$user_id.", fitness_level=".$fitness.",activity_id=".$activity_id.",activity_name='".$activity_name."',hr_zone_a=".$hr_zone_a.",points_rate=".$activity_point." where r_user_test_id=".$r_user_test_id;
					
					
					mysql_query($update);
				}
				else{
				$insert = "INSERT INTO t_heartrate_zone_activity (r_user_id, r_user_test_id, fitness_level,activity_id,activity_name,hr_zone_a,points_rate) VALUES ($user_id,$r_user_test_id,$fitness,$activity_id,'$activity_name','$hr_zone_a','$activity_point')";
				
				mysql_query($insert);
		      
				
				}	

			}
			
}	
    /*MK Added - Print / Email Test Analysis Report*/
    
    /**
    * Mail Analysis Report PDF
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function MailAnalysisReportPDF($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TEST_REPORTPRINT_BYTESTID, array($params['userTestId']));
        $testdata = array();
        if ($rsobj->RecordCount()) {
            $testdata = $rsobj->fields;
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Result got successfully',
            'data' => $testdata,
        );

        return $this->status;
    }
    /*MK Added - Get User Detail Record By UserID*/
    
    /**
    * get user detail by id
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getUserDetailByID($params)
    {
        $user_rs = $this->dbcon->Execute(GET_USERDETAIL_BYID, array($params['UserID']));
        if ($user_rs->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Found Successfully',
                'userDetail' => $user_rs->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'forbidden',
                'status_code' => 400,
                'status_message' => 'User detail not found',
            );
        }

        return $this->status;
    }
    /*SN added get client personal info*/
    
    /**
    * get client detail by id
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getClientDetailByID($params)
    {
        if (!isset($params['user_id'])) {
            return 'Invalid Data Send!';
        }
        $rsobj = $this->dbcon->Execute(GET_CLIENTINFORMATION_QUERY, array($params['user_id']));
        if ($rsobj->RecordCount()) {
            //$personalInfo = $rsobj->fields;
            $personalInfo = array();
            while (!$rsobj->EOF) {
                $personalInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'members' => $personalInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'members' => '',
            );
        }
	

        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * get Client Activity By ID
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getClientActivityByID($params)
    {
        if (!isset($params['user_id'])) {
            return 'Invalid Data Send!';
        }
        $rsobj = $this->dbcon->Execute(GET_CLIENTINFORMATION_ACTIVITY_QUERY, array($params['user_id']));
        if ($rsobj->RecordCount()) {
            //$personalInfo = $rsobj->fields;
            $personalInfo = array();
            while (!$rsobj->EOF) {
                $personalInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'members' => $personalInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'members' => '',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }
    
    /**
    * get week Train Program
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getweekTrainProgram($params)
    {
        $action = ($params['flag'] == 1) ? 1 : 0;
        if ($action) {
            $rsobj = $this->dbcon->Execute(GET_WEEK_PROGRAM, array($params['type'], $params['clubid']));
        } else {
            $rsobj = $this->dbcon->Execute(GET_WEEK_PROGRAM_PID, array($params['type'], $params['clubid']));
        }
        if ($rsobj->RecordCount()) {
            $devices    =   array();
            while (!$rsobj->EOF) {
                $devices[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Recived Week Tabel Details',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'devices' => $devices,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Week tabel details not Avilible',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'memberList' => '',
                'totalCount' => '0',
            );
        }

        return $this->status;
    }

    /**
    * save New Program Point
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    /*public function saveNewProgramPoint($params)
    {
        $rsobj = $this->dbcon->Execute(SAVE_NEW_PROGRAM_POINT, $params);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Save New Program Successfully',
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 400,
            'status_message' => 'Problem with Saving New Program',
            );
        }

        return $this->status;
    }
    */
    
    /**
    * To get test summary by member
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getTestSummaryByMember($params)
    {
        $summary = array();
        $summary['count'] = array('bc' => 0, 'flex' => 0, 'strength' => 0, 'cardio' => 0);
        $bc_rs = $this->dbcon->Execute(GET_TESTCOUNT_BC, $params['userId']);
        if ($bc_rs->RecordCount()) {
            $summary['count']['bc'] = $bc_rs->fields['cnt'];
        }
        $flex_rs = $this->dbcon->Execute(GET_TESTCOUNT_FLEX, $params['userId']);
        if ($flex_rs->RecordCount()) {
            $summary['count']['flex'] = $flex_rs->fields['cnt'];
        }
        $cardio_rs = $this->dbcon->Execute(GET_TESTCOUNT_CARDIO, $params['userId']);
        $analysed = 0;
        $tobeanalysed = 0;
        $printed = 0;
        if ($cardio_rs->RecordCount()) {
            $cardiolist = $cardio_rs->GetRows();
            $counts = 0;
            foreach ($cardiolist as $cardio) {
                $cnt = intval($cardio['cnt']);
                if ($cardio['is_analysed'] == 1) {
                    $analysed = $analysed + $cnt;
                } elseif ($cardio['is_analysed'] == 2) {
                    $printed = $printed + $cnt;
                } else {
                    $tobeanalysed = $tobeanalysed + $cnt;
                }
                $counts = $counts + $cnt;
            }
            $summary['count']['cardio'] = $counts;
        }
        $summary['analysed']['cardio'] = array('a' => $analysed, 'na' => $tobeanalysed, 'p' => $printed);
        $strn_rs = $this->dbcon->Execute(GET_TESTCOUNT_STRENGTH, $params['userId']);
        if ($strn_rs->RecordCount()) {
            $summary['count']['strength'] = $strn_rs->fields['cnt'];
        }
        /*Fitness & Improvements*/
        $summary['improvements'] = array();
        $latest_rs = $this->dbcon->Execute(GETUSERDATAVALUES_DESC, $params['userId']);
        $start_rs = $this->dbcon->Execute(GETUSERDATAVALUES_ASC, $params['userId']);
        $latestfat_rs = $this->dbcon->Execute(GET_USER_DATA_FAT_LEVEL_DESC, $params['userId']);
        $startfat_rs = $this->dbcon->Execute(GET_USER_DATA_FAT_LEVEL_ASC, $params['userId']);
        /*
        $getFatpercentLevelbydesc = $this->members->getFatpercentLevelbydesc($params);
        $getFatpercentLevelbyasc = $this->members->getFatpercentLevelbyasc($params);
        */
        if ($latest_rs->RecordCount()) {
            $rowdata = $latest_rs->fields;
            $fatper = array();
            if ($latestfat_rs->RecordCount()) {
                $fatpercent = $latestfat_rs->GetRows();
                foreach ($fatpercent as $fat) {
                    $fatper['fat_'.$fat['r_test_item_id']] = $fat['value'];
                }
            }
            $currentSummary = array('fl' => $rowdata['fitlevel'], 'wt' => $rowdata['weight']);
            $summary['improvements']['current'] = array_merge($currentSummary, $fatper);
            /*if($start_rs->RecordCount()){
                $rowdata    =    $start_rs->fields;
                $summary['improvements']['begining']    =    array('fl'=>$rowdata['fitlevel'],'wt'=>$rowdata['weight']);
            }*/
        }
        if ($start_rs->RecordCount()) {
            $rowdata = $start_rs->fields;
            $fatper =   array();
            if ($startfat_rs->RecordCount()) {
                $fatpercent = $startfat_rs->GetRows();
                $fatper = array();
                foreach ($fatpercent as $fat) {
                    $fatper['fat_'.$fat['r_test_item_id']] = $fat['value'];
                }
            }
            $beginingSummary = array('fl' => $rowdata['fitlevel'], 'wt' => $rowdata['weight']);
            $summary['improvements']['begining'] = array_merge($beginingSummary, $fatper);
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Counts retreived successfully',
            'summary' => $summary,
        );

        return $this->status;
    }
    
    /**
    * To add coach member
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return void
    */
    public function addCoachMember($params)
    {
        $rsobj = $this->dbcon->Execute(GET_COACH_MEMBER_BY_COACH_MEM, array(
                    $params['CoachId'],
                    $params['UserId'],
                    $params['GroupId'],
                    )
                );
        if ($rsobj->RecordCount()) {
            //$coachmember = $rsobj->fields;
            $data['r_coach_id'] = $params['CoachId'];
            $data['r_user_id'] = $params['UserId'];
            $data['r_group_id'] = $params['GroupId'];
            $query = $this->dbcon->GetUpdateSQL($rsobj, $data);
        } else {
            $data['r_coach_id'] = $params['CoachId'];
            $data['r_user_id'] = $params['UserId'];
            $data['r_group_id'] = $params['GroupId'];
            $query = $this->dbcon->GetInsertSQL($rsobj, $data);
        }
        $this->dbcon->Execute($query);
    }
    
    /**
    * To map club and members
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function mapClubAndMember($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_CLUBMEMBER, array($params['UserId'], $params['CompanyId'], $params['ClubID'])
        );
        if ($rsobj->RecordCount()) {
            $data['r_company_id'] = $params['CompanyId'];
            $data['r_user_id'] = $params['UserId'];
            $data['r_club_id'] = $params['ClubID'];
            $data['r_user_type_id'] = $params['UserTypeId'];
            $data['is_primary'] = $params['isPrimary'];
            $query = $this->dbcon->GetUpdateSQL($rsobj, $data);
        } else {
            $data['r_company_id'] = $params['CompanyId'];
            $data['r_user_id'] = $params['UserId'];
            $data['r_club_id'] = $params['ClubID'];
            $data['r_user_type_id'] = $params['UserTypeId'];
            $data['is_primary'] = $params['isPrimary'];
            $query = $this->dbcon->GetInsertSQL($rsobj, $data);
        }
        $this->dbcon->Execute($query);
        $this->status = array(
            'status_code' => 200,
            'status' => 'success',
            'status_message' => 'Updated Successfully',
        );

        return $this->status;
    }
    
    /**
    * To update mapped member device
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function updateMappedMemberDevice($params)
    {
        if (($params['deviceCode'] != '') and ($params['UserId'] != '')) {
            $rsobj = $this->dbcon->Execute(GET_DEVICE_BY_DEVICECODE, array($params['deviceCode']));
            //$deviceId = 0;
            if ($rsobj->RecordCount()) {
                $device = $rsobj->fields;
                $deviceId = $device['device_id'];
            } else {
                $data['r_device_type_id'] = 0;
                $companyid = REGISTER_CLIENT_COMPANY;
                if (isset($params['companyId']) and ($params['companyId']) != 0) {
                    $companyid = $params['companyId'];
                }
                $data['r_company_id'] = $companyid;
                $data['created_by'] = $params['UserId'];
                $data['created_date'] = date('Y-m-d H:i:s');
                $data['device_code'] = $params['deviceCode'];
                $data['r_club_id'] = $params['clubId'];
                $data['r_device_type_id'] = $params['deviceType'];
                $data['status'] = 0;//Not - deleted
                $insQuery = $this->dbcon->GetInsertSQL($rsobj, $data);
                $this->dbcon->Execute($insQuery);
                $deviceId = $this->dbcon->Insert_ID();
            }
            if ($deviceId != 0) {
                $this->dbcon->Execute(
                    UPDATE_EXIST_DEVICE_UNMAP, array($params['UserId'], date('Y-m-d H:i:s'), $deviceId)
                );
                $this->dbcon->Execute(
                    INSERT_DEVICE_MEMBER,
                    array(
                        $params['memberId'], $deviceId,
                        date('Y-m-d'), date('H:i:s'),
                        $params['UserId'], date('Y-m-d H:i:s')
                    )
                );
            }
            $this->status = array(
                'status_code' => 200,
                'status_message' => 'Updated successfully',
            );
        } else {
            $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to update device',
            );
        }

        return $this->status;
    }

    /**
    * To get fit level for ages sex and speed
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getfitlevelforagesexspeed($params)
    {
        $fitdata = array();
        $rsobj = $this->dbcon->Execute(
            GET_FITLEVEL_DATA_FOR_AGE_SEX_SPEED,
            array($params['age'], $params['age'], $params['gender'], $params['speed'], $params['speed'])
        );
        if ($rsobj->RecordCount()) {
            $fitdata['fitlevel'] = $rsobj->fields['f_fitlevel'];
            $fitdata['point_speed'] = $rsobj->fields['f_point_speed'];
            $fitdata['point_speeda'] = $rsobj->fields['f_point_speeda'];
        }

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'fitdata' => $fitdata,
        );

        return $this->status;
    }

    /**
    * To get 12 weeks training plan for age, sex and fit level
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function get12weekstraninigplanfor_age_sex_fitlevel($params)
    {
        $fitpointdata = array();
        $rsobj = $this->dbcon->Execute(
            GET_FITLEVEL_DATA_FOR_AGE_SEX_FITLEVEL,
            array($params['age'], $params['age'], $params['gender'], $params['fitlevel'])
        );
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $fitpointdata[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'fitpointdata' => $fitpointdata,
        );

        return $this->status;
    }

    /**
    * To get get points per activity for test if
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function getpointsper_activity_for_testid_iantw($params)
    {
        /**/
        $creditperactivity = array();
        //$usertestid = $params['userTestID'];
        $wattdiflperactivty = $params['wattdiflperactivty'];
        $testdataresult = $this->getCardioTestByTestID($params); //userTestID
        if (isset($testdataresult['testrecord'])) {
            $testdata = $testdataresult['testrecord'];
            $iant_p = $testdata['iant_p']; //8.5;//XX 
            $fitleveldataresult = $this->getfitlevelforagesexspeed(
                array('age' => $testdata['age_on_test'], 'gender' => $testdata['gender'], 'speed' => $iant_p)
            );
			
            if (isset($fitleveldataresult['fitdata'])) {
                $fitleveldata = $fitleveldataresult['fitdata'];
                $fitlevel = $fitleveldata['fitlevel'];//3;//XX 
               
                $rsobj = $this->dbcon->Execute(GET_POINTS_PER_ACTIVITY, array($fitlevel, $fitlevel));
                if ($rsobj->RecordCount()) {
                    while (!$rsobj->EOF) {
                        $fetchdata = $rsobj->fields;
                        $creditdata = array();

                        $pointspeed = $fitleveldata['point_speeda'];
                        $pcsp = $fetchdata['f_pcsp'.$fitlevel];
                        $DeflCoef = $fetchdata['f_deflcoef'.$fitlevel];
                        $minhrcond = $fetchdata['f_minhrcond'];
                        $maxhrcond = $fetchdata['f_maxhrcond'];

                        $activitypnt = (($pointspeed * $pcsp) / 100);
                        $aWATTDeflectionPoint = (($wattdiflperactivty * $DeflCoef) / 100);

                        $hrmin = $aWATTDeflectionPoint * ($minhrcond / 100);
                        $hrmax = $aWATTDeflectionPoint * ($maxhrcond / 100);

                        $creditdata['activityid'] = $fetchdata['f_activityid'];
                        $creditdata['activityname'] = $fetchdata['activity_name'];
                        $creditdata['activityimage'] = $fetchdata['activity_image'];
                        $creditdata['activity_type'] = $fetchdata['activity_type'];
                        $creditdata['activity_group_id'] = $fetchdata['r_activity_group_id'];
                        $creditdata['training_device_id'] = $fetchdata['r_training_device_id'];
                        $creditdata['hrzonereq'] = $fetchdata['f_hrzonereq'];
                        $creditdata['activityperiod'] = $fetchdata['f_period'];
                        $creditdata['activitypoint'] = $activitypnt;
                        $creditdata['hrzone'] = array('min' => $hrmin, 'max' => $hrmax);
                        $creditdata['Regr1'] = $testdata['reg_info1']; //further steps applied locally ;)
                        $creditdata['weight'] = $testdata['weight']; //XX 82;

                        $creditperactivity[] = $creditdata;
                        $rsobj->MoveNext();
                    }
                }
            }
        }
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'creditperactivity' => $creditperactivity,
        );

        return $this->status;
        /**/
    }

    /**
    * To post point achieved data
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    public function postpointachieveddata($params)
    {
        if (($params['userid'] != '')) {
           $this->dbcon->Execute(
                INSERT_TO_POINTS_ACHIVED,
                array(
                    $params['userid'], $params['trainingid'],
                    $params['trainingtype'], $params['phaseid'],
                    $params['groupid'], $params['status'],
                    $params['hrdata'], $params['creditdttm'],
                    $params['points'], $params['refid'], $params['reftypeid'], 0, date('Y-m-d')
                )
            );
           // print_r($aaa);
            //$pointachvId = $this->dbcon->Insert_ID();
            $this->status = array(
                'status_code' => 200,
                'status_message' => 'Updated successfully',
            );
        } else {
            $this->status = array(
                'status_code' => 404,
                'status_message' => 'Failed to update device',
            );
        }

        return $this->status;
    }

    
    /**
    * Get & Returns Reports With Achive points & Fit level By Activity
    *
    * @param array $params service parameter, Keys:userId,companyId(optional)
    *
    * @return array
    */
    public function getReportsActivity($params){
        /*User Fit-Level*/
        $rsfitlevel = $this->dbcon->Execute(
            GET_FITLEVEL_ID,array($params['userId'])
        );
        $fitlevels	=	array();
        $fitlevelscount =0;
        if($rsfitlevel->RecordCount()){
            $fitlevelscount=$rsfitlevel->RecordCount();
            $gfl=$rsfitlevel->GetRows();
            foreach($gfl as $fl){
                if(!isset($fitlevels['test'.$fl['testid']])) { 
                    $fitlevels['test'.$fl['testid']]    =   array();
                }
                $fitlevels['test'.$fl['testid']]['graphdata'][]= $fl;
                $fitlevels['test'.$fl['testid']]['count']
                    =count($fitlevels['test'.$fl['testid']]['graphdata']);
            }
        }
        $rs = $this->dbcon->Execute(
            GET_REPORTS_ACTIVITY,array($params['userId'])
        );
        //$rs = $this->db->Execute(GET_REPORTS_ACTIVITY,
        ////array($params['userID'],$params['gid']));
        $report=array();
        $report['activitytcount']=0;
        $report['activity']=array();
        $sum=array();
        $achivedpoints  =   array();
        if($rs->RecordCount()) {
            $report['activitycount']=$rs->RecordCount();
            $activityreport			=$rs->GetRows();
            foreach($activityreport	as $act){
                $report['activity']['act_'.$act['f_groupid']][]=$act;
                $report['activity']['countgroupact_'.$act['f_groupid']]
                    =count($report['activity']['act_'.$act['f_groupid']]);
                if(!isset($achivedpoints['train'.$act['f_trainingid']])){
                    $achivedpoints['train'.$act['f_trainingid']]    =   array();
                }
                $achivedpoints['train'.$act['f_trainingid']]['act_'.$act['f_groupid']][]=$act;
                $achivedpoints['train'.$act['f_trainingid']]['countgroupact_'.$act['f_groupid']]
                        =count($achivedpoints['train'.$act['f_trainingid']]['act_'.$act['f_groupid']]);
                $sum[]=$act['tpoints'];
                $achivedpoints['train'.$act['f_trainingid']]['pointstotal_group'.$act['f_groupid']]=array_sum($sum);
            }
            $report['fitlevelscount']=$fitlevelscount;
            $report['fitlevels']=$fitlevels;
            $report['activitytotalpoints']=array_sum($sum);
            /**/
            $rstestlist = $this->dbcon->Execute(
                GET_USERTEST_BYUSERID,array($params['userId'])
            );
            $result =   array();
            if($rstestlist->RecordCount()) {
                $user_test  =   $rstestlist->GetRows();
                foreach($user_test as $test) {
                    $tesid  =   $test['user_test_id'];
                    $data   =   array();
                    if(
                        isset($achivedpoints['train'.$test['f_cardiotrainingid']])
                    ) {
                        $data=$achivedpoints['train'.$test['f_cardiotrainingid']];
                        $data['test_id']=$tesid;
                        $data['fitlevel']=$test['fitness_level'];
                        $data['fitlevelcount']=$fitlevels['test'.$tesid]['count'];
                        $data['flgraphdata']=$fitlevels['test'.$tesid]['graphdata'];
                    }
                    $result['activitybytest'][] =   $data;
                    $result['activitybytestcount']    =   count($result['activitybytest']);
                }
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Report Details',
                'activitycount' => $rs->RecordCount(),           
                'report' => $result
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Report Details Not Found.',
                'report' => ''
            );
        }
        //Return the result array
        return $this->status;
    }
} // Class End.
;
