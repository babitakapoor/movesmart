<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Strength
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle strength related actions.
 */
 require_once '../service/config.php';
 require_once SQL_PATH.DS.'crypt-decrypt.php';
 require_once SQL_PATH.DS.'helper.php';
 /**
 * Class to handle strength related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class MachineModel
{
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
            'status' => 200,
            'status_message' => 'Success',
        );
        $this->error_general = array(
            'response' => array(
                'status' => 0,
                'status_message' => 'Some error occured. Please try later.',
            ),
        );
		
    }
    
	public function addMachine($params)
    {
		
		$clubid = $params['clubmachineid'];
		$machineId = $params['machineid'];
		//$machine_name = $params['machine_name'];
		
		$statement  =   "SELECT * FROM t_strength_machine_club WHERE `muniqueid`= ? limit 1";
		$machineobj = $this->dbcon->Execute($statement,array($clubid));
		if (!$machineobj->RecordCount()) 
		{
			$this->status = array(
					'status' => 102,
					'status_message' => 'Club not found.',
					);
        } 
		else 
		{
			$machineserialno = $machineobj->fields['mserial_no'];
			
			if ($machineserialno != '') 
			{
                if ($machineserialno == $machineId) { //Already Exist
                   $this->status = array(
						'status' => 101,
						'status_message' => 'Machine already exists.',
						);
                } 
				else 
				{
                    $this->status = array(
						'status' => 104,
						'status_message' => 'Unique id already used by another machine.',
						);
                }
            }
			else
			{
				$date = date('Y-m-d H:i:s');
				$data = array(
						'mserial_no'=>$machineId,
                        'modified_date'=>$date,
                );
				$rsUpdates = $this->dbcon->GetUpdateSQL($machineobj, $data);
				$this->dbcon->Execute($rsUpdates);
				$this->status = array(
							'status' => 100,
							'status_message' => 'Registered successfully.',
							//"sql" => $rsobj->sql 
						);
				
				
			}
		}
		return $this->status;
    }
	
	private function GetClubIdFrommUniqeId($uniqueid){
        $statement  =   "SELECT * FROM t_strength_machine_club WHERE muniqueid = ? limit 1";
		$rsobjClub = $this->dbcon->Execute($statement, array($uniqueid));
		
		if($rsobjClub->RecordCount()) {
			return $rsobjClub->fields['r_club_id'];
        }
        return 0;
    }
	
	public function getMachineList($params= array())
	{

		$uniqueid   = isset($params['clubmachineid']) ?$params['clubmachineid']:'';
        $clubid = $this->GetClubIdFrommUniqeId($uniqueid);
        $data   =   array();
		if($clubid > 0)
		{
			$statement  =   "SELECT 
							smc.strength_machine_club_id, sm.machine_name FROM t_strength_machine_club smc 
                            INNER JOIN  t_strength_machine sm 
                            on smc.r_strength_machine_id = sm.strength_machine_id  
                            WHERE r_club_id = ?";
			$rsobjClub = $this->dbcon->Execute($statement, array($clubid));
			if ($rsobjClub->RecordCount()) 
			{
				$i =0 ;
				while (!$rsobjClub->EOF) {
					$machinelist[$i]['id'] = base64_encode($rsobjClub->fields['strength_machine_club_id']);
					$machinelist[$i]['MachineDesc'] = $rsobjClub->fields['machine_name'];
					$rsobjClub->MoveNext();
					$i++;
				}
				$this->status = array(
					'status' => 100,
					'status_message' => 'Machine List',
					'total_records' => $rsobjClub->RecordCount(),
					// 'sql' => $rsobj->sql,
					'machinelist' => $machinelist,
				);
			}
			else
			{
				$this->status = array(
						'status' => 103,
						'status_message' => 'No Machine is present.',
						//"sql" => $rsobj->sql 
						);
			}
		}
		else
		{
			$this->status = array(
					'status' => 102,
					'status_message' => 'Club not exists.',
					);
		}
		return $this->status;
	}
	
	private function _GetClientTestKeys($code)
    {
		$code   =   Helper::DecryptPassword(trim($code),'shanetha12345678');
        return explode(";",$code);
    }
	
	public function setMachinePairing($params = array())
	{

		$clubid = $params['clubmachineid'];
		$deviceid = $params['machineid'];
		$clientid = $params['clientid'];
		list($clientid,$usertestid)   =   $this->_GetClientTestKeys($clientid);
		//$usertestid = $params['clientid'];
		
		if($deviceid!='') 
		{
			$statement = "SELECT * FROM t_strength_machine_club 
                            WHERE `muniqueid`= ? AND mserial_no = ? limit 1";
			$rsobjClient = $this->dbcon->Execute($statement, array($clubid,$deviceid));
			if ($rsobjClient->RecordCount()) 
			{
				$machineclubid = isset($rsobjClient->fields['strength_machine_club_id'])?$rsobjClient->fields['strength_machine_club_id']:0;
				if ($machineclubid > 0) 
				{
					$statement = "SELECT r_strength_machine_club_id FROM t_user_test 
									WHERE user_test_id = ?";
					$rsobjMachineClient = $this->dbcon->Execute($statement, array($usertestid));
					if($rsobjMachineClient->RecordCount())
					{
						if (isset($rsobjMachineClient->fields['r_strength_machine_club_id']))
						{
							if ($rsobjMachineClient->fields['r_strength_machine_club_id'] == $machineclubid) 
							{
								$this->status = array(
									'status' => 200,
									'status_message' => 'Already paired .',
								);
							} 
							else 
							{
								$date  = date('Y-m-d H:i:s');
								$data = array(
										'r_strength_machine_club_id'=>$machineclubid,
										'modified_date'=>$date,
								);
								$rsUpdates = $this->dbcon->GetUpdateSQL($rsobjMachineClient, $data);
								$this->dbcon->Execute($rsUpdates);
								$this->status = array(
									'status' => 0,
									'status_message' => 'Paired successfully.',
									//"sql" => $rsobj->sql 
									);
									
							}
						}
						else
						{
							$this->status = array(
								'status' => 202,
								'status_message' => 'No Active test for the given client.',
								);
						}
					}
					else
					{
						$this->status = array(
							'status' => 202,
							'status_message' => 'No Active test for the given client.',
							);
					}
				}
				else 
				{
					$this->status = array(
						'status' => 201,
						'status_message' => 'Club or machine not exists.',
						);
				}
				
			}
			else 
			{
				$this->status = array(
					'status' => 201,
					'status_message' => 'Club or machine not exists.',
					);
			}
		} 
		else 
		{
			$this->status = array(
					'status' => 201,
					'status_message' => 'Club or machine not exists.',
					);
		}
		
		return $this->status;
	}
	
	public function getClientReadyForMachine($params = array())
	{	
		//printLog($params);
		$uniqueid = isset($params['clubmachineid']) ? $params['clubmachineid'] : 0;
		$machinemode = isset($params['machinemode']) ? $params['machinemode'] : 0;
		$devicefound = isset($params['devicefound']) ? $params['devicefound'] : '';
		$clubid = $this->GetClubIdFrommUniqeId($uniqueid);
		$message    =   "Unknown error";
		if($clubid > 0) 
		{
			if($machinemode == 1)
			{
				$statement = "SELECT 
								utest.*,tprams.*, '' as device_code, tuser.user_id,
                                tuser.first_name,tuser.middle_name,tuser.last_name,tuserper.userimage,tuser.gender
                                FROM t_user_test utest
                                INNER JOIN t_users tuser ON ( tuser.user_id=utest.r_user_id  AND tuser.is_deleted<>1)
                                LEFT OUTER JOIN t_personalinfo tuserper ON ( tuserper.r_user_id=utest.r_user_id)
                                LEFT OUTER JOIN t_user_test_parameter tprams ON ( tprams.r_user_test_id=utest.user_test_id)
                                WHERE utest.r_club_id=? AND DATE_FORMAT(utest.test_start_date,'%Y-%m-%d')= ? and utest.status != ".TEST_STATUS_CARDIO_COMPLETE;
				$rsobjClient = $this->dbcon->Execute($statement, array($clubid,date("Y-m-d")));
			}
			else
			{
				$statement = "SELECT 
								tusrtest.*,tprams.*, de.device_code, urs.user_id, urs.first_name,urs.middle_name,
								urs.last_name,tuserper.userimage,urs.gender
								FROM t_users urs
								LEFT OUTER JOIN t_personalinfo tuserper ON tuserper.r_user_id=urs.user_id
								INNER JOIN t_device_member dem ON dem.r_user_id = urs.user_id
								INNER JOIN  t_device de ON de.device_id = dem.r_device_id
								LEFT OUTER JOIN t_user_test tusrtest 
								ON urs.user_id = tusrtest.r_user_id and tusrtest.is_analysed = 1
								LEFT OUTER JOIN t_user_test_parameter tprams 
								ON tprams.r_user_test_id=tusrtest.user_test_id		
								WHERE 
								dem.is_deleted != 1 and urs.is_deleted != 1 and
								de.r_device_type_id = 1 and de.device_code in (?)
								and urs.r_club_id = ?
								GROUP BY urs.user_id
								ORDER BY tusrtest.test_start_date desc ";
				$devicefoundin = "'".implode("','", explode(",",$devicefound))."'";
				$rsobjClient = $this->dbcon->Execute($statement, array($devicefoundin,$clubid));
			}
			
			if (!empty($rsobjClient) && $rsobjClient->RecordCount()) 
			{
				$i =0 ;
				$tclients = array();
				while (!$rsobjClient->EOF) { 
					$eachclient = array();
					$eachclient['clientId'] = Helper::EncryptPassword($rsobjClient->fields['user_id']. ";" . $rsobjClient->fields['user_test_id']);
					$eachclient['instructionType'] = $machinemode;
					$eachclient['name'] = $rsobjClient->fields['first_name'];
					$eachclient['test_level'] = $rsobjClient->fields['test_level'];
					if($rsobjClient->fields['userimage'] != '')
					{
						$usrimgfolderpath =  SERVICE_PROFILE_MEMBERURL. $rsobjClient->fields['userimage'];
						$imgP  =  Helper::ConvertImageToBase64($usrimgfolderpath,false);
						if ($imgP) 
						{
							$img  =  $imgP;
						}
						else
						{
							$img = '';
						}
					}
					else
					{
						$img = '';
					}
					$eachclient['img'] = $img;
					$eachclient['weight'] = $rsobjClient->fields['weight'];
					$eachclient['age'] = $rsobjClient->fields['age_on_test'];
					
					if ($machinemode == 1) 
					{
						$loadValue = trim($rsobjClient->fields['total_load_value'],'"');
						$eachclient['loadvalues'] = array();
						if ($loadValue != '') 
						{
							$arLoadValue ='';
							$valLoad= '';
							$arLoadValue = json_decode($loadValue,JSON_UNESCAPED_SLASHES);

							$valLoad =  $arLoadValue;
							
							if(!empty($valLoad))
							{
								foreach ($valLoad as $arl) 
								{
									array_push($eachclient['loadvalues'],
										array(
											"watt" => intval($arl['load']),
											"timeinsec" => intval($arl['time'])
										)
									);
								}
							}
						}
					}  
					else 
					{
						$eachclient['minHRZone']   =    120;
						$eachclient['maxHRZone']   =    150;
						$eachclient['creditPerMin']     =    1.5;
						$eachclient['availableCredits'] =    152.5;
					}
					
					$eachclient['mappeddevice'] = array();
					$mappeddevices = $this->_getClientDevices($rsobjClient->fields['user_id']);
					if($mappeddevices->RecordCount())
					{
						$mi = 0;
						while (!$mappeddevices->EOF) 
						{
							$eachclient['mappeddevice'][$mi]['id'] = $mappeddevices->fields['device_code'];
							$mappeddevices->MoveNext();
							$mi++;
						}
					}
					
					if ($machinemode == 1) 
					{
                        array_push($tclients, $eachclient);
					} 
					else 
					{
						$data   =   array();
                        if($rsobjClient->fields['device_code'] != '') 
						{
							$data[$client->device_code] = $eachclient;
                        }
                        array_push($tclients,$data);
					}
					$rsobjClient->MoveNext();
				}
				
				$this->status = array(
					'status' => 100,
					'status_message' => 'Machine List',
					'total_records' => $rsobjClient->RecordCount(),
					// 'sql' => $rsobj->sql,
					'clientlist' => $tclients,
				);
			}
			else
			{
				$this->status = array('status' => 502,'status_message' => 'No data found.',);
			}
		} 
		else 
		{
			$this->status = array(
					'status' => 502,
					'status_message' => 'No data found.',
					);
		}
		
		return $this->status;
	}
	private function _getClientDevices($clientid)
    {
		$statement  =   "SELECT dev.*, dm.device_member_id,dm.date as mappedon FROM 
						t_device_member dm  INNER JOIN t_device dev ON dev.device_id=dm.r_device_id
                        WHERE  dm.is_deleted<>1 AND  dm.r_user_id=? ";
		$rsobjClient = $this->dbcon->Execute($statement, array($clientid));
        return $rsobjClient;
    }
	
	public function sendTrainingData($params)
    {
		printLog($params);
		$clientcode = isset($params['clientid']) ? $params['clientid'] : 0;
        $trdata = isset($params['trdata']) ? $params['trdata'] : array();
        $client_test_arr    =   $this->_GetClientTestKeys($clientcode);
        if(count($client_test_arr)<2)
		{
        }
        list($clientid, $usertestid) =$client_test_arr;
        $trdatajsonobj = is_array($trdata) ? $trdata: json_decode($trdata,true);
        $statement = "SELECT * FROM t_user_test WHERE user_test_id = ?";
		$rsobjMachine = $this->dbcon->Execute($statement, array($usertestid));
		if ($rsobjMachine->RecordCount())
		{
            $mstatus = $this->_GetMachineStatusCodeByTest($rsobjMachine->fields['status']); // Machine Status
			printLog('from db');
			printLog($mstatus);
			printLog('from db///');
            $load_value =   array();
            $hrvalue = new stdClass();
            $rpmgroup = new stdClass();
            $cdhr = new stdClass();
            
			$rpm = isset($rsobjMachine->fields['rpm']) ? $rsobjMachine->fields['rpm'] : 0;
			$test_active    =   ($rsobjMachine->fields['test_active'] == 0) ? 1 : 0;
			if (isset($rsobjMachine->fields['heart_rate_value'])) {
				$hrvalue = ($rsobjMachine->fields['heart_rate_value'] != '') ? json_decode($rsobjMachine->fields['heart_rate_value']) : new stdClass();
			}
			if ( isset($rsobjMachine->fields['rpm_group']) and ($rsobjMachine->fields['rpm_group'] != '') ) {
				$rpmgroup =  json_decode($rsobjMachine->fields['rpm_group']);
			}
			if (isset($rsobjMachine->fields['load_value'])) {
				$load_value = ($rsobjMachine->fields['load_value'] != '') ? json_decode($rsobjMachine->fields['load_value'],true) : array();
			}
			$cool_start_at  =   0;
			if (isset($test['cool_down_start_time'])) {
				$cool_start_at  =   $rsobjMachine->fields['cool_down_start_time'];
			}
			$cool_stops_at  =   0;
			if (isset($rsobjMachine->fields['cool_down_stop_time'])) {
				$cool_stops_at  =   $rsobjMachine->fields['cool_down_stop_time'];
			}
			if (isset($rsobjMachine->fields['load_value'])) {
				$cdhr= (
					$rsobjMachine->fields['heart_rate_with_cool_down'] != ''
				) ? json_decode($rsobjMachine->fields['heart_rate_with_cool_down']) : new stdClass();
			}
			//$weight =   $test['weight']; //Get Speed is no need now so commented
			$lenlv =    count($load_value);
			$lasthrvalindex  =   ($lenlv>0) ? ($lenlv-1):0;
			$lastprevhrvalindex  =   ($lasthrvalindex>0) ? ($lasthrvalindex-1):0;
			$lasthrtime = 0;
			if(isset($hrvalue->$lasthrvalindex)) 
			{
				$lasthrarr = (array) $hrvalue->$lasthrvalindex;
				$len    =   count($lasthrarr)-1;
				if(isset($lasthrarr[$len]))
				{
					$lasthrtime=$lasthrarr[$len]->time;
				}
			}
			else if (isset($hrvalue->$lastprevhrvalindex))
			{
				$lasthrarr = (array) $hrvalue->$lastprevhrvalindex;
				$len    =   count($lasthrarr)-1;
				if(isset($lasthrarr[$len]))
				{
					$lasthrtime=$lasthrarr[$len]->time;
				}
			}
			//echo "<pre>";print_r($trdatajsonobj);
			$loadvalues =   $this->_GetLoadValuesFromTest($rsobjMachine->fields['total_load_value'],$trdatajsonobj);
			$hrrpm_data =   $this->_GetHrRpmValue(
				$loadvalues,$hrvalue,$rpmgroup,$cdhr,$trdatajsonobj,$lasthrtime,$rsobjMachine->fields['status']
			);
			if($hrrpm_data['rpm']!=0){
				$rpm=$hrrpm_data['rpm'];
			}
			if($hrrpm_data['tStatus'] == $this->_GetTestStatusCodeByMachine($trdatajsonobj[0]['status']))
			{
				$testStatus =   $hrrpm_data['tStatus'];
			}
			else
			{
				$testStatus =   $hrrpm_data['tStatus'];
			}
			//$testStatus =   $hrrpm_data['tStatus'];
			$cd_time    =   ($hrrpm_data['cdStTime']>0) ? $hrrpm_data['cdStTime']:$cool_start_at;
			$cd_endtime =   ($hrrpm_data['cdEndTime']>0) ? $hrrpm_data['cdEndTime']:$cool_stops_at;
			$rpmgroup   =   $hrrpm_data['rpmGroup'];
			$hrdata     =   $hrrpm_data['hrData'];
			$cdhrdata   =   $hrrpm_data['cdHrData'];
			$runTimer   =   $hrrpm_data['runTimer'];
           
            $load_values= '';
			if(!empty($loadvalues))
			{
				if($testStatus == 3)
				{
					$check = 0;
					foreach ($loadvalues as $ldVals) 
					{
						if($ldVals['time'] > "150") 
						{
							$check++;
						}
					}
					if($check >= 3)
					{
						$countLoad = count($loadvalues);
						$lastindex = $countLoad-1;
						$loadvalues[$lastindex]['loadtype'] = 3;
						$load_values = json_encode($loadvalues,JSON_NUMERIC_CHECK);
					}
					else
					{
						$load_values = json_encode($loadvalues,JSON_NUMERIC_CHECK);
					}
				}
				else
				{
					$load_values = json_encode($loadvalues,JSON_NUMERIC_CHECK);
				}
			}
			$heart_rate= '';
			//if(!empty($hrdata))
			if(!empty($hrdata) )
			{
				$heart_rate	= json_encode($hrdata,JSON_NUMERIC_CHECK);
			}
			/*else
			{
				if($trdatajsonobj[0]['time'] == 0)
				{
					$jk = 0;
					$values[0] = array('time' => 0,'data' => 0);
					$hrvalue->$jk = $values;
					$heart_rate	= json_encode($hrvalue,JSON_NUMERIC_CHECK);
				}
			}*/
			$rpm_group= '';
			//if(is_object($rpmgroup) && (count(get_object_vars($rpmgroup)) > 0))
			if(!empty($rpmgroup))
			{
				$rpm_group	= json_encode($rpmgroup,JSON_NUMERIC_CHECK);
			}
			/*else
			{
				if($trdatajsonobj[0]['time'] == 0)
				{
					$kj = 0;
					$valuesrjk = array('0'=>array('time' => 0,'data' => 0));
					$hrvalue->$kj = $valuesrjk;
					$rpm_group	= json_encode($hrvalue,JSON_NUMERIC_CHECK);
				}
			}*/
			$cdhr_group= '';
			if(!empty($cdhrdata))
			{
				$cdhr_group	= json_encode($cdhrdata,JSON_NUMERIC_CHECK);
			}
			
			$modifiedDate = date('Y-m-d H:i:s');
			$statement  =   "UPDATE t_user_test SET 
                            load_value='$load_values', 
                            heart_rate_value='$heart_rate', 
                            rpm='$rpm',
                            cool_down_start_time='$cd_time',
                            cool_down_stop_time='$cd_endtime',
                            rpm_group='$rpm_group',
                            heart_rate_with_cool_down='$cdhr_group',
							modified_date='$modifiedDate',
							current_test_time='$runTimer',
                            status=$testStatus                            
                          WHERE user_test_id=$usertestid";
			//echo $statement;die; 
			$checkLog = $this->dbcon->Execute($statement);
			if($testStatus == 3)
			{
				printLog($checkLog);
			}
			return array('status'=> (int)$mstatus);
        }
        return array(
            'status'=> 305,"status_message"=>"No Active Workout found"
        );
    }
	
	private  function _GetLoadValuesFromTest($total_load,$tr_data){
		
		$t_load =   ($total_load!='') ? json_decode($total_load,true):array();
		//echo "<pre>";print_r($t_load);
		$cloadvalue =   array();
        $len =   count($tr_data);
        if($len>0) 
		{
			$last_machine_load = $tr_data[$len - 1];
			foreach ($t_load as $lval) 
			{
				//echo $lval['time']." ".$last_machine_load['time']."<br/>";
				if ($lval['time'] > $last_machine_load['time']) 
				{
					array_push($cloadvalue, $lval);
					return $cloadvalue;
				}
				array_push($cloadvalue, $lval);
			}
		}
		
		return $cloadvalue;
    }
	
	private function _GetLoadValueIndexByTime($loadvalue,$time)
    {
        
		$index = '';
		$checkTime = (isset($loadvalue[1]))?$loadvalue[1]['time']:'';
		if($time <= 119)
		{
			$count = count($loadvalue);
			$inx =0;
			foreach($loadvalue as $idx => $lv)
			{	
				if($lv['time']>$time)
				{
					return $inx;
				}
				elseif($count == 2 && $time > 59)
				{
					$inx = 1;
				}
				else
				{
					$inx   =   $idx;
				}
			}
		}
		else
		{
			$retindex = 1;
			foreach($loadvalue as $idx => $lv)
			{
				if($lv['time']>$time)
				{
					return $retindex+1;
				}
				$retindex   =   $idx;
			}
		}
		//echo "index ".$retindex."<br/><br/>";
		return $retindex;
    }
	private function _GetHrRpmValue($loadvalue,$hrvalue,$rpmgroup,$cdhr,$trdata,$lasthrtime,$tStatus)
    {
		$tridx=$rpm=$mstatus=$cd_sttime=$cd_endtime=0;
		//$tstatus=
		$prevhrtime =   0;
		$run_time   =   0;
		foreach ($trdata as $tr) 
		{
			$cuhrtime   =   $tr['time'];
			if ($cuhrtime > $lasthrtime ) 
			{
				$ptridx =   ($tridx>0) ? $tridx-1:$tridx;
				$lasthr = $trdata[$ptridx]['heartrate'];
				$lastrpm = $trdata[$ptridx]['RPM'];
				$chr = $tr['heartrate'];
				$lasttime = $prevhrtime>0 ? $prevhrtime: $lasthrtime;
				for ($second=($lasttime+1); $second <=$cuhrtime; $second++) 
				{
					$cidx = $this->_GetLoadValueIndexByTime($loadvalue, $second);
					if($cidx!=-1) 
					{
						if (!isset($hrvalue->$cidx)) 
						{
							$hrvalue->$cidx = array();
						}
						if (!isset($rpmgroup->$cidx)) 
						{
							$rpmgroup->$cidx = array();
						}
						if (!isset($cdhr->$cidx)) 
						{
							$cdhr->$cidx = array();
						}
						$updtime = ($second == ($cuhrtime)) ? $cuhrtime : $second;
						$updhr = ($second == ($cuhrtime)) ? $chr : $lasthr;
						$updrpm = ($second == ($cuhrtime)) ? $tr['RPM'] : $lastrpm;
		
						$pushed = 0;
						if(!is_null($updhr)) 
						{
							$pushed = 1;
							array_push($hrvalue->$cidx,
								array(
									'time' => $updtime,
									'data' => $updhr
								)
							);
						}
						if($updrpm>0) 
						{
							array_push($rpmgroup->$cidx,
								array(
									'time' => $updtime,
									'data' => $updrpm
								)
							);
						}
						if ($tStatus == TEST_STATUS_COOLDOWN) 
						{
							array_push($cdhr->$cidx, $updhr);
						}
					}
				}
				$rpm = $tr['RPM'];
				$prevhrtime =   $cuhrtime;
				$tridx++;
			}
			if ($tStatus == TEST_STATUS_COOLDOWN) 
			{
				if($cd_sttime ==0) 
				{
					$cd_sttime = $cuhrtime;
				}
			}
			/*$is_test_completed   =   false;*/
			if ($tStatus == TEST_STATUS_CARDIO_COMPLETE) 
			{
				$cd_endtime = $cuhrtime;
			}
			$run_timer  =   $this->_getTimeInFormat($cuhrtime);
		}

		$returnResults =  	array(
								'hrData'            =>  $hrvalue,
								'rpmGroup'          =>  $rpmgroup,
								'rpm'               =>  $rpm,
								'tStatus'           =>  $tStatus,
								'mStatus'           =>  $mstatus,
								'cdStTime'          =>  $cd_sttime,
								'cdEndTime'         =>  $cd_endtime,
								'cdHrData'          =>  $cdhr,
								'runTimer'          =>  $run_timer,
								/*'isTestCompleted'   =>  $is_test_completed*/
							);
		//echo "<pre>";print_r($returnResults);
		return $returnResults;
		
    }
	private function _getTimeInFormat($raw_time)
	{
		//return gmdate($raw_time);
		$seconds    =   $raw_time%60;
		$total_minutes    =   floor($raw_time/60);
		$hours      =   floor($total_minutes/60);
		$minutes    =   ($total_minutes%60);
		$hours_str  =   (strlen($hours)>1 ? $hours:"0".$hours);
		$minutes_str  =   (strlen($minutes)>1 ? $minutes:"0".$minutes);
		$seconds_str  =   (strlen($seconds)>1 ? $seconds:"0".$seconds);
		return $hours_str. ":" . $minutes_str.":".$seconds_str;//Testing purpose . ":" . $raw_time;
	}
	
	private function _GetTestStatusCodeByMachine($machinestatus)
	{
		/*DB Test Status
		0 => Not Started/Stop,
		1 => started,
		2 => Cool Down,
		3 => Completed,
		10 => cycle is assigned for the Member,
		20=> Hrm is assigned for the Member ,
		30=> Interrupted,
		40=> Edit mode progress in coach app,50=> test reset from coach app
		*/
		//echo "test startedc ".TEST_STATUS_STARTED;die;
		$testStatus =   0;
		switch($machinestatus){
			case 300:
				$testStatus =   TEST_STATUS_STARTED;
				break;
			case 301:
				$testStatus =   TEST_STATUS_YETTO_START;
				break;
			case 302:
				$testStatus =   TEST_STATUS_COOLDOWN;
				break;
			case 303:
				$testStatus =   TEST_STATUS_CARDIO_COMPLETE;
				break;
			case 304:
				$testStatus =   TEST_STATUS_CANCELLED;
				break;
		}
		return $testStatus;
	}
	public function _GetCurrentWorkoutStatusForTestId($usertestid)
    {
		$statement  =   "SELECT * FROM t_user_test WHERE user_test_id=?";
        $rsobjClient = $this->dbcon->Execute($statement, array($usertestid));
		
        $status = 305;
		if($rsobjClient->RecordCount())
		{
			$status =   $this->_GetMachineStatusCodeByTest($rsobjClient->fields['status']);
		}
        
        return $status;
    }
    
	private function _GetMachineStatusCodeByTest($teststatus)
    {
        /*DB Test Status
        0 => Not Started/Stop,
        1 => started,
        2 => Cool Down,
        3 => Completed,
        10 => cycle is assigned for the Member,
        20=> Hrm is assigned for the Member ,
        30=> Interrupted,
        40=> Edit mode progress in coach app,50=> test reset from coach app
        */
        /*$machineStatus =   300;
        switch($teststatus){
            case 1:
                $machineStatus =   300;
                break;
            case 0:
                $machineStatus =   301;
                break;
            case 2:
                $machineStatus =   302;
                break;
            case 5:
                $machineStatus =   303;
                break;
            case 304:
                $machineStatus =   304;
                break;
        }*/
		$machineStatus =   300;
		switch($teststatus){
			case TEST_STATUS_STARTED:
				$machineStatus =   300;
				break;
			case TEST_STATUS_YETTO_START:
				$machineStatus =   301;
				break;
			case TEST_STATUS_COOLDOWN:
				$machineStatus =   302;
				break;
			case TEST_STATUS_CARDIO_COMPLETE:
				$machineStatus =   303;
				break;
			case TEST_STATUS_CANCELLED:
				$machineStatus =   304;
				break;
		}
        return $machineStatus;
    }
	
	
	
	public function registerClientHRDevice($params)
    {
		
        $clientcode   = isset($params['clientid']) ?$params['clientid']:'';
        $devicecode   = isset($params['deviceid']) ?$params['deviceid']:'';
        $uniqueid   = isset($params['clubmachineid']) ?$params['clubmachineid']:'';
        list($clientid,$usertestid)   =   $this->_GetClientTestKeys($clientcode);
		//$usertestid = $clientcode;
        $status=103;
		if($devicecode != '') 
		{
            $mappeddevices = $this->_getClientDevices($clientid);
			while (!$mappeddevices->EOF) 
			{
				if ($mappeddevices->fields['device_code'] == $devicecode){
                    $status = 201; //already mapped
                }
				$mappeddevices->MoveNext();
			}
			
			if ($status != 201)
			{
                $clubid =   $this->GetClubIdFrommUniqeId($uniqueid);
				
                $deviceid =$this->_GetDeviceIdByDeviceCode($devicecode,$clubid);
				
                if($deviceid > 0)
				{
                    $this->_UpdateDeviceClientPaired($deviceid,$clientid);
                    $status =   200;
                }
            }
        }
		$this->status = array(
					'status' => $status,
					'status_message' => 'status.',
					);
		return $this->status;
    }
	
	private function _GetDeviceIdByDeviceCode($devicecode,$clubid)
    {
		$statement  = "SELECT device_id FROM t_device WHERE device_code=?";
		$rsobjClient = $this->dbcon->Execute($statement, array($devicecode));
        $deviceid = isset($rsobjClient->fields['device_id'])?$rsobjClient->fields['device_id']:0;
        if(!$rsobjClient->RecordCount()) 
		{
			$date = date('Y-m-d H:i:s');
			$data = array(
                    'r_device_type_id' => 1,
                    'device_code' => $devicecode,
					'status' =>0,
                    'r_club_id' => $clubid,
                    'created_date' => $date,
            );
			$objStatment  = "SELECT * from t_device";
			$rsobjClient = $this->dbcon->Execute($objStatment);
			$rsInserts = $this->dbcon->GetInsertSQL($rsobjClient, $data);
			$this->dbcon->Execute($rsInserts);
			$Statment  = "SELECT * from t_device where device_code =? and r_club_id = ? and created_date = ?";
			$rsobjClient = $this->dbcon->Execute($Statment,array($devicecode,$clubid,$date));
            $deviceid = $rsobjClient->fields['device_id'];
        }
		
        return $deviceid;

    }
	
	 private function _UpdateDeviceClientPaired($deviceid,$clientid)
    {
        /*Update Un-Paired devices*/
		$statement = 'select * from t_device_member where r_device_id = ?';
		$memberobj = $this->dbcon->Execute($statement,array($deviceid));
        $data= array('is_deleted' => 1);
		$rsUpdates = $this->dbcon->GetUpdateSQL($memberobj, $data);
		$this->dbcon->Execute($rsUpdates);
		
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$cdate = date('Y-m-d H:i:s');
		$saveData = array('r_user_id'=>$clientid,'r_device_id'=>$deviceid,'date'=>$date,'time'=>$time,'modified_date'=>$cdate);
		$objStatment  = "SELECT * from t_device_member";
		$rsobjClient = $this->dbcon->Execute($objStatment);
		$sInserts = $this->dbcon->GetInsertSQL($rsobjClient, $saveData);
		//echo $sInserts;
		$this->dbcon->Execute($sInserts);
        
    }
	
	public function getCurrentTrainingStatus($params)
	{
		
		$clientcode   = isset($params['clientid']) ?$params['clientid']:'';
        list($clientid,$usertestid)   =   $this->_GetClientTestKeys($clientcode);
		//$usertestid = $clientcode;
        $status =   $this->_GetCurrentWorkoutStatusForTestId($usertestid);
        return array(
            "trainingstatusid"=>$status
        );
	}
	
	public function validateCoach($params)
	{
		
		$coachpin   =   $params['coachpin'];
        $uniqueid   =   $params['clubmachineid'];
		$machineid   =   $params['machineid'];
        $clubid =   $this->GetClubIdFrommUniqeId($uniqueid);
		$coachid    =   $this->_GetCoachIdByCoachPin($coachpin,$clubid);
        if($coachid!=0) {
            $token = $this->_CreateApiTokenByCoach($coachid, $clubid, $machineid);
			return array('status'=>200,'tk'=>$token);//valid accesss
        }
        return array('status'=>201,'tk'=>'');//in - valid accesss
	}
	
	public function _GetCoachIdByCoachPin($coachpin, $clubid)
    {
        $statement  =   "SELECT ec.r_user_id FROM t_employee_centre ec
                              LEFT OUTER JOIN t_users usr ON usr.user_id = ec.r_user_id
                              WHERE ec.r_club_id=? 
                              AND ec.is_deleted<>1 AND usr.uid = ?";
        $rsobjClient = $this->dbcon->Execute($statement, array($clubid,$coachpin));
		if(isset($rsobjClient->fields['r_user_id']) and $rsobjClient->fields['r_user_id']>0){
            return $rsobjClient->fields['r_user_id'];
        }
        return 0;

    }
	
	private function _CreateApiTokenByCoach($coachid,$clubid,$machineid)
    {
        $sperator   =   "__";
        $concatstr    =   $coachid .$sperator.$clubid.$sperator. $machineid . $sperator . time();
        $token =  Helper::EncryptPassword($concatstr);
		return $token;die;

    }
	
	public function searchClientByFirstName($params)
	{
		
		$uniqueid     =   isset($params['clubmachineid']) ?$params['clubmachineid']:'';
        $machineid  =   isset($params['machineid']) ?$params['machineid']:'';
        $token  =   isset($params['token']) ?$params['token']:'';
        $clubid =   $this->GetClubIdFrommUniqeId($uniqueid);
        $isValidToken   =   $this->_isValidToken($token);
        if($isValidToken) 
		{
            $searchtxt = isset($params['searchtxt']) ? $params['searchtxt'] : '';
            $statement = "SELECT * FROM t_users WHERE 
                                first_name LIKE ? 
                                AND r_usertype_id=? 
                                AND is_deleted<>1 AND r_club_id=? LIMIT 0,20";//To be restricted with coach
			$rsobjClient = $this->dbcon->Execute($statement, array($searchtxt.'%',3,$clubid));
            if($rsobjClient->RecordCount())
			{
				$i=0;
				$usrdata = array();
				while (!$rsobjClient->EOF) {
					$usrdata[$i]['clientId'] = Helper::EncryptPassword($rsobjClient->fields['user_id'].";-1");
					$usrdata[$i]['name'] = $rsobjClient->fields['first_name']." ".$rsobjClient->fields['last_name'];
					$rsobjClient->MoveNext();
					$i++;
				}
				$this->status =  array(
									'status' => 200,
									"message"=>"Result found success" ,
									'clients' => $usrdata
								);//in - valid accesss
			}
			else
			{
				$this->status =  array(
									'status' => 101,
									"message"=>"No Client Found" ,
									'clients' => $usrdata
								);//in - valid accesss
			}
        } 
		else 
		{
            $this->status =  array(
								'status' => 203,
								"message"=>"Token Expired",
								'clients' => array()
							);//in - invalid accesss
        }
		//echo"<pre>";print_r($this->status);
		return $this->status;
	}
	
	private function _isValidToken($token){
		
        $sperator   =   "__";
        $tk=Helper::DecryptPassword($token);
		
        $explodes=explode($sperator, $tk);
		
        if(isset($explodes[3]))
		{
            $validcoach =   $explodes[0];//coachid position 0
            $coach  =   $this->_GetCoachByCoachId($validcoach);
			
			if($coach->RecordCount())
			{
                $validtime  =   $explodes[3];//time position 3
				$expire_dur =(time()-$validtime);
				
                if($expire_dur<=60  * 60)
				{
                    //Expiry Minutes converted to seconds
                    return true;
                }
            }
        }
        return false;
    }
	
	public function _GetCoachByCoachId($coachid)
    {
        //b7bd302f641cb2bdeaf91e9530b46019
		$statement  =   "SELECT * FROM t_users WHERE user_id=? AND is_deleted<>1";
        $rsobjClient = $this->dbcon->Execute($statement, array($coachid));
        return $rsobjClient;
    }
	
	public function startTraining($params)
	{
		/*Request: Required Fields - validate*/
		if(!isset($params['clientid'])){
			//return $this->InvalidRequest(404);
		}
		/*Request: Required Fields - validate -Ends */
		
		$clientcode   =  $params['clientid'];
		$client_test_arr    =   $this->_GetClientTestKeys($clientcode);
		if(count($client_test_arr)<2){
			//return $this->InvalidRequest(404);
		}
		list($clientid, $usertestid) =$client_test_arr;
		$machine_club  =   $this->_GetStrengthMachineByUniqueIdAndSerial($params['clubmachineid'],$params['machineid']);
		
		$activity_id    =   0;
		if($machine_club->RecordCount())
		{
			$activity_id =  $machine_club->fields['activity_id'];
		}
		$cardio_test = $this->_GetAvailableTraining($usertestid);
		$status =   401;
		$status_message     =   "No Active training";
		$training_detail_id =   "";
		$current_week       =   1;

		$week_goal=$age=$weight=$minHRZone=$maxHRZone=$creditPerMin=0;
		if($cardio_test)
		{
			$training_id    =   $cardio_test->fields['f_cardiotrainingid'];
			$current_week   =   $this->_GetCurrentWeekDate($cardio_test['f_trainingstdate'],$cardio_test['f_trainingeddate']);
			$week_plan  =   $this->_GetCurrentWeekPlan($training_id,$current_week);
			$plan_id    =   0;
			if(isset($week_plan->fields['f_points'])){
				$week_goal=$week_plan->fields['f_points'];
			}
			if(isset($week_plan->fields['f_cardiotrainingplanid'])){
				$plan_id= $week_plan->fields['f_cardiotrainingplanid'];

			}
			
			$point_id   =   $this->_UpdatePointsAchieved(
				array(
					'userId'        =>  $clientid,
					'trainingId'    =>  $training_id,
					'trainingType'  =>  ACHIEVE_TYPE_TRAINING,
					'creditStatus'  =>  0,
					'refid'         =>  $plan_id,
					'reftype'       =>  ACHIEVE_REFERENCE_TRAINING_PLAN,
					'credits'       =>  0,
					'activityId'    =>  $activity_id
				)
			);
			$trainings_of_week  =   $this->_GetTrainingCountByWeek($plan_id);
			$points_of_week     =   $this->_GetTrainingPointsByWeek($plan_id);
			$client             =   $this->_GetTestByTestUserID($usertestid,$clientid);
			if(count($client)>0) {
				$age = $client->fields['age_on_test'];
				$weight = $client->fields['weight_on_test'];
				$hrZoneData = $this->_GetActivityHrZone($client,$activity_id);
				if(isset($hrZoneData['minHRZone'])){
					$minHRZone  =   $hrZoneData['minHRZone'];
				}
				if(isset($hrZoneData['maxHRZone'])){
					$maxHRZone  =   $hrZoneData['maxHRZone'];
				}
				if(isset($hrZoneData['creditPerMin'])){
					$creditPerMin   =   $hrZoneData['creditPerMin'];
				}
				if(isset($hrZoneData['availableCredits'])){
					$total_credits  =   $hrZoneData['availableCredits'];
				}
			}
			if($point_id!=0){
				$training_detail_id    =    Helper::EncryptPassword(
					$training_id. ";" . $point_id
				);
				$status =   200;
				$status_message =   "Active training is available";
			}
		}
		return array(
			"status"            =>  $status,
			"message"           =>  $status_message,
			"trainingdetid"     =>  @$training_detail_id,
			"weekno"            =>  @$trainings_of_week,
			"thisweekno"        =>  $current_week,
			"totalcredits"      =>  @$total_credits,
			"thisweekgoal"      =>  $week_goal,
			"achivedthisweek"   =>  @$points_of_week,
			"weight"            =>  $weight,
			"age"               =>  $age,
			"minHRZone"         =>  $minHRZone,
			"maxHRZone"         =>  $maxHRZone,
			"creditPerMin"      =>  $creditPerMin
		);
	}
	
	 private function _GetStrengthMachineByUniqueIdAndSerial($clubid,$machine_serial)
	{
		
		$statement  =   "SELECT mclub.*,machine.activity_id,machine.machine_name 
						FROM t_strength_machine_map_club mclub 
						  LEFT JOIN t_strength_machine machine 
							ON machine.strength_machine_id=mclub.r_strength_machine_id
						WHERE f_uniqueid=? AND f_serialno=?";
		
		$rsobjClient = $this->dbcon->Execute($statement, array($clubid,$machine_serial));
		
        return $rsobjClient;
	}
	
	private function _GetAvailableTraining($userTestId)
	{
		$cur_date   =   date('Y-m-d H:i:s');
		//$cur_date   =   date('Y-m-d 00:00:00');
		//$end_date   =   date('Y-m-d 23:59:59');
		$statement  = "SELECT * FROM t_cardiotraining ct
						WHERE ct.f_testid=? 
							AND f_trainingstdate<=? 
							AND f_trainingeddate>=? 
							AND f_isdelete<>1";
		$rsobjClient = $this->dbcon->Execute($statement, array($userTestId,$cur_date,$cur_date));
		if($rsobjClient->RecordCount())
		{
			return $rsobjClient;
		}
		
		return array();
	}
	
	private function _GetCurrentWeekPlan($training_id,$week)
	{
		$statement  =   "SELECT * FROM t_cardiotrainingplan 
						  WHERE f_cardiotrainingid=? AND f_weeknr=?";
		$rsobjClient = $this->dbcon->Execute($statement, array($training_id,$week));
		if($rsobjClient->RecordCount()){
			return $rsobjClient;
		}
		return array();
	}
	private function _GetCurrentWeekDate($start_date,$end_date)
	{
		$start = strtotime($start_date);
		$end = strtotime($end_date);
		$current_date   =   date('Y-m-d H:i:s');
		$current_week   =   1;
		for ($i = $start,$week=1;$i < $end;$i=strtotime('+7 day', $i),$week++) {
			if(strtotime($current_date)>$i){
				$current_week   =   $week;
			}
		}
		return ($current_week>12) ? 12 : $current_week;
	}
	
	private function _GetTrainingCountByWeek($training_plan_id)
	{
		if($training_plan_id!=0) 
		{
			$statement = "SELECT count(*) as training_count 
								FROM t_training_points_achieved 
							 WHERE f_refid=? 
								AND f_reftypeid=?
								AND f_isdelete<>'1'";
			$rsobjClient = $this->dbcon->Execute($statement, array($training_plan_id,3));
			if (isset($rsobjClient->fields['training_count'])) 
			{
				return $rsobjClient->fields['training_count'];
			}
		}
		return 0;
	}
	/*
	 * Access:private
	 * @param mixed $training_plan_id
	 * @return mixed $count*/
	private function _GetTrainingPointsByWeek($training_plan_id)
	{
		if($training_plan_id!=0) {
			$statement = "SELECT SUM(f_points) as achieved_points 
								FROM t_training_points_achieved 
							 WHERE f_refid=?
								AND f_reftypeid=?
								AND f_isdelete<>'1'";
			$rsobjClient = $this->dbcon->Execute($statement, array($training_plan_id,3));
			if (isset($rsobjClient->fields['achieved_points'])) {
				return $rsobjClient->fields['achieved_points'];
			}
		}
		return 0;
	}
	
	private function _GetTestByTestUserID($user_test_id,$user_id)
	{
		$statement = "SELECT t.*,tp.*, 
							urs.user_id, 
							urs.first_name,
							urs.middle_name,
							urs.last_name,
							tuserper.userimage,
							urs.gender,
							fl.f_point_speed AS point_speed,
							fl.f_point_speeda AS point_speeda
					FROM t_users urs
						LEFT OUTER JOIN t_personalinfo tuserper ON tuserper.r_user_id=urs.user_id
						INNER JOIN t_device_member dem ON dem.r_user_id = urs.user_id                                        
						LEFT OUTER JOIN t_user_test t  ON urs.user_id = t.r_user_id and t.is_analysed = 1
						LEFT OUTER JOIN t_user_test_parameter tp ON tp.r_user_test_id=t.user_test_id
						LEFT OUTER JOIN t_fitlevel fl ON ( 
							fl.f_agemin<=tp.age_on_test  
							AND fl.f_agemax >=tp.age_on_test  
							AND fl.f_gender=urs.gender
							AND f_speed_min<=tp.iant_p 
							AND f_speed_max >=tp.iant_p
						)
					WHERE 
						tp.r_user_test_id=? 
						AND tp.r_user_id=? 
						AND tp.`status`<>'1' ORDER BY tp.user_test_parameter_id DESC LIMIT 0,1";
		$rsobjClient = $this->dbcon->Execute($statement, array($user_test_id,$user_id));
		return ($rsobjClient->RecordCount()) ? $rsobjClient:array();

	}
	
	private function _GetActivityHrZone($client,$activity_id=-1)
	{
		$age    =   $client->fields['age_on_test'];
		$gender    =   $client->fields['gender'];
		$iant_p    =   $client->fields['iant_p'];
		$hrzone =   array();
		$hrzone['minHRZone']   =    0;
		$hrzone['maxHRZone']   =    0;
		$hrzone['creditPerMin']     =    0;
		$hrzone['availableCredits'] =    0;
		$wattdiflperactivty =   $this->_CalculateWATTDeflectionPointForActivity($iant_p);
		$flevel =$client->fields['fitness_level'];
		/*Get Activity Points*/
		$statement = "SELECT 
						  actpoint.*,
						  act.activity_id,
						  act.activity_name,
						  act.activity_image,
						  act.activity_type,
						  act.r_training_device_id, 
						  act.r_activity_group_id,
						  act.f_uiorder,
						  act.f_hrzonereq
					  FROM t_activity act
						LEFT OUTER JOIN t_activitypoints actpoint
							ON actpoint.f_activityid = act.activity_id
					  WHERE actpoint.f_min_level<=? 
							AND actpoint.f_max_level>=?
							AND act.r_activity_group_id > 0 ( ? !=-1 ?  AND act.activity_id=?:'' )
					  ORDER BY act.r_activity_group_id, act.f_uiorder";
		
		$rsobjClient = $this->dbcon->Execute($statement, array($flevel,$activity_id));
		if($rsobjClient->RecordCount()) 
		{
			$achieve_credits  =   $this->_GetAchievePoints($client->fields['r_user_id']);
			while (!$rsobjClub->EOF)
			{
				$pointspeed = $client->fields['point_speeda'];
				$pcsp = $rsobjClub->fields['f_pcsp'.$flevel];
				$DeflCoef = $rsobjClub->fields['f_deflcoef'.$flevel];
				$minhrcond = $rsobjClub->fields['f_minhrcond'];
				$maxhrcond = $rsobjClub->fields['f_maxhrcond'];

				$activitypnt = (($pointspeed * $pcsp) / 100);
				$aWATTDeflectionPoint = (($wattdiflperactivty * $DeflCoef) / 100);

				$hrmin = $aWATTDeflectionPoint * ($minhrcond / 100);
				$hrmax = $aWATTDeflectionPoint * ($maxhrcond / 100);
				$weight    =   $client->weight;

				$reg1obj = ($client->fields['reg_info1'] !='') ? json_decode($client->fields['reg_info1'],true):array();

				$B0 = (isset($reg1obj['B0']))?$reg1obj['B0']:0; //XX 80.6;//
				$B1 = (isset($reg1obj['B1']))?$reg1obj['B1']:0; //XX 7.6;//
				$minHRZone  = round(($B1 *(((($hrmin*11.35 + 320)/(0.9 * $weight)) - 4.25 ) / 2.98)) + $B0);
				$maxHRZone  = round(($B1 *(((($hrmax*11.35 + 320)/(0.9 * $weight)) - 4.25 ) / 2.98)) + $B0);

				if (($minHRZone + $maxHRZone) >= 10){
					if (($maxHRZone - $minHRZone ) < 10) {
						$minHRZone = $maxHRZone - 10; //Minimum 10 point should be differed
					}
				}
				$period =   isset($rsobjClub->fields['f_period']) ? $rsobjClub->fields['f_period']:10;
				$hrzone['minHRZone']        =   $minHRZone;
				$hrzone['maxHRZone']        =   $maxHRZone;
				$hrzone['creditPerMin']     =   ($achieve_credits/$period);
				$rsobjClub->MoveNext();
				$i++;
			}
			$hrzone['availableCredits'] =   $achieve_credits;
		}
		return $hrzone;
	}
	
	private function _GetAchievePoints($user_id)
	{
		$statement  =   "SELECT SUM(f_points) AS achieve_points 
						FROM t_training_points_achieved 
						WHERE f_userid= ?
							AND f_status= ?
							AND f_trainingtype= ?
							AND f_isdelete<>1";

		$rsobjClient = $this->dbcon->Execute($statement, array($user_id,0,1));
		if($rsobjClient->RecordCount()) 
		{
			return $rsobjClient->fields['achieve_points'];
		}
		return 0;
	}
	
	private function _CalculateWATTDeflectionPointForActivity($iant_p)
	{
		return round((((($iant_p * 11.35)+320)/0.9)-320)/11.35);
	}
	
	public function updateTrainingCredits($params)
	{
		
		/*Request: Required Fields - validate*/
            if(!isset($params['clientid'])){
                //return $this->InvalidRequest(404);
            }
            /*Request: Required Fields - validate -Ends */
            $clientcode   =  $params['clientid'];
            $client_test_arr    =   $this->_GetClientTestKeys($clientcode);
            if(count($client_test_arr)<2)
			{
                //return $this->InvalidRequest(404);
            }
            list($clientid, $usertestid) =$client_test_arr;
			
            $client   =   $this->_GetTestByTestUserID($usertestid,$clientid);
            $training_code   =  $params['trainingdetid'];
            $training_detail    =   $this->_GetClientTestKeys($training_code);
            if(count($training_detail)<2)
			{
               // return $this->InvalidRequest(404);
            }
            list($training_id, $point_id) =$training_detail;
            /*Request: Required Fields - validate -Ends */

            /*Get Achieve Points By Point ID*/
            //$point_id
            $achieve_point =   $this->_GetPointsAchievedByID($point_id);
            /*Achieve Points By Point ID*/
            if(isset($params['trdata']) and $params['trdata']!="") 
			{
                $trdata = json_decode($params['trdata'], true);
                $tr_data = is_array($trdata) ? $trdata : json_decode($trdata, true);
                //$load_values = $this->_GetLoadValuesFromTest($client['total_load_value'], $tr_data);
                $hr_value = array();
                $rpm_value = array();
                $last_hr_time = 0;
                if (isset($achieve_point->fields['f_hrdata']) and ($achieve_point->fields['f_hrdata'] != '')) {
                    $rpm_hr_value = json_decode($achieve_point['f_hrdata'],true);
                    if(isset($rpm_hr_value['hr'])) {
                        $hr_value = $rpm_hr_value['hr'];
                        $len    =   count($hr_value)-1;
                        if(isset($hr_value[$len])){
                            $hr_len =   count($hr_value[$len])-1;
                            if(isset($hr_value[$len][$hr_len]['time'])) {
                                $last_hr_time = $hr_value[$len][$hr_len]['time'];
                            }
                        }
                    }
                    if(isset($rpm_hr_value['rpm']))
                        $rpm_value    =   $rpm_hr_value['rpm'];

                }
                $hr_data    =   $this->_GetHrRpmTrainingValue($hr_value,$rpm_value,$tr_data,$last_hr_time);
            }
            $credits    =   isset($params['credits']) ? $params['credits']:0;
            if(isset($achieve_point['f_points'])){
                $credits    =   $credits+$achieve_point['f_points'];
            }
            $this->_UpdatePointsAchieved(array(
                'hr_rpm'        =>  json_encode($hr_data),
                'credits'       =>  $credits,
                'creditedOn'    =>  date('Y-m-d H:i:s')
            ),$point_id);
            return array("status" => 200);
	}
	private function _UpdatePointsAchieved($params=array(),$point_id=0)
	{
		$data   =   array();
		if(isset($params['trainingId'])) {
			$data['f_trainingid'] = $params['trainingId'];
		}
		if(isset($params['userId'])){
			$data['f_userid']    =   $params['userId'];
		}
		if(isset($params['trainingType'])){
			$data['f_trainingtype']    =   $params['trainingType'];
		}
		if(isset($params['hr_rpm'])){
			$data['f_hrdata']    =   $params['hr_rpm'];
		}
		if(isset($params['credits'])){
			$data['f_points']    =   $params['credits'];
		}
		if(isset($params['refid'])){
			$data['f_refid']    =   $params['refid'];
		}
		if(isset($params['reftype'])){
			$data['f_reftypeid']    =   $params['reftype'];
		}
		if(isset($params['creditStatus'])) {
			$data['f_status'] = $params['creditStatus'];
		}
		if(isset($params['activityId'])) {
			$data['f_activityid'] = $params['activityId'];
		}
		if(isset($params['creditedOn'])) {
			$data['f_creditdttm'] = $params['creditedOn'];
		}
		if($point_id!=0)
		{
			$statement  =   "SELECT * FROM t_training_points_achieved WHERE `f_pointachievedid`= ? limit 1";
			$machineobj = $this->dbcon->Execute($statement,array($point_id));
			$data['f_moddttm']  =   date('Y-m-d H:i:s');
			$rsUpdates = $this->dbcon->GetUpdateSQL($machineobj, $data);
			$this->dbcon->Execute($rsInserts);
			$getstatement  =   "SELECT * FROM t_training_points_achieved order by id desc limit 1";
			$getobj = $this->dbcon->Execute($getstatement);
		} else {
			$data['f_crdttm']  =   date('Y-m-d H:i:s');
			$statement  =   "SELECT * FROM t_training_points_achieved";
			$machineobj = $this->dbcon->Execute($statement);
			$rsInserts = $this->dbcon->GetInsertSQL($machineobj, $data);
            $this->dbcon->Execute($rsInserts);
			$getstatement  =   "SELECT * FROM t_training_points_achieved order by id desc limit 1";
			$getobj = $this->dbcon->Execute($getstatement);
		}
		if($getobj){
			if($getobj->RecordCount())
			{
				return $getobj->fields['f_pointachievedid'];
			}
			else
			{
				return 0;
			}
		}
		return 0;
	}
	private function _GetHrRpmTrainingValue($hr,$rpm_group,$tr_data,$last_hr_time)
	{
		$tridx=$prev_hr_time=$rpm=0;
		foreach($tr_data as $tr) {
			$cur_hr_time = $tr['time'];
			/*$hr_index = $this->_GetHrIndexByTime($hr, $last_hr_time);
			if (!isset($hr[$hr_index])) {
				$hr[$hr_index]  =   array();
			}*/
			/*if (!isset($rpm_group[$hr_index])) {
				$rpm_group[$hr_index]  =   array();
			}*/
			if($cur_hr_time>$last_hr_time) {
				//$ptridx =   ($tridx>0) ? $tridx-1:$tridx;
				//$lasthr =   $tr_data[$ptridx]['heartrate'];
				$chr = $tr['heartrate'];
				$lasttime = $prev_hr_time>0 ? $prev_hr_time: $last_hr_time;
				/*for ($second=$lasttime; $second < $cur_hr_time; $second++) {
					$updtime = ($second == ($cur_hr_time - 1)) ? $cur_hr_time : $second;
					$updhr = ($second == ($cur_hr_time - 1)) ? $chr : $lasthr;
					$updrpm = ($second == ($cur_hr_time - 1)) ? $tr['RPM'] : $rpm;
					array_push($hr,[
						'hr'        =>  $updhr,
						'timediff'  =>  $updtime
					]);
					array_push($rpm_group[$hr_index],
						array(
							'time' => $updtime,
							'data' => $updrpm
						)
					);
				}*/
				array_push($hr,[
					'hr'        =>  $chr,
					'timediff'  =>  ($cur_hr_time-$lasttime)
				]);
				$prev_hr_time =   $cur_hr_time;
				$tridx++;
			}
		}/**/
		return $hr;
		/*return array(
			'hr'=>$hr,
			'rpm'=>$rpm_group,
		);*/
	}
	private function _GetPointsAchievedByID($point_id)
	{
		$statement  =   "SELECT *
							FROM t_training_points_achieved 
							WHERE f_pointachievedid=? AND f_isdelete<>1";
		$rsobjClient = $this->dbcon->Execute($statement, array($point_id));
		if($rsobjClient->RecordCount())
		{
			return $rsobjClien;
		}
		return array();
	}

	public function updateTrainingStatus($params)
	{
		/*Request: Required Fields - validate*/
		if( (!isset($params['clientid'])) or (!isset($params['status']))){
			//return $this->InvalidRequest(404);
		}
		/*Request: Required Fields - validate -Ends */
		$clientcode   =  $params['clientid'];
		$client_test_arr    =   $this->_GetClientTestKeys($clientcode);
		if(count($client_test_arr)<2){
			//return $this->InvalidRequest(404);
		}
		$status =   $params['status'];
		list($client_id, $user_test_id)=$client_test_arr;
		$test_status    =   $this->_GetTestStatusCodeByMachine($status);
		$is_test_completed   =   0;
		$is_testactive   =   1;
		
		if($test_status==TEST_STATUS_CARDIO_COMPLETE){
			$is_test_completed= 1;
			$is_testactive= 0;
		}
		$statement  =   "UPDATE t_user_test SET 
                            status=$test_status,
							test_active = $is_testactive,
							is_test_completed = $is_test_completed                       
							WHERE user_test_id=$user_test_id";
				
		if($this->dbcon->Execute($statement))
		{
			return array(
				'status'=>$status,
				'message'=>'Status updated successfully'
			);
		}
		return array(
			'status'=>404,
			'message'=>'Test is not found or invalid test ID'
		);
	}
		
} //Class End.
;
