<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Booking
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle booking related actions.
 */
require_once SQL_PATH.DS.'booking.php';
/** 
 * Class to handle Booking Tests related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class bookingModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param array $dbcon connection arguments  
     */
    public function __construct($dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 1,
            'status_message' => 'Opps an error as occurred',
        );
    }

    /**
    * Returns an array obj of slot types
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getSlotType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_SLOT_TYPES);

        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Get SlotType  failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Get SlotType success',
                'booking' => $rsobj->GetRows(),
            );
        }

        return $this->status;
    }

   
    /**
    * Get all Booking for the User
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function getAllBooking($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_UESR_BOOKING, array($params['userId'])
        );
        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Getting Users bookings failed',
        );
        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Getting Users bookings  success',
                'booking' => $rsobj->GetRows(),
            );
        }

        return $this->status;
    }


    /**
    * To insert or Update the Row IN bookings table
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function updateBooking($params)
    {
        $bookingId = isset($params['bookingId']) ? $params['bookingId'] : null;
        $data = array(
            'booking_id' => $bookingId,
            'r_user_id' => isset($params['userId']) ? $params['userId'] : '',
            'r_club_id' => isset($params['clubId']) ? $params['clubId'] : '',
            'r_company_id' => isset($params['companyId']) ?
            $params['companyId'] : '',
            'r_slot_id' => isset($params['slotId']) ? $params['slotId'] : '',
            'start_date' => isset($params['start_date']) ?
            $params['start_date'] : '',
            'start_time' => isset($params['start_time']) ?
            $params['start_time'] : '',
            'end_time' => isset($params['end_time']) ? $params['end_time'] : '',
            'calendar_booking_id' => isset($params['calendar_booking_id']) ?
            $params['calendar_booking_id'] : '',
            'booking_status' => 0,
            'is_canceled' => 0,
        );
        $rsobj = $this->dbcon->Execute(GET_BOOKING_DETAILS, array($bookingId));
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Users successfully Updated.',
                'booking_id' => $bookingId,
            );
        } else {
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $bookingsId = $this->dbcon->Insert_ID();

            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'new Booking Created Successfully',
                'booking_id' => $bookingsId,
                // 'sql' => $rsobj->sql,
            );
        }

        return $status;
    }

         
    /**
    * To cancel the booking
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function cancelBooking($params)
    {
        $bookingId = isset($params['bookingId']) ?
            $params['bookingId'] : null;
        $data = array(
            'is_canceled' => isset($params['is_canceled']) ?
            $params['is_canceled'] : '',
        );
        $rsobj = $this->dbcon->Execute(
            GET_BOOKING_DETAILS, array($bookingId)
        );
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Booking Canceled Successfully',
                'booking_id' => $bookingId,
            );
        } else {
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => ' Booking Is Not available',
                'booking_id' => $bookingId,
                // 'sql' => $rsobj->sql,
            );
        }

        return $status;
    }

        
    /**
    * To Delete the Available Booking
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array obj
    */
    public function deleteBooking($params)
    {
        $bookingId = isset($params['bookingId']) ?
            $params['bookingId'] : null;
        $data = array(
            'is_deleted' => isset($params['is_deleted']) ?
            $params['is_deleted'] : '',
        );
        $rsobj = $this->dbcon->Execute(
            GET_BOOKING_DETAILS, array($bookingId)
        );
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Booking Delted Successfully',
                'booking_id' => $bookingId,
            );
        } else {
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => ' Booking Is Not available',
                'booking_id' => $bookingId,
                // 'sql' => $rsobj->sql,
            );
        }

        return $status;
    }
}

// End Class.
;
