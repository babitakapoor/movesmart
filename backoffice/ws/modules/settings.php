<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Settings
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle settings related actions.
 */
require_once SQL_PATH.DS.'settings.php'; 
 /**
 * Class to handle setting related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */

class settingsModel
{   
    public $dbcon;
    public $status;
    public $error_general;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );
        $this->error_general = array(
            'response' => array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
            ),
        );
    }
     
    
    /**
    * Returns an json obj of get User Type for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getUserType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_USER_TYPE);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get user type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'userTypeDetails' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get user type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }

    /**
    * Returns an json obj of get Menu Permissions for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getMenuPermissions($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_MENU_PAGES);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get menu pages success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getMenuPermissions' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get menu pages success failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of get Menu pages for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getMenuPages($params)
    {
        $qry = GET_MENU_PAGES;
        $arr = array();
        if (isset($params['isVisible'])) {
            $qry = GET_MENU_PAGES_ONLY_VISIBLE;
            if (isset($params['isVisibleUser'])) {
                $qry = GET_MENU_PAGES_ONLY_VISIBLE_USER;
                $arr = array($params['userTypeId']);
            }
        }

        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get menu pages success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getMenuPages' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get menu pages success failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getMenuPages' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }

    /**
    * Returns an json obj of get brand list for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getBrand($params)
    {
        $qry = GET_BRAND;
        $arr = array();
        if (isset($params['isVisible'])) {
            $qry = GET_BRAND_ONLY_VISIBLE;
            if (isset($params['isVisibleUser'])) {
                $qry = GET_BRAND_ONLY_VISIBLE_USER;
                $arr = array($params['userTypeId']);
            }
        }
        $rsobj = $this->dbcon->Execute($qry, $arr);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get brand pages success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getBrand' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get brand pages failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getBrand' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }

 
    /**
    * Returns an json obj of get questionnaries list for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuestionaries($params)
    {

        $qry = GET_QUESTIONNAIRE;
        //$arr = array();
        /*if(isset($params['isVisible'])) {
            $qry = GET_QUESTIONNAIRE_ONLY_VISIBLE;
            if(isset($params['isVisibleUser']))    {
                $qry = GET_BQUESTIONNAIRE_ONLY_VISIBLE_USER;
                $arr = array($params['userTypeId']);
            }
        }*/
        $params['languageid'] = DEFAULT_LANG;
        $phaseid = isset($params['phaseid']) ? $params['phaseid'] : 0;
        $groupid = isset($params['groupid']) ? $params['groupid'] : 0;
        $rsobj = $this->dbcon->Execute($qry, array($params['languageid'], $phaseid, $groupid));
		 
		// $ar = array($params['languageid'], $phaseid, $groupid);
		 
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get questionaries success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuestionaries' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get questionaries failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getQuestionaries' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get QuesActivity for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuesActivity($params)
    {
        if ($params) {
        }
        $qry = GET_QUES_ACTIVITY;
        //$arr = array();
        $rsobj = $this->dbcon->Execute($qry);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Question Activity success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuesactivity' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Question Activity failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getQuesactivity' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of get Parent Menus for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getParentMenus($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_MENU_PAGES_PARENT);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get parent menus success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getParentMenus' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get parent menus failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getParentMenus' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of get Menu Page Detail for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getMenuPageDetail($param)
    {
        $rsobj = $this->dbcon->Execute(GET_MENU_PAGE_EXIST, array($param['menuPageId']));

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get menu detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getMenuPageDetail' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get menu pages success failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getMenuPageDetail' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
    /**
     * Description : To get brand detail
     * Param       : Array
     * return      : Array.
     **/
     
    /**
    * Returns an json obj of get Brand Detail for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getBrandDetail($param)
    {
        $rsobj = $this->dbcon->Execute(GET_BY_BRAND_ID, array($param['brandId']));
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get group detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getBrandDetail' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getBrandDetail' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of get Questionnaire Detail for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuestionnaireDetail($param)
    {
        $rsobj = $this->dbcon->Execute(GET_BY_QUESTION_ID, array($param['questionnaireId']));

        if ($rsobj->RecordCount()) {
            $formquesinfo   =   array();
            while (!$rsobj->EOF) {
                $formquesinfo = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get group detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuestionnaireDetailByID' => $formquesinfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getQuestionnaireDetailByID' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of get User Type Page Permission for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    
    public function getUserTypePagePermission($params)
    {
        $rsobj = $this->dbcon->Execute(GET_PAGE_USER_TYPE_PERMISSION, array($params['userTypeId']));

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get user type permissions success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'UserTypePermission' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get user type permissions failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }

  
    /**
    * Returns an json obj of insert User Type Permissions  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertUserTypePermissions($params)
    {
        $userTypeId = $params['userTypeId'];
        $revertFlag = 0;

        $this->dbcon->BeginTrans();

        //Delete existing page permissions for user type
        $this->dbcon->Execute(DELETE_USER_TYPE_PAGE_PERMISSIONS, array($userTypeId));

        $pageSettings = $params['pageMenuSettings'];
        foreach ($pageSettings as $key => $pages) {
            $permissionId = '';
            $rsobj = $this->dbcon->Execute(GET_PAGE_PERMISSION, array($permissionId, $userTypeId));

            $data = array(
                    'r_menu_page_id' => $key,
                    'r_user_type_id' => $userTypeId,
                    'add_f' => (isset($pages['add']) && $pages['add'] == 1) ? 1 : 0,
                    'edit_f' => (isset($pages['edit']) && $pages['edit'] == 1) ? 1 : 0,
                    'view_f' => (isset($pages['view']) && $pages['view'] == 1) ? 1 : 0,
                    'delete_f' => (isset($pages['delete']) && $pages['delete'] == 1) ? 1 : 0,
            );

            if ($rsobj->RecordCount()) {
                $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully user type permission updated',
                    // 'sql' => $rsobj->sql,
                );
            } else {
                $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
                $rsInsert = $this->dbcon->Execute($rsInserts);
                if (!$rsInsert) {
                    $revertFlag = 1;
                }
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully user type permission added',
                    // 'sql' => $rsobj->sql,
                );
            }
        }

        if ($revertFlag === 1) {
            $this->dbcon->RollbackTrans();
        } else {
            $this->dbcon->CommitTrans();
        }

        //Return the result array
        return $this->status;
    }
	
	
       /**
    * Returns an json obj of Insert Update Menus for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function InsertUpdateMenus($param)
    {
        $update = (isset($param['id'])) ? $param['id'] : 0;
        if ($update != 0) {
            $rsobj = $this->dbcon->Execute(
                UPDATE_MENU,
                array(
                    $param['menuDisplayName'], $param['menuName'],
                    $param['parentvalue'], $param['menu_type'],
                    $param['menuIcon'], $param['menuOrder'],
                    $param['position'], $param['fileName'], $param['isVisible'], $param['id']
                )
            );
        } else {
            $rsobj = $this->dbcon->Execute(
                INSERT_MENU,
                array(
                    $param['menuDisplayName'],
                    $param['menuName'],
                    $param['parentvalue'],
                    $param['menu_type'],
                    $param['menuIcon'],
                    $param['menuOrder'], $param['position'], $param['fileName'], $param['isVisible']
                )
            );
        }
        $lastInsertId = $this->dbcon->Insert_ID();
        if ($rsobj->RecordCount()) {
            $this->status = array(
                    'status' => 'insert_success',
                    'status_code' => 200,
                    'inserud' => $lastInsertId,
                    // 'sql' => $rsobj->sql,
                    'status_message' => 'Menu details added successfully ',
                );
        } else {
            $this->status = $this->error_general;
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of fetch Manage group for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function fetchManagegroup($param)
    {
        $userid = isset($param['userid']) ? isset($param['userid']) : 1;
        $rsobj = $this->dbcon->Execute(GET_GROUP_EXIST, $userid);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getGroupDetail' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getGroupDetail' => '',
              );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of fetch Manage brand for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function fetchManagebrand($param)
    {
        $userid = isset($param['userid']) ? isset($param['userid']) : 1;
        $rsobj = $this->dbcon->Execute(GET_BRAND_EXIST, $userid);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get brand detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getBrandDetail' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getBrandDetail' => '',
              );
        }

        return $this->status;
    }

    /**
    * Returns an json obj of Insert Update Question for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function InsertUpdateQuestion($param)
    {
		
        $date = date('Y-m-d H:i:s');
        $loggedUserId = isset($param['loggedUserId']) ? $param['loggedUserId'] : '1';
        //$questionnaireId = (isset($param['questionnaireId'])) ? $param['questionnaireId'] : 0;
        //$questionnaireText = (isset($param['questionnaireText'])) ? $param['questionnaireText'] : '-1';
        $group_name = (isset($param['group_name'])) ? $param['group_name'] : '-1';
        $phase_name = (isset($param['phase_name'])) ? $param['phase_name'] : '-1';
        $questopic = (isset($param['questopic'])) ? $param['questopic'] : '';
        $questopictxt = (isset($param['questopictxt'])) ? $param['questopictxt'] : '';
        $questxtarr = (isset($param['questxt'])) ? json_decode($param['questxt']) : array();
        $quesoptnumitems = (isset($param['quesoptnumitems'])) ? json_decode($param['quesoptnumitems']) : array();
        $questxt = (isset($param['questxt'])) ? $param['questxt'] : '';
        $quesicon = (isset($param['quesicon'])) ? $param['quesicon'] : '';
        $activityques = (isset($param['activityques'])) ? $param['activityques'] : '-1';
        $questype = (isset($param['questype'])) ? $param['questype'] : '-1';
        //$selectId = (isset($param['selectid'])) ? $param['selectid'] : 0;
        //$questionnaireId = (isset($param['questionnaireId'])) ? $param['questionnaireId'] : 0;
        $quesId = (isset($param['quesId'])) ? $param['quesId'] : 0;
        $formId = (isset($param['formId'])) ? $param['formId'] : 0;
        $isquestypechange = (isset($param['isquestypechange'])) ? $param['isquestypechange'] : 0;
        $orderid = (isset($param['orderid']) && $param['orderid']!='') ? $param['orderid'] : 0;
        $score = (isset($param['score'])) ? $param['score'] : 0;
        $formoptionIds = array();
        $quesoptionIds = array();
        $title = $group_name.'_'.$phase_name.'_'.$questopictxt;

        //XX $rsobj = $this->dbcon->Execute(GET_FORM_EXTST,array($title));
        //echo "GET_FORM_EXTST_BY_ID,array($quesId)";
        $rsobj = $this->dbcon->Execute(GET_FORM_PHASE_GROUP_TOPIC, array($phase_name, $group_name, $questopic));
        //$monitorvariableId = '';
        if (!($rsobj->RecordCount())) {
			
			
            //echo "INSERT_MONITOR_VARIABLE,array($title)";
            $rsobj = $this->dbcon->Execute(INSERT_MONITOR_VARIABLE, array($title));
            if ($rsobj->RecordCount()) {
                $row = $rsobj->getRows();
                if (isset($row[0]['id'])) {
                    //$monitorvariableId = $row[0]['id'];
                }
            }
        } else {
		
            $row = $rsobj->getRows();
            if (isset($row[0]['form_id'])) {
                $formId = $row[0]['form_id'];
            }
        }
        if ($formId > 0) {
            //echo UPDATE_FORM_QUESTION." array($title,$date,$loggedUserId,$formId)";
            $this->dbcon->Execute(
                UPDATE_FORM_QUESTION, array($title, $date, $loggedUserId, $questopic, $formId)
            );
			
        } else {
            //echo "INSERT_FORM_QUESTION,array($title,$phase_name,$group_name,$questopic,$date,$loggedUserId)";
           $qu=array($title, $phase_name, $group_name, $questopic, $date, $loggedUserId);
		   
		   $this->dbcon->Execute(
                INSERT_FORM_QUESTION,
                array($title, $phase_name, $group_name, $questopic, $date, $loggedUserId)
            );
			
			
            $formId = $this->dbcon->Insert_ID();
			
        }
        //XX $formId = ($questionnaireId) ?$questionnaireId:$this->dbcon->Insert_ID();
        $formelemId = $quesId;
        if ($quesId > 0) {
			
            //echo "UPADTE_QUESTION,array($formId,$questxt,$activityques,$date,$questype,$quesicon,$quesId)";
            $this->dbcon->Execute(
                UPADTE_QUESTION,
                array($formId, $questxt, $activityques, $date, $questype, $quesicon, $orderid, $score, $quesId)
            );
            $rsobj = $this->dbcon->Execute(GET_QUESTION_OPTION, array($quesId));
            $this->dbcon->Execute(UPDATE_QUESTION_OPTION_VALUES, array($quesId));

            if ($rsobj->RecordCount()) {
                while (!$rsobj->EOF) {
                    $formoptionIds[] = $rsobj->fields;
                    $rsobj->MoveNext();
                }
            }
            foreach ($formoptionIds as $ids) {
                $quesoptionIds[] = $ids['id'];
            }
        } else {
		
            $this->dbcon->Execute(
                INSERT_QUESTION,
                array(
                    $formId, $questxt, $quesicon, $activityques, $date, $loggedUserId, $questype, $orderid, $score
                )
            );
            $formelemId = $this->dbcon->Insert_ID();
        }
        foreach ($questxtarr as $key => $ques) {
            $item[$key] = (array) $ques;
            $queslangeditid = isset($item[$key]['queslangeditid']) ? $item[$key]['queslangeditid'] : 0;
            if ($queslangeditid > 0) {
                $this->dbcon->Execute(
                    UPDATE_QUESTION_LANGUAGE,
                    array(
                        $formelemId, $item[$key]['value'],
                        $item[$key]['queslanguageid'],
                        $item[$key]['quesinfo'], $date, $queslangeditid
                    )
                );
            } else {
                $this->dbcon->Execute(
                    INSERT_QUESTION_LANGUAGE,
                    array(
                        $formelemId, $item[$key]['value'], $item[$key]['queslanguageid'],
                        $item[$key]['quesinfo'], $date
                    )
                );
            }
        }
        if ($isquestypechange == 1) {
            $this->dbcon->Execute(UPDATE_QUESTION_OPTION_QUESTYPE, array($formelemId));
            $this->dbcon->Execute(UPDATE_QUESTION_OPTION_PROPERTY_QUESTYPE, array($formelemId));
            $this->dbcon->Execute(UPDATE_QUESTION_OPTION_LANG_QUESTYPE, array($formelemId));
        }
        //$quesansopt = '';
        $optIds = array();
        foreach ($quesoptnumitems as $key => $opt) {
            $item[$key] = (array) $opt;
            $optIds[] = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
        }
	$rsobj = $this->dbcon->Execute(DELETE_BY_OPTION_ID, $formelemId);
        foreach ($quesoptnumitems as $key => $opt) {
            $item[$key] = (array) $opt;   
				
            if ($questype == 1) {
				
                $quesansopt = $item[$key]['value'];
                $optionitem = $item[$key]['optnumvalarr'];
                $optpropid = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
                $quesansopt = rtrim($quesansopt, '_');
				 
				
                foreach ($optionitem as $index => $options) {
					
                    $optitem[$index] = (array) $options;
					//print_r($optitem[$index]);
                    $numoptid = isset($optitem[$index]['numoptid']) ? $optitem[$index]['numoptid'] : 0;
                    $updateoption = array();
                    $updateoption['r_form_elementid'] = $formelemId;
                    if (isset($optitem[$index]['min'])) {
                        $updateoption['min_value'] = $optitem[$index]['min'];
                    }
                    if (isset($optitem[$index]['max'])) {
                        $updateoption['max_value'] = $optitem[$index]['max'];
                    }
                    if (isset($optitem[$index]['range'])) {
                        $updateoption['range_points'] = $optitem[$index]['range'];
                    }
                    $updateoption['f_isdelete'] = 0;
                   // $updateoptrs = $this->dbcon->Execute(GETOPTION_BYID, array($numoptid));
					
					
                   /* if ($updateoptrs->RecordCount()) {
                        $updateoption['modifiedon'] = $date;
                        $upoptquery = $this->dbcon->GetUpdateSQL($updateoptrs, $updateoption);
                        $this->dbcon->Execute($upoptquery);
                    } else {
                        $updateoption['createdon'] = $date;
                        $insoptquery = $this->dbcon->GetInsertSQL($updateoptrs, $updateoption);
                        $this->dbcon->Execute($insoptquery);
                    }*/
								
                    $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId,'',$updateoption['min_value'],$updateoption['max_value'],$updateoption['range_points'],$date,$orderid)
                    );
				  				
                }
                if ($optpropid > 0) {
                    $this->dbcon->Execute(
                        UPDATE_QUESTION_OPTION_PROPERTY,
                        array($formelemId, $quesansopt, $date, $optpropid)
                    );
					
                } else {
                    $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION_PROPERTY,
                        array($formelemId, $quesansopt, $date)
                    );
                }
            }
            if ($questype == 2) {
				
				
                //$radioopteditid = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
				 $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId,'','','',$item[$key]['radiopoint'],$date,0)
                    );
					$quesoptionId = $this->dbcon->Insert_ID();
					//echo $quesoptionId;
					//die;
              /*  if ($radioopteditid) {
                    $this->dbcon->Execute(
                        UPDATE_QUESTION_OPTION,
                        array($formelemId, '', '', $item[$key]['radiopoint'], $date, $radioopteditid)
                    );
                } else {
                    $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId, '', '', '', $item[$key]['radiopoint'], $date, '')
                    );
                }*/
               // 
			   
                $optionitem = $item[$key]['quesoptlang'];
					//$rsobj = $this->dbcon->Execute(DELETE_BY_OPTION_LANG, $quesoptionId);
				
                foreach ($optionitem as $index => $options) {
                    $optitem[$index] = (array) $options;
					//print_r($optitem);
                    $opteditlangid = isset($optitem[$index]['opteditlangid']) ? $optitem[$index]['opteditlangid'] : 0;
					   
					
                     if ($opteditlangid > 0) {
                        $this->dbcon->Execute(
                            UPDATE_QUESTION_OPTION_LANG,
                            array(
                                $quesoptionId, $optitem[$index]['value'],
                                $optitem[$index]['optradiolanguageid'], $date, $opteditlangid
                            )
                        );
						
							
                    } else { 
                        $this->dbcon->Execute(
                            INSERT_QUESTION_OPTION_LANG,
                            array(
                                $quesoptionId, $optitem[$index]['value'],
                                $optitem[$index]['optradiolanguageid'], $date
                            )
                        );
						
							
                   }
                }
            }     
            if ($questype == 3) {
                $yesnooptid = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
                // if ($yesnooptid) {
                //     $this->dbcon->Execute(
                //         UPDATE_QUESTION_OPTION,
                //         array($formelemId, '', '', $item[$key]['value'], $date, $yesnooptid)
                //     );
                // } else {
                    $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId, '', '', '', $item[$key]['value'], $date, $item[$key]['order'])
                    );
               // }
            }
            if ($questype == 4) {
                $smilyoptid = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
                // if ($smilyoptid) {
                //     $this->dbcon->Execute(
                //         UPDATE_QUESTION_OPTION,
                //         array($formelemId, '', '', $item[$key]['value'], $date, $smilyoptid)
                //     );
                // } else {
                    $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId, '', '', '', $item[$key]['value'], $date, $item[$key]['order'])
                    );
               // }
            }
            /*SN added Question Type-checkbox -20160426*/
            if ($questype == 5) {
				$this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId, '', '', '', $item[$key]['checkpoint'], $date,0)
                    );
					 $quesoptionId =  $this->dbcon->Insert_ID();
					 //echo $item[$key]['checkpoint'];
                /* $checkopteditid = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
                //if(in_array($checkopteditid,$quesoptionIds)){
                if ($checkopteditid) {
                    $this->dbcon->Execute(
                        UPDATE_QUESTION_OPTION,
                        array($formelemId, '', '', $item[$key]['checkpoint'], $date, $checkopteditid)
                    );
                } else {
                    $this->dbcon->Execute(
                        INSERT_QUESTION_OPTION,
                        array($formelemId, '', '', '', $item[$key]['checkpoint'], $date, '')
                    );
                } */
                /*}else{
                    $rsobj = $this->dbcon->Execute(UPDATE_QUESTION_OPTION_VALUES,array($checkopteditid));
                }*/
             //   $quesoptionId = ($checkopteditid) ? $checkopteditid : $this->dbcon->Insert_ID();
                $optionitem = $item[$key]['quesoptlang'];
                foreach ($optionitem as $index => $options) {
                    $optitem[$index] = (array) $options;
                    $opteditlangid = isset($optitem[$index]['opteditlangid']) ? $optitem[$index]['opteditlangid'] : 0;
                    if ($opteditlangid > 0) {
                        $this->dbcon->Execute(
                            UPDATE_QUESTION_OPTION_LANG,
                            array(
                                $quesoptionId,
                                $optitem[$index]['value'],
                                $optitem[$index]['optchecklanguageid'], $date, $opteditlangid
                            )
                        );
                    } else {
                        $this->dbcon->Execute(
                            INSERT_QUESTION_OPTION_LANG,
                            array(
                                $quesoptionId,
                                $optitem[$index]['value'],
                                $optitem[$index]['optchecklanguageid'],
                                $date
                            )
                        );
                    }
                }
            }
            if ($questype == 6) {
                $likdisoptid = isset($item[$key]['opteditid']) ? $item[$key]['opteditid'] : 0;
                // if ($likdisoptid) {
                //     $this->dbcon->Execute(
                //         UPDATE_QUESTION_OPTION,
                //         array($formelemId, '', '', $item[$key]['value'], $date, $likdisoptid)
                //     );
                // } else {
                   $this->dbcon->Execute(
                       INSERT_QUESTION_OPTION,
                       array($formelemId, '', '', '', $item[$key]['value'], $date, $item[$key]['order'])
                   );
              //  }
            }
        }

        //}
        //$questxtmonitor = $title.'_ques_'.$formelemId;

        /*
        if(!($quesId > 0)){
            $rsobj = $this->dbcon->Execute(INSERT_MONITOR_VARIABLE,array($questxtmonitor));
            $monitorelemvariableId = $this->dbcon->Insert_ID();            
        }
        */
        /**/
        $rsobj = $this->dbcon->Execute(GET_FORMELEMENT_MV_EXIST, array($formelemId));
        if (!$rsobj->RecordCount()) {
            $questxtmonitor = $title.'_ques_'.$formelemId;
            $this->dbcon->Execute(INSERT_MONITOR_VARIABLE, array($questxtmonitor));
            $mvelemvariableId = $this->dbcon->Insert_ID();
            $this->dbcon->Execute(INSERT_FORMELEMONITORVARIABLE, array($formelemId, $mvelemvariableId));
            /*
            $row = $rsobj->getRows();
            if(isset($row[0]['id'])) {
                $monitorelemvariableId = $row[0]['id'];
            }*/
        }
        /**/
        if ($formelemId) {
        }
        if ($formId) {
            $this->status = array(
                'status' => 200,
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
                'quesId' => $formelemId,
            );
        } else {
            $this->status = array(
                'status' => 0,
                'status_code' => 0,
                'status_message' => 'Error',
                'quesId' => '',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of insert Phase Range for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertPhaseRange($params)
    {
        $phase_name = isset($params['phase_name']) ? $params['phase_name'] : 0;
        $group_name = isset($params['group_name']) ? $params['group_name'] : 0;
        //$pointweight = isset($params['pointweight']) ? $params['pointweight'] : 0;
        //$maxweight = isset($params['maxweight']) ? $params['maxweight'] : 0;
        $rangeid = isset($params['rangeid']) ? $params['rangeid'] : 0;
        $phaserange = isset($params['phaserange']) ? json_decode($params['phaserange']) : array();
        $topicid = isset($params['topicid']) ? $params['topicid'] : 0;
        $date = date('Y-m-d H:i:s');
        //$data = array();
        /*
        if(isset($params['phase_name'])) { 
            $data['r_phase_id']        =    $params['phase_name'];
        }
        if(isset($params['group_name'])) { 
            $data['r_group_id']        =    $params['group_name'];
        }
        if(isset($params['topicid'])) { 
            $data['r_topic_id']        =    $params['topicid'];
        }
        if(isset($params['maxweight'])) {
            $data['max_range']        =    $params['maxweight'];
        }
        if(isset($params['maxweight'])) {
            $data['min_range']        =    0;
        }
        $data['text_type']        =    0;
        $data['f_isdelete']        =    0;
        */
        $quesphagerangeId = '';
        foreach ($phaserange as $key => $range) {
            $item[$key] = (array) $range;
            if ($rangeid != 0) {
                $quesphagerangeId = $rangeid;
                $this->dbcon->Execute(
                    UPADTE_QUES_PHASE_RANGE,
                    array($phase_name, $group_name, $item[$key]['max'], $item[$key]['min'], $date, $topicid, $rangeid)
                );
                $this->dbcon->Execute(
                    UPADTE_QUES_PHASE_RANGE_LANGUAGE,
                    array($date, $rangeid)
                );
            } else {
                $this->dbcon->Execute(
                    INSERT_QUES_PHASE_RANGE,
                    array($phase_name, $group_name, $item[$key]['max'], $item[$key]['min'], $date, $topicid)
                );
                $quesphagerangeId = $this->dbcon->Insert_ID();
            }
            $i = 1;
            foreach ($item[$key]['Text'] as $index => $val) {
                $ranges[$index] = (array) $val;
                $objrange = (array) $ranges[$index];
                $obj = (array) $objrange[0];
                //    $i=1;
                foreach ($obj['Text'] as $k => $v) {
                    $text[$k] = (array) $v;
                    $this->dbcon->Execute(
                        INSERT_QUES_PHASE_RANGE_LANGUAGE,
                        array($quesphagerangeId, $text[$k]['languageid'], $index, $text[$k]['value'], $date)
                    );
                }
                $i = $i + 1;
            }
            /*$i = 1;
            foreach($item[$key] as $index=>$val){
                $ranges[$index] =(array)$val;
                $objrange = (array)$ranges[$index];
                $obj = (array)$objrange[0];
                $rsobj = $this->dbcon->Execute(
                    INSERT_QUES_PHASE_RANGE,
                    array($phase_name,$group_name,$pointweight,$maxweight,$obj['min'],$obj['max'],$i,$date)
                );
                $quesphagerangeId = $this->dbcon->Insert_ID();
                foreach($obj['Text'] as $k=>$v){
                    $text[$k] = (array)$v;
                    $rsobj = $this->dbcon->Execute(
                        INSERT_QUES_PHASE_RANGE_LANGUAGE,
                        array($quesphagerangeId,$text[$k]['languageid'],$text[$k]['value'],$date)
                    );
                }
                $i = $i+1;
            }*/
        }
        if ($quesphagerangeId) {
            $this->status = array(
                'status' => 200,
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
                'quesphagerangeId' => $quesphagerangeId,
            );
        } else {
            $this->status = array(
                'status' => 0,
                'status_code' => 0,
                'status_message' => 'Error',
                'quesphagerangeId' => '',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of insert Phase Weight for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertPhaseWeight($params)
    {
        $phase_id = isset($params['phase_id']) ? $params['phase_id'] : 0;
        $group_id = isset($params['group_id']) ? $params['group_id'] : 0;
        $pointweight = isset($params['pointweight']) ? $params['pointweight'] : 0;
        $maxweight = isset($params['maxweight']) ? $params['maxweight'] : 0;
        $phaseweightageid = isset($params['phaseweightageid']) ? $params['phaseweightageid'] : 0;
        $date = date('Y-m-d H:i:s');
        //echo UPDATE_QUES_PHASE_WEIGHT."($phase_id,$group_id,$pointweight,$maxweight,$date)";
        if ($phaseweightageid) {
            $this->dbcon->Execute(
                UPDATE_QUES_PHASE_WEIGHT,
                array($phase_id, $group_id, $pointweight, $maxweight, $date, $phaseweightageid)
            );
        } else {
            $this->dbcon->Execute(
                INSERT_QUES_PHASE_WEIGHT,
                array($phase_id, $group_id, $pointweight, $maxweight, $date)
            );
        }
        $phaseId = $this->dbcon->Insert_ID();
        if ($phaseId) {
            $this->status = array(
                'status' => 200,
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
                'phaseId' => $phaseId,
            );
        } else {
            $this->status = array(
                'status' => 0,
                'status_code' => 0,
                'status_message' => 'Error',
                'phaseId' => '',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of Insert Update Education for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function InsertUpdateEducation($params)
    {
        //$edutitletxtitems = 0;//isset($params['edutitletxtitems']) ?json_decode($params['edutitletxtitems']):0;
        $eduinfotxtitems = isset($params['eduinfotxtitems']) ? json_decode($params['eduinfotxtitems']) : 0;
        $group_id = isset($params['group_name']) ? $params['group_name'] : 0;
        //$indextype = 0;//isset($params['indextype']) ?$params['indextype']:0;
        //$showday = 0;//isset($params['showday']) ?$params['showday']:0;
        $edueditid = 0;//isset($params['eduid']) ?$params['eduid']:0;
        $date = date('Y-m-d H:i:s');
        //print_r($params['eduinfotxtitems']."--".$eduinfotxtitems);
        foreach ($eduinfotxtitems as $k => $info) {
            $obj[$k] = (array) $info;
            $edueditid = isset($obj[$k]['edueditid']) ? $obj[$k]['edueditid'] : 0;
            $indexgroup = isset($obj[$k]['indexgroup']) ? $obj[$k]['indexgroup'] : 0;
            $days = isset($obj[$k]['days']) ? $obj[$k]['days'] : 0;
            //$infoarr = isset($obj[$k]['info']) ? $obj[$k]['info'] : 0;
            $eduquery = $this->dbcon->Execute(GET_EDUCATION_BY_ID, array($edueditid));
            $edudata = array();
            $edudata['r_group_id'] = $group_id;
            $edudata['index_val'] = $indexgroup;
            $edudata['day'] = $days;
            if ($eduquery->RecordCount()) {
                $edudata['modifiedon'] = $date;
                $updatequery = $this->dbcon->GetUpdateSQL($eduquery, $edudata);
                $this->dbcon->Execute($updatequery);
            } else {
                $edudata['createdon'] = $date;
                $insertquery = $this->dbcon->GetInsertSQL($eduquery, $edudata);
                $this->dbcon->Execute($insertquery);
                $edueditid = $this->dbcon->Insert_ID();
            }
            foreach ($obj[$k]['info'] as $ind => $value) {
                $object[$ind] = (array) $value;
                $edulangeditid = isset($object[$ind]['edulangeditid']) ? $object[$ind]['edulangeditid'] : 0;
                $updatetxtitems['ref_id'] = $edueditid;
                $updatetxtitems['r_langref_id'] = 1;
                if (isset($object[$ind]['edutitleinfo'])) {
                    $updatetxtitems['languagae_desc'] = $object[$ind]['edutitleinfo'];
                }
                if (isset($object[$ind]['eduinfotxtlangid'])) {
                    $updatetxtitems['r_language_id'] = $object[$ind]['eduinfotxtlangid'];
                }
                $updatetxtitems['f_isdelete'] = 0;
                $updateoptrs = $this->dbcon->Execute(GET_EDUCATION_LANGUAGE_DESC_BYID, array($edulangeditid));
                // echo $updateoptrs->sql."-".$updateoptrs->RecordCount();
                if ($updateoptrs->RecordCount()) {
                    $updatetxtitems['modifiedon'] = $date;
                    $upoptquery = $this->dbcon->GetUpdateSQL($updateoptrs, $updatetxtitems);
                    // echo $upoptquery;
                    $this->dbcon->Execute($upoptquery);
                } else {
                    $updatetxtitems['createdon'] = $date;
                    $insoptquery = $this->dbcon->GetInsertSQL($updateoptrs, $updatetxtitems);
                    $this->dbcon->Execute($insoptquery);
                }
            }
        }
        if ($edueditid!=0) {
            $this->status = array(
                'status' => 200,
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
            );
        } else {
            $this->status = array(
                'status' => 0,
                'status_code' => 0,
                'status_message' => 'Error',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Education for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getEducation($param)
    {
        $groupid = $param['groupid'];
        $langid = 1;
        $rsobj = $this->dbcon->Execute(GET_EDUCATION_GROUP, array($groupid, $langid));
        if ($rsobj->RecordCount()) {
            $eduInfo    =   array();
            while (!$rsobj->EOF) {
                $eduInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get question phase group type detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getEducation' => $eduInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get question phasegroup type failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getEducation' => '',
              );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of delete Phase Range By Id  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deletePhaseRangeById($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_PHASE_RANGE_ID, $params['id']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 200,
                'status_message' => 'Delete successfully',
            );
        }

        return $this->status;
    }
    /**
    * Insert an json obj of delete Phase Range By Id  for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertQuesActivity($params)
    {
        $activity = isset($params['activity']) ? $params['activity'] : 0;
        $activquescredit = isset($params['activquescredit']) ? $params['activquescredit'] : 0;
        $activityperweek = isset($params['activityperweek']) ? $params['activityperweek'] : 0;
        $groupid = isset($params['groupid']) ? $params['groupid'] : 0;
        $phaseid = isset($params['phaseid']) ? $params['phaseid'] : 0;
        $quesactid = isset($params['quesactid']) ? $params['quesactid'] : 0;
        $ques_actpointid = isset($params['ques_actpointid']) ? $params['ques_actpointid'] : 0;
        $quesacttopicid = isset($params['quesacttopicid']) ? $params['quesacttopicid'] : 0;
        $minutes = isset($params['minutes']) ? $params['minutes'] : 0;
        $seconds = $minutes * 60;
        $date = date('Y-m-d H:i:s');
        if ($quesactid) {
            $this->dbcon->Execute(
                UPDATE_PHASE_QUES_ACTIVITY,
                array($activity, $phaseid, $groupid, $date, $quesacttopicid, $quesactid)
            );
        } else {
            $this->dbcon->Execute(
                INSERT_PHASE_QUES_ACTIVITY, array($activity, $phaseid, $groupid, $date, $quesacttopicid)
            );
        }

        $phaseactivityId = ($quesactid) ? $quesactid : $this->dbcon->Insert_ID();
        if ($ques_actpointid) {
            $this->dbcon->Execute(
                UPDATE_PHASE_QUES_ACTIVITY_POINTS,
                array($phaseactivityId, $activquescredit, $activityperweek, $seconds, $date, $ques_actpointid)
            );
        } else {
            $this->dbcon->Execute(
                INSERT_PHASE_QUES_ACTIVITY_POINTS,
                array($phaseactivityId, $activquescredit, $activityperweek, $date)
            );
        }
        if ($phaseactivityId) {
            $this->status = array(
                'status' => 200,
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
                'phaseactivityId' => $phaseactivityId,
            );
        } else {
            $this->status = array(
                'status' => 0,
                'status_code' => 0,
                'status_message' => 'Error',
                'phaseactivityId' => '',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Ques Phase Group Type  for a company.
    *
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuesPhaseGroupType($param)
    {
        $rsobj = $this->dbcon->Execute(GET_QUES_PHASE_GROUP_TYPE, array($param['phaseid'], $param['groupid']));
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get question phase group type detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getQuesPhaseGroupType' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get question phasegroup type failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getQuesPhaseGroupType' => '',
              );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Ques Phase Group  Type  for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuesPhaseGroup($param)
    {
        if ($param) {
        }
        $rsobj = $this->dbcon->Execute(GET_QUES_PHASE_GROUP);
        if ($rsobj->RecordCount()) {
            $userInfo   =   0;
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get question phase group type detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getQuesPhaseGroup' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get question phasegroup type failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getQuesPhaseGroup' => '',
              );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Ques Phase Group Activity Points  for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuesPhaseGroupActivityPoints($param)
    {
        $rsobj = $this->dbcon->Execute(
            GET_QUES_PHASE_GROUP_ACTIVITY_POINTS, array($param['phaseid'], $param['groupid'])
        );
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get question phase group type detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getQuesPhaseGroupActivityPoints' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get question phasegroup type failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getQuesPhaseGroupActivityPoints' => '',
              );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Form Id For Phase And Group And Topic  for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getFormIdForPhaseAndGroupAndTopic($param)
    {
        $rsobj = $this->dbcon->Execute(
            GET_FORM_PHASE_GROUP_TOPIC,
            array($param['phaseid'], $param['groupid'], $param['topicid'])
        );
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get Form details for phase group type detail success',
                    'total_records' => $rsobj->RecordCount(),
                    'getFormIdForPhaseAndGroup' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get Form details for phase group type detail success',
                    'total_records' => 0,
                    'getQuesPhaseGroupType' => '',
              );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of fetch Manage questionnaire for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    /*public function fetchManagequestionnaire($param)
    {
        $userid = isset($param['userid']) ? isset($param['userid']) : 1;
        $rsobj = $this->dbcon->Execute(GET_QUESTIONNAIRE, $userid);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get brand detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getQuestionnaireDetail' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getQuestionnaireDetail' => '',
              );
        }

        return $this->status;
    }
    */
    /**
    * Returns an json obj of fetch Manage Menu for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function fetchManageMenu($param)
    {
        $userid = isset($param['userid']) ? isset($param['userid']) : 1;
        $rsobj = $this->dbcon->Execute(GET_MENU_PAGE_EXIST, $userid);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getMenuPageDetail' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getMenuPageDetail' => '',
              );
        }

        return $this->status;
    }
    */
    /**
    * Returns an json obj of delete Menu Page for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deleteMenuPage($params)
    {
        $rsobj = $this->dbcon->Execute(
            DELETE_MENU_PAGE_PARENT_EXIST, array($params['menuPageId'])
        );

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'parent_exist',
                'status_code' => 1,
                'status_message' => 'Delete failed, This menu associated as parent',
            );
        } else {
            $this->dbcon->Execute(DELETE_MENU_PAGE, array($params['menuPageId']));
            if ($this->dbcon->Affected_Rows() > 0) {
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Menu deleted successfully',
                );
            } else {
                $this->status = $this->error_general;
            }
        }

        return $this->status;
    }

    /**
    * Returns an json obj of delete questionnaire for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deletequestionnaire($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_QUESTIONNAIRE_ID, $params['deleteid']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of insert Brand  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertBrand($params)
    {
		
        $update = (isset($params['id'])) ? $params['id'] : 0;
        //$date = date('Y-m-d H:i:s');
        if ($update != 0) {
            // update;
            $this->dbcon->Execute(
                UPDATE_BRAND,
                array($params['manageBrandName'], $params['manageBrandNameDescription'], $params['id'])
            );
			
        } else {
            //insert;
			$str='Select * from t_machine_brands where brand_name="'.$params['manageBrandName'].'" and brand_description="'.$params['manageBrandNameDescription'].'"';
			$res = mysql_query($str);
			$row = mysql_fetch_array($res);
			
			if(empty($row)){
				
            $this->dbcon->Execute(
                INSERT_BRAND,
                array($params['manageBrandName'], $params['manageBrandNameDescription'], $params['loggedUserId'])
            );
		$lastInsertId = $this->dbcon->Insert_ID();
			}
			else{
				
			$lastInsertId =0;
		}
			 
        }
     	
        if (isset($lastInsertId) && $lastInsertId>0) {
            $this->status = array(
                'status' => 'parent_exist',
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
            );
        }
			else if (isset($lastInsertId) && $lastInsertId==0) {
            $this->status = array(
                'status' => 'parent_exist',
                'status_code' => 2,
                'status_message' => 'Already Exist',
            );
        }
		else {
			
            $this->status = array(
                'status' => 'parent_exist',
                'status_code' => 0,
                'status_message' => 'error',
            );
        }
	
        return $this->status;
    }
     
    /**
    * Returns an json obj of insert or update Strength Programs for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertUpdateStrengthProgram($params)
    {
        $strengthProgramId = (isset($params['strengthProgramId'])) ? $params['strengthProgramId'] : '-1';
        $dateTime = date('Y-m-d H:i:s');

        $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM_EXIST, array($strengthProgramId));

        $data = array(
                'description' => $params['description'],
                'type' => $params['typeval'],
                'r_company_id' => $params['companyId'],
                'modified_by' => $params['loggedUser'],
                'modified_date' => $dateTime,
                );

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'update_success',
                'status_code' => 200,
                'status_message' => 'Strength program details updated successfully',
                'strength_program_id' => $strengthProgramId,
            );
        } else {
            $data['created_by'] = $params['loggedUser'];
            $data['created_date'] = $dateTime;
            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $rsInsert = $this->dbcon->Execute($rsInserts);

            if ($rsInsert) {
                $this->status = array(
                    'status' => 'insert_success',
                    'status_code' => 200,
                    'status_message' => 'Strength program details added successfully',
                    'strength_program_id' => $this->dbcon->Insert_ID(),
                );
            } else {
                $this->status = $this->error_general;
            }
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of insert or update Strength Programs Training for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function insertUpdateStrengthProgramTraining($params)
    {
        $strengthProgramId = (isset($params['strengthProgramId'])) ? $params['strengthProgramId'] : '-1';
        $strgthProgTrainId = (
            isset($params['strengthProgramTrainingId'])
        ) ? $params['strengthProgramTrainingId'] : '-1';

        //$traningWeek = $params['training_week'];
        $dateTime = date('Y-m-d H:i:s');

        $rsobj = $this->dbcon->Execute(
            GET_STRENGTH_PROGRAM_TRAINING_EXIST,
            array($strgthProgTrainId, $strengthProgramId)
        );
        $data = array(
                'r_strength_program_id' => $strengthProgramId,
                'training_week' => $params['training_week'],
                'series' => $params['series'],
                'reputation' => $params['reputation'],
                'time' => $params['time'],
                'strength_percentage' => $params['strength_percentage'],
                'rest' => $params['rest'],
                'modified_by' => $params['loggedUserId'],
                'modified_date' => $dateTime,
                );

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'update_success',
                'status_code' => 200,
                'status_message' => 'Strength program training details updated successfully',
            );
        } else {
            $data['created_by'] = $params['loggedUserId'];
            $data['created_date'] = $dateTime;
            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $rsInsert = $this->dbcon->Execute($rsInserts);

            if ($rsInsert) {
                $this->status = array(
                    'status' => 'insert_success',
                    'status_code' => 200,
                    'status_message' => 'Strength program training details added successfully',
                );
            } else {
                $this->status = $this->error_general;
            }
        }

        return $this->status;
    }

 
    /**
    * Returns an json obj of get Strength Programs detail by id for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getStrengthProgramDetailById($params)
    {
        $strengthProgramId = (isset($params['strengthProgramId'])) ? $params['strengthProgramId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM_EXIST, array($strengthProgramId));

        $this->status = $this->error_general;
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Set strength program details success',
                'rows' => $rsobj->fields,
            );
        }

        return $this->status;
    }

    /**
    * Returns an json obj of get Strength Programs training list for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getStrengthTrainingList($params)
    {
        $strengthProgramId = (isset($params['strengthProgramId'])) ? $params['strengthProgramId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM_TRAINING_LIST, array($strengthProgramId));
        $this->status = $this->error_general;
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Set strength program details success',
                'rows' => $rsobj->GetRows(),
                // 'sql' => $rsobj->sql,
                'totalCount' => $this->getLastQueryTotalCount(),
                'total_records' => $rsobj->RecordCount(),
                'count' => $rsobj->fields,
            );
        } else {
            $this->status = array(
                'status' => 'Failed',
                'status_code' => 400,
                'status_message' => 'Set strength program details failed',
                // 'sql' => $rsobj->sql,
            );
        }

        return $this->status;
    }
     
    /**
    * Returns an json obj of delete Strength Programs training by id for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deleteStrengthProgramTrainingById($params)
    {
        $this->status = $this->error_general;

        $strngthProgTrainId = (
            isset($params['strengthProgramTrainingId'])
        ) ? $params['strengthProgramTrainingId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM_TRAINING_BY_ID, array($strngthProgTrainId));

        $data = array('is_deleted' => 1);

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Set strength program training delete success',
            );
        }

        return $this->status;
    }
     
    /**
    * Returns an json obj of delete Strength Programs  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deleteStrengthProgramById($params)
    {
        $this->status = $this->error_general;

        $strengthProgramId = (isset($params['strengthProgramId'])) ? $params['strengthProgramId'] : '-1';
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM_EXIST, array($strengthProgramId));

        $data = array('is_deleted' => 1);

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Strength program delete success',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of strength Program List  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function strengthProgramList($params)
    {
		
        $limit = '';
        if (isset($params['limitStart']) && isset($params['limitEnd'])) {
            $limitStart = $params['limitStart'];
            $limitEnd = $params['limitEnd'];
            $limit = 'LIMIT '.$limitStart.','.$limitEnd;
        }

        $filterQry = '';

        //To get filter data
        if (isset($params['searchType'])) {
            if (trim($params['searchValue']) != '') {
                $filterQry = 'AND '.$params['searchType']." LIKE '".trim($params['searchValue'])."%'";
            }
        }

        $sort = '';

        if (isset($params['labelField']) && isset($params['sortType'])) {
            $sort = 'ORDER BY '.$params['labelField'].' '.$params['sortType'];
        }

        $qry = GET_STREGTH_PROGRAM_BY_COMPANY.' '.$filterQry.' '.$sort.' '.$limit;

        $rsobj = $this->dbcon->Execute($qry, array($params['companyId']));

        if ($rsobj->RecordCount()) {
            $strengthProgramList    =   array();
            while (!$rsobj->EOF) {
                $strengthProgramList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get menu detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthProgramList' => $strengthProgramList,
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 400,
                'status_message' => 'Get menu pages success failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'strengthProgramList' => '',
            );
        }

        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of get Last Query Total Count for a company.
    * 
    * @return array object object
    */ 
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }

    /**
     * Returns an json obj of get Security Questions for a company.
     * @return array object object
     * @internal param array $param service parameter, Search arguments
     *
     */ 
    public function getSecurityQuestions()
    {
        $rsobj = $this->dbcon->Execute(GET_SECURITY_QUESTIONS);

        $sQuestions = array();
        if ($rsobj->RecordCount()) {
            $sQuestions = $rsobj->GetRows();
        }
        $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get menu detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'sQuestions' => $sQuestions,
            );

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Language Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getLanguageType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_LANGUAGE_TYPE);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get language type details success',
                'total_records' => $rsobj->RecordCount(),
                'total_count' => $this->getLastQueryTotalCount(),
                // 'sql' => $rsobj->sql,
                'languageTypeDetails' => $userInfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get language  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of insert Language Type Permissions for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertLanguageTypePermissions($params)
    {
        if ($params) {

        }
        $update = (isset($params['id'])) ? $params['id'] : 0;
        $lastInsertId   =$params['id'];
        $statusmessage  =   "Update Language successfully";
        if ($update != 0) {
            $rsobj = $this->dbcon->Execute(
                UPDATE_TRANSLATION,
                array(
                    $params['manageMenutranslationkeyTypeId'],
                    $params['manageMenuLanguageTypeId'],
                    $params['menuDisplayName'],
                    $params['id']
                )
            );
        } else {
            $rsobj = $this->dbcon->Execute(
                SET_LANGUAGE_PROGRAM,
                array(
                    $params['manageMenutranslationkeyTypeId'],
                    $params['manageMenuLanguageTypeId'],
                    $params['menuDisplayName']
                )
            );
            $lastInsertId = $this->dbcon->Insert_ID();
            $statusmessage  =   "Inserted Language successfully";
        }
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => $statusmessage,
                'status_code' => 200,
                'inserud' => $lastInsertId,
                // 'sql' => $rsobj->sql,
                'status_message' => 'language translation details added successfully ',
            );
        } else {
            $this->status = $this->error_general;
        }

         return $this->status;
    }
        
    /**
    * Returns an json obj of get Translation key Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getTranslationkeyType($params){
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_TRANSLATIONKEY_TYPE);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get translationkey type details success',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'translationkeyTypeDetails' => $userInfo,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get translationkey  type details failed',
            'total_records' => 0,
            // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }
        
    /**
    * Returns an json obj of get Translations for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTranslations($params)
    {
        $rsobj = $this->dbcon->Execute(GET_TRANSLATION, array($params['translationkey_id']));

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get translationkey type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'translationlist' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get translationkey  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of update Translations Key for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    /*public function updateTranslationsKey($params)
    {
        $date = date('Y-m-d H:i:s');
        $rsobj = $this->dbcon->Execute(GET_TRANSLATIONKEY_BY_ID, array($params['id']));
        $data = array();
        if (isset($params['translationkey'])) {
            $data['translation_key'] = $params['translationkey'];
        }

        if ($rsobj->RecordCount()) {
            $data['modified_on'] = $date;
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully  updated',
                    // 'sql' => $rsobj->sql,
                );
        } else {
            $data['created_on'] = $date;
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
                //Set the status message
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully added',
                    // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    */
    /**
    * Returns an json obj of Update Imported Translations for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function UpdateImportedTranslations($params)
    {
        $date = date('Y-m-d H:i:s');
        $translationkey_id = 0;
        $translations = isset($params['translations']) ? $params['translations'] : array();
        if (count($translations) > 0) {
            foreach ($translations as $t) {
                $data = array();
                if ($t['translationKeytext']) {
                    $data['translation_text'] = $t['translationKeytext'];
                }
                if ($t['translationLanguageID']) {
                    $data['r_language_id'] = $t['translationLanguageID'];
                }
                $rsobj = $this->dbcon->Execute(
                    'SELECT translationkey_id 
                        FROM t_translation_key 
                      WHERE translation_key=? LIMIT 0,1',
                    array($t['translationKey'])
                );
                if ($rsobj->RecordCount()) {
                    $row = $rsobj->getRows();
                    if (isset($row[0]['translationkey_id'])) {
                        $translationkey_id = $row[0]['translationkey_id'];
                    }
                }
                if ($translationkey_id) {
                    $data['r_translation_key'] = $translationkey_id;
                }
                $rstranskey = $this->dbcon->Execute(
                    'SELECT * FROM t_translations 
                    WHERE r_language_id=? AND r_translation_key=?',
                    array($t['translationLanguageID'], $translationkey_id)
                );
                if ($rstranskey->RecordCount()) {
                    $data['modified_on'] = $date;
                    $rsUpdates = $this->dbcon->GetUpdateSQL($rstranskey, $data);
                    $this->dbcon->Execute($rsUpdates);
                        //Set the status message
                        $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Successfully  updated',
                        // 'sql' => $rstranskey->sql,
                        );
                } else {
                    $data['created_on'] = $date;
                    $rsInserts = $this->dbcon->GetInsertSQL($rstranskey, $data);
                    $this->dbcon->Execute($rsInserts);
                        //Set the status message
                        $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Successfully added',
                        // 'sql' => $rstranskey->sql,
                        );
                }
            }
        }
    }
    
    /**
    * Returns an json obj of update Translations  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    /*public function updateTranslations($params)
    {
        //$date = date('Y-m-d H:i:s');
            echo GET_TRANSLATION_BY_ID.$params['id'].$params['langid'];
            //exit;
            // $rsobj = $this->dbcon->Execute(GET_TRANSLATION_BY_ID,array($params['id'],$params['langid']));

            // if($params['translation_text']){
            // $data['translation_text'] = $params['translation_text'];
            // }
            // if($rsobj->RecordCount()) {    
                // $data['modified_on']=$date;
                // $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                // echo $rsUpdates;
                // $rsUpdate = $this->dbcon->Execute($rsUpdates);            
                // //Set the status message
                // $this->status = array(
                    // "status"            => "success",
                    // "status_code"        => 200,
                    // "status_message"    => "Successfully  updated",
                    // "sql" => $rsobj->sql
                // );            
            // } else {        
                // $data['created_on']=$date;
                // $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                // $rsInsert = $this->dbcon->Execute($rsInserts);            
                // //Set the status message
                // $this->status = array(
                    // "status"            => "success",
                    // "status_code"        => 200,
                    // "status_message"    => "Successfully added",
                    // "sql" => $rsobj->sql
                // );
            // }        
            // //Return the result array    
            // return $this->status;
    }
    */
    /**
    * Returns an json obj of get Translation Language Type  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getTranslationLanguageType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_TRANSLATIONLANGUAGE_TYPE);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get translationLanguage type details success',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'translationkeyTypeDetails' => $userInfo,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get translationLanguage  type details failed',
            'total_records' => 0,
            // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }
    
    /**
    * Returns an json obj of insert Language for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertLanguage($params)
    {
        $update = (isset($params['id'])) ? $params['id'] : 0;
        if ($update != 0) {
            $rsobj = $this->dbcon->Execute(
                UPDATE_LANGUAGE,
                array(
                    $params['manageMenuLanguage'],
                    $params['checkval'],
                    $params['id'],
                    $params['date'] = date('Y-m-d H:i:s')
                )
            );
        } else {
            $rsobj = $this->dbcon->Execute(SET_LANGUAGE, array($params['manageMenuLanguage'], $params['checkval']));
        }
        $lastInsertId = $this->dbcon->Insert_ID();
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'insert_success',
                'status_code' => 200,
                'inserud' => $lastInsertId,
                // 'sql' => $rsobj->sql,
                'status_message' => 'language details added successfully ',
            );
        } else {
            $this->status = $this->error_general;
        }

        return $this->status;
    }
        
    /**
    * Returns an json obj of editLang Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function editLangType($params)
    {
        // write Query and pass editId;
        $rsobj = $this->dbcon->Execute(EDIT_LANGUAGE_TYPE, array($params['id']));
        if ($rsobj->RecordCount()) {
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get language type details success',
            'total_records' => $rsobj->GetRows(),
            // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get language  type details failed',
            );
        }

        return $this->status;
    }
        
    /**
    * Returns an json obj of edit Trans Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function editTransType($params)
    {
        // write Query and pass editId;
            $rsobj = $this->dbcon->Execute(EDIT_TRANSLATION_TYPE, array($params['id']));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Etid  translation type details success',
                'total_records' => $rsobj->GetRows(),
                // 'sql' => $rsobj->sql,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get language  type details failed',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of delete language for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deletelanguage($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_LANGUAGE_ID, $params['languageId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of delete translation for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deletetranslation($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_TRANSLATION_ID, $params['translationId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Language Details for a company.
    * 
    * @return array object object
    */ 
    public function getLanguageDetails()
    {
        $rsobj = $this->dbcon->Execute(GET_LANGUAGE_DETAILS);
       //  error_log(print_r($rsobj,true),3,$path);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get language details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'languageTypeDetails' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get language details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get device settings for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getdevicesettings($param)
    {
        $clubid = isset($param['clubId']) ? $param['clubId'] : 1;
        $UserId = isset($param['UserId']) ? $param['UserId'] : 0;
        $rsobj = $this->dbcon->Execute(GET_DEVICE_MACHINE_DATA, array($clubid, $UserId));
        if ($rsobj->RecordCount()) {
            $detail =   array();
            while (!$rsobj->EOF) {
                $detail[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get machine device details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'devicemachinedetail' => $detail,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get machine device details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'devicemachinedetail' => '',
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Language Active Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getLanguageActiveType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_LANGUAGE_ACTIVE_TYPE);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get language type details success',
                'total_records' => $rsobj->RecordCount(),
                'total_count' => $this->getLastQueryTotalCount(),
                // 'sql' => $rsobj->sql,
                'languageTypeDetails' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get language  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of delete Brand for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deleteBrand($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_BRAND_ID, $params['brandId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
        }

        return $this->status;
    }

    /**
    * Returns an json obj of get Group for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getGroup($params)
    {
        $qry = GET_GROUP;
        $arr = array();
        if (isset($params['isVisible'])) {
            $qry = GET_GROUP_ONLY_VISIBLE;
            if (isset($params['isVisibleUser'])) {
                $qry = GET_GROUP_ONLY_VISIBLE_USER;
                $arr = array($params['userTypeId']);
            }
        }
        $rsobj = $this->dbcon->Execute($qry, $arr);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get group success',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'getGroup' => $userInfo,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get group failed',
            'total_records' => 0,
            // 'sql' => $rsobj->sql,
            'getGroup' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Returns an json obj of get Group Detail  for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */    
    public function getGroupDetail($param)
    {
        $rsobj = $this->dbcon->Execute(GET_BY_GROUP_ID, array($param['groupId']));
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Get group detail success',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'getGroupDetail' => $userInfo,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get group detail failed',
            'total_records' => 0,
            // 'sql' => $rsobj->sql,
            'getGroupDetail' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }

    /**
    * Returns an json obj of insert Group  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function insertGroup($params)
    {
        $update = (isset($params['id'])) ? $params['id'] : 0;
        //$date = date('Y-m-d H:i:s');
        if ($update != 0) {
            // update;
            $this->dbcon->Execute(UPDATE_GROUP, array($params['manageGroupName'], $params['id']));
        } else {
            //insert;
            $this->dbcon->Execute(INSERT_GROUP, array($params['manageGroupName'], $params['loggedUserId']));
        }
        $lastInsertId = $this->dbcon->Insert_ID();
        if ($lastInsertId) {
            $this->status = array(
            'status' => 'parent_exist',
            'status_code' => 1,
            'status_message' => 'Insert Successfully',
            );
        } else {
            $this->status = array(
            'status' => 'parent_exist',
            'status_code' => 0,
            'status_message' => 'error',
            );
        }

        return $this->status;
    }
     
    /**
    * Returns an json obj of delete Group for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    
    public function deleteGroup($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_GROUP_ID, $params['groupId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
            'status' => 'failed delete',
            'status_code' => 0,
            'status_message' => 'error',
            );
        } else {
            $this->status = array(
            'status' => 'Delete Successfully',
            'status_code' => 1,
            'status_message' => 'Delete successfully',
            );
        }

        return $this->status;
    }
        
    /**
    * Returns an json obj of delete menu for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */        
    public function deleteMenu($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_MENU_ID, $params['menuDeleteId']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Ques Group for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getQuesGroup($param)
    {
        if ($param) {
        }
        $rsobj = $this->dbcon->Execute(GET_QUES_GROUP);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getGroupDetail' => $userInfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getBrandDetail' => '',
                  );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Ques Phase for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuesPhase($param)
    {
        if ($param) {
        }
        $rsobj = $this->dbcon->Execute(GET_QUES_PHASE);
        if ($rsobj->RecordCount()) {
            $phaseinfo  =   array();
            while (!$rsobj->EOF) {
                $phaseinfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getPhaseDetail' => $phaseinfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getPhaseDetail' => '',
                  );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Ques Phase Range for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuesPhaseRange($param)
    {
        $rsobj = $this->dbcon->Execute(
            GET_QUES_PHASE_RANGE,
            array($param['defaultlang'], $param['defaultlang'], $param['defaultlang'], $param['defaultlang'])
        );
        if ($rsobj->RecordCount()) {
            $phaserangeinfo =   array();
            while (!$rsobj->EOF) {
                $phaserangeinfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    //"sql" => $rsobj->sql,
                    'phaserangeinfo' => $phaserangeinfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    //"sql" => $rsobj->sql,
                    'phaserangeinfo' => '',
                  );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Education By Group Id for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getEducationByGroupId($param)
    {

        $rsobj = $this->dbcon->Execute(GET_EDUCATION_BY_GROUP, array($param['groupid']));
        if ($rsobj->RecordCount()) {
            $eduinfo    =   array();
            while (!$rsobj->EOF) {
                $eduinfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get education detail success',
                    'total_records' => $rsobj->RecordCount(),
                    //"sql" => $rsobj->sql,
                    'eduinfo' => $eduinfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get education detail failed',
                    'total_records' => 0,
                    //"sql" => $rsobj->sql,
                    'eduinfo' => '',
                  );
        }

        return $this->status;
    }
       
    /**
    * Returns an json obj of get Group Topics for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getGroupTopics($params)
    {
        $qry = GET_GROUP_TOPICS;
        $arr = array($params['groupid']);
        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get menu pages success',
            'total_records' => $rsobj->RecordCount(),
            // 'sql' => $rsobj->sql,
            'getGroupTopics' => $userInfo,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get menu pages success failed',
            'total_records' => 0,
            // 'sql' => $rsobj->sql,
            'getGroupTopics' => '',
            );
        }
        //Return the result array    
        return $this->status;
    }
        
    /**
    * Returns an json obj of get Question By Id  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */        
    public function getQuestionById($params)
    {
        $qry = GET_BY_QUESTION_ID;
        $arr = array($params['id']);
        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get menu pages success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuestionById' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get menu pages success failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getQuestionById' => '',
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get question option by id.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getQuestionOptionById($params)
    {
        $qry = GET_BY_QUESTION_OPTION_ID;
        $arr = array($params['id']);
        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo = array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get menu pages success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuestionOptionById' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get menu pages success failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuestionOptionById' => '',
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Phase Weight age  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getPhaseWeightage($params)
    {
        $qry = GET_QUES_PHASE_WEIGHTAGE;
        $arr = array($params['phaseid'], $params['groupid']);
        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get phase weightage success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getPhaseWeightage' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get phase weightage success failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getPhaseWeightage' => array(),
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Phase Range By Id for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getPhaseRangeById($params)
    {
        $qry = GET_BY_PHASE_RANGE_ID;
        $arr = array($params['id']);
        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo = array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get phase weightage success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getPhaseRangeById' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get phase weightage success failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getPhaseRangeById' => '',
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of delete Question By Id for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function deleteQuestionById($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_QUESION_ID, $params['id']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Page Name for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getPageName($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_PAGE_NAME);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Pagename type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'pagecontentDetails' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get pagename  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of Insert Update Page Content  for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function InsertUpdatePageContent($param)
    {    
	//print_r($param);
		$page_main_id = (isset($param['main_id'])) ? strip_tags($param['main_id']) : '';

        $pagename = (isset($param['pagename'])) ? $param['pagename'] : '';
		
        $pagecntid = (isset($param['pagecntid'])) ? $param['pagecntid'] : '';
        $contentitems = (isset($param['contentitems'])) ? json_decode($param['contentitems']) : array();
        //$title = (isset($param['title'])) ? json_decode($param['title']) : '';
        $title = (isset($param['title'])) ? $param['title'] : '';
        $bannerImg = (isset($param['bannerImg'])) ? $param['bannerImg'] : '';
            //$id = '';
            $pageId = '';
        $rsobj = $this->dbcon->Execute(GET_PAGECONTENT_EXIT, array($pagename));
        if ($rsobj->RecordCount()) {
            $row = $rsobj->getRows();
            if (isset($row[0]['t_pageid'])) {
                $pageId = $row[0]['t_pageid'];
            }
        }
        if ($pagecntid!='') {
			
            $this->dbcon->Execute(UPDATE_PAGE_CONTENT, array($pagecntid));
        }
        if ($pageId) {   
		
            $this->dbcon->Execute(UPDATE_PAGE_CONTENT, array($pageId));
        }
        if($pageId){
			
             $this->dbcon->Execute(UPDATE_PAGE_BANNER,array($bannerImg,$pageId));
        }
		
			
		
		 if(!empty($page_main_id) && $page_main_id!=0){
			$str = "select * from `t_pagecontent_language` where `id`=".$page_main_id;
		
			$res=mysql_query($str);
			$page=mysql_fetch_array($res);
			$l_id = $page['language_id'];
			foreach ($contentitems as  $key => $content)
			{
				$item[$key] = (array) $content;
				$titleval = $item[$key]['title'];
					if($item[$key]['contentlanguageid']==$l_id){
						
						$rsrc   =   $this->dbcon->Execute(
						UPDATE_PAGE_CONTENTS,
						array(
							$pagename,$item[$key]['contentlanguageid'],$item[$key]['value'], $titleval,$page_main_id
						)
						);
						
					}
				}
            //echo $rsrc->sql;
        
		}   
		else{ 
		/* $str = "delete from `t_pagecontent_language` where `t_pageid`=".$pageId;   
			
			$res=mysql_query($str); */
        foreach ($contentitems as  $key => $content) {
            $item[$key] = (array) $content;
            $titleval = $item[$key]['title'];
            $rsrc   =   $this->dbcon->Execute(
                INSERT_PAGE_CONTENT,
                array(
                    $pagename,$item[$key]['contentlanguageid'],$item[$key]['value'], $titleval
                )
            );
            //echo $rsrc->sql;
        }
		}
        $pgid = ($pagecntid) ? $pagecntid : $this->dbcon->Insert_ID();
        if ($pgid) {
            $this->status = array(
                'status' => 'Insert Successfully',
                'status_code' => 1,
                'status_message' => 'Insert successfully',
                );
        } else {
            $this->status = array(
                'status' => 'failed insert',
                'status_code' => 0,
                'status_message' => 'error',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of delete Page Content By Id  for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    public function deletePageContentById($param)
    {
        $pgid = (isset($param['id'])) ? $param['id'] : '';
		$id = (isset($param['c_id'])) ? $param['c_id'] : '';
		
        if ($id) {
            $this->dbcon->Execute(UPDATE_PAGE_CONTENTSS, array($pgid,$id));
        }
        if ($id) {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Insert successfully',
                );
        } else {
            $this->status = array(
                'status' => 'failed insert',
                'status_code' => 0,
                'status_message' => 'error',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Page Content Details for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getPageContentDetails($params)
    {
		
        if ($params) {
        }
        $languageid = 1;
            //echo GET_PAGECONTENT_TYPE.$languageid;
            //$rsobj = $this->dbcon->Execute(GET_PAGECONTENT_TYPE, array($languageid));
			   
            $rsobj = $this->dbcon->Execute(GET_PAGECONTENT_TYPE);
			 //$rsobj = $this->dbcon->Execute(GET_LANGUAGE_PAGECONTENT);
				
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get pagecontent type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'pagecontentdetails' => $userInfo,
                );
        } else {      
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get pagecontent  type details failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Language Page Content Details for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getLanguagePageContentDetails($params)
    {
        if ($params) {
        }
            //echo GET_PAGECONTENT_TYPE.$languageid;
            $rsobj = $this->dbcon->Execute(GET_LANGUAGE_PAGECONTENT);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get pagecontent type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getLanguagePageContentDetails' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get pagecontent  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Page Content By Id  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */    
    public function getPageContentById($params)
    {
		
        $qry = GET_BY_PAGECONT_ID;

        $arr = array($params['id'],$params['c_id']);
        $rsobj = $this->dbcon->Execute($qry, $arr);

        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get menu pages success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getPageContentById' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get menu pages failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'getPageContentById' => '',
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Disease for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getDisease($param)
    {
        if ($param) {
        }
        $rsobj = $this->dbcon->Execute(GET_DISEASE);
        if ($rsobj->RecordCount()) {
            $diseaseinfo    =   array();
            while (!$rsobj->EOF) {
                $diseaseinfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getDisease' => $diseaseinfo,
                );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getDisease' => '',
                  );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of update Disease for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function updateDisease($param)
    {
        $diseasetopicvals = (isset($param['diseasetopicval'])) ? $param['diseasetopicval'] : array();
        $diseasetopicvals = json_decode($diseasetopicvals, true);
        foreach ($diseasetopicvals as $diseasetopicval) {
            $diseaseid = $diseasetopicval['diseaseid'];
            $diseasetopics = $diseasetopicval['value'];
            foreach ($diseasetopics as $diseasetopic) {
                $topicid = $diseasetopic['topicid'];
                $val = $diseasetopic['val'];
                    //echo "$val, $diseaseid, $topicid ---";
                    $this->dbcon->Execute(UPDATE_DISEASE, array($val, $diseaseid, $topicid));
            }
        }

            //if ($id) {
            $this->status = array(
                'status' => 'Insert Successfully',
                'status_code' => 1,
                'status_message' => 'Insert successfully',
            );
            /*
            } else {
            $this->status = array(
                "status" => "failed insert",
                "status_code" => 0,
                "status_message" => "error"
                );
            } */
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Phase Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getPhaseType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_PHASE);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Phase type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getPhaseType' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get pagecontent  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of get Group List Type for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getGroupListType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_GROUPLIST);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get GroupList type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getGroupListType' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get GroupList  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }

    /**
    * Returns an json obj of get Questions Type  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getQuestionsType($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_QUESTIONHINTS);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Questions type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'Questions' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Questions  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
        
    /**
    * Returns an json obj of strength Program Default Id for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function strengthProgramDefaultId($param)
    {
        //echo GET_STRENGTH_PROGRAM_DEFAULT_ID.$$param['companyId'];
        $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM_DEFAULT_ID, array($param['companyId']));
        //print_r($rsobj);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $strengthprgm[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get group detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthprgm' => $strengthprgm,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'strengthprgm' => '',
              );
        }

        return $this->status;
    }
      */
    /**
    * Returns an json obj of Insert Update Question Hints for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function InsertUpdateQuestionHints($params)
    {
        $queshintitems = isset($params['queshintitems']) ? json_decode($params['queshintitems']) : array();
        $queshintid = (isset($params['queshintid'])) ? $params['queshintid'] : 0;
        $date = date('Y-m-d H:i:s');
        if ($queshintid) {
            $this->dbcon->Execute(
                UPDATE_QUES_HINTS,
                array(
                    $params['questionid'],
                    $params['phase_name'], $params['group_name'],
                    $params['noofdays'], $date, $queshintid
                )
            );
        } else {
            $this->dbcon->Execute(
                INSERT_QUES_HINTS,
                array($params['questionid'], $params['phase_name'], $params['group_name'], $params['noofdays'], $date)
            );
        }

        $ques_hint_id = ($queshintid) ? $queshintid : $this->dbcon->Insert_ID();
        if ($ques_hint_id) {
            foreach ($queshintitems as $key => $hints) {
                $item[$key] = (array) $hints;
                $queshintlangeditid = isset($item[$key]['queshintlangeditid']) ? $item[$key]['queshintlangeditid'] : '';
                if ($queshintlangeditid) {
                    $this->dbcon->Execute(
                        UPDATE_QUES_HINT_LANG,
                        array(
                            $ques_hint_id,
                            $item[$key]['queslanguageid'],
                            $item[$key]['value'],
                            $date,
                            $queshintlangeditid
                        )
                    );
                } else {
                    $this->dbcon->Execute(
                        INSERT_QUES_HINT_LANG,
                        array($ques_hint_id, $item[$key]['queslanguageid'], $item[$key]['value'], $date)
                    );
                }
            }
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Question Hints for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuestionHints($params)
    {
        $params['languageid'] = DEFAULT_LANG;
        $rsobj = $this->dbcon->Execute(GET_QUESTION_HINTS, array($params['languageid']));
            //print_r($rsobj);
            if ($rsobj->RecordCount()) {
                $getQuestionHints   =   array();
                while (!$rsobj->EOF) {
                    $getQuestionHints[] = $rsobj->fields;
                    $rsobj->MoveNext();
                }
                $this->status = array(
                    'status' => 'success',
                    'status_code' => 1,
                    'status_message' => 'Get group detail success',
                    'total_records' => $rsobj->RecordCount(),
                    // 'sql' => $rsobj->sql,
                    'getQuestionHints' => $getQuestionHints,
                );
            } else {
                $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'Get group detail failed',
                    'total_records' => 0,
                    // 'sql' => $rsobj->sql,
                    'getQuestionHints' => '',
                  );
            }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Question Hints By Id for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getQuestionHintsById($param)
    {
        $rsobj = $this->dbcon->Execute(GET_BY_QUESTION_HINT_ID, array($param['queshintId']));

        if ($rsobj->RecordCount()) {
            $queshintinfo   =   array();
            while (!$rsobj->EOF) {
                $queshintinfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get group detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getQuestionHintsById' => $queshintinfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' =>0,
                // 'sql' => $rsobj->sql,
                'getQuestionHintsById' => '',
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    /**
    * Returns an json obj of delete Question Hint By Id for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function deleteQuestionHintById($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_BY_QUESION_HINT_ID, $params['id']);
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        } else {
            $this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get User Fit level for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getUserFitlevel($params)
    {
        $rsobj = $this->dbcon->Execute(GET_FITLEVEL_ID, array($params['userid']));
        //print_r($rsobj);
        if ($rsobj->RecordCount()) {
            $pointfitlevel  =   array();
            while (!$rsobj->EOF) {
                $pointfitlevel[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get group detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'pointfitlevel' => $pointfitlevel,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'pointfitlevel' => '',
            );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Reset Users for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getResetUsers($params)
    {
        if ($params) {
        }
            //echo GET_PAGECONTENT_TYPE.$languageid;        
            $userin = (object) array('inobj' => RESET_USERS);
      /*   $rsobj = $this->dbcon->Execute(GET_RESET_USERS, array($userin)); */
		  $rsobj = $this->dbcon->Execute(GET_RESET_USERS);
        if ($rsobj->RecordCount()) {
            $userInfo   =   array();
            while (!$rsobj->EOF) {
                $userInfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get pagecontent type details success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getresetusers' => $userInfo,
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get pagecontent  type details failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                );
        }
            //Return the result array    
            return $this->status;
    }
    
    public function deleteUser($params)
	{
		$userid = $params['id'];
		$this->deleteData($userid,'t_users','user_id');
		
		$this->deleteData($userid,'t_user_test','r_user_id');
		$this->deleteData($userid,'t_user_test_parameter','r_user_id');
		$this->deleteData($userid,'t_testing_measuring_transaction','r_user_id');
		
		$this->deleteData($userid,'t_user_strength_level','r_user_id');
		$this->deleteData($userid,'t_coach_member','r_user_id');
		$this->deleteData($userid,'t_coach_member','r_coach_id');
		$this->deleteData($userid,'t_device_member','r_user_id'); 

		$this->deleteData($userid,'t_mvactual_answers','actmv_userid');
		$this->deleteData($userid,'t_mv_data','user_id');

		$this->deleteData($userid,'t_mind_activity','user_id');
		$this->deleteData($userid,'t_mind_activity','coach_id');
		$this->deleteData($userid,'t_medical_jointproblem','r_user_id');
		$this->deleteData($userid,'t_medical_muscleinjury','r_user_id');
		$this->deleteData($userid,'t_medical_riskfactor','r_user_id');
		$this->deleteData($userid,'t_member_medicalinfo','r_user_id');
		$this->deleteData($userid,'t_member_medicalinfo','r_user_id');
		$this->deleteData($userid,'t_messages','sender_id');
		$this->deleteData($userid,'t_messages','receiver_id');
		$this->deleteData($userid,'t_notification_message_users','user_id');
		$this->deleteData($userid,'t_personalinfo','user_id');
		$this->deleteData($userid,'t_plan_member','r_user_id');
		$this->deleteData($userid,'t_points_achieve','r_user_id');
		$this->deleteData($userid,'t_ques_phase_user','r_user_id');
		$this->deleteData($userid,'t_save_credits','user_id');
		$this->deleteData($userid,'t_sports_specific_info','r_user_id');
		
		$stengthTestId = $this->getData($userid,'strength_user_test_id','r_user_id','strength_user_test_id');
		
		$this->deleteData($userid,'t_strength_user_test','r_user_id');
		if(!empty($stengthTestId))
		{
			$this->deleteRelationData($stengthTestId,'t_strength_user_test_activity','r_strength_user_test_id');
			$this->deleteRelationData($stengthTestId,'t_strength_user_test_week','r_strength_user_test_id');
		}
		
		$this->deleteData($userid,'t_testing_measuring','r_user_id');
		$this->deleteData($userid,'t_testing_measuring_transaction','r_user_id');
		$this->deleteData($userid,'t_test_overall_result_level','r_user_id');
		$this->deleteData($userid,'t_test_result','r_user_id');
		
		$machineId = $this->getData($userid,'t_training_machines_client','client_id','t_training_machine_id');
		
		$this->deleteData($userid,'t_training_machines_client','client_id');
		if(!empty($machineId))
		{
			$this->deleteRelationData($machineId,'t_training_machine_client_paired_devices','t_training_machine_clients_id');
			$this->deleteRelationData($machineId,'t_training_machine_data','t_training_machine_clients_id');
		}
		$this->deleteData($userid,'t_training_points_achieved','f_userid');
		$this->deleteData($userid,'t_training_points_transaction','r_user_id');
		$this->deleteData($userid,'t_training_workload','r_user_id');
		$this->deleteData($userid,'t_voucher','user_id');
		//die;
		return true;
		
	}
	
	public function deleteData($id, $table,$field)
	{
		$statement =   "delete from ".$table." where `".$field."` = ?";
		//echo $statement."<br/>";
		$this->dbcon->Execute($statement,array($id));
		return true;
	}
	
	public function deleteRelationData($relationids, $table,$field)
	{
		$statement =   "delete from ".$table." where `".$field."` in(?)";
		$ids = implode(",",$relationids);
		$rs = $this->dbcon->Execute($statement,array($ids));
		return true;
	}
	
	public function getData($id, $table,$field,$getField)
	{
		$ids  = array();
		$statement  =   "SELECT * FROM ".$table." WHERE `".$field."` = ? ";
		$obj = $this->dbcon->Execute($statement,array($id));
		if(isset($obj->fields) && $obj->RecordCount() > 0) 
		{
			while (!$obj->EOF) 
			{
				$ids[] = $obj->fields[$getField];
				$obj->MoveNext();
			}
		}
		return $ids;
	}
    
    /**
    * Returns an json obj of Insert Message Email for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
	
	function getMachine_groupData(){
		$sql = "SELECT * FROM t_machine_group";
		$result = mysql_query($sql);
		//if ($result->num_rows > 0) {
			// output data of each row
			while($row = mysql_fetch_array($result)) {
				return $row;
         
		}
	} 
	
	
	
    public function InsertMessageEmail($params)
    {
        $text = (isset($params['text'])) ? $params['text'] : '';
        $currentid = (isset($params['currentid'])) ? $params['currentid'] : 0;
        $mid = (isset($params['mid'])) ? json_decode($params['mid']) : array();
        $clubid = (isset($params['clubid'])) ? $params['clubid'] : 0;
        $msgtemplate = (isset($params['msgtemplate'])) ? $params['msgtemplate'] : 0;
        $date = date('Y-m-d H:i:s');
        if ($msgtemplate == 3) {
            //CMT BY MK TO SN > Change 3 Change TO Constant
                foreach ($mid as $id) {
                    $this->dbcon->Execute(
                        INSERT_MESSAGE_EMAIL,
                        array($text, $currentid, $msgtemplate, $id, $clubid, $date)
                    );
                }
        } else {
            foreach ($mid as $index => $options) {
                $slctitem[$index] = (array) $options;
                $this->dbcon->Execute(
                    INSERT_MESSAGE_EMAIL,
                    array($text, $currentid, $msgtemplate, $slctitem[$index]['mid'], $clubid, $date)
                );
            }
        }
        $lastInsertId = $this->dbcon->Insert_ID();
        if ($lastInsertId) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Insert Successfully',
                );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'error',
                );
        }

        return $this->status;
    }
    
    /**
    * Returns an json obj of get Calender Details  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    public function getCalenderDetails($params)
    {
        $clubid = $params['clubId'];
        $userId = $params['userId'];
        //$company_id    =    (
        //isset($params['companyId'])
        // and  $params['companyId']!=''
        //) ? $params['companyId']:REGISTER_CLIENT_COMPANY;
        $circute_membercnt = $this->getClubSettingByKey(
                array('clubid' => $clubid, 'key' => 'count_circute_members')
        );
        $mixed_membercnt = $this->getClubSettingByKey(
            array('clubid' => $clubid, 'key' => 'count_mixed_members')
        );
        $programsrs = $this->dbcon->Execute(
            GET_PROGRAMS_LIST, array($clubid)
        );
        $programs = array();
        $programscount = 0;
        if ($programsrs->RecordCount()) {
            $programscount = $programsrs->RecordCount();
            $programs = $programsrs->GetRows();
        }
        $rsobj = $this->dbcon->Execute(GET_CALENTER_DETAILS, array($userId));
        if ($rsobj->RecordCount()) {
            $Calender   =   array();
            while (!$rsobj->EOF) {
                $Calender[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Get Calender Details',
            'calendernotecount' => $rsobj->RecordCount(),
            'calendernotes' => $Calender,
            );
        } else {
            $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'Get Calender Details Not Found.',
            'calendernotecount' => 0,
            'calendernotes' => array(),
            );
        }
        //Return the result array
        $this->status['programscount'] = $programscount;
        $this->status['programs'] = $programs;
        $this->status['memberscount'] = array('circutecnt' => $circute_membercnt, 'mixedcnt' => $mixed_membercnt);

        return $this->status;
    }
        
    
    /**
    * Returns an json obj of Save Calender Note for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function SaveCalenderNote($params)
    {
        $caldrid = (isset($params['id'])) ? $params['id'] : 0;
        $text = (isset($params['text'])) ? $params['text'] : '';
        $start_date = (isset($params['start_date'])) ? $params['start_date'] : '';
        $end_date = (isset($params['end_date'])) ? $params['end_date'] : '';
        $program = (isset($params['program'])) ? $params['program'] : 0;
        $memberscnt = (isset($params['memberscnt'])) ? $params['memberscnt'] : -1;
        $userId = $params['userId'];
        $calenderrs = $this->dbcon->Execute(GET_CALENDAR_BYID, array($caldrid));
        $data['r_user_id'] = $userId;
        $data['r_program_id'] = $program;
        if (isset($params['text'])) {
            $data['text'] = $text;
        }
        if ($start_date != '') {
            $data['start_period'] = $start_date;
        }
        if ($end_date != '') {
            $data['end_period'] = $end_date;
        }
        if ($memberscnt != -1) {
            $data['max_members'] = $memberscnt;
        }
        if (isset($params['isDelete'])) {
            $data['is_deleted'] = $params['isDelete'];
        }
        $data['modified_dt'] = date('Y-m-d H:i:s');
        $staus = 'error';
        $stauscode = 0;
        $stausmessage = 'Insert/Update Failed';
        if ($calenderrs->RecordCount()) {
            $updateSql = $this->dbcon->GetUpdateSQL($calenderrs, $data);
            $this->dbcon->Execute($updateSql);
            $stauscode = 1;
            $stausmessage = 'Updated Successfully';
            $staus = 'success';
        } else {
            $data['created_dt'] = date('Y-m-d H:i:s');
            $insertSql = $this->dbcon->GetInsertSQL($calenderrs, $data);
            $this->dbcon->Execute($insertSql);
            $lastInsertId = $this->dbcon->Insert_ID();
            if ($lastInsertId) {
                $staus = 'success';
                $stauscode = 1;
                $stausmessage = 'Inserted Successfully';
            }
        }
        $this->status = array(
            'status' => $staus,
            'status_code' => $stauscode,
            'status_message' => $stausmessage,
            );

        return $this->status;
    }
    
    /**
    * Returns an json obj of getClub Setting By Key  for a company.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function getClubSettingByKey($params)
    {
        $rsobj = $this->dbcon->Execute(GET_CLUBSETTINGS_VLAUE, array($params['clubid'], $params['key']));
        $value = '';
        if ($rsobj->RecordCount()) {
            $value = $rsobj->fields['settings_value'];
        }

        return $value;
    }
    /*PK Get Goal Details(20160503)*/
    public function getGoal($param)
    {
       
        $rsobj = $this->dbcon->Execute(GET_GOAL);
        if ($rsobj->RecordCount()) {
            $goalinfo   =   array();
            while (!$rsobj->EOF) {
                $goalinfo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get goal detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getGoal' => $goalinfo,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getDisease' => '',
              );
        }

        return $this->status;
    }
        
    /**
    * Returns an json obj of updateGoal for a company.
    * 
    * @param array $param service parameter, Search arguments 
    *
    * @return array object object
    */ 
    public function updateGoal($param)
    {
        $goaltopicvals = (isset($param['goaltopicval'])) ? $param['goaltopicval'] : array();
        $goaltopicvals = json_decode($goaltopicvals, true);
        foreach ($goaltopicvals as $goaltopicval) {
            $goalid = $goaltopicval['goalid'];
            $goaltopics = $goaltopicval['value'];
            foreach ($goaltopics as $goaltopic) {
                $topicid = $goaltopic['topicid'];
                $val = $goaltopic['val'];
                    //$vals = (isset($goaltopic["val"]))?$goaltopic["val"]:0;
                    $this->dbcon->Execute(UPDATE_GOAL, array($val, $goalid, $topicid));
            }
        }
        $this->status = array(
                'status' => 'Insert Successfully',
                'status_code' => 1,
                'status_message' => 'Insert successfully',
            );

        return $this->status;
    }
	/////////////////////added by gaurav thakur , function to add new key for transaltion///////////////
	public function insertLanguageKey($params)
	{
		$key = $params['translation_key']."_".str_replace(" ","_",strtoupper($params['translation_key_description']));
		$des = $params['translation_key_description'];
		$date = date('Y-m-d H:i:s');
		
		$this->dbcon->Execute("insert into t_translation_key (translation_key,transaction_key_desc,created_on,modified_on) values('$key','$des','$date','$date')");
		
		return true;
	}
	public function getMachineGroup(){
		 $rsobj = $this->dbcon->Execute(GET_MACHINE_GROUP);
		 
		// die;    
        if ($rsobj->RecordCount()) {
            $group   =   array();
            while (!$rsobj->EOF) {
				
               $group[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get goal detail success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'groups' => $group,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get group detail failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'getDisease' => '',
              );
        }
                    
        return $this->status;
		
	} 
	
	/***********for program list***********/
	public function programlist($params){
		 $rsobj = $this->dbcon->Execute(GET_PROGRAM_LIST);
		 
		// die;    
        if ($rsobj->RecordCount()) {
            $group   =   array();
            while (!$rsobj->EOF) {
				
               $program[] = $rsobj->fields;
			   
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get program detail success',
                'total_records' => $rsobj->RecordCount(),
                 'sql' => $rsobj->sql,
                'program' => $program,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get program detail failed',
                'total_records' => $rsobj->RecordCount(),
                 'sql' => $rsobj->sql,
              );
        }
          
        return $this->status;
		
	}   
	/************program detail according to id*******/
	

	public function getprogramDetailbyId($params){
		
		$program_id = $params['programId'];
		 $rsobj = $this->dbcon->Execute(GET_PROGRAM_LIST_BY_ID,array($program_id));
		 
		// die;    
        if ($rsobj->RecordCount()) {
           $program = $rsobj->fields;
			// for($i=1;$i<=12;$i++){
            $program_id = $rsobj->fields['strength_program_id'];
			$week1 = $this->getweek_schedule($program_id,1);
			$week2 = $this->getweek_schedule($program_id,2);
			$week3 = $this->getweek_schedule($program_id,3);
			$week4 = $this->getweek_schedule($program_id,4);
			$week5 = $this->getweek_schedule($program_id,5);
			$week6 = $this->getweek_schedule($program_id,6);
			$week7 = $this->getweek_schedule($program_id,7);
			$week8 = $this->getweek_schedule($program_id,8);
			$week9 = $this->getweek_schedule($program_id,9);
			$week10 = $this->getweek_schedule($program_id,10);
			$week11 = $this->getweek_schedule($program_id,11);
			$week12 = $this->getweek_schedule($program_id,12);
			// }
			
			//print_r($week);
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get program detail success',
                'total_records' => $rsobj->RecordCount(),
                 'sql' => $rsobj->sql,
                'program' => $program,
				'week1'=>$week1,
				'week2'=>$week2,
				'week3'=>$week3,
				'week4'=>$week4,
				'week5'=>$week5,
				'week6'=>$week6,
				'week7'=>$week7,
				'week8'=>$week8,
				'week9'=>$week9,
				'week10'=>$week10,
				'week11'=>$week11,
				'week12'=>$week12,
				
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get program detail failed',
                'total_records' => $rsobj->RecordCount(),
                 'sql' => $rsobj->sql,
              );
        }
          
        return $this->status;
		
	} 
	public function getweek_schedule($program_id,$week){
		$str = "select * from t_strength_program_week where r_strength_pgmid=".$program_id." AND r_week=".$week;
		$row = mysql_query($str);
		$data = mysql_fetch_assoc($row);
		return $data;
	}
	/***********to save strength program in the backoffice****/
public function saveStrengthProgram($params){
			
			
			if(isset($params['strength_program_id'])){
				 $strength_id = $params['strength_program_id'];
				
			}
			else{
				$strength_id = 0;
			}
			$t_english = (isset($params['title_english'])) ? $params['title_english'] : 0;
			
			$t_dutch = (isset($params['title_dutch'])) ? $params['title_dutch'] : 0;
			
			$type = (isset($params['program_type'])) ? $params['program_type'] : 0;
			
			$default = (isset($params['status_id'])) ? $params['status_id'] : 0;
			$user_id = (isset($params['user_id'])) ? $params['user_id'] : 0;
			$clubid = (isset($params['club'])) ? $params['club'] : 0;
			$company_id = (isset($params['company_id'])) ? $params['company_id'] : 0;
		 $rsobj = $this->dbcon->Execute(GET_STRENGTH_PROGRAM,array($strength_id));
		 
        if ($rsobj->RecordCount()) {
           $modified_on = date('Y-m-d h:i:s');
		   $modified_by = $user_id;
        $str="update t_strength_program set title_english='$t_english',title_dutch='$t_dutch',type=$type,f_isdefault=$default,r_company_id=$company_id,modified_date='$modified_on',modified_by=$modified_by where strength_program_id=$strength_id";
		  mysql_query($str); 
		//  $str1="update t_strength_program set `r_week`='$t_english',`series`='$t_dutch',`reps`=$type,`time_sec`=$default,`strength`=$default,`pause`=$default,`modified_on`='$modified_on' where `strength_machine_id` = $strength_id";
		 // mysql_query($str1);
		 $str1 = "delete from t_strength_program_week where r_strength_pgmid=".$strength_id;
		mysql_query($str1);
		  for($i=1;$i<=12;$i++){
				$week = $i;
				$series = (isset($params['series'.$i]) && $params['series'.$i]!='')?$params['series'.$i]:0;
				$Reps = (isset($params['Reps'.$i]) && $params['Reps'.$i]!='')?$params['Reps'.$i]:0;
				$Time = (isset($params['Time'.$i]) && $params['Time'.$i]!='')?$params['Time'.$i]:0;
				$strength = (isset($params['strenght'.$i]) && $params['strenght'.$i]!='')?$params['strenght'.$i]:0;
				$Pause = (isset($params['Pause'.$i]) && $params['Pause'.$i]!='')?$params['Pause'.$i]:0;
			/* $series = $params['series'.$i];
                $Reps = $params['Reps'.$i];
                $Time = $params['Time'.$i];
                $strength = $params['strenght'.$i];
                $Pause = $params['Pause'.$i]; */
		$str2="insert into t_strength_program_week (`r_strength_pgmid`,`r_week`,`series`,`reps`,`time_sec`,`strength`,`pause`,`modified_on`) values ($strength_id,$week,$series,$Reps,$Time,$strength,$Pause,'$modified_on')";
        
        
			mysql_query($str2);
			
			 }
           
        } else {
			//echo "hello";
			$created_on = date('Y-m-d h:i:s');
			$created_by = $user_id;
			$str2 ="select * from t_strength_program where title_english='$t_english' and  type=".$type;
			
			$res = mysql_query($str2);
			$row = mysql_fetch_array($res);
			if(empty($row)){
			$str="insert into t_strength_program (`title_english`,`title_dutch`,`type`,`r_company_id`,`f_isdefault`,`created_by`,`created_date`) values ('$t_english','$t_dutch',$type,$company_id,$default,$created_by,'$created_on')";
	
			mysql_query($str);
			 $lastInsertId =  mysql_insert_id();
			 for($i=1;$i<=12;$i++){
				$week = $i;
               
               $series = (isset($params['series'.$i]) && $params['series'.$i]!='')?$params['series'.$i]:0;
				$Reps = (isset($params['Reps'.$i]) && $params['Reps'.$i]!='')?$params['Reps'.$i]:0;
				$Time = (isset($params['Time'.$i]) && $params['Time'.$i]!='')?$params['Time'.$i]:0;
				$strength = (isset($params['strenght'.$i]) && $params['strenght'.$i]!='')?$params['strenght'.$i]:0;
				$Pause = (isset($params['Pause'.$i]) && $params['Pause'.$i]!='')?$params['Pause'.$i]:0;
				
			
			 $str1="insert into t_strength_program_week (`r_strength_pgmid`,`r_week`,`series`,`reps`,`time_sec`,`strength`,`pause`,`created_on`) values ($lastInsertId,$week,$series,$Reps,$Time,$strength,$Pause,'$created_on')";
         
			mysql_query($str1);
            
			 }
			}
			else{
				 $lastInsertId =  0;
			}
		}
		
			//mysql_query($str1);
             if(isset($lastInsertId) && $lastInsertId>0) {
               // echo "hello";
				 $this->status = array(
            'status' => 'success',
            'status_code' => 1,
            'status_message' => 'Inserted Successfully',
			'insert_id' => $lastInsertId,
            );
            }
			else if(isset($lastInsertId) && $lastInsertId==0){
			
            $this->status = array(
                'status' => 'parent_exist',
                'status_code' => 2,
                'status_message' => 'Already Exist',
                );
			}
			else {
			
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'error',
                );
			}
        
       
//print_r($this->status);
//die;
        return $this->status;
		
	} 
public function deleteProgram($params)
    {
		
        $rsobj = $this->dbcon->Execute(DELETE_BY_PROGRAM_ID, $params['programId']);
		$str = "UPDATE `t_strength_program_week` SET f_isdelete=1 WHERE `r_strength_pgmid`=".$params['programId'];
		$update = mysql_query($str);
        if ($update) {
			$this->status = array(
                'status' => 'Delete Successfully',
                'status_code' => 1,
                'status_message' => 'Delete successfully',
                );
            
        } else {
           $this->status = array(
                'status' => 'failed delete',
                'status_code' => 0,
                'status_message' => 'error',
                );
        }

        return $this->status;
    }
	
	public function gettimeslotbyId($params)
    {
		$id = $params['timeslot_id'];
		$str = "select * from t_time_slots where id=".$id;
		$row = mysql_query($str);
       $result = mysql_fetch_assoc($row);

        return $result;
    }
	public function updatetimeslot($params)
    {
		
		$id = $params['id'];
		$lesson = $params['lesson'];
		$str = "update t_time_slots set lesson='$lesson' where id=$id";
		$update = mysql_query($str);
		if($update){
       $this->status = array(
                'status' => 'update Successfully',
                'status_code' => 1,
                'status_message' => 'update successfully',
                );
		}
		return $this->status;
    }
	
} //Class End.
;
