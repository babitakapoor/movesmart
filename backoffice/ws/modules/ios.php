<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package IOS
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle ios related actions.
 */
require_once SQL_PATH.DS.'ios.php';
 /**
 * Class for functions to handle ios related actions.
 
 * @category Modules
 
 * @package IOS
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class iosModel
{
 
    /**
    * This function is to get members list for a test.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMemberListForTest($params)
    {
        //$status = array();
        $membersListTestToDo = array();
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $qry = GET_MEMBERLIST_FOR_TEST." AND usertest.status IN(0,10,20) AND users.r_status_id = '5' ";
        if (isset($params['userId'])) {
            $qry .= ' AND users.user_id='.$params['userId'];
        }

        $qry .= ' GROUP BY users.user_id';

        $rsobj = $this->dbcon->Execute($qry, array($companyId, $clubId));
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Members List for Test are successfully retrieved',
        );

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $membersListTestToDo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No results found!',
            );
        }

        //Return the result array
        return array(
            'membersList' => array(
                'member' => $membersListTestToDo,
                'status' => $this->status,

            ),
            /*'sql' => array('sql' => $rsobj->sql)* /
        );
    }

    
    /**
    * This function is to get members list for a training.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMemberListForTraining($params)
    {
        //$status = array();
        $memLstTrainingToDo = array();
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $rsobj = $this->dbcon->Execute(
            GET_MEMBERLIST_FOR_TRAINING,
            array($params['start_time_duration'], $params['end_time_duration'], $companyId, $clubId)
        );

        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Members List for Training are successfully retrieved',
        );

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $memLstTrainingToDo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No results found!',
            );
        }

        //Return the result array
        return array(
            'membersList' => array(
                'member' => $memLstTrainingToDo,
                'status' => $this->status,

            ),
        );
    }


    /**
    * This function is to insert members for a test.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    *
    public function insertMemberForTest($params)
    {
        //$status = array();
        $insertMemberForTest = array();
        $updatePointSql = '';
        //Get TestId for TestName
        $getTestId = $this->dbcon->Execute(GET_TEST_DETAILS, array($params['testName']));
        //End
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'r_user_id' => isset($params['memberId']) ? $params['memberId'] : '',
            'user_test_id' => isset($params['userTestId']) ? $params['userTestId'] : '',
            'r_test_id' => isset($getTestId->fields['test_id']) ? $getTestId->fields['test_id'] : 1,
            'test_start_date' => isset($params['testDate']) ? $params['testDate'] : $dateTime,
             'weight' => isset($params['weight']) ? $params['weight'] : '',
             'r_equipment_id' => isset($params['cycleId']) ? $params['cycleId'] : '',
             'status' => isset($params['status']) ? $params['status'] : '',
             'reason' => isset($params['reason']) ? $params['reason'] : '',
             'kcal' => isset($params['kcal']) ? $params['kcal'] : '',

        );
        if ($data['user_test_id'] != '' && ctype_digit($data['user_test_id'])) {
            $rsobj = $this->dbcon->Execute(GET_MEMBERLIST_INSERTFORTEST_IOSQUERY, array($data['user_test_id']));
            if ($rsobj->RecordCount()) {

                //Auto interrupt the same device assigned for member for the same club
                $arrStatus = array(10, 20, 30);
                if (in_array($params['status'], $arrStatus)) {
                    $arr = $rsobj->fields;
                    $qryUpdate = 'UPDATE `t_user_test` SET `r_equipment_id`=0 WHERE status IN(0,10,20)
                            AND `r_club_id`='.$arr['r_club_id'].' AND `r_equipment_id`='.$params['cycleId'].'
                            AND `user_test_id` != '.$params['userTestId'];
                    $this->dbcon->Execute($qryUpdate);
                }
                //Auto interrupt the same device assigned for member for the same club

                $data['modified_by'] = 1;
                $data['modified_date'] = $dateTime;
                /*If test status completed(3) , test end date should be captured.* /
                if (isset($params['status']) && $params['status'] == '3') {
                    $data['test_end_date'] = $dateTime;
                }

                if (
                    isset($params['loadValue'])
                    && $params['loadValue'] != ''
                    && $params['getLoadValues'] == true
                ) {
                    $data['load_value'] = $params['loadValue'];
                    $params['loadValue'] = json_encode($params['loadValue']);
                    $updatePointSql = "UPDATE t_user_test SET load_value='".$params['loadValue']."'
                        WHERE user_test_id='".$params['userTestId']."'";
                    $this->dbcon->Execute($updatePointSql);
                }
                //echo "<pre>"; print_r($params); echo "</pre>";die;                
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                $messge = 'Successfully Updated';
            } else {
                $data['created_by'] = 1;
                $data['created_date'] = $dateTime;
                $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                $this->dbcon->Execute($rsInserts);
                $messge = 'Successfully Added';
            }
        } else {
            $messge = 'User Test ID Required';
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => $messge,
            'updatesql' => $updatePointSql,
        );
        //Return the result array
        return array(
            'insertMemberForTestList' => array(
                'insertMember' => $insertMemberForTest,
                'status' => $this->status,
                // 'sql' => $rsobj->sql,

            ),
        );
    }

    /**
    * This function is get heart rate.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getHeartRate($params)
    {
        //$status = array();
        $getHeartRateList = array();

        $userId = isset($params['userId']) ? $params['userId'] : '';
        $heartRate = isset($params['heartRate']) ? $params['heartRate'] : '';
        $dateTime = date('Y-m-d H:i:s');

        $testIdLastValue = array();
        $rsGetTest = $this->dbcon->Execute(GET_GETUSERLASTTEST_QUERY, $userId);
        if ($rsGetTest->RecordCount()) {
            while (!$rsGetTest->EOF) {
                $testIdLastValue[] = $rsGetTest->fields;
                $rsGetTest->MoveNext();
            }
        }

        $testId = $testIdLastValue[0]['r_test_id'];
        $rsobj = $this->dbcon->Execute(GET_INSERTHERTRATEFORTEST_IOSQUERY, array($userId, $testId));
        if ($rsobj->RecordCount()) {
            $data = array(
                'r_test_id' => $testId,
                'r_user_id' => $userId,
                'hrm' => $heartRate,
                'created_by' => 1,
                'created_date' => $dateTime,
            );
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
        } else {
            $data = array(
                'r_test_id' => $testId,
                'r_user_id' => $userId,
                'hrm' => $heartRate,
                'created_by' => 1,
                'created_date' => $dateTime,
            );
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Heart Rate Successfully added',
        );
        //Return the result array
        return array(
            'getHeartRateList' => array(
                'getHeartRate' => $getHeartRateList,
                'status' => $this->status,

            ),
        );
    }

  /**
 * Insert Member Test Process Details.
 * 
 * param string $params 
 * 
 * return array
 * access public
 * author Murugeswaran N
 * 
 * Created Date  : Sept-17-2014
 * Modified Date : Sept-17-2014
 * /
    // Query string will be in this Format:{
      "mod":"ios","method":"insertMemberTestProcessDetails","param":{
      "memberId":"1","machineId":"1","equipmentId":"1","test_status":"1","test_date":"2014-09-17","testLevel":"f"
      }
     }
    public function insertMemberTestProcessDetails($params)
    {
        //$status = array();
        //Get Person Id For Member
        $rsobj = $this->dbcon->Execute(GET_MEMBER_PERSONID, array($params['r_user_id']));
        $data = array(
                'r_user_id' => isset($params['r_user_id']) ? $params['r_user_id'] : '',
                'person_id' => isset($rsobj->fields['person_id']) ? $rsobj->fields['person_id'] : 0,
                'r_machine_id' => isset($params['r_machine_id']) ? $params['r_machine_id'] : '',
                'r_equipment_id' => isset($params['r_equipment_id']) ? $params['r_equipment_id'] : '',
                'test_status' => isset($params['test_status']) ? $params['test_status'] : '',
                'test_date' => isset($params['test_date']) ? $params['test_date'] : '',
                'test_level' => isset($params['test_level']) ? $params['test_level'] : '',
            );
        $dateTime = date('Y-m-d H:i:s');
        $rsobj = $this->dbcon->Execute(
             GET_MEMBER_TEST_PROCESS_DETAILS,
             array($data['r_user_id'], $data['r_machine_id'], $data['r_equipment_id'])
        );
        if ($rsobj->RecordCount()) {
            $data['modified_by'] = 1;
            $data['modified_date'] = $dateTime;
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $messge = 'Successfully Updated';
        } else {
            $data['created_by'] = 1;
            $data['created_date'] = $dateTime;
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $messge = 'Successfully Added';
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => $messge,
        );
        //Return the result array
        return array(
            'insertMemberTestProcess' => array(
                'status' => $this->status,
            ),
        );
    }

   
    /**
    * This function is to get test levels.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getTestLevels($params)
    {
        //$status = array();
        $getTestLevelsList = array();
        $loadValues = array();
        $memberId = isset($params['memberId']) ? $params['memberId'] : '';
        $rsobj = $this->dbcon->Execute(GET_TESTLEVEL_IOSQUERY, array($memberId));
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Test To Do List are successfully retrieved',
        );
        if ($rsobj && $rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $getTestLevelsList[] = $rsobj->fields;
                $testLevel[] = $rsobj->fields['test_level'];
                $loadValues[] = $rsobj->fields['load_value'];
                $rsobj->MoveNext();
            }
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No results found!',
            );
        }

        //Return the result array
        return array(
            'Response' => array(
                'initialParameter' => $getTestLevelsList,
                'testLevel' => $testLevel,
                'loadValue' => $loadValues,
                'status' => $this->status,
            ),
        );
    }

    
    /**
    * This function is to Add or update cool down history.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return void
    * /
    public function insertCoolDownHistory($params)
    {
        $dateTime = date('Y-m-d H:i:s');

        //Insert new history row
        if ($params['insertFlag'] == 1) {
            $rsobj = $this->dbcon->Execute(COOL_DOWN_TEST_EXIST, array($params['userTestId']));

            $data = array(
                'r_user_test_id' => isset($params['userTestId']) ? $params['userTestId'] : '',
                'heart_rate_with_cool_down' => isset($params['coolDownHR']) ? $params['coolDownHR'] : '',
                'cool_down_start_time' => isset($params['currentTestTime']) ? $params['currentTestTime'] : '',
                'exact_cool_down_start_date' => $dateTime,
            );
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
        } elseif ($params['insertFlag'] == 0) { 
            /* Update latest history row * /

            //Get latest history id by test id
            $rsobj = $this->dbcon->Execute(COOL_DOWN_HISTORY_ID_EXIST, array($params['userTestId']));

            //Update latest test cool down end status
            if ($rsobj->RecordCount() > 0) {
                $row = $rsobj->fields;
                $userTestId = $row['history_id'];

                if ($userTestId > 0) {
                    $rsobj = $this->dbcon->Execute(COOL_DOWN_BY_HISTORY_ID, array($userTestId));

                    $data = array(
                        'cool_down_stop_time' => isset($params['currentTestTime']) ? $params['currentTestTime'] : '',
                        'exact_cool_down_stop_date' => $dateTime,
                    );
                    $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                    $this->dbcon->Execute($rsUpdates);
                }
            }
            //Update latest test cool down end status
        }
    }

    /**
    * This function is to insert heart rate.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function insertHeartRate($params)
    {
        //$status = array();
        $insertHeartRate = array();
        $testStatus = '';

        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'user_test_id' => isset($params['userTestId']) ? $params['userTestId'] : '',
            'current_test_time' => isset($params['currentTestTime']) ? $params['currentTestTime'] : '',
            'r_equipment_id' => isset($params['equipmentId']) ? $params['equipmentId'] : '',
            'kcal' => isset($params['kcal']) ? $params['kcal'] : '',
        );
        if ($data['user_test_id'] != '') {
            $rsobj = $this->dbcon->Execute(GET_USERTESTID_INSERTHRM_IOSQUERY, array($data['user_test_id']));
            if ($rsobj->RecordCount()) {

                //Update cool down status for test id begins
                if ($params['coolDownStatus'] == 0) {
                    $data['heart_rate_without_cool_down'] = $params['heartRateValue'];
                    $data['heart_rate_value'] = $params['heartRateValue'];
                } elseif ($params['coolDownStatus'] == 1) {
                    $data['cool_down_start_time'] = $params['currentTestTime'];
                    $data['heart_rate_with_cool_down'] = $params['coolDownHR'];

                    //Insert cool down history 
                    $params['insertFlag'] = 1;
                    $this->insertCoolDownHistory($params);
                } elseif ($params['coolDownStatus'] == 2) {
                    $data['heart_rate_with_cool_down'] = $params['coolDownHR'];
                } elseif ($params['coolDownStatus'] == 3) {
                    $data['cool_down_stop_time'] = $params['currentTestTime'];
                    $data['heart_rate_with_cool_down'] = $params['coolDownHR'];
                    $data['heart_rate_value'] = $params['heartRateValue'];

                    //Update cool down history 
                    $params['insertFlag'] = 0;
                    $this->insertCoolDownHistory($params);
                }

                //Update cool down status for test id ends

                $testStatus = $rsobj->fields['status'];
                $rpm = $rsobj->fields['rpm'];
                $data['modified_by'] = 1;
                $data['modified_date'] = $dateTime;
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                $messge = 'Successfully Updated';
            } else {
                $rpm = '';
                $messge = 'User Test ID not found.';
            }
        } else {
            $rpm = '';
            $messge = 'User Test ID Required';
        }
        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => $messge,
        );
        //Return the result array
        return array(
            'insertHeartRateList' => array(
                'insertHeartRate' => $insertHeartRate,
                'status' => $this->status,
                'rpm' => $rpm,
                'rpm_group' => $rsobj->fields['rpm_group'],
                'testStatus' => $testStatus,
            ),
        );
    }

    /**
    * This function is to get test info for coach.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getTestInforForCoach($params)
    {
        //$status = array();
        //$getTestInforForCoach = array();
        $testInfoforCoachList = array();
        $members = array();
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $qry = GET_TESTINFOFORCOACH_IOSQUERY;

        if (isset($params['userId'])) {
            $qry .= ' AND u.`user_id`='.$params['userId'];
        }

        $rsobj = $this->dbcon->Execute($qry, array($companyId, $clubId));
        //Set the status message
        if ($rsobj->RecordCount()) {
            $countStart = 0;
            while (!$rsobj->EOF) {
                if ($countStart == 0) {
                    $testInfoforCoachList[0] = array(
                                                'clubId' => $rsobj->fields['club_id'],
                                                'clubName' => $rsobj->fields['club_name'],
                        );
                }
                    //To cool down status
                    $heartRateValue = $rsobj->fields['heart_rate_value'];
                if ($rsobj->fields['cool_down_start_time'] > 0 && $rsobj->fields['cool_down_stop_time'] == 0) {
                    $heartRateValue = (
                        $rsobj->fields['heart_rate_value'] != ''
                    ) ? ($rsobj->fields['heart_rate_value'].'_'.$rsobj->fields['heart_rate_with_cool_down']) : '';
                }

                $members[] = array(
                            'memberId' => $rsobj->fields['user_id'],
                            'firstName' => $rsobj->fields['first_name'],
                            'lastName' => $rsobj->fields['last_name'],
                            'machineId' => $rsobj->fields['machineId'],
                            'machineName' => $rsobj->fields['machineName'],
                            'startLevel' => $rsobj->fields['startLevel'],
                            'testWattLevel' => $rsobj->fields['startLevel'].' = '.$rsobj->fields['start_load_test'],
                            'testStatus' => $rsobj->fields['status'],
                            'imageURL' => $rsobj->fields['imageURL'],
                            'heartRateValue' => $heartRateValue,
                            'height' => $rsobj->fields['height'],
                            'weight' => $rsobj->fields['weight'],
                            'loadValue' => $rsobj->fields['load_value'],
                            'currentTestTime' => $rsobj->fields['current_test_time'],
                            'userTestId' => $rsobj->fields['userTestId'],
                            'coolDownStartTime' => $rsobj->fields['cool_down_start_time'],
                            'kcal' => $rsobj->fields['kcal'],
                            'age' => $rsobj->fields['age_on_test'],
                            'rpm' => $rsobj->fields['rpm'],
                            'rpm_group' => $rsobj->fields['rpm_group'],
                    );
                $rsobj->MoveNext();
                $countStart = $countStart + 1;
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Test Information For Coach successfully retrieved',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No Results found!',
            );
        }

        $functionReturnArray = array(
                'testInfoforCoach' => $testInfoforCoachList,
                'members' => $members,
                'status' => $this->status,
                // 'sql' => $rsobj->sql,
        );

        return $functionReturnArray;
    }

    /**
    * This function is to Get members list based on search criteria.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMembersList($params)
    {
        $filter = '';
        $searchText = $params['searchText'];

        if ($params['searchBy'] == 'name') {
            $filter = "AND (usr.first_name
                LIKE '$searchText%' OR usr.last_name LIKE '$searchText%' OR usr.email LIKE '$searchText%') ";
        } elseif ($params['searchBy'] == 'gsm') {
            $filter = "AND usrp.gsm LIKE '$searchText%' ";
        }

        $qry = GET_MEMBER_LIST_BY_SEARCH.' '.$filter;
        $rsobj = $this->dbcon->Execute($qry, array($params['userTypeId'], $params['companyId'], $params['clubId']));

        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Member list search failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Member list search success',
                'members' => $rsobj->GetRows(),
            );
        }

        return $this->status;
    }

    
    /**
    * This function is to Get members training using HRM.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMembersTrainingUsingHrm($params)
    {
        $qry = GET_MEMBER_LIST_SEARCH_USING_HRM_DEVICE;
        $hrmIds = implode(',', $params['hrmIds']);
        $qry .= 'AND device.device_code IN('.$hrmIds.')';

        $rsobj = $this->dbcon->Execute($qry, array($params['companyId'], $params['clubId']));

        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Member list for training using HRM ID failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Member list for training using HRM ID success',
                'members' => $rsobj->GetRows(),
            );
        }

        return $this->status;
    }

    
    /**
    * This function is to update member test and test parameter data.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function updateUserTestDetails($params)
    {
        $rsobj = $this->dbcon->Execute(GET_USER_TEST_ID_EXIST, array($params['userTestId']));

        $this->status = array(
            'status' => 'error',
            'error_code' => '0',
            'status_message' => 'Member test data update failed',
        );

        if ($rsobj->RecordCount() > 0) {

            //Update Test data begins
            $dataTest = array('test_level' => $params['startLevel'],
                          'weight' => $params['weight'],
                          'load_value' => $params['load_value'],
                          'status' => $params['testStatus'],
                         );
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $dataTest);
            $this->dbcon->Execute($rsUpdates);
            //Update Test data ends

            //Update Test parameter data begins
            $rsTestParam = $this->dbcon->Execute(GET_USER_TEST_PARAMETER_ID_EXIST, array($params['userTestId']));
            $dataTestParam = array('test_level' => $params['startLevel'],
                          'start_load_test' => $params['startLoadTest'],
                          'weight_on_test' => $params['weight'],
                         );
            $rsUpdates1 = $this->dbcon->GetUpdateSql($rsTestParam, $dataTestParam);
            $this->dbcon->Execute($rsUpdates1);
            //Update Test parameter data ends

            //Update Member personal data begins
            $rsMember = $this->dbcon->Execute(GET_USER_TEST_MEMBER_PERSONAL_EXIST, array($params['userId']));
            $dataMember = array('height' => $params['height']);
            $rsUpdates2 = $this->dbcon->GetUpdateSql($rsMember, $dataMember);
            $this->dbcon->Execute($rsUpdates2);
            //Update Member personal data ends    

            $this->status = array(
                'status' => 'success',
                'error_code' => '200',
                'status_message' => 'Member test data update success',
            );
        }

        return $this->status;
    }

    

    /**
    * This function is to get Heart Rate Monitor for Device.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function mapHrmIdForMember($params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $hrmId = isset($params['hrmId']) ? $params['hrmId'] : '';
        //$userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        //$isOwnUsed = isset($params['isOwnUsed']) ? $params['isOwnUsed'] : '';
        //$clubId = isset($params['clubId']) ? $params['clubId'] : '';
        //$companyId = isset($params['companyId']) ? $params['companyId'] : '';
        //$equipmentId = isset($params['cycleId']) ? $params['cycleId'] : '';

        //Insert heart rate monitor device begins
        $insertHRMonitor = $this->insertHeartRateMonitor($params);
        $deviceId = $insertHRMonitor['movesmart']['device_id'];
        //Insert heart rate monitor device ends

        //Map device and member begins        
        if ($deviceId > 0) {
            $params['deviceId'] = $deviceId;
            $insertDeviceMember = $this->insertDeviceMember($params);

            if (isset($insertDeviceMember['movesmart'])) {
                $paramsAssign['hrmId'] = $hrmId;
                $paramsAssign['memberId'] = $userId;
                $this->reAssignDevice($paramsAssign);

                return $insertDeviceMember;
            } else {
                return array(
                    'movesmart' => $this->status,
                );
            }
        } else {
            return array(
                'movesmart' => $this->status,
            );
        }
        //Map device and member ends        
    }

   
    /**
    * This function is to Get heart rate device associate with test.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function checkDeviceAssociatedWithTest($params)
    {
        $rsobj = $this->dbcon->Execute(GET_DEVICE_EXIST_IN_RUNNING_TEST, array($params['deviceId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Device associated with test',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Device not associated with any test',
            );
        }

        return $this->status;
    }

    
    /**
    * This function is to insert heart rate monitor.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function insertHeartRateMonitor($params)
    {
        //$userId = isset($params['userId']) ? $params['userId'] : '';
        $hrmId = isset($params['hrmId']) ? $params['hrmId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $dateTime = date('Y-m-d H:i:s');
        //$mode = $params['mode'];

        $arr = array('', 'null', '(null)');

        if (in_array(strtolower($hrmId), $arr)) {
            $this->status = array(
                'status' => 'error',
                'status_code' => '0',
                'status_message' => 'Device id is null',
            );

            return array(
                'movesmart' => $this->status,
            );
        }

        $data = array(
            'r_club_id' => $clubId,
            'r_company_id' => $companyId,
            'r_device_type_id' => 1,
            'status_id' => 0,
            'created_by' => 1,
            'created_date' => $dateTime,
            'modified_by' => 1,
            'modified_date' => $dateTime,
        );

        $data['device_code'] = $hrmId;

        $rsobj = $this->dbcon->Execute(GET_DEVICE_HEART_RATE, array($hrmId, $clubId, $companyId));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Device Code Already Exist',
                'total_records' => $rsobj->RecordCount(),
                'device_id' => $rsobj->fields['device_id'],
            );
        } else {
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Device Details Added Successfully',
                'device_id' => $this->dbcon->Insert_ID(),
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }


    /**
    * This function is to insert device member.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function insertDeviceMember($params)
    {
        $deviceId = (isset($params['deviceId'])) ? $params['deviceId'] : '';
        $userId = (isset($params['userId'])) ? $params['userId'] : '';
        $userTestId = (isset($params['userTestId'])) ? $params['userTestId'] : '';
        $isOwnUsed = (isset($params['isOwnUsed']) && $params['isOwnUsed'] == 1) ? 1 : 0;
        //$equipmentId = isset($params['equipmentId']) ? $params['equipmentId'] : '';
        $isValidate = (isset($params['isValidate']) && $params['isValidate'] === true) ? 1 : 0;

        $dateTime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
        $time = date('H:i:s');

        $data = array(
            'r_user_id' => $userId,
            'r_device_id' => $deviceId,
            'status' => 0,
            'date' => $date,
            'time' => $time,
            'created_by' => 1,
            'created_date' => $dateTime,
            'modified_by' => 1,
            'modified_date' => $dateTime,
        );
        $rsobj = $this->dbcon->Execute(GET_DEVICE_MEMBER_HEART_RATE_EXIST, array($deviceId, $userId));

        if ($rsobj->RecordCount()) {
            $data1 = array(
                    'is_deleted' => 0,
            );
            $row = $rsobj->getRows();
            $rsUserId = $row[0]['r_user_id'];

            if ($isValidate == 1) {
                if ($rsUserId == $userId) {

                    // Disable other mapped device of user
                    $rsUpdateDeviceUser = $this->dbcon->Execute(UPDATE_DEVICE_USER_MAPPING, array($userId));

                    $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data1);
                    $rsUpdate = $this->dbcon->Execute($rsUpdates);
                    $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Device Member Added Successfully',
                    );
                } else {
                    $this->status = array(
                        'status' => 'error',
                        'status_code' => 1,
                        'status_message' => 'Device Already mapped',
                    );
                }
            } else {

                    // Disable device and other user mapping
                    $rsUpdateDevice = $this->dbcon->Execute(UPDATE_DEVICE_OTHER_USER_MAPPING, array($deviceId));

                    // Disable other mapped device of user
                    $rsUpdateDeviceUser = $this->dbcon->Execute(UPDATE_DEVICE_USER_MAPPING, array($userId));

                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data1);
                $rsUpdate = $this->dbcon->Execute($rsUpdates);

                $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Device Member Added Successfully',
                    );
            }
        } else {

            // Disable other mapped device of user
            $rsUpdateDeviceUser = $this->dbcon->Execute(UPDATE_DEVICE_USER_MAPPING, array($userId));

            /* check with test_id pass * /
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);

            // Disable device and other user mapping
            $rsUpdateDevice = $this->dbcon->Execute(UPDATE_DEVICE_OTHER_USER_MAPPING, array($deviceId));

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Device Member Added Successfully',
            );
        }

            //Set Device Id For user Test Table begins
            $dataTestUpdate = array(
               'r_device_id' => $deviceId,
               'is_own_used' => $isOwnUsed,
            );
        $testId = $userTestId;
        $rsTestUpdate = $this->dbcon->Execute(GET_MEMBERLIST_INSERTFORTEST_IOSQUERY, array($testId));
        if ($rsTestUpdate->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsTestUpdate, $dataTestUpdate);
            $rsUpdate = $this->dbcon->Execute($rsUpdates);
        }
            //Set Device Id For user Test Table ends        

        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }

   
    /**
    * This function is to Change booking status.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function changeBookingStatus($params)
    {
        $rsobj = $this->dbcon->Execute(GET_BOOKING_STATUS, array($params['userId'], $params['calendarBookingId']));
        $data = array('booking_status' => 1);

        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Booking status update failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Booking status update success',
            );
        }

        return $this->status;
    }

   
    /**
    * This function is to get Device Associated To Member.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getDeviceAssociatedtoMember($params)
    {
        $associatedDevice = array();
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $userId = isset($params['userId']) ? $params['userId'] : '';
        $heartMonitorId = isset($params['heartMonitorId']) ? $params['heartMonitorId'] : '';
        if ($heartMonitorId == '') {
            $rsobj = $this->dbcon->Execute(
                GET_DEVICE_ASSOCIATED_MEMBER,
                array($params['start_time_duration'], $params['end_time_duration'], $clubId, $companyId, $userId)
            );
        } else {
            $sql = GET_MEMBER_ASSOCIATED_WITH_HRM." AND td.device_code='".$heartMonitorId."'";
            if (isset($params['mode'])) {
                $sql = GET_MEMBER_ASSOCIATED_WITH_HRM." AND td.device_code_windows='".$heartMonitorId."'";
            }
            $rsobj = $this->dbcon->Execute($sql);
        }
        //Set the status message
        if ($rsobj->RecordCount()) {
            $countStart = 0;
            while (!$rsobj->EOF) {
                if ($rsobj->fields['device_code']) {
                    $id = $rsobj->fields['device_code'];
                } else {
                    $id = $rsobj->fields['device_code_windows'];
                }
                $bookingStatus = ($rsobj->fields['calendar_booking_id'] > 0) ? 'available' : 'notAvailable';

                $getUserTestData = $this->dbcon->Execute(
                    GET_MEMBER_LAST_TEST_DETAIL_NEW, array($rsobj->fields['user_id'])
                );

                $weight = (
                     $rsobj->fields['weight_on_test'] != ''
                ) ? $rsobj->fields['weight_on_test'] : $rsobj->fields['weight'];

                $associatedDevice[] =
                               array(
                                    'id' => $id,
                                    'membersAssociated' => array(
                                    'memberId' => $rsobj->fields['user_id'],
                                    'firstName' => $rsobj->fields['first_name'],
                                    'lastName' => $rsobj->fields['last_name'],
                                    'gender' => $rsobj->fields['gender'],
                                    'age' => $getUserTestData->fields['age_on_test'],
                                    'weight' => $weight,
                                    'email' => $rsobj->fields['email'],
                                    'imageurl' => $rsobj->fields['userimage'],
                                    'companyId' => $rsobj->fields['r_company_id'],
                                    'clubId' => $rsobj->fields['r_club_id'],
                                    'externalUserId' => $rsobj->fields['external_application_user_id'],
                                    'bookingStatus' => $bookingStatus,
                                    'bookingId' => $rsobj->fields['booking_id'],
                                    'bookingIdExternal' => $rsobj->fields['calendar_booking_id'],
                                    'externalPersonId' => $rsobj->fields['person_id'],
                                    ),
                                );
                $rsobj->MoveNext();
                $countStart = $countStart + 1;
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully retrieved',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No Results found!',
            );
        }

        return array(
            'deviceList' => array(
                'device' => $associatedDevice,
                'status' => $this->status,
             ),
        );
    }

   
    /**
    * This function is to check Device Member Exist or not.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getDeviceMember($params)
    {
        $deviceId = isset($params['device_id']) ? $params['device_id'] : '';
        $rsobj = $this->dbcon->Execute(GET_DEVICE_MEMBER_HEART_RATE, array($deviceId));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'Error',
                'status_code' => 100,
                'status_message' => 'Already this Device has been Mapped ',
                'total_records' => $rsobj->RecordCount(),
            );
        } else {
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Device Member Not Exist',
            );
        }
        //Return the result array
        return array(
            'movesmart' => $this->status,
        );
    }

    
    /**
    * This function is to push cycle availability..
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function pushCycleAvailability($params)
    {
        $clubId = (isset($params['clubId'])) ? $params['clubId'] : '';
        $companyId = (isset($params['companyId'])) ? $params['companyId'] : '';
        $cycleList = (isset($params['cycleList'])) ? $params['cycleList'] : '';

        $sFlag = 0;
        if (is_array($cycleList)) {
            foreach ($cycleList as $key => $cycle) {
                $cycleName = trim(strtolower($key));
                $status = ($cycle > 0) ? 1 : 0;

                $data = array(
                        'equipment_name' => $cycleName,
                        'r_company_id' => $companyId,
                        'r_club_id' => $clubId,
                        'status' => $status,
                );

                //Insert/Update Cycle data
                $rsobj = $this->dbcon->Execute(GET_CYCLE_AVAILABILITY_CLUB, array($clubId, $companyId, $cycleName));

                if ($rsobj->RecordCount()) {
                    $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                    $this->dbcon->Execute($rsUpdates);
                    $sFlag = 1;
                } else {
                    $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
                    $this->dbcon->Execute($rsInserts);

                    if ($this->dbcon->Insert_ID() > 0) {
                        $sFlag = 1;
                    }
                }
                //Insert/Update Cycle data
            }
        }

        //Set the status message
        if ($sFlag == 1) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully updated',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Failure to updated',
            );
        }

        return $this->status;
    }

    /**
    * This function is to set cycle with tablet.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function setCycleWithTablet($params)
    {
        $ErrorMessage = '';
        $cycleId = isset($params['cycleId']) ? $params['cycleId'] : '';
        $status = isset($params['status']) ? $params['status'] : '';
        if ($cycleId == '') {
            $ErrorMessage = 'please Provide the CycleId';
        }
        if ($status == '') {
            $ErrorMessage = 'please Provide the status';
        }
        if ($ErrorMessage == '') {
            $rsobj = $this->dbcon->Execute(GET_CYCLE_LIST, array($cycleId));
            if ($rsobj->RecordCount()) {
                $data = array(
                    'status' => $params['status'],
                    'tablet_id' => $params['tabletId'],
                );
                //Dynamically construct update query
                $update_sql = $this->dbcon->GetUpdateSql($rsobj, $data);
                $rsobj = $this->dbcon->Execute($update_sql);
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully Updated',
             );
        } else {
            $this->status = array(
                'status' => 'failure',
                'status_code' => 100,
                'status_message' => 'Not Updated',
                'Error' => $ErrorMessage,
             );
        }

        return array('status' => array('statusMessage' => $this->status));
    }

    /**
    * This function is to get Cycle Associated With Members.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getCycleAssociatedWithMembers($params)
    {
        $cycleAvailability = array();
        $clubId = (isset($params['clubId'])) ? $params['clubId'] : '';
        $companyId = (isset($params['companyId'])) ? $params['companyId'] : '';
        $rsobj = $this->dbcon->Execute(GET_CYCLE_ASSOCIATED_MEMBERS, array($clubId, $companyId));

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $cycleAvailability[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully retrieved',
             );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No members Available!',
            );
        }

        return array(
            'membersAssociated' => array('membersList' => $cycleAvailability),
            'clubId' => $clubId,
            'companyId' => $companyId,
            'status' => $this->status,
            // 'sql' => $rsobj->sql,
        );
    }

    /**
    * This function is to get members Associated With cycle.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMemberAssociatedWithCycle($params)
    {
        $clubId = (isset($params['clubId'])) ? $params['clubId'] : '';
        $companyId = (isset($params['companyId'])) ? $params['companyId'] : '';
        $cycleName = (isset($params['cycleId'])) ? $params['cycleId'] : '';
        $rsobj = $this->dbcon->Execute(GET_MEMBER_ASSOCIATED_CYCLE, array($companyId, $clubId, $cycleName));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully retrieved',
                'memberData' => $rsobj->getRows(),
             );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'No members Available!',
            );
        }

        return $this->status;
    }

   
    /**
    * This function is to To push current test result.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function insertTestResults($params)
    {
        $clubId = (isset($params['clubId'])) ? $params['clubId'] : '';
        $companyId = (isset($params['companyId'])) ? $params['companyId'] : '';
        $personId = (isset($params['memberId'])) ? $params['memberId'] : '';
        $testResults = (isset($params['testResults'])) ? $params['testResults'] : '';

        $data = array(
            'heart_rate_value' => $testResults,
        );

        $rsobj = $this->dbcon->Execute(GET_USER_CURRENT_TEST_ID, array($personId, $companyId, $clubId));
        $row = $rsobj->getRows();

        $testId = (
            isset($row[0]['current_test_id'])
            && $row[0]['current_test_id'] > 0
        ) ? $row[0]['current_test_id'] : '';
        if ($testId > 0) {
            $userId = $row[0]['r_user_id'];
            $rsobj = $this->dbcon->Execute(GET_USER_TEST, array($clubId, $userId, $testId));
            if ($rsobj->RecordCount()) {
                //Dynamically construct update query
                $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                $this->dbcon->Execute($rsUpdates);
                if ($rsobj->RecordCount()) {
                    $this->status = array(
                        'status' => 'success',
                        'status_code' => 200,
                        'status_message' => 'Successfully Updated',
                     );
                } else {
                    $this->status = array(
                        'status' => 'error',
                        'status_code' => 0,
                        'status_message' => 'Member Push Test Result Failed',
                    );
                }
            }
        } else {
            //Set the status message
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Member Push Test Result Failed',
            );
        }

        //Return the result array
        return $this->status;
    }

    /**
    * This function is to get Cycle ID and Staus.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getCycleStatus($params)
    {
        $cycleStatusId = array();
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';

        $rsobj = $this->dbcon->Execute(GET_CYCLEIDSTATUS_LIST, array($clubId, $companyId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $cycleStatusId[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Cycle For this Club and Company!',
                );
        }

        return array(
            'cycleStatusId' => array('cycle' => $cycleStatusId),
            'status' => array('statusMessage' => $this->status),
        );
    }

    /**
    * This function is to get member associated with tablet.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getAssociatedWithTablet($params)
    {
        $cycleId = isset($params['cycleId']) ? $params['cycleId'] : '';
        $isAssocociatedTablet = isset($params['isAssocociatedTablet']) ? $params['isAssocociatedTablet'] : '';

        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'Is_associated_with_tablet' => $isAssocociatedTablet,
            'modified_by' => 1,
            'modified_date' => $dateTime,
        );
        $rsobj = $this->dbcon->Execute(GET_CYCLEASSOCIATEDWITHTABLET_LIST, array($cycleId));
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

                //Set the Status Message.
                $this->status = array(
                    'status' => 'Error',
                    'status_code' => 100,
                    'status_message' => 'Cycle Associate with tablet',
                );
        } else {
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Cycle ID not found',
                );
        }

        return array(
            'status' => array('statusMessage' => $this->status),
        );
    }

    /**
    * This function is to update Equipment ID for User.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function assignCycleForMember($params)
    {
        $memberAssociateId = isset($params['memberAssociateId']) ? $params['memberAssociateId'] : '';
        $cycleId = isset($params['cycleId']) ? $params['cycleId'] : '';

        $dateTime = date('Y-m-d H:i:s');
        $data = array(
            'r_equipment_id' => $cycleId,
            'modified_by' => 1,
            'modified_date' => $dateTime,
        );
        $rsobj = $this->dbcon->Execute(GET_ASSIGNCYCLEWITHMEMBER_LIST, array($memberAssociateId));
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

                //Set the Status Message.
                $this->status = array(
                    'status' => 'Error',
                    'status_code' => 100,
                    'status_message' => 'Cycle Assigned with user',
                );
        } else {
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'User not found',
                );
        }

        return array(
            'status' => array('statusMessage' => $this->status),
        );
    }

    /**
    * This function is to get Test Levels for Tablet.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getTestLevelsForTablet($params)
    {
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        $loadValues = array();
        $rsobj = $this->dbcon->Execute(GET_TESTLEVELS_TABLET, array($userTestId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $loadValues[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                    'levels' => $rsobj->fields['load_value'],
                    'rpm' => $rsobj->fields['rpm'],
                    'rpm_group' => $rsobj->fields['rpm_group'],
                    'is_test_completed' => $rsobj->fields['is_test_completed'],
                    'status' => $rsobj->fields['status'],
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Test Levels Found',
                    'levels' => '',
                    'rpm' => '',
                    'is_test_completed' => '',
                    'status' => '',
                );
        }

        return array(
            'movesmart' => $this->status,
            'loadValues' => $loadValues,
        );
    }

    /**
    * This function is to get Test heart rate for tablet.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getTestHeartRateForTablet($params)
    {
        $heartRate = array();
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        $rsobj = $this->dbcon->Execute(GET_HEARTRATE_TABLET, array($userTestId));

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $heartRate[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                    'heartRate' => $heartRate,
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Test Levels Fount',
                    'heartRate' => '',
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * This function is to save sport med test.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function saveSportmedTest($params)
    {
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        $rsobj = $this->dbcon->Execute(GET_TEST_RESULT, array($userTestId));
        $data = array(
                'r_user_id' => isset($params['userId']) ? $params['userId'] : '',
                'r_user_test_id' => $userTestId,
                'sportmed_graph' => isset($params['sportmedGraph']) ? json_encode($params['sportmedGraph']) : '',
                'adjust_sportmed_graph' => isset($params['sportmedGraph']) ? json_encode($params['sportmedGraph']) : '',
                'tmp_last_adjust_sportmed_graph_data' => isset($params['sportmedGraph']
        ) ? json_encode($params['sportmedGraph']) : '', );
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            //Update user test parameter data from sportmedGraph data
            if ($userTestId > 0) {
                $paramTest['userTestId'] = $userTestId;
                $paramTest['updateData'] = $params['sportmedGraph'];
                $paramTest['weight'] = $params['weight'];
                $this->updateTestParameterData($paramTest);
            }
            //Update user test parameter data from sportmedGraph data

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully Updated',
            );
        } else {
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $lastInsertId = $this->dbcon->Insert_ID();

            //Update user test parameter data from sportmedGraph data
            if ($lastInsertId > 0) {
                $paramTest['userTestId'] = $userTestId;
                $paramTest['updateData'] = $params['sportmedGraph'];
                $paramTest['weight'] = $params['weight'];
                $this->updateTestParameterData($paramTest);
            }
            //Update user test parameter data from sportmedGraph data

            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully Image Uploaded',
            );
        }

        //Return the result array
        return $this->status;
    }

    /**
    * This function is to Update user test parameter data from sportmedGraph data.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function updateTestParameterData($params)
    {
        $userTestId = isset($params['userTestId']) ? $params['userTestId'] : '';
        $rsobj = $this->dbcon->Execute(GET_USER_TEST_PARAMETER_EXIST, array($userTestId));

        if (isset($params['updateData'])) {
            $result = $params['updateData']['result'];
            $data['max_hr'] = $result['max_HR'];
            $data['max_p'] = $result['max_cycle_load'];
            $data['regr_coeff'] = $result['regr_coeft'];

            $data['iant_hr'] = $result['defl_HR'];
            $data['iant_p'] = $result['defl_cycle_load'];
            $data['relative_iant_p'] = ($result['defl_cycle_load']) / ($params['weight']);
            $data['relative_max_p'] = ($result['max_cycle_load']) / ($params['weight']);
            $data['iant_vo2'] = $result['iant_vo2'];
            $data['relative_iant_vo2'] = $result['rel_iant_vo2'];
            $data['fitness_level'] = $result['fitness_level'];
        }

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Successfully updated',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Update failed',
            );
        }

        return $this->status;
    }

    /**
    * To get the Cycle availability for based on club and company Id.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getCycleAvailability($params)
    {
        $cycleList = array();
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';

        $rsobj = $this->dbcon->Execute(GET_CYCLE_AVAILABILITY_FOR_CLUB, array($clubId, $companyId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $cycleList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                    'cycleList' => $cycleList,
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Test Levels Fount',
                    'cycleList' => '',
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To re assign device.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function reAssignDevice($params)
    {
        $hrmId = isset($params['hrmId']) ? $params['hrmId'] : '';
        $memberId = isset($params['memberId']) ? $params['memberId'] : '';

        $isSpecificRecord = isset($params['isSpecificRecord']) ? $params['isSpecificRecord'] : '';

        if ($isSpecificRecord == 1) {
            /* update for current Device Member * /
                $rsCurrentDevice = $this->dbcon->Execute(GET_DEVICE_STATUS_CURRENT, array($memberId, $hrmId));
            if ($rsCurrentDevice->RecordCount()) {
                $deviceMember = $rsCurrentDevice->fields['device_member_id'];
                $rsDeviceMember = $this->dbcon->Execute(GET_DEVICE_MEMBER_ID, array($deviceMember));
                $data = array(
                            'is_deleted' => 1,
                        );
                $rsUpdates = $this->dbcon->GetUpdateSql($rsDeviceMember, $data);
                $rsUpdate = $this->dbcon->Execute($rsUpdates);
                $this->status = array(
                            'status' => 'success',
                            'status_code' => 200,
                            'status_message' => 'Device Re-assign Updated Successfully',
                    );
            } else {
                $this->status = array(
                            'status' => 'error',
                            'status_code' => 200,
                            'status_message' => 'No Device Re-assign Records Found',
                        );
            }
                /* End * /
        } else {
            /* update for other device member * /
                $rsobj = $this->dbcon->Execute(GET_DEVICE_STATUS, array($memberId, $hrmId));
            if ($rsobj->RecordCount()) {
                while (!$rsobj->EOF) {
                    $deviceMemberId = $rsobj->fields['device_member_id'];
                    $rsDeviceMember = $this->dbcon->Execute(GET_DEVICE_MEMBER_ID, array($deviceMemberId));
                    $data = array(
                            'is_deleted' => 1,
                        );
                    $rsUpdates = $this->dbcon->GetUpdateSql($rsDeviceMember, $data);
                    $rsUpdate = $this->dbcon->Execute($rsUpdates);
                    $rsobj->MoveNext();
                }
                $this->status = array(
                            'status' => 'success',
                            'status_code' => 200,
                            'status_message' => 'Device Re-assign Updated Successfully',
                    );
            } else {
                $this->status = array(
                            'status' => 'error',
                            'status_code' => 200,
                            'status_message' => 'No Device Re-assign Records Found',
                        );
            }
                /* update for other Device Member End*/
                /* update for current Device Member * /
                $rsCurrentDevice = $this->dbcon->Execute(GET_DEVICE_STATUS_CURRENT, array($memberId, $hrmId));
            if ($rsCurrentDevice->RecordCount()) {
                $deviceMember = $rsCurrentDevice->fields['device_member_id'];
                $rsDeviceMember = $this->dbcon->Execute(GET_DEVICE_MEMBER_ID, array($deviceMember));
                $data = array(
                            'is_deleted' => 0,
                        );
                $rsUpdates = $this->dbcon->GetUpdateSql($rsDeviceMember, $data);
                $rsUpdate = $this->dbcon->Execute($rsUpdates);
            }
                /* End * /
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To get the Equipemt Id  with company Id, Club Id and cycle Id.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getEquipmentId($params)
    {
        $cycleNum = isset($params['cycleNum']) ? 'cycle'.$params['cycleNum'] : die('Required Cycle');
        $clubId = isset($params['clubId']) ? $params['clubId'] : die('Required Club');
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';

        $rsobj = $this->dbcon->Execute(GET_EQUIPMENTID, array($cycleNum, $companyId, $clubId));
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $equipmentList[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                    'equipmentList' => $equipmentList,
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Test Levels Found',
                    'equipmentList' => array('equipment_id' => ''),
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To get the Load Value  with company Id, Club Id and cycle Id.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getCycleTestlevels($params)
    {
        $cycleId = isset($params['cycleId']) ? $params['cycleId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';

        $rsobj = $this->dbcon->Execute(GET_CYCLETEST_LEVELS, array($cycleId, $clubId));

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $testLevels[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                    'testLevels' => $testLevels,
                    // 'sql' => $rsobj->sql,
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Test Levels Found',
                    'testLevels' => array('load_value' => ''),
                    // 'sql' => $rsobj->sql,
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    
    /**
    * To insert RPM from CCA.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function insertRpmFromCCA($params)
    {
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        //$companyId = isset($params['companyId']) ? $params['companyId'] : '';

        $revertFlag = 0;
        $this->dbcon->BeginTrans();
        $errorText = 'Check your input data';

        foreach ($params['cycles'] as $paramCycle) {
            $params['cycleNum'] = $paramCycle['cycleId'];
            $result = $this->getEquipmentId($params);

            $code = '0';
            //If cycle is not found
            if (!isset($result['movesmart']['equipmentList'][0]['equipment_id'])) {
                $errorText = 'Cycle is not found';
                $revertFlag = 1;
                $code = '1';
                break;
            }

            $params['cycleId'] = $result['movesmart']['equipmentList'][0]['equipment_id'];
            $cycleId = isset($params['cycleId']) ? $params['cycleId'] : '';

            $rpm = isset($paramCycle['rpm']) ? $paramCycle['rpm'] : '';

            $rsRetrieve = $this->dbcon->Execute(GET_USERTESTRPM, array($cycleId, $clubId));

            //get user test id
            if ($rsRetrieve->RecordCount()) {
                $rsobj = $this->dbcon->Execute(GET_UPDATERPMUSERTEST, array($rsRetrieve->fields['user_test_id']));

                $data = array(
                        'rpm' => $rpm,
                 );

                //Updtae RPM group
                if ($rpm != '') {

                    //Repeat RPM as in config interval
                    $b = array_fill(0, CCA_UPDATE_TIME_INTERVAL, $rpm);
                    $newRpm = implode(',', $b);
                    $rpmGroupArr = array();
                    if ($rsRetrieve->fields['rpm_group'] != '') {
                        $rpmGroupArr = explode(',', $rsRetrieve->fields['rpm_group']);
                    }

                    $data['rpm_group'] = $newRpm;

                    if (is_array($rpmGroupArr) && count($rpmGroupArr) > 0) {
                        array_push($rpmGroupArr, $newRpm);
                        $rpmGroup = implode(',', $rpmGroupArr);
                        $data['rpm_group'] = $rpmGroup;
                    }
                }

                if (
                    ($rpm == 0 || $rpm == '')
                    && (($rsRetrieve->fields['cool_down_start_time'] > 0)
                    && ($rsRetrieve->fields['cool_down_stop_time'] == 0))
                ) {
                    $data = array(
                            'rpm' => $rpm,
                            'status' => 3,
                            'is_test_completed' => 'zero_RPM',
                     );
                }

                if ($rsobj->RecordCount()) {
                    $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
                    $this->dbcon->Execute($rsUpdates);
                    $code = '0';
                } else {
                    $errorText = 'Update failed';
                    $code = '1';
                    $revertFlag = 1;
                    break;
                }
            } else {
                $errorText = 'There is no active test for this cycle';
                $code = '1';
                $revertFlag = 1;
                break;
            }
        }

        if ($revertFlag === 1) {
            $this->dbcon->RollbackTrans();
            $this->status = array(
                'status' => 'error',
                'error_code' => $code,
                'status_message' => $errorText,
            );
        } else {
            $this->dbcon->CommitTrans();
            $this->status = array(
                'status' => 'success',
                'error_code' => $code,
                'status_message' => 'Successfully updated',
            );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To get get Same Assigned Device.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getSameAssignedDevice($params)
    {
        $hrmId = isset($params['hrmId']) ? $params['hrmId'] : '';
        $memberId = isset($params['memberId']) ? $params['memberId'] : '';
        $rsCurrentDevice = $this->dbcon->Execute(GET_DEVICE_CURRENT, array($memberId, $hrmId));
        if ($rsCurrentDevice->RecordCount()) {
            $this->status = array(
                    'status' => 'Error',
                    'status_code' => 200,
                    'status_message' => 'Device already Assigned',
            );
        } else {
            $this->status = array(
                    'status' => 'Success',
                    'status_code' => 200,
                    'status_message' => 'No Device Re-assign Records Found',
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To insert the member Training Points.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function insertMemberTrainingPoints($params)
    {
        $memberId = isset($params['r_user_id']) ? $params['r_user_id'] : '';
        $userTestId = isset($params['r_user_test_id']) ? $params['r_user_test_id'] : '';
        $rsPointsAchieve = $this->dbcon->Execute(GET_POINTS_ACHIEVE, array($memberId, $userTestId));
        if ($rsPointsAchieve->RecordCount()) {
            $params['week'] = $rsPointsAchieve->fields['week'];
            $params['points_collected_date'] = date('Y-m-d H:i:s');
            $rsPointsTransation = $this->dbcon->Execute(GET_TRANSACTION);

            // To Add the Training Points in training points transaction table.
            $rsInsertTransation = $this->dbcon->GetInsertSql($rsPointsTransation, $params);
            $rsInsertTransation = $this->dbcon->Execute($rsInsertTransation);
            $lastInsertId = $this->dbcon->Insert_ID();
            //$pointsAchieveId = $rsPointsAchieve->fields['points_achieve_id'];
            $achievedPoint = $rsPointsAchieve->fields['achieved_point'];
            $acutalPoint = $achievedPoint + $params['achieved_point'];
            // To update the points achieve table.
            if ($lastInsertId > 0) {
                $dataPointsAchieved['achieved_point'] = $acutalPoint;
                $rsUpdates = $this->dbcon->GetUpdateSql($rsPointsAchieve, $dataPointsAchieved);
                $this->dbcon->Execute($rsUpdates);
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Training Points Transaction Details Successfully Added',
            );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 0,
                    'status_message' => 'No Records Found',
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To get Training Device List.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getTrainingDeviceList($params)
    {
        if ($params){
            
        }
        $rsobj = $this->dbcon->Execute(GET_TRAINING_DEVICE);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $trainingDevice[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                    'status' => 'success',
                    'status_code' => 200,
                    'status_message' => 'Successfully retrieved',
                    'trainingDevice' => $trainingDevice,
                 );
        } else {
            $this->status = array(
                    'status' => 'error',
                    'status_code' => 200,
                    'status_message' => 'No Test Levels Found',
                    'trainingDevice' => '',
                );
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * To get Member List For Strength Training.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMemberListForStrengthTraining($params)
    {
        //$status = array();
        $memListTrainingToDo = array();
        $companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $rsobj = $this->dbcon->Execute(
                GET_MEMBERLIST_FOR_STRENGTH_TRAINING,
                array($params['start_time_duration'], $params['end_time_duration'], $companyId, $clubId)
        );

        //Set the status message
        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Members List for strength Training are successfully retrieved',
        );

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $memListTrainingToDo[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 200,
                'status_message' => 'No results found!',
            );
        }

        //Return the result array
        return array(
            'membersList' => array(
                'member' => $memListTrainingToDo,
                'status' => $this->status,

            ),
        );
    }

    /**
    * Method to get members strength activity data in training.
    * 
    * @param array $params service parameter, Search arguments 
    *
    * @return array object obj
    * /
    public function getMemberStrengthTestActivity($params)
    {
        $rsobj = $this->dbcon->Execute(
                GET_ACTIVITY_DETAIL_FOR_STRENGTH_TRAINING,
                array($params['userId'], $params['strengthUserTestId'], $params['strengthMahineId'])
        );

        //Set the status message
        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Members List for strength Training activity data failed',
        );

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $activityDetails[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Members List for strength Training activity data success',
                'activity_details' => $activityDetails,
            );
        }

        //Return the result array
        return $this->status;
    }
    */
} // End Class.
;
