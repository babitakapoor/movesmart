<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle login related actions.
 */
require_once SQL_PATH.DS.'login.php';
 /**
 * Class for functions to handle login related actions.
 
 * @category Modules
 
 * @package Login
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class loginModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 0,
            'status_message' => 'No result found',
        );
    }

    /**
    * Authenticate & Returns the Login User Information(On Success)
    *
    * @param array $params service parameter, Keys:Name,Password & Role, LoginType
    *
    * @return array object
    */
    public function authenticate($params)
    {
        //echo "5";
        $username = isset($params['username']) ? $params['username'] : '';
        $password = isset($params['password']) ? $params['password'] : '';
        /*MK Added 04 Jan 2015 Task No.3946 */
        $usertype_id = isset($params['usertype_id']) ? $params['usertype_id'] : -1;
        $user = null;
        //echo "4";
        if ($usertype_id != -1 && $usertype_id != 3) {
						
            $rsobj = $this->dbcon->Execute(GET_USER_LOGIN_BYTYPE, array($username, $username, $password, $usertype_id));
        } 
		 elseif($usertype_id == 3){
				 $rsobj = $this->dbcon->Execute(GET_USER_LOGIN_MEMBER, array($username, $username, $password, $usertype_id));
				 
			} 
		else {
            $rsobj = $this->dbcon->Execute(GET_USER_LOGIN, array($username, $username, $password));
        }
        //echo GET_USER_LOGIN_BYTYPE;
        /*MK Edited 04 Jan 2015 Task No.3946 - Ends Login with User Type to Limit the coach / client login*/
        if ($rsobj->RecordCount()) {
			
            while (!$rsobj->EOF) {
                $user = $rsobj->fields;
                $rsobj->MoveNext();
				
            }
        }
		
        //echo "5";
		
        if (!empty($user)) {
		
            //status_code 1 - authenticated user, 2 - invalid user name and pass, 3 - Not authorized
            $userTypeAccept = array(1, 4, 2, 3, 9);
            if ($user['is_deleted'] == 1) {
                $this->status['data'] = $user;
                $this->status['data']['status_code'] = 4;
            } elseif (!empty($user['usertype_id']) &&  !in_array($user['usertype_id'], $userTypeAccept)) {
                $this->status['data']['status_code'] = 3;
                $this->status['data']['usertype_id'] = -1;
            } elseif (in_array($user['usertype_id'], $userTypeAccept) && $user['r_status_id']!=2) {
                $this->status['data'] = $user;
                $this->status['data']['status_code'] = 1;
            }
			  elseif ($user['usertype_id']==3 && $user['r_status_id']==2) {
				 
                //$this->status['data'] = $user;
                $this->status['data']['status_code'] = 5;
            } 
			
        } else {
            $this->status['data']['status_code'] = 2;
            $this->status['data']['usertype_id'] = -1;
        }

        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns permitted pages Based on the User Role
    *
    * @param array $params service parameter, Keys:FileName,UserTypeId(Role)
    *
    * @return array object
    */
    public function getUserPagePermissions($params)
    {
      
        $fileName = isset($params['fileName']) ? $params['fileName'] : '';
        $userTypeId = isset($params['userTypeId']) ? $params['userTypeId'] : '';

        $rsobj = $this->dbcon->Execute(GET_USER_PAGE_PERMISSION, array($userTypeId, $fileName));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User page permissions success',
                'userPermission' => $rsobj->getRows(),
            );
        }

        return $this->status;
    }
    
     /**
    * Direct Login
    *
    * @param array $params service parameter, Keys:FileName,UserTypeId(Role)
    *
    * @return array object
    */
    /*
    public function directLogin($params)
    {
        //$userName = isset($params['userName']) ? $params['userName'] : '';
        $password = isset($params['password']) ? $params['password'] : '';

        $rsobj = $this->dbcon->Execute(GET_USER_LOGIN, array($username, $username, $password));
        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User page permissions success',
                'userPermission' => $rsobj->getRows(),
            );
        }

        return $this->status;
    }
    */
}
