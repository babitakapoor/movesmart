<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Club
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle club related actions.
 */
require_once SQL_PATH.DS.'club.php';
/**
 * Class to handle Club Related Functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class ClubModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'success',
            'status_code' => 200,
            'status_message' => 'Success',
        );

        $this->error_general = array(
            'response' => array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Some error occured. Please try later.',
            ),
        );
    }

    /**
    * Get & Returns the Club Details for the Company / Authorized Club.
    *
    * @param array $params service parameter, Keys:CompanyId(company_id)
        ,authorizedClubId
    *
    * @return array object
    */
    public function getClubDetails($params)
    {
        $where = '';
        $is_deleted = isset($params['is_deleted']) ? $params['is_deleted'] : -1;
        $company_id = isset($params['company_id']) ? $params['company_id'] : -1;

        /*Authorized club Id will be displayed.*/
        $where .= (isset($params['authorizedClubId']) && 
            $params['authorizedClubId'] != '') ?
            ' AND club_id IN('.$params['authorizedClubId'].')' : '';

        $clubIdInQuery = GET_CLUB_DETAIL_DROPDOWN.$where;

      /*   $rsobj = $this->dbcon->Execute(
            $clubIdInQuery, array($company_id, $is_deleted)
        ); */
  $rsobj = $this->dbcon->Execute(
            GET_CLUB_DETAIL_DROPDOWN   
        );
		
        if ($rsobj->RecordCount()) {
            $category   =   array();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
			
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Personal Info Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'clublist' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Personal Info Details Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
		
		
        //Return the result array   		
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns the Club Images for the Company / Club.
    *
    * @param array $params service parameter, Keys:Company,ClubId
    *
    * @return array object
    */
    /*
    public function getClubImages($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_CLUB_IMAGES_BY_CLUB_ID, 
            $params['clubId'], $params['companyId']
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Get club images success',
                'clubImages' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => '0',
                'status_message' => 'Get club images failed',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Add / Update Returns Club Images for the Company / Club.
    *
    * @param array $params service parameter, Keys:Company,ClubId
    *
    * @return array object
    */
    /*public function saveClubImage($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_CLUB_IMAGE_BY_IMAGE_ID, 
            array(-1, $params['clubId'], $params['companyId'])
        );

        $data = array('r_club_id' => $params['clubId'],
                      'r_company_id' => $params['companyId'],
                      'image_path' => $params['imagePath'],
                      );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'error',
                'status_code' => '200',
                'status_message' => 'Add club images error',
            );
        } else {
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $data);
            $this->dbcon->Execute($rsInserts);
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Add club images success',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Delete Image & Returns Reault of Deleted Club Image.
    *
    * @param array $params service parameter, Keys:Company,ClubId
    *
    * @return array object
    */
    /*public function deleteClubImage($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_CLUB_IMAGE_BY_IMAGE_ID,
            array(
                $params['clubImageId'], 
                $params['clubId'], $params['companyId']
            )
        );

        $data = array('is_deleted' => 1);

        if ($rsobj->RecordCount() > 0) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            $this->status = array(
                'status' => 'error',
                'status_code' => '200',
                'status_message' => 'Delete club image success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => '0',
                'status_message' => 'Delete club image failed',
            );
        }
        //Return the result array    
        return $this->status;
    }
    */
    /**
    * Get & Returns Result of Club List for a company .
    *
    * @param array $params service parameter, Keys:CompanyId,Limit Controls
    *
    * @return array object
    */
    public function getClubListByCompany($params)
    {
        $limit = '';
        if (isset($params['limitStart']) && isset($params['limitEnd'])) {
            $limitStart = $params['limitStart'];
            $limitEnd = $params['limitEnd'];
            $limit = 'LIMIT '.$limitStart.','.$limitEnd;
        }

        $filterQry = '';

        //To get filter data
        if (isset($params['searchType'])) {
            if (trim($params['searchValue']) != '') {
                $filterQry = 'AND '.$params['searchType'].
                    " LIKE '".trim($params['searchValue'])."%'";
            }
        }

        $sort = 'ORDER BY `club_name` ASC ';

        if (isset($params['labelField']) && isset($params['sortType'])) {
            $sort = 'ORDER BY '.$params['labelField'].' '.$params['sortType'];
        }

        $qry = GET_CLUB_LIST_BY_COMPANY.' '.$filterQry.' '.$sort.' '.$limit;

        $rsobj = $this->dbcon->Execute($qry, array($params['companyId']));

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get test result success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'rows' => $rsobj->GetRows(),
                'totalCount' => $this->getLastQueryTotalCount(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get test result failed',
                // 'sql' => $rsobj->sql,
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Get & Returns Detail of a Chosen Club .
    *
    * @param array $params service parameter, Keys:ClubId
    *
    * @return array object
    */
    public function getClubDetailById($params)
    {
		
        $rsobj = $this->dbcon->Execute(
            GET_CLUB_DETAIL_BY_CLUB_ID, array($params['clubId'])
        );

        if ($rsobj->RecordCount()) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get club result success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'rows' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get club result success',
                // 'sql' => $rsobj->sql,
            );
        }

        //Return the result array    
        return $this->status;
    }

    /**
    * Add / Update the Detail of a Club.
    *
    * @param array $params service parameter, Keys:ClubId, other Club Data
    *
    * @return array object
    */
     public function UpdateClubDetails($params)
     {
		
         $clubId = isset($params['clubId']) ? $params['clubId'] : '';
         $dateTime = date('Y-m-d H:i:s');

         $data = array(
                'r_company_id' => $params['companyId'],
                'club_name' => $params['club_name'],
                'street' => $params['street'],
                'number' => $params['number'],
                'bus' => $params['bus'],
				'group_id'=>$params['group_name'],
                'post_code' => $params['post_code'],
                'location' => $params['location'],
                'email_id' => $params['email_id'],
                'status' => $params['status_id'],
                't_ismedicalinfo' => (isset($params['ismedical']) ? 1 : 0),
                'commercial_name' => $params['commercial_name'],
                'modified_date' => $dateTime,
                'modified_by' => $params['loggedUserId'],
        );

         $rsobj = $this->dbcon->Execute(GET_CLUB_DETAIL_BY_CLUB_ID, array($clubId));
		
         if ($rsobj->RecordCount()) {
			 echo "hello";
			 die;
             $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
             $this->dbcon->Execute($rsUpdates);

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Club details update success',
            );
            /*"sql"               => $rsUpdate->sql*/
         } else {
             $data['created_date'] = $dateTime;
             $data['created_by'] = $params['loggedUserId'];
             $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
             $this->dbcon->Execute($rsInserts);

             $clubId = $this->dbcon->Insert_ID();
			
			$day = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
			
			 foreach($day as $key=>$days)
			{
				
				for($i=8;$i<=22;$i++){
				
					$even = array('time'=>$i.':00',
									'days'=>$day[$key],
									'lesson'=>'FREE');
				//	$odd = array('time'=>$i.':30',
					//			'days'=>$day[$key],
					//			'lesson'=>'FREE');
				$insert_even = "INSERT into t_time_slots(time,days,lesson,clubId) values ('".$even['time']."','".$even['days']."','".$even['lesson']."',$clubId)";
				mysql_query($insert_even);
			//	$insert_odd = "INSERT into t_time_slots(time,days,lesson,clubId) values ('".$odd['time']."','".$odd['days']."','".$odd['lesson']."',$clubId)";
				//mysql_query($insert_odd);
						
								
				}
				
				
			} 
             $this->status = $this->error_general;

             if ($clubId > 0) {
                 //Set the status message
                $this->status = array(
                    'status' => 'insert_success',
                    'status_code' => 200,
                    'status_message' => 'Club details add success',
                    //"sql"               => $rsInsert->sql,
                    'club_id' => $clubId,
                );
             }
         }
        //Return the result array

        return $this->status;
     }

    /**
     * Returns The Total Records Count Common to the Club Model
     * @return int
     * @internal param array $params service parameter, Keys:ClubId, other Club Data
     *
     */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }

    /**
    * Get & Returns Search Result of a All Equipments
    *
    * @param array $params service parameter Key:searchType,
    *
    * @return array object
    */
    /*
    public function getEquipmentList($params)
    {
        if ($params) {
        }
        $rsobj = $this->dbcon->Execute(GET_EQUIPMENT_DROPDOWN);
        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Equipment List Retrieved',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'equipmentList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Equipment List Not Found',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get & Returns Search Result of a Equipments Available for a Club
    *
    * @param array $params service parameter Key:searchType,
    *
    * @return array object
    */
    /*
    public function getManageCycleSearch($params)
    {
        //$companyId = isset($params['companyId']) ? $params['companyId'] : '';
        $condition = '';
        //$SearchVal = '';
        $category = array();

        $cycleSearchQuery = GET_MANAGE_CYCLE;

        if (isset($params['searchType']) &&
            $params['searchType'] != '' && 
            !empty($params['searchType'])
        ) {
            $searchType = $params['searchType'];
            $condition .= " WHERE (cl.club_id='".$searchType."')";
        }
        $queryCycle = $cycleSearchQuery.$condition;
        $rsobj = $this->dbcon->Execute($queryCycle);

        if ($rsobj->RecordCount()) {
            $totalrecords = $rsobj->RecordCount();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Cycle Search Received',
                'total_records' => $totalrecords,
                // 'sql' => $rsobj->sql,
                'memberlist' => $category,
            );
        } else {
            $totalrecords = $rsobj->RecordCount();
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Cycle Not Found.',
                'total_records' => $totalrecords,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => array(
                'manageCycleList' => $category,
            ),
        );
    }
    */
    /**
    * Get & Returns Records of a Equipments Available for a Club
    *
    * @param array $params service parameter Key:searchType,
    *
    * @return array object
    */
    /*public function getManageCycle($params)
    {
        $category = array();
        $equipmentAvailableId = isset($params['equipmentAvailable']) ?
            $params['equipmentAvailable'] : '';

        if ($equipmentAvailableId != '') {
            $condition = ' WHERE ea.equipment_available_id = '.
                $equipmentAvailableId.' ORDER BY cl.club_name';
            $queryCycle = GET_MANAGE_CYCLE.$condition;
            $rsobj = $this->dbcon->Execute($queryCycle);
        } else {
            $rsobj = $this->dbcon->Execute(
                GET_MANAGE_CYCLE.' ORDER BY cl.club_name,e.equipment_id'
            );
        }

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Cycle Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'manageCycleList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Cycle Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get & Returns Records of a Search Equipments List By Club, Company or Equipments Information
    *
    * @param array $params service parameter Key:searchType,
    *
    * @return array object
    */
    /*
    public function getManageLinkCycle($params)
    {
        if ($params) {
        }
        $category = array();
        $rsobj = $this->dbcon->Execute(GET_LINKCYCLE_WITH_MEMBER);

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Cycle Details Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'manageCycleList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Cycle Details Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'manageCycleList' => $category,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get & Returns Records of a Search Equipments List By Club, Company or Equipments Information
    *
    * @param array $params service parameter Key:searchType,
    *
    * @return array object
    */
    /*public function getManageLinkCycleSearch($params)
    {
        $condition = '';
        $category = array();

        $linkSearchQuery = GET_LINKCYCLE_WITH_MEMBER;

        if (isset($params['searchType']) && 
            $params['searchType'] != '' && 
            !empty($params['searchType'])
        ) {
            $searchType = $params['searchType'];
            $condition .= " AND (u.r_club_id='".$searchType."')";
        }
        $queryCycle = $linkSearchQuery.$condition;
        $rsobj = $this->dbcon->Execute($queryCycle);

        if ($rsobj->RecordCount()) {
            $totalrecords = $rsobj->RecordCount();
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }

            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Cycle Search Received',
                'total_records' => $totalrecords,
                // 'sql' => $rsobj->sql,
                'memberlist' => $category,
            );
        } else {
            $totalrecords = $rsobj->RecordCount();
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Cycle Not Found.',
                'total_records' => $totalrecords,
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => array(
                'linkCycleList' => $category,
            ),
        );
    }
    */
    /**
    * Get & Returns Records of a All Equipments List
    *
    * @param array $params service parameter
    *
    * @return array object
    */
    /*
    public function getCycleList($params)
    {
        if ($params) {
        }
        $category = array();
        $rsobj = $this->dbcon->Execute(GET_CYCLE_LIST_DROPDOWN);

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Cycle List Received',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'cycleList' => $category,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Cycles Not Found.',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'cycleList' => $category,
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }
    */
    /**
    * Get & Returns Records of a Equipments available for a Club
    *
    * @param array $params service parameter, Keys:clubId
    *
    * @return array object
    */
    /*public function getCycleAvailableForClub($params)
    {
        //$userId = isset($params['userId']) ? $params['userId'] : '';
        $clubId = isset($params['clubId']) ? $params['clubId'] : '';
        $category = array();
        $rsobj = $this->dbcon->Execute(GETCYCLEAVAILABLE_FORCLUB, $clubId);

        if ($rsobj->RecordCount()) {
            while (!$rsobj->EOF) {
                $category[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            //Set the status message
            $status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Count Retrieved',
                'availableCycles' => $category,
                // 'sql' => $rsobj->sql,
            );
        } else {
            $status = array(
                'availableCycles' => '0',
            );
        }

        return $status;
    }
    */
    /**
    * Add / Update Returns Records of a Employee, Role,Company, Club Mapped Details
    *
    * @param array $params service parameter, Keys:clubId,userTypeId
    *
    * @return array object
    */
    public function insertEmployeeClubRole($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_EMPLOYEE_CLUB_ROLES, 
            array($params['userId'], $params['clubId'], $params['userTypeId'])
        );

        $isMainContact = (isset($params['isMainContact']) && $params['isMainContact'] == 1) ? 1 : 0;

        $data = array(
                'r_user_id' => $params['userId'],
                'r_company_id' => $params['companyId'],
                'r_club_id' => $params['clubId'],
                'r_user_type_id' => $params['userTypeId'],
                'is_main_contact' => $isMainContact,
        );

        if ($rsobj->RecordCount()) {
            //Set the status message
            $this->status = array(
                'status' => 'exist',
                'status_code' => 200,
                'status_message' => 'Employee club role exist',
                // 'sql' => $rsobj->sql,
            );
        } else {
            if ($isMainContact == 1) {
                $this->dbcon->Execute(
                    UPDATE_CLUB_MAIN_CONTACT, array($params['clubId'])
                );
            }

            $rsInserts = $this->dbcon->GetInsertSQL($rsobj, $data);
            $this->dbcon->Execute($rsInserts);

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Employee club role added successfully',
                // 'sql' => $rsobj->sql,
            );
        }

        //Return the result array
        return $this->status;
    }

    /**
    * Get & Returns Records of a Employee, Role,Company, Club Mapped Details
    *
    * @param array $params service parameter, Keys:employeeCentreId
    *
    * @return array object
    */
    public function getEmployeeClubRoles($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_EMPLOYEE_COMPANY_CLUB_ROLES, array($params['userId'], $params['companyId'])
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get user club roles success',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
                'employeeClubRoles' => $rsobj->GetRows(),
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 1,
                'status_message' => 'Get user club roles success failed',
                'total_records' => $rsobj->RecordCount(),
                // 'sql' => $rsobj->sql,
            );
        }
        //Return the result array    
        return $this->status;
    }
     
    /**
    * Delete & Returns Result of a Employee, Role,Company, Club Mapped Details
    *
    * @param array $params service parameter, Keys:employeeCentreId
    *
    * @return array object
    */
    public function deleteEmployeeClubRole($params)
    {
        $data = array(
                'employee_centre_id' => $params['employeeCentreId'],
                'is_deleted' => 1,
        );
        $rsobj = $this->dbcon->Execute(
            GET_EMPLOYEE_CENTRE, array($params['employeeCentreId'])
        );

        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Employee centre role deleted successfully',
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Employee role update delete failed',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }

        return $this->status;
    }

    /**
    * Get & Returns Records of a Employee,Company, Club Mapped Details
    *
    * @param array $params service parameter, Keys:companyId
    *
    * @return array object
    */
    public function getEmployeeCenterDetails($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_EMPLOYEE_CENTER_CLUB, array($params['clubId'])
        );
        if ($rsobj->RecordCount()) {
            $empCenter  =   array();
            while (!$rsobj->EOF) {
                $empCenter[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Employee Center Details Successfully Retrieved',
                'employeeCenter' => $empCenter,
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Employee center details not found',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
                'employeeCenter' => '',
            );
        }
        //Return the result array    
        return array(
            'movesmart' => $this->status,
        );
    }

    /**
    * Get & Returns Records of a Employee for a Company
    *
    * @param array $params service parameter, Keys:companyId
    *
    * @return array object
    */
    public function getCompanyEmployees($params)
    {
        $rsobj = $this->dbcon->Execute(
            GET_COMPANY_EMPLOYEES_BY_ID, array($params['companyId'])
        );
        if ($rsobj->RecordCount()) {
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Get company employees success',
                'rows' => $rsobj->GetRows(),
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get company employees failed',
            );
        }
        //Return the result array    

        return $this->status;
    }

    /**
    * Delete & Returns Deleted Result of a Employee Club Mapping information
    *
    * @param array $params service parameter, Keys:empCenterId
    *
    * @return array object
    */
    public function deleteEmployeeCentre($params)
    {
        $empCenterId = isset($params['empCenterId']) ? $params['empCenterId'] : '';
        $dateTime = date('Y-m-d H:i:s');
        $data = array(
                'employee_centre_id' => isset($params['empCenterId']) ?
                    $params['empCenterId'] : '0',
                'is_deleted' => '1',
                'modified_by' => '1',
                'modified_date' => $dateTime,
        );
        $rsobj = $this->dbcon->Execute(GET_EMPLOYEE_DELETED, array($empCenterId));
        if ($rsobj->RecordCount()) {
            $rsUpdates = $this->dbcon->GetUpdateSQL($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);
            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'User Deleted.',
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'User Not Found.',
                'total_records' => 0,
                // 'sql' => $rsobj->sql,
            );
        }
    }

    /**
    * Delete & Returns Deleted Result of a Club
    *
    * @param array $params service parameter, Keys:ClubId, other Club Data
    *
    * @return array object
    */
    public function deleteClubById($params)
    {
        $rsobj = $this->dbcon->Execute(GET_CLUB_ID_EXIST, array($params['clubId']));
        if ($rsobj->RecordCount()) {
            //Update club id delete status
            $this->dbcon->Execute(UPDATE_CLUB_ID_SOFT_DELETE, array($params['clubId']));

            //Set the status message
            $this->status = array(
                'status' => 'success',
                'status_code' => 200,
                'status_message' => 'Club delete success',
                'rows' => $rsobj->GetRows(),
                // 'sql' => $rsobj->sql,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Club delete failed',
            );
        }
        //Return the result array    

        return $this->status;
    }
} //End Class.
;
