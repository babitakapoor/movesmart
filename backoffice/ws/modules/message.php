<?php
/**
 * PHP version 5.
 
 * @category Modules
 
 * @package Message
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description DB access functions to handle message related actions.
 */
require_once SQL_PATH.DS.'message.php'; 
 /**
 * Class to handle Message related functions.
 
 * @category Modules
 
 * @package Admin
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @version Release: 1.0
 
 * @link http://movesmart.company/admin/
 */
class messageModel
{
    public $dbcon;
    public $status;

    /**
     * Class constructor.
     * @param ADOConnection|array $dbcon connection arguments
     */
    public function __construct(ADOConnection $dbcon)
    {
        $this->dbcon = $dbcon;

        $this->status = array(
            'status' => 'error',
            'status_code' => 1,
            'status_message' => 'Opps an error as occurred',
        );
    }

    /**
    * Returns an json obj of get Messages By User Id for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function getMessagesByUserId($params)
    {
        $qry = GET_MESSAGES_BY_USER_ID;

        if (isset($params['msgDate']) && !isset($params['searchText'])) {
            $qry .= " AND DATE(MSG.`updated_date`) = '".$params['msgDate']."'";
        }

        if (isset($params['searchText'])) {
            $qry .= " AND MSG.`message` LIKE '%".$params['searchText']."%'";
            $qry .= ' AND DATE(MSG.`updated_date`) BETWEEN DATE(NOW() - INTERVAL 30 DAY) AND DATE(NOW()) ';
        }

        $rsobj = $this->dbcon->Execute($qry, array($params['userId'], $params['userId']));

        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Get member messages failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Get member messages success',
                'message' => $rsobj->GetRows(),
            );
        }

        return $this->status;
    }
    */
    /**
    * Returns an json obj of get Message Detail By Id for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function getMessageDetailById($params)
    {
        $rsobj = $this->dbcon->Execute(GET_MESSAGES_DETAIL_BY_ID, array($params['messageId']));

        $this->status = array(
            'status' => 'error',
            'status_code' => '0',
            'status_message' => 'Get message detail failed',
        );

        if ($rsobj->RecordCount() > 0) {
            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Get message detail success',
                'message' => $rsobj->GetRows(),
            );
        }

        return $this->status;
    }
    */
    /**
    * Returns an json obj of insert New Message for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function insertNewMessage($params)
    {
        $rsobj = $this->dbcon->Execute(INERT_NEW_MESSAGE, array($params['message_id']));

        if ($rsobj->RecordCount() > 0) {
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $params);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Message update success',
            );
        } else {
            $rsInserts = $this->dbcon->GetInsertSql($rsobj, $params);
            $this->dbcon->Execute($rsInserts);

            //Update parent message date
            if ($params['message_parent_id'] > 0) {
                $rsParent = $this->dbcon->Execute(INERT_NEW_MESSAGE, array($params['message_parent_id']));
                if ($rsParent->RecordCount() > 0) {
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $data['is_read'] = 0;
                    $rsUpdatesParent = $this->dbcon->GetUpdateSql($rsParent, $data);
                    $this->dbcon->Execute($rsUpdatesParent);
                }
            }

            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Message insert success',
            );
        }

        return $this->status;
    }
    */
    /**
    * Returns an json obj of delete Message By Id for a company.
    *
    * @param array $params service parameter, Search arguments 
    *
    * @return array object object
    */
    /*public function deleteMessageById($params)
    {
        $rsobj = $this->dbcon->Execute(DELETE_MESSAGE_BY_ID, array($params['messageId']));

        if ($rsobj->RecordCount() > 0) {
            $data['is_deleted'] = 1;
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Delete message success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => '200',
                'status_message' => 'Delete message failed',
            );
        }

        return $this->status;
    }
    */
    
    /**
    * To get mark message read by id
    * 
    * @param array $params service parameter, Search arguments
    *
    * @return array object
    */
    /*public function markMessageReadById($params)
    {
        $rsobj = $this->dbcon->Execute(MARK_MESSAGE_READ_BY_ID, array($params['messageId']));

        if ($rsobj->RecordCount() > 0) {
            $data['is_read'] = 1;
            $rsUpdates = $this->dbcon->GetUpdateSql($rsobj, $data);
            $this->dbcon->Execute($rsUpdates);

            $this->status = array(
                'status' => 'success',
                'status_code' => '200',
                'status_message' => 'Mark message as read success',
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => '200',
                'status_message' => 'Mark message as read failed',
            );
        }

        return $this->status;
    }
    */
    /**
     * To get message details
     *
     * @param array $params service parameter, Search arguments
     *
     * @return array object
     */
    public function getMessageDetails($params)
    {
        $startlimit = $params['limit_start'];
        $records = $params['limit_records'];
        $query = GET_MESSAGE_DETAILS;
        if ($records != -1) {
            $query .= ' LIMIT '.$startlimit.','.$records;
        }
        $rsobj = $this->dbcon->Execute($query);//,array($startlimit,$records);
        if ($rsobj->RecordCount()) {
            $message    =   array();
            while (!$rsobj->EOF) {
                $message[] = $rsobj->fields;
                $rsobj->MoveNext();
            }
            $this->status = array(
                'status' => 'success',
                'status_code' => 1,
                'status_message' => 'Get Message Details',
                'recordsfound' => $rsobj->RecordCount(),
                'total_records' => $this->getLastQueryTotalCount(),
                // 'sql' => $rsobj->sql,
                'message' => $message,
            );
        } else {
            $this->status = array(
                'status' => 'error',
                'status_code' => 0,
                'status_message' => 'Get Message Details Not Found.',
                'recordsfound' => 0,
                'total_records' => $this->getLastQueryTotalCount(),
                // 'sql' => $rsobj->sql,
                'message' => '',
            );
        }
        //Return the result array
        return $this->status;
    }
    /**
     * Get Total Record Count. Common to All Message Model
     *
     * @return Integer totalCount
     */
    public function getLastQueryTotalCount()
    {
        $rsobj = $this->dbcon->Execute(GET_QUERY_LAST_TOTAL_COUNT);

        $toatlCount = 0;

        if ($rsobj->RecordCount()) {
            $row = $rsobj->GetRows();
            $toatlCount = $row[0]['total_count'];
        }

        return $toatlCount;
    }

} // End Class.
;
