<?php 
include_once("../include/define.php");
include_once(BASE_DIR.'classes'.DS.'class.controller.php'); 
global $isCronVarApi;
$isCronVarApi = 1;
$controller=new controller();
if(isset($_REQUEST['p'])){
	$p=$_REQUEST['p'];
	switch($p){
		case('getStatusOfTheTest'):				 
			include_once('getStatusOfTheTest.php');
			break;	
		case('getCycleTestlevels'):				 
			include_once('getCycleTestlevels.php');
			break;
		case('pushRpmAndHr') :			 
			include_once('pushRpmAndHr.php');
			break;				
		case('pushCycleAvailability'):				 
			include_once('pushCycleAvailability.php');
			break;
		case('getClubs') :			 
			include_once('getClubs.php');
			break;					
	}
}

?>