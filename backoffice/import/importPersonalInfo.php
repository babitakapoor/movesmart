<?php
/**
 * PHP version 5.
 
 * @category Import
 
 * @package Import_personal_info
 
 * @author Shanetha Tech <info@shanethatech.com>
 
 * @license movesmart.company http://movesmart.company
 
 * @link http://movesmart.company/admin/

 * @description To all import functions.
 */
    $parameter = json_decode($_REQUEST['param'], true);

    // construting the parameter in order to make the query construction dynamically in ADODB file .
    $params['r_city_id'] = (isset($parameter['city']) && $parameter['city'] != '') ? $this->members->getCityId(array('city' => $parameter['city'])) : '';
    $params['r_profession_id'] = (isset($parameter['profession']) && $parameter['profession'] != '') ? $this->members->getProfessionId(array('profession' => $parameter['profession'])) : '';
    $params['r_nationality_id'] = (isset($parameter['nationality']) && $parameter['nationality'] != '') ? $this->members->getNationalityId(array('nationality' => $parameter['nationality'])) : '';

    //Return the error if names are not found in server

    ($params['r_city_id'] == 'false' && $params['city'] != '') ? die('City Name is Not Found') : $params['r_city_id'];
    ($params['r_profession_id'] == 'false' && $params['profession'] != '') ? die('Profession Name is Not Found') : '';
    ($params['r_nationality_id'] == 'false' && $params['nationality'] != '') ? die('Nationality Name is Not Found') : $params['r_nationality_id'];

    $params['person_id'] = (!isset($parameter['personId']) && $parameter['personId'] != '') ? die('Required Person Id') : $parameter['personId'];

    $parameters['personalDataPersonId'] = $parameter['personId'];
    $parameters['companyId'] = $parameter['companyId'];
    $memberBasicDetail = $this->members->getMemberBasicDetail($parameters);

    echo $params['r_user_id'] = (!isset($memberBasicDetail['user_id']) && $memberBasicDetail['user_id'] != '') ? die('Required Person Id') : $memberBasicDetail['user_id'];
    $params['doorno'] = $parameter['doorno'];
    $params['address'] = $parameter['address'];
    $params['zipcode'] = $parameter['zipcode'];
    $params['bus'] = $parameter['bus'];
    $params['phone'] = $parameter['phone'];
    $params['mobile'] = $parameter['mobile'];
    $params['fax'] = $parameter['fax'];
    $params['dob'] = $this->common->siteSaveDateFormat($parameter['dob']);
    $params['nativelanguage'] = $parameter['nativelanguage'];
    $params['company'] = $parameter['company'];
    $params['hobby'] = $parameter['hobby'];
    $params['training'] = $parameter['training'];
    $params['location'] = $parameter['location'];
    $params['height'] = $parameter['height'];
    $params['weight'] = $parameter['weight'];
    $params['securityno'] = $parameter['securityno'];
    $params['location_'.$parameter['location']] = 1;
    $params['gsm'] = $parameter['gsm'];
    $params['doctor'] = $parameter['doctor'];
    $params['doctor_phone'] = $parameter['doctor_phone'];
    $params['doctor_specification'] = $parameter['doctor_specification'];
    $params['company_doctor'] = $parameter['company_doctor'];
    $params['company_doctor_phone'] = $parameter['company_doctor_phone'];
    $params['company_specification'] = $parameter['company_specification'];
    $params['specialist'] = $parameter['specialist'];
    $params['specialist_phone'] = $parameter['specialist_phone'];
    $params['specialist_specification'] = $parameter['specialist_specification'];
    $params['work_starton'] = $this->common->siteSaveDateFormat($parameter['work_starton']);
    $params['work_endon'] = $this->common->siteSaveDateFormat($parameter['work_endon']);
    $params['notes'] = $parameter['notes'];

    $result = $this->import->importPersonalInfo($params);
    echo $result;
